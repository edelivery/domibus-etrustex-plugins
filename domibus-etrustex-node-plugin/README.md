# README

## Configuration

In domibus.properties:

    domibus.auth.unsecureLoginAllowed=false
    domibus.entityManagerFactory.packagesToScan=eu.domibus,eu.europa.ec.etrustex

## Release

### Databases

#### Oracle

#####Liquibase generation

In order to generate the full SQL statements of a release, use the profile 'liquibase' and run 
   
    mvn clean compile -Pliquibase
the file databasechangelog.csv

This profile is only meant for the release process. DO NOT USE in the bamboo build.
In the 'liquibase' profile of pom.xml, a new executions should be added for oracle with the new 'changelog' file entry for liquibase generation.
The file shoud be created in the folder 
    
    src/main/conf/oracle

## Local End to End Testing
To mock eTrustEx Node, in etrustex-node-plugin.properties change:
 1 etx.node.services.DocumentBundleService.endpoint= URL of ETX Node mock - http://localhost:8080/etrustexnodemock
 3 etx.node.services.ApplicationResponseService.endpoint=  URL for Node plugin Inbox ApplicationResponse
 4 etx.node.services.DocumentWrapperService.endpoint = wiremock endpoint
 5 etx.node.services.RetrieveInterchangeAgreementService.endpoint = wiremock endpoint

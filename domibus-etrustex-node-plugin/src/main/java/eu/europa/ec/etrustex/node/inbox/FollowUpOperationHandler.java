package eu.europa.ec.etrustex.node.inbox;

import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;

/**
 * Interface for the follow-up of each inbox action.
 * It exposes methods to handle positive and negative scenario.
 *
 * @author Federico Martini
 * @version 1.0
 * @since 14/11/2017
 */
public interface FollowUpOperationHandler {

    /**
     * Whenever a message has been correctly sent through Domibus, this method will be called on the appropriate handler
     *
     * @param dto - input object of type {@link ETrustExAdapterDTO}
     */
    void handleSendMessageSuccess(ETrustExAdapterDTO dto);

    /**
     * Whenever a message has failed to be sent through Domibus, this method will be called on the appropriate handler
     *
     * @param dto - input object of type {@link ETrustExAdapterDTO}
     */
    void handleSendMessageFailed(ETrustExAdapterDTO dto);
}

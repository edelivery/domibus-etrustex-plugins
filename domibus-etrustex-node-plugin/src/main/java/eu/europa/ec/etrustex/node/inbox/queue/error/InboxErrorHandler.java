package eu.europa.ec.etrustex.node.inbox.queue.error;

import eu.europa.ec.etrustex.node.inbox.queue.QueueMessage;

/**
 * Base methods to handle {@link QueueMessage} from the queue etxNodePluginInboxErrorQueue
 *
 * @author François Gautier
 * @version 1.0
 * @since 10-Nov-17
 */
public interface InboxErrorHandler<T extends QueueMessage> {

    /**
     * Update the entities and send notification
     * @param queueMessage information from the error queue
     */
    void handleErrorWithNotification(T queueMessage);

    /**
     * Only update the entities
     * @param queueMessage information from the error queue
     */
    void handleErrorWithoutNotification(T queueMessage);
}

package eu.europa.ec.etrustex.node.inbox.queue.messagetransmission;

/**
 * Enum to differentiate between transmission of a bundle and transmission of a wrapper
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 01/12/2017
 */
public enum MessageTransmission {
    BUNDLE,
    WRAPPER
}

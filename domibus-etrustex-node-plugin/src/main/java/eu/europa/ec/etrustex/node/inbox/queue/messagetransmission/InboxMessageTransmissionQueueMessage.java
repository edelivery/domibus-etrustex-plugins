package eu.europa.ec.etrustex.node.inbox.queue.messagetransmission;

import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import eu.europa.ec.etrustex.node.inbox.queue.QueueMessage;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

/**
 * Message Transmission queue message type
 *
 * @author Catalin Enache, Arun Raj
 * @version 1.0
 * @since 25/09/2017
 */
public class InboxMessageTransmissionQueueMessage extends QueueMessage {

    /**Id(primary key) of ETX_MESSAGE table*/
    private Long messageId;

    /**Id of ETX_ATTACHMENT table. May be null if no wrapper needs to be transmitted.*/
    private Long attachmentId;

    /** to differentiate between bundle(default value) and wrapper transmission **/
    private MessageTransmission messageTransmission = MessageTransmission.BUNDLE;

    /**
     * Empty constructor
     */
    public InboxMessageTransmissionQueueMessage(){}

    public InboxMessageTransmissionQueueMessage(final MessageType messageType, final MessageDirection messageDirection,
                                                final Long messageId) {
        super(messageType, messageDirection);
        this.messageId = messageId;
    }

    public InboxMessageTransmissionQueueMessage(final MessageType messageType, final MessageDirection messageDirection,
                                                final Long messageId, final Long attachmentId) {
        super(messageType, messageDirection);
        this.messageId = messageId;
        this.attachmentId = attachmentId;
    }

    public InboxMessageTransmissionQueueMessage(final MessageType messageType,
                                                final MessageDirection messageDirection,
                                                final MessageTransmission messageTransmission,
                                                final Long messageId, final Long attachmentId) {
        super(messageType, messageDirection);
        this.messageTransmission = messageTransmission;
        this.messageId = messageId;
        this.attachmentId = attachmentId;
    }

    @Override
    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    @Override
    public Long getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Long attachmentId) {
        this.attachmentId = attachmentId;
    }

    public MessageTransmission getMessageTransmission() {
        return messageTransmission;
    }

    public void setMessageTransmission(MessageTransmission messageTransmission) {
        this.messageTransmission = messageTransmission;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof InboxMessageTransmissionQueueMessage)) return false;

        InboxMessageTransmissionQueueMessage that = (InboxMessageTransmissionQueueMessage) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(messageTransmission, that.messageTransmission)
                .append(messageId, that.messageId)
                .append(attachmentId, that.attachmentId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(messageTransmission)
                .append(messageId)
                .append(attachmentId)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .appendSuper(super.toString())
                .append("messageTransmission", messageTransmission)
                .append("messageId", messageId)
                .append("attachmentId", attachmentId)
                .toString();
    }
}

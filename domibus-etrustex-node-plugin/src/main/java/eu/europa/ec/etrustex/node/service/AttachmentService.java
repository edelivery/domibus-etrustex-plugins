package eu.europa.ec.etrustex.node.service;

import eu.europa.ec.etrustex.node.data.model.attachment.AttachmentState;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import org.springframework.transaction.annotation.Propagation;

/**
 * Interface which declares the operations needed for {@code EtxAttachment} entities
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 21/09/2017
 */
public interface AttachmentService {

    /**
     * Load an {@code EtxAttachment} using the attachmentId
     *
     * @param attachmentId id of the attachment to load
     * @return {@code EtxAttachment} corresponding to DB record
     */
    EtxAttachment findAttachmentById(final Long attachmentId);

    /**
     * Finds an EtxAttachment entity by the Domibus message id.
     *
     * @param domibusMessageId a Domibus message id
     * @return {@code EtxAttachment} the persistent object with the given Domibus message id, or null if not found
     */
    EtxAttachment findAttachmentByDomibusMessageId(final String domibusMessageId);

    /**
     * Performs update on {@code EtxAttachment} entity only if oldState match
     *
     * @param attachmentEntity attachment entity to be updated
     * @param oldState         old state
     * @param newState         new state
     */
    void updateAttachmentState(EtxAttachment attachmentEntity, AttachmentState oldState, AttachmentState newState);

    /**
     * Updates the {@link EtxAttachment} entity
     *
     * @param etxAttachment Instance of {@link EtxAttachment} with the values to be updated
     * @see eu.europa.ec.etrustex.node.data.dao.DaoBase#update(Object)
     */
    void update(EtxAttachment etxAttachment);

    /**
     * Set the {@link EtxAttachment} of a given id in {@link AttachmentState#FAILED} state and add an error with a given message
     * This method has {@link Propagation#REQUIRES_NEW}
     *
     * @param id           of a {@link EtxAttachment}
     * @param errorMessage Message to be included in the Error object
     */
    void attachmentInError(Long id, String errorMessage);
}

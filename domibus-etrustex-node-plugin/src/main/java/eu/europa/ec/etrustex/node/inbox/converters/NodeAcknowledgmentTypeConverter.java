package eu.europa.ec.etrustex.node.inbox.converters;

import ec.schema.xsd.ack_2.AcknowledgmentType;
import ec.schema.xsd.commonaggregatecomponents_2.AcknowledgedDocumentReferenceType;
import ec.schema.xsd.commonbasiccomponents_1.AckIndicatorType;
import eu.europa.ec.etrustex.common.converters.node.NodePartyConverter;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.DocumentReferenceType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DocumentTypeCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IssueDateType;
import org.joda.time.DateTime;

/**
 * Utility class converter for building {@link AcknowledgmentType} object use as part of
 * Node response to be sent back
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 13/10/2017
 */
public class NodeAcknowledgmentTypeConverter {

    /**
     * private constructor. all methods are static
     */
    private NodeAcknowledgmentTypeConverter() {
    }

    /**
     * It builds an object of type {@link AcknowledgmentType} which is part of Node response
     * @param issueDate           issue date
     * @param documentReferenceID document reference id
     * @param documentTypeCode    document type code - message bundle or status message
     * @param senderPartyUuid     sender party uuid
     * @param receiverPartyUuid   receiver party uuid   @return {@link AcknowledgmentType} object
     */
    public static AcknowledgmentType buildAcknowledgmentType(final DateTime issueDate, final String documentReferenceID,
                                                             final String documentTypeCode, final String senderPartyUuid, final String receiverPartyUuid) {
        final AcknowledgmentType acknowledgmentType = new AcknowledgmentType();


        //ack indicator
        final AckIndicatorType ackIndicatorType = new AckIndicatorType();
        ackIndicatorType.setValue(true);
        acknowledgmentType.setAckIndicator(ackIndicatorType);

        //issue date
        final IssueDateType issueDateType = new IssueDateType();
        issueDateType.setValue(issueDate);
        acknowledgmentType.setIssueDate(issueDateType);

        //document bundle or message status id
        final AcknowledgedDocumentReferenceType acknowledgedDocumentReferenceType = new AcknowledgedDocumentReferenceType();
        final DocumentReferenceType documentReferenceType = new DocumentReferenceType();
        final IDType idType = new IDType();
        idType.setValue(documentReferenceID);
        documentReferenceType.setID(idType);

        //document type code
        final DocumentTypeCodeType documentTypeCodeType = new DocumentTypeCodeType();
        documentTypeCodeType.setValue(documentTypeCode);
        documentReferenceType.setDocumentTypeCode(documentTypeCodeType);

        acknowledgedDocumentReferenceType.setDocumentReference(documentReferenceType);

        //sender party
        acknowledgedDocumentReferenceType.setSenderParty(NodePartyConverter.buildPartyType(senderPartyUuid));

        //receiver party
        acknowledgedDocumentReferenceType.getReceiverParty().add(NodePartyConverter.buildPartyType(receiverPartyUuid));

        acknowledgmentType.setAcknowledgedDocumentReference(acknowledgedDocumentReferenceType);

        return acknowledgmentType;
    }
}

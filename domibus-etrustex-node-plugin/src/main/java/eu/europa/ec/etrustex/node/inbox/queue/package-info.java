/**
 * This package contains classes needed to handle JMS operations for
 * {@code InboxServices} defined queues
 *
 * To add also in the future OutboxServices ones
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 24/10/2017
 */
package eu.europa.ec.etrustex.node.inbox.queue;
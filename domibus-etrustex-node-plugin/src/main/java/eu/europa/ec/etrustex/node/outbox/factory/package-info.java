/**
 * This package contains all the factories used for the outbox services.
 *
 * @author Federico Martini
 * @version 1.0
 * @since 26/10/2017
 */
package eu.europa.ec.etrustex.node.outbox.factory;
package eu.europa.ec.etrustex.node.inbox.queue.messagetransmission;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import eu.europa.ec.etrustex.node.domain.DomainNodePlugin;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.Queue;

/**
 * MessageTransmission queue producer for InboxServices.<br/>
 * <p>
 * Converts the message to JSON format before sending as TEXT message to the queue.<br/>
 * <p>
 * The JSON converter is defined in Spring xml configuration file
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 26/09/2017
 */
@Service("etxNodePluginInboxMessageTransmissionProducer")
public class InboxMessageTransmissionProducer {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxMessageTransmissionProducer.class);

    private final Queue jmsQueue;

    private final JmsTemplate jmsTemplate;

    private ClearEtxLogKeysService clearEtxLogKeysService;
    private DomainNodePluginService domainNodePluginService;

    public InboxMessageTransmissionProducer(@Qualifier("etxNodePluginInboxMessageTransmissionQueue") Queue jmsQueue,
                                            @Qualifier("etxNodePluginInboxMessageTransmissionJmsTemplate") JmsTemplate jmsTemplate,
                                            ClearEtxLogKeysService clearEtxLogKeysService,
                                            DomainNodePluginService domainNodePluginService) {
        this.jmsQueue = jmsQueue;
        this.jmsTemplate = jmsTemplate;
        this.clearEtxLogKeysService = clearEtxLogKeysService;
        this.domainNodePluginService = domainNodePluginService;
    }

    /**
     * Send a TEXT (Json) message to dedicated queue {@code etxNodePluginInboxMessageTransmissionQueue}
     * to trigger message bundle transmission to the backed. <br/>
     * This is to be used for bundle transmission to backend.<br/>
     *
     * @param messageId id of the message
     */
    @DomainNodePlugin
    @ClearEtxLogKeys
    public void triggerBundleTransmissionToBackend(final Long messageId) {
        domainNodePluginService.setNodePluginDomain();
        LOG.debug("triggerBundleTransmissionToBackend -> start");

        final InboxMessageTransmissionQueueMessage queueMessage = new InboxMessageTransmissionQueueMessage(MessageType.MESSAGE_BUNDLE,
                MessageDirection.NODE_TO_BACKEND, messageId);

        LOG.info("triggerBundleTransmissionToBackend -> put message with messageId= [{}]", messageId);
        jmsTemplate.convertAndSend(jmsQueue, queueMessage);

        LOG.debug("triggerBundleTransmissionToBackend -> end");
        clearEtxLogKeysService.clearKeys();
    }

    /**
     * Send a TEXT (Json) message to dedicated queue {@code etxNodePluginInboxMessageTransmissionQueue}
     * to trigger wrapper transmission to backend.
     * To be used in case of wrapper transmission where attachment id will be known
     *
     * @param messageId    id of the message
     * @param attachmentId id of the attachment
     */
    public void triggerWrapperTransmissionToBackend(final Long messageId, final Long attachmentId) {
        LOG.debug("triggerMessageTransmissionToBackend -> start");

        final InboxMessageTransmissionQueueMessage queueMessage = new InboxMessageTransmissionQueueMessage(MessageType.MESSAGE_BUNDLE,
                MessageDirection.NODE_TO_BACKEND, MessageTransmission.WRAPPER, messageId, attachmentId);

        LOG.info("triggerMessageTransmissionToBackend -> put message with messageId=[{}] attachmentId=[{}]", messageId, attachmentId);

        jmsTemplate.convertAndSend(jmsQueue, queueMessage);
        LOG.debug("triggerMessageTransmissionToBackend -> end");

    }

    /**
     * send a TEXT (Json) message to dedicated queue {@code etxNodePluginInboxMessageTransmissionQueue}
     * for trigger message status transmission to the backend
     *
     * @param messageId id of the message
     */
    public void triggerMessageTransmissionToBackend(final Long messageId) {
        LOG.debug("triggerMessageTransmissionToBackend -> start");

        final InboxMessageTransmissionQueueMessage queueMessage = new InboxMessageTransmissionQueueMessage(MessageType.MESSAGE_STATUS,
                MessageDirection.NODE_TO_BACKEND, messageId);

        LOG.info("triggerMessageTransmissionToBackend -> put message with messageId= [{}]", messageId);
        jmsTemplate.convertAndSend(jmsQueue, queueMessage);

        LOG.debug("triggerMessageTransmissionToBackend -> end");

    }

    /**
     * Resend a TEXT (Json) message to queue {@code etxNodePluginInboxMessageTransmissionQueue}
     * the message contains previously populated error information
     *
     * @param message JMS message of type {@link InboxMessageTransmissionQueueMessage}
     */
    public void triggerMessageWithError(final InboxMessageTransmissionQueueMessage message) {
        LOG.debug("triggerMessageWithError -> start");

        LOG.info("triggerMessageWithError -> put message: {}", message);
        jmsTemplate.convertAndSend(jmsQueue, message);

        LOG.debug("triggerMessageWithError -> end");
    }

}

package eu.europa.ec.etrustex.node.outbox.handlers;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.jms.TextMessageCreator;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.jms.core.JmsOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * @author Federico Martini
 * @since 24/03/2017
 * @version 1.0
 */
@Service("etxNodePluginRICAHandler")
@Scope("prototype")
public class RetrieveICAHandler implements ETrustExOperationHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(RetrieveICAHandler.class);

    @Resource(name = "etxNodePluginBackendToNodeRICAJmsTemplate")
    private JmsOperations jmsOperations;

    @Override
    public boolean handle(ETrustExAdapterDTO etxDto) {

        jmsOperations.send(buildJMSMessage(etxDto));
        LOG.info("AS4 Message Id [{}]. RetrieveICARequest message from backend plugin was posted into etxNodePluginInboundRICAQueue.", etxDto.getAs4MessageId() );
        return true;
    }

    @Override
    public TextMessageCreator buildJMSMessage(ETrustExAdapterDTO etxDto) {
        String dtoAsXml = TransformerUtils.serializeObjToXML(etxDto);
        TextMessageCreator msgCreator = new TextMessageCreator(dtoAsXml);
        return msgCreator;
    }

}

package eu.europa.ec.etrustex.node.service;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.util.LoggingUtils;
import eu.europa.ec.etrustex.node.data.dao.MessageDao;
import eu.europa.ec.etrustex.node.data.model.common.EtxError;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import static eu.europa.ec.etrustex.node.data.model.common.ErrorType.TECHNICAL;

/**
 * {@code MessageServicePlugin} implementation
 * <p>
 * Responsible for creation and update of messages {@code EtxMessage}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 21/09/2017
 */
@Service("etxNodePluginMessageServicePlugin")
public class MessageServicePluginImpl implements MessageServicePlugin {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(MessageServicePluginImpl.class);

    @Autowired
    private MessageDao messageDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public void createMessage(EtxMessage messageEntity) {
        messageDao.save(messageEntity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EtxMessage findMessageById(long etxMessageId) {
        EtxMessage byId = messageDao.findById(etxMessageId);
        LOG.putMDC(LoggingUtils.MDC_ETX_MESSAGE_UUID, byId.getMessageUuid());
        return byId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(EtxMessage etxMessage) {
        messageDao.update(etxMessage);
    }

    @Override
    public EtxMessage findMessageByDomibusMessageId(String domibusMsgId) {
        return messageDao.findMessageByDomibusMessageId(domibusMsgId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void messageInError(Long id, String errorMessage) {
        EtxMessage message = findMessageById(id);
        message.setMessageState(MessageState.FAILED);
        message.setError(new EtxError(TECHNICAL.getCode(), errorMessage));
    }
}

package eu.europa.ec.etrustex.node.inbox.client.adapter;

import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.services.wsdl.applicationresponse_2.ApplicationResponsePortType;
import ec.services.wsdl.applicationresponse_2.FaultResponse;
import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseRequest;
import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseResponse;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import eu.europa.ec.etrustex.node.data.model.AuditEntity;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.data.model.common.StatusResponseCode;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.xml.ws.Holder;
import java.util.UUID;


/**
 * Webservice client for SubmitApplicationResponse operation in case of error in the inbox service
 *
 * @author François Gautier
 * @version 1.0
 * @since 14-Nov-17
 */
@Service("etxNodePluginSubmitApplicationResponseFaultAdapter")
@Scope("prototype")
public class SubmitApplicationResponseFaultAdapter extends NodeServicesAdapterBase {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SubmitApplicationResponseFaultAdapter.class);

    public static final String MESSAGE_BUNDLE = "BDL";

    @Autowired
    public ETrustExNodePluginProperties eTrustExNodePluginProperties;

    @Override
    public String getServiceEndpoint() {
        return eTrustExNodePluginProperties.getApplicationResponseServiceEndpoint();
    }

    /**
     * Send the notification back to the EtrustExNode following the failure of a MessageBundle
     *
     * @param sender      {@link EtxParty} sending the notification (usually is the receiver of the failed message)
     * @param receiver    {@link EtxParty} receiving the notification (usually is the sender of the failed message)
     * @param messageUuid {@link EtxMessage#getMessageUuid()} of the failed message
     * @throws FaultResponse in case of error invoking {@link ApplicationResponsePortType#submitApplicationResponse(SubmitApplicationResponseRequest, Holder)}
     */
    public void sendNotification(EtxParty sender, EtxParty receiver, String messageUuid) throws FaultResponse {

        //fill in authorisation header
        Holder<HeaderType> authorisationHeader = fillAuthorisationHeader(sender.getPartyUuid(), receiver.getPartyUuid());

        //invoke the Node webservice
        ApplicationResponsePortType port = buildApplicationResponsePort(sender.getNodeUser().getName(), sender.getNodeUser().getPassword());

        SubmitApplicationResponseRequest submitApplicationResponseRequest = new SubmitApplicationResponseRequest();

        String uuid = UUID.randomUUID().toString();
        submitApplicationResponseRequest.setApplicationResponse(ApplicationResponseTypeBuilder.getInstance()
                .withType(MESSAGE_BUNDLE)
                .withMessageUuid(uuid)
                .withMessageState(StatusResponseCode.BDL4.getCode())
                .withIssueDateTime(AuditEntity.getGMTCalendar())
                .withMessageReferenceUuid(messageUuid)
                .build());

        SubmitApplicationResponseResponse submitApplicationResponseResponse = port.submitApplicationResponse(submitApplicationResponseRequest, authorisationHeader);
        LOG.info("Node submitApplicationResponse service called for message Uuid= {}", messageUuid);
        boolean result = submitApplicationResponseResponse.getAck().getAckIndicator().isValue();
        LOG.info("Received response from NODE for submitApplicationResponse with UUID[{}] and response acknowledgement[{}]", uuid, result);
    }
}

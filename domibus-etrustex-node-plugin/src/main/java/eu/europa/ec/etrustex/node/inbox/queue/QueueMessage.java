package eu.europa.ec.etrustex.node.inbox.queue;

import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Base JMS message class for Inbox Services.
 *
 * Marked as {@code abstract} in order to be extended by default
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 25/09/2017
 */
public abstract class QueueMessage {

    /** message type */
    private MessageType messageType;

    /** message direction */
    private MessageDirection messageDirection;

    /** error code */
    private String errorCode;

    /** error description */
    private String errorDescription;

    public QueueMessage() {}

    public QueueMessage(MessageType messageType, MessageDirection messageDirection) {
        this.messageType = messageType;
        this.messageDirection = messageDirection;
    }

    public abstract Long getMessageId();

    public abstract Long getAttachmentId();

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public MessageDirection getMessageDirection() {
        return messageDirection;
    }

    public void setMessageDirection(MessageDirection messageDirection) {
        this.messageDirection = messageDirection;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof QueueMessage)) return false;

        QueueMessage that = (QueueMessage) o;

        return new EqualsBuilder()
                .append(messageType, that.messageType)
                .append(messageDirection, that.messageDirection)
                .append(errorCode, that.errorCode)
                .append(errorDescription, that.errorDescription)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(messageType)
                .append(messageDirection)
                .append(errorCode)
                .append(errorDescription)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("messageType", messageType)
                .append("messageDirection", messageDirection)
                .append("errorCode", errorCode)
                .append("errorDescription", errorDescription)
                .toString();
    }
}

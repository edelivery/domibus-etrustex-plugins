package eu.europa.ec.etrustex.node.inbox.queue.error;

import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxMessageTransmissionQueueMessage;
import org.springframework.stereotype.Service;

/**
 * Handle the errors in the MessageBundle flow from the jms queue etxNodePluginInboxErrorQueue
 *
 * @author François Gautier
 * @version 1.0
 * @since 10-Nov-17
 */
@Service("etxNodePluginInboxErrorMessageTransmissionHandler")
public class InboxErrorMessageTransmissionHandler extends InboxErrorHandlerBase<InboxMessageTransmissionQueueMessage> {

}

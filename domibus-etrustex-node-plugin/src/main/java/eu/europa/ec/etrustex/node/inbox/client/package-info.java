/**
 * This package contains all the client classes necessary to call the eTrustEx node WS for Inbox services.
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 04/10/2017
 */
package eu.europa.ec.etrustex.node.inbox.client;
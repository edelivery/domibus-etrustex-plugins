package eu.europa.ec.etrustex.node.service.security;

import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;

/**
 * Responsible for authentication and authorization of users and parties
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 19/09/2017
 */
public interface IdentityManager {

    /**
     * search a Party by party Uuid
     *
     * @param partyUuid party Uuid to search for
     * @return party from DB of type {@link EtxParty} or null if not found
     */
    EtxParty searchPartyByUuid(String partyUuid);

}

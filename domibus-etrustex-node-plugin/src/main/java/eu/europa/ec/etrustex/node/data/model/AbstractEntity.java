package eu.europa.ec.etrustex.node.data.model;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * @author François GAUTIER
 * @version 1.0
 * @since 20/09/2017
 */
@MappedSuperclass
public abstract class AbstractEntity<T extends Serializable> {

    public abstract T getId();

    public abstract void setId(T id);
}

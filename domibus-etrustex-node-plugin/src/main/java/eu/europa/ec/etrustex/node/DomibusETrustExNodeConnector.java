package eu.europa.ec.etrustex.node;

import eu.domibus.common.DeliverMessageEvent;
import eu.domibus.common.MessageReceiveFailureEvent;
import eu.domibus.common.MessageSendFailedEvent;
import eu.domibus.common.MessageSendSuccessEvent;
import eu.domibus.ext.domain.MessageAttemptDTO;
import eu.domibus.ext.domain.UserMessageDTO;
import eu.domibus.ext.services.MessageMonitorExtService;
import eu.domibus.ext.services.UserMessageExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.MessageNotFoundException;
import eu.domibus.plugin.AbstractBackendConnector;
import eu.domibus.plugin.transformer.MessageRetrievalTransformer;
import eu.domibus.plugin.transformer.MessageSubmissionTransformer;
import eu.europa.ec.etrustex.common.converters.ETrustExAdapterDTOSubmissionConverter;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.handlers.Action;
import eu.europa.ec.etrustex.common.handlers.Service;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.util.*;
import eu.europa.ec.etrustex.node.domain.DomainNodePlugin;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import eu.europa.ec.etrustex.node.inbox.FollowUpOperationHandler;
import eu.europa.ec.etrustex.node.inbox.FollowUpOperationHandlerFactory;
import eu.europa.ec.etrustex.node.outbox.factory.ETrustExOperationHandlerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_FAULT;
import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_CONVERSATION_ID;


/**
 * This class is implementing the connector between Domibus access point and the ETrustEx node.
 *
 * @author Federico Martini
 * @version 1.0
 * @since 23/03/2017
 */
public class DomibusETrustExNodeConnector extends AbstractBackendConnector<ETrustExAdapterDTO, ETrustExAdapterDTO> {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomibusETrustExNodeConnector.class);

    private static ETrustExAdapterDTOSubmissionConverter defaultTransformer = new ETrustExAdapterDTOSubmissionConverter();

    @Autowired
    protected ETrustExOperationHandlerFactory operationHandlerFactory;

    @Autowired
    protected FollowUpOperationHandlerFactory followUpOperationHandlerFactory;

    @Autowired
    private MessageMonitorExtService messageMonitorService;

    @Autowired
    private UserMessageExtService userMessageService;

    @Autowired
    private ClearEtxLogKeysService clearEtxLogKeysService;

    @Autowired
    private DomainNodePluginService domainNodePluginService;

    public DomibusETrustExNodeConnector(String name) {
        super(name);
    }

    @Override
    public MessageSubmissionTransformer<ETrustExAdapterDTO> getMessageSubmissionTransformer() {
        return defaultTransformer;
    }

    @Override
    public MessageRetrievalTransformer<ETrustExAdapterDTO> getMessageRetrievalTransformer() {
        return defaultTransformer;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    @DomainNodePlugin
    @ClearEtxLogKeys
    public void deliverMessage(DeliverMessageEvent deliverMessageEvent) {
        domainNodePluginService.setNodePluginDomain();
        String messageId = deliverMessageEvent.getMessageId();
        ETrustExAdapterDTO dto = new ETrustExAdapterDTO();

        try {
            dto = downloadMessage(messageId, dto);
            LOG.putMDC(MDC_ETX_CONVERSATION_ID, dto.getAs4ConversationId());
        } catch (final MessageNotFoundException ex) {
            LOG.error("Message cannot be downloaded due to:", ex);
            throw new ETrustExPluginException(ETrustExError.ETX_005, "Message cannot be downloaded", ex);
        }

        try {
            Service service = Service.fromString(dto.getAs4Service());
            Action action = Action.fromString(dto.getAs4Action());
            operationHandlerFactory.getOperationHandler(service, action).handle(dto);
        } catch (Exception ex) {
            LOG.error("Could not perform operation with {}",dto , ex);
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "Could not perform operation", ex);
        }
        clearEtxLogKeysService.clearKeys();
    }

    /**
     * Retrieves a Domibus message using the external services API. This is done to understand the action and to identify the correct operation handler.
     * In case of an exception, the transaction (started by Domibus) is rolled back and the JMS message will be put again into EtxNodePluginNotificationQueue.
     *
     * @param messageSendSuccessEventevent a message send success event with the Domibus message id
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    @ClearEtxLogKeys
    public void messageSendSuccess(MessageSendSuccessEvent messageSendSuccessEventevent) {
        String messageId = messageSendSuccessEventevent.getMessageId();
        try {
            UserMessageDTO userMsgDTO = userMessageService.getMessage(messageId);
            LOG.putMDC(MDC_ETX_CONVERSATION_ID, userMsgDTO.getCollaborationInfo().getConversationId());
            LOG.info("The submitted message was sent successfully.");
            LOG.debug("User message retrieved: {}", userMsgDTO);

            ETrustExAdapterDTO dto = new ETrustExAdapterDTO();
            dto.setAs4MessageId(userMsgDTO.getMessageInfo().getMessageId());

            Action action = Action.fromString(userMsgDTO.getCollaborationInfo().getAction());

            FollowUpOperationHandler fuHandler = followUpOperationHandlerFactory.getOperationHandler(action);
            fuHandler.handleSendMessageSuccess(dto);
        } catch (Exception e) {
            LOG.error("For Domibus message with id [{}] in success encountered exception", messageId, e);
            throw e;
        }
        clearEtxLogKeysService.clearKeys();
    }

    /**
     * Retrieves a Domibus message using the external services API. This is done to understand the action and to identify the correct operation handler.
     *
     * @param messageSendFailedEvent a message send failure event with the Domibus message id
     */
    @Override
    @ClearEtxLogKeys
    public void messageSendFailed(MessageSendFailedEvent messageSendFailedEvent) {
        String messageId = messageSendFailedEvent.getMessageId();
        LOG.warn("Domibus message with ID: [{}] failed to be sent. Handling message failure.", messageId);

        try {
            UserMessageDTO userMsgDTO = userMessageService.getMessage(messageId);
            LOG.debug("For AS4 message ID: [{}], User message retrieved:[{}]", messageId, userMsgDTO);
            ETrustExAdapterDTO dto = new ETrustExAdapterDTO();
            dto.setAs4MessageId(userMsgDTO.getMessageInfo().getMessageId());
            // A fault payload containing all the attempts is added to the dto object
            retrieveErrors(dto);

            Action action = Action.fromString(userMsgDTO.getCollaborationInfo().getAction());
            FollowUpOperationHandler fuHandler = followUpOperationHandlerFactory.getOperationHandler(action);
            fuHandler.handleSendMessageFailed(dto);
        } catch (Exception e) {
            LOG.error("For Domibus message with id [{}] in failure encountered exception", messageId, e);
            throw e;
        }
        clearEtxLogKeysService.clearKeys();
    }

    private void retrieveErrors(ETrustExAdapterDTO dto) {

        List<MessageAttemptDTO> attemptsHistory = messageMonitorService.getAttemptsHistory(dto.getAs4MessageId());
        StringBuilder errors = new StringBuilder();
        for (MessageAttemptDTO messageAttemptDTO : attemptsHistory) {
            errors.append(messageAttemptDTO.toString()).append("|");
        }
        PayloadDTO etxPayloadDTO = new PayloadDTO(CONTENT_FAULT.getValue(), errors.toString().getBytes(), AS4MessageConstants.MIME_TYPE_TEXT_XML, false, PayloadDescription.ETRUSTEX_FAULT_RESPONSE_PAYLOAD.getValue());
        dto.addAs4Payload(etxPayloadDTO);
    }

    @Override
    public void messageReceiveFailed(MessageReceiveFailureEvent messageReceiveFailureEvent) {
        ErrorUtils.logMessageReceiveFailureEventString(messageReceiveFailureEvent, this.getClass().getName());
        throw new UnsupportedOperationException("Operation not available");
    }

}

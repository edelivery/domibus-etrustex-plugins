package eu.europa.ec.etrustex.node.outbox.client.adapters;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.adapters.Adapter;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.jms.TextMessageCreator;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.FaultDTO;
import eu.europa.ec.etrustex.common.util.ETrustExPluginAS4Properties;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.data.model.administration.EtxUser;
import eu.europa.ec.etrustex.node.service.security.IdentityManager;
import eu.europa.ec.etrustex.node.webservice.client.WSClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsOperations;

import static eu.europa.ec.etrustex.common.util.TransformerUtils.serializeObjToXML;

/**
 * Base class for all Outbox services adapters.
 *
 * @author Federico Martini
 * @version 1.0
 * @since 05 July 2017
 */
public abstract class ETrustExAdapterBase extends WSClient implements Adapter {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ETrustExAdapterBase.class);

    @Autowired
    @Qualifier("etxNodePluginBackendToNodeJmsTemplate")
    private JmsOperations jmsInbound;

    @Autowired
    private IdentityManager identityManager;

    @Autowired
    @Qualifier("etxNodePluginNodeToBackendJmsTemplate")
    JmsOperations jmsOperations;

    @Autowired
    @Qualifier("etxPluginAS4Properties")
    ETrustExPluginAS4Properties commonPluginProperties;

    /**
     * Get {@link EtxUser} linked to the {@link EtxParty} of a given partyUUID
     *
     * @param partyUuid of the {@link EtxParty}
     * @return {@link EtxUser} of the {@link EtxParty} of the given partyUUID or null if not found
     */
    EtxUser getUser(String partyUuid) {
        EtxParty etxParty = identityManager.searchPartyByUuid(partyUuid);
        if (etxParty == null) {
            throw new IllegalStateException("Etx user not found: " + partyUuid);
        }
        return etxParty.getNodeUser();
    }

    void handleFaultDto(ETrustExAdapterDTO etxDto, Exception ex) {
        LOG.warn("For AS4 message ID: [{}], handleFaultDto with conversationId: [{}]", etxDto.getAs4MessageId(), etxDto.getAs4ConversationId(), ex);
        if (etxDto.hasFaultPayload()) {
            // This triggers the JMS container retry mechanism
            throw new ETrustExPluginException(ETrustExError.DEFAULT, ex.getMessage(), ex);
        }
        // Adding the DTO to keep track of the exception
        etxDto.addAs4Payload(new FaultDTO(ETrustExError.DEFAULT, ex));
        // Substitutes the JMS message inside the queue
        jmsInbound.send(new TextMessageCreator(serializeObjToXML(etxDto)));
    }
}



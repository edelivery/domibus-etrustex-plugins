package eu.europa.ec.etrustex.node.data.dao;

import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;

/**
 * @author François GAUTIER
 * @version 1.0
 * @since 19/09/2017
 */
public interface PartyDao extends DaoBase<EtxParty> {

    /**
     * Find the {@link EtxParty} with a given uuid
     *
     * @param uuid of a party
     * @return EtxParty with given uuid or null if not found
     */
    EtxParty findByPartyUUID(String uuid);

}

package eu.europa.ec.etrustex.node.inbox.queue;

import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.exceptions.EtrustExMarker;
import eu.europa.ec.etrustex.node.inbox.queue.downloadattachment.InboxDownloadAttachmentQueueMessage;
import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxMessageTransmissionQueueMessage;

public interface NodeConsumer {

    default void handleMessage(InboxDownloadAttachmentQueueMessage message) {
        DomibusLoggerFactory.getLogger(NodeConsumer.class).error(EtrustExMarker.NON_RECOVERABLE,
                "queue consumer calling handleMessage InboxDownloadAttachmentQueueMessage");
    }

    default void handleMessage(InboxMessageTransmissionQueueMessage message) {
        DomibusLoggerFactory.getLogger(NodeConsumer.class).error(EtrustExMarker.NON_RECOVERABLE,
                "queue consumer calling handleMessage InboxMessageTransmissionQueueMessage");
    }
}

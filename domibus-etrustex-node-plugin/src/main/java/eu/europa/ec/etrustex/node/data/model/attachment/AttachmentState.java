package eu.europa.ec.etrustex.node.data.model.attachment;

/**
 * @author François GAUTIER
 * @version 1.0
 * @since 20/09/2017
 */
public enum AttachmentState {
    CREATED(false),

    UPLOADED(false),

    DOWNLOADED(false),

    SUBMITTED_TO_DOMIBUS(false),

    SENT_TO_BACKEND(false),

    PROCESSED_BY_BACKEND(true),

    FAILED(true);

    private boolean isFinal;

    AttachmentState(boolean isFinal) {
        this.isFinal = isFinal;
    }

    public boolean isFinal() {
        return isFinal;
    }
}

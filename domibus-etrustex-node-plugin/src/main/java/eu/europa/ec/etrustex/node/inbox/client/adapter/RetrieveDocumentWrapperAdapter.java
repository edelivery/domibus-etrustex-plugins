package eu.europa.ec.etrustex.node.inbox.client.adapter;

import ec.schema.xsd.commonaggregatecomponents_2.*;
import ec.schema.xsd.documentwrapperrequest_1.DocumentWrapperRequestType;
import ec.services.wsdl.documentwrapper_2.DocumentWrapperPortType;
import ec.services.wsdl.documentwrapper_2.FaultResponse;
import ec.services.wsdl.documentwrapper_2.RetrieveDocumentWrapperRequestRequest;
import ec.services.wsdl.documentwrapper_2.RetrieveDocumentWrapperRequestResponse;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.converters.node.DocumentReferenceRequestTypeBuilder;
import eu.europa.ec.etrustex.common.converters.node.NodePartyConverter;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.service.WorkspaceService;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.data.model.attachment.AttachmentState;
import eu.europa.ec.etrustex.node.data.model.attachment.ChecksumAlgorithmType;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.service.AttachmentService;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import javax.xml.ws.Holder;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Webservice client for RetrieveDocumentWrapper operation on Node exposed services
 * It's used by InboxServices
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 04/10/2017
 */
@Service("etxNodePluginRetrieveDocumentWrapperAdapter")
@Scope("prototype")
public class RetrieveDocumentWrapperAdapter extends NodeServicesAdapterBase {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(RetrieveDocumentWrapperAdapter.class);

    @Autowired
    private ETrustExNodePluginProperties eTrustExNodePluginProperties;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    @Qualifier("etxNodePluginWorkspaceService")
    private WorkspaceService workspaceService;

    @Override
    public String getServiceEndpoint() {
        return eTrustExNodePluginProperties.getDocumentWrapperServiceEndpoint();
    }

    /**
     * Calls Node retrieveDocumentWrapper operation, store attachment locally and update {@link EtxAttachment} entity in the DB
     *
     * @param etxAttachment attachment to be retrieved
     * @throws FaultResponse business Node fault
     */
    public void downloadAttachment(final EtxAttachment etxAttachment) throws FaultResponse {
        LOG.debug("downloadAttachment -> start");

        //extract message bundle, sender and receiver
        final EtxMessage etxMessage = etxAttachment.getMessage();
        final EtxParty sender = etxMessage.getSender();

        //we identity to the node with receiver credentials
        final EtxParty senderForNode = etxMessage.getReceiver();

        //fill in authorisation header
        Holder<HeaderType> authorisationHeader = fillAuthorisationHeader(senderForNode.getPartyUuid(), null);

        //prepare the webservice request
        final RetrieveDocumentWrapperRequestRequest retrieveDocumentWrapperRequestRequest = new RetrieveDocumentWrapperRequestRequest();
        final DocumentWrapperRequestType documentWrapperRequest = new DocumentWrapperRequestType();

        retrieveDocumentWrapperRequestRequest.setDocumentWrapperRequest(documentWrapperRequest);
        documentWrapperRequest.setSenderParty(NodePartyConverter.buildPartyType(sender.getPartyUuid()));

        final DocumentReferenceRequestType documentReferenceRequest = DocumentReferenceRequestTypeBuilder.
                buildDocumentReferenceRequest(etxAttachment.getAttachmentUuid(), etxAttachment.getAttachmentType());
        documentWrapperRequest.setDocumentReferenceRequest(documentReferenceRequest);

        //invoke the Node webservice
        DocumentWrapperPortType port = buildDocumentWrapperPort(senderForNode.getNodeUser().getName(), senderForNode.getNodeUser().getPassword());

        RetrieveDocumentWrapperRequestResponse response = port.retrieveDocumentWrapperRequest(retrieveDocumentWrapperRequestRequest, authorisationHeader);
        LOG.info("Node retrieveDocumentWrapper service called for attachment ID: [{}]", etxAttachment.getId());

        //update attachment
        updateAttachment(etxAttachment, response);
    }


    /**
     * Parse the response from node, extract the attachment and write the file on node plugin workspace
     *
     * @param etxAttachment                          attachment loaded from DB
     * @param retrieveDocumentWrapperRequestResponse Node response
     */
    void updateAttachment(final EtxAttachment etxAttachment, final RetrieveDocumentWrapperRequestResponse retrieveDocumentWrapperRequestResponse) {
        LOG.debug("updateAttachment -> start");
        if (retrieveDocumentWrapperRequestResponse != null && retrieveDocumentWrapperRequestResponse.getDocumentWrapper() != null) {

            final ResourceInformationReferenceType resourceInformationReferenceType = retrieveDocumentWrapperRequestResponse.getDocumentWrapper().getResourceInformationReference();
            final LargeAttachmentType largeAttachment = resourceInformationReferenceType.getLargeAttachment();
            final Base64BinaryType binaryType = largeAttachment.getStreamBase64Binary();

            //mime code
            etxAttachment.setMimeType(binaryType.getMimeCode());

            //checksum
            if (resourceInformationReferenceType.getDocumentHash() != null) {
                etxAttachment.setChecksum(DatatypeConverter.parseHexBinary(resourceInformationReferenceType.getDocumentHash().getValue()));
            }

            //algorithm type
            if (resourceInformationReferenceType.getDocumentHashMethod() != null) {
                etxAttachment.setChecksumAlgorithmType(ChecksumAlgorithmType.fromValue(resourceInformationReferenceType.getDocumentHashMethod().getValue()));
            }

            //document size and validation
            Long expectedDocumentSize = null;
            if (resourceInformationReferenceType.getDocumentSize() != null && resourceInformationReferenceType.getDocumentSize().getValue() != null) {
                expectedDocumentSize = resourceInformationReferenceType.getDocumentSize().getValue().longValue();
            }
            LOG.info("retrieveDocumentWrapper retrieved attachment [ID: {}] with expectedDocumentSize: {}", etxAttachment.getId(), expectedDocumentSize);

            //write the attachment file
            if (binaryType.getValue() != null) {
                try (InputStream inputStream = binaryType.getValue().getInputStream()){

                    final Path attachmentPath = workspaceService.writeFile(etxAttachment.getId(), inputStream);
                    LOG.debug("Input stream successfully closed");

                    Long actualDocumentSize = Files.size(attachmentPath);
                    if (expectedDocumentSize != null && ObjectUtils.notEqual(actualDocumentSize, expectedDocumentSize)) {
                        throw new ETrustExPluginException(ETrustExError.DEFAULT, "Download size mismatch. Actual size (" + actualDocumentSize + ") <> expected size (" + expectedDocumentSize + ")");
                    }
                    etxAttachment.setFilePath(attachmentPath.toAbsolutePath().toString());

                } catch (IOException ex) {
                    throw new ETrustExPluginException(ETrustExError.DEFAULT, ex.getMessage(), ex);
                }
            }

            //update attachment
            attachmentService.updateAttachmentState(etxAttachment, AttachmentState.CREATED, AttachmentState.DOWNLOADED);
        }
        LOG.debug("updateAttachment -> end");
    }


}

package eu.europa.ec.etrustex.node.service;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.service.CacheService;
import eu.europa.ec.etrustex.common.service.RuntimeManager;
import eu.europa.ec.etrustex.common.service.WorkspaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * @author Federico Martini
 * @version 1.0
 * @since 17/01/2018
 */
@Service("etxNodePluginRuntimeManager")
public class NodeRuntimeManagerImpl implements RuntimeManager {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(NodeRuntimeManagerImpl.class);

    @Autowired
    private CacheService cacheService;

    @Autowired
    @Qualifier("etxNodePluginWorkspaceService")
    private WorkspaceService workspaceService;


    /**
     * <ol>
     * <li>Clears all the ehCache entries</li>
     * <li>Reloads the workspace folder configured</li>
     * </ol>
     * <br/>
     */
    @Override
    public void resetCache() {
        LOG.info("Proceed with resetting all caches");
        cacheService.clearCache();
        workspaceService.loadConfiguration();
        LOG.info("All caches successfully reset");
    }
}

package eu.europa.ec.etrustex.node.inbox.queue.downloadattachment;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.Queue;

/**
 * DownloadAttachment queue producer for InboxServices
 * <p>
 * Converts the message to JSON format before sending as TEXT message to the queue
 * <p>
 * The JSON converter is defined in Spring xml configuration file
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 25/09/2017
 */
@Service("etxNodePluginInboxDownloadAttachmentProducer")
public class InboxDownloadAttachmentProducer {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxDownloadAttachmentProducer.class);

    private final Queue jmsQueue;

    private final JmsTemplate jmsTemplate;

    public InboxDownloadAttachmentProducer(@Qualifier("etxNodePluginInboxDownloadAttachmentQueue") Queue jmsQueue,
                                           @Qualifier("etxNodePluginInboxDownloadAttachmentJmsTemplate") JmsTemplate jmsTemplate) {
        this.jmsQueue = jmsQueue;
        this.jmsTemplate = jmsTemplate;
    }

    /**
     * send a TEXT (Json) message to dedicated queue {@code etxNodePluginInboxDownloadAttachmentQueue}
     *
     * @param messageId id of the message
     * @param attachmentId id of the attachment
     */
    public void triggerDownloadAttachment(final Long messageId, final Long attachmentId) {
        LOG.debug("triggerDownloadAttachment -> start");

        final InboxDownloadAttachmentQueueMessage queueMessage = new InboxDownloadAttachmentQueueMessage(MessageType.MESSAGE_BUNDLE,
                MessageDirection.NODE_TO_BACKEND, messageId, attachmentId);

        LOG.info("triggerDownloadAttachment -> put message:  {}",  queueMessage);

        jmsTemplate.convertAndSend(jmsQueue, queueMessage);
        LOG.debug("triggerDownloadAttachment -> end");

    }

    /**
     *  send a TEXT (Json) message to dedicated queue {@code etxNodePluginInboxDownloadAttachmentQueue}
     *
     * @param queueMessage queue message of type {@code InboxDownloadAttachmentQueueMessage}
     */
    void triggerDownloadAttachment(final InboxDownloadAttachmentQueueMessage queueMessage) {
        LOG.debug("triggerDownloadAttachment -> start");

        LOG.info("triggerDownloadAttachment -> put message:  {}",  queueMessage);

        jmsTemplate.convertAndSend(jmsQueue, queueMessage);
        LOG.debug("triggerDownloadAttachment -> end");
    }

}

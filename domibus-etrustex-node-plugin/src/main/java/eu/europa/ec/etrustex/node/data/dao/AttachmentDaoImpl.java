package eu.europa.ec.etrustex.node.data.dao;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 * @author François GAUTIER
 * @version 1.0
 * @since 19/09/2017
 */
@Repository("etxNodePluginAttachmentDaoImpl")
public class AttachmentDaoImpl extends DaoBaseImpl<EtxAttachment> implements AttachmentDao {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(AttachmentDaoImpl.class);

    public AttachmentDaoImpl() {
        super(EtxAttachment.class);
    }

    @Override
    public EtxAttachment findAttachmentByDomibusMessageId(String domibusMessageId) {
        TypedQuery<EtxAttachment> query = getEntityManager().createNamedQuery("findAttachmentByDomibusMessageIdExtNodePlugin", EtxAttachment.class);
        query.setParameter("domibusMessageId", domibusMessageId);
        try {
            return query.getSingleResult();
        } catch (NoResultException nrEx) {
            LOG.debug("Could not find any attachment related to Domibus message with id [{}]", domibusMessageId, nrEx);
            return null;
        }
    }

}

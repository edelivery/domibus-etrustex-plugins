package eu.europa.ec.etrustex.node.data.dao;

import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;

/**
 * @author François GAUTIER
 * @version 1.0
 * @since 19/09/2017
 */
public interface AttachmentDao extends DaoBase<EtxAttachment> {

    /**
     * Finds an EtxAttachment entity by the Domibus message id.
     *
     * @param domibusMessageId a Domibus message id
     * @return a persisted object {@link EtxAttachment} corresponding to the given Domibus message id, or null if not found
     */
    EtxAttachment findAttachmentByDomibusMessageId(String domibusMessageId);

}

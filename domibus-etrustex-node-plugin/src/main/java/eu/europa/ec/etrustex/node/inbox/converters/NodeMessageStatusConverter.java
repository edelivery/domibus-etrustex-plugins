package eu.europa.ec.etrustex.node.inbox.converters;


import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseRequest;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import oasis.names.specification.ubl.schema.xsd.applicationresponse_2.ApplicationResponseType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.DocumentResponseType;

import static eu.europa.ec.etrustex.common.util.TransformerUtils.serializeObjToXML;

/**
 * Converter utility class.
 * <p>
 * Node specific message type {@link SubmitApplicationResponseRequest} will be converted to {@link EtxMessage}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 13/10/2017
 */
public class NodeMessageStatusConverter {

    /**
     * private constructor as all the methods will be static
     */
    private NodeMessageStatusConverter() {
    }

    /**
     * Converts to plugin specific message object {@link EtxMessage}
     * from node {@link SubmitApplicationResponseRequest} object
     *
     * @param submitApplicationResponseRequest node ApplicationResponseRequest
     * @return plugin specific message bundle type {@link EtxMessage}
     */
    public static EtxMessage toEtxMessage(SubmitApplicationResponseRequest submitApplicationResponseRequest) {
        final EtxMessage etxMessageStatus = new EtxMessage();

        final ApplicationResponseType applicationResponse = submitApplicationResponseRequest.getApplicationResponse();
        if (applicationResponse == null) {
            return etxMessageStatus;
        }

        //message uuid
        if (applicationResponse.getID() != null) {
            etxMessageStatus.setMessageUuid(applicationResponse.getID().getValue());
        }

        //message ref uuid
        final DocumentResponseType documentResponse = applicationResponse.getDocumentResponse();
        if (documentResponse.getDocumentReference() != null && documentResponse.getDocumentReference().getID() != null) {
            etxMessageStatus.setMessageReferenceUuid(documentResponse.getDocumentReference().getID().getValue());
        }

        //message issue date
        if (applicationResponse.getIssueDate() != null) {
            etxMessageStatus.setIssueDateTime(applicationResponse.getIssueDate().getValue().toGregorianCalendar());
        }

        //message xml
        etxMessageStatus.setXml(serializeObjToXML(submitApplicationResponseRequest));

        return etxMessageStatus;
    }
}

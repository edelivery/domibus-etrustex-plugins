package eu.europa.ec.etrustex.node.outbox.client.adapters;

import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.schema.xsd.commonaggregatecomponents_2.InterchangeAgreementType;
import ec.services.wsdl.retrieveinterchangeagreementsrequest_2.FaultResponse;
import ec.services.wsdl.retrieveinterchangeagreementsrequest_2.RetrieveInterchangeAgreementsRequestPortType;
import ec.services.wsdl.retrieveinterchangeagreementsrequest_2.SubmitRetrieveInterchangeAgreementsRequestRequest;
import ec.services.wsdl.retrieveinterchangeagreementsrequest_2.SubmitRetrieveInterchangeAgreementsRequestResponse;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.jms.TextMessageCreator;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import eu.europa.ec.etrustex.node.data.dao.PartyDao;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.data.model.administration.EtxUser;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.jms.core.JmsOperations;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import java.util.List;

import static eu.europa.ec.etrustex.common.converters.PartyTypeConverter.getUuid;
import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_ID_GENERIC;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.*;


/**
 * This class is responsible of:
 * 1. adapting the DTO information in order to call a WS
 * 2. adapting the WS response to a new DTO
 * 3. send the outgoing DTO to a reply queue
 * <p>
 * It is used to communicate with the Retrieve Interchange Agreement Service
 *
 * @author Federico Martini
 * @version 1.0
 * @since 08/06/2017
 */
@Service("etxNodePluginRetrieveICAAdapter")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RetrieveICAAdapter extends ETrustExAdapterBase {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(RetrieveICAAdapter.class);
    @Autowired
    private ETrustExNodePluginProperties eTrustExNodePluginProperties;
    @Autowired
    @Qualifier("etxNodePluginNodeToBackendRICAJmsTemplate")
    private JmsOperations jmsOutbound;
    @Autowired
    private PartyDao partyDao;


    private RetrieveInterchangeAgreementsRequestPortType retrieveInterchangeAgreementsRequestPortType;

    @PostConstruct
    private void createPortType() {
        retrieveInterchangeAgreementsRequestPortType = getRetrieveInterchangeAgreementsRequestPortType();
    }

    @Override
    public String getServiceEndpoint() {
        return eTrustExNodePluginProperties.getRetrieveInterchangeAgreementServiceEndpoint();
    }

    @Override
    public void performOperation(ETrustExAdapterDTO etxDto) {

        LOG.info("AS4 Message Id [{}], performing operation [{}].", etxDto.getAs4MessageId(), etxDto.getAs4Action());

        HeaderType headerType = extractETrustExXMLPayload(ContentId.CONTENT_ID_HEADER, etxDto);
        if (headerType == null) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "Could not find the header payload for AS4 message [" + etxDto.getAs4MessageId() + "]");
        }

        Holder<HeaderType> authorizationHeaderHolder = new Holder<>();
        authorizationHeaderHolder.value = headerType;

        SubmitRetrieveInterchangeAgreementsRequestRequest request = extractETrustExXMLPayload(CONTENT_ID_GENERIC, etxDto);
        if (request == null) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "Could not find the generic payload for AS4 message [" + etxDto.getAs4MessageId() + "]");
        }


        try {
            EtxUser etxUser = getUser(etxDto.getAs4FromPartyId());
            setCredentials(etxUser.getName(), etxUser.getPassword(), (BindingProvider) retrieveInterchangeAgreementsRequestPortType);
            SubmitRetrieveInterchangeAgreementsRequestResponse response =
                    retrieveInterchangeAgreementsRequestPortType.submitRetrieveInterchangeAgreementsRequest(request, authorizationHeaderHolder);
            acknowledgeOk(etxDto, authorizationHeaderHolder.value, response);
        } catch (FaultResponse faultResponse) {
            acknowledgeFault(etxDto, faultResponse);
        } catch (Exception ex) {
            handleFaultDto(etxDto, ex);
        }
        LOG.info("AS4 Message Id [{}]. Operation [{}] performed.", etxDto.getAs4MessageId(), etxDto.getAs4Action());
    }

    public void acknowledgeOk(ETrustExAdapterDTO incomingEtxDto, HeaderType headerType, SubmitRetrieveInterchangeAgreementsRequestResponse response) {

        ETrustExAdapterDTO replyEtxDto = incomingEtxDto.invert();
        replyEtxDto.setAs4Action(commonPluginProperties.getAs4_Action_RetrieveICA_Response());
        replyEtxDto.addAs4Payload(TransformerUtils.getHeaderPayloadDTO(serializeObjToXML(headerType)));
        replyEtxDto.addAs4Payload(TransformerUtils.getGenericPayloadDTO(serializeObjToXML(response)));

        List<InterchangeAgreementType> icaList = response.getRetrieveInterchangeAgreementsResponse().getInterchangeAgreement();
        for (InterchangeAgreementType ica : icaList) {
            createPartyIfNotFound(getUuid(ica.getSenderParty()));
            createPartyIfNotFound(getUuid(ica.getReceiverParty()));
        }
        // Send it to the reply queue
        jmsOutbound.send(new TextMessageCreator(serializeObjToXML(replyEtxDto)));
        LOG.info("For AS4 message ID: [{}], Success Acknowledgement from Node posted to jms queue etxNodePluginOutboundRICAQueue.", replyEtxDto.getAs4RefToMessageId());
    }

    private void createPartyIfNotFound(String partyUuid) {
        if (StringUtils.isNotBlank(partyUuid)) {
            EtxParty receiver = partyDao.findByPartyUUID(partyUuid);

            if (receiver == null) {
                EtxParty instance = new EtxParty();
                instance.setPartyUuid(partyUuid);
                partyDao.save(instance);
            }
        }
    }

    public void acknowledgeFault(ETrustExAdapterDTO incomingEtxDto, FaultResponse faultResp) {
        StringBuilder descriptions = new StringBuilder();
        if (faultResp.getCause() != null) {
            LOG.error("Fault response Error cause: ", faultResp.getCause());
            descriptions.append(faultResp.getCause().getMessage()).append("\n");
            faultResp = new FaultResponse("Error with exception in Node", faultResp.getFaultInfo());
        }
        for (DescriptionType desType : faultResp.getFaultInfo().getDescription()) {
            descriptions.append("\n").append(desType.getValue());
        }
        LOG.warn("For AS4 message ID: [{}] Fault response received from Node with Code [{}] ; Descriptions:[{}].", incomingEtxDto.getAs4MessageId(), faultResp.getFaultInfo().getResponseCode().getValue(), descriptions);

        ETrustExAdapterDTO replyEtxDto = incomingEtxDto.invert();
        replyEtxDto.setAs4Action(commonPluginProperties.getAs4_Action_RetrieveICA_Response());
        replyEtxDto.addAs4Payload(getFaultResponsePayloadDTO(faultResp));

        // Send it to the reply queue
        jmsOutbound.send(new TextMessageCreator(serializeObjToXML(replyEtxDto)));
        LOG.info("For AS4 message ID: [{}] Fault response received from Node posted to jms queue etxNodePluginOutboundRICAQueue.", replyEtxDto.getAs4RefToMessageId());
    }
}

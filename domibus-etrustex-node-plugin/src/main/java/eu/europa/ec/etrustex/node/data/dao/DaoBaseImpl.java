package eu.europa.ec.etrustex.node.data.dao;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @param <T> the entity type which is handled by this DAO
 * @author François GAUTIER
 * @version 1.0
 * @since 19/09/2017
 * <p>
 * An abstract JPA-based DAO. Each DAO instance is bound to a specific Entity
 * type, which is managed by the given DAO.
 */
public abstract class DaoBaseImpl<T> implements DaoBase<T> {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DaoBaseImpl.class);

    private final String modelName;
    private final Class<T> entityClass;

    @PersistenceContext(unitName = "domibusJTA")
    private EntityManager entityManager;

    protected DaoBaseImpl(Class<T> entityClass) {
        this.modelName = entityClass.getAnnotation(Entity.class).name();
        this.entityClass = entityClass;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * eu.europa.ec.etrustex.adapter.persistence.DaoBaseLocal#findById(java.lang
     * .Long)
     */
    @Override
    public T findById(Long id) {
        LOG.debug("finding {} instance with ID: {}", modelName, id);
        return entityManager.find(getEntityClass(), id);
    }

    private Class<T> getEntityClass() {
        return entityClass;
    }

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see eu.europa.ec.etrustex.adapter.persistence.DaoBaseLocal#save(T)
     */
    @Override
    public void save(T instance) {
        LOG.debug("saving {} instance {}", modelName, instance);
        entityManager.persist(instance);
        LOG.debug("save successful");
    }

    /*
     * (non-Javadoc)
     *
     * @see eu.europa.ec.etrustex.adapter.persistence.DaoBaseLocal#update(T)
     */
    @Override
    public T update(T detachedInstance) {
        LOG.debug("updating {} detached instance", modelName);
        T result = entityManager.merge(detachedInstance);
        LOG.debug("update successful");
        return result;
    }

}

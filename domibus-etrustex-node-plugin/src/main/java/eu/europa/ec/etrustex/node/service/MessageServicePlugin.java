package eu.europa.ec.etrustex.node.service;


import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import org.springframework.transaction.annotation.Propagation;

/**
 * Interface which declares the operations needed for {@code EtxMessage} entities
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 21/09/2017
 */
public interface MessageServicePlugin {

    void createMessage(EtxMessage messageEntity);

    /**
     * Retrieves the EtxMessage entity corresponding to an ID (primary key).
     *
     * @param etxMessageId
     * @return {@link EtxMessage}
     * @see eu.europa.ec.etrustex.node.data.dao.DaoBase#findById(Long)
     */
    EtxMessage findMessageById(long etxMessageId);

    /**
     * Updates the EtxMessage entity
     *
     * @param etxMessage
     * @see eu.europa.ec.etrustex.node.data.dao.DaoBase#update(Object)
     */
    void update(EtxMessage etxMessage);

    /**
     * To retrieve the entry from ETX_MESSAGE with a matching Domibus message Id
     *
     * @param domibusMsgId
     * @return {@link EtxMessage}
     */
    EtxMessage findMessageByDomibusMessageId(String domibusMsgId);

    /**
     * Set the {@link EtxMessage} of a given id in {@link MessageState#FAILED} state and add an error with a given message
     * This method has {@link Propagation#REQUIRES_NEW}
     *
     * @param id           of a {@link EtxMessage}
     * @param errorMessage Message to be included in the Error object
     */
    void messageInError(Long id, String errorMessage);
}

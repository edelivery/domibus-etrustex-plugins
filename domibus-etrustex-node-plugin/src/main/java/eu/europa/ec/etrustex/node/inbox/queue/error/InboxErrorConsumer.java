package eu.europa.ec.etrustex.node.inbox.queue.error;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.node.domain.DomainNodePlugin;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import eu.europa.ec.etrustex.node.inbox.queue.NodeConsumer;
import eu.europa.ec.etrustex.node.inbox.queue.downloadattachment.InboxDownloadAttachmentQueueMessage;
import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxMessageTransmissionQueueMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Error queue consumer for InboxServices - it will store either {@code InboxMessageTransmissionQueueMessage}
 * or {@code InboxDownloadAttachmentQueueMessage} received from the other queues
 * <p>
 * Based on Spring {@code MessageListenerAdapter} implementation which send an already converted message of above type to {@code handleMessage} method
 * <p>
 * Conversion back from TEXT message/JSON is made by the the same {@code MessageConverter} defined in Spring config file and used by
 * {@code InboxMessageTransmissionProducer} or {@code InboxDownloadAttachmentProducer}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 27/09/2017
 */
@Service("etxNodePluginInboxErrorConsumer")
public class InboxErrorConsumer implements NodeConsumer {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxErrorConsumer.class);

    @Autowired
    private InboxErrorMessageTransmissionHandler inboxErrorMessageTransmissionHandler;
    @Autowired
    private InboxErrorDownloadHandler handleErrorDownload;

    @Autowired
    private ClearEtxLogKeysService clearEtxLogKeysService;

    @Autowired
    private DomainNodePluginService domainNodePluginService;

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    @DomainNodePlugin
    @ClearEtxLogKeys
    public void  handleMessage(InboxDownloadAttachmentQueueMessage message) {
        domainNodePluginService.setNodePluginDomain();
        LOG.info("InboxDownloadAttachmentQueueMessage message received on error queue ->  {}", message);
        handleErrorDownload.handleErrorWithNotification(message);
        clearEtxLogKeysService.clearKeys();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    @DomainNodePlugin
    @ClearEtxLogKeys
    public void  handleMessage(InboxMessageTransmissionQueueMessage message) {
        domainNodePluginService.setNodePluginDomain();
        LOG.info("InboxMessageTransmissionQueueMessage message received on error queue -> {}", message);
        switch (message.getMessageType()) {
            case MESSAGE_BUNDLE:
                inboxErrorMessageTransmissionHandler.handleErrorWithNotification(message);
                break;
            case MESSAGE_STATUS:
                inboxErrorMessageTransmissionHandler.handleErrorWithoutNotification(message);
                break;
            default:
                throw new IllegalArgumentException(message.getMessageType() + " not supported for error handling");
        }
        clearEtxLogKeysService.clearKeys();
    }
}

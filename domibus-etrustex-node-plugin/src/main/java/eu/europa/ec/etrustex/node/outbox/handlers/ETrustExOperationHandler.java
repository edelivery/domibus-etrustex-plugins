package eu.europa.ec.etrustex.node.outbox.handlers;

import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.jms.TextMessageCreator;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;

/**
 * @author Federico Martini
 * @since 24/03/2017
 * @version 1.0
 */
public interface ETrustExOperationHandler extends OperationHandler {

    TextMessageCreator buildJMSMessage(ETrustExAdapterDTO etxDto);
}

/**
 * This package will contains all the classes necessary for the outbox services.
 * Outbox services are initiated by back-end systems, through Domibus to eTrustEx node.
 *
 * @author Federico Martini
 * @version 1.0
 * @since 26/10/2017
 */
package eu.europa.ec.etrustex.node.outbox;
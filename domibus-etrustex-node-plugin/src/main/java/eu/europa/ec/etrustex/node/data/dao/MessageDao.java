package eu.europa.ec.etrustex.node.data.dao;

import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;

/**
 * @author François GAUTIER
 * @version 1.0
 * @since 19/09/2017
 */
public interface MessageDao extends DaoBase<EtxMessage> {

    /**
     * Retrieves an entity from ETX_MESSAGE which is matching the Domibus message id.
     *
     * @param domibusMessageId
     * @return a persisted object {@link EtxMessage} corresponding to the given Domibus message id, or null if not found
     */
    EtxMessage findMessageByDomibusMessageId(String domibusMessageId);

}

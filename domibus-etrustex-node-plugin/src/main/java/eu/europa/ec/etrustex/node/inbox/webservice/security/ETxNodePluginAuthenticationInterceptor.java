package eu.europa.ec.etrustex.node.inbox.webservice.security;

import eu.domibus.ext.services.AuthenticationExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * CXF interceptor for authenticating the User credentials.
 * <p>
 * Only Basic authentication has been implemented.
 * Authorization is simple - based on {@code ETX_USER.USR_NODE_SYS=1/0}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 28/09/2017
 */
@Component("etxNodePluginAuthenticationInterceptor")
public class ETxNodePluginAuthenticationInterceptor extends AbstractPhaseInterceptor<Message> {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ETxNodePluginAuthenticationInterceptor.class);

    @Autowired
    private AuthenticationExtService authenticationExtService;

    public ETxNodePluginAuthenticationInterceptor() {
        super(Phase.PRE_PROTOCOL);
    }

    @Override
    public void handleMessage(Message message) throws Fault {
        HttpServletRequest httpRequest = (HttpServletRequest) message.get(AbstractHTTPDestination.HTTP_REQUEST);
        LOG.info("Intercepted request for " + httpRequest.getPathInfo());

        authenticationExtService.authenticate(httpRequest);
    }

}
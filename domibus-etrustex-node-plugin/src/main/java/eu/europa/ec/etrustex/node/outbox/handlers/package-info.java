/**
 * This package contains all the classes necessary to handle an incoming DTO sent from the backend plugin through Domibus.
 * The DTO is converted in a text message and sent as a JMS message in the appropriate queue.
 * <p>
 * See {@link eu.europa.ec.etrustex.node.outbox.factory.ETrustExOperationHandlerFactory}
 *
 * @author Federico Martini
 * @version 1.0
 * @since 26/10/2017
 */
package eu.europa.ec.etrustex.node.outbox.handlers;
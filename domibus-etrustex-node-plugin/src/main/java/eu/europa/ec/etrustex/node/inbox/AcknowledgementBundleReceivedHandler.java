package eu.europa.ec.etrustex.node.inbox;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.Validate;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.inbox.queue.downloadattachment.InboxDownloadAttachmentProducer;
import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxBundleTransmissionNodeHandler;
import eu.europa.ec.etrustex.node.service.MessageServicePlugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Acknowledgement of MessageBundle reception from the backEnd.
 * <p>
 * In the Inbox flow, the BundleMessage recorded in {@link EtxMessage} is sent by the
 * {@link InboxBundleTransmissionNodeHandler} to the Back End.<br/>
 * Asynchronously, after being processed by the backend plugin, the acknowledge for the Bundle Message will be sent by
 * the BackendPlugin to the Node plugin. On receipt of the acknowledgement, the node plugin will initiate download of the
 * wrappers and transmission to the backend.<br/>
 * The Node plugin also checks on the receiver configuration whether the receiver party requires wrappers or not. <br/>
 *
 * @author François Gautier
 * @version 1.0
 * @since 04-Dec-17
 */
@Service("etxNodePluginAckHandler")
public class AcknowledgementBundleReceivedHandler implements OperationHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(AcknowledgementBundleReceivedHandler.class);
    @Autowired
    private InboxDownloadAttachmentProducer inboxDownloadAttachmentProducer;
    @Autowired
    private MessageServicePlugin messageService;

    @Override
    public boolean handle(ETrustExAdapterDTO etxDto) {

        EtxMessage messageBundle = messageService.findMessageByDomibusMessageId(etxDto.getAs4RefToMessageId());
        Validate.notNull(messageBundle, "EtxMessage not found for domibusMessageId = " + etxDto.getAs4RefToMessageId());
        EtxParty receiverParty = messageBundle.getReceiver();

        if (receiverParty != null && receiverParty.isRequiresWrappers() && messageBundle.hasAttachments()) {
            for (EtxAttachment etxAttachment : messageBundle.getAttachmentList()) {
                //trigger DownloadAttachment for each
                inboxDownloadAttachmentProducer.triggerDownloadAttachment(messageBundle.getId(), etxAttachment.getId());
            }
        } else {
            LOG.info("The receiver [" + etxDto.getAs4RefToMessageId() + "] was not found or does not requires wrappers.");
        }
        return true;
    }

}

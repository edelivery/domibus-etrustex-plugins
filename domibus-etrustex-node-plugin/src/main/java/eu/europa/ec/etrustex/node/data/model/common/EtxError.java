package eu.europa.ec.etrustex.node.data.model.common;

import eu.europa.ec.etrustex.node.data.model.AuditEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author François GAUTIER
 * @version 1.0
 * @since 20/09/2017
 */
@Entity(name = "EtxNodeError")
@AttributeOverrides({
        @AttributeOverride(name = "creationDate",
                column = @Column(name = "ERR_CREATED_ON", insertable = false, updatable = false, nullable = false)),
        @AttributeOverride(name = "modificationDate",
                column = @Column(name = "ERR_UPDATED_ON"))})
@Table(name = "ETX_ERROR")
public class EtxError extends AuditEntity<Long> implements Serializable {
    private static final long serialVersionUID = -3912973810107297366L;

    /** max length of the column ERR_DETAIL */
    public static final int ERROR_DETAIL_MAX_LENGTH = 2500;

    @Id
    @Column(name = "ERR_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ERR_RSP_CODE", nullable = false, updatable = false)
    private String responseCode;

    @Column(name = "ERR_DETAIL", length = ERROR_DETAIL_MAX_LENGTH, updatable = false)
    private String detail;

    /**
     * Default constructor
     */
    public EtxError() {
        // Default Constructor
    }

    public EtxError(String responseCode, String detail) {
        this.responseCode = responseCode;
        this.detail = detail;
    }

    public String getDetail() {
        return detail;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String errorType) {
        this.responseCode = errorType;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
                .append("id", id)
                .append("responseCode", responseCode)
                .append("detail", detail)
                .toString();
    }
}

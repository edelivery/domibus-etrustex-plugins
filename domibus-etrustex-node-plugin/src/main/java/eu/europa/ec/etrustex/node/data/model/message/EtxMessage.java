package eu.europa.ec.etrustex.node.data.model.message;

import eu.europa.ec.etrustex.node.data.model.AuditEntity;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import eu.europa.ec.etrustex.node.data.model.common.EtxError;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * @author François GAUTIER
 * @version 1.0
 * @since 20/09/2017
 */
@Entity(name = "EtxNodeMessage")
@AttributeOverrides({
        @AttributeOverride(name = "creationDate", column = @Column(name = "MSG_CREATED_ON", insertable = false, updatable = false, nullable = false)),
        @AttributeOverride(name = "modificationDate", column = @Column(name = "MSG_UPDATED_ON"))})
@NamedQueries({
        @NamedQuery(name = "findMessageByDomibusMessageIdExtNodePlugin",
                query = "SELECT msg                                     " +
                        "FROM EtxNodeMessage msg                            " +
                        "WHERE msg.domibusMessageId = :domibusMessageID ")
})
@Table(name = "ETX_MESSAGE")
public class EtxMessage extends AuditEntity<Long> implements Serializable {

    private static final long serialVersionUID = 3350328912640505380L;

    @Id
    @Column(name = "MSG_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "MSG_TYPE", nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private MessageType messageType;

    @Column(name = "MSG_UUID", length = 250, nullable = false, updatable = false)
    private String messageUuid;

    @Column(name = "MSG_STATE", nullable = false)
    @Enumerated(EnumType.STRING)
    private MessageState messageState;

    @Column(name = "MSG_DIRECTION", nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private MessageDirection directionType;

    @Column(name = "MSG_PARENT_UUID", length = 250, updatable = false)
    private String messageParentUuid;

    @Column(name = "MSG_REF_UUID", length = 250, updatable = false)
    private String messageReferenceUuid;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "MSG_ISSUE_DT", nullable = false, updatable = false)
    private Calendar issueDateTime;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "MSG_SENDER_PAR_ID", nullable = false, updatable = false)
    private EtxParty sender;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "MSG_RECEIVER_PAR_ID", nullable = false, updatable = false)
    private EtxParty receiver;

    @OneToMany(mappedBy = "message", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<EtxAttachment> attachmentList;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "ERR_ID", unique = true)
    private EtxError error;

    @Column(name = "DOMIBUS_MESSAGEID")
    private String domibusMessageId;

    @Column(name = "MSG_XML")
    @Lob
    private String xml;

    public List<EtxAttachment> getAttachmentList() {
        if (attachmentList == null) {
            attachmentList = new ArrayList<>();
        }

        return attachmentList;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public String getMessageUuid() {
        return messageUuid;
    }

    public void setMessageUuid(String messageUuid) {
        this.messageUuid = messageUuid;
    }

    public MessageState getMessageState() {
        return messageState;
    }

    public void setMessageState(MessageState messageState) {
        this.messageState = messageState;
    }

    public MessageDirection getDirectionType() {
        return directionType;
    }

    public void setDirectionType(MessageDirection directionType) {
        this.directionType = directionType;
    }

    public String getMessageParentUuid() {
        return messageParentUuid;
    }

    public void setMessageParentUuid(String messageParentUuid) {
        this.messageParentUuid = messageParentUuid;
    }

    public String getMessageReferenceUuid() {
        return messageReferenceUuid;
    }

    public void setMessageReferenceUuid(String messageReferenceUuid) {
        this.messageReferenceUuid = messageReferenceUuid;
    }

    public Calendar getIssueDateTime() {
        return issueDateTime;
    }

    public void setIssueDateTime(Calendar issueDateTime) {
        this.issueDateTime = issueDateTime;
    }

    public EtxParty getSender() {
        return sender;
    }

    public void setSender(EtxParty sender) {
        this.sender = sender;
    }

    public EtxParty getReceiver() {
        return receiver;
    }

    public void setReceiver(EtxParty receiver) {
        this.receiver = receiver;
    }

    public void setAttachmentList(List<EtxAttachment> attachmentList) {
        this.attachmentList = attachmentList;
    }

    public void addAttachment(EtxAttachment etxAttachment) {
        getAttachmentList().add(etxAttachment);
    }

    public EtxAttachment getAttachmentById(long etxAttachmentId) {
        for (EtxAttachment etxAttachment : getAttachmentList()) {
            if (etxAttachmentId == etxAttachment.getId()) {
                return etxAttachment;
            }
        }
        return null;
    }

    public boolean hasAttachments() {
        return CollectionUtils.isNotEmpty(this.getAttachmentList());
    }

    public EtxError getError() {
        return error;
    }

    public void setError(EtxError error) {
        this.error = error;
    }

    public String getDomibusMessageId() {
        return domibusMessageId;
    }

    public void setDomibusMessageId(String domibusMessageId) {
        this.domibusMessageId = domibusMessageId;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    @Override
    public String toString() {

        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", id)
                .append("messageType", messageType)
                .append("messageUuid", messageUuid)
                .append("messageState", messageState)
                .append("directionType", directionType)
                .append("messageParentUuid", messageParentUuid)
                .append("messageReferenceUuid", messageReferenceUuid)
                .append("issueDateTime", issueDateTime != null ? issueDateTime.getTime() : null)
                .append("sender", sender)
                .append("receiver", receiver)
                .append("domibusMessageId", domibusMessageId)
                .append("error", error)
                .append("attachmentList", attachmentList)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EtxMessage that = (EtxMessage) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (messageType != that.messageType) return false;
        if (messageUuid != null ? !messageUuid.equals(that.messageUuid) : that.messageUuid != null) return false;
        if (messageState != that.messageState) return false;
        if (directionType != that.directionType) return false;
        if (messageParentUuid != null ? !messageParentUuid.equals(that.messageParentUuid) : that.messageParentUuid != null)
            return false;
        if (messageReferenceUuid != null ? !messageReferenceUuid.equals(that.messageReferenceUuid) : that.messageReferenceUuid != null)
            return false;
        if (issueDateTime != null ? !issueDateTime.equals(that.issueDateTime) : that.issueDateTime != null)
            return false;
        if (sender != null ? !sender.equals(that.sender) : that.sender != null) return false;
        if (receiver != null ? !receiver.equals(that.receiver) : that.receiver != null) return false;
        if (attachmentList != null ? !attachmentList.equals(that.attachmentList) : that.attachmentList != null)
            return false;
        if (error != null ? !error.equals(that.error) : that.error != null) return false;
        return domibusMessageId != null ? domibusMessageId.equals(that.domibusMessageId) : that.domibusMessageId == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (messageType != null ? messageType.hashCode() : 0);
        result = 31 * result + (messageUuid != null ? messageUuid.hashCode() : 0);
        result = 31 * result + (messageState != null ? messageState.hashCode() : 0);
        result = 31 * result + (directionType != null ? directionType.hashCode() : 0);
        result = 31 * result + (messageParentUuid != null ? messageParentUuid.hashCode() : 0);
        result = 31 * result + (messageReferenceUuid != null ? messageReferenceUuid.hashCode() : 0);
        result = 31 * result + (issueDateTime != null ? issueDateTime.hashCode() : 0);
        result = 31 * result + (sender != null ? sender.hashCode() : 0);
        result = 31 * result + (receiver != null ? receiver.hashCode() : 0);
        result = 31 * result + (attachmentList != null ? attachmentList.hashCode() : 0);
        result = 31 * result + (error != null ? error.hashCode() : 0);
        result = 31 * result + (domibusMessageId != null ? domibusMessageId.hashCode() : 0);
        return result;
    }
}

package eu.europa.ec.etrustex.node.application;

import eu.europa.ec.etrustex.common.jobs.DomainInitializer;
import eu.europa.ec.etrustex.common.jobs.QuartzJobBean;
import eu.europa.ec.etrustex.node.domain.DomainInitializerNode;
import org.springframework.beans.factory.annotation.Autowired;

import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

public abstract class NodeQuartzJobBean extends QuartzJobBean {

    @Autowired
    protected ETrustExNodePluginProperties eTrustExNodePluginProperties;

    @Autowired
    private DomainInitializerNode domainInitializerNode;

    protected DomainInitializer getDomainInitializer() {
        return domainInitializerNode;
    }

    protected boolean isAllowedToRun(String domainName) {
        String domain = eTrustExNodePluginProperties.getDomain();
        return domain == null || equalsIgnoreCase(domain, domainName);
    }
}

package eu.europa.ec.etrustex.node.outbox.factory;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.handlers.Action;
import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.handlers.Service;
import eu.europa.ec.etrustex.node.inbox.AcknowledgementBundleReceivedHandler;
import eu.europa.ec.etrustex.node.outbox.handlers.RetrieveICAHandler;
import eu.europa.ec.etrustex.node.outbox.handlers.SendMessageStatusHandler;
import eu.europa.ec.etrustex.node.outbox.handlers.StoreDocumentWrapperHandler;
import eu.europa.ec.etrustex.node.outbox.handlers.SubmitDocumentBundleHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 * The ETrustExOperationHandlerFactory determines which is the right operation handler based on a service - action pair.
 * The flow of the request is from Domibus to Node.
 *
 * @author Federico Martini
 * @version 1.0
 * @since 23/03/2017
 */
@org.springframework.stereotype.Service("etxNodePluginETrustExOperationHandlerFactory")
public class ETrustExOperationHandlerFactory {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ETrustExOperationHandlerFactory.class);
    public static final String ERROR_LOG = "Service [{}] - Action[{}] combination not found.";
    public static final String COMBINATION_NOT_FOUND = "Service [{}] - Action[{}] combination not found.";

    @Autowired
    private ApplicationContext context;

    public OperationHandler getOperationHandler(Service service, Action action) {

        if (service == null) {
            throw new IllegalArgumentException("Service is missing");
        }
        if (action == null) {
            throw new IllegalArgumentException("Action is missing");
        }

        OperationHandler opeHandler = null;

        switch (service) {
            case BUNDLE_TRANSMISSION_SERVICE:
                switch (action) {
                    case NOTIFY_BUNDLE_TO_BACKEND_RESPONSE:
                        opeHandler = (AcknowledgementBundleReceivedHandler) context.getBean("etxNodePluginAckHandler");
                        break;
                    default:
                        LOG.error(ERROR_LOG, service, action);
                        break;
                }
                break;
            case APPLICATION_RESPONSE_SERVICE:
                switch (action) {
                    case SUBMIT_APPLICATION_RESPONSE_REQUEST:
                        // This will assure that Spring creates a new object at each call
                        opeHandler = (SendMessageStatusHandler) context.getBean("etxNodePluginSMSHandler");
                        break;
                    default:
                        LOG.error(ERROR_LOG, service, action);
                        break;
                }
                break;

            case DOCUMENT_BUNDLE_SERVICE:
                switch (action) {

                    case SUBMIT_DOCUMENT_BUNDLE_REQUEST:
                        // This will assure that Spring creates a new object at each call
                        opeHandler = (SubmitDocumentBundleHandler) context.getBean("etxNodePluginSDBHandler");
                        break;
                    default:
                        LOG.error(ERROR_LOG, service, action);
                        break;
                }
                break;

            case DOCUMENT_WRAPPER_SERVICE:
                switch (action) {
                    case STORE_DOCUMENT_WRAPPER_REQUEST:
                        // This will assure that Spring creates a new object at each call
                        opeHandler = (StoreDocumentWrapperHandler) context.getBean("etxNodePluginSDWHandler");
                        break;
                    default:
                        LOG.error(ERROR_LOG, service, action);
                        break;
                }
                break;


            case RETRIEVE_ICA_SERVICE:
                switch (action) {
                    case RETRIEVE_ICA_REQUEST:
                        // This will assure that Spring creates a new object at each call
                        opeHandler = (RetrieveICAHandler) context.getBean("etxNodePluginRICAHandler");
                        break;
                    default:
                        LOG.error(ERROR_LOG, service, action);
                        break;
                }
                break;

            default:
                LOG.error(ERROR_LOG, service, action);
                break;
        }

        return opeHandler;
    }

}

package eu.europa.ec.etrustex.node.application;

import eu.domibus.ext.domain.DomibusPropertyMetadataDTO;
import eu.domibus.ext.services.DomibusPropertyExtService;
import eu.domibus.ext.services.DomibusPropertyExtServiceDelegateAbstract;
import eu.domibus.ext.services.DomibusPropertyManagerExt;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author François Gautier
 * @version 1.1
 */
@Service("etxNodePluginProperties")
public class ETrustExNodePluginProperties extends DomibusPropertyExtServiceDelegateAbstract implements DomibusPropertyManagerExt {

    public static final String ARTIFACT_NAME = "Etx-NODE-Artifact-Name";
    public static final String ARTIFACT_VERSION = "Etx-NODE-Artifact-Version";
    public static final String BUILD_TIME = "Etx-NODE-Build-Time";
    public static final String DOCUMENT_BUNDLE_SERVICE_ENDPOINT = "etx.node.services.DocumentBundleService.endpoint";
    public static final String DOCUMENT_WRAPPER_SERVICE_ENDPOINT = "etx.node.services.DocumentWrapperService.endpoint";
    public static final String APPLICATION_RESPONSE_SERVICE_ENDPOINT = "etx.node.services.ApplicationResponseService.endpoint";
    public static final String RETRIEVE_INTERCHANGE_AGREEMENT_SERVICE_ENDPOINT = "etx.node.services.RetrieveInterchangeAgreementService.endpoint";
    public static final String NODE_CONNECTION_TIMEOUT = "etx.node.connectionTimeout";
    public static final String NODE_RECEIVE_TIMEOUT = "etx.node.receiveTimeout";
    public static final String USER = "etx.node.user";
    public static final String PWD = "etx.node.pwd";
    public static final String DOMAIN = "etx.node.domain";
    public static final String PLUGIN_PAYLOADS_DOWNLOAD_LOCATION = "etx.node.payloads.download.location";
    public static final String CLEAN_FILES_CRON = "etx.node.clean.files.cron";
    public static final String CLEAN_FILES_TTL = "etx.node.clean.files.ttl";
    public static final String CLEAN_FILES_SLEEP = "etx.node.clean.files.sleep.ms";
    public static final String PLUGIN_PAYLOADS_DELETE_TIMEOUT = "etx.node.payloads.delete.timeout";
    public static final String MODULE = "ETX_NODE_PLUGIN";
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ETrustExNodePluginProperties.class);

    private Map<String, DomibusPropertyMetadataDTO> knownProperties;

    @Autowired
    private DomibusPropertyExtService domibusPropertyExtService;


    public ETrustExNodePluginProperties() {
        List<DomibusPropertyMetadataDTO> allProperties = Arrays.asList(
                new DomibusPropertyMetadataDTO(ARTIFACT_NAME, DomibusPropertyMetadataDTO.Type.STRING, MODULE, DomibusPropertyMetadataDTO.Usage.GLOBAL, false),
                new DomibusPropertyMetadataDTO(ARTIFACT_VERSION, DomibusPropertyMetadataDTO.Type.STRING, MODULE, DomibusPropertyMetadataDTO.Usage.GLOBAL, false),
                new DomibusPropertyMetadataDTO(BUILD_TIME, DomibusPropertyMetadataDTO.Type.STRING, MODULE, DomibusPropertyMetadataDTO.Usage.GLOBAL, false),
                new DomibusPropertyMetadataDTO(DOCUMENT_BUNDLE_SERVICE_ENDPOINT, DomibusPropertyMetadataDTO.Type.STRING, MODULE, DomibusPropertyMetadataDTO.Usage.GLOBAL, false),
                new DomibusPropertyMetadataDTO(DOCUMENT_WRAPPER_SERVICE_ENDPOINT, DomibusPropertyMetadataDTO.Type.STRING, MODULE, DomibusPropertyMetadataDTO.Usage.GLOBAL, false),
                new DomibusPropertyMetadataDTO(APPLICATION_RESPONSE_SERVICE_ENDPOINT, DomibusPropertyMetadataDTO.Type.STRING, MODULE, DomibusPropertyMetadataDTO.Usage.GLOBAL, false),
                new DomibusPropertyMetadataDTO(RETRIEVE_INTERCHANGE_AGREEMENT_SERVICE_ENDPOINT, DomibusPropertyMetadataDTO.Type.STRING, MODULE, DomibusPropertyMetadataDTO.Usage.GLOBAL, false),
                new DomibusPropertyMetadataDTO(NODE_CONNECTION_TIMEOUT, DomibusPropertyMetadataDTO.Type.NUMERIC, MODULE, DomibusPropertyMetadataDTO.Usage.GLOBAL, true),
                new DomibusPropertyMetadataDTO(NODE_RECEIVE_TIMEOUT, DomibusPropertyMetadataDTO.Type.NUMERIC, MODULE, DomibusPropertyMetadataDTO.Usage.GLOBAL, true),
                new DomibusPropertyMetadataDTO(USER, DomibusPropertyMetadataDTO.Type.STRING, MODULE, DomibusPropertyMetadataDTO.Usage.GLOBAL, false),
                new DomibusPropertyMetadataDTO(PWD, DomibusPropertyMetadataDTO.Type.STRING, MODULE, DomibusPropertyMetadataDTO.Usage.GLOBAL, false),
                new DomibusPropertyMetadataDTO(DOMAIN, DomibusPropertyMetadataDTO.Type.STRING, MODULE, DomibusPropertyMetadataDTO.Usage.GLOBAL, false),
                new DomibusPropertyMetadataDTO(PLUGIN_PAYLOADS_DOWNLOAD_LOCATION, DomibusPropertyMetadataDTO.Type.STRING, MODULE, DomibusPropertyMetadataDTO.Usage.GLOBAL, false),
                new DomibusPropertyMetadataDTO(CLEAN_FILES_TTL, DomibusPropertyMetadataDTO.Type.NUMERIC, MODULE, DomibusPropertyMetadataDTO.Usage.GLOBAL, true),
                new DomibusPropertyMetadataDTO(CLEAN_FILES_SLEEP, DomibusPropertyMetadataDTO.Type.NUMERIC, MODULE, DomibusPropertyMetadataDTO.Usage.GLOBAL, true),
                new DomibusPropertyMetadataDTO(PLUGIN_PAYLOADS_DELETE_TIMEOUT, DomibusPropertyMetadataDTO.Type.NUMERIC, MODULE, DomibusPropertyMetadataDTO.Usage.GLOBAL, true)
        );
        knownProperties = allProperties.stream().collect(Collectors.toMap(DomibusPropertyMetadataDTO::getName, x -> x));
    }

    @PostConstruct
    public void init() {
        LOG.info("");
        LOG.info("########   #######  ##     ## #### ########  ##     ##  ######          ######## ######## ########  ##     ##  ######  ######## ######## ##     ##         ########  ##       ##     ##  ######   #### ##    ##  ");
        LOG.info("##     ## ##     ## ###   ###  ##  ##     ## ##     ## ##    ##         ##          ##    ##     ## ##     ## ##    ##    ##    ##        ##   ##          ##     ## ##       ##     ## ##    ##   ##  ###   ##  ");
        LOG.info("##     ## ##     ## #### ####  ##  ##     ## ##     ## ##               ##          ##    ##     ## ##     ## ##          ##    ##         ## ##           ##     ## ##       ##     ## ##         ##  ####  ##  ");
        LOG.info("##     ## ##     ## ## ### ##  ##  ########  ##     ##  ######  ####### ######      ##    ########  ##     ##  ######     ##    ######      ###    ####### ########  ##       ##     ## ##   ####  ##  ## ## ##  ");
        LOG.info("##     ## ##     ## ##     ##  ##  ##     ## ##     ##       ##         ##          ##    ##   ##   ##     ##       ##    ##    ##         ## ##           ##        ##       ##     ## ##    ##   ##  ##  ####  ");
        LOG.info("##     ## ##     ## ##     ##  ##  ##     ## ##     ## ##    ##         ##          ##    ##    ##  ##     ## ##    ##    ##    ##        ##   ##          ##        ##       ##     ## ##    ##   ##  ##   ###  ");
        LOG.info("########   #######  ##     ## #### ########   #######   ######          ########    ##    ##     ##  #######   ######     ##    ######## ##     ##         ##        ########  #######   ######   #### ##    ##  ");
        LOG.info("");
        LOG.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        LOG.info("+             " + getDisplayVersion() + "            +");
        LOG.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    }

    @Override
    public Map<String, DomibusPropertyMetadataDTO> getKnownProperties() {
        return knownProperties;
    }


    public String getDomain() {
        return domibusPropertyExtService.getProperty(DOMAIN);
    }

    public String getUser() {
        return domibusPropertyExtService.getProperty(USER);
    }

    public String getPwd() {
        return domibusPropertyExtService.getProperty(PWD);
    }

    public String getApplicationResponseServiceEndpoint() {
        return domibusPropertyExtService.getProperty(APPLICATION_RESPONSE_SERVICE_ENDPOINT);
    }

    public String getDocumentWrapperServiceEndpoint() {
        return domibusPropertyExtService.getProperty(DOCUMENT_WRAPPER_SERVICE_ENDPOINT);
    }

    public String getRetrieveInterchangeAgreementServiceEndpoint() {
        return domibusPropertyExtService.getProperty(RETRIEVE_INTERCHANGE_AGREEMENT_SERVICE_ENDPOINT);
    }

    public String getDocumentBundleServiceEndpoint() {
        return domibusPropertyExtService.getProperty(DOCUMENT_BUNDLE_SERVICE_ENDPOINT);
    }

    public String getDownloadLocation() {
        return domibusPropertyExtService.getProperty(PLUGIN_PAYLOADS_DOWNLOAD_LOCATION);
    }

    public long getCleanFilesTtl() {
        String ttl = domibusPropertyExtService.getProperty(CLEAN_FILES_TTL);
        if (StringUtils.isBlank(ttl)) {
            return 0;
        }
        return Long.parseLong(ttl);
    }

    public int getDeleteTimeout() {
        String resolvedProperty = domibusPropertyExtService.getProperty(PLUGIN_PAYLOADS_DELETE_TIMEOUT);
        if (StringUtils.isBlank(resolvedProperty)) {
            return 0;
        }
        return Integer.parseInt(resolvedProperty);
    }

    public int getSleepDeleteFile() {
        String ttl = domibusPropertyExtService.getProperty(CLEAN_FILES_SLEEP);
        if (StringUtils.isBlank(ttl)) {
            return 100;
        }
        return Integer.parseInt(ttl);
    }

    public int getNodeConnectionTimeout() {
        Integer integerProperty = domibusPropertyExtService.getIntegerProperty(NODE_CONNECTION_TIMEOUT);
        if(integerProperty == null) {
           return 30000;
        }
        return integerProperty;
    }

    public int getNodeReceiveTimeout() {
        Integer integerProperty = domibusPropertyExtService.getIntegerProperty(NODE_RECEIVE_TIMEOUT);
        if(integerProperty == null) {
            return 120000;
        }
        return integerProperty;
    }

    private String getDisplayVersion() {
        StringBuilder display = new StringBuilder();
        display.append(domibusPropertyExtService.getProperty(ARTIFACT_NAME));
        display.append(" Version [");
        display.append(domibusPropertyExtService.getProperty(ARTIFACT_VERSION));
        display.append("] Build-Time [");
        display.append(domibusPropertyExtService.getProperty(BUILD_TIME));
        display.append("]");
        return display.toString();
    }
}
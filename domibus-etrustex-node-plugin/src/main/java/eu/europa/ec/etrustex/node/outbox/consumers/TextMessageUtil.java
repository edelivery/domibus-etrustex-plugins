package eu.europa.ec.etrustex.node.outbox.consumers;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import eu.europa.ec.etrustex.common.util.Validate;
import org.apache.commons.lang3.StringUtils;

import javax.jms.JMSException;
import javax.jms.TextMessage;

/**
 * @author François Gautier
 * @version 1.0
 * @since 09/03/2018
 */
public class TextMessageUtil {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(TextMessageUtil.class);

    private TextMessageUtil() {
        //Hidden constructor
    }

    public static ETrustExAdapterDTO getETrustExAdapterDTO(TextMessage message) {
        ETrustExAdapterDTO dto;
        Validate.notNull(message, "Error getting the property from JMS message");
        try {
            String dtoAsXml = message.getText();
            if (StringUtils.isEmpty(dtoAsXml)) {
                throw new ETrustExPluginException(ETrustExError.DEFAULT, "Xml representing the DTO is empty!");
            }
            dto = (ETrustExAdapterDTO) TransformerUtils.deserializeXMLToObj(dtoAsXml);
        } catch (Exception ex) {
            try {
                LOG.error("Error getting the property from JMS message {}", message.getText(), ex);
            } catch (JMSException e) {
                LOG.error("Error getting the property from JMS message", ex);
            }
            throw new ETrustExPluginException(ETrustExError.DEFAULT, ex.getMessage());
        }
        return dto;
    }
}

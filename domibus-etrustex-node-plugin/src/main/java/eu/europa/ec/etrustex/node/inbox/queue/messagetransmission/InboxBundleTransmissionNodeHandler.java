package eu.europa.ec.etrustex.node.inbox.queue.messagetransmission;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.MessagingProcessingException;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.util.ConversationIdKey;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import eu.europa.ec.etrustex.node.inbox.FollowUpOperationHandler;
import eu.europa.ec.etrustex.node.service.NodeConnectorSubmissionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service to handle all operations related to transmission of Bundles {@link MessageDirection#NODE_TO_BACKEND}.
 * Wrapper transmission is handled in a separate class
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 01/12/2017
 */
@Service("etxNodePluginInboxBundleTransmissionHandler")
public class InboxBundleTransmissionNodeHandler extends InboxMessageTransmissionHandlerBase implements FollowUpOperationHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxBundleTransmissionNodeHandler.class);

    @Autowired
    private NodeConnectorSubmissionService nodeConnectorSubmissionService;

    /**
     * Handles submission of Bundle messages to Domibus for transmission to backend. Expected input is {@link InboxMessageTransmissionQueueMessage} with
     * id of table {@code ETX_MESSAGE} to retrieve instance of {@link EtxMessage} of type {@link MessageType#MESSAGE_BUNDLE} to be transmitted.<br/>
     * <p>
     * On successful submission the {@link EtxMessage} will be updated with the DomibusMessageId.<br/>
     * <p>
     * Note:
     * In case of Domibus throwing a {@link MessagingProcessingException} we receives a {@link ETrustExPluginException} having it as cause
     *
     * @param message Instance of {@link InboxMessageTransmissionQueueMessage}
     * @see NodeConnectorSubmissionService#submitToDomibus(ETrustExAdapterDTO) for more details
     * @see InboxMessageTransmissionHandlerBase#handleError(InboxMessageTransmissionQueueMessage, Exception) for exception handling
     */
    @Override
    public void handleMessageTransmissionToBackend(InboxMessageTransmissionQueueMessage message) {

        try {
            final Long etxMessageId = message.getMessageId();
            LOG.debug("At handleMessageTransmissionToBackend for Bundle with ID:[{}] - start", etxMessageId);

            EtxMessage etxMessage = loadValidateBundleForTransmission(etxMessageId);

            //build the DTO
            ETrustExAdapterDTO eTrustExAdapterDTO = populateETXAdapterDTOForSendingBundleToBackend(etxMessage);

            final String domibusMsgId = nodeConnectorSubmissionService.submitToDomibus(eTrustExAdapterDTO);

            handleDomibusSubmissionSuccess(etxMessage, domibusMsgId);
        } catch (Exception exp) {
            handleError(message, exp);
        }
    }


    /**
     * Retrieve the instance of {@link EtxMessage} identified by the input and validates if it is a {@link MessageType#MESSAGE_BUNDLE} ready for transmission to backend.<br/>
     *
     * @param etxMessageId Primary key of table {@code}ETX_MESSAGE{@code}. Mandatory
     * @return instance of {@link EtxMessage}
     * @see InboxMessageTransmissionHandlerBase#loadValidateEtxMessageForTransmission(Long)
     */
    EtxMessage loadValidateBundleForTransmission(Long etxMessageId) {
        EtxMessage etxMessage = loadValidateEtxMessageForTransmission(etxMessageId);
        if (etxMessage.getMessageType() != MessageType.MESSAGE_BUNDLE) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "Message ID:[" + etxMessageId + "] specified for Inbox Message transmission to backend is not a Bundle.");
        }
        return etxMessage;
    }

    /**
     * To populate the AS4 metadata for transmitting the Bundle to the Backend.<br/>
     * For Bundle message, the request xml notified by Node will be sent as payload content id - {@link eu.europa.ec.etrustex.common.util.ContentId#CONTENT_ID_GENERIC}.<br/>
     *
     * @param etxMessage used to create the {@link ETrustExAdapterDTO}
     * @return {@link ETrustExAdapterDTO} for sending to Domibus
     */
    ETrustExAdapterDTO populateETXAdapterDTOForSendingBundleToBackend(EtxMessage etxMessage) {
        final ETrustExAdapterDTO eTrustExAdapterDTO = createEtxAdapterDTOForInboxTransmission(etxMessage);

        //Collaboration Info
        eTrustExAdapterDTO.setAs4Service(commonProperties.getAs4ServiceInboxBundleTransmission());
        eTrustExAdapterDTO.setAs4ServiceType(commonProperties.getAs4ServiceInboxBundleTransmissionServiceType());
        eTrustExAdapterDTO.setAs4Action(commonProperties.getAs4ActionInboxBundleTransmissionRequest());

        //payloads
        PayloadDTO bundleMsgPayloadDTO = TransformerUtils.getGenericPayloadDTO(etxMessage.getXml());
        eTrustExAdapterDTO.addAs4Payload(bundleMsgPayloadDTO);

        final String as4ConversationID = ConversationIdKey.INBOX_MESSAGEBUNDLE.getConversationIdKey() + etxMessage.getMessageUuid();
        eTrustExAdapterDTO.setAs4ConversationId(as4ConversationID);
        return eTrustExAdapterDTO;
    }


    /**
     * This method will be called when the Domibus of Node plugin is able to successfully send the bundle to the backend.<br/>
     * Using the domibus message id, the {@link EtxMessage} of the {@link MessageType#MESSAGE_BUNDLE} will be retrieved and status will be updated as {@link MessageState#SENT_TO_BACKEND}.<br/>
     *
     * @param dto the object of type {@link ETrustExAdapterDTO} coming from the connector
     */
    @Override
    public void handleSendMessageSuccess(ETrustExAdapterDTO dto) {

        final String domibusMessageId = StringUtils.trim(dto.getAs4MessageId());
        if (StringUtils.isBlank(domibusMessageId)) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "The Domibus message id required to retrieve the message bundle has not been set");
        } else {

            //checks if a message bundle is found in DB
            EtxMessage etxMsg = messageService.findMessageByDomibusMessageId(domibusMessageId);
            if (etxMsg != null) {
                etxMsg.setMessageState(MessageState.SENT_TO_BACKEND);
                messageService.update(etxMsg);
            } else {
                throw new ETrustExPluginException(ETrustExError.DEFAULT, "For Domibus message with id [" + domibusMessageId + "] no message bundle was found");
            }
        }
    }


    /**
     * Gets an {@link InboxMessageTransmissionQueueMessage} object and sends it to the error queue.
     *
     * @param dto the object of type {@link ETrustExAdapterDTO} coming from the connector
     */
    @Override
    public void handleSendMessageFailed(ETrustExAdapterDTO dto) {
        InboxMessageTransmissionQueueMessage queueMessage = super.createMsgSendFailedQueueMessage(dto, MessageType.MESSAGE_BUNDLE);

        // Relying on the Inbox error mechanism to handle the fault and notify the Node of the unsuccessful send
        inboxErrorProducer.triggerError(queueMessage);
    }

}

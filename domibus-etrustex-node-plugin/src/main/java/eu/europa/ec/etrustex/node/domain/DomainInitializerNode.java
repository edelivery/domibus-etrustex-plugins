package eu.europa.ec.etrustex.node.domain;

import eu.europa.ec.etrustex.common.jobs.DomainInitializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DomainInitializerNode implements DomainInitializer {

    @Autowired
    private DomainNodePluginService domainNodePluginService;

    @Override
    @DomainNodePlugin
    public void init(){
        domainNodePluginService.setNodePluginDomain();
    }
}

package eu.europa.ec.etrustex.node.web;

import eu.europa.ec.etrustex.node.service.NodeRuntimeManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Controller to expose an URL for administration activities related to backend plugin.
 *
 * @author Arun Raj
 * @version 1.0
 * @since 18/07/2017
 */

@Controller
@EnableWebMvc
public class ResetNodePluginCacheController {

    @Autowired
    protected NodeRuntimeManagerImpl runtimeManager;

    /**
     * Reload the cache of backend plugin
     * @return String
     */
    @GetMapping("/resetCacheNode")
    @ResponseBody
    public String handleRequest(HttpServletResponse response) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null &&
                authentication.getAuthorities()
                        .stream()
                        .noneMatch(grantedAuthority -> grantedAuthority.getAuthority().contains("ROLE_ANONYMOUS"))) {
            runtimeManager.resetCache();
            return "Cache reset.";
        }
        try {
            response.sendRedirect("login?returnUrl=%2FresetCache");
            return "REDIRECT TO LOGIN";
        } catch (IOException e) {
            throw new IllegalStateException("Could not redirect to login", e);
        }
    }
}

package eu.europa.ec.etrustex.node.inbox.queue.messagetransmission;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.MessagingProcessingException;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.util.ETrustExPluginAS4Properties;
import eu.europa.ec.etrustex.node.data.model.common.EtxError;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import eu.europa.ec.etrustex.node.inbox.queue.error.InboxErrorConsumer;
import eu.europa.ec.etrustex.node.inbox.queue.error.InboxErrorProducer;
import eu.europa.ec.etrustex.node.service.MessageServicePlugin;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_FAULT;
import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_MESSAGE_UUID;

/**
 * Base (abstract) class for Inbox Messages (Bundle and Status) to backend
 *
 * @author Arun Raj
 * @version 1.0
 * @since 31/10/2017
 */
public abstract class InboxMessageTransmissionHandlerBase {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxMessageTransmissionHandlerBase.class);

    @Autowired
    @Qualifier("etxPluginAS4Properties")
    ETrustExPluginAS4Properties commonProperties;

    @Autowired
    MessageServicePlugin messageService;

    @Autowired
    InboxErrorProducer inboxErrorProducer;

    @Autowired
    InboxMessageTransmissionProducer inboxMessageTransmissionProducer;

    public abstract void handleMessageTransmissionToBackend(InboxMessageTransmissionQueueMessage message);


    /**
     * <p><b>To handle exceptions in Inbox Message Transmission to backend.</b></p>
     * <p>
     * <p><b>For exceptions of type {@link MessagingProcessingException} that may be raised during Domibus Submission:</b>
     * These are validation failures during Domibus submission. Generally these occur due to pMode conflicts and retry would result in same error.
     * Hence trigger is raised to {@code}InboxErrorQueue{@code}. In future, retry may be considered if required.<br/>
     * </p>
     * <p>
     * <p><b>Other exceptions:</b> e.g: {@link ETrustExPluginException} - may or may not be thrown due to temporary issues and are considered for retry.<br/>
     * If the errors are thrown for the first time, the {@link InboxMessageTransmissionQueueMessage} will not have error details populated. Hence on the first attempt,
     * the error details are populated to the queue message to be made available in {@link InboxErrorConsumer} for error logging in DB.<br/>
     * On subsequent retries, since the error details are already available, the error is thrown back for transaction rollback.<br/>
     * </p>
     *
     * @param message Instance of {@link InboxMessageTransmissionQueueMessage} - Mandatory
     * @param ex      Exception encountered - Mandatory
     */
    void handleError(InboxMessageTransmissionQueueMessage message, Exception ex) {
        LOG.error("Encountered exception at Inbox message transmission to Backend ", ex);

        if (ex.getCause() instanceof MessagingProcessingException) {
            MessagingProcessingException mpEx = (MessagingProcessingException) ex.getCause();
            message.setErrorCode(mpEx.getEbms3ErrorCode() != null ? mpEx.getEbms3ErrorCode().getErrorCodeName() :
                    ETrustExError.DEFAULT.getCode());
            message.setErrorDescription(mpEx.getMessage());
            inboxErrorProducer.triggerError(message);
        } else {
            if (StringUtils.isBlank(message.getErrorCode()) && StringUtils.isBlank(message.getErrorDescription())) {
                //fill in error details and put message back to inbox message transmission queue.
                if (ex instanceof ETrustExPluginException) {
                    ETrustExPluginException etxExp = (ETrustExPluginException) ex;
                    message.setErrorCode(etxExp.getError().getCode());
                    message.setErrorDescription(etxExp.getMessage());
                } else {
                    message.setErrorCode(ETrustExError.DEFAULT.getCode());
                    message.setErrorDescription(ex.getMessage());
                }
                inboxMessageTransmissionProducer.triggerMessageWithError(message);
            } else {
                //this will trigger rollback and retry mechanism
                throw new ETrustExPluginException(ETrustExError.DEFAULT, ex.getMessage(), ex);
            }
        }
    }

    /**
     * Retrieve the instance of {@link EtxMessage} identified by the input and validates if it is ready for transmission to backend.<br/>
     * Main validations are that the {@link EtxMessage} is known and the message is not in status {@link MessageState#FAILED}
     *
     * @param etxMessageId Primary key of table {@code ETX_MESSAGE}. Mandatory
     * @return message from DB of type {@link EtxMessage}
     */
    EtxMessage loadValidateEtxMessageForTransmission(Long etxMessageId) {
        EtxMessage etxMessage = messageService.findMessageById(etxMessageId);
        if (etxMessage == null) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "Message with id[" + etxMessageId + "] specified for Inbox Message transmission to backend is not known."); //NOSONAR
        }
        if (etxMessage.getMessageState() == MessageState.FAILED) {
            throw new ETrustExPluginException(ETrustExError.ETX_009, "Message with id[" + etxMessageId + "] intended for Inbox Message transmission is already in FAILED state."); //NOSONAR
        }
        if (etxMessage.getDirectionType() != MessageDirection.NODE_TO_BACKEND) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "Message with id[" + etxMessageId + "] specified for Inbox Message transmission to backend is not intended for " + MessageDirection.NODE_TO_BACKEND); //NOSONAR
        }
        LOG.putMDC(MDC_ETX_MESSAGE_UUID, etxMessage.getMessageUuid());
        return etxMessage;
    }

    /**
     * On successful sumbission of message to Domibus for the first time, the Domibus message id is added and the message status is updated as {@link MessageState#SUBMITTED_TO_DOMIBUS}.<br/>
     *
     * @param etxMessage   Instance of {@link EtxMessage} to be updated. Mandatory
     * @param domibusMsgId Domibus message id
     */
    void handleDomibusSubmissionSuccess(EtxMessage etxMessage, String domibusMsgId) {
        if (etxMessage == null) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "EtxMessage object should not be null on successful submission to Domibus!");
        }

        if (StringUtils.isBlank(etxMessage.getDomibusMessageId())) {
            etxMessage.setDomibusMessageId(domibusMsgId);
            etxMessage.setMessageState(MessageState.SUBMITTED_TO_DOMIBUS);
        }
        messageService.update(etxMessage);
    }

    /**
     * Create an {@link ETrustExAdapterDTO} with common AS4 metadata needed for transmission from Node To Backend for Inbox transmission.
     * Common AS4 metadata are:
     * <ul>
     * <li>From Party Id = eTrustEx Node</li>
     * <li>From Party Id Type</li>
     * <li>From Party Role</li>
     * <li>To Party Id = Receiver Party of eTrustEx message</li>
     * <li>To Party Id Type</li>
     * <li>To Party Role</li>
     * <li>Original sender = Sender party of eTrustEx message</li>
     * <li>Original receiver = Receiver Party of eTrustEx message</li>
     * </ul>
     *
     * @param etxMessage Instance of {@link EtxMessage} to be transmitted to backend. Mandatory
     * @return adapter dto of type {@link ETrustExAdapterDTO}
     */
    ETrustExAdapterDTO createEtxAdapterDTOForInboxTransmission(EtxMessage etxMessage) {
        ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();

        //From Party
        eTrustExAdapterDTO.setAs4FromPartyId(commonProperties.getAs4ETrustExNodePartyId());
        eTrustExAdapterDTO.setAs4FromPartyIdType(commonProperties.getAs4FromPartyIdType());
        eTrustExAdapterDTO.setAs4FromPartyRole(commonProperties.getAs4FromPartyRole());

        //To Party
        eTrustExAdapterDTO.setAs4ToPartyId(etxMessage.getReceiver().getPartyUuid());
        eTrustExAdapterDTO.setAs4ToPartyIdType(commonProperties.getAs4ToPartyIdType());
        eTrustExAdapterDTO.setAs4ToPartyRole(commonProperties.getAs4ToPartyRole());

        //Message Properties
        eTrustExAdapterDTO.setAs4OriginalSender(etxMessage.getSender().getPartyUuid());
        eTrustExAdapterDTO.setAs4FinalRecipient(etxMessage.getReceiver().getPartyUuid());

        return eTrustExAdapterDTO;
    }

    /**
     * Searches for the persisted eTrustEx message corresponding to a Domibus message id.
     * Builds and populates an {@link InboxMessageTransmissionQueueMessage} object with the appropriate data.
     * <p>
     * Note: <b>This should be used only for {@link MessageType#MESSAGE_BUNDLE} and {@link MessageType#MESSAGE_STATUS} and NOT for attachments<b/>
     *
     * @param dto         the object of type {@link ETrustExAdapterDTO} coming from the connector
     * @param messageType the type of JMS message {@link MessageType}
     * @return a JMS message of type {@link InboxMessageTransmissionQueueMessage}
     */
    InboxMessageTransmissionQueueMessage createMsgSendFailedQueueMessage(ETrustExAdapterDTO dto, MessageType messageType) {
        EtxMessage etxMsg = messageService.findMessageByDomibusMessageId(dto.getAs4MessageId());
        if (etxMsg == null) {
            // It should never happen and this is blocking
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "The eTrustEx message related to Domibus message with id [" + dto.getAs4MessageId() + "] was not found");
        }
        PayloadDTO payload = dto.getPayloadFromCID(CONTENT_FAULT);
        if (payload == null) {
            // It should never happen and this is blocking
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "Fault payload was not found");
        }
        // Queue Message to return
        InboxMessageTransmissionQueueMessage queueMessage = new InboxMessageTransmissionQueueMessage(
                messageType,
                MessageDirection.NODE_TO_BACKEND,
                etxMsg.getId());

        queueMessage.setErrorCode(ETrustExError.DEFAULT.getCode());
        queueMessage.setErrorDescription(StringUtils.abbreviate(payload.getPayloadAsString(), EtxError.ERROR_DETAIL_MAX_LENGTH));
        return queueMessage;
    }

}

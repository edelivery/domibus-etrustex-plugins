package eu.europa.ec.etrustex.node.data.model.common;

/**
 * @author François GAUTIER
 * @version 1.0
 * @since 20/09/2017
 */
public enum FaultResponseCode {
    ERROR_DUPLICATE("error.duplicate", "Duplicate message ID for synchronous and  asynchronous operation / Party entity already exists"),
    ERROR_ID("error.id", "Hard business rule violation for synchronous and asynchronous operation (message ID related) / Message ID element not present in request"),
    ERROR_XSD("error.xsd", "XSD violation for synchronous operation"),
    ERROR_HARDRULE("error.hardrule", "Hard business rule violation for synchronous operation"),
    ERROR_BINARY("error.binary", "No binary data for Wrapper / Zero sized binary data for Wrapper / No binary data for AttachedDocument / Zero sized binary data for AttachedDocument"),
    ERROR_NOTEXISTENT("error.notexisting", "Document Wrapper cannot be deleted because it does not exist"),
    ERROR_LINKED("error.linked", "Document Wrapper cannot be deleted because it is linked to a Bundle"),

    MESSAGE_UNDEFINED("message.undefined", "Unknown document as direct child of operation element / Not exactly 1 document as direct child of operation element"),

    SLA_BINARY_SIZE("sla.binary_size", "Binary part too large for Wrapper with chunking (under max post size)"),
    SLA_XML_SIZE("sla.xml_size", "XML part too large"),
    SLA_VOLUME("sla.volume", "Volume exceeded (e.g. exceeded limit for this month across all Wrappers/Bundles)");

    private final String code;
    private final String description;

    FaultResponseCode(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public static FaultResponseCode fromCode(String code) {
        for (FaultResponseCode responseCode : values()) {
            if (responseCode.getCode().equals(code)) {
                return responseCode;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}

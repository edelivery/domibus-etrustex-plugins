package eu.europa.ec.etrustex.node.webservice.client;

import ec.services.wsdl.applicationresponse_2.ApplicationResponsePortType;
import ec.services.wsdl.documentbundle_2.DocumentBundlePortType;
import ec.services.wsdl.documentwrapper_2.DocumentWrapperPortType;
import ec.services.wsdl.inboxrequest_2.InboxRequestPortType;
import ec.services.wsdl.retrieveinterchangeagreementsrequest_2.RetrieveInterchangeAgreementsRequestPortType;
import ec.services.wsdl.retrieverequest_2.RetrieveRequestPortType;
import eu.europa.ec.etrustex.common.ws.CxfWebServiceProvider;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * The bean will be instantiated on startup by bean factory that perform eager initialization of singletons.
 */
@Service("etxNodePluginWebServiceProvider")
@Lazy(value = false)
public class NodeWebServiceProvider extends CxfWebServiceProvider {

    @Autowired
    @Qualifier("etxNodePluginProperties")
    private ETrustExNodePluginProperties nodeProperties;

    @PostConstruct
    private void init() {

        // preload all Node services as first time, it takes much longer than the further consequent loads
        buildJaxWsProxy(ApplicationResponsePortType.class, false, null);
        buildJaxWsProxy(DocumentBundlePortType.class, false, null);
        buildJaxWsProxy(DocumentWrapperPortType.class, true, null);
        buildJaxWsProxy(InboxRequestPortType.class, false, null);
        buildJaxWsProxy(RetrieveInterchangeAgreementsRequestPortType.class, false, null);
        buildJaxWsProxy(RetrieveRequestPortType.class, false, null);
    }

    @Override
    public Object buildJaxWsProxy(Class portType, boolean isMtomEnabled, String serviceEndpoint) {


        Object proxyObject = super.buildJaxWsProxy(portType, isMtomEnabled, serviceEndpoint);

        Client wsClient = ClientProxy.getClient(proxyObject);
        setTimeouts(wsClient);

        return proxyObject;
    }

    private void setTimeouts(Client endpointClient) {

        Long connectionTimeout = Long.valueOf(nodeProperties.getNodeConnectionTimeout());
        Long receiveTimeout = Long.valueOf(nodeProperties.getNodeReceiveTimeout());

        HTTPClientPolicy policy = getHttpClientPolicy(endpointClient);
        policy.setConnectionTimeout(connectionTimeout);
        policy.setReceiveTimeout(receiveTimeout);
    }
}

package eu.europa.ec.etrustex.node.data.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

/**
 * An audited and versioned entity.
 *
 * @author François GAUTIER
 * @version 1.0
 * @since 20/09/2017
 */
@MappedSuperclass
public abstract class AuditEntity<T extends Serializable> extends AbstractEntity<T> {

    @Temporal(TemporalType.TIMESTAMP)
    private Calendar creationDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Calendar modificationDate;

    public Calendar getCreationDate() {
        return this.creationDate;
    }

    public Calendar getModificationDate() {
        return this.modificationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    public void setModificationDate(Calendar modificationDate) {
        this.modificationDate = modificationDate;
    }

    @PrePersist
    protected void onCreate() {
        creationDate = modificationDate = getGMTCalendar();
    }

    @PreUpdate
    protected void onUpdate() {
        modificationDate = getGMTCalendar();
    }

    // MySQL doesn't support datetime in ISO8601 (only yyyy-MM-dd HH:mm:ss, instead of yyyy-MM-dd'T'HH:mm:ssZ),
    // create a calendar with no timezone info (default GMT)
    public static Calendar getGMTCalendar() {
        Calendar calendar = Calendar.getInstance();
        Calendar gregorianCalendar = new GregorianCalendar(calendar.get(GregorianCalendar.YEAR), calendar.get(GregorianCalendar.MONTH), calendar.get(GregorianCalendar.DAY_OF_MONTH),
                calendar.get(GregorianCalendar.HOUR_OF_DAY), calendar.get(GregorianCalendar.MINUTE), calendar.get(GregorianCalendar.SECOND));
        return gregorianCalendar;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .append("creationDate", creationDate)
                .append("modificationDate", modificationDate)
                .toString();
    }
}

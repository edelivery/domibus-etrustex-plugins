package eu.europa.ec.etrustex.node.inbox.client.adapter;

import eu.europa.ec.etrustex.common.util.CalendarHelper;
import oasis.names.specification.ubl.schema.xsd.applicationresponse_2.ApplicationResponseType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.DocumentReferenceType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.DocumentResponseType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.ResponseType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.*;

import java.util.Calendar;

/**
 * Utility class to build ApplicationResponseType instances
 *
 * @author François Gautier
 * @version 1.0
 * @since 14-Nov-17
 */
public class ApplicationResponseTypeBuilder {

    private String messageUuid;
    private Calendar issueDateTime;
    private String messageReferenceUuid;
    private String code;
    private String type;

    private ApplicationResponseTypeBuilder() {
    }

    public static ApplicationResponseTypeBuilder getInstance(){
        return new ApplicationResponseTypeBuilder();
    }

    public ApplicationResponseType build() {
        return create(
                this.messageUuid,
                this.issueDateTime,
                this.messageReferenceUuid,
                this.type,
                this.code);
    }

    /**
     *
     * @param type of the message
     * @return builder
     */
    public ApplicationResponseTypeBuilder withType(String type) {
        this.type = type;
        return this;
    }

    /**
     *
     * @param messageUuid UUID of the message
     * @return builder
     */
    public ApplicationResponseTypeBuilder withMessageUuid(String messageUuid) {
        this.messageUuid = messageUuid;
        return this;
    }

    /**
     *
     * @param issueDateTime of the message
     * @return builder
     */
    public ApplicationResponseTypeBuilder withIssueDateTime(Calendar issueDateTime) {
        this.issueDateTime = issueDateTime;
        return this;
    }

    /**
     *
     * @param messageReferenceUuid of the message
     * @return builder
     */
    public ApplicationResponseTypeBuilder withMessageReferenceUuid(String messageReferenceUuid) {
        this.messageReferenceUuid = messageReferenceUuid;
        return this;
    }

    /**
     *
     * @param code of the message
     * @return builder
     */
    public ApplicationResponseTypeBuilder withMessageState(String code) {
        this.code = code;
        return this;
    }

    private static ApplicationResponseType create(
            String messageUuid,
            Calendar issueDateTime,
            String messageReferenceUuid,
            String messageBundle,
            String code) {
        ApplicationResponseType applicationResponseType = new ApplicationResponseType();

        applicationResponseType.setID(getIdType(messageUuid));
        applicationResponseType.setIssueDate(getIssueDateType(issueDateTime));
        applicationResponseType.setDocumentResponse(
                getDocumentResponseType(
                        messageReferenceUuid,
                        messageBundle,
                        code
                ));

        return applicationResponseType;
    }

    private static DocumentResponseType getDocumentResponseType(String messageReferenceUuid, String messageBundle, String code) {
        DocumentResponseType documentResponse = new DocumentResponseType();

        documentResponse.setResponse(getResponseType(messageReferenceUuid, code));
        documentResponse.setDocumentReference(getDocumentReferenceType(
                getIdType(messageReferenceUuid),
                getDocumentTypeCodeType(messageBundle)));

        return documentResponse;
    }

    private static DocumentReferenceType getDocumentReferenceType(IDType idType, DocumentTypeCodeType documentTypeCodeType) {
        DocumentReferenceType documentReference = new DocumentReferenceType();
        documentReference.setID(idType);
        documentReference.setDocumentTypeCode(documentTypeCodeType);
        return documentReference;
    }

    private static DocumentTypeCodeType getDocumentTypeCodeType(String messageBundle) {
        DocumentTypeCodeType documentTypeCode = new DocumentTypeCodeType();
        documentTypeCode.setValue(messageBundle);//currently we expect only status messages for bundles
        return documentTypeCode;
    }

    private static ResponseType getResponseType(String messageReferenceUuid, String code) {
        ResponseType response = new ResponseType();
        response.setReferenceID(getReferenceIDType(messageReferenceUuid));
        response.setResponseCode(getResponseCodeType(code));
        return response;
    }

    private static ResponseCodeType getResponseCodeType(String code) {
        ResponseCodeType responseCode = new ResponseCodeType();
        responseCode.setValue(code);
        return responseCode;
    }

    private static ReferenceIDType getReferenceIDType(String messageReferenceUuid) {
        ReferenceIDType referenceID = new ReferenceIDType();
        referenceID.setValue(messageReferenceUuid);
        return referenceID;
    }

    private static IssueDateType getIssueDateType(Calendar issueDateTime) {
        IssueDateType issueDate = new IssueDateType();
        issueDate.setValue(CalendarHelper.getJodaDateFromCalendar(issueDateTime));

        return issueDate;
    }

    private static IDType getIdType(String uuid) {
        IDType idType = new IDType();
        idType.setValue(uuid);
        return idType;
    }
}

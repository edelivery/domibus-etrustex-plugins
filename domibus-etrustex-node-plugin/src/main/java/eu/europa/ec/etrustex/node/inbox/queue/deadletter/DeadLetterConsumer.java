package eu.europa.ec.etrustex.node.inbox.queue.deadletter;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.node.domain.DomainNodePlugin;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import static eu.europa.ec.etrustex.common.exceptions.EtrustExMarker.NON_RECOVERABLE;

@Service("etxNodePluginDeadLetterConsumer")
public class DeadLetterConsumer implements MessageListener {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DeadLetterConsumer.class);

    @Autowired
    private ClearEtxLogKeysService clearEtxLogKeysService;

    @Autowired
    private DomainNodePluginService domainNodePluginService;

    @Override
    @DomainNodePlugin
    @ClearEtxLogKeys
    public void onMessage(Message message) {
        domainNodePluginService.setNodePluginDomain();
        String text = message.toString();
        if (message instanceof TextMessage) {
            try {
                text = ((TextMessage) message).getText();
            } catch (JMSException e) {
                LOG.error(NON_RECOVERABLE, e.getMessage(), e);
            }
        }
        LOG.error(NON_RECOVERABLE, "Dead Letter: {}", text);
        clearEtxLogKeysService.clearKeys();
    }
}
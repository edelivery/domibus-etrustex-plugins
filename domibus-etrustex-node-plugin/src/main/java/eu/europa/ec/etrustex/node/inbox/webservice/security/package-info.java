/**
 * This package contains classes for securing calls to exposed webservices - authentication/authorization mechanism
 * Mainly Apache CXF interceptors
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 28/09/2017
 */
package eu.europa.ec.etrustex.node.inbox.webservice.security;
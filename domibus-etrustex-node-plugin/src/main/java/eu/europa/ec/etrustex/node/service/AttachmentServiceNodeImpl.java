package eu.europa.ec.etrustex.node.service;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.util.LoggingUtils;
import eu.europa.ec.etrustex.node.data.dao.AttachmentDao;
import eu.europa.ec.etrustex.node.data.model.attachment.AttachmentState;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import eu.europa.ec.etrustex.node.data.model.common.EtxError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import static eu.europa.ec.etrustex.node.data.model.common.ErrorType.TECHNICAL;

/**
 * Implementation of {@link AttachmentService}
 * <p>
 * Handles operations on {@link EtxAttachment} entities
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 03/10/2017
 */
@Service("etxNodePluginAttachmentService")
public class AttachmentServiceNodeImpl implements AttachmentService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(AttachmentServiceNodeImpl.class);

    @Autowired
    private AttachmentDao attachmentDao;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public EtxAttachment findAttachmentById(Long attachmentId) {
        EtxAttachment byId = attachmentDao.findById(attachmentId);
        if (byId != null) {
            LOG.putMDC(LoggingUtils.MDC_ETX_ATT_UUID, byId.getAttachmentUuid());
            if (byId.getMessage() != null) {
                LOG.putMDC(LoggingUtils.MDC_ETX_MESSAGE_UUID, byId.getMessage().getMessageUuid());
            }
        }
        return byId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public EtxAttachment findAttachmentByDomibusMessageId(String domibusMessageId) {
        EtxAttachment attachmentByDomibusMessageId = attachmentDao.findAttachmentByDomibusMessageId(domibusMessageId);
        if (attachmentByDomibusMessageId != null) {
            LOG.putMDC(LoggingUtils.MDC_ETX_ATT_UUID, attachmentByDomibusMessageId.getAttachmentUuid());
            if (attachmentByDomibusMessageId.getMessage() != null) {
                LOG.putMDC(LoggingUtils.MDC_ETX_MESSAGE_UUID, attachmentByDomibusMessageId.getMessage().getMessageUuid());
            }
        }
        return attachmentByDomibusMessageId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void updateAttachmentState(EtxAttachment attachmentEntity, AttachmentState oldState, AttachmentState newState) {
        if (attachmentEntity.getStateType() == oldState) {
            attachmentEntity.setStateType(newState);

            attachmentDao.update(attachmentEntity);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void update(EtxAttachment etxAttachment) {
        attachmentDao.update(etxAttachment);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void attachmentInError(Long id, String errorMessage) {
        EtxAttachment attachment = findAttachmentById(id);
        if (attachment != null) {
            attachment.setStateType(AttachmentState.FAILED);
            attachment.setError(new EtxError(TECHNICAL.getCode(), errorMessage));
        }
    }
}

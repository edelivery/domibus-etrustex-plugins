package eu.europa.ec.etrustex.node.data.model.message;

/**
 * @author François GAUTIER
 * @version 1.0
 * @since 20/09/2017
 */
public enum MessageState {

    CREATED(false),

    UPLOADED(false),

    DOWNLOADED(false),

    SUBMITTED_TO_DOMIBUS(false),

    SENT_TO_BACKEND(false),

    DOWNLOADED_BY_BACKEND(false),

    FAILED(true);


    private boolean isFinal;

    MessageState(boolean isFinal) {
        this.isFinal = isFinal;
    }

    public boolean isFinal() {
        return isFinal;
    }
}

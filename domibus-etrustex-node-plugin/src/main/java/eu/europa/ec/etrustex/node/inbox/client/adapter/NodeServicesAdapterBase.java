package eu.europa.ec.etrustex.node.inbox.client.adapter;

import ec.schema.xsd.commonaggregatecomponents_2.BusinessHeaderType;
import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.schema.xsd.commonaggregatecomponents_2.TechnicalHeaderType;
import eu.europa.ec.etrustex.common.converters.node.NodePartnerConverter;
import eu.europa.ec.etrustex.node.webservice.client.WSClient;
import oasis.names.specification.ubl.schema.xsd.signatureaggregatecomponents_2.SignatureInformationType;
import org.unece.cefact.namespaces.standardbusinessdocumentheader.BusinessScope;
import org.unece.cefact.namespaces.standardbusinessdocumentheader.Scope;

import javax.xml.ws.Holder;

/**
 * Base class for all Webservice clients of Node exposed webservices
 * <p>
 * It's used by InboxServices to send requests
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 04/10/2017
 */
public abstract class NodeServicesAdapterBase extends WSClient {

    /**
     * business header marker
     */
    static final String STATUS_SCOPE_IDENTIFIER = "CREATE_STATUS_AVAILABLE";

    Holder<HeaderType> fillAuthorisationHeader(String senderPartyName, String receiverPartyName) {

        Holder<HeaderType> authorizationHeaderHolder = new Holder<>();
        authorizationHeaderHolder.value = new HeaderType();
        BusinessHeaderType businessHeaderType = new BusinessHeaderType();
        if (receiverPartyName != null) {
            businessHeaderType.getReceiver().add(NodePartnerConverter.buildPartnerType(receiverPartyName));
        }
        businessHeaderType.getSender().add(NodePartnerConverter.buildPartnerType(senderPartyName));

        authorizationHeaderHolder.value.setBusinessHeader(businessHeaderType);

        //create the technical header
        TechnicalHeaderType technicalHeaderType = new TechnicalHeaderType();
        technicalHeaderType.getSignatureInformation().add(new SignatureInformationType());
        authorizationHeaderHolder.value.setTechnicalHeader(technicalHeaderType);

        return authorizationHeaderHolder;
    }

    /**
     * Form the Header parameter element for eTrustEx service invocations
     *
     * @param senderPartyName   sender party's name
     * @param receiverPartyName receiver party's name
     * @return {@code HeaderType} holder
     */
    Holder<HeaderType> fillAuthorisationHeaderWithStatusScope(String senderPartyName, String receiverPartyName) {
        Holder<HeaderType> authorizationHeaderHolder = fillAuthorisationHeader(senderPartyName, receiverPartyName);

        BusinessScope businessScope = new BusinessScope();
        Scope scope = new Scope();
        scope.setType(STATUS_SCOPE_IDENTIFIER);
        businessScope.getScope().add(scope);
        authorizationHeaderHolder.value.getBusinessHeader().setBusinessScope(businessScope);

        return authorizationHeaderHolder;
    }

}

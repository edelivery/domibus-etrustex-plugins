package eu.europa.ec.etrustex.node.application;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.service.WorkspaceFileService;
import eu.europa.ec.etrustex.common.util.FileUtils;
import eu.europa.ec.etrustex.common.util.attachment.FileRepositoryUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.nio.file.Path;
import java.util.concurrent.TimeUnit;

/**
 * Cleans internal file workspace (specified by the implementation of {@link WorkspaceFileService}).
 * Cleaning is done by deleting old files and directories.
 * Old files are considered to be files with 'lastModified' value older than specified
 * by the implementation of {@link WorkspaceFileService})
 */
@DisallowConcurrentExecution
public class CleanAttachmentFilesJob extends NodeQuartzJobBean {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(CleanAttachmentFilesJob.class);

    @Autowired
    @Qualifier("etxNodePluginWorkspaceService")
    private WorkspaceFileService workspaceFileService;

    @Override
    public void process(JobExecutionContext jobExecutionContext) {
        Path repositoryRoot = workspaceFileService.getRoot();
        long ttlInDays = eTrustExNodePluginProperties.getCleanFilesTtl();
        if (ttlInDays > 0) {
            LOG.info("Workspace cleanup started [TTL: {} day(s); path: {} {}]",
                    ttlInDays,
                    repositoryRoot,
                    FileRepositoryUtil.getHumanReadableStorageInfo(repositoryRoot));
            long startTime = System.nanoTime();
            long ttlInMs = TimeUnit.MILLISECONDS.convert(ttlInDays, TimeUnit.DAYS);
            FileUtils.deleteOldFilesInDirectory(repositoryRoot, ttlInMs);
            long endTime = System.nanoTime();
            LOG.info("Workspace cleanup finished [execution time (s): {} {}]", (endTime - startTime) * 0.000000001,  FileRepositoryUtil.getHumanReadableStorageInfo(repositoryRoot));

            LOG.info("Root File Workspace content AFTER cleanup is: {}", FileRepositoryUtil.getPathContent(repositoryRoot));
        } else {
            LOG.warn("Workspace cleanup skipped [TTL: {}]",ttlInDays);
        }
    }
}

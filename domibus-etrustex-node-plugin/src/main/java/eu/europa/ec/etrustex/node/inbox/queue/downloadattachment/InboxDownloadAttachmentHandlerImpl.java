package eu.europa.ec.etrustex.node.inbox.queue.downloadattachment;

import ec.services.wsdl.documentwrapper_2.FaultResponse;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.node.data.model.attachment.AttachmentState;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import eu.europa.ec.etrustex.node.inbox.client.adapter.RetrieveDocumentWrapperAdapter;
import eu.europa.ec.etrustex.node.inbox.queue.QueueMessage;
import eu.europa.ec.etrustex.node.inbox.queue.error.InboxErrorProducer;
import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxMessageTransmissionProducer;
import eu.europa.ec.etrustex.node.service.AttachmentService;
import oasis.names.specification.ubl.schema.xsd.fault_1.FaultType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_ATT_UUID;

/**
 * Implementation of {@code InboxDownloadAttachmentHandler}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 03/10/2017
 */
@Service("etxNodePluginInboxDownloadAttachmentHandler")
@Transactional
public class InboxDownloadAttachmentHandlerImpl implements InboxDownloadAttachmentHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxDownloadAttachmentHandlerImpl.class);

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private RetrieveDocumentWrapperAdapter retrieveDocumentWrapperAdapter;

    @Autowired
    private InboxMessageTransmissionProducer inboxMessageTransmissionProducer;

    @Autowired
    private InboxErrorProducer inboxErrorProducer;

    @Autowired
    private InboxDownloadAttachmentProducer inboxDownloadAttachmentProducer;

    /**
     * {@inheritDoc}
     */
    @Override
    public void handleDownloadAttachment(InboxDownloadAttachmentQueueMessage queueMessage) {

        //load and validate the attachment
        EtxAttachment etxAttachment = loadValidateDownloadAttachment(queueMessage.getMessageId(), queueMessage.getAttachmentId());

        try {

            //calls Node retrieveDocumentWrapper and PayloadService to store the attachment locally
            retrieveDocumentWrapperAdapter.downloadAttachment(etxAttachment);

            //raise trigger for wrapper transmission to backend
            inboxMessageTransmissionProducer.triggerWrapperTransmissionToBackend(queueMessage.getMessageId(), queueMessage.getAttachmentId());

        } catch (FaultResponse faultResponse) {
            //node business error
            handleFaultResponse(faultResponse, queueMessage);
        } catch (Exception ex) {
            handleException(ex, queueMessage);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EtxAttachment loadValidateDownloadAttachment(final Long messageId, final Long attachmentId) {
        LOG.debug("loadValidateDownloadAttachment -> start");
        String errorDetails = "Attachment [ID: " + attachmentId + "]";

        //load the attachment
        final EtxAttachment etxAttachment = attachmentService.findAttachmentById(attachmentId);

        if (etxAttachment == null) {
            errorDetails += " not found";
            LOG.error(errorDetails);
            throw new ETrustExPluginException(ETrustExError.DEFAULT, errorDetails, null);
        }
        LOG.debug("Attachment loaded=  {}",  etxAttachment);
        LOG.putMDC(MDC_ETX_ATT_UUID, etxAttachment.getAttachmentUuid());

        if (etxAttachment.getStateType() != AttachmentState.CREATED) {
            errorDetails += " has an invalid state: " + etxAttachment.getStateType() + " from expected one: " + AttachmentState.CREATED.name();
            LOG.error(errorDetails);
            throw new ETrustExPluginException(ETrustExError.ETX_009, errorDetails, null);
        }

        //get the message
        EtxMessage etxMessage = etxAttachment.getMessage();

        if (!Objects.equals(messageId, etxMessage.getId()) || Objects.equals(etxMessage.getMessageState(), MessageState.FAILED)) {
            errorDetails = "message bundle ID: " + messageId + " is already in state: " + MessageState.FAILED.name();
            LOG.error(errorDetails);
            throw new ETrustExPluginException(ETrustExError.ETX_009, errorDetails, null);
        }

        return etxAttachment;
    }

    /**
     * Handles {@code FaultResponse} business error from Node webservice
     *
     * @param faultResponse Node business error
     * @param queueMessage InboxDownloadAttachment queue message
     */
    void handleFaultResponse(FaultResponse faultResponse, InboxDownloadAttachmentQueueMessage queueMessage) {

        //node business fault
        LOG.error("FaultResponse from Node retrieveDocumentWrapper service: ", faultResponse);

        //update the error details
        updateQueueMessageErrorDetails(queueMessage, faultResponse);

        //send it to error queue
        inboxErrorProducer.triggerError(queueMessage);

        LOG.debug("message: {} sent to error queue", queueMessage);
    }

    /**
     * Handles other types of errors extending {@code Exception} thrown when Node webservice is invoked
     *
     * @param exception exception to be handled
     * @param queueMessage InboxDownloadAttachment queue message
     */
    void handleException(Exception exception, InboxDownloadAttachmentQueueMessage queueMessage) {
        LOG.error("An exception occurred while performing retrieveDocumentWrapperRequest from Node: ", exception);

        if (StringUtils.isBlank(queueMessage.getErrorCode()) && StringUtils.isBlank(queueMessage.getErrorDescription())) {
            //fill up error details
            queueMessage.setErrorCode(ETrustExError.DEFAULT.name());
            queueMessage.setErrorDescription(exception.getMessage() + " caused by: " + ExceptionUtils.getRootCauseMessage(exception));

            //send it back to DownloadQueue
            inboxDownloadAttachmentProducer.triggerDownloadAttachment(queueMessage);

        } else {

            //this will trigger rollback and retry mechanism
            throw new ETrustExPluginException(ETrustExError.DEFAULT, exception.getMessage(), exception);
        }

    }

    /**
     * Updates the {@code QueueMessage} with error details
     *
     * @param queueMessage queue message to be updated
     * @param faultResponse Node fault business error
     */
    void updateQueueMessageErrorDetails(QueueMessage queueMessage, FaultResponse faultResponse) {
        if (queueMessage != null && faultResponse != null && faultResponse.getFaultInfo() != null) {
            final FaultType faultInfo =  faultResponse.getFaultInfo();

            //add the response code
            if (faultInfo.getResponseCode() != null) {
                queueMessage.setErrorCode(faultResponse.getFaultInfo().getResponseCode().getValue());
            }

            //add the description
            if (faultInfo.getDescription() != null) {
                StringBuilder description = new StringBuilder();
                int size = faultInfo.getDescription().size();
                for (int i = 0; i < size; i++) {
                    description.append(faultInfo.getDescription().get(i).getValue());
                    if (i < size - 1) {
                        description.append(System.lineSeparator());
                    }

                }
                queueMessage.setErrorDescription(description.toString());
            }
        }
    }

}

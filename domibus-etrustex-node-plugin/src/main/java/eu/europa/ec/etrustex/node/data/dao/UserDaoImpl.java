package eu.europa.ec.etrustex.node.data.dao;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.node.data.model.administration.EtxUser;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 * {@code UserDao} interface implementation
 *
 * @author François GAUTIER, Catalin Enache
 * @version 1.0
 * @since 19/09/2017
 */
@Repository("etxNodePluginUserDaoImpl")
public class UserDaoImpl extends DaoBaseImpl<EtxUser> implements UserDao {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DaoBaseImpl.class);

    public UserDaoImpl() {
        super(EtxUser.class);
    }

    @Override
    public EtxUser findUserByName(String userName) {
        TypedQuery<EtxUser> query = getEntityManager().createNamedQuery("findUserByNameEtxNodePlugin", EtxUser.class);
        query.setParameter("userName", userName);
        List<EtxUser> resultList = query.getResultList();
        if (resultList.isEmpty()) {
            LOG.warn("No EtxUser found in Node Plugin DB for UserByName {}", userName);
            return null;
        }
        return resultList.get(0);
    }

}
package eu.europa.ec.etrustex.node.inbox;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.handlers.Action;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxBundleTransmissionNodeHandler;
import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxStatusMessageTransmissionNodeHandler;
import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxWrapperTransmissionNodeHandler;
import org.springframework.beans.factory.annotation.Autowired;

import static eu.europa.ec.etrustex.common.exceptions.EtrustExMarker.NON_RECOVERABLE;
import static eu.europa.ec.etrustex.common.util.Validate.notNull;

/**
 * The FollowUpOperationHandlerFactory determines the correct handler for an operation based on the inbox action.
 * Each handler takes care of needed actions after an inbox message has been submitted. For ex. updating the status of ETX_MESSAGE table.
 *
 * @author Federico Martini
 * @version 1.0
 * @since 14/11/2017
 */
@org.springframework.stereotype.Service("etxNodePluginFollowUpOperationHandlerFactory")
public class FollowUpOperationHandlerFactory {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(FollowUpOperationHandlerFactory.class);

    @Autowired
    private InboxStatusMessageTransmissionNodeHandler inboxStatusMessageTransmissionNodeHandler;
    @Autowired
    private InboxBundleTransmissionNodeHandler inboxBundleTransmissionNodeHandler;
    @Autowired
    private InboxWrapperTransmissionNodeHandler inboxWrapperTransmissionNodeHandler;

    public FollowUpOperationHandler getOperationHandler(final Action action) {

        notNull(action, "Action is missing on FollowUpOperationHandler at eTrustEx Node plugin.");

        FollowUpOperationHandler fuOpeHandler = null;

        switch (action) {
            case STATUS_MESSAGE_TRANSMISSION:
                fuOpeHandler = inboxStatusMessageTransmissionNodeHandler;
                break;

            case NOTIFY_BUNDLE_TO_BACKEND_REQUEST:
                fuOpeHandler = inboxBundleTransmissionNodeHandler;
                break;

            case TRANSMIT_WRAPPER_TO_BACKEND:
                fuOpeHandler = inboxWrapperTransmissionNodeHandler;
                break;

            /// For all Outbox & ICA Operations
            case SUBMIT_APPLICATION_RESPONSE_RESPONSE:
            case STORE_DOCUMENT_WRAPPER_RESPONSE:
            case SUBMIT_DOCUMENT_BUNDLE_RESPONSE:
            case RETRIEVE_ICA_RESPONSE:
            case FAULT_ACTION:
                fuOpeHandler = new FollowUpOperationHandler() {
                    @Override
                    public void handleSendMessageSuccess(ETrustExAdapterDTO dto) {
                        //do nothing
                    }

                    @Override
                    public void handleSendMessageFailed(ETrustExAdapterDTO dto) {
                        //raise alert for manual handling
                        LOG.error(NON_RECOVERABLE, "Failure in OutboxService response/fault transmission for AS4 message ID: [{}] with service: [{}] and action: [{}]. Handle manually from Messages page or JMS Operations page in Domibus Admin console.", dto.getAs4MessageId(), dto.getAs4Service(), action);
                        throw new ETrustExPluginException(ETrustExError.DEFAULT, "Failure in OutboxService response/fault transmission for AS4 message ID: [" + dto.getAs4MessageId() + "] with service:[" + dto.getAs4Service() + "] and action:[" + action + "].");
                    }
                };
                break;
            default:
                LOG.error(NON_RECOVERABLE,"Action[" + action + "] not handled.");
                break;
        }
        notNull(fuOpeHandler, "Follow Up Operation Handler should not be null at eTrustEx Node plugin. Action submitted:" + action);
        return fuOpeHandler;
    }

}

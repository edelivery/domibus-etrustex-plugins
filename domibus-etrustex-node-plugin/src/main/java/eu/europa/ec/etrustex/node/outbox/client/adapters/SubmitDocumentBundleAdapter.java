package eu.europa.ec.etrustex.node.outbox.client.adapters;

import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.services.wsdl.documentbundle_2.DocumentBundlePortType;
import ec.services.wsdl.documentbundle_2.FaultResponse;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleRequest;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleResponse;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.jms.TextMessageCreator;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import eu.europa.ec.etrustex.node.data.model.administration.EtxUser;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.ws.Holder;

import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_MESSAGE_UUID;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.*;


/**
 * This class is responsible of:
 * 1. adapting the DTO information in order to call a WS
 * 2. adapting the WS response to a new DTO
 * 3. send the outgoing DTO to a reply queue
 *
 * @author Federico Martini
 * @version 1.0
 * @since 28/03/2017
 */
@Service("etxNodePluginSubmitDocumentBundleAdapter")
public class SubmitDocumentBundleAdapter extends ETrustExAdapterBase {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SubmitDocumentBundleAdapter.class);
    @Autowired
    private ETrustExNodePluginProperties eTrustExNodePluginProperties;

    @Override
    public String getServiceEndpoint() {
        return eTrustExNodePluginProperties.getDocumentBundleServiceEndpoint();
    }

    @Override
    public void performOperation(ETrustExAdapterDTO etxDto) {

        LOG.info("Performing operation [{}]", etxDto.getAs4Action());
        try {
            HeaderType headerType = extractETrustExXMLPayload(ContentId.CONTENT_ID_HEADER, etxDto);
            if (headerType == null) {
                throw new ETrustExPluginException(ETrustExError.DEFAULT, "Could not find the header payload for AS4 message [" + etxDto.getAs4MessageId() + "]");
            }

            Holder<HeaderType> authorizationHeaderHolder = new Holder<>();
            authorizationHeaderHolder.value = headerType;

            PayloadDTO submitDocumentBundlePayload = etxDto.getPayloadFromCID(ContentId.CONTENT_ID_GENERIC);
            if (submitDocumentBundlePayload == null) {
                throw new ETrustExPluginException(ETrustExError.DEFAULT, "Could not find the generic payload for AS4 message [" + etxDto.getAs4MessageId() + "]");
            }

            SubmitDocumentBundleRequest request = getSubmitDocumentBundleRequest(getPayloadAsXML(submitDocumentBundlePayload));
            LOG.putMDC(MDC_ETX_MESSAGE_UUID, request.getDocumentBundle().getID().getValue());

            EtxUser etxUser = getUser(etxDto.getAs4FromPartyId());
            DocumentBundlePortType port = buildDocumentBundlePort(etxUser.getName(), etxUser.getPassword());
            SubmitDocumentBundleResponse response = port.submitDocumentBundle(request, authorizationHeaderHolder);
            acknowledgeOk(etxDto, authorizationHeaderHolder.value, response);
        } catch (FaultResponse faultResponse) {
            acknowledgeFault(etxDto, faultResponse);
        } catch (Exception ex) {
            handleFaultDto(etxDto, ex);
        }
        LOG.info("Operation [{}] performed.", etxDto.getAs4Action());
    }

    void acknowledgeOk(ETrustExAdapterDTO incomingEtxDto, HeaderType headerType, SubmitDocumentBundleResponse response) {

        ETrustExAdapterDTO replyEtxDto = incomingEtxDto.invert();
        replyEtxDto.setAs4Action(commonPluginProperties.getAs4_Action_SubmitDocumentBundle_Response());
        replyEtxDto.addAs4Payload(getHeaderPayloadDTO(serializeObjToXML(headerType)));
        replyEtxDto.addAs4Payload(getGenericPayloadDTO(serializeObjToXML(response)));

        // Send it to the reply queue
        jmsOperations.send(new TextMessageCreator(serializeObjToXML(replyEtxDto)));
        LOG.info("Success acknowledgement from Node posted to jms queue etxNodePluginOutboundQueue.");
    }

    public void acknowledgeFault(ETrustExAdapterDTO incomingEtxDto, FaultResponse faultResp) {
        StringBuilder descriptions = new StringBuilder();
        if (faultResp.getCause() != null) {
            LOG.error("Fault response Error cause: ", faultResp.getCause());
            descriptions.append(faultResp.getCause().getMessage()).append("\n");
            faultResp = new FaultResponse("Error with exception in Node", faultResp.getFaultInfo());
        }
        for (DescriptionType desType : faultResp.getFaultInfo().getDescription()) {
            descriptions.append(desType.getValue()).append("\n");
        }
        LOG.warn("Fault response received from Node with code: [{}] and descriptions: [{}]", faultResp.getFaultInfo().getResponseCode().getValue(), descriptions);

        ETrustExAdapterDTO replyEtxDto = incomingEtxDto.invert();
        replyEtxDto.setAs4Action(commonPluginProperties.getAs4_Action_SubmitDocumentBundle_Response());
        replyEtxDto.addAs4Payload(getFaultResponsePayloadDTO(faultResp));

        // Send it to the reply queue
        jmsOperations.send(new TextMessageCreator(serializeObjToXML(replyEtxDto)));
        LOG.info("For AS4 message ID: [{}], fault response received from Node posted to jms queue etxNodePluginOutboundQueue.", replyEtxDto.getAs4RefToMessageId());
    }


    private static SubmitDocumentBundleRequest getSubmitDocumentBundleRequest(String xml) {
        return getObjectFromXml(xml);
    }
}

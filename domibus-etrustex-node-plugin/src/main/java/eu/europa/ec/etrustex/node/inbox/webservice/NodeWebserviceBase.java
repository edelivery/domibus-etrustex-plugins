package eu.europa.ec.etrustex.node.inbox.webservice;

import ec.schema.xsd.commonaggregatecomponents_2.BusinessHeaderType;
import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.util.HeaderUtils;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.service.security.IdentityManager;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.unece.cefact.namespaces.standardbusinessdocumentheader.Partner;

import javax.xml.ws.Holder;
import java.util.List;

import static eu.europa.ec.etrustex.common.exceptions.EtrustExMarker.NON_RECOVERABLE;

/**
 * Base class for Webservices exposed to the Node by InboxServices
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 13/10/2017
 */
public abstract class NodeWebserviceBase<T, R> {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(NodeWebserviceBase.class);

    @Autowired
    private IdentityManager identityManager;

    /**
     * It creates {@link EtxMessage} object from Node {@code nodeRequest} and saves it to DB
     *
     * @param nodeRequest   node request
     * @param senderParty   sender's party
     * @param receiverParty receiver's party
     * @return {@link EtxMessage} saved in DB
     */
    abstract EtxMessage createPersistMessage(T nodeRequest, EtxParty senderParty, EtxParty receiverParty);

    /**
     * build the response to be sent back to Node including ACK
     *
     * @param nodeRequest Node request
     * @param header      Header of the request
     * @return Node response
     */
    abstract R buildAckNodeResponse(T nodeRequest, Holder<HeaderType> header);

    /**
     * Extracts from request header {@link HeaderType} object only Sender and Receiver parties
     * and put them back to response header
     *
     * @param header request header
     * @return response header
     */
    HeaderType buildHeaderResponse(Holder<HeaderType> header) {
        LOG.debug("buildHeaderResponse -> start");
        final HeaderType headerTypeResponse = new HeaderType();
        final BusinessHeaderType businessHeaderTypeRequest = header.value.getBusinessHeader();
        if (businessHeaderTypeRequest != null) {
            BusinessHeaderType businessHeaderType = new BusinessHeaderType();

            //TBD we should add all or only the first from the list?
            businessHeaderType.getSender().addAll(businessHeaderTypeRequest.getSender());
            businessHeaderType.getReceiver().addAll(businessHeaderTypeRequest.getReceiver());

            headerTypeResponse.setBusinessHeader(businessHeaderType);
        }
        LOG.debug("buildHeaderResponse -> end");

        return headerTypeResponse;
    }


    /**
     * Extract the sender/receiver party from from {@link BusinessHeaderType}
     *
     * @param partnerList sender or receiver party list from {@link BusinessHeaderType}
     * @return null if partUuid is null or DB search doesn't find an {@link EtxParty}
     */
    EtxParty extractValidateParty(List<Partner> partnerList) {
        //get the sender party
        final String partyUuid = HeaderUtils.extractValidateParty(partnerList);

        //wil return if partyUuid is null or is not found in DB
        EtxParty etxParty = identityManager.searchPartyByUuid(partyUuid);

        if (etxParty == null) {
            LOG.error(NON_RECOVERABLE, "[ValidationFailed] Sender/Receiver party={} received in Business Header of notification from eTrustEx Node is not found in DB of Node Plugin.", partyUuid);
        }
        return etxParty;
    }

    /**
     * Returns today date as {@link DateTime} object
     *
     * @return today date
     */
    DateTime buildIssueDate() {
        return DateTime.now();
    }
}

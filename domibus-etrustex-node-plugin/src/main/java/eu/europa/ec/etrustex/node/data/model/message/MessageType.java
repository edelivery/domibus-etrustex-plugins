package eu.europa.ec.etrustex.node.data.model.message;

/**
 * @author François GAUTIER
 * @version 1.0
 * @since 20/09/2017
 */
public enum MessageType {
    MESSAGE_BUNDLE,
    MESSAGE_STATUS
}

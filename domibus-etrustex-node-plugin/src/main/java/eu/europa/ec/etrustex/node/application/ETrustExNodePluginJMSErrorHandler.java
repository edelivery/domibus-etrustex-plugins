package eu.europa.ec.etrustex.node.application;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import org.springframework.stereotype.Service;
import org.springframework.util.ErrorHandler;

/**
 * @author Arun Raj
 * @since 1.3.1
 */
@Service("etxNodePluginJMSErrorHandler")
public class ETrustExNodePluginJMSErrorHandler implements ErrorHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ETrustExNodePluginJMSErrorHandler.class);

    private ClearEtxLogKeysService clearEtxLogKeysService;
    private DomainNodePluginService domainNodePluginService;

    public ETrustExNodePluginJMSErrorHandler(ClearEtxLogKeysService clearEtxLogKeysService, DomainNodePluginService domainNodePluginService) {
        this.clearEtxLogKeysService = clearEtxLogKeysService;
        this.domainNodePluginService = domainNodePluginService;
    }

    @Override
    public void handleError(Throwable t) {
        domainNodePluginService.setNodePluginDomain();
        LOG.warn("Encountered JMS error in Node Plugin:", t);
        clearEtxLogKeysService.clearKeys();
    }
}

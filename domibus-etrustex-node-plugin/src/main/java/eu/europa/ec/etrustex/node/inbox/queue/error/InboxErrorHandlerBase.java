package eu.europa.ec.etrustex.node.inbox.queue.error;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.util.Validate;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import eu.europa.ec.etrustex.node.inbox.client.adapter.SubmitApplicationResponseFaultAdapter;
import eu.europa.ec.etrustex.node.inbox.queue.QueueMessage;
import eu.europa.ec.etrustex.node.inbox.queue.downloadattachment.InboxDownloadAttachmentQueueMessage;
import eu.europa.ec.etrustex.node.service.AttachmentService;
import eu.europa.ec.etrustex.node.service.MessageServicePlugin;
import org.springframework.beans.factory.annotation.Autowired;

import static eu.europa.ec.etrustex.common.exceptions.EtrustExMarker.NON_RECOVERABLE;
import static eu.europa.ec.etrustex.node.data.model.common.EtxError.ERROR_DETAIL_MAX_LENGTH;
import static org.apache.commons.lang3.StringUtils.substring;

/**
 * Base methods to handle {@link QueueMessage} from the queue etxNodePluginInboxErrorQueue
 *
 * @author François Gautier
 * @version 1.0
 * @since 10-Nov-17
 */
abstract class InboxErrorHandlerBase<T extends QueueMessage> implements InboxErrorHandler<T> {
    private final static DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxErrorHandlerBase.class);

    @Autowired
    private MessageServicePlugin messageService;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    SubmitApplicationResponseFaultAdapter submitApplicationResponseFaultAdapter;

    /**
     * Send a notification to the etrustExNode and update the failed {@link EtxMessage} and {@link EtxAttachment}
     * Update the failed entities with error information and FAILED states.
     * <p>In case of errors in wrappers download or transmission, there may be several errors triggered for the same message bundle. However only 1 notification should be raised for each bundle with failed wrapper.</p>
     *
     * @param queueMessage with a non null {@link InboxDownloadAttachmentQueueMessage@getId} and a
     *                     nullable {@link InboxDownloadAttachmentQueueMessage@getAttachmentId}
     */
    @Override
    public void handleErrorWithNotification(T queueMessage) {
        LOG.debug("Inbox flow handleErrorWithNotification -> start");

        EtxMessage etxMessage = getMessageById(queueMessage.getMessageId());
        Validate.notNull(etxMessage, "ETX_Message should not be null in Inbox Error handler flow.");
        EtxAttachment etxAttachment = getAttachmentById(queueMessage.getAttachmentId());

        MessageState initialMessageState = etxMessage.getMessageState();
        updateEntities(queueMessage, etxMessage, etxAttachment);
        //In case of errors in wrappers download or transmission, there may be several errors triggered for the same message bundle.
        // However only 1 notification should be raised for each bundle with failed wrapper.
        if (initialMessageState != MessageState.FAILED) {
            try {
                submitApplicationResponseFaultAdapter.sendNotification(
                        etxMessage.getReceiver(),
                        etxMessage.getSender(),
                        etxMessage.getMessageUuid());
            } catch (Exception e) {
                LOG.error("Exception while submitting an ApplicationResponse: ", e);
                // FaultResponse is also take care of here
                throw new ETrustExPluginException(ETrustExError.DEFAULT, e.getMessage(), e);
            }
        } else {
            StringBuilder errAlreadySentMsg = new StringBuilder("Inbox ETX_MESSAGE with ID: [").append(etxMessage.getId()).append("] for Bundle_UUID:[").append(etxMessage.getMessageUuid()).append("]");
            if (etxAttachment != null) {
                errAlreadySentMsg.append(" and Attachment_UUID in error:[").append(etxAttachment.getAttachmentUuid()).append("]");
            }
            errAlreadySentMsg.append(" is already in FAILED state. Failure notification is expected to be raised already. NOT raising new failure notification.");
            LOG.info(errAlreadySentMsg.toString());
        }

        LOG.debug("Inbox flow handleErrorWithNotification -> end");
    }

    /**
     * Update the failed entities with error information and FAILED states
     *
     * @param queueMessage with a non null {@link InboxDownloadAttachmentQueueMessage@getId} and a
     *                     nullable {@link InboxDownloadAttachmentQueueMessage@getAttachmentId}
     */
    @Override
    public void handleErrorWithoutNotification(T queueMessage) {
        EtxMessage messageById = getMessageById(queueMessage.getMessageId());
        EtxAttachment attachmentById = getAttachmentById(queueMessage.getAttachmentId());
        LOG.error(NON_RECOVERABLE, "Message with id {} and with attachment id {} is in failed WITHOUT notification", messageById, attachmentById);
        updateEntities(
                queueMessage,
                messageById,
                attachmentById);
    }


    /**
     * Generates errors and changes state to failed on {@link EtxMessage} and {@link EtxAttachment}
     * This method requires a new transaction because, in any cases, we need those changes to be persisted;
     * Even in the case of failure to transmit the notification to the EtrustExNode
     *
     * @param queueMessage not null
     * @param message      not null
     * @param attachment   nullable
     */
    private void updateEntities(T queueMessage, EtxMessage message, EtxAttachment attachment) {
        if (message.getMessageState() != MessageState.FAILED) {
            messageService.messageInError(
                    message.getId(),
                    getErrorMessage(queueMessage, "message", message.getMessageUuid()));
        }
        if (attachment != null) {
            attachmentService.attachmentInError(
                    attachment.getId(),
                    getErrorMessage(queueMessage, "wrapper", attachment.getAttachmentUuid()));
        }
    }

    static String getErrorMessage(QueueMessage queueMessage, String type, String messageUuid) {
        return substring("Failed to send " + type + " with uuid: " + messageUuid +
                " [ (" + queueMessage.getClass() + ") " + queueMessage.getErrorCode() + ": " + queueMessage.getErrorDescription() + " ]", 0, ERROR_DETAIL_MAX_LENGTH);
    }

    /**
     * @param attachmentId is nullable
     * @return the {@link EtxAttachment} with the given ID
     */
    private EtxAttachment getAttachmentById(Long attachmentId) {
        if (attachmentId == null) {
            return null;
        }
        return attachmentService.findAttachmentById(attachmentId);
    }

    /**
     * @param messageId is not null
     * @return the {@link EtxMessage} with the given ID
     * @throws NullPointerException if the messageId is null
     */
    private EtxMessage getMessageById(Long messageId) {
        return messageService.findMessageById(messageId);
    }

}

package eu.europa.ec.etrustex.node.inbox.webservice;

import ec.schema.xsd.ack_2.AcknowledgmentType;
import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.schema.xsd.documentbundle_1.DocumentBundleType;
import ec.services.wsdl.documentbundle_2.DocumentBundlePortType;
import ec.services.wsdl.documentbundle_2.FaultResponse;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleRequest;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleResponse;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.exceptions.ErrorTypeConverter;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.common.util.LoggingUtils;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.data.model.attachment.AttachmentDirection;
import eu.europa.ec.etrustex.node.data.model.attachment.AttachmentState;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import eu.europa.ec.etrustex.node.inbox.converters.NodeAcknowledgmentTypeConverter;
import eu.europa.ec.etrustex.node.inbox.converters.NodeMessageBundleConverter;
import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxMessageTransmissionProducer;
import eu.europa.ec.etrustex.node.service.MessageServicePlugin;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.jws.WebService;
import javax.xml.ws.Holder;

import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_MESSAGE_UUID;

/**
 * Webservice class which exposes DocumentBundle-v2 wsdl to Node
 * <p>
 * It implements {@link DocumentBundlePortType} and extends {@link NodeWebserviceBase}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 28/09/2017
 */
@Transactional
@WebService(
        name = "DocumentBundleServiceV2",
        portName = "DocumentBundleSoap11",
        serviceName = "DocumentBundleService-2.0",
        targetNamespace = "ec:services:wsdl:DocumentBundle-2",
        endpointInterface = "ec.services.wsdl.documentbundle_2.DocumentBundlePortType"
)
public class DocumentBundleServiceImpl extends NodeWebserviceBase<SubmitDocumentBundleRequest, SubmitDocumentBundleResponse>
        implements DocumentBundlePortType {

    /**
     * attachment active state
     */
    static final boolean ATT_ACTIVE_STATE = true;
    /**
     * node response document type code
     */
    static final String NODE_DOCUMENT_TYPE = "BDL";

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DocumentBundleServiceImpl.class);

    @Autowired
    private MessageServicePlugin messageService;

    @Autowired
    private InboxMessageTransmissionProducer inboxMessageTransmissionProducer;

    @Autowired
    private ClearEtxLogKeysService clearEtxLogKeysService;

    /**
     * Webservice method for push DocumentBundle by Node
     *
     * @param submitDocumentBundleRequest Node request
     * @param header                      submitDocumentBundle header
     * @return SubmitDocumentBundle response
     * @throws FaultResponse Exception thrown by the webservice
     */
    @Override
    @Transactional
    @ClearEtxLogKeys(clearCustomKeys = {LoggingUtils.MDC_ETX_MESSAGE_UUID})
    public SubmitDocumentBundleResponse submitDocumentBundle(SubmitDocumentBundleRequest submitDocumentBundleRequest, Holder<HeaderType> header) throws FaultResponse {
        if (submitDocumentBundleRequest.getDocumentBundle() != null &&
                submitDocumentBundleRequest.getDocumentBundle().getID() != null) {
            LOG.putMDC(MDC_ETX_MESSAGE_UUID, submitDocumentBundleRequest.getDocumentBundle().getID().getValue());
        }
        LOG.debug("submitDocumentBundle -> start");
        try {
            EtxParty senderParty = null;
            EtxParty receiverParty = null;
            if (header.value.getBusinessHeader() != null) {

                //get the sender party
                senderParty = extractValidateParty(header.value.getBusinessHeader().getSender());

                //get the receiver party
                receiverParty = extractValidateParty(header.value.getBusinessHeader().getReceiver());
            }

            if (senderParty != null && receiverParty != null) {
                final String senderPartyUuid = senderParty.getPartyUuid();
                final String receiverPartyUuid = receiverParty.getPartyUuid();
                LOG.info("NodePlugin received SubmitDocumentBundle for senderParty={}, receiverParty= {}", senderPartyUuid, receiverPartyUuid);

                //create the message and persist it
                EtxMessage messageBundle = createPersistMessage(submitDocumentBundleRequest, senderParty, receiverParty);

                //Backend plugin should be aware if the bundle has no attachments or receiverParty.requiresWrappers = false
                if (LOG.isDebugEnabled()) {
                    LOG.debug("submitDocumentBundle -> receiverParty " + receiverPartyUuid + " has requiresWrappers=" + receiverParty.isRequiresWrappers());
                    LOG.debug("submitDocumentBundle -> messageBundle " + messageBundle.getId() + " has" + (messageBundle.hasAttachments() ? StringUtils.EMPTY : " no") + " attachments");
                }

                //trigger bundle transmission to backend - no matter if the bundle has wrappers or not
                inboxMessageTransmissionProducer.triggerBundleTransmissionToBackend(messageBundle.getId());
            }

            //set the header back for response
            header.value = buildHeaderResponse(header);

            //return the response
            return buildAckNodeResponse(submitDocumentBundleRequest, header);
        } catch (ETrustExPluginException e) {
            LOG.error("Error occurred while receiving Inbox workflow - submit message bundle: ", e);
            throw new FaultResponse(e.getMessage(), ErrorTypeConverter.buildFaultType(e), e);
        } catch (Exception e) {
            LOG.error("Error occurred while receiving Inbox workflow - submit message bundle: ", e);
            throw new FaultResponse(e.getMessage(), null, e);
        }
        finally {
            clearEtxLogKeysService.clearSpecificKeys(MDC_ETX_MESSAGE_UUID);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    EtxMessage createPersistMessage(SubmitDocumentBundleRequest nodeRequest, EtxParty senderParty, EtxParty receiverParty) {
        LOG.debug("createPersistBundleMessage -> start");

        //create and populates the message bundle
        final EtxMessage messageBundle = NodeMessageBundleConverter.toEtxMessage(nodeRequest, receiverParty.isRequiresWrappers());

        //set the state, type and direction
        messageBundle.setMessageType(MessageType.MESSAGE_BUNDLE);
        messageBundle.setMessageState(MessageState.CREATED);
        messageBundle.setDirectionType(MessageDirection.NODE_TO_BACKEND);

        //set the sender party
        messageBundle.setSender(senderParty);

        //set the receiver party
        messageBundle.setReceiver(receiverParty);

        //attachments
        for (EtxAttachment etxAttachment : messageBundle.getAttachmentList()) {
            etxAttachment.setMessage(messageBundle);
            etxAttachment.setStateType(AttachmentState.CREATED);
            etxAttachment.setDirectionType(AttachmentDirection.NODE_TO_BACKEND);
            etxAttachment.setActiveState(ATT_ACTIVE_STATE);
        }
        messageService.createMessage(messageBundle);
        LOG.debug("createPersistBundleMessage -> end");

        return messageBundle;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    SubmitDocumentBundleResponse buildAckNodeResponse(SubmitDocumentBundleRequest nodeRequest, Holder<HeaderType> header) {
        LOG.debug("buildSubmitDocumentBundleAckResponse -> start");

        final SubmitDocumentBundleResponse submitDocumentBundleResponse = new SubmitDocumentBundleResponse();
        final DocumentBundleType documentBundleType = nodeRequest.getDocumentBundle();

        //message status id
        final String messageId = documentBundleType.getID() != null ? documentBundleType.getID().getValue() : null;

        //sender party UUID
        final String senderPartyUuid = CollectionUtils.isNotEmpty(header.value.getBusinessHeader().getSender()) ?
                header.value.getBusinessHeader().getSender().get(0).getIdentifier().getValue() : null;

        //receiver party uuid
        final String receiverPartyUuid = CollectionUtils.isNotEmpty(header.value.getBusinessHeader().getReceiver()) ?
                header.value.getBusinessHeader().getReceiver().get(0).getIdentifier().getValue() : null;

        //ack
        final AcknowledgmentType acknowledgmentType = NodeAcknowledgmentTypeConverter.buildAcknowledgmentType(buildIssueDate(),
                messageId, NODE_DOCUMENT_TYPE, senderPartyUuid, receiverPartyUuid);

        submitDocumentBundleResponse.setAck(acknowledgmentType);
        LOG.debug("buildSubmitDocumentBundleAckResponse -> end");

        return submitDocumentBundleResponse;
    }


}

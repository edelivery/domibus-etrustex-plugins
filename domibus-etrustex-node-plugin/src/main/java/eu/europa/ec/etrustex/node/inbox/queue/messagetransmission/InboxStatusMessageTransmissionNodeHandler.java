package eu.europa.ec.etrustex.node.inbox.queue.messagetransmission;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.MessagingProcessingException;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.util.ConversationIdKey;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import eu.europa.ec.etrustex.node.inbox.FollowUpOperationHandler;
import eu.europa.ec.etrustex.node.service.NodeConnectorSubmissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Handles the message status queue messages and transmission to backend
 * through Domibus
 * <p>
 * It extends {@link InboxMessageTransmissionHandlerBase}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 06/11/2017
 */
@Service("etxNodePluginInboxStatusMessageTransmissionHandler")
public class InboxStatusMessageTransmissionNodeHandler extends InboxMessageTransmissionHandlerBase implements FollowUpOperationHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxStatusMessageTransmissionNodeHandler.class);

    @Autowired
    private NodeConnectorSubmissionService nodeConnectorSubmissionService;

    /**
     * It load the {@link EtxMessage} from DB and submits the message to Domibus
     * <p>
     * In case of Domibus throwing a {@link MessagingProcessingException} we put the message
     * to {@code InboxErrorQueue} - hence the {@code Propagation.REQUIRES_NEW} attribute added as
     * Domibus may rollback internally
     * @see InboxMessageTransmissionHandlerBase#handleError(InboxMessageTransmissionQueueMessage, Exception) for error handling
     *
     * @param message {@code InboxMessageTransmission} queue message
     */
    @Override
    public void handleMessageTransmissionToBackend(final InboxMessageTransmissionQueueMessage message) {
        LOG.debug("handleMessageTransmissionToBackend -> start");

        try {
            //load and validate the message
            EtxMessage etxMessage = loadValidateMessage(message);

            //populate the DTO for Domibus
            final ETrustExAdapterDTO eTrustExAdapterDTO = populateEtxAdapterDTOForStatusMessage(etxMessage);

            final String domibusMsgId = nodeConnectorSubmissionService.submitToDomibus(eTrustExAdapterDTO);

            handleDomibusSubmissionSuccess(etxMessage, domibusMsgId);
        } catch (Exception e) {

            //sent it manually to error queue
            handleError(message, e);
        }
        LOG.debug("handleMessageTransmissionToBackend -> end");

    }


    /**
     * loads from DB and validates the status message
     *
     * @param message JMS message of type {@link InboxMessageTransmissionQueueMessage}
     * @return message fromDB of type {@link EtxMessage}
     */
    EtxMessage loadValidateMessage(final InboxMessageTransmissionQueueMessage message) {
        LOG.debug("loadValidateStatusMessage -> start");
        final Long messageId = message.getMessageId();

        //load from DB and validation
        final EtxMessage etxMessage = loadValidateEtxMessageForTransmission(messageId);

        //check if it a status message
        if (etxMessage.getMessageType() != MessageType.MESSAGE_STATUS) {
            final String errorDetails = "Message ID:[" + messageId + "] specified for Inbox Message transmission to backend is not a Status Message.";
            LOG.error(errorDetails);
            throw new ETrustExPluginException(ETrustExError.ETX_009, errorDetails, null);
        }

        LOG.debug("loadValidateStatusMessage -> end");
        return etxMessage;
    }

    /**
     * Construct the object {@link ETrustExAdapterDTO} needed for status message submission to Domibus
     *
     * @param etxMessage input object of type {@link EtxMessage}
     * @return {@link ETrustExAdapterDTO}
     */
    ETrustExAdapterDTO populateEtxAdapterDTOForStatusMessage(EtxMessage etxMessage) {
        final ETrustExAdapterDTO dto = createEtxAdapterDTOForInboxTransmission(etxMessage);

        //collaboration info
        dto.setAs4Service(commonProperties.getAs4ServiceStatusMessageTransmission());
        dto.setAs4ServiceType(commonProperties.getAs4ServiceStatusMessageTransmissionServiceType());
        dto.setAs4Action(commonProperties.getAs4ActionStatusMessageTransmissionRequest());

        //conversation id
        final String as4ConversationID = ConversationIdKey.INBOX_MESSAGE_STATUS.getConversationIdKey()+ etxMessage.getMessageUuid();
        dto.setAs4ConversationId(as4ConversationID);

        //payloads
        PayloadDTO statusMessagePayloadDTO = TransformerUtils.getGenericPayloadDTO(etxMessage.getXml());
        dto.addAs4Payload(statusMessagePayloadDTO);

        return dto;
    }

    /**
     * Takes care of finding the stored eTrustEx message and to update its status to SENT_TO_BACKEND.
     *
     * @param dto the object of type {@link ETrustExAdapterDTO} coming from the connector
     */
    @Override
    public void handleSendMessageSuccess(ETrustExAdapterDTO dto) {
        EtxMessage etxMsg = messageService.findMessageByDomibusMessageId(dto.getAs4MessageId());
        if (etxMsg == null) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "The eTrustEx message related to Domibus message with id [" + dto.getAs4MessageId() + "] was not found");
        }
        etxMsg.setMessageState(MessageState.SENT_TO_BACKEND);
        messageService.update(etxMsg);
    }

    /**
     * Gets an InboxMessageTransmissionQueueMessage object and sends it to the error queue.
     *
     * @param dto the object of type {@link ETrustExAdapterDTO} coming from the connector
     */
    @Override
    public void handleSendMessageFailed(ETrustExAdapterDTO dto) {
        InboxMessageTransmissionQueueMessage queueMessage = super.createMsgSendFailedQueueMessage(dto, MessageType.MESSAGE_STATUS);
        // Relying on the Inbox error mechanism to handle the fault
        inboxErrorProducer.triggerError(queueMessage);
    }

}

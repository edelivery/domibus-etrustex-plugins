package eu.europa.ec.etrustex.node.service;

import eu.domibus.ext.services.AuthenticationExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.MessagingProcessingException;
import eu.europa.ec.etrustex.common.exceptions.DomibusSubmissionException;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.node.DomibusETrustExNodeConnector;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import eu.europa.ec.etrustex.node.domain.DomainNodePlugin;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_CONVERSATION_ID;

/**
 * Service class which submit the {@link ETrustExAdapterDTO} using {@link DomibusETrustExNodeConnector}.
 * <p>
 * In case of Domibus throwing a validation exception - {@link MessagingProcessingException} and Domibus rolls back the transaction.
 * In order to avoid the Domibus transaction rollback affecting the plugin error handling flows, the attribute - {@link Propagation#REQUIRES_NEW}
 * is added and we throw a {@link ETrustExPluginException} having the Domibus exception as cause.
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 06/12/2017
 */
@Service("etxNodePluginConnectorSubmissionService")
public class NodeConnectorSubmissionService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(NodeConnectorSubmissionService.class);

    @Autowired
    private AuthenticationExtService authenticationExtService;

    @Autowired
    private DomibusETrustExNodeConnector domibusETrustExNodeConnector;

    @Autowired
    private ETrustExNodePluginProperties eTrustExNodePluginProperties;

    @Autowired
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Autowired
    private DomainNodePluginService domainNodePluginService;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @DomainNodePlugin
    @ClearEtxLogKeys
    public String submitToDomibus(ETrustExAdapterDTO dto) {
        domainNodePluginService.setNodePluginDomain();
        LOG.debug("Submitting DTO: {}", dto);
        LOG.putMDC(MDC_ETX_CONVERSATION_ID, dto.getAs4ConversationId());
        if (SecurityContextHolder.getContext().getAuthentication() == null ||
                !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            authenticationExtService.basicAuthenticate(eTrustExNodePluginProperties.getUser(), eTrustExNodePluginProperties.getPwd());
        }

        try {
            String submit = domibusETrustExNodeConnector.submit(dto);
            LOG.putMDC(DomibusLogger.MDC_MESSAGE_ID, submit);
            return submit;
        } catch (IllegalArgumentException | MessagingProcessingException mpEx) {
            LOG.error("For Message:[{}] submission to Domibus failed with error:", dto, mpEx);
            throw new DomibusSubmissionException(mpEx.getMessage(), mpEx);
        }
        finally {
            clearEtxLogKeysService.clearKeys();
        }
    }
}

package eu.europa.ec.etrustex.node.data.dao;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 * @author François GAUTIER
 * @version 1.0
 * @since 19/09/2017
 */
@Repository("etxNodePluginMessageDaoImpl")
public class MessageDaoImpl extends DaoBaseImpl<EtxMessage> implements MessageDao {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(MessageDaoImpl.class);

    public MessageDaoImpl() {
        super(EtxMessage.class);
    }

    @Override
    public EtxMessage findMessageByDomibusMessageId(String domibusMessageId) {
        LOG.debug("Finding EtxMessage instance with Domibus message id : {}", domibusMessageId);
        TypedQuery<EtxMessage> query = getEntityManager().createNamedQuery("findMessageByDomibusMessageIdExtNodePlugin", EtxMessage.class);
        query.setParameter("domibusMessageID", domibusMessageId);
        try {
            return query.getSingleResult();
        } catch (NoResultException nrEx) {
            LOG.debug("Could not find any EtxMessage(Bundle/Status) related to Domibus message with id [" + domibusMessageId + "]", nrEx);
            return null;
        }
    }
}

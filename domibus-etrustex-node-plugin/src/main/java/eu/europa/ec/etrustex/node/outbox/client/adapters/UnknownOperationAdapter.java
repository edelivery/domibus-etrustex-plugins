package eu.europa.ec.etrustex.node.outbox.client.adapters;

import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Adapter for handling unknown AS4 actions.
 *
 * @author Federico Martini
 * @version 1.0
 * @since 29/06/2017
 */
@Service("etxNodePluginUnknownOperationAdapter")
public class UnknownOperationAdapter extends ETrustExAdapterBase {

    @Override
    public String getServiceEndpoint() {
        return null;
    }

    @PostConstruct
    @Override
    protected void validateEndPoint() {
        //No end point to validate
    }


    @Override
    public void performOperation(ETrustExAdapterDTO etxDto) {
        throw new ETrustExPluginException(ETrustExError.DEFAULT, "Unknown operation for AS4 message [" + etxDto.getAs4MessageId() + "]");
    }

}

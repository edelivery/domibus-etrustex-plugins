package eu.europa.ec.etrustex.node.inbox.queue.downloadattachment;

import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import eu.europa.ec.etrustex.node.inbox.queue.QueueMessage;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * DownloadAttachment queue message type
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 25/09/2017
 */
public class InboxDownloadAttachmentQueueMessage extends QueueMessage {

    private Long messageId;

    private Long attachmentId;

    public InboxDownloadAttachmentQueueMessage() {}

    public InboxDownloadAttachmentQueueMessage(final MessageType messageType, final MessageDirection messageDirection,
                                               final Long messageId, final Long attachmentId) {
        super(messageType, messageDirection);
        this.messageId = messageId;
        this.attachmentId = attachmentId;
    }

    @Override
    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    @Override
    public Long getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Long attachmentId) {
        this.attachmentId = attachmentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof InboxDownloadAttachmentQueueMessage)) return false;

        InboxDownloadAttachmentQueueMessage that = (InboxDownloadAttachmentQueueMessage) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(messageId, that.messageId)
                .append(attachmentId, that.attachmentId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(messageId)
                .append(attachmentId)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .appendSuper(super.toString())
                .append("messageId", messageId)
                .append("attachmentId", attachmentId)
                .toString();
    }
}

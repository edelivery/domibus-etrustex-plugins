package eu.europa.ec.etrustex.node.inbox.queue.downloadattachment;

import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;

/**
 * DownloadAttachment queue handler for InboxServices
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 03/10/2017
 */
public interface InboxDownloadAttachmentHandler {

    /**
     * Receives the queue message, validate the attachment and finally calls
     * <p>
     * {@code RetrieveDocumentWrapper} webservice from Node
     *
     * @param queueMessage message queue of type {@link InboxDownloadAttachmentQueueMessage}
     */
    void handleDownloadAttachment(InboxDownloadAttachmentQueueMessage queueMessage);

    /**
     * Loads attachment from DB, validates it
     * <p>
     * Validation means message should not be in FAILED state and attachment should in CREATED state
     * <p>
     * throws {@link ETrustExPluginException} in case of error
     *
     * @param messageId    message id
     * @param attachmentId attachment id
     * @return attachment entity {@link EtxAttachment}
     */
    EtxAttachment loadValidateDownloadAttachment(final Long messageId, final Long attachmentId);
}

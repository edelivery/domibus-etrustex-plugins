package eu.europa.ec.etrustex.node.inbox.queue.messagetransmission;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.MessagingProcessingException;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.LargePayloadDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.service.WorkspaceService;
import eu.europa.ec.etrustex.common.util.ConversationIdKey;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import eu.europa.ec.etrustex.common.util.Validate;
import eu.europa.ec.etrustex.node.data.model.attachment.AttachmentState;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import eu.europa.ec.etrustex.node.data.model.common.EtxError;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import eu.europa.ec.etrustex.node.inbox.FollowUpOperationHandler;
import eu.europa.ec.etrustex.node.service.AttachmentService;
import eu.europa.ec.etrustex.node.service.NodeConnectorSubmissionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.Files;
import java.nio.file.Path;

import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_FAULT;

/**
 * Service to handle all operations related to transmission of Bundles or Wrappers for {@link MessageDirection#NODE_TO_BACKEND}.
 *
 * @author Arun Raj
 * @version 1.0
 * @since 27/09/2017
 */
@Service("etxNodePluginInboxWrapperTransmissionHandler")
public class InboxWrapperTransmissionNodeHandler extends InboxMessageTransmissionHandlerBase implements FollowUpOperationHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxWrapperTransmissionNodeHandler.class);

    @Autowired
    private NodeConnectorSubmissionService nodeConnectorSubmissionService;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    @Qualifier("etxNodePluginWorkspaceService")
    private WorkspaceService workspaceService;

    /**
     * Handles submission of Wrapper messages to Domibus for transmission to backend. Expected input is {@link InboxMessageTransmissionQueueMessage} with
     * id of tables {@code}ETX_MESSAGE{@code} & {@code}ETX_ATTACHMENT{@code} to retrieve instance of {@link EtxAttachment} to be transmitted.<br/>
     * On successful submission the {@link EtxAttachment} provided will be updated with the DomibusMessageId.<br/>
     * <p>
     * Note:
     * 1.{@link EtxAttachment} by itself does not have enough information for transmission.
     * Hence the related {@link EtxMessage} should always be provided for wrapper transmission.<br/>
     * 2. In case of Domibus throwing a {@link MessagingProcessingException} we put the message to {@code InboxErrorQueue} -
     * hence the {@link NodeConnectorSubmissionService#submitToDomibus(ETrustExAdapterDTO)} should be used to manage the transaction.<br/>
     *
     * @param message Instance of {@link InboxMessageTransmissionQueueMessage}
     * @see InboxMessageTransmissionHandlerBase#handleError(InboxMessageTransmissionQueueMessage, Exception) for exception handling
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void handleMessageTransmissionToBackend(InboxMessageTransmissionQueueMessage message) {

        try {
            Long etxMessageId = message.getMessageId();
            Long etxAttachmentId = message.getAttachmentId();
            LOG.debug("At handleMessageTransmissionToBackend for Bundle with ID: [{}] & Attachment with ID: [{}] - start", etxMessageId, etxAttachmentId);

            EtxMessage etxMessage = loadValidateBundleForTransmission(etxMessageId);
            EtxAttachment etxAttachment = loadValidateAttachmentForTransmission(etxAttachmentId, etxMessage);

            ETrustExAdapterDTO eTrustExAdapterDTO = populateETXAdapterDTOForSendBundleWrapperToBackend(etxMessage, etxAttachment);

            String domibusMsgId = nodeConnectorSubmissionService.submitToDomibus(eTrustExAdapterDTO);
            handleDomibusSubmissionSuccess(etxAttachment, domibusMsgId);

        } catch (Exception exp) {
            handleError(message, exp);
        }
    }


    /**
     * Retrieve the instance of {@link EtxMessage} identified by the input and validates if it is a {@link MessageType#MESSAGE_BUNDLE} ready for transmission to backend.<br/>
     *
     * @param etxMessageId Primary key of table {@code}ETX_ADT_MESSAGE{@code}. Mandatory
     * @return {@link EtxMessage} of type {@link MessageType#MESSAGE_BUNDLE}
     * @see InboxMessageTransmissionHandlerBase#loadValidateEtxMessageForTransmission(Long)
     */
    private EtxMessage loadValidateBundleForTransmission(Long etxMessageId) {
        EtxMessage etxMessage = loadValidateEtxMessageForTransmission(etxMessageId);
        if (etxMessage.getMessageType() != MessageType.MESSAGE_BUNDLE) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "Message ID: [" + etxMessageId + "] specified for Inbox Message transmission to backend is not a Bundle.");
        }
        return etxMessage;
    }

    /**
     * From the {@link EtxMessage} instance find the {@link EtxAttachment} identified by the attachmentId and validate its status for transmission to backend.<br/>
     * Main validation is that the Attachment should be found and status should be in {@link AttachmentState#DOWNLOADED}
     *
     * @param attachmentId - Id of the Attachment to be transmitted to backend
     * @param etxMessage   - Instance of {@link EtxMessage} from which attachment is to be retrieved
     * @return instance of {@link EtxAttachment}
     */
    private EtxAttachment loadValidateAttachmentForTransmission(Long attachmentId, EtxMessage etxMessage) {
        EtxAttachment etxAttachment = etxMessage.getAttachmentById(attachmentId);
        if (etxAttachment == null) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "Attachment ID: [" + attachmentId + "] specified for Inbox Message transmission to backend is not known.");
        }
        if (etxAttachment.getStateType() != AttachmentState.DOWNLOADED) {
            throw new ETrustExPluginException(ETrustExError.ETX_009, "Attachment ID: [" + attachmentId + "] is not in valid state for transmission to backend.");
        }
        return etxAttachment;
    }

    /**
     * To populate the AS4 metadata for transmitting the Bundle & Wrapper to the Backend.<br/>
     * Content ids required is - {@link TransformerUtils#CONTENT_ID_DOCUMENT}.<br/>
     * For wrapper transmission, payload profile validation should not be enabled in pMode.<br/>
     *
     * @param etxMessage    contains metadata to send the attachment
     * @param etxAttachment Attachment to attach to the message as payload
     * @return DTO to send Wrapper to the backEnd
     */
    private ETrustExAdapterDTO populateETXAdapterDTOForSendBundleWrapperToBackend(EtxMessage etxMessage, EtxAttachment etxAttachment) {
        ETrustExAdapterDTO eTrustExAdapterDTO = createEtxAdapterDTOForInboxTransmission(etxMessage);

        //Collaboration Info
        eTrustExAdapterDTO.setAs4Service(commonProperties.getAs4ServiceInboxWrapperTransmission());
        eTrustExAdapterDTO.setAs4ServiceType(commonProperties.getAs4ServiceInboxWrapperTransmissionServiceType());
        eTrustExAdapterDTO.setAs4Action(commonProperties.getAs4ActionInboxWrapperTransmissionRequest());
        eTrustExAdapterDTO.setAs4RefToMessageId(etxMessage.getDomibusMessageId());
        // Seen as non reproducible error in JIRA - EDELIVERY-3449
        Validate.notNull(eTrustExAdapterDTO.getAs4RefToMessageId(), "At InboxWrapperTransmissionHandler , while sending wrapper to Backend, RefToMessageId should not be null. It should be filled with the Domibus message id of the Bundle.");

        StringBuilder as4ConversationID = new StringBuilder().append(ConversationIdKey.INBOX_MESSAGEBUNDLE.getConversationIdKey()).append(etxMessage.getMessageUuid());
        eTrustExAdapterDTO.setAttachmentId(etxAttachment.getAttachmentUuid());

        Path wrapperFile = workspaceService.getFile(etxAttachment.getId());
        if (!Files.exists(wrapperFile)) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "For Bundle message with Id:" + etxMessage.getId() + " and Attachment with Id:" + etxAttachment.getId() + " , the file does not exist at location:" + wrapperFile.normalize().toAbsolutePath());
        }
        LargePayloadDTO largePayloadDTO = TransformerUtils.getLargePayloadDTO(wrapperFile.toFile(), etxAttachment.getFileName(), etxAttachment.getMimeType());
        eTrustExAdapterDTO.addAs4Payload(largePayloadDTO);

        //add conversation id
        as4ConversationID.append(ConversationIdKey.INBOX_ATTACHMENT.getConversationIdKey()).append(etxAttachment.getAttachmentUuid());

        eTrustExAdapterDTO.setAs4ConversationId(as4ConversationID.toString());
        return eTrustExAdapterDTO;
    }

    /**
     * On successful sumbission of message to Domibus, the status of attachment is updated as {@link AttachmentState#SUBMITTED_TO_DOMIBUS} and domibus message id is updated to the Attachment.<br/>
     *
     * @param etxAttachment Instance of {@link EtxAttachment} to be updated. Optional
     * @param domibusMsgId  message Id of the attachment in Domibus
     */
    private void handleDomibusSubmissionSuccess(EtxAttachment etxAttachment, String domibusMsgId) {

        etxAttachment.setDomibusMessageId(domibusMsgId);
        etxAttachment.setStateType(AttachmentState.SUBMITTED_TO_DOMIBUS);
        attachmentService.update(etxAttachment);
    }

    /**
     * This method will be called for each wrapper (attachment). <br/>
     * Using the DomibusMessageId, the correct instance of {@link EtxAttachment} is retrieved.
     * For each attachment found it will update its status to {@link MessageState#SENT_TO_BACKEND} & delete the corresponding file in the workspace folder.
     *
     * @param dto the object of type {@link ETrustExAdapterDTO} coming from the connector
     */
    @Override
    public void handleSendMessageSuccess(ETrustExAdapterDTO dto) {

        String domibusMessageId = StringUtils.trim(dto.getAs4MessageId());
        Validate.notBlank(domibusMessageId, "The Domibus message id required to retrieve the attachment has not been set");

        // Checks the presence of any attachment
        EtxAttachment etxAttachment = attachmentService.findAttachmentByDomibusMessageId(domibusMessageId);
        Validate.notNull(etxAttachment, "For Domibus message with id [" + domibusMessageId + "] no ETX wrapper was found");

        etxAttachment.setStateType(AttachmentState.SENT_TO_BACKEND);
        attachmentService.update(etxAttachment);
        workspaceService.deleteFile(etxAttachment.getId());
    }

    /**
     * Creates an {@link InboxMessageTransmissionQueueMessage} object with the error details and sends it to the error queue.
     * It searches for the existence of an attachment and adds it to the JMS message.
     * If the attachment exists, it attempts to delete the corresponding file in the workspace folder.
     *
     * @param dto the object of type {@link ETrustExAdapterDTO} coming from the connector
     */
    @Override
    public void handleSendMessageFailed(ETrustExAdapterDTO dto) {

        //Following 3 validations are never expected to fail. They are blocking in case of failure.
        EtxAttachment etxAttachment = attachmentService.findAttachmentByDomibusMessageId(dto.getAs4MessageId());
        Validate.notNull(etxAttachment, "For Domibus message ID: [" + dto.getAs4MessageId() + "] no related eTrustEx attachment was found.");
        EtxMessage etxMessage = etxAttachment.getMessage();
        Validate.notNull(etxMessage, "For Domibus message ID: [" + dto.getAs4MessageId() + "] and Inbox Attachment UUID:[" + etxAttachment.getAttachmentUuid() + "] no related eTrustEx message was found.");
        PayloadDTO payload = dto.getPayloadFromCID(CONTENT_FAULT);
        Validate.notNull(payload, "Fault payload was not found on InboxWrapperTransmissionHandler:handleSendMessageFailed for Domibus message id:[" + dto.getAs4MessageId() + "] ");

        // Queue Message to return
        InboxMessageTransmissionQueueMessage queueMessage = new InboxMessageTransmissionQueueMessage(etxMessage.getMessageType(), MessageDirection.NODE_TO_BACKEND, etxMessage.getId());

        queueMessage.setErrorCode(ETrustExError.DEFAULT.getCode());
        queueMessage.setErrorDescription(StringUtils.abbreviate(payload.getPayloadAsString(), EtxError.ERROR_DETAIL_MAX_LENGTH));
        queueMessage.setAttachmentId(etxAttachment.getId());
        // Relying on the Inbox error mechanism to handle the fault and notify the Node of the unsuccessful send
        inboxErrorProducer.triggerError(queueMessage);
        // The delete attempt is called here because in any case the JMS message has to be sent.
        workspaceService.deleteFile(etxAttachment.getId());
    }

}

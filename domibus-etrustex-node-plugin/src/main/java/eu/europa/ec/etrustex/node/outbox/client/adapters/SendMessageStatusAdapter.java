package eu.europa.ec.etrustex.node.outbox.client.adapters;

import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.services.wsdl.applicationresponse_2.ApplicationResponsePortType;
import ec.services.wsdl.applicationresponse_2.FaultResponse;
import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseRequest;
import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseResponse;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.jms.TextMessageCreator;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import eu.europa.ec.etrustex.node.data.model.administration.EtxUser;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsOperations;
import org.springframework.stereotype.Service;

import javax.xml.ws.Holder;

import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_MESSAGE_UUID;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.*;


/**
 * This class is responsible of:
 * 1. adapting the DTO information in order to call a WS
 * 2. adapting the WS response to a new DTO
 * 3. send the outgoing DTO to a reply queue
 *
 * @author Federico Martini
 * @version 1.0
 * @since 28/03/2017
 */
@Service("etxNodePluginSendMessageStatusAdapter")
public class SendMessageStatusAdapter extends ETrustExAdapterBase {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SendMessageStatusAdapter.class);

    @Autowired
    @Qualifier("etxNodePluginNodeToBackendJmsTemplate")
    private JmsOperations nodeToBackend;

    @Autowired
    private ETrustExNodePluginProperties eTrustExNodePluginProperties;

    @Override
    public String getServiceEndpoint() {
        return eTrustExNodePluginProperties.getApplicationResponseServiceEndpoint();
    }

    @Override
    public void performOperation(ETrustExAdapterDTO etxDto) {

        LOG.info("For AS4 Message Id: [{}], performing operation [{}]", etxDto.getAs4MessageId(), etxDto.getAs4Action());
        try {
            HeaderType headerType = TransformerUtils.extractETrustExXMLPayload(ContentId.CONTENT_ID_HEADER, etxDto);
            if (headerType == null) {
                throw new ETrustExPluginException(ETrustExError.DEFAULT, "Could not find the header payload for AS4 message [" + etxDto.getAs4MessageId() + "]");
            }

            Holder<HeaderType> authorizationHeaderHolder = new Holder<>();
            authorizationHeaderHolder.value = headerType;

            PayloadDTO submitApplicationResponseRequestPayload = etxDto.getPayloadFromCID(ContentId.CONTENT_ID_GENERIC);
            if (submitApplicationResponseRequestPayload == null) {
                throw new ETrustExPluginException(ETrustExError.DEFAULT, "Could not find the generic payload for AS4 message [" + etxDto.getAs4MessageId() + "]");
            }

            SubmitApplicationResponseRequest request = getSubmitApplicationResponseRequest(getPayloadAsXML(submitApplicationResponseRequestPayload));
            LOG.putMDC(MDC_ETX_MESSAGE_UUID, request.getApplicationResponse().getID().getValue());

            EtxUser etxUser = getUser(etxDto.getAs4FromPartyId());
            ApplicationResponsePortType port = buildApplicationResponsePort(etxUser.getName(), etxUser.getPassword());
            SubmitApplicationResponseResponse response = port.submitApplicationResponse(request, authorizationHeaderHolder);
            acknowledgeOk(etxDto, authorizationHeaderHolder.value, response);
        } catch (FaultResponse faultResponse) {
            acknowledgeFault(etxDto, faultResponse);
        } catch (Exception ex) {
            handleFaultDto(etxDto, ex);
        }
        LOG.info("For AS4 Message Id: [{}], operation [{}] performed.", etxDto.getAs4MessageId(), etxDto.getAs4Action());
    }

    void acknowledgeOk(ETrustExAdapterDTO incomingEtxDto, HeaderType headerType, SubmitApplicationResponseResponse response) {
        ETrustExAdapterDTO replyEtxDto = incomingEtxDto.invert();
        replyEtxDto.setAs4Action(commonPluginProperties.getAs4_Action_SubmitApplicationResponse_Response());
        replyEtxDto.addAs4Payload(TransformerUtils.getHeaderPayloadDTO(serializeObjToXML(headerType)));
        replyEtxDto.addAs4Payload(TransformerUtils.getGenericPayloadDTO(serializeObjToXML(response)));

        // Send it to the reply queue
        nodeToBackend.send(new TextMessageCreator(TransformerUtils.serializeObjToXML(replyEtxDto)));
        LOG.info("For AS4 Message Id: [{}], success acknowledgement from Node posted to jms queue etxNodePluginOutboundQueue.", replyEtxDto.getAs4RefToMessageId());
    }

    public void acknowledgeFault(ETrustExAdapterDTO incomingEtxDto, FaultResponse faultResp) {
        StringBuilder descriptions = new StringBuilder();
        if (faultResp.getCause() != null) {
            LOG.error("Fault response Error cause: ", faultResp.getCause());
            descriptions.append(faultResp.getCause().getMessage()).append("\n");
            faultResp = new FaultResponse("Error with exception in Node", faultResp.getFaultInfo());
        }
        for (DescriptionType desType : faultResp.getFaultInfo().getDescription()) {
            descriptions.append(desType.getValue()).append("\n");
        }
        LOG.warn("For AS4 Message Id [{}], Fault response received from Node with code: [{}] and descriptions: [{}].", incomingEtxDto.getAs4MessageId(), faultResp.getFaultInfo().getResponseCode().getValue(), descriptions);

        ETrustExAdapterDTO replyEtxDto = incomingEtxDto.invert();
        replyEtxDto.setAs4Action(commonPluginProperties.getAs4_Action_SubmitApplicationResponse_Response());
        replyEtxDto.addAs4Payload(getFaultResponsePayloadDTO(faultResp));

        // Send it to the reply queue
        nodeToBackend.send(new TextMessageCreator(TransformerUtils.serializeObjToXML(replyEtxDto)));
        LOG.info("For AS4 Message Id [{}], Fault response received from Node posted to jms queue etxNodePluginOutboundQueue.", replyEtxDto.getAs4RefToMessageId());
    }

    private static SubmitApplicationResponseRequest getSubmitApplicationResponseRequest(String xml) {
        return getObjectFromXml(xml);
    }
}

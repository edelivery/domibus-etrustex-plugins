package eu.europa.ec.etrustex.node.outbox.factory;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.adapters.Adapter;
import eu.europa.ec.etrustex.common.handlers.Action;
import eu.europa.ec.etrustex.common.handlers.Service;

import javax.annotation.Resource;
import java.util.Map;

import static eu.europa.ec.etrustex.node.outbox.factory.ETrustExOperationHandlerFactory.ERROR_LOG;

/**
 * The ETrustExOperationAdapterFactory determines which is the right adapter to perform the operation (a synchronous WS call) based on a service - action pair.
 * The flow of the request is from Domibus to Node.
 * <p>
 * {@link ETrustExOperationAdapterFactory#adaptersFactory} is defined in the spring configuration etrustex-node-plugin-connector.xml
 *
 * @author Federico Martini, François Gautier
 * @version 2.0
 * @since 29/06/2017
 */
@org.springframework.stereotype.Service("etxNodePluginETrustExOperationAdapterFactory")
public class ETrustExOperationAdapterFactory {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ETrustExOperationAdapterFactory.class);

    @Resource(name = "etxNodePluginAdaptersFactory")
    private Map<Action, Adapter> adaptersFactory;

    /**
     * Retrieve the appropriate {@link Adapter} for a given {@link Service} and {@link Action}
     *
     * @param service not used here
     * @param action  to be performed by the adapter
     * @return correct {@link Adapter} for a given {@link Action} or {@link eu.europa.ec.etrustex.node.outbox.client.adapters.UnknownOperationAdapter} if no appropriate {@link Adapter} found
     */
    public Adapter getOperationAdapter(Service service, Action action) {
        Adapter adapter = adaptersFactory.get(action);
        if (adapter == null) {
            LOG.error(ERROR_LOG, service, action);
            return adaptersFactory.get(Action.UNKNOWN);
        }
        return adapter;
    }
}

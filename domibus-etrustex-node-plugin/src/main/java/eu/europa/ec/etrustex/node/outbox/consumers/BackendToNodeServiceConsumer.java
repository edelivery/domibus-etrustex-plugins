package eu.europa.ec.etrustex.node.outbox.consumers;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.handlers.Action;
import eu.europa.ec.etrustex.common.handlers.Service;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.node.domain.DomainNodePlugin;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import eu.europa.ec.etrustex.node.outbox.factory.ETrustExOperationAdapterFactory;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_CONVERSATION_ID;

/**
 * BackendToNodeServiceConsumer is a message lister implementation.
 * It is used to listen on the queues named:
 * 1. jms/etx.plugin.internal.backendToNode
 * 2. jms/etx.plugin.internal.ricaInbound
 * <p>
 * It deserializes an Xml into an ETrustExAdapterDTO object and submits to Domibus through the connector.
 *
 * @author Federico Martini
 * @version 1.0
 * @since April 2017
 */
@org.springframework.stereotype.Service("etxNodePluginBackendToNodeServiceConsumer")
public class BackendToNodeServiceConsumer implements MessageListener {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(BackendToNodeServiceConsumer.class);

    private final ETrustExOperationAdapterFactory adapterFactory;

    private ClearEtxLogKeysService clearEtxLogKeysService;
    private DomainNodePluginService domainNodePluginService;

    public BackendToNodeServiceConsumer(ETrustExOperationAdapterFactory adapterFactory,
                                        ClearEtxLogKeysService clearEtxLogKeysService,
                                        DomainNodePluginService domainNodePluginService) {
        this.adapterFactory = adapterFactory;
        this.clearEtxLogKeysService = clearEtxLogKeysService;
        this.domainNodePluginService = domainNodePluginService;
    }

    @DomainNodePlugin
    @ClearEtxLogKeys
    public void onMessage(final Message message) {
        domainNodePluginService.setNodePluginDomain();
        ETrustExAdapterDTO dto = TextMessageUtil.getETrustExAdapterDTO((TextMessage) message);

        LOG.putMDC(DomibusLogger.MDC_MESSAGE_ID, dto.getAs4MessageId());
        LOG.putMDC(MDC_ETX_CONVERSATION_ID, dto.getAs4ConversationId());

        Service service = Service.fromString(dto.getAs4Service());
        Action action = Action.fromString(dto.getAs4Action());

        LOG.info("Received DTO with service [{}] and action [{}]", service, action);

        adapterFactory.getOperationAdapter(service, action).performOperation(dto);
        clearEtxLogKeysService.clearKeys();
    }
}

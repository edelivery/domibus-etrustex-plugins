package eu.europa.ec.etrustex.node.domain;

import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.services.DomainContextExtService;
import eu.domibus.ext.services.DomainExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static eu.domibus.logging.DomibusLogger.MDC_DOMAIN;

/**
 * Service responsible for setting the domain MDC keys to eTrustEx Node Plugin domain in multi-tenant environment.
 *
 * @author Francois Gautier
 * @since 1.2
 */
@Service
public class DomainNodePluginService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomainNodePluginService.class);

    @Autowired
    private DomainContextExtService domainContextExtService;

    @Autowired
    private DomainExtService domainExtService;

    @Autowired
    private ETrustExNodePluginProperties eTrustExNodePluginProperties;

    public void setNodePluginDomain() {
        String domainCode = eTrustExNodePluginProperties.getDomain();
        if (StringUtils.isNotBlank(domainCode)) {
            LOG.debug("Set domainCode to {}", domainCode);
            DomainDTO domain = domainExtService.getDomain(domainCode);
            domainContextExtService.setCurrentDomain(domain);
            LOG.putMDC(MDC_DOMAIN, domain.getCode());
        }
    }
}
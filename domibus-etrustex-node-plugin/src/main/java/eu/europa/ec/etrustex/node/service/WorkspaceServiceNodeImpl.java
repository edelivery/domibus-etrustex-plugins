package eu.europa.ec.etrustex.node.service;

import eu.europa.ec.etrustex.common.service.AbstractWorkspaceService;
import eu.europa.ec.etrustex.common.util.WorkSpaceUtils;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Implementation for
 * {@link eu.europa.ec.etrustex.common.service.WorkspaceService}
 * and
 * {@link eu.europa.ec.etrustex.common.service.WorkspaceFileService}
 * <p>
 * which handles creation and deletion of the attachment files
 * in the workspace/plugin payload folder
 *
 * @author François Gautier
 * @version 1.0
 * @since 22/12/2017
 */
@Service("etxNodePluginWorkspaceService")
public class WorkspaceServiceNodeImpl extends AbstractWorkspaceService {

    @Autowired
    private ETrustExNodePluginProperties eTrustExNodePluginProperties;

    /**
     * Returns the path for saving the attachments
     *
     * @return workspace path to save the attachments
     */
    @Transactional(readOnly = true)
    public Path getRoot() {
        String downloadLocation = eTrustExNodePluginProperties.getDownloadLocation();
        Validate.notNull(downloadLocation, "Workspace workspaceRoot path is not configured");

        Path workspaceRoot = Paths.get(downloadLocation).normalize().toAbsolutePath();
        LOG.info("Initializing the payload folder loaded from CommonAS4Properties : {}", workspaceRoot);
        return WorkSpaceUtils.getPath(workspaceRoot);
    }

    @Override
    public int getSleepMillis() {
        return eTrustExNodePluginProperties.getSleepDeleteFile();
    }

    @Override
    public int getTimeOut() {
        return eTrustExNodePluginProperties.getDeleteTimeout();
    }
}

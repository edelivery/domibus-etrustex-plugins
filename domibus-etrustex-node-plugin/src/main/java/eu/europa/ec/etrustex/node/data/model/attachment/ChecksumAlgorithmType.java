package eu.europa.ec.etrustex.node.data.model.attachment;

/**
 * @author François GAUTIER
 * @version 1.0
 * @since 20/09/2017
 */
public enum ChecksumAlgorithmType {
    SHA_512("SHA-512");

    private String value;

    ChecksumAlgorithmType(String value) {
        this.value = value;
    }

    public static ChecksumAlgorithmType fromValue(String value) {
        for (ChecksumAlgorithmType checksumAlgorithmType : values()) {
            if (checksumAlgorithmType.value.equals(value)) {
                return checksumAlgorithmType;
            }
        }
        throw new IllegalArgumentException("No matching enum found for value: " + value);
    }

    public String getValue() {
        return value;
    }
}

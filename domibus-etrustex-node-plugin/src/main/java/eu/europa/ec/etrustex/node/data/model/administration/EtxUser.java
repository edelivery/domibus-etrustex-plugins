package eu.europa.ec.etrustex.node.data.model.administration;

import eu.europa.ec.etrustex.node.data.model.AuditEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author François GAUTIER
 * @version 1.0
 * @since 20/09/2017
 */
@Entity(name = "etxNodePluginUser")
@AttributeOverrides({
        @AttributeOverride(name = "creationDate",
                column = @Column(name = "USR_CREATED_ON", insertable = false, updatable = false, nullable = false)),
        @AttributeOverride(name = "modificationDate",
                column = @Column(name = "USR_UPDATED_ON"))})
@Table(name = "ETX_USER")
@NamedQueries({
        @NamedQuery(name = "findUserByNameEtxNodePlugin",
                query = "Select usr from etxNodePluginUser usr    " +
                        "where usr.name = :userName     ",
                hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
})
public class EtxUser extends AuditEntity<Long> implements Serializable {

    private static final long serialVersionUID = -6595993781112694972L;

    @Id
    @Column(name = "USR_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "USR_NAME", length = 250, nullable = false)
    private String name;

    @Column(name = "USR_PASSWORD", length = 250, nullable = false)
    private String password;

    @Override
    public Long getId() {
        return this.id;
    }


    public String getName() {
        return this.name;
    }

    public String getPassword() {
        return this.password;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
                .append("id", id)
                .append("name", name)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EtxUser etxUser = (EtxUser) o;

        if (id != null ? !id.equals(etxUser.id) : etxUser.id != null) return false;
        if (name != null ? !name.equals(etxUser.name) : etxUser.name != null) return false;
        return password != null ? password.equals(etxUser.password) : etxUser.password == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}

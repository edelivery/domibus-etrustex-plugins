package eu.europa.ec.etrustex.node.outbox.handlers;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.jms.TextMessageCreator;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.LargePayloadDTO;
import eu.europa.ec.etrustex.common.service.WorkspaceFileService;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.jms.core.JmsOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.InputStream;
import java.nio.file.Path;


/**
 * @author Federico Martini
 * @version 2.0
 * @since Dec 2017
 */
@Service("etxNodePluginSDWHandler")
@Scope("prototype")
public class StoreDocumentWrapperHandler implements ETrustExOperationHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(StoreDocumentWrapperHandler.class);

    @Resource(name = "etxNodePluginBackendToNodeJmsTemplate")
    private JmsOperations jmsOperations;

    @Autowired
    @Qualifier("etxNodePluginWorkspaceService")
    private WorkspaceFileService workspaceFileService;

    @Override
    public boolean handle(ETrustExAdapterDTO etxDto) throws ETrustExPluginException {

        LargePayloadDTO largePayloadDTO = (LargePayloadDTO) etxDto.getPayloadFromCID(ContentId.CONTENT_ID_DOCUMENT);
        if (largePayloadDTO == null) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "Could not find the document to store for AS4 message [" + etxDto.getAs4MessageId() + "]");
        }
        String fileUID = largePayloadDTO.getFileName();
        try (InputStream is = largePayloadDTO.getPayloadDataHandler().getInputStream()){

            if (StringUtils.isBlank(fileUID)) {
                fileUID = LargePayloadDTO.UNKNOWN_PAYLOAD;
                LOG.warn("AS4 Message Id [{}]. For StoreDocumentWrapperRequest, File name was not provided", etxDto.getAs4MessageId());
            }
            // Adding Domibus unique id (without the domain)
            fileUID += "-" + StringUtils.substringBefore(etxDto.getAs4MessageId(), "@");
            // Adding payload type
            fileUID += LargePayloadDTO.PAYLOAD_SUFFIX;
            largePayloadDTO.setFileUID(fileUID);
            Path filePath = workspaceFileService.getFile(largePayloadDTO.getFilePath(), fileUID);
            largePayloadDTO.setFilePath(filePath.getParent().toString());
            workspaceFileService.writeLargeFile(filePath, is);

        } catch (Exception ex) {
            LOG.error("AS4 Message Id [{}]. For StoreDocumentWrapperRequest, could not save payload to file:[{}].", etxDto.getAs4MessageId(), fileUID);
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "Could not save payload to file [" + fileUID + "]", ex);
        }
        // Sends it to the queue
        jmsOperations.send(buildJMSMessage(etxDto));
        LOG.info("AS4 Message Id [{}]. StoreDocumentWrapperRequest message from backend plugin Message was posted into etxNodePluginInboundQueue.", etxDto.getAs4MessageId() );
        return true;
    }

    @Override
    public TextMessageCreator buildJMSMessage(ETrustExAdapterDTO etxDto) {
        String dtoAsXml = TransformerUtils.serializeObjToXML(etxDto);
        TextMessageCreator msgCreator = new TextMessageCreator(dtoAsXml);
        return msgCreator;
    }

}

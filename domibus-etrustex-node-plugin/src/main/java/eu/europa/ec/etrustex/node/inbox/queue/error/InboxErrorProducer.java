package eu.europa.ec.etrustex.node.inbox.queue.error;


import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.node.inbox.queue.QueueMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.Queue;

/**
 * Error queue producer for InboxServices
 * <p>
 * Converts the message to JSON format before sending as TEXT message to the queue
 * <p>
 * The JSON converter is defined in Spring xml configuration file
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 09/10/2017
 */
@Service("etxNodePluginInboxErrorProducer")
public class InboxErrorProducer {

    /** logger */
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxErrorProducer.class);

    @Autowired
    @Qualifier("etxNodePluginInboxErrorQueue")
    private Queue jmsQueue;

    @Autowired
    @Qualifier("etxNodePluginInboxErrorQueueJmsTemplate")
    private JmsTemplate jmsTemplate;

    /**
     * Put the message of base type {@link QueueMessage} to the error queue
     * <p>
     * This is always executing in a new transaction regarding the caller method.
     *
     * @param queueMessage message to be put to {@code InboxErrorQueue}
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void triggerError(QueueMessage queueMessage) {
        LOG.debug("triggerError -> start");

        LOG.info("triggerError -> put message:  {}", queueMessage);
        jmsTemplate.convertAndSend(jmsQueue, queueMessage);

        LOG.debug("triggerError -> end");
    }
}

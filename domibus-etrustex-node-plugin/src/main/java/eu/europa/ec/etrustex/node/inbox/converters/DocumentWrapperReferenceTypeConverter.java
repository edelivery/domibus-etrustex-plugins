package eu.europa.ec.etrustex.node.inbox.converters;

import ec.schema.xsd.commonaggregatecomponents_2.DocumentWrapperReferenceType;
import ec.schema.xsd.commonaggregatecomponents_2.ResourceInformationReferenceType;
import eu.europa.ec.etrustex.common.model.entity.attachment.AttachmentType;
import eu.europa.ec.etrustex.node.data.model.attachment.ChecksumAlgorithmType;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;

import javax.xml.bind.DatatypeConverter;

/**
 * Converter utility class.
 * <p>
 * Node specific attachment type {@link DocumentWrapperReferenceType} will be converted to
 * {@link EtxAttachment}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 20/09/2017
 */
public class DocumentWrapperReferenceTypeConverter {

    /**
     * private constructor as all methods are static
     */
    private DocumentWrapperReferenceTypeConverter() {
    }

    /**
     * Converts to plugin specific attachment type from the node specific type
     *
     * @param documentWrapperReferenceType attachment coming from Node of type {@link DocumentWrapperReferenceType}
     * @return etrustex plugin specific attachment type {@link EtxAttachment}
     */
    public static EtxAttachment toEtxAttachment(DocumentWrapperReferenceType documentWrapperReferenceType) {
        if (documentWrapperReferenceType == null) return null;

        EtxAttachment etxAttachment = new EtxAttachment();

        //attachment uuid
        etxAttachment.setAttachmentUuid(documentWrapperReferenceType.getID().getValue());

        //attachment type
        if (documentWrapperReferenceType.getDocumentTypeCode() != null) {
            etxAttachment.setAttachmentType(AttachmentType.valueOf(documentWrapperReferenceType.getDocumentTypeCode().getValue()));
        }

        ResourceInformationReferenceType resourceInformationReferenceType = documentWrapperReferenceType.getResourceInformationReference();
        if (resourceInformationReferenceType != null) {

            //file name
            etxAttachment.setFileName(resourceInformationReferenceType.getName().getValue());

            //file size
            if (resourceInformationReferenceType.getDocumentSize() != null) {
                etxAttachment.setFileSize(resourceInformationReferenceType.getDocumentSize().getValue().longValueExact());
            }

            //document hash method
            etxAttachment.setChecksumAlgorithmType(ChecksumAlgorithmType.fromValue(resourceInformationReferenceType.getDocumentHashMethod().getValue()));

            //document hash
            if (resourceInformationReferenceType.getDocumentHash() != null) {
                etxAttachment.setChecksum(DatatypeConverter.parseHexBinary(resourceInformationReferenceType.getDocumentHash().getValue()));
            }
        }

        return etxAttachment;
    }

}

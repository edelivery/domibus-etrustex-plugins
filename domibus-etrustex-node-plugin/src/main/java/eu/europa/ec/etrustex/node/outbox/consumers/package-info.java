/**
 * This package contains all the JMS queue consumers used for the outbox services.
 * <p>
 * See {@link javax.jms.MessageListener}
 *
 * @author Federico Martini
 * @version 1.0
 * @since 26/10/2017
 */
package eu.europa.ec.etrustex.node.outbox.consumers;
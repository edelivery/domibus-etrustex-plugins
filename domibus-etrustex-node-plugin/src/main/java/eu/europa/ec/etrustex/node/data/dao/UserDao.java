package eu.europa.ec.etrustex.node.data.dao;

import eu.europa.ec.etrustex.node.data.model.administration.EtxUser;

/**
 * DAO Interface for ETX_USER table
 *
 * @author François GAUTIER, Catalin Enache
 * @version 1.0
 * @since 19/09/2017
 */
public interface UserDao extends DaoBase<EtxUser> {

    /**
     * Find user from ETX_USER table by username
     *
     * @param userName user's name
     * @return entity class of type {@code EtxUser}
     */
    EtxUser findUserByName(String userName);

}
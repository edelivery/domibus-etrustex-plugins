package eu.europa.ec.etrustex.node.data.model.attachment;

/**
 * @author François GAUTIER
 * @version 1.0
 * @since 20/09/2017
 */
public enum AttachmentDirection {
    NODE_TO_BACKEND, BACKEND_TO_NODE
}

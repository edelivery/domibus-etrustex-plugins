package eu.europa.ec.etrustex.node.outbox.consumers;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.node.domain.DomainNodePlugin;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import eu.europa.ec.etrustex.node.service.NodeConnectorSubmissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_CONVERSATION_ID;

/**
 * NodeToBackendServiceConsumer is a message lister implementation.
 * It is used to listen on the queues named:
 * 1. jms/etx.plugin.internal.nodeToBackend
 * 2. jms/etx.plugin.internal.ricaOutbound
 * <p>
 * It deserializes an Xml into an ETrustExAdapterDTO object and submits to Domibus through the connector.
 *
 * @author Federico Martini
 * @version 1.0
 * @since April 2017
 */
@Service("etxNodePluginNodeToBackendServiceConsumer")
public class NodeToBackendServiceConsumer implements MessageListener {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(NodeToBackendServiceConsumer.class);

    @Autowired
    private NodeConnectorSubmissionService nodeConnectorSubmissionService;

    @Autowired
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Autowired
    private DomainNodePluginService domainNodePluginService;

    @DomainNodePlugin
    @ClearEtxLogKeys
    public void onMessage(final Message message) {
        domainNodePluginService.setNodePluginDomain();
        ETrustExAdapterDTO dto = TextMessageUtil.getETrustExAdapterDTO((TextMessage) message);
        LOG.putMDC(DomibusLogger.MDC_MESSAGE_ID, dto.getAs4MessageId());
        LOG.putMDC(MDC_ETX_CONVERSATION_ID, dto.getAs4ConversationId());

        nodeConnectorSubmissionService.submitToDomibus(dto);
        LOG.info("Reply submitted to Domibus Node plugin.");
        clearEtxLogKeysService.clearKeys();
    }
}

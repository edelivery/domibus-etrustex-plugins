package eu.europa.ec.etrustex.node.inbox.queue.messagetransmission;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.node.domain.DomainNodePlugin;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import eu.europa.ec.etrustex.node.inbox.queue.NodeConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * MessageTransmission queue consumer for InboxServices
 * <p>
 * Based on Spring {@code MessageListenerAdapter} implementation which send an already converted {@code InboxMessageTransmissionQueueMessage}
 * to {@code handleMessage} method
 * <p>
 * Conversion back from TEXT message/JSON is made by the the same {@code MessageConverter} defined in Spring config file and used by
 * {@code InboxMessageTransmissionProducer}
 *
 * @author Catalin Enache, Arun Raj
 * @version 1.0
 * @since 27/09/2017
 */
@Service("etxNodePluginInboxMessageTransmissionConsumer")
public class InboxMessageTransmissionConsumer implements NodeConsumer {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxMessageTransmissionConsumer.class);

    @Autowired
    private InboxStatusMessageTransmissionNodeHandler inboxStatusMessageTransmissionNodeHandler;
    @Autowired
    private InboxBundleTransmissionNodeHandler inboxBundleTransmissionNodeHandler;
    @Autowired
    private InboxWrapperTransmissionNodeHandler inboxWrapperTransmissionNodeHandler;
    @Autowired
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Autowired
    private DomainNodePluginService domainNodePluginService;

    /**
     * Initiate message transmission to Backend for:<br/>
     * <ul>
     * <li>Bundle messages - {@link InboxBundleTransmissionNodeHandler}</li>
     * <li>Wrapper - {@link InboxWrapperTransmissionNodeHandler}</li>
     * <li>Status messages - {@link InboxStatusMessageTransmissionNodeHandler}</li>
     * </ul>
     *
     * @param message an instance of {@link InboxMessageTransmissionQueueMessage} having details of message for transmission
     */
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    @DomainNodePlugin
    @ClearEtxLogKeys
    public void handleMessage(final InboxMessageTransmissionQueueMessage message) {
        domainNodePluginService.setNodePluginDomain();
        LOG.info("InboxMessageTransmissionQueueMessage -> message received: {}", message);

        switch (message.getMessageType()) {
            case MESSAGE_BUNDLE:
                switch (message.getMessageTransmission()) {
                    case BUNDLE:
                        inboxBundleTransmissionNodeHandler.handleMessageTransmissionToBackend(message);
                        break;
                    case WRAPPER:
                        inboxWrapperTransmissionNodeHandler.handleMessageTransmissionToBackend(message);
                        break;
                    default:
                        //this cannot happen
                        throw new AssertionError("Invalid MessageTransmission enum value");
                }
                break;
            case MESSAGE_STATUS:
                inboxStatusMessageTransmissionNodeHandler.handleMessageTransmissionToBackend(message);
                break;
            default:
                //this cannot happen
                throw new AssertionError("Invalid MessageType enum value");
        }
        clearEtxLogKeysService.clearKeys();
    }
}

package eu.europa.ec.etrustex.node.inbox.converters;


import ec.schema.xsd.commonaggregatecomponents_2.DocumentWrapperReferenceType;
import ec.schema.xsd.documentbundle_1.DocumentBundleType;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleRequest;
import eu.europa.ec.etrustex.common.util.CalendarHelper;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import org.apache.commons.collections4.CollectionUtils;

import static eu.europa.ec.etrustex.common.util.TransformerUtils.serializeObjToXML;

/**
 * Converter utility class.
 * <p>
 * Node specific message type {@link SubmitDocumentBundleRequest} will be converted to {@link EtxMessage} and
 * {@code java.util.List} of {@link EtxAttachment}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 20/09/2017
 */
public class NodeMessageBundleConverter {

    /**
     * private constructor as all the methods will be static
     */
    private NodeMessageBundleConverter() {
    }

    /**
     * Converts to plugin specific message object {@link EtxMessage} ((including attachments) )
     * from node {@link SubmitDocumentBundleRequest} object
     *
     * @param submitDocumentBundleRequest   node DocumentBundleRequest
     * @param receiverPartyRequiresWrappers if set to false no attachments will be added to {@link EtxMessage}
     * @return plugin specific message bundle type {@link EtxMessage}
     */
    public static EtxMessage toEtxMessage(SubmitDocumentBundleRequest submitDocumentBundleRequest, boolean receiverPartyRequiresWrappers) {
        final EtxMessage etxMessageBundle = new EtxMessage();

        final DocumentBundleType documentBundleType = submitDocumentBundleRequest.getDocumentBundle();
        if (documentBundleType == null) {
            return etxMessageBundle;
        }

        //message uuid
        if (documentBundleType.getID() != null) {
            etxMessageBundle.setMessageUuid(documentBundleType.getID().getValue());
        }

        //parent message uuid
        if (documentBundleType.getParentDocumentID() != null) {
            etxMessageBundle.setMessageParentUuid(documentBundleType.getParentDocumentID().getValue());
        }

        //message ref uuid
        if (documentBundleType.getUUID() != null) {
            etxMessageBundle.setMessageReferenceUuid(documentBundleType.getUUID().getValue());
        }

        //message issue date
        etxMessageBundle.setIssueDateTime(CalendarHelper.getCalendarIssueDateFromDocumentBundle(documentBundleType));

        //message xml
        etxMessageBundle.setXml(serializeObjToXML(submitDocumentBundleRequest));

        //if there are attachments
        if (receiverPartyRequiresWrappers && CollectionUtils.isNotEmpty(documentBundleType.getDocumentWrapperReference())) {
            for (DocumentWrapperReferenceType documentWrapperReferenceType : documentBundleType.getDocumentWrapperReference()) {
                EtxAttachment etxAttachment = DocumentWrapperReferenceTypeConverter.toEtxAttachment(documentWrapperReferenceType);
                etxMessageBundle.getAttachmentList().add(etxAttachment);
            }
        }

        return etxMessageBundle;
    }
}

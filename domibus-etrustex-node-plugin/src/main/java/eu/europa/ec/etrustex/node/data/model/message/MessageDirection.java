package eu.europa.ec.etrustex.node.data.model.message;

/**
 * @author François GAUTIER
 * @version 1.0
 * @since 20/09/2017
 */
public enum MessageDirection {
    NODE_TO_BACKEND, BACKEND_TO_NODE;

    public MessageDirection getOpposite() {
        return this == NODE_TO_BACKEND ? BACKEND_TO_NODE : NODE_TO_BACKEND;
    }
}

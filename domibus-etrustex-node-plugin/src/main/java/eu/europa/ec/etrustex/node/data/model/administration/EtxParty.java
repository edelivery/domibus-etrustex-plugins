package eu.europa.ec.etrustex.node.data.model.administration;

import eu.europa.ec.etrustex.node.data.model.AuditEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author François GAUTIER
 * @version 1.0
 * @since 20/09/2017
 */
@Entity(name = "EtxNodeParty")
@AttributeOverrides({
        @AttributeOverride(name = "creationDate",
                column = @Column(name = "PAR_CREATED_ON", insertable = false, updatable = false, nullable = false)),
        @AttributeOverride(name = "modificationDate",
                column = @Column(name = "PAR_UPDATED_ON"))})
@NamedQueries({
        @NamedQuery(name = "findByUUID",
                query = "SELECT p FROM EtxNodeParty p WHERE p.partyUuid = :partyUUID",
                hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
})
@Table(name = "ETX_PARTY")
public class EtxParty extends AuditEntity<Long> implements Serializable {

    private static final long serialVersionUID = 2793524751315811781L;

    @Id
    @Column(name = "PAR_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "PAR_UUID", length = 50, nullable = false, unique = true)
    private String partyUuid;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "USR_ID", unique = true)
    private EtxUser nodeUser;

    @Column(name = "PAR_REQUIRES_WRAPPERS", nullable = false)
    private boolean requiresWrappers;

    public EtxParty() {
        //empty constructor
    }

    @Override
    public Long getId() {
        return id;
    }

    public EtxUser getNodeUser() {
        return nodeUser;
    }

    public String getPartyUuid() {
        return partyUuid;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public void setNodeUser(EtxUser nodeUser) {
        this.nodeUser = nodeUser;
    }

    public void setPartyUuid(String partyUuid) {
        this.partyUuid = partyUuid;
    }

    public boolean isRequiresWrappers() {
        return requiresWrappers;
    }

    public void setRequiresWrappers(boolean requiresWrappers) {
        this.requiresWrappers = requiresWrappers;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", id)
                .append("partyUuid", partyUuid)
                .append("nodeUser", nodeUser)
                .append("requiresWrappers", requiresWrappers)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EtxParty etxParty = (EtxParty) o;

        if (requiresWrappers != etxParty.requiresWrappers) return false;
        if (id != null ? !id.equals(etxParty.id) : etxParty.id != null) return false;
        if (partyUuid != null ? !partyUuid.equals(etxParty.partyUuid) : etxParty.partyUuid != null) return false;
        return nodeUser != null ? nodeUser.equals(etxParty.nodeUser) : etxParty.nodeUser == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (partyUuid != null ? partyUuid.hashCode() : 0);
        result = 31 * result + (nodeUser != null ? nodeUser.hashCode() : 0);
        result = 31 * result + (requiresWrappers ? 1 : 0);
        return result;
    }
}

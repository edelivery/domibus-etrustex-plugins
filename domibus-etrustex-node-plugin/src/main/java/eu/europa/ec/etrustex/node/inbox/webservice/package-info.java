/**
 * This package contains classes to expose WS useful for the Inbox services.
 * The eTrustEx node calls them.
 *
 * @author Catalin Enache
 * @version 2.0
 * @since 28/09/2017
 */
package eu.europa.ec.etrustex.node.inbox.webservice;
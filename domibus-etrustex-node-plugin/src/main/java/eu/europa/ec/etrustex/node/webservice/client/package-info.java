/**
 * This package contains all the classes necessary to invoke the Node WS operations.
 * A JaxWs point to point proxy is built for each PortType (operations that can be performed and messages involved).
 * The connection is set-up using credentials.
 * <p>
 * See {@link eu.europa.ec.etrustex.node.webservice.client.NodeWebServiceProvider}
 *
 * @author Federico Martini
 * @version 1.0
 * @since 26/10/2017
 */
package eu.europa.ec.etrustex.node.webservice.client;
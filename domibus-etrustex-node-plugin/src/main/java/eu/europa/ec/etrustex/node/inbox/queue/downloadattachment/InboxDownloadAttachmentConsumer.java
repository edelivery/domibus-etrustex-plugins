package eu.europa.ec.etrustex.node.inbox.queue.downloadattachment;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.node.domain.DomainNodePlugin;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import eu.europa.ec.etrustex.node.inbox.queue.NodeConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * DownloadAttachment queue consumer for InboxServices
 * <p>
 * Based on Spring {@code MessageListenerAdapter} implementation which send an already converted {@code InboxDownloadAttachmentQueueMessage}
 * to {@code handleMessage} method
 * <p>
 * Conversion back from TEXT message/JSON is made by the the same {@code MessageConverter} defined in Spring config file and used by
 * {@code InboxDownloadAttachmentProducer}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 27/09/2017
 */
@Service("etxNodePluginInboxDownloadAttachmentConsumer")
public class InboxDownloadAttachmentConsumer implements NodeConsumer {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxDownloadAttachmentConsumer.class);

    @Autowired
    private InboxDownloadAttachmentHandler inboxDownloadAttachmentHandler;

    @Autowired
    private ClearEtxLogKeysService clearEtxLogKeysService;

    @Autowired
    private DomainNodePluginService domainNodePluginService;

    @Transactional(propagation = Propagation.REQUIRED)
    @DomainNodePlugin
    @ClearEtxLogKeys
    @Override
    public void handleMessage(final InboxDownloadAttachmentQueueMessage message) {
        domainNodePluginService.setNodePluginDomain();
        LOG.info("InboxDownloadAttachmentConsumer -> message received: {}", message);

        inboxDownloadAttachmentHandler.handleDownloadAttachment(message);

        clearEtxLogKeysService.clearKeys();
    }
}

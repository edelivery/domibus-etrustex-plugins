package eu.europa.ec.etrustex.node.data.dao;

public interface DaoBase<T> {

    /**
     * Finds an entity of the current type by its numeric ID (primary key).
     *
     * @param id an ID
     * @return the persistent object with the given ID, or null, if not found
     */
    T findById(Long id);

    /**
     * Saves the given, <i>new</i> entity.
     *
     * @param instance
     */
    void save(T instance);

    /**
     * Updates the given detached instance (must exist already).
     *
     * @param detachedInstance
     * @return
     */
    T update(T detachedInstance);

}
package eu.europa.ec.etrustex.node.data.dao;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import java.util.List;

import static eu.domibus.logging.DomibusLogger.MDC_DOMAIN;

/**
 * @author François GAUTIER
 * @version 1.0
 * @since 19/09/2017
 */
@Repository("etxNodePluginPartyDaoImpl")
public class PartyDaoImpl extends DaoBaseImpl<EtxParty> implements PartyDao {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PartyDaoImpl.class);

    public PartyDaoImpl() {
        super(EtxParty.class);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public EtxParty findByPartyUUID(String uuid) {
        LOG.debug("MDC domain:[{}]", LOG.getMDC(MDC_DOMAIN));
        TypedQuery<EtxParty> query = getEntityManager().createNamedQuery("findByUUID", EtxParty.class);
        query.setParameter("partyUUID", uuid);

        List<EtxParty> resultList = query.getResultList();
        if (resultList.isEmpty()) {
            LOG.warn("No EtxParty found in Node Plugin DB for PartyUUID {}", uuid);
            return null;
        }
        return resultList.get(0);
    }

}

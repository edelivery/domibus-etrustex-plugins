package eu.europa.ec.etrustex.node.inbox.webservice;

import ec.schema.xsd.ack_2.AcknowledgmentType;
import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.services.wsdl.applicationresponse_2.ApplicationResponsePortType;
import ec.services.wsdl.applicationresponse_2.FaultResponse;
import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseRequest;
import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseResponse;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.exceptions.ErrorTypeConverter;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.common.util.LoggingUtils;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import eu.europa.ec.etrustex.node.inbox.converters.NodeAcknowledgmentTypeConverter;
import eu.europa.ec.etrustex.node.inbox.converters.NodeMessageStatusConverter;
import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxMessageTransmissionProducer;
import eu.europa.ec.etrustex.node.service.MessageServicePlugin;
import oasis.names.specification.ubl.schema.xsd.applicationresponse_2.ApplicationResponseType;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.jws.WebService;
import javax.xml.ws.Holder;

import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_MESSAGE_UUID;

/**
 * Webservice class which exposes ApplicationResponseService-2.0 wsdl to Node
 * <p>
 * It implements {@link ApplicationResponsePortType} and extends {@link NodeWebserviceBase}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 13/10/2017
 */
@Transactional
@WebService(
        name = "ApplicationResponseServiceV2",
        portName = "ApplicationResponseSoap11",
        serviceName = "ApplicationResponseService-2.0",
        targetNamespace = "ec:services:wsdl:ApplicationResponse-2",
        endpointInterface = "ec.services.wsdl.applicationresponse_2.ApplicationResponsePortType"
)
public class ApplicationResponseServiceImpl extends NodeWebserviceBase<SubmitApplicationResponseRequest, SubmitApplicationResponseResponse>
        implements ApplicationResponsePortType {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ApplicationResponseServiceImpl.class);

    /**
     * node response document type code
     */
    static final String NODE_DOCUMENT_TYPE = "301";

    @Autowired
    private MessageServicePlugin messageService;

    @Autowired
    private InboxMessageTransmissionProducer inboxMessageTransmissionProducer;

    @Autowired
    private ClearEtxLogKeysService clearEtxLogKeysService;

    @Override
    @Transactional
    @ClearEtxLogKeys(clearCustomKeys = {LoggingUtils.MDC_ETX_MESSAGE_UUID})
    public SubmitApplicationResponseResponse submitApplicationResponse(SubmitApplicationResponseRequest submitApplicationResponseRequest,
                                                                       Holder<HeaderType> header) throws FaultResponse {
        if (submitApplicationResponseRequest.getApplicationResponse() != null &&
                submitApplicationResponseRequest.getApplicationResponse().getID() != null) {
            LOG.putMDC(MDC_ETX_MESSAGE_UUID, submitApplicationResponseRequest.getApplicationResponse().getID().getValue());
        }
        LOG.debug("submitApplicationResponse -> start");
        try {

            EtxParty senderParty = null;
            EtxParty receiverParty = null;
            if (header.value.getBusinessHeader() != null) {

                //get the sender party
                senderParty = extractValidateParty(header.value.getBusinessHeader().getSender());

                //get the receiver party
                receiverParty = extractValidateParty(header.value.getBusinessHeader().getReceiver());
            }

            if (senderParty != null && receiverParty != null) {
                final String senderPartyUuid = senderParty.getPartyUuid();
                final String receiverPartyUuid = receiverParty.getPartyUuid();
                LOG.info("NodePlugin received StatusMessage notification for senderParty= {}, receiverParty={}", senderPartyUuid, receiverPartyUuid);

                EtxMessage messageStatus = createPersistMessage(submitApplicationResponseRequest, senderParty, receiverParty);

                //put a message to InboxMessageTransmission queue
                inboxMessageTransmissionProducer.triggerMessageTransmissionToBackend(messageStatus.getId());
            }

            //set the header back for response
            header.value = buildHeaderResponse(header);

            //return the response
            return buildAckNodeResponse(submitApplicationResponseRequest, header);
        } catch (ETrustExPluginException e) {
            LOG.error("Error occurred while receiving Inbox workflow - SubmitApplicationResponse: ", e);
            throw new FaultResponse(e.getMessage(), ErrorTypeConverter.buildFaultType(e), e);
        } catch (Exception e) {
            LOG.error("Error occurred while receiving Inbox workflow - SubmitApplicationResponse: ", e);
            throw new FaultResponse(e.getMessage(), null, e);
        }
        finally {
            clearEtxLogKeysService.clearSpecificKeys(MDC_ETX_MESSAGE_UUID);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    EtxMessage createPersistMessage(SubmitApplicationResponseRequest nodeRequest, EtxParty senderParty, EtxParty receiverParty) {
        LOG.debug("createPersistMessageStatus -> start");

        //create and populates the message bundle
        final EtxMessage messageStatus = NodeMessageStatusConverter.toEtxMessage(nodeRequest);

        //set the state, type and direction
        messageStatus.setMessageType(MessageType.MESSAGE_STATUS);
        messageStatus.setMessageState(MessageState.CREATED);
        messageStatus.setDirectionType(MessageDirection.NODE_TO_BACKEND);

        //set the sender party
        messageStatus.setSender(senderParty);

        //set the receiver party
        messageStatus.setReceiver(receiverParty);

        //save the message in DB
        messageService.createMessage(messageStatus);

        LOG.debug("createPersistMessageStatus -> end");

        return messageStatus;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    SubmitApplicationResponseResponse buildAckNodeResponse(SubmitApplicationResponseRequest nodeRequest, Holder<HeaderType> header) {
        LOG.debug("buildAckNodeResponse -> start");

        //build the response object
        final SubmitApplicationResponseResponse submitApplicationResponseResponse = new SubmitApplicationResponseResponse();

        final ApplicationResponseType applicationResponseType = nodeRequest.getApplicationResponse();

        //message status id
        final String messageId = applicationResponseType.getID() != null ? applicationResponseType.getID().getValue() : null;

        //sender party UUID
        final String senderPartyUuid = CollectionUtils.isNotEmpty(header.value.getBusinessHeader().getSender()) ?
                header.value.getBusinessHeader().getSender().get(0).getIdentifier().getValue() : null;

        //receiver party uuid
        final String receiverPartyUuid = CollectionUtils.isNotEmpty(header.value.getBusinessHeader().getReceiver()) ?
                header.value.getBusinessHeader().getReceiver().get(0).getIdentifier().getValue() : null;

        //ack
        final AcknowledgmentType acknowledgmentType = NodeAcknowledgmentTypeConverter.buildAcknowledgmentType(buildIssueDate(),
                messageId, NODE_DOCUMENT_TYPE, senderPartyUuid, receiverPartyUuid);
        submitApplicationResponseResponse.setAck(acknowledgmentType);

        LOG.debug("buildAckNodeResponse -> end");

        return submitApplicationResponseResponse;
    }

}

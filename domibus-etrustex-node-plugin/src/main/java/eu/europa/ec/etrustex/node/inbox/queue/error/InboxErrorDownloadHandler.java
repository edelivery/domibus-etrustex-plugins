package eu.europa.ec.etrustex.node.inbox.queue.error;

import eu.europa.ec.etrustex.node.inbox.queue.downloadattachment.InboxDownloadAttachmentQueueMessage;
import org.springframework.stereotype.Service;

/**
 * Handle the errors in the download of attachment flow from the jms queue etxNodePluginInboxErrorQueue
 *
 * @author François Gautier
 * @version 1.0
 * @since 10-Nov-17
 */
@Service("etxNodePluginInboxErrorDownloadHandler")
public class InboxErrorDownloadHandler extends InboxErrorHandlerBase<InboxDownloadAttachmentQueueMessage> {

}

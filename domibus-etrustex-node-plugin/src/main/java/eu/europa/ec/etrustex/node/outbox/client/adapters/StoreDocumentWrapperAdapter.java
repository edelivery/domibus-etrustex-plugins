package eu.europa.ec.etrustex.node.outbox.client.adapters;

import ec.schema.xsd.commonaggregatecomponents_2.Base64BinaryType;
import ec.schema.xsd.commonaggregatecomponents_2.DocumentSizeType;
import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.services.wsdl.documentwrapper_2.DocumentWrapperPortType;
import ec.services.wsdl.documentwrapper_2.FaultResponse;
import ec.services.wsdl.documentwrapper_2.StoreDocumentWrapperRequest;
import ec.services.wsdl.documentwrapper_2.StoreDocumentWrapperResponse;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.jms.TextMessageCreator;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.LargePayloadDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.service.WorkspaceFileService;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import eu.europa.ec.etrustex.node.data.model.administration.EtxUser;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.xml.ws.Holder;
import java.math.BigDecimal;
import java.nio.file.Path;

import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_ATT_UUID;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.extractETrustExXMLPayload;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.getFaultResponsePayloadDTO;


/**
 * This class is responsible of:
 * 1. adapting the DTO information in order to call a WS
 * 2. adapting the WS response to a new DTO
 * 3. send the outgoing DTO to a reply queue
 *
 * @author Federico Martini
 * @version 2.0
 * @since 14/12/2017
 */
@Service("etxNodePluginStoreDocumentWrapperAdapter")
public class StoreDocumentWrapperAdapter extends ETrustExAdapterBase {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(StoreDocumentWrapperAdapter.class);

    @Autowired
    @Qualifier("etxNodePluginWorkspaceService")
    private WorkspaceFileService workspaceFileService;

    private static StoreDocumentWrapperRequest getStoreDocumentWrapperRequest(String xml) {
        return (StoreDocumentWrapperRequest) TransformerUtils.deserializeXMLToObj(xml);
    }

    @Autowired
    private ETrustExNodePluginProperties eTrustExNodePluginProperties;

    @Override
    public String getServiceEndpoint() {
        return eTrustExNodePluginProperties.getDocumentWrapperServiceEndpoint();
    }

    @Override
    public void performOperation(ETrustExAdapterDTO etxDto) {
        LargePayloadDTO largePayloadDTO = null;
        LOG.info("Performing operation [{}].", etxDto.getAs4Action());
        try {
            HeaderType headerType = extractETrustExXMLPayload(ContentId.CONTENT_ID_HEADER, etxDto);
            if (headerType == null) {
                throw new ETrustExPluginException(ETrustExError.DEFAULT, "Could not find the header payload for AS4 message [" + etxDto.getAs4MessageId() + "]");
            }

            Holder<HeaderType> authorizationHeaderHolder = new Holder<>();
            authorizationHeaderHolder.value = headerType;

            PayloadDTO storeDocumentWrapperRequestPayload = etxDto.getPayloadFromCID(ContentId.CONTENT_ID_GENERIC);
            if (storeDocumentWrapperRequestPayload == null) {
                throw new ETrustExPluginException(ETrustExError.DEFAULT, "Could not find the generic payload for AS4 message [" + etxDto.getAs4MessageId() + "]");
            }

            largePayloadDTO = (LargePayloadDTO) etxDto.getPayloadFromCID(ContentId.CONTENT_ID_DOCUMENT);
            if (largePayloadDTO == null) {
                throw new ETrustExPluginException(ETrustExError.DEFAULT, "Could not find the document to store for AS4 message [" + etxDto.getAs4MessageId() + "]");
            }
            LOG.debug(" LargePayloadDTO:[{}].", largePayloadDTO);

            StoreDocumentWrapperRequest request = getStoreDocumentWrapperRequest(TransformerUtils.getPayloadAsXML(storeDocumentWrapperRequestPayload));
            LOG.putMDC(MDC_ETX_ATT_UUID, request.getDocumentWrapper().getID().getValue());

            if (StringUtils.isBlank(largePayloadDTO.getFileUID())){
                throw new ETrustExPluginException(ETrustExError.DEFAULT, "Could not find the large payload file unique ID, AS4 message [" + etxDto.getAs4MessageId() + "]");
            }

            Path filePath = workspaceFileService.getFile(largePayloadDTO.getFilePath(), largePayloadDTO.getFileUID());
            largePayloadDTO.loadPayloadFromFile(filePath.toFile());
            Base64BinaryType binaryType = new Base64BinaryType();
            binaryType.setMimeCode(largePayloadDTO.getMimeType());
            binaryType.setValue(largePayloadDTO.getPayloadDataHandler());
            request.getDocumentWrapper().getResourceInformationReference().getLargeAttachment().setStreamBase64Binary(binaryType);
            // Sets the actual file size in the soap message
            Long documentSize = filePath.toFile().length();
            DocumentSizeType type = new DocumentSizeType();
            type.setValue(BigDecimal.valueOf(documentSize));
            request.getDocumentWrapper().getResourceInformationReference().setDocumentSize(type);
            EtxUser etxUser = getUser(etxDto.getAs4FromPartyId());
            DocumentWrapperPortType port = buildDocumentWrapperPort(etxUser.getName(), etxUser.getPassword());
            LOG.debug("Invoking DocumentWrapperPortType:storeDocumentWrapper for Attachment UUID:[{}]", request.getDocumentWrapper().getID().getValue());
            StoreDocumentWrapperResponse response = port.storeDocumentWrapper(request, authorizationHeaderHolder);
            acknowledgeOk(etxDto, authorizationHeaderHolder.value, response);
            deletePayload(largePayloadDTO);
        } catch (FaultResponse faultResponse) {
            acknowledgeFault(etxDto, faultResponse);
            deletePayload(largePayloadDTO);
        } catch (Exception ex) {
            handleFaultDto(etxDto, ex);
        }
        LOG.info("Operation [{}] successfully performed.", etxDto.getAs4Action());
    }

    private void deletePayload(LargePayloadDTO largePayloadDTO) {
        try {
            Path filePath = workspaceFileService.getFile(largePayloadDTO.getFilePath(), largePayloadDTO.getFileUID());
            workspaceFileService.deleteFile(filePath);
        } catch (Exception ex) {
            LOG.error("Error deleting payload file [{}]", largePayloadDTO.getFileUID(), ex);
        }
    }

    void acknowledgeOk(ETrustExAdapterDTO incomingEtxDto, HeaderType headerType, StoreDocumentWrapperResponse response) {

        ETrustExAdapterDTO replyEtxDto = incomingEtxDto.invert();
        replyEtxDto.setAs4Action(commonPluginProperties.getAs4_Action_StoreDocumentWrapper_Response());
        replyEtxDto.addAs4Payload(TransformerUtils.getGenericPayloadDTO(TransformerUtils.serializeObjToXML(response)));

        // Send it to the reply queue
        jmsOperations.send(new TextMessageCreator(TransformerUtils.serializeObjToXML(replyEtxDto)));
        LOG.info("Success acknowledgement from Node posted to jms queue etxNodePluginOutboundQueue");
    }

    public void acknowledgeFault(ETrustExAdapterDTO incomingEtxDto, FaultResponse faultResp) {
        StringBuilder descriptions = new StringBuilder();
        if (faultResp.getCause() != null) {
            LOG.error("Fault response Error cause: ", faultResp.getCause());
            descriptions.append(faultResp.getCause().getMessage()).append("\n");
            faultResp = new FaultResponse("Error with exception in Node", faultResp.getFaultInfo());
        }
        for (DescriptionType desType : faultResp.getFaultInfo().getDescription()) {
            descriptions.append(desType.getValue()).append("\n");
        }
        LOG.warn("Fault response received from Node with code: [{}] & descriptions [{}]", faultResp.getFaultInfo().getResponseCode().getValue(), descriptions);

        ETrustExAdapterDTO replyEtxDto = incomingEtxDto.invert();
        replyEtxDto.setAs4Action(commonPluginProperties.getAs4_Action_StoreDocumentWrapper_Response());
        replyEtxDto.addAs4Payload(getFaultResponsePayloadDTO(faultResp));

        // Send it to the reply queue
        jmsOperations.send(new TextMessageCreator(TransformerUtils.serializeObjToXML(replyEtxDto)));
        LOG.info("Fault response received from Node posted to jms queue etxNodePluginOutboundQueue.");
    }
}

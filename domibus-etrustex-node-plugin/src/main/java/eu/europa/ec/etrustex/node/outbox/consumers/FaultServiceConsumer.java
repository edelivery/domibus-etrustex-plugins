package eu.europa.ec.etrustex.node.outbox.consumers;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.FaultDTO;
import eu.europa.ec.etrustex.common.model.LargePayloadDTO;
import eu.europa.ec.etrustex.common.service.WorkspaceFileService;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import eu.europa.ec.etrustex.node.domain.DomainNodePlugin;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import eu.europa.ec.etrustex.node.service.NodeConnectorSubmissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.nio.file.Path;

import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_CONVERSATION_ID;

/**
 * FaultServiceConsumer is a message lister implementation.
 * It is used to listen on queue named jms/etx.plugin.internal.faultQueue
 * <p>
 * It deserializes an Xml into an ETrustExAdapterDTO object.
 * It creates a reply DTO object adding a fault payload; then it submits it to Domibus through the connector.
 *
 * @author Federico Martini
 * @version 1.0
 * @since 03 July 2017
 */
@Service("etxNodePluginFaultServiceConsumer")
public class FaultServiceConsumer implements MessageListener {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(FaultServiceConsumer.class);
    private static final String NA = "na";

    @Autowired
    private NodeConnectorSubmissionService nodeConnectorSubmissionService;

    @Autowired
    @Qualifier("etxNodePluginWorkspaceService")
    private WorkspaceFileService workspaceFileService;

    @Autowired
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Autowired
    private DomainNodePluginService domainNodePluginService;

    @DomainNodePlugin
    @ClearEtxLogKeys
    public void onMessage(final Message message) {
        domainNodePluginService.setNodePluginDomain();
        ETrustExAdapterDTO dto = TextMessageUtil.getETrustExAdapterDTO((TextMessage) message);
        LOG.putMDC(MDC_ETX_CONVERSATION_ID, dto.getAs4ConversationId());
        // In case of large payloads it attempts to ultimately delete the related file
        LargePayloadDTO largePayloadDTO = (LargePayloadDTO) dto.getPayloadFromCID(ContentId.CONTENT_ID_DOCUMENT);
        if (largePayloadDTO != null) {
            deletePayload(largePayloadDTO);
        }

        ETrustExAdapterDTO replyEtxDto = dto.invert();
        replyEtxDto.setAs4Action(FaultDTO.FAULT_ACTION);

        if (dto.hasFaultPayload()) {
            FaultDTO faultDTO = (FaultDTO) dto.getPayloadFromCID(ContentId.CONTENT_FAULT);
            LOG.info("Fault payload detected: {}", faultDTO);
            replyEtxDto.addAs4Payload(TransformerUtils.getFaultPayloadDTO(getErrorMessage(faultDTO)));
        }

        LOG.warn("Submitting failure reply referred to AS4 message with Id [" + replyEtxDto.getAs4RefToMessageId() + "]");
        nodeConnectorSubmissionService.submitToDomibus(replyEtxDto);
        clearEtxLogKeysService.clearKeys();
    }

    private String getErrorMessage(FaultDTO faultDTO) {
        return String.format("%s : %s [%s]", faultDTO.getDescription(), faultDTO.getThrowable().getMessage(), getDetailMessage(faultDTO));
    }

    private String getDetailMessage(FaultDTO faultDTO) {
        Throwable cause = faultDTO.getThrowable().getCause();
        if (cause != null) {
            return cause.getMessage();
        }
        return NA;
    }

    private void deletePayload(LargePayloadDTO largePayloadDTO) {
        try {
            Path filePath = workspaceFileService.getFile(largePayloadDTO.getFilePath(), largePayloadDTO.getFileUID());
            workspaceFileService.deleteFile(filePath);
        } catch (Exception ex) {
            LOG.error("Error deleting payload file[{}]", largePayloadDTO.getFileUID(), ex);
        }
    }
}

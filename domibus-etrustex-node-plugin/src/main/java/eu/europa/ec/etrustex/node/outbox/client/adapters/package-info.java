/**
 * This package contains all the classes necessary to handle the incoming DTO in order to invoke the appropriate eTrustEx node WS port type.
 * There is one Adapter per each eTrustEx node WS port type.
 * Each adapter replies, using an internal JMS reply queue, with an outgoing DTO containing either an acknowledgeOk or acknowledgeFault information.
 * The {@link eu.europa.ec.etrustex.node.outbox.client.adapters.UnknownOperationAdapter} is just there to handle exceptional cases: when the operation invoked does not exist.
 * <p>
 * See {@link eu.europa.ec.etrustex.node.outbox.factory.ETrustExOperationAdapterFactory}
 *
 * @author Federico Martini
 * @version 1.0
 * @since 26/10/2017
 */
package eu.europa.ec.etrustex.node.outbox.client.adapters;
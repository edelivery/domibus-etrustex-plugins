package eu.europa.ec.etrustex.node.webservice.client;

import ec.services.wsdl.applicationresponse_2.ApplicationResponsePortType;
import ec.services.wsdl.documentbundle_2.DocumentBundlePortType;
import ec.services.wsdl.documentwrapper_2.DocumentWrapperPortType;
import ec.services.wsdl.inboxrequest_2.InboxRequestPortType;
import ec.services.wsdl.retrieveinterchangeagreementsrequest_2.RetrieveInterchangeAgreementsRequestPortType;
import ec.services.wsdl.retrieverequest_2.RetrieveRequestPortType;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import org.apache.commons.codec.binary.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.xml.ws.BindingProvider;

import static org.apache.commons.lang3.StringUtils.isBlank;


public abstract class WSClient {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(WSClient.class);

    @Autowired
    private NodeWebServiceProvider webServiceProvider;

    public abstract String getServiceEndpoint();

    private String serviceEndpoint;

    /**
     * Validate the presence of a {@link WSClient#serviceEndpoint} after the construction of the bean
     */
    @PostConstruct
    protected void validateEndPoint(){
        if(LOG.isInfoEnabled()) {
            LOG.info(this.toString());
        }
        if (isBlank(getServiceEndpoint())) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "Service endpoint is not configured");
        }
        serviceEndpoint = getServiceEndpoint();
    }

    protected DocumentWrapperPortType buildDocumentWrapperPort(String user, String pwd) {
        DocumentWrapperPortType port = (DocumentWrapperPortType) webServiceProvider.buildJaxWsProxy(DocumentWrapperPortType.class, true, serviceEndpoint);

        BindingProvider bindingProvider = (BindingProvider) port;
        webServiceProvider.setupConnectionCredentials(bindingProvider, user, StringUtils.getBytesUtf8(pwd));

        return port;
    }

    protected ApplicationResponsePortType buildApplicationResponsePort(String user, String pwd) {
        // Prepare WS connection
        ApplicationResponsePortType port = (ApplicationResponsePortType) webServiceProvider.buildJaxWsProxy(ApplicationResponsePortType.class, false, serviceEndpoint);

        BindingProvider bindingProvider = (BindingProvider) port;
        webServiceProvider.setupConnectionCredentials(bindingProvider, user,StringUtils.getBytesUtf8(pwd));

        return port;
    }

    protected DocumentBundlePortType buildDocumentBundlePort(String user, String pwd) {
        // Prepare WS connection
        DocumentBundlePortType port = (DocumentBundlePortType) webServiceProvider.buildJaxWsProxy(DocumentBundlePortType.class, false, serviceEndpoint);

        BindingProvider bindingProvider = (BindingProvider) port;
        webServiceProvider.setupConnectionCredentials(bindingProvider, user, StringUtils.getBytesUtf8(pwd));

        return port;
    }

    protected InboxRequestPortType buildInboxRequestPort(String user, String pwd) {
        // Prepare WS connection
        InboxRequestPortType port = (InboxRequestPortType) webServiceProvider.buildJaxWsProxy(InboxRequestPortType.class, false, serviceEndpoint);

        BindingProvider bindingProvider = (BindingProvider) port;
        webServiceProvider.setupConnectionCredentials(bindingProvider, user, StringUtils.getBytesUtf8(pwd));

        return port;
    }

    protected RetrieveInterchangeAgreementsRequestPortType getRetrieveInterchangeAgreementsRequestPortType() {
        return (RetrieveInterchangeAgreementsRequestPortType) webServiceProvider.buildJaxWsProxy(
                    RetrieveInterchangeAgreementsRequestPortType.class, false, serviceEndpoint);
    }

    protected void setCredentials(String user, String pwd, BindingProvider port) {
        webServiceProvider.setupConnectionCredentials(port, user, StringUtils.getBytesUtf8(pwd));
    }

    protected RetrieveRequestPortType buildRetrieveRequestPort(String user, String pwd) {

        // Prepare WS connection
        RetrieveRequestPortType port = (RetrieveRequestPortType) webServiceProvider.buildJaxWsProxy(RetrieveRequestPortType.class, false, serviceEndpoint);

        BindingProvider bindingProvider = (BindingProvider) port;

        webServiceProvider.setupConnectionCredentials(bindingProvider, user, StringUtils.getBytesUtf8(pwd));

        return port;
    }

}

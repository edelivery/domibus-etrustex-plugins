package eu.europa.ec.etrustex.node.service.security;

import eu.europa.ec.etrustex.node.data.dao.PartyDao;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * {@code IdentityManager} implementation
 * <p>
 * Responsible for authentication and authorization of users and parties
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 19/09/2017
 */
@Service("etxNodePluginIdentityManager")
@Transactional(readOnly = true)
public class IdentityManagerImpl implements IdentityManager {

    @Autowired
    private PartyDao partyDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public EtxParty searchPartyByUuid(String partyUuid) {
        if (StringUtils.isBlank(partyUuid)) {
            return null;
        }
        return partyDao.findByPartyUUID(partyUuid);
    }

}

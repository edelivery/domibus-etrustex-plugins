-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: src/main/resources/config/liquibase/changelog/changelog-1.2.0-delta.xml
-- Ran at: 6/19/20 1:46 PM
-- Against: null@offline:oracle?changeLogFile=C:\workspace\domibus-etrustex-plugins\domibus-etrustex-node-plugin\target/liquibase/changelog-1.2.0-delta.oracle
-- Liquibase version: 3.5.4
-- *********************************************************************

SET DEFINE OFF;

-- Changeset src/main/resources/config/liquibase/changelog/changelog-1.2.0-delta.xml::EDELIVERY-6432::venugar
ALTER TABLE ETX_USER DROP COLUMN USR_NODE_SYS;


-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: src/main/resources/config/liquibase/changelog/20180613142000_1.1.0-delta.xml
-- Ran at: 6/19/20 1:46 PM
-- Against: null@offline:oracle?changeLogFile=C:\workspace\domibus-etrustex-plugins\domibus-etrustex-node-plugin\target/liquibase/changelog-1.1.0-data.oracle
-- Liquibase version: 3.5.4
-- *********************************************************************

SET DEFINE OFF;

-- Changeset src/main/resources/config/liquibase/changelog/20180613142000_1.1.0-delta.xml::EDELIVERY-2009_create_hibernate_sequence::gautifr
set serveroutput on;
DECLARE
    l_new_seq INTEGER;
    rcount number := 0;
    new_seq_no INTEGER;
    sql_stmt  VARCHAR2(3000);
BEGIN
    select max(maxnum) INTO l_new_seq from(
            select (max(last_number) + 1) maxnum  from user_sequences
            WHERE sequence_name IN (
            'ETX_ATT_SQ',
            'ETX_ERR_SQ',
            'ETX_MSG_SQ',
            'ETX_PAR_SQ',
            'ETX_USR_SQ'
            )
            union
            select (max(msg_id) + 1) maxnum from ETX_MESSAGE
            union
            select (max(att_id) + 1) maxnum from ETX_ATTACHMENT
            union
            select (max(err_id) +1) maxnum from ETX_ERROR
    );

    select count(1) into rcount from user_sequences where upper(sequence_name) = 'HIBERNATE_SEQUENCE';
    if rcount > 0 then
        sql_stmt := 'alter sequence HIBERNATE_SEQUENCE increment by ' || l_new_seq;
        dbms_output.put_line(sql_stmt);
        execute immediate sql_stmt;

        sql_stmt := 'select HIBERNATE_SEQUENCE.nextval from dual';
        dbms_output.put_line(sql_stmt);
        execute immediate sql_stmt into new_seq_no;
        dbms_output.put_line('new_seq_no:'||new_seq_no);

        sql_stmt := 'alter sequence HIBERNATE_SEQUENCE increment by  1';
        dbms_output.put_line(sql_stmt);
        execute immediate sql_stmt;
    else
        sql_stmt := 'Create sequence HIBERNATE_SEQUENCE start with ' || l_new_seq || ' increment by 1 MINVALUE 1 MAXVALUE 9999999999999999999999999999 CACHE 20 NOORDER';
        dbms_output.put_line(sql_stmt);
        execute immediate sql_stmt;
    end if;
END;
/

-- Changeset src/main/resources/config/liquibase/changelog/20180613142000_1.1.0-delta.xml::EDELIVERY-2009_delete_sequences::gautifr
DROP SEQUENCE ETX_ATT_SQ;

DROP SEQUENCE ETX_ERR_SQ;

DROP SEQUENCE ETX_MSG_SQ;

DROP SEQUENCE ETX_PAR_SQ;

DROP SEQUENCE ETX_USR_SQ;


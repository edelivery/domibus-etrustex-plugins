CREATE ALIAS to_date FOR "eu.europa.ec.etrustex.node.testutils.SqlFunctions.toDate";
ALTER TABLE ETX_MESSAGE     ALTER COLUMN MSG_CREATED_ON SET DEFAULT CURRENT_DATE;
ALTER TABLE ETX_PARTY       ALTER COLUMN PAR_CREATED_ON SET DEFAULT CURRENT_DATE;
ALTER TABLE ETX_USER        ALTER COLUMN USR_CREATED_ON SET DEFAULT CURRENT_DATE;
ALTER TABLE ETX_ATTACHMENT  ALTER COLUMN ATT_CREATED_ON SET DEFAULT CURRENT_DATE;

Insert into ETX_USER (USR_ID,USR_NAME,USR_PASSWORD,USR_CREATED_ON,USR_UPDATED_ON,USR_NODE_SYS) values (1001,'USER_1','USER_1',to_date('02-MAR-17','DD-MON-RR'),null,0);
Insert into ETX_USER (USR_ID,USR_NAME,USR_PASSWORD,USR_CREATED_ON,USR_UPDATED_ON,USR_NODE_SYS) values (1002,'USER_2','USER_2',to_date('14-MAR-17','DD-MON-RR'),null,0);
Insert into ETX_USER (USR_ID,USR_NAME,USR_PASSWORD,USR_CREATED_ON,USR_UPDATED_ON,USR_NODE_SYS) values (1003,'USER_3','USER_2',to_date('30-JAN-18','DD-MON-RR'),null,0);

Insert into ETX_PARTY (PAR_ID,PAR_UUID,PAR_CREATED_ON,PAR_UPDATED_ON,USR_ID,PAR_REQUIRES_WRAPPERS) values (101,'PARTY_1',to_date('02-MAR-17','DD-MON-RR'),null,1001,1);
Insert into ETX_PARTY (PAR_ID,PAR_UUID,PAR_CREATED_ON,PAR_UPDATED_ON,USR_ID,PAR_REQUIRES_WRAPPERS) values (102,'PARTY_2',to_date('14-MAR-17','DD-MON-RR'),null,1002,1);

Insert into ETX_MESSAGE (MSG_ID,MSG_TYPE,MSG_UUID,MSG_STATE,MSG_PARENT_UUID,MSG_REF_UUID,MSG_SENDER_PAR_ID,MSG_RECEIVER_PAR_ID,MSG_DIRECTION,MSG_ISSUE_DT,ERR_ID,MSG_CREATED_ON,MSG_UPDATED_ON,DOMIBUS_MESSAGEID) values (10,'MESSAGE_BUNDLE','e08cc298-ebfd-4185-916b-8a6a90af714a','SENT_TO_BACKEND',null,'EUPL V1.2',101,102,'NODE_TO_BACKEND',to_date('16-JAN-18','DD-MON-RR'),null,to_date('16-JAN-18','DD-MON-RR'),to_date('16-JAN-18','DD-MON-RR'),'2e09ab8a-3802-4318-9de6-b2a74af8e82c@domibus.eu');

Insert into ETX_ATTACHMENT (ATT_ID,MSG_ID,ATT_UUID,ATT_TYPE,ATT_STATE,ATT_DIRECTION,ATT_FILE_NAME,ATT_FILE_PATH,ATT_FILE_SIZE,ATT_MIME_TYPE,ATT_CHECKSUM,ATT_CHECKSUM_ALGORITHM,ATT_CREATED_ON,ATT_UPDATED_ON,ERR_ID,ATT_ACTIVE_STATE,DOMIBUS_MESSAGEID) values (10,10,'FGA_0bf7b783-2f06-4e5d-82ef-e7d07f634132_1','BINARY','SENT_TO_BACKEND','NODE_TO_BACKEND','eupl1.1.-licence-en_0.pdf','C:\dev\oracle\weblogic\user_projects\domains\red_domain\conf\domibus\payloads\7748',34271,'aeolus antro luctantis','26316A5CAE50C2234CA16717C1F1CBE7B0330F7AA663BB94BEB622F1D4751634B4CA4A8CF4298974214C40353C5E6FAEE954561830871B5EFAC02776E9F925CB','SHA_512',to_date('16-JAN-18','DD-MON-RR'),to_date('16-JAN-18','DD-MON-RR'),null,1,'b3f36a7e-7077-40ae-8eed-d2af13f22f26@domibus.eu');

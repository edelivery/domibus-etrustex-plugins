package eu.europa.ec.etrustex.node.data.dao;

import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 30/01/2018
 */
public class PartyDaoIT extends AbstractDaoIT<EtxParty>{

    @Autowired
    private PartyDao partyDao;
    @Autowired
    private UserDao userDao;

    @Test
    public void findByUUID_notFound() {
        EtxParty result = partyDao.findByPartyUUID("NOT_FOUND");
        assertThat(result, is(nullValue()));
    }

    @Test
    public void findByUUID_ok() {
        EtxParty result = partyDao.findByPartyUUID("PARTY_1");
        assertThat(result, is(notNullValue()));
    }

    @Override
    protected Long getExistingId() {
        return 101L;
    }

    @Override
    protected DaoBase<EtxParty> getDao() {
        return partyDao;
    }

    @Override
    protected EtxParty getNewEntity() {
        EtxParty etxParty = new EtxParty();
        etxParty.setPartyUuid(UUID.randomUUID().toString());
        etxParty.setNodeUser(userDao.findById(1003L));
        etxParty.setRequiresWrappers(true);
        return etxParty;
    }
}
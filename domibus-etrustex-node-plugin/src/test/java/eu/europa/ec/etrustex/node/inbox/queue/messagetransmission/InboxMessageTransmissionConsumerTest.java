package eu.europa.ec.etrustex.node.inbox.queue.messagetransmission;

import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * JUnit for {@link InboxMessageTransmissionConsumer}
 *
 * @author Arun Raj, Catalin Enache
 * @version 1.0
 * @since 27/09/2017
 */
@SuppressWarnings("Duplicates")
@RunWith(JMockit.class)
public class InboxMessageTransmissionConsumerTest {

    @Injectable
    private InboxStatusMessageTransmissionNodeHandler inboxStatusMessageTransmissionNodeHandler;

    @Injectable
    private InboxBundleTransmissionNodeHandler inboxBundleTransmissionNodeHandler;

    @Injectable
    private InboxMessageTransmissionHandlerBase inboxMessageTransmissionHandlerBase;
    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Injectable
    private DomainNodePluginService domainNodePluginService;
    @Injectable
    private InboxWrapperTransmissionNodeHandler inboxWrapperTransmissionNodeHandler;

    @Tested
    private InboxMessageTransmissionConsumer inboxMessageTransmissionConsumer;

    /**
     * To test handle message on receipt of valid bundle wrapper transmission message.
     * {@link InboxBundleTransmissionNodeHandler#handleMessageTransmissionToBackend(InboxMessageTransmissionQueueMessage)} should be invoked without error.
     */
    @Test
    public void testHandleMessage_InvokeBundleHandler(final @Mocked Exception exception) throws Exception {

        final Long messageId = 10L;
        final  InboxMessageTransmissionQueueMessage message = new InboxMessageTransmissionQueueMessage(MessageType.MESSAGE_BUNDLE, MessageDirection.NODE_TO_BACKEND, messageId);
        message.setMessageTransmission(MessageTransmission.BUNDLE);

        //function under test
        inboxMessageTransmissionConsumer.handleMessage(message);

        new FullVerifications() {{

            InboxMessageTransmissionQueueMessage actualQueueMessage;
            inboxBundleTransmissionNodeHandler.handleMessageTransmissionToBackend(actualQueueMessage = withCapture());
            times = 1;
            Assert.assertEquals("message should match", message, actualQueueMessage);

            domainNodePluginService.setNodePluginDomain();
            times = 1;
            clearEtxLogKeysService.clearKeys();
            times = 1;
        }};
    }

    @Test
    public void testHandleMessage_InvokeWrapperHandler(final @Mocked Exception exception) throws Exception {

        final Long messageId = 10L;
        final  InboxMessageTransmissionQueueMessage message = new InboxMessageTransmissionQueueMessage(MessageType.MESSAGE_BUNDLE, MessageDirection.NODE_TO_BACKEND, messageId);
        message.setMessageTransmission(MessageTransmission.WRAPPER);

        //function under test
        inboxMessageTransmissionConsumer.handleMessage(message);

        new FullVerifications() {{

            InboxMessageTransmissionQueueMessage actualQueueMessage;
            inboxWrapperTransmissionNodeHandler.handleMessageTransmissionToBackend(actualQueueMessage = withCapture());
            times = 1;
            Assert.assertEquals("message should match", message, actualQueueMessage);

            inboxMessageTransmissionHandlerBase.handleError(message, withAny(exception));
            times = 0;

            domainNodePluginService.setNodePluginDomain();
            times = 1;
            clearEtxLogKeysService.clearKeys();
            times = 1;
        }};
    }

    /**
     * To test handle message on receipt of valid bundle wrapper transmission message.
     * {@link InboxBundleTransmissionNodeHandler#handleMessageTransmissionToBackend(InboxMessageTransmissionQueueMessage)} should be invoked without error.
     */
    @Test
    public void testHandleMessage_InvokeStatusMessageHandler(final @Mocked Exception exception) throws Exception {

        final Long messageId = 10L;
        final  InboxMessageTransmissionQueueMessage message = new InboxMessageTransmissionQueueMessage(MessageType.MESSAGE_STATUS, MessageDirection.NODE_TO_BACKEND, messageId);

        //function under test
        inboxMessageTransmissionConsumer.handleMessage(message);

        new FullVerifications() {{
            InboxMessageTransmissionQueueMessage actualQueueMessage;
            inboxStatusMessageTransmissionNodeHandler.handleMessageTransmissionToBackend(actualQueueMessage = withCapture());
            times = 1;
            Assert.assertEquals("message should match", message, actualQueueMessage);

            inboxMessageTransmissionHandlerBase.handleError(message, withAny(exception));
            times = 0;

            domainNodePluginService.setNodePluginDomain();
            times = 1;
            clearEtxLogKeysService.clearKeys();
            times = 1;
        }};
    }

}
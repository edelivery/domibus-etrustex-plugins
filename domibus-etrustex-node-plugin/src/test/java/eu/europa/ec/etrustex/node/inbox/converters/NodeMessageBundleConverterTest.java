package eu.europa.ec.etrustex.node.inbox.converters;

import ec.schema.xsd.commonaggregatecomponents_2.DocumentWrapperReferenceType;
import ec.schema.xsd.documentbundle_1.DocumentBundleType;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleRequest;
import eu.europa.ec.etrustex.common.util.CalendarHelper;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import eu.europa.ec.etrustex.node.data.model.attachment.ChecksumAlgorithmType;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.testutils.ReflectionUtils;
import eu.europa.ec.etrustex.node.testutils.ResourcesUtils;
import mockit.Expectations;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IssueDateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IssueTimeType;
import org.apache.commons.collections4.CollectionUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.xml.bind.DatatypeConverter;
import java.util.*;

import static eu.europa.ec.etrustex.common.util.TransformerUtils.getObjectFromXml;

/**
 * JUnit class for {@link NodeMessageBundleConverter}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 21/11/2017
 */
@RunWith(JMockit.class)
public class NodeMessageBundleConverterTest {
    private final String msgUuid = "TEST_MSG_uuid_12345";
    private final String msgParentUuid = "TEST_MSG_uuid_12345";
    private final String msgRefUuid = "TEST_MSG_uuid_12345";
    private final Calendar gcDate = GregorianCalendar.getInstance();
    private final String attUuid = "TEST_ATT_uuid_56789";
    private final DocumentWrapperReferenceType documentWrapperReferenceType = new DocumentWrapperReferenceType();
    private final List<DocumentWrapperReferenceType> documentWrapperReferenceTypeList = Collections.singletonList(documentWrapperReferenceType);
    private final EtxAttachment etxAttachment = new EtxAttachment();
    private final IssueDateType issueDateType = new IssueDateType();
    private final IssueTimeType issueTimeType = new IssueTimeType();
    private final boolean receiverPartyReqWrappers = true;


    @Before
    public void setUp() {
        DateTime jodaDate = new DateTime().withDate(gcDate.get(Calendar.YEAR), gcDate.get(Calendar.MONTH)+1, gcDate.get(Calendar.DAY_OF_MONTH));
        issueDateType.setValue(jodaDate);
        LocalTime jodaTime = new DateTime().withTime(gcDate.get(Calendar.HOUR_OF_DAY), gcDate.get(Calendar.MINUTE), gcDate.get(Calendar.SECOND), gcDate.get(Calendar.MILLISECOND)).toLocalTime();
        issueTimeType.setValue(jodaTime);
        etxAttachment.setAttachmentUuid(attUuid);
    }

    @Test
    public void testPrivateConstructor_HappyFlow() throws Exception {
        ReflectionUtils.testPrivateConstructor(NodeMessageBundleConverter.class);
    }

    @Test
    public void testToEtxMessage_HappyFlow(final @Mocked SubmitDocumentBundleRequest submitDocumentBundleRequest,
                                           final @Mocked DocumentBundleType documentBundleType) {
        
        //mock of the other converter
        new MockUp<DocumentWrapperReferenceTypeConverter>() {
            @Mock
            EtxAttachment toEtxAttachment(DocumentWrapperReferenceType documentWrapperReferenceType){
                return etxAttachment;
            }
        };

        new Expectations() {{
            submitDocumentBundleRequest.getDocumentBundle();
            times = 1;
            result = documentBundleType;

            documentBundleType.getID().getValue();
            times = 1;
            result = msgUuid;

            documentBundleType.getParentDocumentID().getValue();
            times = 1;
            result = msgParentUuid;

            documentBundleType.getUUID().getValue();
            times = 1;
            result = msgRefUuid;

            documentBundleType.getIssueDate();
            times = 2;
            result = issueDateType;

            documentBundleType.getIssueTime();
            times = 2;
            result = issueTimeType;

            documentBundleType.getDocumentWrapperReference();
            times = 2;
            result = documentWrapperReferenceTypeList;

        }};

        //tested method
        final EtxMessage etxMessage = NodeMessageBundleConverter.toEtxMessage(submitDocumentBundleRequest, receiverPartyReqWrappers);

        verifyEtxMessage(submitDocumentBundleRequest, etxMessage);

        Assert.assertEquals("hour must match", gcDate.get(Calendar.HOUR_OF_DAY), etxMessage.getIssueDateTime().get(Calendar.HOUR_OF_DAY));
        Assert.assertEquals("minute must match", gcDate.get(Calendar.MINUTE), etxMessage.getIssueDateTime().get(Calendar.MINUTE));
        Assert.assertEquals("second must match", gcDate.get(Calendar.SECOND), etxMessage.getIssueDateTime().get(Calendar.SECOND));


    }

    @Test
    public void testToEtxMessage_IssueDateOnly(final @Mocked SubmitDocumentBundleRequest submitDocumentBundleRequest,
                                           final @Mocked DocumentBundleType documentBundleType) {

        //mock of the other converter
        new MockUp<DocumentWrapperReferenceTypeConverter>() {
            @Mock
            EtxAttachment toEtxAttachment(DocumentWrapperReferenceType documentWrapperReferenceType){
                return etxAttachment;
            }
        };

        new Expectations() {{
            submitDocumentBundleRequest.getDocumentBundle();
            times = 1;
            result = documentBundleType;

            documentBundleType.getID().getValue();
            times = 1;
            result = msgUuid;

            documentBundleType.getParentDocumentID().getValue();
            times = 1;
            result = msgParentUuid;

            documentBundleType.getUUID().getValue();
            times = 1;
            result = msgRefUuid;

            documentBundleType.getIssueDate();
            times = 2;
            result = issueDateType;

            documentBundleType.getIssueTime();
            times = 1;
            result = null;

            documentBundleType.getDocumentWrapperReference();
            times = 2;
            result = documentWrapperReferenceTypeList;

        }};

        //tested method
        final EtxMessage etxMessage = NodeMessageBundleConverter.toEtxMessage(submitDocumentBundleRequest, receiverPartyReqWrappers);

        verifyEtxMessage(submitDocumentBundleRequest, etxMessage);

    }

    @Test
    public void testToEtxMessage_NullInput(final @Mocked SubmitDocumentBundleRequest submitDocumentBundleRequest) {

        new Expectations() {{
            submitDocumentBundleRequest.getDocumentBundle();
            times = 1;
            result = null;

        }};

        //tested method
        final EtxMessage etxMessage = NodeMessageBundleConverter.toEtxMessage(submitDocumentBundleRequest, receiverPartyReqWrappers);

        Assert.assertNotNull("resulting object shouldn't be null", etxMessage);
        Assert.assertNull("msg Uuid should be null", etxMessage.getMessageUuid());

    }

    @Test
    public void testToEtxMessage_WithInputData(){

        //input data from xml file
        SubmitDocumentBundleRequest submitDocumentBundleRequest = getObjectFromXml(ResourcesUtils.readResource("data/inbox/documentbundle/SubmitDocumentBundleRequest.xml")
        );

        Assert.assertNotNull("documentbundle shouldn't be null", submitDocumentBundleRequest.getDocumentBundle());
        DocumentBundleType documentBundleType = submitDocumentBundleRequest.getDocumentBundle();

        //tested method
        EtxMessage etxMessage = NodeMessageBundleConverter.toEtxMessage(submitDocumentBundleRequest, receiverPartyReqWrappers);

        //message bundle assertions
        Assert.assertEquals(documentBundleType.getID().getValue(), etxMessage.getMessageUuid());
        Assert.assertNull(etxMessage.getMessageParentUuid());
        Assert.assertEquals(documentBundleType.getUUID().getValue(), etxMessage.getMessageReferenceUuid());
        Assert.assertEquals(CalendarHelper.getCalendarFromJodaDateTime(documentBundleType.getIssueDate().getValue(),
                documentBundleType.getIssueTime().getValue()), etxMessage.getIssueDateTime());
        Assert.assertEquals(TransformerUtils.serializeObjToXML(submitDocumentBundleRequest), etxMessage.getXml());

        //attachment converter assertions
        Assert.assertTrue("at least one wrapper must exist", CollectionUtils.isNotEmpty(documentBundleType.getDocumentWrapperReference()));
        DocumentWrapperReferenceType documentWrapperReferenceType = documentBundleType.getDocumentWrapperReference().get(0);
        Assert.assertNotNull("documentWrapperReferenceType shouldn't be null", documentWrapperReferenceType);
        Assert.assertTrue("message should have 1 attachment", etxMessage.getAttachmentList() != null && etxMessage.getAttachmentList().size() == 1);

        EtxAttachment etxAttachment = etxMessage.getAttachmentList().get(0);
        Assert.assertNotNull("etxAttachment shouldn't be null", etxAttachment);

        //checking all the fields set on etx attachment
        Assert.assertEquals(documentWrapperReferenceType.getID().getValue(), etxAttachment.getAttachmentUuid());
        Assert.assertEquals(documentWrapperReferenceType.getDocumentTypeCode().getValue(), etxAttachment.getAttachmentType().name());
        Assert.assertEquals(documentWrapperReferenceType.getResourceInformationReference().getName().getValue(), etxAttachment.getFileName());
        Assert.assertTrue(Objects.equals(documentWrapperReferenceType.getResourceInformationReference().getDocumentSize().getValue().longValueExact(), etxAttachment.getFileSize()));
        Assert.assertEquals(ChecksumAlgorithmType.fromValue(documentWrapperReferenceType.getResourceInformationReference().getDocumentHashMethod().getValue()), etxAttachment.getChecksumAlgorithmType());
        Assert.assertEquals(documentWrapperReferenceType.getResourceInformationReference().getDocumentHash().getValue(), DatatypeConverter.printHexBinary(etxAttachment.getChecksum()));

    }


    private void verifyEtxMessage(SubmitDocumentBundleRequest submitDocumentBundleRequest, EtxMessage etxMessage) {
        Assert.assertNotNull("resulting object shouldn't be null", etxMessage);
        Assert.assertEquals("msg Uuid didn't match", msgUuid, etxMessage.getMessageUuid());
        Assert.assertEquals("msg parent Uuid didn't match", msgParentUuid, etxMessage.getMessageParentUuid());
        Assert.assertEquals("msg ref Uuid didn't match", msgRefUuid, etxMessage.getMessageReferenceUuid());
        Assert.assertEquals("year must match", gcDate.get(Calendar.YEAR), etxMessage.getIssueDateTime().get(Calendar.YEAR));
        Assert.assertEquals("month must match", gcDate.get(Calendar.MONTH), etxMessage.getIssueDateTime().get(Calendar.MONTH));
        Assert.assertEquals("day must match", gcDate.get(Calendar.DAY_OF_MONTH), etxMessage.getIssueDateTime().get(Calendar.DAY_OF_MONTH));
        Assert.assertEquals("xml didn't match", TransformerUtils.serializeObjToXML(submitDocumentBundleRequest), etxMessage.getXml());
        Assert.assertTrue("attachment list size should match", etxMessage.getAttachmentList() != null && etxMessage.getAttachmentList().size() == 1);
        Assert.assertEquals("attachment uuid should match", attUuid, etxMessage.getAttachmentList().get(0).getAttachmentUuid());
    }

}
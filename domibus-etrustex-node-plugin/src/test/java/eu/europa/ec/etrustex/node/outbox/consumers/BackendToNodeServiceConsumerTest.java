package eu.europa.ec.etrustex.node.outbox.consumers;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.adapters.Adapter;
import eu.europa.ec.etrustex.common.handlers.Action;
import eu.europa.ec.etrustex.common.handlers.Service;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import eu.europa.ec.etrustex.node.outbox.factory.ETrustExOperationAdapterFactory;
import eu.europa.ec.etrustex.node.testutils.ETrustExAdapterDTOBuilder;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.jms.TextMessage;

/**
 * @author François Gautier
 * @version 1.0
 * @since 09/03/2018
 */
@RunWith(JMockit.class)
public class BackendToNodeServiceConsumerTest {

    private static final String ACTION = "ACTION";
    private static final String SERVICE = "SERVICE";
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(BackendToNodeServiceConsumerTest.class);

    @Injectable
    private ETrustExOperationAdapterFactory adapterFactory;
    @Tested
    private BackendToNodeServiceConsumer nodeToBackendServiceConsumer;
    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Injectable
    private DomainNodePluginService domainNodePluginService;
    @Injectable
    private Adapter adapter;

    @Test
    public void onMessage(final @Mocked TextMessage txtMessage, final @Mocked TextMessageUtil textMessageUtil) {
        final ETrustExAdapterDTO eTrustExAdapterDTO = ETrustExAdapterDTOBuilder
                .getInstance()
                .withAction(Action.SUBMIT_APPLICATION_RESPONSE_REQUEST.getCode())
                .withService(Service.APPLICATION_RESPONSE_SERVICE.getCode())
                .build();
        new Expectations() {{
            TextMessageUtil.getETrustExAdapterDTO(txtMessage);
            times = 1;
            result = eTrustExAdapterDTO;

            adapterFactory.getOperationAdapter(Service.APPLICATION_RESPONSE_SERVICE, Action.SUBMIT_APPLICATION_RESPONSE_REQUEST);
            times = 1;
            result = adapter;
        }};

        nodeToBackendServiceConsumer.onMessage(txtMessage);

        new FullVerifications() {{
            adapter.performOperation(eTrustExAdapterDTO);
            times = 1;
            domainNodePluginService.setNodePluginDomain();
            times = 1;
            clearEtxLogKeysService.clearKeys();
            times = 1;
        }};
    }
}
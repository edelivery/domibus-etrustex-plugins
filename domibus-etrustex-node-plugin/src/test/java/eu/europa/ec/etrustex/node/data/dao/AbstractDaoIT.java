package eu.europa.ec.etrustex.node.data.dao;

import eu.europa.ec.etrustex.node.AbstractSpringContextIT;
import eu.europa.ec.etrustex.node.data.model.AbstractEntity;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 29/01/2018
 */
public abstract class AbstractDaoIT<T extends AbstractEntity<Long>> extends AbstractSpringContextIT{

    @Test
    @Transactional
    public void save() {
        T newEntity = getNewEntity();
        getDao().save(newEntity);
        assertThat(newEntity.getId(), is(notNullValue()));
    }

    @Test
    public void findById() {
        T byId = getDao().findById(getExistingId());
        assertThat(byId, is(notNullValue()));
    }

    @Test
    @Transactional
    public void update() {
        T newEntity = getNewEntity();
        newEntity.setId(getExistingId());
        T updated = getDao().update(newEntity);
        assertThat(updated, is(notNullValue()));
    }

    /**
     * To implement this method, find a valid entity in the file
     * data/database/base_test_data.sql
     *
     * @return id of a valid entity
     */
    protected abstract Long getExistingId();

    protected abstract DaoBase<T> getDao();

    protected abstract T getNewEntity();
}


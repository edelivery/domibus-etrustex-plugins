package eu.europa.ec.etrustex.node.outbox.factory;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.handlers.Action;
import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.handlers.Service;
import eu.europa.ec.etrustex.node.inbox.AcknowledgementBundleReceivedHandler;
import eu.europa.ec.etrustex.node.outbox.handlers.RetrieveICAHandler;
import eu.europa.ec.etrustex.node.outbox.handlers.SendMessageStatusHandler;
import eu.europa.ec.etrustex.node.outbox.handlers.StoreDocumentWrapperHandler;
import eu.europa.ec.etrustex.node.outbox.handlers.SubmitDocumentBundleHandler;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.context.ApplicationContext;

import java.util.Arrays;
import java.util.Collection;

import static eu.europa.ec.etrustex.common.handlers.Action.UNKNOWN;
import static eu.europa.ec.etrustex.common.handlers.Action.*;
import static eu.europa.ec.etrustex.common.handlers.Service.*;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 08-Dec-17
 */
@RunWith(Parameterized.class)
public class ETrustExOperationHandlerFactoryTest {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ETrustExOperationHandlerFactoryTest.class);

    @Injectable
    private ApplicationContext context;

    @Tested
    private ETrustExOperationHandlerFactory handler;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Parameter
    public Service service;
    @Parameter(1)
    public Action action;
    @Parameter(2)
    public String handlerName;
    @Parameter(3)
    public Class<? extends OperationHandler> clazz;
    @Parameter(4)
    public Class<? extends Exception> expectedException;

    @Parameters( name = "{index}: {0} | {1}" )
    public static Collection combinations() {
        return Arrays.asList(new Object[][]{
                // Happy flow scenario
                {BUNDLE_TRANSMISSION_SERVICE, NOTIFY_BUNDLE_TO_BACKEND_RESPONSE, "etxNodePluginAckHandler", AcknowledgementBundleReceivedHandler.class, null},
                {APPLICATION_RESPONSE_SERVICE, SUBMIT_APPLICATION_RESPONSE_REQUEST, "etxNodePluginSMSHandler", SendMessageStatusHandler.class, null},
                {DOCUMENT_BUNDLE_SERVICE, SUBMIT_DOCUMENT_BUNDLE_REQUEST, "etxNodePluginSDBHandler", SubmitDocumentBundleHandler.class, null},
                {DOCUMENT_WRAPPER_SERVICE, STORE_DOCUMENT_WRAPPER_REQUEST, "etxNodePluginSDWHandler", StoreDocumentWrapperHandler.class, null},
                {RETRIEVE_ICA_SERVICE, RETRIEVE_ICA_REQUEST, "etxNodePluginRICAHandler", RetrieveICAHandler.class, null},
                // returns null
                {BUNDLE_TRANSMISSION_SERVICE, UNKNOWN, "", OperationHandler.class, null},
                {APPLICATION_RESPONSE_SERVICE, UNKNOWN, "", OperationHandler.class, null},
                {DOCUMENT_BUNDLE_SERVICE, UNKNOWN, "", OperationHandler.class, null},
                {DOCUMENT_WRAPPER_SERVICE, UNKNOWN, "", OperationHandler.class, null},
                {RETRIEVE_ICA_SERVICE, UNKNOWN, "", OperationHandler.class, null},

                {Service.UNKNOWN, UNKNOWN, "", OperationHandler.class, null},
                //exception
                {null, UNKNOWN, "", OperationHandler.class, IllegalArgumentException.class},
                {Service.UNKNOWN, null, "", OperationHandler.class, IllegalArgumentException.class},
        });
    }

    @Test
    public void getOperator() throws Exception {
        LOG.info("Parametrized Number is : " + service + " " + action + " " + handlerName);

        if (StringUtils.isNotBlank(handlerName)) {
            LOG.info("");
            new Expectations() {{
                context.getBean(handlerName);
                times = 1;
                result = clazz.newInstance();
            }};
        }
        if (expectedException != null) {
            thrown.expect(expectedException);
            thrown.expectMessage(CoreMatchers.containsString(" is missing"));
        }

        OperationHandler operationHandler = handler.getOperationHandler(service, action);

        new FullVerifications() {{
        }};

        if (StringUtils.isNotBlank(handlerName) && clazz != null) {
            assertThat(operationHandler, notNullValue());
        } else {
            assertThat(operationHandler, nullValue());
        }
    }

}
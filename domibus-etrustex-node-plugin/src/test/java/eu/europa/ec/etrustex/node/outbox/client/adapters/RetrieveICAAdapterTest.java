package eu.europa.ec.etrustex.node.outbox.client.adapters;

import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.schema.xsd.commonaggregatecomponents_2.InterchangeAgreementType;
import ec.schema.xsd.retrieveinterchangeagreementsresponse_2.RetrieveInterchangeAgreementsResponseType;
import ec.services.wsdl.retrieveinterchangeagreementsrequest_2.FaultResponse;
import ec.services.wsdl.retrieveinterchangeagreementsrequest_2.SubmitRetrieveInterchangeAgreementsRequestRequest;
import ec.services.wsdl.retrieveinterchangeagreementsrequest_2.SubmitRetrieveInterchangeAgreementsRequestResponse;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.jms.TextMessageCreator;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import eu.europa.ec.etrustex.node.data.dao.PartyDao;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.testutils.FaultResponseUtils;
import eu.europa.ec.etrustex.node.testutils.PayloadDTOBuilder;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PartyIdentificationType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PartyType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.jms.core.JmsOperations;

import javax.xml.ws.Holder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_ID_GENERIC;
import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_ID_HEADER;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.deserializeXMLToObj;
import static eu.europa.ec.etrustex.node.testutils.ETrustExAdapterDTOBuilder.getEmpty;
import static eu.europa.ec.etrustex.node.testutils.ETrustExPluginPropertiesTestConstants.TEST_AS4_ACTION_RETRIEVEICA_RESPONSE;
import static eu.europa.ec.etrustex.node.testutils.ReflectionUtils.getPrivateField;
import static eu.europa.ec.etrustex.node.testutils.ResourcesUtils.readResource;
import static org.apache.commons.codec.binary.StringUtils.getBytesUtf8;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 20-Nov-17
 */
public class RetrieveICAAdapterTest extends BaseAdapterTest {

    @Injectable
    private ETrustExNodePluginProperties eTrustExNodePluginProperties;
    @Injectable
    private JmsOperations etxNodePluginNodeToBackendRICAJmsTemplate;
    @Injectable
    private PartyDao partyDao;

    @Tested
    private RetrieveICAAdapter retrieveICAAdapter;

    @Test
    public void performOperation_noHeaderPayload() {
        try {
            retrieveICAAdapter.performOperation(getEmpty());
            Assert.fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getError(), is(ETrustExError.DEFAULT));
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void performOperation_noPayload() {
        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(CONTENT_ID_HEADER.getValue())
                .withPayload(HEADER)
                .build());
        try {
            retrieveICAAdapter.performOperation(dto);
            Assert.fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getError(), is(ETrustExError.DEFAULT));
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void performOperation_acknowledgeOk_noPartyCreated() throws Exception {
        final List<SubmitRetrieveInterchangeAgreementsRequestRequest> requests = new ArrayList<>();
        final List<Holder<HeaderType>> headers = new ArrayList<>();

        new Expectations() {{
            riaRequestPortType.submitRetrieveInterchangeAgreementsRequest(withCapture(requests), withCapture(headers));
            times = 1;
            result = getResult();
        }};

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(CONTENT_ID_HEADER.getValue())
                .withPayload(getBytesUtf8(readResource("data/ica/Header.xml")))
                .build());
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(CONTENT_ID_GENERIC.getValue())
                .withPayload(getBytesUtf8(readResource("data/ica/retrieveInterchangeAgreementsRequest.xml")))
                .build());

        retrieveICAAdapter.performOperation(dto);

        new FullVerifications() {{
            TextMessageCreator messageCreator;
            etxNodePluginNodeToBackendRICAJmsTemplate.send(messageCreator = withCapture());
            times = 1;

            ETrustExAdapterDTO fromTxtMessageCreator =
                    (ETrustExAdapterDTO) deserializeXMLToObj(
                            (String) getPrivateField(TextMessageCreator.class, messageCreator, "dto"));
            assertThat(fromTxtMessageCreator.getAs4Action(), is(TEST_AS4_ACTION_RETRIEVEICA_RESPONSE));

            assertThat(fromTxtMessageCreator,
                    sameBeanAs((ETrustExAdapterDTO) deserializeXMLToObj(readResource("data/outbox/ica/EtrustExAdapterDTO.xml")))
                            .ignoring("as4Payloads")
            );
        }};

        assertThat(requests.size(), is(1));
        assertThat(headers.size(), is(1));
    }

    @Test
    public void performOperation_acknowledgeOk_createParty() throws Exception {
        final List<SubmitRetrieveInterchangeAgreementsRequestRequest> requests = new ArrayList<>();
        final List<Holder<HeaderType>> headers = new ArrayList<>();
        InterchangeAgreementType interchangeAgreementType1 = getInterchangeAgreementType("SENDER1", "RECEIVER1");
        InterchangeAgreementType interchangeAgreementType2 = getInterchangeAgreementType("SENDER2", "RECEIVER12");

        new Expectations() {{
            riaRequestPortType.submitRetrieveInterchangeAgreementsRequest(withCapture(requests), withCapture(headers));
            times = 1;
            result = getResult(Arrays.asList(interchangeAgreementType1, interchangeAgreementType2));

            partyDao.findByPartyUUID("SENDER1");
            times = 1;
            result = new EtxParty();

            partyDao.findByPartyUUID("RECEIVER1");
            times = 1;
            result = null;

            partyDao.findByPartyUUID("SENDER2");
            times = 1;
            result = null;

            partyDao.findByPartyUUID("RECEIVER12");
            times = 1;
            result = null;
        }};

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(CONTENT_ID_HEADER.getValue())
                .withPayload(getBytesUtf8(readResource("data/ica/Header.xml")))
                .build());
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(CONTENT_ID_GENERIC.getValue())
                .withPayload(getBytesUtf8(readResource("data/ica/retrieveInterchangeAgreementsRequest.xml")))
                .build());

        retrieveICAAdapter.performOperation(dto);

        new FullVerifications() {{
            TextMessageCreator messageCreator;
            etxNodePluginNodeToBackendRICAJmsTemplate.send(messageCreator = withCapture());
            times = 1;

            ETrustExAdapterDTO fromTxtMessageCreator =
                    (ETrustExAdapterDTO) deserializeXMLToObj(
                            (String) getPrivateField(TextMessageCreator.class, messageCreator, "dto"));
            assertThat(fromTxtMessageCreator.getAs4Action(), is(TEST_AS4_ACTION_RETRIEVEICA_RESPONSE));

            assertThat(fromTxtMessageCreator,
                    sameBeanAs((ETrustExAdapterDTO) deserializeXMLToObj(readResource("data/outbox/ica/EtrustExAdapterDTO.xml")))
                            .ignoring("as4Payloads")
            );

            partyDao.save(withAny(new EtxParty()));
            times = 3;
        }};

        assertThat(requests.size(), is(1));
        assertThat(headers.size(), is(1));
    }

    private InterchangeAgreementType getInterchangeAgreementType(String senderUuid, String receiverUuid) {
        InterchangeAgreementType interchangeAgreementType = new InterchangeAgreementType();
        interchangeAgreementType.setSenderParty(getPartyType(senderUuid));
        interchangeAgreementType.setReceiverParty(getPartyType(receiverUuid));
        return interchangeAgreementType;
    }

    private PartyType getPartyType(String uuid) {
        PartyType partyType = new PartyType();
        PartyIdentificationType e = new PartyIdentificationType();
        IDType value = new IDType();
        value.setValue(uuid);
        e.setID(value);
        partyType.getPartyIdentification().add(e);
        return partyType;
    }

    @Test
    public void performOperation_acknowledgeFault() throws Exception {
        final List<SubmitRetrieveInterchangeAgreementsRequestRequest> requests = new ArrayList<>();
        final List<Holder<HeaderType>> headers = new ArrayList<>();

        new Expectations() {{
            riaRequestPortType.submitRetrieveInterchangeAgreementsRequest(withCapture(requests), withCapture(headers));
            times = 1;
            result = new FaultResponse("ERROR", FaultResponseUtils.getFaultType());
        }};

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(CONTENT_ID_HEADER.getValue())
                .withPayload(HEADER)
                .build());
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(CONTENT_ID_GENERIC.getValue())
                .withPayload(getBytesUtf8(readResource("data/ica/retrieveInterchangeAgreementsRequest.xml")))
                .build());

        retrieveICAAdapter.performOperation(dto);

        new FullVerifications() {{
            TextMessageCreator messageCreator;
            etxNodePluginNodeToBackendRICAJmsTemplate.send(messageCreator = withCapture());
            times = 1;

            ETrustExAdapterDTO fromTxtMessageCreator =
                    (ETrustExAdapterDTO) deserializeXMLToObj(
                            (String) getPrivateField(TextMessageCreator.class, messageCreator, "dto"));
            assertThat(fromTxtMessageCreator.getAs4Action(), is(TEST_AS4_ACTION_RETRIEVEICA_RESPONSE));
            assertThat(fromTxtMessageCreator,
                    sameBeanAs((ETrustExAdapterDTO) deserializeXMLToObj(readResource("data/outbox/ica/FaultResponse.xml")))
                            .ignoring("as4Payloads")
            );
        }};

        assertThat(requests.size(), is(1));
        assertThat(headers.size(), is(1));
    }

    @Test
    public void performOperation_handleFaultDto() throws Exception {
        final List<SubmitRetrieveInterchangeAgreementsRequestRequest> requests = new ArrayList<>();
        final List<Holder<HeaderType>> headers = new ArrayList<>();

        new Expectations() {{
            riaRequestPortType.submitRetrieveInterchangeAgreementsRequest(withCapture(requests), withCapture(headers));
            times = 1;
            result = new Exception();
        }};

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(CONTENT_ID_HEADER.getValue())
                .withPayload(HEADER)
                .build());
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(CONTENT_ID_GENERIC.getValue())
                .withPayload(getBytesUtf8(readResource("data/ica/retrieveInterchangeAgreementsRequest.xml")))
                .build());

        retrieveICAAdapter.performOperation(dto);

        new FullVerifications() {{
            TextMessageCreator messageCreator;
            etxNodePluginBackendToNodeJmsTemplate.send(messageCreator = withCapture());
            times = 1;

            ETrustExAdapterDTO fromTxtMessageCreator =
                    (ETrustExAdapterDTO) deserializeXMLToObj(
                            (String) getPrivateField(TextMessageCreator.class, messageCreator, "dto"));
            // Warning, the stacktrace is not being checked here!
            assertThat(fromTxtMessageCreator,
                    sameBeanAs((ETrustExAdapterDTO) deserializeXMLToObj(readResource("data/outbox/ica/Exception.xml")))
                            .ignoring("as4Payloads")
            );
        }};

        assertThat(requests.size(), is(1));
        assertThat(headers.size(), is(1));
    }

    private SubmitRetrieveInterchangeAgreementsRequestResponse getResult() {
        return getResult(new ArrayList<>());
    }

    private SubmitRetrieveInterchangeAgreementsRequestResponse getResult(List<InterchangeAgreementType> c) {
        SubmitRetrieveInterchangeAgreementsRequestResponse submitRetrieveInterchangeAgreementsRequestResponse = new SubmitRetrieveInterchangeAgreementsRequestResponse();
        RetrieveInterchangeAgreementsResponseType value = new RetrieveInterchangeAgreementsResponseType();
        value.getInterchangeAgreement().addAll(c);
        submitRetrieveInterchangeAgreementsRequestResponse.setRetrieveInterchangeAgreementsResponse(value);
        return submitRetrieveInterchangeAgreementsRequestResponse;
    }
}
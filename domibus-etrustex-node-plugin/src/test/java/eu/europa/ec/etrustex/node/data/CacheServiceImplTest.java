package eu.europa.ec.etrustex.node.data;

import eu.europa.ec.etrustex.common.service.CacheService;
import eu.europa.ec.etrustex.common.service.CacheServiceImpl;
import eu.europa.ec.etrustex.node.AbstractSpringContextIT;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.jcache.JCacheCacheManager;
import org.springframework.core.io.ClassPathResource;

import javax.cache.Caching;
import javax.cache.spi.CachingProvider;
import java.io.File;
import java.io.IOException;

/**
 * @author Federico Martini
 * @version 1.0
 * @since 19/01/2017
 */
public class CacheServiceImplTest extends AbstractSpringContextIT {

    private final static String NODE_CACHE_MANAGER = "etxNodePlugin";

    private final static String CACHE_NAME = "eu.europa.ec.etrustex.node.data.model.administration.EtxParty";

    @Autowired
    private CacheService cacheService;
    @Autowired
    private CacheManager cacheManager;

    @Test
    public void testClearCache_HappyFlow() {
        Assert.assertNotNull("cache manager should be present", cacheManager);
        //Assert.assertEquals("cache manager must match the one from config file", cacheManager.getName(), NODE_CACHE_MANAGER);

        Cache cache = cacheManager.getCache(CACHE_NAME);
        Assert.assertNotNull("cache named " + CACHE_NAME + " should be present", cache);

        //put a new element
        cache.put("key1", new EtxParty());

        Assert.assertNotNull("cache should have size > 0", cache.get("key1"));

        //do the thing
        cacheService.clearCache();

        Assert.assertNull("cache named " + CACHE_NAME + " should have been cleared", cache.get("key1"));

    }

}
package eu.europa.ec.etrustex.node.testutils;

import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseCodeType;
import oasis.names.specification.ubl.schema.xsd.fault_1.FaultType;

/**
 * @author François Gautier
 * @version 1.0
 * @since 15/02/2018
 */
public class FaultResponseUtils {

    private static final String FAULT_CODE = "FaultCode";

    private FaultResponseUtils() {
    }

    public static FaultType getFaultType() {
        FaultType faultInfo = new FaultType();
        DescriptionType e = new DescriptionType();
        e.setValue("Error description");
        faultInfo.getDescription().add(e);
        faultInfo.setResponseCode(getResponseCodeType());
        return faultInfo;
    }

    private static ResponseCodeType getResponseCodeType() {
        ResponseCodeType responseCodeType = new ResponseCodeType();
        responseCodeType.setValue(FAULT_CODE);
        return responseCodeType;
    }
}

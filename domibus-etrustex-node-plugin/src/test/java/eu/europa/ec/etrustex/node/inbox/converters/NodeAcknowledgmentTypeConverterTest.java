package eu.europa.ec.etrustex.node.inbox.converters;

import ec.schema.xsd.ack_2.AcknowledgmentType;
import ec.schema.xsd.commonaggregatecomponents_2.AcknowledgedDocumentReferenceType;
import eu.europa.ec.etrustex.node.testutils.ReflectionUtils;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.DocumentReferenceType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PartyType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DocumentTypeCodeType;
import org.apache.commons.collections4.CollectionUtils;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;

/**
 * JUnit for {@link NodeAcknowledgmentTypeConverter}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 27/11/2017
 */
public class NodeAcknowledgmentTypeConverterTest {

    @Test
    public void testPrivateConstructor_HappyFlow() throws Exception {
        ReflectionUtils.testPrivateConstructor(NodeAcknowledgmentTypeConverter.class);
    }

    @Test
    public void testBuildAcknowledgmentType_HappyFlow() throws Exception {

        DateTime issueDate = DateTime.now();

        String documentReferenceId = "TEST_doc_REF_ID";
        String documentTypeCode = "TEST_doc_type";

        final String senderPartyUuid = "TEST_EGREFFE_NP_PARTY_B4";
        final String receiverPartyUuid = "TEST_EDMA_AP_I2C6_PARTY";

        //tested method
        AcknowledgmentType acknowledgmentType = NodeAcknowledgmentTypeConverter.buildAcknowledgmentType(issueDate, documentReferenceId,
                documentTypeCode, senderPartyUuid, receiverPartyUuid);

        Assert.assertNotNull("resulting object shouldn't be null", acknowledgmentType);
        Assert.assertTrue("ackIndicator should be true", acknowledgmentType.getAckIndicator().isValue());
        Assert.assertEquals("issue date should match", issueDate, acknowledgmentType.getIssueDate().getValue());
        Assert.assertNotNull(acknowledgmentType.getAcknowledgedDocumentReference());
        final AcknowledgedDocumentReferenceType acknowledgedDocumentReferenceType = acknowledgmentType.getAcknowledgedDocumentReference();
        Assert.assertNotNull(acknowledgedDocumentReferenceType.getDocumentReference());

        final DocumentReferenceType documentReferenceType = acknowledgedDocumentReferenceType.getDocumentReference();

        Assert.assertEquals("document ref id should match", documentReferenceId, documentReferenceType.getID().getValue());
        Assert.assertNotNull(documentReferenceType.getDocumentTypeCode());
        final DocumentTypeCodeType documentTypeCodeType = documentReferenceType.getDocumentTypeCode();
        Assert.assertEquals("document type code should match", documentTypeCode, documentTypeCodeType.getValue());

        Assert.assertNotNull(acknowledgedDocumentReferenceType.getSenderParty());
        final PartyType senderParty = acknowledgedDocumentReferenceType.getSenderParty();
        Assert.assertNotNull(senderParty.getEndpointID());
        Assert.assertEquals("sender party uuid should match", senderPartyUuid, senderParty.getEndpointID().getValue());

        Assert.assertTrue(CollectionUtils.isNotEmpty(acknowledgedDocumentReferenceType.getReceiverParty()));
        final PartyType receiverParty = acknowledgedDocumentReferenceType.getReceiverParty().get(0);
        Assert.assertNotNull(receiverParty.getEndpointID());
        Assert.assertEquals("receiver party uuid should match", senderPartyUuid, senderParty.getEndpointID().getValue());
    }

}
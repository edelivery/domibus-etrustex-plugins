package eu.europa.ec.etrustex.node;

import eu.domibus.messaging.MessageNotFoundException;
import eu.domibus.plugin.MessageLister;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;

import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.Collection;

public class WeblogicNotificationListenerService implements MessageListener, JmsListenerConfigurer, MessageLister {
    @Override
    public Collection<String> listPendingMessages() {
        return null;
    }

    @Override
    public void removeFromPending(String s) throws MessageNotFoundException {

    }

    @Override
    public void onMessage(Message message) {

    }

    @Override
    public void configureJmsListeners(JmsListenerEndpointRegistrar registrar) {

    }
}

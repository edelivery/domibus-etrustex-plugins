package eu.europa.ec.etrustex.node.testutils;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * @author François Gautier
 * @version 1.0
 * @since 26-Sep-17
 */
public class ResourcesUtils {
    private ResourcesUtils() {
    }

    /**
     * loads file from /src/test/resources folder
     *
     * @param fileName name of the file
     * @return String of the content of the file in UTF_8
     * @throws IllegalArgumentException for IO exception
     */
    public static String readResource(final String fileName) {
        try {
            return IOUtils.toString(new FileInputStream(ResourcesUtils.class.getClassLoader().getResource(fileName).getPath()), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Allows to get the InputStream of a file.
     *
     * @param fileName name of the file
     * @return the InputStream attached to the File object
     * @throws IllegalArgumentException for IO exception
     */
    public static InputStream getFileInputStream(final String fileName) {
        try {
            return new FileInputStream(new File("src/test/resources/" + fileName));
        } catch (FileNotFoundException ioEx) {
            throw new IllegalArgumentException("File not found", ioEx);
        }
    }
}

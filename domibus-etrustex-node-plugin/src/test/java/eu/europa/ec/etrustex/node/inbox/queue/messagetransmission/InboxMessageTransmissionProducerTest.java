package eu.europa.ec.etrustex.node.inbox.queue.messagetransmission;

import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.apache.commons.lang3.RandomUtils;
import org.hamcrest.core.IsNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.Queue;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 08-Dec-17
 */
@RunWith(JMockit.class)
public class InboxMessageTransmissionProducerTest {
    @Injectable
    private Queue etxNodePluginInboxMessageTransmissionQueue;

    @Injectable
    private JmsTemplate inboxMessageTransmissionJmsTemplate;
    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Injectable
    private DomainNodePluginService domainNodePluginService;
    @Tested
    private InboxMessageTransmissionProducer inboxMessageTransmissionProducer;
    private long messageId;
    private long attachmentId;

    @Before
    public void setUp() throws Exception {
        messageId = RandomUtils.nextLong(0, 99999);
        attachmentId = RandomUtils.nextLong(0, 99999);
    }

    @Test
    public void triggerBundleTransmissionToBackend() throws Exception {
        inboxMessageTransmissionProducer.triggerBundleTransmissionToBackend(messageId);

        new FullVerifications() {{
            InboxMessageTransmissionQueueMessage message;
            inboxMessageTransmissionJmsTemplate.convertAndSend(etxNodePluginInboxMessageTransmissionQueue, message = withCapture());
            times = 1;

            assertThat(message.getMessageType(), is(MessageType.MESSAGE_BUNDLE));
            assertThat(message.getMessageTransmission(), is(MessageTransmission.BUNDLE));
            assertThat(message.getMessageDirection(), is(MessageDirection.NODE_TO_BACKEND));
            assertThat(message.getMessageId(), is(messageId));
            assertThat(message.getAttachmentId(), IsNull.nullValue());
            assertThat(message.getErrorCode(), IsNull.nullValue());
            assertThat(message.getErrorDescription(), IsNull.nullValue());

            domainNodePluginService.setNodePluginDomain();
            times = 1;
            clearEtxLogKeysService.clearKeys();
            times = 1;
        }};
    }

    @Test
    public void triggerWrapperTransmissionToBackend() throws Exception {
        inboxMessageTransmissionProducer.triggerWrapperTransmissionToBackend(messageId, attachmentId);

        new FullVerifications() {{
            InboxMessageTransmissionQueueMessage message;
            inboxMessageTransmissionJmsTemplate.convertAndSend(etxNodePluginInboxMessageTransmissionQueue, message = withCapture());
            times = 1;

            assertThat(message.getMessageType(), is(MessageType.MESSAGE_BUNDLE));
            assertThat(message.getMessageTransmission(), is(MessageTransmission.WRAPPER));
            assertThat(message.getMessageDirection(), is(MessageDirection.NODE_TO_BACKEND));
            assertThat(message.getMessageId(), is(messageId));
            assertThat(message.getAttachmentId(), is(attachmentId));
            assertThat(message.getErrorCode(), IsNull.nullValue());
            assertThat(message.getErrorDescription(), IsNull.nullValue());
        }};
    }

    @Test
    public void triggerMessageTransmissionToBackend() throws Exception {
        inboxMessageTransmissionProducer.triggerMessageTransmissionToBackend(messageId);

        new FullVerifications() {{
            InboxMessageTransmissionQueueMessage message;
            inboxMessageTransmissionJmsTemplate.convertAndSend(etxNodePluginInboxMessageTransmissionQueue, message = withCapture());
            times = 1;

            assertThat(message.getMessageType(), is(MessageType.MESSAGE_STATUS));
            assertThat(message.getMessageTransmission(), is(MessageTransmission.BUNDLE));
            assertThat(message.getMessageDirection(), is(MessageDirection.NODE_TO_BACKEND));
            assertThat(message.getMessageId(), is(messageId));
            assertThat(message.getAttachmentId(), IsNull.nullValue());
            assertThat(message.getErrorCode(), IsNull.nullValue());
            assertThat(message.getErrorDescription(), IsNull.nullValue());
        }};
    }

    @Test
    public void triggerMessageWithError() throws Exception {
        final InboxMessageTransmissionQueueMessage messageInitial = new InboxMessageTransmissionQueueMessage();
        inboxMessageTransmissionProducer.triggerMessageWithError(messageInitial);

        new FullVerifications() {{
            InboxMessageTransmissionQueueMessage message;
            inboxMessageTransmissionJmsTemplate.convertAndSend(etxNodePluginInboxMessageTransmissionQueue, message = withCapture());
            times = 1;

            assertThat(message, is(messageInitial));
        }};
    }
}
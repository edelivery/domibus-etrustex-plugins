package eu.europa.ec.etrustex.node;

import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainTaskExecutor;
import eu.domibus.ext.delegate.converter.DomainExtConverter;
import eu.domibus.ext.delegate.services.multitenancy.DomainTaskExecutorExtDelegate;
import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.services.DomainContextExtService;
import eu.domibus.ext.services.DomainExtService;
import eu.domibus.ext.services.DomibusPropertyExtService;
import eu.europa.ec.etrustex.common.service.CacheService;
import eu.europa.ec.etrustex.common.service.CacheServiceImpl;
import eu.europa.ec.etrustex.common.service.WorkspaceService;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import eu.europa.ec.etrustex.node.service.NodeRuntimeManagerImpl;
import eu.europa.ec.etrustex.node.service.WorkspaceServiceNodeImpl;
import eu.europa.ec.etrustex.node.testutils.h2.InMemoryDataBaseConfig;
import org.apache.commons.io.FileUtils;
import org.junit.runner.RunWith;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.jcache.JCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.cache.Caching;
import javax.cache.spi.CachingProvider;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * @author François Gautier
 * @version 1.0
 * @since 05/02/2018
 * <p>
 * Extends this class to access to an InMemory Database and Spring Beans (to be defined here)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        InMemoryDataBaseConfig.class,
        AbstractSpringContextIT.ConfigPlugin.class,
        AbstractSpringContextIT.ConfigDomibus.class})
@WebAppConfiguration(value = "src/test/resources")
@EnableWebMvc
@EnableCaching
public abstract class AbstractSpringContextIT {

    @Configuration
    @ComponentScan(basePackages = {
            "eu.europa.ec.etrustex.node.web",
            "eu.europa.ec.etrustex.node.data.dao",
            "eu.europa.ec.etrustex.node.data.model",
            "eu.europa.ec.etrustex.common.util",
            "eu.europa.ec.etrustex.common.service"})
    static class ConfigPlugin {
        @Bean
        public NodeRuntimeManagerImpl runtimeManager() {
            return new NodeRuntimeManagerImpl();
        }

        @Bean
        public WorkspaceService etxNodePluginWorkspaceService() {
            return new WorkspaceServiceNodeImpl();
        }

        @Bean
        public ETrustExNodePluginProperties eTrustExPluginProperties() {
            return new ETrustExNodePluginProperties(){
                public String getDownloadLocation(){
                    return "target/downloadLocation";
                }
            };
        }
        @Bean
        public Properties eTrustExPluginCommonAS4Properties() {
            return new Properties();
        }

        @Bean
        public CacheService cacheService(CacheManager cacheManager) {
            return new CacheServiceImpl(cacheManager);
        }

        @Bean
        public Properties eTrustExNodeProperties() {
            Properties properties = new Properties();
            properties.setProperty("plugin.payloads.download.location", "./target/payload");
            try {
                FileUtils.forceMkdir(new File("./target/payload"));
            } catch (IOException e) {
                throw new IllegalStateException("Payload folder not create (./target/payload)");
            }
            return properties;
        }
    }

    @Configuration
    static class ConfigDomibus {

        @Bean("cacheManager")
        public org.springframework.cache.CacheManager ehCacheManager() throws Exception {
            CachingProvider provider = Caching.getCachingProvider();
            ClassLoader classLoader = getClass().getClassLoader();

            //default cache
            final ClassPathResource classPathResource = new ClassPathResource("etrustex-nodePlugin-ehcache-test.xml");

            javax.cache.CacheManager cacheManager = provider.getCacheManager(
                    classPathResource.getURL().toURI(),
                    classLoader);

            return new JCacheCacheManager(cacheManager);
        }

        @Bean
        public DomainExtService domainExtService() {
            return new DomainExtService() {
                @Override
                public DomainDTO getDomainForScheduler(String s) {
                    return new DomainDTO(s, s);
                }

                @Override
                public DomainDTO getDomain(String s) {
                    return new DomainDTO(s, s);
                }
            };
        }
        @Bean
        public DomibusPropertyExtService domibusPropertyExtService(){
            return new DomibusPropertyExtService() {
                @Override
                public String getProperty(String s) {
                    return null;
                }

                @Override
                public String getDomainProperty(DomainDTO domainDTO, String s) {
                    return null;
                }

                @Override
                public void setDomainProperty(DomainDTO domainDTO, String s, String s1) {

                }

                @Override
                public boolean containsDomainPropertyKey(DomainDTO domainDTO, String s) {
                    return false;
                }

                @Override
                public boolean containsPropertyKey(String s) {
                    return false;
                }

                @Override
                public String getDomainProperty(DomainDTO domainDTO, String s, String s1) {
                    return null;
                }

                @Override
                public String getDomainResolvedProperty(DomainDTO domainDTO, String s) {
                    return null;
                }

                @Override
                public String getResolvedProperty(String s) {
                    return null;
                }
            };
        }
        @Bean
        public DomainContextExtService domainContextExtService() {
            return new DomainContextExtService() {
                @Override
                public DomainDTO getCurrentDomain() {
                    return null;
                }

                @Override
                public DomainDTO getCurrentDomainSafely() {
                    return null;
                }

                @Override
                public void setCurrentDomain(DomainDTO domainDTO) {

                }

                @Override
                public void clearCurrentDomain() {

                }
            };
        }

        @Bean
        public DomainExtConverter domainExtConverter() {
            return new DomainExtConverter() {
                @Override
                public <T, U> T convert(U u, Class<T> aClass) {
                    return null;
                }

                @Override
                public <T, U> List<T> convert(List<U> list, Class<T> aClass) {
                    return null;
                }

                @Override
                public <T, U> Map<String, T> convert(Map<String, U> map, Class<T> aClass) {
                    return null;
                }
            };
        }

        @Bean
        public DomainTaskExecutor domainTaskExecutor() {
            return new DomainTaskExecutor() {
                @Override
                public <T> T submit(Callable<T> callable) {
                    try {
                        return callable.call();
                    } catch (Exception e) {
                        throw new IllegalStateException();
                    }
                }

                @Override
                public void submit(Runnable runnable) {
                    runnable.run();
                }

                @Override
                public Future<?> submit(Runnable runnable, boolean b) {
                    return null;
                }

                @Override
                public void submit(Runnable runnable, Runnable runnable1, File file, boolean b, Long aLong, TimeUnit timeUnit) {
                    runnable.run();
                    runnable1.run();
                }

                @Override
                public void submit(Runnable runnable, Runnable runnable1, File file) {
                    runnable.run();
                    runnable1.run();
                }

                @Override
                public void submit(Runnable runnable, Domain domain) {
                    runnable.run();
                }

                @Override
                public void submit(Runnable runnable, Domain domain, boolean b, Long aLong, TimeUnit timeUnit) {
                    runnable.run();
                }

                @Override
                public void submitLongRunningTask(Runnable runnable, Runnable runnable1, Domain domain) {
                    runnable.run();
                    runnable1.run();
                }

                @Override
                public void submitLongRunningTask(Runnable runnable, Domain domain) {
                    runnable.run();
                }
            };
        }

        @Bean
        public DomainTaskExecutorExtDelegate domainTaskExecutorExtDelegate() {
            return new DomainTaskExecutorExtDelegate();
        }

        @Bean
        public Executor taskExecutor() {
            return Runnable::run;
        }

        @Bean
        public DomainNodePluginService domainNodePluginService(){
            return new DomainNodePluginService();
        }
    }

}

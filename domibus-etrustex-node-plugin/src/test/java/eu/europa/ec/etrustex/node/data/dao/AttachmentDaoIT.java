package eu.europa.ec.etrustex.node.data.dao;

import eu.europa.ec.etrustex.node.data.model.attachment.AttachmentDirection;
import eu.europa.ec.etrustex.node.data.model.attachment.AttachmentState;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static eu.europa.ec.etrustex.common.model.entity.attachment.AttachmentType.BINARY;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 30/01/2018
 */
public class AttachmentDaoIT extends AbstractDaoIT<EtxAttachment> {

    @Autowired
    private AttachmentDao attachmentDao;
    @Autowired
    private MessageDao messageDao;

    @Test
    public void findMessageByDomibusMessageId_ok() {
        EtxAttachment result = attachmentDao.findAttachmentByDomibusMessageId("b3f36a7e-7077-40ae-8eed-d2af13f22f26@domibus.eu");
        assertThat(result, is(notNullValue()));
    }
    @Test
    public void findMessageByDomibusMessageId_notFound() {
        EtxAttachment result = attachmentDao.findAttachmentByDomibusMessageId("NOT_FOUND");
        assertThat(result, is(nullValue()));
    }

    @Override
    protected Long getExistingId() {
        return 10L;
    }

    @Override
    protected DaoBase<EtxAttachment> getDao() {
        return attachmentDao;
    }

    @Override
    protected EtxAttachment getNewEntity() {
        EtxAttachment etxAttachment = new EtxAttachment();
        etxAttachment.setAttachmentUuid(UUID.randomUUID().toString());
        etxAttachment.setStateType(AttachmentState.CREATED);
        etxAttachment.setAttachmentType(BINARY);
        etxAttachment.setDirectionType(AttachmentDirection.NODE_TO_BACKEND);
        etxAttachment.setMessage(messageDao.findById(10L));
        etxAttachment.setActiveState(true);
        return etxAttachment;
    }
}
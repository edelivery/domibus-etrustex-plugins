package eu.europa.ec.etrustex.node.web;

import eu.europa.ec.etrustex.node.AbstractSpringContextIT;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockServletContext;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author François Gautier
 * @version 1.0
 * @since 12/02/2018
 */
public class ResetNodePluginCacheControllerIT extends AbstractSpringContextIT {

    @Autowired
    private WebApplicationContext webAppContext;

    private MockMvc mvc;

    @SuppressWarnings("Duplicates")
    @Before
    public void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(webAppContext).build();

        MockServletContext sc = new MockServletContext("");
        ServletContextListener listener = new ContextLoaderListener(webAppContext);
        ServletContextEvent event = new ServletContextEvent(sc);
        listener.contextInitialized(event);
    }

    @Test
    @WithMockUser(roles = {"USER", "ADMIN"})
    public void resetCache() throws Exception {
        mvc.perform(get("/resetCacheNode")).andExpect(status().isOk());
    }

    @Test
    public void resetCache_redirect_notAuthenticated() throws Exception {
        redirect();
    }

    @Test
    @WithMockUser(roles = {"ANONYMOUS"})
    public void resetCache_redirect_notAuthorized() throws Exception {
        redirect();
    }

    private void redirect() throws Exception {
        mvc.perform(get("/resetCacheNode"))
                .andExpect(status().isFound())
                .andExpect(header().string("location", "login?returnUrl=%2FresetCache"))
                .andExpect(content().string("REDIRECT TO LOGIN"));
    }
}
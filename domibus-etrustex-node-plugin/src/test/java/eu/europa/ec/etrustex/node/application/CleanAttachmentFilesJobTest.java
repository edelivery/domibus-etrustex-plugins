package eu.europa.ec.etrustex.node.application;

import eu.europa.ec.etrustex.common.service.WorkspaceFileService;
import eu.europa.ec.etrustex.common.util.FileUtils;
import eu.europa.ec.etrustex.node.domain.DomainInitializerNode;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.nio.file.Path;
import java.util.concurrent.TimeUnit;

/**
 * @author François Gautier
 * @version 1.0
 * @since 09/03/2018
 */
@RunWith(JMockit.class)
public class CleanAttachmentFilesJobTest {

    @Injectable
    private WorkspaceFileService etxNodePluginWorkspaceService;

    @Injectable
    private DomainInitializerNode domainInitializerNode;

    @Injectable
    private ETrustExNodePluginProperties eTrustExNodePluginProperties;

    @Tested
    private CleanAttachmentFilesJob cleanAttachmentFilesJob;

    private Path path;

    @Before
    public void setUp() {
        new File("cleanup").mkdir();
        path = new File("cleanup").toPath();
    }

    @After
    public void tearDown() throws Exception {
        org.apache.commons.io.FileUtils.deleteDirectory(new File("cleanup"));
    }

    @Test
    public void isAllowed() {
        new Expectations() {{
            eTrustExNodePluginProperties.getDomain();
            times = 1;
            result = "test";
        }};
        Assert.assertThat(cleanAttachmentFilesJob.isAllowedToRun("test"), Is.is(true));
        new FullVerifications() {{
        }};
    }

    @Test
    public void isNotAllowed_empty() {
        new Expectations() {{
            eTrustExNodePluginProperties.getDomain();
            times = 1;
            result = null;
        }};
        Assert.assertThat(cleanAttachmentFilesJob.isAllowedToRun("test"), Is.is(true));
        new FullVerifications() {{
        }};
    }

    @Test
    public void executeInternal_ODay() {
        new Expectations() {{
            etxNodePluginWorkspaceService.getRoot();
            times = 1;
            result = path;

            eTrustExNodePluginProperties.getCleanFilesTtl();
            times = 1;
            result = 0;
        }};
        cleanAttachmentFilesJob.process(null);
        new FullVerifications() {{
        }};
    }

    @Test
    public void executeInternal_0() {
        new Expectations() {{
            etxNodePluginWorkspaceService.getRoot();
            times = 1;
            result = path;

            eTrustExNodePluginProperties.getCleanFilesTtl();
            times = 1;
            result = 0;
        }};
        cleanAttachmentFilesJob.process(null);
        new FullVerifications() {{
        }};
    }

    @Test
    public void executeInternal_1day(@Mocked final FileUtils fileUtils) {
        new Expectations() {{
            etxNodePluginWorkspaceService.getRoot();
            times = 1;
            result = path;

            eTrustExNodePluginProperties.getCleanFilesTtl();
            times = 1;
            result = 1;
        }};
        cleanAttachmentFilesJob.process(null);
        new FullVerifications() {{
            FileUtils.deleteOldFilesInDirectory(path, TimeUnit.MILLISECONDS.convert(1L, TimeUnit.DAYS));
            times = 1;
        }};
    }
}
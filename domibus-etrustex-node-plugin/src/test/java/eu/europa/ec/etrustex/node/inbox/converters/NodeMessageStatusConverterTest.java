package eu.europa.ec.etrustex.node.inbox.converters;

import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseRequest;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.testutils.ReflectionUtils;
import mockit.FullVerifications;
import oasis.names.specification.ubl.schema.xsd.applicationresponse_2.ApplicationResponseType;
import org.junit.Assert;
import org.junit.Test;

import static eu.europa.ec.etrustex.common.util.TransformerUtils.getObjectFromXml;
import static eu.europa.ec.etrustex.node.testutils.ResourcesUtils.readResource;

/**
 * JUnit for {@link NodeMessageStatusConverter}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 24/11/2017
 */
public class NodeMessageStatusConverterTest {

    @Test
    public void testPrivateConstructor_HappyFlow() throws Exception {
        ReflectionUtils.testPrivateConstructor(NodeMessageStatusConverter.class);
    }

    @Test
    public void testToEtxMessage_HappyFlow() {

        //input data from xml file
        SubmitApplicationResponseRequest request = getObjectFromXml(
                readResource("data/inbox/statusmessage/SubmitApplicationResponseRequest.xml")
        );

        //tested method
        EtxMessage etxMessage = NodeMessageStatusConverter.toEtxMessage(request);

        //status message assertions
        Assert.assertNotNull("application response shouldn't be null", request.getApplicationResponse());
        ApplicationResponseType applicationResponseType = request.getApplicationResponse();

        Assert.assertEquals(applicationResponseType.getID().getValue(), etxMessage.getMessageUuid());
        Assert.assertEquals(applicationResponseType.getDocumentResponse().getDocumentReference().getID().getValue(), etxMessage.getMessageReferenceUuid());
        Assert.assertEquals(applicationResponseType.getIssueDate().getValue().toGregorianCalendar(), etxMessage.getIssueDateTime());
        Assert.assertEquals(TransformerUtils.serializeObjToXML(request), etxMessage.getXml());

    }

    @Test
    public void testToEtxMessage_NullInput() {
        //tested method
        EtxMessage etxMessage = NodeMessageStatusConverter.toEtxMessage(new SubmitApplicationResponseRequest());

        new FullVerifications() {{
        }};
        Assert.assertNotNull("etx message shouldn't be null", etxMessage);
        Assert.assertNull("message uuid should be null", etxMessage.getMessageUuid());
    }
}
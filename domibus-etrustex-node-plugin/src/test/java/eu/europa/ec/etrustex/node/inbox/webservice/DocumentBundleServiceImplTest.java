package eu.europa.ec.etrustex.node.inbox.webservice;

import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.schema.xsd.documentbundle_1.DocumentBundleType;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleRequest;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleResponse;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.data.model.attachment.AttachmentDirection;
import eu.europa.ec.etrustex.node.data.model.attachment.AttachmentState;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import eu.europa.ec.etrustex.node.inbox.converters.NodeAcknowledgmentTypeConverter;
import eu.europa.ec.etrustex.node.inbox.converters.NodeMessageBundleConverter;
import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxMessageTransmissionProducer;
import eu.europa.ec.etrustex.node.service.MessageServicePlugin;
import eu.europa.ec.etrustex.node.service.security.IdentityManager;
import mockit.*;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import org.hamcrest.core.IsNull;
import org.joda.time.DateTime;
import org.junit.Test;
import org.unece.cefact.namespaces.standardbusinessdocumentheader.Partner;

import javax.xml.ws.Holder;
import java.util.Collections;
import java.util.List;

import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_MESSAGE_UUID;
import static eu.europa.ec.etrustex.node.data.model.message.MessageType.MESSAGE_BUNDLE;
import static eu.europa.ec.etrustex.node.inbox.webservice.DocumentBundleServiceImpl.ATT_ACTIVE_STATE;
import static eu.europa.ec.etrustex.node.testutils.HeaderUtils.buildHeader;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Junit class for {@link DocumentBundleServiceImpl}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 21/11/2017
 */
public class DocumentBundleServiceImplTest {

    @Injectable
    private MessageServicePlugin messageService;
    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Injectable
    private DomainNodePluginService domainNodePluginService;
    @Injectable
    private IdentityManager identityManager;

    @Injectable
    private InboxMessageTransmissionProducer inboxMessageTransmissionProducer;

    @Tested
    private DocumentBundleServiceImpl documentBundleService;

    @Test
    public void testSubmitDocumentBundle_HappyFlow() throws Exception {

        final SubmitDocumentBundleRequest submitDocumentBundleRequest = new SubmitDocumentBundleRequest();
        final SubmitDocumentBundleResponse submitDocumentBundleResponse = new SubmitDocumentBundleResponse();

        final String senderPartyUuid = "TEST_EGREFFE_APP_PARTY_B4";
        final String receiverPartyUuid = "TEST_EGREFFE_NP_PARTY_B4";

        final Holder<HeaderType> headerTypeHolder = buildHeader(senderPartyUuid, receiverPartyUuid);
        final EtxParty senderParty = new EtxParty();
        senderParty.setPartyUuid(senderPartyUuid);
        final EtxParty receiverParty = new EtxParty();
        receiverParty.setPartyUuid(receiverPartyUuid);
        receiverParty.setRequiresWrappers(true);

        final EtxMessage messageBundle = new EtxMessage();
        final Long messageId = 20L;
        messageBundle.setId(messageId);
        final EtxAttachment etxAttachment = new EtxAttachment();
        final Long attachmentId = 100L;
        etxAttachment.setId(attachmentId);
        messageBundle.getAttachmentList().add(etxAttachment);

        //noinspection Duplicates
        new Expectations(documentBundleService) {{
            documentBundleService.extractValidateParty(headerTypeHolder.value.getBusinessHeader().getSender());
            times = 1;
            result = senderParty;

            documentBundleService.extractValidateParty(headerTypeHolder.value.getBusinessHeader().getReceiver());
            times = 1;
            result = receiverParty;

            documentBundleService.createPersistMessage(submitDocumentBundleRequest, senderParty, receiverParty);
            times = 1;
            result = messageBundle;

            documentBundleService.buildAckNodeResponse(submitDocumentBundleRequest, headerTypeHolder);
            times = 1;
            result = submitDocumentBundleResponse;
        }};

        //tested method
        documentBundleService.submitDocumentBundle(submitDocumentBundleRequest, headerTypeHolder);

        new FullVerificationsInOrder() {{
            List<Partner> senderPartyListActual;
            documentBundleService.extractValidateParty(senderPartyListActual = withCapture());
            assertEquals("sender party uuid must match", senderPartyUuid, senderPartyListActual.get(0).getIdentifier().getValue());

            List<Partner> receiverPartyListActual;
            documentBundleService.extractValidateParty(receiverPartyListActual = withCapture());
            assertEquals("receiver party uuid must match", receiverPartyUuid, receiverPartyListActual.get(0).getIdentifier().getValue());

            Long msgIdActual;
            inboxMessageTransmissionProducer.triggerBundleTransmissionToBackend(msgIdActual = withCapture());
            assertEquals("message id should match", messageId, msgIdActual);

            Holder<HeaderType> holderActual;
            documentBundleService.buildHeaderResponse(holderActual = withCapture());
            assertEquals("header type holder must match", headerTypeHolder, holderActual);

            clearEtxLogKeysService.clearSpecificKeys(MDC_ETX_MESSAGE_UUID);
            times = 1;
        }};
    }

    @Test
    public void testCreatePersistMessage_HappyFlow() {

        final SubmitDocumentBundleRequest submitDocumentBundleRequest = new SubmitDocumentBundleRequest();
        final EtxParty senderParty = new EtxParty();
        final EtxParty receiverParty = new EtxParty();
        final String senderPartyUuid = "TEST_EGREFFE_APP_PARTY_B4";
        final String receiverPartyUuid = "TEST_EGREFFE_NP_PARTY_B4";
        senderParty.setPartyUuid(senderPartyUuid);
        receiverParty.setPartyUuid(receiverPartyUuid);

        final EtxAttachment etxAttachment = new EtxAttachment();
        final EtxMessage etxMessage = new EtxMessage();
        etxMessage.setAttachmentList(Collections.singletonList(etxAttachment));

        //stubbing the converter
        new MockUp<NodeMessageBundleConverter>() {

            @Mock
            EtxMessage toEtxMessage(SubmitDocumentBundleRequest submitDocumentBundleRequest, boolean receiverPartyRequiresWrappers) {
                return etxMessage;
            }
        };

        //method to test
        documentBundleService.createPersistMessage(submitDocumentBundleRequest, senderParty, receiverParty);

        new FullVerifications() {{
            messageService.createMessage(etxMessage);
            times = 1;
        }};
        assertEquals(MESSAGE_BUNDLE, etxMessage.getMessageType());
        assertEquals(MessageState.CREATED, etxMessage.getMessageState());
        assertEquals(MessageDirection.NODE_TO_BACKEND, etxMessage.getDirectionType());
        assertEquals(senderParty, etxMessage.getSender());
        assertEquals(receiverParty, etxMessage.getReceiver());
        assertEquals(etxMessage, etxAttachment.getMessage());
        assertEquals(AttachmentState.CREATED, etxAttachment.getStateType());
        assertEquals(AttachmentDirection.NODE_TO_BACKEND, etxAttachment.getDirectionType());
        assertEquals(ATT_ACTIVE_STATE, etxAttachment.isActiveState());
    }

    @Test
    public void testBuildAckNodeResponse_HappyFlow(final @Mocked NodeAcknowledgmentTypeConverter nodeAcknowledgmentTypeConverter) throws Exception {

        final String messageId = "TEST_msg_id";
        final String senderPartyUuid = "TEST_EGREFFE_APP_PARTY_B4";
        final String receiverPartyUuid = "TEST_EGREFFE_NP_PARTY_B4";

        final Holder<HeaderType> headerTypeHolder = buildHeader(senderPartyUuid, receiverPartyUuid);
        final SubmitDocumentBundleRequest submitDocumentBundleRequest = new SubmitDocumentBundleRequest();
        final DocumentBundleType documentBundleType = new DocumentBundleType();
        documentBundleType.setID(getIdType(messageId));
        submitDocumentBundleRequest.setDocumentBundle(documentBundleType);

        //tested method
        SubmitDocumentBundleResponse submitDocumentBundleResponse = documentBundleService.buildAckNodeResponse(submitDocumentBundleRequest, headerTypeHolder);
        assertThat(submitDocumentBundleResponse.getAck(), is(IsNull.notNullValue()));
        new FullVerifications() {{

            String senderPartyUuidActual, receiverPartyUuidActual, documentTypeCodeActual, documenRefIdActual;
            DateTime issueDateActual;
            NodeAcknowledgmentTypeConverter.buildAcknowledgmentType(issueDateActual = withCapture(),
                    documenRefIdActual = withCapture(), documentTypeCodeActual = withCapture(), senderPartyUuidActual = withCapture(), receiverPartyUuidActual = withCapture());
            times = 1;
            assertEquals(DateTime.now().toLocalDate(), issueDateActual.toLocalDate());
            assertEquals(messageId, documenRefIdActual);
            assertEquals(DocumentBundleServiceImpl.NODE_DOCUMENT_TYPE, documentTypeCodeActual);
            assertEquals(senderPartyUuid, senderPartyUuidActual);
            assertEquals(receiverPartyUuid, receiverPartyUuidActual);

        }};

    }

    private IDType getIdType(String messageId) {
        IDType value = new IDType();
        value.setValue(messageId);
        return value;
    }
}
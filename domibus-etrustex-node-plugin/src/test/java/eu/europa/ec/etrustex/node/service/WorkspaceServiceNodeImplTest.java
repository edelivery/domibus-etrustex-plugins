package eu.europa.ec.etrustex.node.service;

import eu.domibus.ext.delegate.services.multitenancy.DomainTaskExecutorExtDelegate;
import eu.domibus.ext.services.DomainContextExtService;
import eu.europa.ec.etrustex.common.util.ETrustExPluginAS4Properties;
import eu.europa.ec.etrustex.common.util.WorkSpaceUtils;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.Executor;

import static org.hamcrest.core.Is.is;

/**
 * @author François Gautier
 * @version 1.0
 * @since 22-Dec-17
 */
@RunWith(JMockit.class)
public class WorkspaceServiceNodeImplTest {

    @Injectable
    ETrustExPluginAS4Properties etxPluginAS4Properties;
    @Injectable
    Executor taskExecutor;
    @Injectable
    DomainTaskExecutorExtDelegate domainTaskExecutorExtDelegate;
    @Injectable
    DomainContextExtService domainContextExtService;
    @Injectable
    private ETrustExNodePluginProperties eTrustExNodePluginProperties;
    @Tested
    private WorkspaceServiceNodeImpl workspaceServiceNode;

    @Before
    public void recordExpectationsForPostConstruct() {
        new MockUp<WorkspaceServiceNodeImpl>() {
            @Mock
            void loadConfiguration() {
            }
        };
    }
    @Test
    @SuppressWarnings("unused")
    public void getRoot(
            @Mocked final Paths paths,
            @Mocked final WorkSpaceUtils workSpaceUtils) {

        final Path path = getPath();

        new Expectations() {{
            Paths.get(anyString);
            times = 1;
            result = path;

            WorkSpaceUtils.getPath((Path) any);
            times = 1;

            eTrustExNodePluginProperties.getDownloadLocation();
            times = 1;
            result = "target/downloadLocation";
        }};

        workspaceServiceNode.getRoot();

        new FullVerifications() {{
            Path absoluteNormalizedPath;
            WorkSpaceUtils.getPath(absoluteNormalizedPath = withCapture());
            Assert.assertThat(absoluteNormalizedPath, is(path.normalize().toAbsolutePath()));
        }};

    }

    private Path getPath() {
        return new File("").toPath();
    }

}
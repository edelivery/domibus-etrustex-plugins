package eu.europa.ec.etrustex.node.outbox.client.adapters;

import ec.schema.xsd.ack_2.AcknowledgmentType;
import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.services.wsdl.applicationresponse_2.FaultResponse;
import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseRequest;
import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseResponse;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.jms.TextMessageCreator;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import eu.europa.ec.etrustex.node.testutils.FaultResponseUtils;
import eu.europa.ec.etrustex.node.testutils.PayloadDTOBuilder;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.hamcrest.core.IsNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jms.core.JmsOperations;
import org.springframework.jms.core.MessageCreator;

import javax.xml.ws.Holder;

import static eu.europa.ec.etrustex.common.util.ContentId.*;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.deserializeXMLToObj;
import static eu.europa.ec.etrustex.node.testutils.ETrustExAdapterDTOBuilder.getEmpty;
import static eu.europa.ec.etrustex.node.testutils.ETrustExPluginPropertiesTestConstants.TEST_AS4_ACTION_SUBMITAPPLICATIONRESPONSE_RESPONSE;
import static eu.europa.ec.etrustex.node.testutils.ReflectionUtils.getPrivateField;
import static eu.europa.ec.etrustex.node.testutils.ResourcesUtils.readResource;
import static org.apache.commons.codec.binary.StringUtils.getBytesUtf8;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author François Gautier
 * @version 1.0
 * @since 08/03/2018
 */
@RunWith(JMockit.class)
public class SendMessageStatusAdapterTest extends BaseAdapterTest {
    private static final byte[] PAYLOAD = getBytesUtf8("<ec.services.wsdl.applicationresponse__2.SubmitApplicationResponseRequest><applicationResponse><id><value>ATT_ID_1</value></id><issueDate><value>2015-07-01T00:00:00.000+02:00</value></issueDate><documentResponse><response><referenceID><value></value></referenceID><responseCode><value>BDL:1</value></responseCode><description><oasis.names.specification.ubl.schema.xsd.commonbasiccomponents__2.DescriptionType><value>Available</value></oasis.names.specification.ubl.schema.xsd.commonbasiccomponents__2.DescriptionType></description></response><documentReference><id><value>REF_BUNDLEID_1</value></id><documentTypeCode><value>BDL</value></documentTypeCode></documentReference></documentResponse></applicationResponse></ec.services.wsdl.applicationresponse__2.SubmitApplicationResponseRequest>");

    @Injectable
    private ETrustExNodePluginProperties eTrustExNodePluginProperties;

    @Injectable
    private JmsOperations etxNodePluginNodeToBackendJmsTemplate;

    @Tested
    private SendMessageStatusAdapter sendMessageStatusAdapter;

    @Test
    public void performOperation_NoHeaderPayload() throws NoSuchFieldException, IllegalAccessException {
        sendMessageStatusAdapter.performOperation(getEmpty());

        new FullVerifications() {{
            TextMessageCreator messageCreator;
            etxNodePluginBackendToNodeJmsTemplate.send(messageCreator = withCapture());
            times = 1;

            ETrustExAdapterDTO textMessage =
                    (ETrustExAdapterDTO) deserializeXMLToObj(
                            (String) getPrivateField(TextMessageCreator.class, messageCreator, "dto"));

            assertThat(textMessage.getPayloadFromCID(CONTENT_FAULT), IsNull.notNullValue());
        }};
    }

    @Test
    public void performOperation_NoPayload() throws NoSuchFieldException, IllegalAccessException {

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(CONTENT_ID_HEADER.getValue())
                .withPayload(getBytesUtf8("<ec.schema.xsd.commonaggregatecomponents__2.HeaderType/>"))
                .build());
        try {
            sendMessageStatusAdapter.performOperation(dto);
//            Assert.fail();
        } catch (ETrustExPluginException e) {
//            assertThat(e.getError(), is(ETrustExError.DEFAULT));
        }

        new FullVerifications() {{
            TextMessageCreator messageCreator;
            etxNodePluginBackendToNodeJmsTemplate.send(messageCreator = withCapture());
            times = 1;

            ETrustExAdapterDTO textMessage =
                    (ETrustExAdapterDTO) deserializeXMLToObj(
                            (String) getPrivateField(TextMessageCreator.class, messageCreator, "dto"));

            assertThat(textMessage.getPayloadFromCID(CONTENT_FAULT), IsNull.notNullValue());
        }};
    }

    @Test
    public void performOperation_AcknowledgeOk() throws Exception {
        new Expectations() {{

            arPortType.submitApplicationResponse(((SubmitApplicationResponseRequest) any), (Holder<HeaderType>) any);
            times = 1;
            result = getOkResponse();

        }};

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_HEADER.getValue())
                .withPayload(getBytesUtf8(readResource("data/outbox/document/Header.xml")))
                .build());

        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_GENERIC.getValue())
                .withPayload(PAYLOAD)
                .build());

        sendMessageStatusAdapter.performOperation(dto);

        new FullVerifications() {{
            etxNodePluginNodeToBackendJmsTemplate.send((MessageCreator) any);
            times = 1;
        }};
    }

    @Test
    public void performOperation_Exception_AlreadyWithFault() throws Exception {
        new Expectations() {{
            arPortType.submitApplicationResponse(((SubmitApplicationResponseRequest) any), (Holder<HeaderType>) any);
            times = 1;
            result = new Exception();
        }};

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_HEADER.getValue())
                .withPayload(getBytesUtf8(readResource("data/outbox/document/Header.xml")))
                .build());

        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_GENERIC.getValue())
                .withPayload(PAYLOAD)
                .build());

        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_FAULT.getValue())
                .withPayload(getBytesUtf8("ERROR"))
                .build());

        try {
            sendMessageStatusAdapter.performOperation(dto);
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getError(), is(ETrustExError.DEFAULT));
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void performOperation_Exception() throws Exception {
        new Expectations() {{

            arPortType.submitApplicationResponse(((SubmitApplicationResponseRequest) any), (Holder<HeaderType>) any);
            times = 1;
            result = new Exception();

        }};

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_HEADER.getValue())
                .withPayload(getBytesUtf8(readResource("data/outbox/document/Header.xml")))
                .build());

        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_GENERIC.getValue())
                .withPayload(PAYLOAD)
                .build());

        sendMessageStatusAdapter.performOperation(dto);

        new FullVerifications() {{
            TextMessageCreator messageCreator;
            etxNodePluginBackendToNodeJmsTemplate.send(messageCreator = withCapture());
            times = 1;

            ETrustExAdapterDTO textMessage =
                    (ETrustExAdapterDTO) deserializeXMLToObj(
                            (String) getPrivateField(TextMessageCreator.class, messageCreator, "dto"));

            assertThat(textMessage.getPayloadFromCID(CONTENT_FAULT), IsNull.notNullValue());
        }};
    }

    @Test
    public void performOperation_AcknowledgeFault() throws Exception {
        new Expectations() {{

            arPortType.submitApplicationResponse(((SubmitApplicationResponseRequest) any), (Holder<HeaderType>) any);
            times = 1;
            result = new FaultResponse("", FaultResponseUtils.getFaultType());

        }};

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_HEADER.getValue())
                .withPayload(getBytesUtf8(readResource("data/outbox/document/Header.xml")))
                .build());

        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_GENERIC.getValue())
                .withPayload(PAYLOAD)
                .build());

        sendMessageStatusAdapter.performOperation(dto);

        new FullVerifications() {{

            etxNodePluginNodeToBackendJmsTemplate.send((MessageCreator) any);
            times = 1;

        }};

    }

    @Test
    public void acknowledgeFault() throws NoSuchFieldException, IllegalAccessException {
        ETrustExAdapterDTO dto = getEmpty();

        sendMessageStatusAdapter.acknowledgeFault(dto, new FaultResponse("ERROR", FaultResponseUtils.getFaultType()));

        new FullVerifications() {{
            TextMessageCreator messageCreator;
            etxNodePluginNodeToBackendJmsTemplate.send(messageCreator = withCapture());
            times = 1;

            ETrustExAdapterDTO textMessage =
                    (ETrustExAdapterDTO) deserializeXMLToObj(
                            (String) getPrivateField(TextMessageCreator.class, messageCreator, "dto"));
            assertThat(textMessage.getAs4Action(), is(TEST_AS4_ACTION_SUBMITAPPLICATIONRESPONSE_RESPONSE));

            assertThat(textMessage.getPayloadFromCID(CONTENT_ID_FAULT_RESPONSE), IsNull.notNullValue());
        }};
    }

    @Test
    public void acknowledgeOk() throws NoSuchFieldException, IllegalAccessException {

        ETrustExAdapterDTO dto = getEmpty();

        sendMessageStatusAdapter.acknowledgeOk(dto, new HeaderType(), new SubmitApplicationResponseResponse());

        new FullVerifications() {{
            TextMessageCreator messageCreator;
            etxNodePluginNodeToBackendJmsTemplate.send(messageCreator = withCapture());
            times = 1;

            ETrustExAdapterDTO textMessage =
                    (ETrustExAdapterDTO) deserializeXMLToObj(
                            (String) getPrivateField(TextMessageCreator.class, messageCreator, "dto"));
            assertThat(textMessage.getAs4Action(), is(TEST_AS4_ACTION_SUBMITAPPLICATIONRESPONSE_RESPONSE));

            assertThat(textMessage.getPayloadFromCID(CONTENT_ID_HEADER), IsNull.notNullValue());
            assertThat(textMessage.getPayloadFromCID(CONTENT_ID_GENERIC), IsNull.notNullValue());
        }};
    }

    private SubmitApplicationResponseResponse getOkResponse() {
        SubmitApplicationResponseResponse response = new SubmitApplicationResponseResponse();
        AcknowledgmentType ack = new AcknowledgmentType();
        response.setAck(ack);
        return response;
    }

}
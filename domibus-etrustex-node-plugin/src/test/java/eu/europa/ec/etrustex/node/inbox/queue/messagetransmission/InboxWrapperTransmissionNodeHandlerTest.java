package eu.europa.ec.etrustex.node.inbox.queue.messagetransmission;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.service.WorkspaceService;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.common.util.ETrustExPluginAS4Properties;
import eu.europa.ec.etrustex.node.data.model.attachment.AttachmentState;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import eu.europa.ec.etrustex.node.inbox.queue.error.InboxErrorProducer;
import eu.europa.ec.etrustex.node.service.AttachmentService;
import eu.europa.ec.etrustex.node.service.MessageServicePlugin;
import eu.europa.ec.etrustex.node.service.NodeConnectorSubmissionService;
import eu.europa.ec.etrustex.node.testutils.ResourcesUtils;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static eu.europa.ec.etrustex.node.data.model.message.MessageDirection.NODE_TO_BACKEND;
import static eu.europa.ec.etrustex.node.data.model.message.MessageType.MESSAGE_STATUS;
import static eu.europa.ec.etrustex.node.testutils.EtxMessageBuilder.formEtxMessageInboxBundle1Wrapper;
import static org.apache.commons.lang3.RandomUtils.nextLong;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * JUNIT tests for the methods of InboxStatusMessageTransmissionHandler.
 *
 * @author Federico Martini
 * @version 1.0
 * @since 16/11/2017
 */
@SuppressWarnings({"Duplicates", "ResultOfMethodCallIgnored"})
@RunWith(JMockit.class)
public class InboxWrapperTransmissionNodeHandlerTest {

    @Injectable
    private NodeConnectorSubmissionService nodeConnectorSubmissionService;

    @Injectable
    private ETrustExPluginAS4Properties etxPluginAS4Properties;

    @Injectable
    private InboxErrorProducer inboxErrorProducer;

    @Injectable
    private InboxMessageTransmissionProducer inboxMessageTransmissionProducer;

    @Injectable
    private MessageServicePlugin messageService;

    @Injectable
    private AttachmentService attachmentService;

    @Injectable
    private WorkspaceService etxNodePluginWorkspaceService;

    @Tested
    private InboxWrapperTransmissionNodeHandler inboxBundleWrapperTransmissionHandler;

    private String domibusMessageId;
    private Long attachmentId;
    private Path attachmentFile;

    @Before
    public void setUp() {
        domibusMessageId = UUID.randomUUID().toString();
        attachmentId = nextLong(0, 99999);
        File resourceFile = new File("src/test/resources/payload.xml");
        attachmentFile = resourceFile.toPath();
    }

    @Test
    public void handleMessageTransmissionToBackend_notMessageBundle() {
        final EtxMessage etxMessage = formEtxMessageInboxBundle1Wrapper();
        final InboxMessageTransmissionQueueMessage message =
                new InboxMessageTransmissionQueueMessage(
                        MESSAGE_STATUS,
                        NODE_TO_BACKEND,
                        etxMessage.getId(),
                        etxMessage.getAttachmentList().get(0).getId());
        etxMessage.setMessageType(MessageType.MESSAGE_STATUS);

        new Expectations() {{
            messageService.findMessageById(etxMessage.getId());
            times = 1;
            result = etxMessage;
        }};

        inboxBundleWrapperTransmissionHandler.handleMessageTransmissionToBackend(message);

        new FullVerifications() {{
            inboxMessageTransmissionProducer.triggerMessageWithError(message);
            times = 1;
        }};

        assertThat(message.getErrorCode(), is(ETrustExError.DEFAULT.getCode()));
    }

    @Test
    public void handleMessageTransmissionToBackend_attachmentNotFound() {
        final EtxMessage etxMessage = formEtxMessageInboxBundle1Wrapper();
        final InboxMessageTransmissionQueueMessage message =
                new InboxMessageTransmissionQueueMessage(
                        MESSAGE_STATUS,
                        NODE_TO_BACKEND,
                        etxMessage.getId(),
                        nextLong(0, 99999));

        new Expectations() {{
            messageService.findMessageById(etxMessage.getId());
            times = 1;
            result = etxMessage;
        }};

        inboxBundleWrapperTransmissionHandler.handleMessageTransmissionToBackend(message);

        new FullVerifications() {{
            inboxMessageTransmissionProducer.triggerMessageWithError(message);
            times = 1;
        }};

        assertThat(message.getErrorCode(), is(ETrustExError.DEFAULT.getCode()));
    }

    @Test
    public void handleMessageTransmissionToBackend_messageNotDownloaded() {
        final EtxMessage etxMessage = formEtxMessageInboxBundle1Wrapper();
        final EtxAttachment etxAttachment = etxMessage.getAttachmentList().get(0);
        etxAttachment.setStateType(AttachmentState.PROCESSED_BY_BACKEND);

        final InboxMessageTransmissionQueueMessage message =
                new InboxMessageTransmissionQueueMessage(
                        MESSAGE_STATUS,
                        NODE_TO_BACKEND,
                        etxMessage.getId(),
                        etxAttachment.getId());

        new Expectations() {{
            messageService.findMessageById(etxMessage.getId());
            times = 1;
            result = etxMessage;
        }};

        inboxBundleWrapperTransmissionHandler.handleMessageTransmissionToBackend(message);

        new FullVerifications() {{
            inboxMessageTransmissionProducer.triggerMessageWithError(message);
            times = 1;
        }};

        assertThat(message.getErrorCode(), is(ETrustExError.ETX_009.getCode()));
    }

    @Test
    public void handleMessageTransmissionToBackend_ok() throws Exception {
        final EtxMessage etxMessage = formEtxMessageInboxBundle1Wrapper();
        final EtxAttachment etxAttachment = etxMessage.getAttachmentList().get(0);
        final String domibusId = UUID.randomUUID().toString();
        final InboxMessageTransmissionQueueMessage message =
                new InboxMessageTransmissionQueueMessage(
                        MESSAGE_STATUS,
                        NODE_TO_BACKEND,
                        etxMessage.getId(),
                        etxAttachment.getId());

        new Expectations() {{
            messageService.findMessageById(etxMessage.getId());
            times = 1;
            result = etxMessage;

            etxPluginAS4Properties.getAs4ETrustExNodePartyId();
            times = 1;
            result = "getAs4ETrustExNodePartyId";
            etxPluginAS4Properties.getAs4FromPartyIdType();
            times = 1;
            result = "getAs4FromPartyIdType";
            etxPluginAS4Properties.getAs4FromPartyRole();
            times = 1;
            result = "getAs4FromPartyRole";
            etxPluginAS4Properties.getAs4ToPartyIdType();
            times = 1;
            result = "getAs4ToPartyIdType";
            etxPluginAS4Properties.getAs4ToPartyRole();
            times = 1;
            result = "getAs4ToPartyRole";
            etxPluginAS4Properties.getAs4ServiceInboxWrapperTransmission();
            times = 1;
            result = "getAs4ServiceInboxWrapperTransmission";
            etxPluginAS4Properties.getAs4ServiceInboxWrapperTransmissionServiceType();
            times = 1;
            result = "getAs4ServiceInboxWrapperTransmissionServiceType";
            etxPluginAS4Properties.getAs4ActionInboxWrapperTransmissionRequest();
            times = 1;
            result = "getAs4ActionInboxWrapperTransmissionRequest";

            etxNodePluginWorkspaceService.getFile(etxAttachment.getId());
            times = 1;
            result = attachmentFile;

            nodeConnectorSubmissionService.submitToDomibus(withAny(new ETrustExAdapterDTO()));
            times = 1;
            result = domibusId;
        }};

        inboxBundleWrapperTransmissionHandler.handleMessageTransmissionToBackend(message);

        new FullVerifications() {{
            ETrustExAdapterDTO etxAttachmentCaptured;
            nodeConnectorSubmissionService.submitToDomibus(etxAttachmentCaptured = withCapture());
            times = 1;

            assertThat(
                    etxAttachmentCaptured,
                    sameBeanAs(getObjectMapper().readValue(ResourcesUtils.readResource(
                            "data/inbox/wrapperTransmission/etrustExAdapterDTO.json"),//readResource(fileName),
                            ETrustExAdapterDTO.class))
                            .ignoring("as4Payloads"));
            attachmentService.update(etxAttachment);
            times = 1;
        }};

        assertThat(etxAttachment.getStateType(), is(AttachmentState.SUBMITTED_TO_DOMIBUS));
        assertThat(etxAttachment.getDomibusMessageId(), is(domibusId));
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();

        SimpleModule module = new SimpleModule();
        module.addSerializer(PayloadDTO.class, new JsonSerializer<PayloadDTO>() {
            @Override
            public void serialize(PayloadDTO value, JsonGenerator gen, SerializerProvider serializers) {

            }
        });
        objectMapper.registerModule(module);
        return objectMapper;
    }

    @Test
    public void handleMessageTransmissionToBackend_fileNotFound() {
        final EtxMessage etxMessage = formEtxMessageInboxBundle1Wrapper();
        final EtxAttachment etxAttachment = etxMessage.getAttachmentList().get(0);
        final InboxMessageTransmissionQueueMessage message =
                new InboxMessageTransmissionQueueMessage(
                        MESSAGE_STATUS,
                        NODE_TO_BACKEND,
                        etxMessage.getId(),
                        etxAttachment.getId());

        new Expectations() {{
            messageService.findMessageById(etxMessage.getId());
            times = 1;
            result = etxMessage;

            etxPluginAS4Properties.getAs4ETrustExNodePartyId();
            times = 1;
            result = "getAs4ETrustExNodePartyId";

            etxPluginAS4Properties.getAs4FromPartyIdType();
            times = 1;
            result = "getAs4FromPartyIdType";

            etxPluginAS4Properties.getAs4FromPartyRole();
            times = 1;
            result = "getAs4FromPartyRole";

            etxPluginAS4Properties.getAs4ToPartyIdType();
            times = 1;
            result = "getAs4ToPartyIdType";

            etxPluginAS4Properties.getAs4ToPartyRole();
            times = 1;
            result = "getAs4ToPartyRole";

            etxPluginAS4Properties.getAs4ServiceInboxWrapperTransmission();
            times = 1;
            result = "getAs4ServiceInboxWrapperTransmission";

            etxPluginAS4Properties.getAs4ServiceInboxWrapperTransmissionServiceType();
            times = 1;
            result = "getAs4ServiceInboxWrapperTransmissionServiceType";

            etxPluginAS4Properties.getAs4ActionInboxWrapperTransmissionRequest();
            times = 1;
            result = "getAs4ActionInboxWrapperTransmissionRequest";

            etxNodePluginWorkspaceService.getFile(etxAttachment.getId());
            times = 1;
            result = Paths.get("ThiPathDoesNotExists");
        }};

        inboxBundleWrapperTransmissionHandler.handleMessageTransmissionToBackend(message);

        new FullVerifications() {{
            inboxMessageTransmissionProducer.triggerMessageWithError(message);
            times = 1;
        }};

        assertThat(message.getErrorCode(), is(ETrustExError.DEFAULT.getCode()));

    }

    @Test
    public void handleSendMessageSuccess_Exception() {

        new Expectations() {{
            attachmentService.findAttachmentByDomibusMessageId(domibusMessageId);
            times = 1;
            result = null;
        }};

        try {
            inboxBundleWrapperTransmissionHandler.handleSendMessageSuccess(getDto());
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getError(), is(ETrustExError.DEFAULT));
            assertThat(e.getMessage(), CoreMatchers.containsString(domibusMessageId));

        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void handleSendMessageSuccess_Ok_with_deleteFile() {

        final String attachmentUuid = UUID.randomUUID().toString();
        final EtxAttachment etxAttachment = getEtxAttachment(attachmentUuid);

        new Expectations() {{
            attachmentService.findAttachmentByDomibusMessageId(domibusMessageId);
            times = 1;
            result = etxAttachment;

        }};

        inboxBundleWrapperTransmissionHandler.handleSendMessageSuccess(getDto());

        new FullVerifications() {{
            attachmentService.update(etxAttachment);
            times = 1;
            etxNodePluginWorkspaceService.deleteFile(etxAttachment.getId());
            times = 1;
        }};

        assertThat(etxAttachment.getStateType(), is(AttachmentState.SENT_TO_BACKEND));

    }

    @Test
    public void handleSendMessageSuccess_File_delete_exception() {

        final String attachmentUuid = UUID.randomUUID().toString();
        final EtxAttachment etxAttachment = getEtxAttachment(attachmentUuid);

        new Expectations() {{
            attachmentService.findAttachmentByDomibusMessageId(domibusMessageId);
            times = 1;
            result = etxAttachment;

            etxNodePluginWorkspaceService.deleteFile(etxAttachment.getId());
            times = 1;
            result = new ETrustExPluginException(ETrustExError.DEFAULT, "");

        }};

        try {
            inboxBundleWrapperTransmissionHandler.handleSendMessageSuccess(getDto());
            Assert.fail("ETrustExPluginException is expected");
        } catch (ETrustExPluginException etxEx) {
            assertThat(etxEx.getError(), is(ETrustExError.DEFAULT));
        }

        new FullVerifications() {{
            attachmentService.update(etxAttachment);
            times = 1;
        }};

    }

    @Test
    public void handleSendMessageSuccess_IdNotFound() {

        try {
            ETrustExAdapterDTO dto = getDto();
            dto.setAs4MessageId(null);
            inboxBundleWrapperTransmissionHandler.handleSendMessageSuccess(dto);
            Assert.fail("ETrustExPluginException is expected");
        } catch (ETrustExPluginException etxEx) {
            assertThat(etxEx.getError(), is(ETrustExError.DEFAULT));
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void handleSendMessageFailed_Ok_with_deleteFile() throws UnsupportedEncodingException {

        //final EtxMessage etxMessage = getEtxMessage(domibusMessageId);
        final String attachmentUuid = UUID.randomUUID().toString();
        final EtxAttachment etxAttachment = getEtxAttachment(attachmentUuid);
        final ETrustExAdapterDTO dto = getDtoWithPayload();

        new Expectations() {{
            attachmentService.findAttachmentByDomibusMessageId(dto.getAs4MessageId());
            times = 1;
            result = etxAttachment;

        }};

        inboxBundleWrapperTransmissionHandler.handleSendMessageFailed(dto);

        new FullVerifications() {{
            InboxMessageTransmissionQueueMessage queueMessageActual;
            inboxErrorProducer.triggerError(queueMessageActual = withCapture());
            times = 1;

            Assert.assertNotNull(queueMessageActual);
            assertThat(queueMessageActual.getMessageType(), is(MessageType.MESSAGE_BUNDLE));
            assertThat(queueMessageActual.getMessageDirection(), is(NODE_TO_BACKEND));
            assertThat(queueMessageActual.getErrorCode(), is(ETrustExError.DEFAULT.getCode()));

            etxNodePluginWorkspaceService.deleteFile(etxAttachment.getId());
            times = 1;
        }};

    }

    @Test
    public void handleSendMessageFailed_Exception() throws UnsupportedEncodingException {

        final EtxMessage etxMessage = getEtxMessage(domibusMessageId);
        final ETrustExAdapterDTO realDto = getDtoWithPayload();

        new Expectations() {{
            attachmentService.findAttachmentByDomibusMessageId(realDto.getAs4MessageId());
            times = 1;
            result = null;
        }};

        try {
            inboxBundleWrapperTransmissionHandler.handleSendMessageFailed(realDto);
            Assert.fail("ETrustExPluginException is expected");
        } catch (ETrustExPluginException etxEx) {
            assertThat(etxEx.getError(), is(ETrustExError.DEFAULT));
        }

        new FullVerifications() {{
        }};

    }

    private EtxMessage getEtxMessage(String domibusMessageId) {
        final EtxMessage etxMessage = new EtxMessage();
        etxMessage.setId(1234L);
        etxMessage.setDomibusMessageId(domibusMessageId);
        etxMessage.setMessageState(MessageState.SUBMITTED_TO_DOMIBUS);
        etxMessage.setMessageType(MessageType.MESSAGE_BUNDLE);
        return etxMessage;
    }

    private ETrustExAdapterDTO getDto() {
        ETrustExAdapterDTO dto = new ETrustExAdapterDTO();
        dto.setAs4MessageId(domibusMessageId);
        return dto;
    }

    private EtxAttachment getEtxAttachment(String attachmentUuid) {
        final EtxAttachment etxAttachment = new EtxAttachment();
        etxAttachment.setMessage(getEtxMessage(domibusMessageId));
        etxAttachment.setId(4567L);
        etxAttachment.setId(attachmentId);
        etxAttachment.setAttachmentUuid(attachmentUuid);
        etxAttachment.setStateType(AttachmentState.SUBMITTED_TO_DOMIBUS);
        return etxAttachment;
    }

    private ETrustExAdapterDTO getDtoWithPayload() throws UnsupportedEncodingException {
        ETrustExAdapterDTO dto = new ETrustExAdapterDTO();
        dto.setAs4MessageId(domibusMessageId);
        PayloadDTO payloadDTO = new PayloadDTO(ContentId.CONTENT_FAULT.getValue(), "XML Payload to TEST".getBytes(StandardCharsets.UTF_8), "mymeType", false, "JUNIT test");
        dto.addAs4Payload(payloadDTO);
        return dto;
    }

}
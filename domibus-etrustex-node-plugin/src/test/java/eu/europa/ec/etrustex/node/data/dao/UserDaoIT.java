package eu.europa.ec.etrustex.node.data.dao;

import eu.europa.ec.etrustex.node.data.model.administration.EtxUser;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 29-Jan-18
 */
public class UserDaoIT extends AbstractDaoIT<EtxUser> {

    @Autowired
    private UserDao userDao;

    @Test
    public void name_notFound() {
        EtxUser result = userDao.findUserByName("NOT_FOUND");
        assertThat(result, is(nullValue()));
    }

    @Test
    public void name_ok() {
        EtxUser result = userDao.findUserByName("USER_1");
        assertThat(result, is(notNullValue()));
    }

    @Override
    protected Long getExistingId() {
        return 1001L;
    }

    @Override
    protected DaoBase<EtxUser> getDao() {
        return userDao;
    }

    @Override
    protected EtxUser getNewEntity() {
        EtxUser etxUser = new EtxUser();
        etxUser.setName(UUID.randomUUID().toString());
        etxUser.setPassword(UUID.randomUUID().toString());
        return etxUser;
    }
}
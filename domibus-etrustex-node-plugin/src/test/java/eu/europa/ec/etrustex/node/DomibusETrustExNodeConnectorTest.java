package eu.europa.ec.etrustex.node;

import eu.domibus.common.*;
import eu.domibus.ext.domain.CollaborationInfoDTO;
import eu.domibus.ext.domain.MessageAttemptDTO;
import eu.domibus.ext.domain.MessageInfoDTO;
import eu.domibus.ext.domain.UserMessageDTO;
import eu.domibus.ext.services.MessageMonitorExtService;
import eu.domibus.ext.services.UserMessageExtService;
import eu.domibus.messaging.MessageNotFoundException;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.handlers.Action;
import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.handlers.Service;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import eu.europa.ec.etrustex.node.inbox.FollowUpOperationHandlerFactory;
import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxWrapperTransmissionNodeHandler;
import eu.europa.ec.etrustex.node.outbox.factory.ETrustExOperationHandlerFactory;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collections;
import java.util.UUID;

/**
 * @author François Gautier
 * @version 1.0
 * @since 29/06/2018
 */
@SuppressWarnings("Duplicates")
@RunWith(JMockit.class)
public class DomibusETrustExNodeConnectorTest {
    private static final String ACTION = "ACTION";
    private static final String MSG_ID = UUID.randomUUID().toString();
    private static final String FINAL_RECIPIENT = "FINALRECIPIENT";

    @Injectable
    private ETrustExOperationHandlerFactory operationHandlerFactory;
    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Injectable
    private DomainNodePluginService domainNodePluginService;
    @Injectable
    private FollowUpOperationHandlerFactory followUpOperationHandlerFactory;
    @Injectable
    private String name;
    @Injectable
    private MessageMonitorExtService messageMonitorService;

    @Injectable
    private UserMessageExtService userMessageService;

    @Tested
    @Mocked
    private DomibusETrustExNodeConnector domibusETrustExNodeConnector;

    @Mocked
    private InboxWrapperTransmissionNodeHandler inboxWrapperTransmissionNodeHandler;

    @Test
    public void messageSendSuccess_happyFlow() {
        final UserMessageDTO userMsgDTO = new UserMessageDTO();
        CollaborationInfoDTO collaborationInfo = new CollaborationInfoDTO();
        collaborationInfo.setAction(ACTION);
        userMsgDTO.setCollaborationInfo(collaborationInfo);
        MessageInfoDTO messageInfo = new MessageInfoDTO();
        messageInfo.setMessageId(MSG_ID);
        userMsgDTO.setMessageInfo(messageInfo);

        new Expectations() {{
            userMessageService.getMessage(MSG_ID);
            times = 1;
            result = userMsgDTO;

            followUpOperationHandlerFactory.getOperationHandler(Action.fromString(ACTION));
            times = 1;
            result = inboxWrapperTransmissionNodeHandler;

        }};

        domibusETrustExNodeConnector.messageSendSuccess(new MessageSendSuccessEvent(MSG_ID));

        new FullVerifications() {{
            inboxWrapperTransmissionNodeHandler.handleSendMessageSuccess((ETrustExAdapterDTO) any);
            times = 1;

            clearEtxLogKeysService.clearKeys();
            times = 1;
        }};
    }

    @Test(expected = IllegalArgumentException.class)
    public void messageSendSuccess_Exception() {
        final UserMessageDTO userMsgDTO = new UserMessageDTO();
        CollaborationInfoDTO collaborationInfo = new CollaborationInfoDTO();
        collaborationInfo.setAction(ACTION);
        userMsgDTO.setCollaborationInfo(collaborationInfo);
        MessageInfoDTO messageInfo = new MessageInfoDTO();
        messageInfo.setMessageId(MSG_ID);
        userMsgDTO.setMessageInfo(messageInfo);

        new Expectations() {{
            userMessageService.getMessage(MSG_ID);
            times = 1;
            result = userMsgDTO;

            followUpOperationHandlerFactory.getOperationHandler(Action.fromString(ACTION));
            times = 1;
            result = inboxWrapperTransmissionNodeHandler;

            inboxWrapperTransmissionNodeHandler.handleSendMessageSuccess((ETrustExAdapterDTO) any);
            times = 1;
            result = new IllegalArgumentException();
        }};

        domibusETrustExNodeConnector.messageSendSuccess(new MessageSendSuccessEvent(MSG_ID));

        new FullVerifications() {{
        }};
    }

    @Test
    public void messageSendFailed_happyFlow() {
        final UserMessageDTO userMsgDTO = new UserMessageDTO();
        CollaborationInfoDTO collaborationInfo = new CollaborationInfoDTO();
        collaborationInfo.setAction(ACTION);
        userMsgDTO.setCollaborationInfo(collaborationInfo);
        MessageInfoDTO messageInfo = new MessageInfoDTO();
        messageInfo.setMessageId(MSG_ID);
        userMsgDTO.setMessageInfo(messageInfo);

        new Expectations() {{
            userMessageService.getMessage(MSG_ID);
            times = 1;
            result = userMsgDTO;

            MessageAttemptDTO messageAttemptDTO = new MessageAttemptDTO();
            messageAttemptDTO.setError("ERROR");
            messageMonitorService.getAttemptsHistory(anyString);
            times = 1;
            result = Collections.singletonList(messageAttemptDTO);

            followUpOperationHandlerFactory.getOperationHandler(Action.fromString(ACTION));
            times = 1;
            result = inboxWrapperTransmissionNodeHandler;
        }};

        domibusETrustExNodeConnector.messageSendFailed(new MessageSendFailedEvent(MSG_ID));

        new FullVerifications() {{
            inboxWrapperTransmissionNodeHandler.handleSendMessageFailed((ETrustExAdapterDTO) any);
            times = 1;

            clearEtxLogKeysService.clearKeys();
        }};
    }

    @Test(expected = IllegalArgumentException.class)
    public void messageSendFailed_exception() {
        final UserMessageDTO userMsgDTO = new UserMessageDTO();
        CollaborationInfoDTO collaborationInfo = new CollaborationInfoDTO();
        collaborationInfo.setAction(ACTION);
        userMsgDTO.setCollaborationInfo(collaborationInfo);
        MessageInfoDTO messageInfo = new MessageInfoDTO();
        messageInfo.setMessageId(MSG_ID);
        userMsgDTO.setMessageInfo(messageInfo);

        new Expectations() {{
            userMessageService.getMessage(MSG_ID);
            times = 1;
            result = userMsgDTO;

            followUpOperationHandlerFactory.getOperationHandler(Action.fromString(ACTION));
            times = 1;
            result = inboxWrapperTransmissionNodeHandler;

            inboxWrapperTransmissionNodeHandler.handleSendMessageFailed((ETrustExAdapterDTO) any);
            times = 1;
            result = new IllegalArgumentException();
        }};

        domibusETrustExNodeConnector.messageSendFailed(new MessageSendFailedEvent(MSG_ID));

        new FullVerifications() {{
        }};
    }

    @Test(expected = UnsupportedOperationException.class)
    public void messageReceiveFailed() {
        MessageReceiveFailureEvent messageReceiveFailureEvent = new MessageReceiveFailureEvent();
        messageReceiveFailureEvent.setErrorResult(new ErrorResultImpl());

        domibusETrustExNodeConnector.messageReceiveFailed(messageReceiveFailureEvent);

        new FullVerifications() {{
        }};
    }

    @Test
    public void deliverMessage_happyFlow() throws Exception {
        final ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        eTrustExAdapterDTO.setAs4Service("SERVICE");
        eTrustExAdapterDTO.setAs4Action("ACTION");
        new Expectations() {{
            domibusETrustExNodeConnector.downloadMessage(anyString, (ETrustExAdapterDTO) any);
            times = 1;
            result = new ETrustExAdapterDTO();

            operationHandlerFactory.getOperationHandler(Service.fromString(eTrustExAdapterDTO.getAs4Service()), Action.fromString(eTrustExAdapterDTO.getAs4Action()));
            times = 1;
            result = (OperationHandler) dto -> false;
        }};
        domibusETrustExNodeConnector.deliverMessage(new DeliverMessageEvent(MSG_ID, FINAL_RECIPIENT));

        new FullVerifications() {{
            domainNodePluginService.setNodePluginDomain();
            clearEtxLogKeysService.clearKeys();
        }};
    }

    @Test(expected = ETrustExPluginException.class)
    public void deliverMessage_exception() throws Exception {
        final ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        eTrustExAdapterDTO.setAs4Service("SERVICE");
        eTrustExAdapterDTO.setAs4Action("ACTION");
        new Expectations() {{
            domibusETrustExNodeConnector.downloadMessage(anyString, (ETrustExAdapterDTO) any);
            times = 1;
            result = new ETrustExAdapterDTO();

            operationHandlerFactory.getOperationHandler(Service.fromString(eTrustExAdapterDTO.getAs4Service()), Action.fromString(eTrustExAdapterDTO.getAs4Action()));
            times = 1;
            result = new IllegalArgumentException();
        }};
        domibusETrustExNodeConnector.deliverMessage(new DeliverMessageEvent(MSG_ID, FINAL_RECIPIENT));

        new FullVerifications() {{
        }};
    }


    @Test(expected = ETrustExPluginException.class)
    public void deliverMessage_MessageNotFoundException() throws Exception {
        new Expectations() {{
            domibusETrustExNodeConnector.downloadMessage(anyString, (ETrustExAdapterDTO) any);
            times = 1;
            result = new MessageNotFoundException();
        }};

        domibusETrustExNodeConnector.deliverMessage(new DeliverMessageEvent(MSG_ID, FINAL_RECIPIENT) );

        new FullVerifications() {{
        }};
    }
}
package eu.europa.ec.etrustex.node.testutils.h2;

import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author François Gautier
 * @version 1.0
 * @since 30/01/2018
 */
@EnableTransactionManagement
@ActiveProfiles("IN_MEMORY_DATABASE")
public class InMemoryDataBaseConfig extends AbstractDatabaseConfig{

    @Bean
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .build();

    }

    Map<Object, String> getProperties() {
        return new HashMap<>();
    }
}

package eu.europa.ec.etrustex.node.outbox.client.adapters;

import ec.schema.xsd.ack_2.AcknowledgmentType;
import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.services.wsdl.documentwrapper_2.FaultResponse;
import ec.services.wsdl.documentwrapper_2.StoreDocumentWrapperRequest;
import ec.services.wsdl.documentwrapper_2.StoreDocumentWrapperResponse;
import eu.europa.ec.etrustex.common.jms.TextMessageCreator;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.LargePayloadDTO;
import eu.europa.ec.etrustex.common.service.WorkspaceFileService;
import eu.europa.ec.etrustex.common.util.AS4MessageConstants;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import eu.europa.ec.etrustex.node.data.model.administration.EtxUser;
import eu.europa.ec.etrustex.node.testutils.FaultResponseUtils;
import eu.europa.ec.etrustex.node.testutils.PayloadDTOBuilder;
import mockit.*;
import org.hamcrest.core.IsNull;
import org.junit.Test;
import org.springframework.jms.core.MessageCreator;

import javax.activation.DataHandler;
import javax.mail.util.ByteArrayDataSource;
import javax.xml.ws.Holder;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;

import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_FAULT;
import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_ID_HEADER;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.deserializeXMLToObj;
import static eu.europa.ec.etrustex.node.testutils.ETrustExAdapterDTOBuilder.getEmpty;
import static eu.europa.ec.etrustex.node.testutils.ReflectionUtils.getPrivateField;
import static eu.europa.ec.etrustex.node.testutils.ResourcesUtils.readResource;
import static org.apache.commons.codec.binary.StringUtils.getBytesUtf8;
import static org.junit.Assert.assertThat;

/**
 * @author Federico Martini
 * @version 1.0
 * @since 20-Dec-17
 */
@SuppressWarnings({"Duplicates", "unused"})
public class StoreDocumentWrapperAdapterTest extends BaseAdapterTest {

    private static final String SERVICE_ENDPOINT_TEST = "ServiceEndPoint";
    private static final String MESSAGE_FAULT = "MessageFault";
    private static final String FAULT_CODE = "FaultCode";

    @Injectable
    private ETrustExNodePluginProperties eTrustExNodePluginProperties;

    @Injectable
    private WorkspaceFileService etxNodePluginWorkspaceService;

    @Tested
    private StoreDocumentWrapperAdapter storeDocumentWrapperAdapter;

    private EtxUser etxUser;

    @Test
    public void performOperation_NoHeaderPayload() throws NoSuchFieldException, IllegalAccessException {
        storeDocumentWrapperAdapter.performOperation(getEmpty());

        new FullVerifications() {{
            TextMessageCreator messageCreator;
            etxNodePluginBackendToNodeJmsTemplate.send(messageCreator = withCapture());
            times = 1;

            ETrustExAdapterDTO textMessage =
                    (ETrustExAdapterDTO) deserializeXMLToObj(
                            (String) getPrivateField(TextMessageCreator.class, messageCreator, "dto"));

            assertThat(textMessage.getPayloadFromCID(CONTENT_FAULT), IsNull.notNullValue());
        }};
    }

    @Test
    public void performOperation_NoPayload() throws NoSuchFieldException, IllegalAccessException {

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(CONTENT_ID_HEADER.getValue())
                .withPayload(getBytesUtf8("<ec.schema.xsd.commonaggregatecomponents__2.HeaderType/>"))
                .build());

        storeDocumentWrapperAdapter.performOperation(dto);

        new FullVerifications() {{
            TextMessageCreator messageCreator;
            etxNodePluginBackendToNodeJmsTemplate.send(messageCreator = withCapture());
            times = 1;

            ETrustExAdapterDTO textMessage =
                    (ETrustExAdapterDTO) deserializeXMLToObj(
                            (String) getPrivateField(TextMessageCreator.class, messageCreator, "dto"));

            assertThat(textMessage.getPayloadFromCID(CONTENT_FAULT), IsNull.notNullValue());
        }};
    }

    @Test
    public void performOperation_AcknowledgeOk(final @Mocked Path filePath) throws Exception {
        DataHandler dataHandler = new DataHandler(new ByteArrayDataSource(readResource("data/outbox/document/LargePayloadDTO.xml"), "text/xml"));
        final LargePayloadDTO largePayloadDTO = new LargePayloadDTO(ContentId.CONTENT_ID_DOCUMENT.getValue(), AS4MessageConstants.MIME_TYPE_APP_OCTET, dataHandler, "JUNIT TEST", Locale.getDefault(), "certificate.pdf");
        largePayloadDTO.setFileUID("LargePayloadUID");
        Path workspaceRoot = Paths.get("data/outbox/document").normalize().toAbsolutePath();
        final Path path = workspaceRoot.resolve("LargePayloadDTO.xml");
        largePayloadDTO.setFilePath(path.toString());
        largePayloadDTO.setFileName(String.valueOf(path.getFileName()));

        new Expectations() {{

            etxNodePluginWorkspaceService.getFile(largePayloadDTO.getFilePath(), largePayloadDTO.getFileUID());
            times = 2;
            result = path;

            dwPortType.storeDocumentWrapper((StoreDocumentWrapperRequest) any, (Holder<HeaderType>) any);
            times = 1;
            result = getOkResponse();

        }};

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_HEADER.getValue())
                .withPayload(getBytesUtf8(readResource("data/outbox/document/Header.xml")))
                .build());

        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_GENERIC.getValue())
                .withPayload(getBytesUtf8(readResource("data/outbox/document/StoreDocWrapperRequest.xml")))
                .build());

        dto.addAs4Payload(largePayloadDTO);

        storeDocumentWrapperAdapter.performOperation(dto);

        new FullVerifications() {{

            etxNodePluginWorkspaceService.getFile(largePayloadDTO.getFilePath(), largePayloadDTO.getFileUID());
            times = 2;

            dwPortType.storeDocumentWrapper((StoreDocumentWrapperRequest) any, (Holder<HeaderType>) any);
            times = 1;

            etxNodePluginWorkspaceService.deleteFile(withAny(filePath));
            times = 1;

            etxNodePluginNodeToBackendJmsTemplate.send((MessageCreator) any);
            times = 1;

        }};

    }

    @Test
    public void performOperation_AcknowledgeFault(final @Mocked Path filePath) throws Exception {
        DataHandler dataHandler = new DataHandler(new ByteArrayDataSource(readResource("data/outbox/document/LargePayloadDTO.xml"), "text/xml"));
        final LargePayloadDTO largePayloadDTO = new LargePayloadDTO(ContentId.CONTENT_ID_DOCUMENT.getValue(), AS4MessageConstants.MIME_TYPE_APP_OCTET, dataHandler, "JUNIT TEST", Locale.getDefault(), "certificate.pdf");
        largePayloadDTO.setFileUID("LargePayloadUID");
        Path workspaceRoot = Paths.get("data/outbox/document").normalize().toAbsolutePath();
        final Path path = workspaceRoot.resolve("LargePayloadDTO.xml");
        largePayloadDTO.setFilePath(path.toString());
        largePayloadDTO.setFileName(String.valueOf(path.getFileName()));

        new Expectations() {{

            etxNodePluginWorkspaceService.getFile(largePayloadDTO.getFilePath(), largePayloadDTO.getFileUID());
            times = 2;
            result = path;

            dwPortType.storeDocumentWrapper((StoreDocumentWrapperRequest) any, (Holder<HeaderType>) any);
            times = 1;
            result = new FaultResponse(MESSAGE_FAULT, FaultResponseUtils.getFaultType());

        }};

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_HEADER.getValue())
                .withPayload(getBytesUtf8(readResource("data/outbox/document/Header.xml")))
                .build());

        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_GENERIC.getValue())
                .withPayload(getBytesUtf8(readResource("data/outbox/document/StoreDocWrapperRequest.xml")))
                .build());

        dto.addAs4Payload(largePayloadDTO);

        storeDocumentWrapperAdapter.performOperation(dto);

        new FullVerifications() {{

            etxNodePluginWorkspaceService.getFile(largePayloadDTO.getFilePath(), largePayloadDTO.getFileUID());
            times = 2;

            dwPortType.storeDocumentWrapper((StoreDocumentWrapperRequest) any, (Holder<HeaderType>) any);
            times = 1;

            etxNodePluginWorkspaceService.deleteFile(withAny(filePath));
            times = 1;

            etxNodePluginNodeToBackendJmsTemplate.send((MessageCreator) any);
            times = 1;

        }};

    }

    @Test
    public void performOperation_Exception(final @Mocked Path filePath) throws Exception {
        DataHandler dataHandler = new DataHandler(new ByteArrayDataSource(readResource("data/outbox/document/LargePayloadDTO.xml"), "text/xml"));
        final LargePayloadDTO largePayloadDTO = new LargePayloadDTO(ContentId.CONTENT_ID_DOCUMENT.getValue(), AS4MessageConstants.MIME_TYPE_APP_OCTET, dataHandler, "JUNIT TEST", Locale.getDefault(), "certificate.pdf");
        largePayloadDTO.setFileUID("LargePayloadUID");
        Path workspaceRoot = Paths.get("data/outbox/document").normalize().toAbsolutePath();
        final Path path = workspaceRoot.resolve("LargePayloadDTO.xml");
        largePayloadDTO.setFilePath(path.toString());
        largePayloadDTO.setFileName(String.valueOf(path.getFileName()));

        new Expectations() {{

            etxNodePluginWorkspaceService.getFile(largePayloadDTO.getFilePath(), largePayloadDTO.getFileUID());
            returns(path, new Exception("File not found"));
            times = 2;

        }};

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_HEADER.getValue())
                .withPayload(getBytesUtf8(readResource("data/outbox/document/Header.xml")))
                .build());

        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_GENERIC.getValue())
                .withPayload(getBytesUtf8(readResource("data/outbox/document/StoreDocWrapperRequest.xml")))
                .build());

        dto.addAs4Payload(largePayloadDTO);

        storeDocumentWrapperAdapter.performOperation(dto);

        new FullVerifications() {{

            dwPortType.storeDocumentWrapper((StoreDocumentWrapperRequest) any, (Holder<HeaderType>) any);
            times = 1;

            etxNodePluginNodeToBackendJmsTemplate.send((MessageCreator) any);
            times = 1;

        }};

    }

    @Test
    public void performOperation_NoLargePayload_Exception(final @Mocked Path filePath) throws Exception {
        DataHandler dataHandler = new DataHandler(new ByteArrayDataSource(readResource("data/outbox/document/LargePayloadDTO.xml"), "text/xml"));
        final LargePayloadDTO largePayloadDTO = new LargePayloadDTO(ContentId.CONTENT_ID_DOCUMENT.getValue(), AS4MessageConstants.MIME_TYPE_APP_OCTET, dataHandler, "JUNIT TEST", Locale.getDefault(), "certificate.pdf");

        new Expectations() {{

        }};

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_HEADER.getValue())
                .withPayload(getBytesUtf8(readResource("data/outbox/document/Header.xml")))
                .build());

        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_GENERIC.getValue())
                .withPayload(getBytesUtf8(readResource("data/outbox/document/StoreDocWrapperRequest.xml")))
                .build());

        dto.addAs4Payload(largePayloadDTO);

        storeDocumentWrapperAdapter.performOperation(dto);

        new FullVerifications() {{

            etxNodePluginBackendToNodeJmsTemplate.send((MessageCreator) any);
            times = 1;

        }};

    }

    private StoreDocumentWrapperResponse getOkResponse() {
        StoreDocumentWrapperResponse response = new StoreDocumentWrapperResponse();
        AcknowledgmentType ack = new AcknowledgmentType();
        response.setAck(ack);
        return response;
    }

}
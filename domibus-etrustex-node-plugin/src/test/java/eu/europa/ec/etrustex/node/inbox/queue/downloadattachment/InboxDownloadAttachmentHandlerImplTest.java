package eu.europa.ec.etrustex.node.inbox.queue.downloadattachment;


import ec.services.wsdl.documentwrapper_2.FaultResponse;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.node.data.model.attachment.AttachmentState;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import eu.europa.ec.etrustex.node.inbox.client.adapter.RetrieveDocumentWrapperAdapter;
import eu.europa.ec.etrustex.node.inbox.queue.error.InboxErrorProducer;
import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxMessageTransmissionProducer;
import eu.europa.ec.etrustex.node.service.AttachmentService;
import mockit.*;
import mockit.integration.junit4.JMockit;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseCodeType;
import oasis.names.specification.ubl.schema.xsd.fault_1.FaultType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.fail;

/**
 * JUnit for {@link InboxDownloadAttachmentHandlerImpl}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 13/12/2017
 */
@RunWith(JMockit.class)
public class InboxDownloadAttachmentHandlerImplTest {

    private final Long messageId = RandomUtils.nextLong(0, 99999);
    private final Long attachmentId = RandomUtils.nextLong(0, 99999);

    private final InboxDownloadAttachmentQueueMessage queueMessage = new InboxDownloadAttachmentQueueMessage(
            MessageType.MESSAGE_BUNDLE,
            MessageDirection.NODE_TO_BACKEND,
            messageId,
            attachmentId);
    private final EtxAttachment etxAttachment = new EtxAttachment();

    @Injectable
    private AttachmentService attachmentService;

    @Injectable
    private RetrieveDocumentWrapperAdapter retrieveDocumentWrapperAdapter;

    @Injectable
    private InboxMessageTransmissionProducer inboxMessageTransmissionProducer;

    @Injectable
    private InboxErrorProducer inboxErrorProducer;

    @Injectable
    private InboxDownloadAttachmentProducer inboxDownloadAttachmentProducer;

    @Tested
    private InboxDownloadAttachmentHandlerImpl inboxDownloadAttachmentHandler;

    @Before
    public void setUp() throws Exception {
        etxAttachment.setStateType(AttachmentState.CREATED);
        etxAttachment.setId(attachmentId);
    }

    @Test
    public void handleDownloadAttachment_AttachmentFound_RetrieveWrapperSuccessfully() throws Exception {

        new Expectations(inboxDownloadAttachmentHandler) {{
            inboxDownloadAttachmentHandler.loadValidateDownloadAttachment(messageId, attachmentId);
            times = 1;
            result = etxAttachment;

        }};

        //tested method
        inboxDownloadAttachmentHandler.handleDownloadAttachment(queueMessage);

        new FullVerifications() {{
            EtxAttachment etxAttachmentActual;
            retrieveDocumentWrapperAdapter.downloadAttachment(etxAttachmentActual = withCapture());
            times = 1;
            Assert.assertEquals("etx attachment should match", etxAttachment, etxAttachmentActual);

            Long messageIdActual, attachmentIdActual;
            inboxMessageTransmissionProducer.triggerWrapperTransmissionToBackend(messageIdActual = withCapture(), attachmentIdActual = withCapture());
            Assert.assertEquals("message id should match", messageId, messageIdActual);
            Assert.assertEquals("attachment id should match", attachmentId, attachmentIdActual);
            times = 1;

        }};
    }


    @Test
    public void handleDownloadAttachment_RetrieveWrapperFailsWithFaultResponse_TriggerError() throws Exception {
        final FaultResponse faultResponse = new FaultResponse("unable to retrieve wrapper", new FaultType());

        new Expectations(inboxDownloadAttachmentHandler) {{
            inboxDownloadAttachmentHandler.loadValidateDownloadAttachment(messageId, attachmentId);
            times = 1;
            result = etxAttachment;

            retrieveDocumentWrapperAdapter.downloadAttachment(etxAttachment);
            times = 1;
            result = faultResponse;
        }};

        //tested method
        inboxDownloadAttachmentHandler.handleDownloadAttachment(queueMessage);

        new FullVerifications(inboxDownloadAttachmentHandler) {{
            InboxDownloadAttachmentQueueMessage queueMessageActual;
            inboxDownloadAttachmentHandler.handleFaultResponse(faultResponse, queueMessageActual = withCapture());
            times = 1;
            Assert.assertEquals("queue message should match", queueMessage, queueMessageActual);
        }};
    }


    @Test
    public void handleDownloadAttachment_RetrieveWrapperFailsWithException_TriggerDownloadAgain() throws Exception {

        new Expectations(inboxDownloadAttachmentHandler) {{
            inboxDownloadAttachmentHandler.loadValidateDownloadAttachment(messageId, attachmentId);
            times = 1;
            result = etxAttachment;

            retrieveDocumentWrapperAdapter.downloadAttachment(etxAttachment);
            times = 1;
            result = new java.net.ConnectException("unable to reach node webservice");
        }};

        //tested method
        inboxDownloadAttachmentHandler.handleDownloadAttachment(queueMessage);

        new FullVerifications() {{
            InboxDownloadAttachmentQueueMessage queueMessageActual;
            inboxDownloadAttachmentProducer.triggerDownloadAttachment(queueMessageActual = withCapture());
            times = 1;
            Assert.assertEquals("queue message should match", queueMessage, queueMessageActual);
        }};
    }


    @Test
    public void loadValidateDownloadAttachment_AttachmentMessageAreValid_ReturnAttachment() throws Exception {
        final EtxMessage etxMessage = new EtxMessage();
        etxMessage.setId(messageId);
        etxAttachment.setMessage(etxMessage);

        new Expectations() {{
            attachmentService.findAttachmentById(attachmentId);
            times = 1;
            result = etxAttachment;
        }};

        //tested method
        EtxAttachment attachment = inboxDownloadAttachmentHandler.loadValidateDownloadAttachment(messageId, attachmentId);
        Assert.assertNotNull(attachment);

        new FullVerifications() {{
        }};
    }

    @Test
    public void loadValidateDownloadAttachment_AttachmentNull_ExceptionThrown() throws Exception {

        new Expectations() {{
            attachmentService.findAttachmentById(attachmentId);
            times = 1;
            result = null;
        }};

        try {
            //tested method
            inboxDownloadAttachmentHandler.loadValidateDownloadAttachment(messageId, attachmentId);
            fail("ETrustExPluginException failed");
        } catch (ETrustExPluginException ex) {
            Assert.assertEquals("error code must match", ETrustExError.DEFAULT, ex.getError());
            Assert.assertEquals("error message must match", "Attachment [ID: " + attachmentId + "] not found", ex.getMessage());
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void loadValidateDownloadAttachment_AttachmentInvalidState_ExceptionThrown() throws Exception {

        etxAttachment.setStateType(AttachmentState.FAILED);

        new Expectations() {{
            attachmentService.findAttachmentById(attachmentId);
            times = 1;
            result = etxAttachment;
        }};

        try {
            //tested method
            inboxDownloadAttachmentHandler.loadValidateDownloadAttachment(messageId, attachmentId);
            fail("ETrustExPluginException failed");
        } catch (ETrustExPluginException ex) {
            Assert.assertEquals("error code must match", ETrustExError.ETX_009, ex.getError());
            Assert.assertEquals("error message must match", "Attachment [ID: " + attachmentId +
                            "] has an invalid state: " + etxAttachment.getStateType() + " from expected one: " + AttachmentState.CREATED.name(),
                    ex.getMessage());
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void loadValidateDownloadAttachment_MessageInvalidState_ExceptionThrown() throws Exception {
        final EtxMessage etxMessage = new EtxMessage();
        etxMessage.setId(messageId);
        etxMessage.setMessageState(MessageState.FAILED);
        etxAttachment.setMessage(etxMessage);

        new Expectations() {{
            attachmentService.findAttachmentById(attachmentId);
            times = 1;
            result = etxAttachment;
        }};

        try {
            //tested method
            inboxDownloadAttachmentHandler.loadValidateDownloadAttachment(messageId, attachmentId);
            fail("ETrustExPluginException failed");
        } catch (ETrustExPluginException ex) {
            Assert.assertEquals("error code must match", ETrustExError.ETX_009, ex.getError());
            Assert.assertEquals("error message must match", "message bundle ID: " + messageId +
                            " is already in state: " + MessageState.FAILED.name(),
                    ex.getMessage());
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void handleFaultResponse() throws Exception {
        final FaultResponse faultResponse = new FaultResponse("error occurred while retrieving document wrapper",
                new FaultType());

        //tested method
        inboxDownloadAttachmentHandler.handleFaultResponse(faultResponse, queueMessage);

        new FullVerifications() {{
            inboxDownloadAttachmentHandler.updateQueueMessageErrorDetails(queueMessage, faultResponse);

            inboxErrorProducer.triggerError(queueMessage);
        }};
    }

    @Test
    public void handleException_QueueMessageWithoutErrorDetails_TriggerDownloadQueue(final @Mocked InboxDownloadAttachmentQueueMessage downloadAttachmentQueueMessage) throws Exception {
        final Exception exception = new ConnectException("unable to reach Node webservice");

        new Expectations() {{
            downloadAttachmentQueueMessage.getErrorCode();
            times = 1;
            result = StringUtils.EMPTY;

            downloadAttachmentQueueMessage.getErrorDescription();
            times = 1;
            result = StringUtils.EMPTY;
        }};

        //tested method
        inboxDownloadAttachmentHandler.handleException(exception, downloadAttachmentQueueMessage);

        new FullVerifications() {{
            String actualValue;
            downloadAttachmentQueueMessage.setErrorCode(actualValue = withCapture());
            Assert.assertEquals("error code must match", ETrustExError.DEFAULT.name(), actualValue);


            downloadAttachmentQueueMessage.setErrorDescription(actualValue = withCapture());
            Assert.assertEquals("error description must match",
                    exception.getMessage() + " caused by: " + ExceptionUtils.getRootCauseMessage(exception),
                    actualValue);

            inboxDownloadAttachmentProducer.triggerDownloadAttachment(downloadAttachmentQueueMessage);
        }};
    }

    @Test
    public void handleException_QueueMessageWithErrorDetails_ExceptionThrown(final @Mocked InboxDownloadAttachmentQueueMessage downloadAttachmentQueueMessage) throws Exception {
        final Exception exception = new ConnectException("unable to reach Node webservice");

        new Expectations() {{
            downloadAttachmentQueueMessage.getErrorCode();
            times = 1;
            result = ETrustExError.DEFAULT.getCode();

        }};

        try {
            //tested method
            inboxDownloadAttachmentHandler.handleException(exception, downloadAttachmentQueueMessage);
            fail("ETrustExPluginException failed");
        } catch (ETrustExPluginException ex) {
            Assert.assertEquals("error code must match", ETrustExError.DEFAULT, ex.getError());
            Assert.assertEquals("error message must match", exception.getMessage(), ex.getMessage());
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void updateQueueMessageErrorDetails() throws Exception {

        final String responseCode = "BDL:7";
        final String errorDescription1 = "error description 1";
        final String errorDescription2 = "error description 2";
        final ResponseCodeType responseCodeType = new ResponseCodeType();
        responseCodeType.setValue(responseCode);
        final FaultType faultType = new FaultType();
        faultType.setResponseCode(responseCodeType);

        final List<DescriptionType> descriptionTypeList = new ArrayList<>();
        final DescriptionType descriptionType1 = new DescriptionType();
        descriptionType1.setValue(errorDescription1);
        descriptionTypeList.add(descriptionType1);
        final DescriptionType descriptionType2 = new DescriptionType();
        descriptionType2.setValue(errorDescription2);
        descriptionTypeList.add(descriptionType2);
        faultType.getDescription().addAll(descriptionTypeList);

        final FaultResponse faultResponse = new FaultResponse("error occurred while retrieving document wrapper",
                faultType);

        //tested method
        inboxDownloadAttachmentHandler.updateQueueMessageErrorDetails(queueMessage, faultResponse);

        Assert.assertEquals("error code must match", responseCode, queueMessage.getErrorCode());
        Assert.assertTrue("error description 1 should be present", queueMessage.getErrorDescription().contains(errorDescription1));
        Assert.assertTrue("error description 2 should be present", queueMessage.getErrorDescription().contains(errorDescription2));

    }
}
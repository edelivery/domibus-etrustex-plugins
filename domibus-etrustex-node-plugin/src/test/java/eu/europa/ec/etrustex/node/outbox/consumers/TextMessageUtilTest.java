package eu.europa.ec.etrustex.node.outbox.consumers;

import com.shazam.shazamcrest.matcher.Matchers;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import eu.europa.ec.etrustex.node.testutils.ETrustExAdapterDTOBuilder;
import eu.europa.ec.etrustex.node.testutils.ResourcesUtils;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import static org.hamcrest.core.Is.is;

/**
 * @author François Gautier
 * @version 1.0
 * @since 09/03/2018
 */
@RunWith(JMockit.class)
public class TextMessageUtilTest {

    @Test
    public void getETrustExAdapterDTO_NPE() {
        try {
            TextMessageUtil.getETrustExAdapterDTO(null);
            Assert.fail();
        } catch (ETrustExPluginException e) {
            Assert.assertThat(e.getError(), is(ETrustExError.DEFAULT));
        }
        new FullVerifications() {{
        }};
    }

    @Test
    public void getETrustExAdapterDTO_empty(final @Mocked TextMessage txtMessage) throws JMSException {
        new Expectations() {{
            txtMessage.getText();
            minTimes = 1;
            maxTimes = 2;
            result = "";
        }};
        try {
            TextMessageUtil.getETrustExAdapterDTO(txtMessage);
            Assert.fail();
        } catch (ETrustExPluginException e) {
            Assert.assertThat(e.getError(), is(ETrustExError.DEFAULT));
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void getETrustExAdapterDTO_blank(final @Mocked TextMessage txtMessage) throws JMSException {
        new Expectations() {{
            txtMessage.getText();
            minTimes = 1;
            maxTimes = 2;
            result = null;
        }};
        try {
            TextMessageUtil.getETrustExAdapterDTO(txtMessage);
            Assert.fail();
        } catch (ETrustExPluginException e) {
            Assert.assertThat(e.getError(), is(ETrustExError.DEFAULT));
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void getETrustExAdapterDTO(final @Mocked TextMessage txtMessage) throws JMSException {
        final ETrustExAdapterDTO eTrustExAdapterDTO = ETrustExAdapterDTOBuilder.getEmpty();
        new Expectations() {{
            txtMessage.getText();
            times = 1;
            result = TransformerUtils.serializeObjToXML(eTrustExAdapterDTO);
        }};
        ETrustExAdapterDTO result = TextMessageUtil.getETrustExAdapterDTO(txtMessage);

        Assert.assertThat(result, Matchers.sameBeanAs(eTrustExAdapterDTO));

        new FullVerifications() {{
        }};
    }

    @Test
    public void getETrustExAdapterDTO_SoapFaultJMSErrorMessage(final @Mocked TextMessage txtMessage) throws JMSException {
        final String faultErrorJMSTextMsg = ResourcesUtils.readResource("data/outbox/document/ETrustExAdapterDTO_InternalServerErrorAtEtxNode.xml");
        new Expectations(){{
            txtMessage.getText();
            result = faultErrorJMSTextMsg;
        }};

        ETrustExAdapterDTO eTrustExAdapterDTO = TextMessageUtil.getETrustExAdapterDTO(txtMessage);
        Assert.assertNotNull(eTrustExAdapterDTO);
        new FullVerifications(){{}};

    }
}
package eu.europa.ec.etrustex.node.service.security;

import eu.europa.ec.etrustex.node.data.dao.PartyDao;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

/**
 * JUnit for {#link IdentityManagerImpl}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 12/12/2017
 */
@RunWith(JMockit.class)
public class IdentityManagerImplTest {

    @Injectable
    private PartyDao partyDao;

    @Tested
    private IdentityManagerImpl identityManager;

    @Test
    public void searchPartyByUuid_PartyUuidValid_ReturnExtParty() {
        final String partyUuid = "TEST";
        final EtxParty etxParty = new EtxParty();
        etxParty.setPartyUuid(partyUuid);

        new Expectations() {{
            partyDao.findByPartyUUID(partyUuid);
            times = 1;
            result = etxParty;
        }};

        //tested method
        identityManager.searchPartyByUuid(partyUuid);

        new FullVerifications() {{
            String partyUuidActual;
            partyDao.findByPartyUUID(partyUuidActual = withCapture());
            times = 1;
            assertEquals("party uuid should match", partyUuid, partyUuidActual);
        }};
    }

    @Test
    public void searchPartyByUuid_PartyUuidInvalid_ReturnNull() {
        final String partyUuid = null;

        //tested method
        EtxParty party = identityManager.searchPartyByUuid(partyUuid);
        Assert.assertNull(party);

        new FullVerifications() {{
        }};
    }

}
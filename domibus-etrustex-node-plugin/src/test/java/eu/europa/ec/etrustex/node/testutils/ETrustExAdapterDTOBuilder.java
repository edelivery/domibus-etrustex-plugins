package eu.europa.ec.etrustex.node.testutils;

import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;

/**
 * @author François Gautier
 * @version 1.0
 * @since 20-Nov-17
 */
public class ETrustExAdapterDTOBuilder {
    private String action;
    private String agreementRef;
    private String conversationId;
    private String finalRecipient;
    private String fromPartyId;
    private String fromPartyIdType;
    private String fromPartyRole;
    private String messageId;
    private String originalSender;
    private String refToMessageId;
    private String service;
    private String serviceType;
    private String toPartyId;
    private String toPartyIdType;
    private String toPartyRole;
    private String attachmentId;
    private String backendMessageId;

    private ETrustExAdapterDTOBuilder() {
    }

    public static ETrustExAdapterDTOBuilder getInstance(){
      return new ETrustExAdapterDTOBuilder();
    }

    public ETrustExAdapterDTO build(){
        ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        eTrustExAdapterDTO.setAs4Action(action);
        eTrustExAdapterDTO.setAs4AgreementRef(agreementRef);
        eTrustExAdapterDTO.setAs4ConversationId(conversationId);
        eTrustExAdapterDTO.setAs4FinalRecipient(finalRecipient);
        eTrustExAdapterDTO.setAs4FromPartyId(fromPartyId);
        eTrustExAdapterDTO.setAs4FromPartyIdType(fromPartyIdType);
        eTrustExAdapterDTO.setAs4FromPartyRole(fromPartyRole);
        eTrustExAdapterDTO.setAs4MessageId(messageId);
        eTrustExAdapterDTO.setAs4OriginalSender(originalSender);
        eTrustExAdapterDTO.setAs4RefToMessageId(refToMessageId);
        eTrustExAdapterDTO.setAs4Service(service);
        eTrustExAdapterDTO.setAs4ServiceType(serviceType);
        eTrustExAdapterDTO.setAs4ToPartyId(toPartyId);
        eTrustExAdapterDTO.setAs4ToPartyIdType(toPartyIdType);
        eTrustExAdapterDTO.setAs4ToPartyRole(toPartyRole);
        eTrustExAdapterDTO.setAttachmentId(attachmentId);
        eTrustExAdapterDTO.setEtxBackendMessageId(backendMessageId);
        return eTrustExAdapterDTO;
    }

    public static ETrustExAdapterDTO getEmpty(){
        return ETrustExAdapterDTOBuilder
                .getInstance()
                .withAction("")
                .withAgreementRef("")
                .withAttachmentId("")
                .withBackendMessageId("")
                .withConversationId("")
                .withFinalRecipient("")
                .withFromPartyId("")
                .withFromPartyIdType("")
                .withFromPartyRole("")
                .withMessageId("")
                .withOriginalSender("")
                .withRefToMessageId("")
                .withService("")
                .withServiceType("")
                .withToPartyId("")
                .withToPartyIdType("")
                .withToPartyRole("")
                .build();
    }

    public ETrustExAdapterDTOBuilder withAction(String action) {
        this.action = action;
        return this;
    }

    public ETrustExAdapterDTOBuilder withAgreementRef(String agreementRef) {
        this.agreementRef = agreementRef;
        return this;
    }

    public ETrustExAdapterDTOBuilder withConversationId(String conversationId) {
        this.conversationId = conversationId;
        return this;
    }

    public ETrustExAdapterDTOBuilder withFinalRecipient(String finalRecipient) {
        this.finalRecipient = finalRecipient;
        return this;
    }

    public ETrustExAdapterDTOBuilder withFromPartyId(String fromPartyId) {
        this.fromPartyId = fromPartyId;
        return this;
    }

    public ETrustExAdapterDTOBuilder withFromPartyIdType(String fromPartyIdType) {
        this.fromPartyIdType = fromPartyIdType;
        return this;
    }

    public ETrustExAdapterDTOBuilder withFromPartyRole(String fromPartyRole) {
        this.fromPartyRole = fromPartyRole;
        return this;
    }

    public ETrustExAdapterDTOBuilder withMessageId(String messageId) {
        this.messageId = messageId;
        return this;
    }

    public ETrustExAdapterDTOBuilder withOriginalSender(String originalSender) {
        this.originalSender = originalSender;
        return this;
    }

    public ETrustExAdapterDTOBuilder withRefToMessageId(String refToMessageId) {
        this.refToMessageId = refToMessageId;
        return this;
    }

    public ETrustExAdapterDTOBuilder withService(String service) {
        this.service = service;
        return this;
    }

    public ETrustExAdapterDTOBuilder withServiceType(String serviceType) {
        this.serviceType = serviceType;
        return this;
    }

    public ETrustExAdapterDTOBuilder withToPartyId(String toPartyId) {
        this.toPartyId = toPartyId;
        return this;
    }

    public ETrustExAdapterDTOBuilder withToPartyIdType(String toPartyIdType) {
        this.toPartyIdType = toPartyIdType;
        return this;
    }

    public ETrustExAdapterDTOBuilder withToPartyRole(String toPartyRole) {
        this.toPartyRole = toPartyRole;
        return this;
    }

    public ETrustExAdapterDTOBuilder withAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
        return this;
    }

    public ETrustExAdapterDTOBuilder withBackendMessageId(String backendMessageId) {
        this.backendMessageId = backendMessageId;
        return this;
    }
}

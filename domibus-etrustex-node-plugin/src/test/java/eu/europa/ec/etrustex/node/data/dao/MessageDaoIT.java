package eu.europa.ec.etrustex.node.data.dao;

import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Calendar;
import java.util.UUID;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 30/01/2018
 */
public class MessageDaoIT extends AbstractDaoIT<EtxMessage>{

    @Autowired
    private MessageDao messageDao;
    @Autowired
    private PartyDao partyDao;

    @Test
    public void findMessageByDomibusMessageId_ok() {
        EtxMessage result = messageDao.findMessageByDomibusMessageId("2e09ab8a-3802-4318-9de6-b2a74af8e82c@domibus.eu");
        assertThat(result, is(notNullValue()));
    }
    @Test
    public void findMessageByDomibusMessageId_notFound() {
        EtxMessage result = messageDao.findMessageByDomibusMessageId("NOT_FOUND");
        assertThat(result, is(nullValue()));
    }

    @Override
    protected Long getExistingId() {
        return 10L;
    }

    @Override
    protected DaoBase<EtxMessage> getDao() {
        return messageDao;
    }

    @Override
    protected EtxMessage getNewEntity() {
        EtxMessage etxMessage = new EtxMessage();
        etxMessage.setId(null);
        etxMessage.setDirectionType(MessageDirection.NODE_TO_BACKEND);
        etxMessage.setSender(partyDao.findById(101L));
        etxMessage.setReceiver(partyDao.findById(102L));
        etxMessage.setIssueDateTime(Calendar.getInstance());
        etxMessage.setMessageState(MessageState.CREATED);
        etxMessage.setMessageType(MessageType.MESSAGE_BUNDLE);
        etxMessage.setMessageUuid(UUID.randomUUID().toString());
        return etxMessage;
    }
}
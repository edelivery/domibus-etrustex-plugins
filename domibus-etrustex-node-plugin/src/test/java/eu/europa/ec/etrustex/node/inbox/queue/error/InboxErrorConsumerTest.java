package eu.europa.ec.etrustex.node.inbox.queue.error;

import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import eu.europa.ec.etrustex.node.inbox.queue.downloadattachment.InboxDownloadAttachmentQueueMessage;
import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxMessageTransmissionQueueMessage;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author François Gautier
 * @version 1.0
 * @since 14-Nov-17
 */
@RunWith(JMockit.class)
public class InboxErrorConsumerTest {

    @Injectable
    private InboxErrorMessageTransmissionHandler inboxErrorMessageTransmissionHandler;

    @Injectable
    private InboxErrorDownloadHandler inboxErrorDownloadHandler;
    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Injectable
    private DomainNodePluginService domainNodePluginService;
    @Tested
    private InboxErrorConsumer inboxErrorConsumer;

    private InboxMessageTransmissionQueueMessage transmissionQueueMessage;
    private InboxDownloadAttachmentQueueMessage downloadAttachmentQueueMessage;

    @Before
    public void setUp() {
        transmissionQueueMessage = new InboxMessageTransmissionQueueMessage();
        downloadAttachmentQueueMessage = new InboxDownloadAttachmentQueueMessage();
    }

    @Test
    public void transmissionInError_messageStatus_ok() {
        transmissionQueueMessage.setMessageType(MessageType.MESSAGE_STATUS);

        inboxErrorConsumer.handleMessage(transmissionQueueMessage);

        new FullVerifications() {{
            inboxErrorMessageTransmissionHandler.handleErrorWithoutNotification(transmissionQueueMessage);
            times = 1;

            domainNodePluginService.setNodePluginDomain();
            times = 1;
            clearEtxLogKeysService.clearKeys();
            times = 1;
        }};
    }

    @Test
    public void transmissionInError_messageBundle_ok() {
        transmissionQueueMessage.setMessageType(MessageType.MESSAGE_BUNDLE);

        inboxErrorConsumer.handleMessage(transmissionQueueMessage);

        new FullVerifications() {{
            inboxErrorMessageTransmissionHandler.handleErrorWithNotification(transmissionQueueMessage);
            times = 1;

            domainNodePluginService.setNodePluginDomain();
            times = 1;
            clearEtxLogKeysService.clearKeys();
            times = 1;
        }};
    }

    @Test
    public void downloadInError_ok() {
        inboxErrorConsumer.handleMessage(downloadAttachmentQueueMessage);

        new FullVerifications() {{
            inboxErrorDownloadHandler.handleErrorWithNotification(downloadAttachmentQueueMessage);
            times = 1;

            domainNodePluginService.setNodePluginDomain();
            times = 1;
            clearEtxLogKeysService.clearKeys();
            times = 1;
        }};
    }
}
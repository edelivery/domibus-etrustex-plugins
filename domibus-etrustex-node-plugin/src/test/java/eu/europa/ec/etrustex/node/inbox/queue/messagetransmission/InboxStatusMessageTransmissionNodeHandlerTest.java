package eu.europa.ec.etrustex.node.inbox.queue.messagetransmission;

import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseRequest;
import eu.domibus.messaging.MessagingProcessingException;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.common.util.ConversationIdKey;
import eu.europa.ec.etrustex.common.util.ETrustExPluginAS4Properties;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import eu.europa.ec.etrustex.node.inbox.queue.error.InboxErrorProducer;
import eu.europa.ec.etrustex.node.service.MessageServicePlugin;
import eu.europa.ec.etrustex.node.service.NodeConnectorSubmissionService;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.UUID;

import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_FAULT;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.getObjectFromXml;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.serializeObjToXML;
import static eu.europa.ec.etrustex.node.testutils.ResourcesUtils.readResource;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * JUNIT tests for the methods of InboxStatusMessageTransmissionHandler.
 *
 * @author Federico Martini, Catalin Enache
 * @version 1.0
 * @since 16/11/2017
 */
@RunWith(JMockit.class)
public class InboxStatusMessageTransmissionNodeHandlerTest {

    @Injectable
    private NodeConnectorSubmissionService nodeConnectorSubmissionService;

    @Injectable
    private ETrustExPluginAS4Properties etxPluginAS4Properties;

    @Injectable
    private InboxErrorProducer inboxErrorProducer;

    @Injectable
    private InboxMessageTransmissionProducer inboxMessageTransmissionProducer;

    @Injectable
    private MessageServicePlugin messageService;

    @Tested
    private InboxStatusMessageTransmissionNodeHandler inboxStatusMessageTransmissionNodeHandler;

    private String domibusMessageId;

    private Long messageId;

    @Before
    public void setUp() {
        domibusMessageId = UUID.randomUUID().toString();
        messageId = RandomUtils.nextLong(0, 99999);
    }

    @Test
    public void testHandleMessageTransmissionToBackend_HappyFlow() {

        final InboxMessageTransmissionQueueMessage queueMessage = new InboxMessageTransmissionQueueMessage(
                MessageType.MESSAGE_STATUS, MessageDirection.NODE_TO_BACKEND, messageId
        );

        final EtxMessage etxMessage = getEtxMessage(domibusMessageId);
        final ETrustExAdapterDTO dto = getDto();

        new Expectations(inboxStatusMessageTransmissionNodeHandler) {{

            inboxStatusMessageTransmissionNodeHandler.loadValidateMessage(queueMessage);
            times = 1;
            result = etxMessage;

            inboxStatusMessageTransmissionNodeHandler.populateEtxAdapterDTOForStatusMessage(etxMessage);
            times = 1;
            result = dto;

            nodeConnectorSubmissionService.submitToDomibus(dto);
            times = 1;
            result = domibusMessageId;

        }};

        //tested method
        inboxStatusMessageTransmissionNodeHandler.handleMessageTransmissionToBackend(queueMessage);

        new FullVerifications(inboxStatusMessageTransmissionNodeHandler) {{
            EtxMessage etxMessageActual;
            String domibusMsgIdActual;
            inboxStatusMessageTransmissionNodeHandler.handleDomibusSubmissionSuccess(etxMessageActual = withCapture(), domibusMsgIdActual = withCapture());
            Assert.assertEquals("etx message should match", etxMessage, etxMessageActual);
            Assert.assertEquals("domibus message id should match", domibusMessageId, domibusMsgIdActual);
        }};
    }

    @Test
    public void testHandleMessageTransmissionToBackend_ExceptionThrown() throws  Exception {

        final InboxMessageTransmissionQueueMessage queueMessage = new InboxMessageTransmissionQueueMessage(
                MessageType.MESSAGE_STATUS, MessageDirection.NODE_TO_BACKEND, messageId
        );

        final EtxMessage etxMessage = getEtxMessage(domibusMessageId);
        final ETrustExAdapterDTO dto = getDto();

        new Expectations(inboxStatusMessageTransmissionNodeHandler) {{

            inboxStatusMessageTransmissionNodeHandler.loadValidateMessage(queueMessage);
            times = 1;
            result = etxMessage;

            inboxStatusMessageTransmissionNodeHandler.populateEtxAdapterDTOForStatusMessage(etxMessage);
            times = 1;
            result = dto;

            nodeConnectorSubmissionService.submitToDomibus(dto);
            times = 1;
            result = new MessagingProcessingException("domibus exception");

        }};

        //tested method
        inboxStatusMessageTransmissionNodeHandler.handleMessageTransmissionToBackend(queueMessage);

        new FullVerifications(inboxStatusMessageTransmissionNodeHandler) {{
            InboxMessageTransmissionQueueMessage queueMessageActual;
            Exception exceptionActual;
            inboxStatusMessageTransmissionNodeHandler.handleError(queueMessageActual = withCapture(), exceptionActual = withCapture());
            Assert.assertEquals("queue message should match", queueMessage, queueMessageActual);
            Assert.assertEquals("MessagingProcessingException expected", MessagingProcessingException.class, exceptionActual.getClass());
        }};
    }

    @Test
    public void testLoadValidateMessage_HappyFlow() {
        final InboxMessageTransmissionQueueMessage queueMessage = new InboxMessageTransmissionQueueMessage(
                MessageType.MESSAGE_STATUS, MessageDirection.NODE_TO_BACKEND, messageId
        );

        final EtxMessage etxMessage = getEtxMessage(domibusMessageId);

        new Expectations(inboxStatusMessageTransmissionNodeHandler) {{
            inboxStatusMessageTransmissionNodeHandler.loadValidateEtxMessageForTransmission(messageId);
            times = 1;
            result = etxMessage;
        }};

        //tested method
        inboxStatusMessageTransmissionNodeHandler.loadValidateMessage(queueMessage);

        new FullVerifications() {{
        }};
    }

    @Test
    public void testLoadValidateMessage_WrongMessageType() {
        final InboxMessageTransmissionQueueMessage queueMessage = new InboxMessageTransmissionQueueMessage(
                MessageType.MESSAGE_STATUS, MessageDirection.NODE_TO_BACKEND, messageId
        );

        final EtxMessage etxMessage = getEtxMessage(domibusMessageId);
        etxMessage.setMessageType(MessageType.MESSAGE_BUNDLE);

        new Expectations(inboxStatusMessageTransmissionNodeHandler) {{
            inboxStatusMessageTransmissionNodeHandler.loadValidateEtxMessageForTransmission(messageId);
            times = 1;
            result = etxMessage;
        }};


        try {
            //tested method
            inboxStatusMessageTransmissionNodeHandler.loadValidateMessage(queueMessage);
            Assert.fail("ETrustExPluginException expected");
        } catch (ETrustExPluginException e) {
            Assert.assertEquals("error type must match", ETrustExError.ETX_009, e.getError());
            Assert.assertEquals("error message should match", "Message ID:[" + messageId + "] specified for Inbox Message transmission to backend is not a Status Message.", e.getMessage());
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void testPopulateEtxAdapterDTOForStatusMessage_HappyFlow(final @Mocked ETrustExAdapterDTO dto) {

        final EtxMessage etxMessage = getEtxMessage(domibusMessageId);


        new Expectations(inboxStatusMessageTransmissionNodeHandler) {{
            inboxStatusMessageTransmissionNodeHandler.createEtxAdapterDTOForInboxTransmission(etxMessage);
            times = 1;
            result = dto;
        }};

        //tested method
        inboxStatusMessageTransmissionNodeHandler.populateEtxAdapterDTOForStatusMessage(etxMessage);

        new Verifications() {{
            String as4ServiceActual;
            dto.setAs4Service(as4ServiceActual = withCapture());
            times = 1;
            Assert.assertEquals("as4 service must match", etxPluginAS4Properties.getAs4ServiceStatusMessageTransmission(), as4ServiceActual);

            String as4ServiceTypeActual;
            dto.setAs4ServiceType(as4ServiceTypeActual = withCapture());
            times = 1;
            Assert.assertEquals("as4 service type must match", etxPluginAS4Properties.getAs4ServiceStatusMessageTransmissionServiceType(), as4ServiceTypeActual);

            String as4ActionActual;
            dto.setAs4Action(as4ActionActual = withCapture());
            times = 1;
            Assert.assertEquals("as4 action must match", etxPluginAS4Properties.getAs4ActionStatusMessageTransmissionRequest(), as4ActionActual);

            PayloadDTO payloadDTOActual;
            dto.addAs4Payload(payloadDTOActual = withCapture());
            times = 1;
            Assert.assertEquals("payload DTO must match", TransformerUtils.getGenericPayloadDTO(etxMessage.getXml()), payloadDTOActual);

            String as4ConversationIdActual;
            dto.setAs4ConversationId(as4ConversationIdActual = withCapture());
            times = 1;
            Assert.assertEquals("as4 conversation id must match", ConversationIdKey.INBOX_MESSAGE_STATUS.getConversationIdKey()+ etxMessage.getMessageUuid(), as4ConversationIdActual);

        }};
    }


    @Test
    public void testHandleSendMessageSuccess_Ok() {

        final EtxMessage etxMessage = getEtxMessage(domibusMessageId);

        new Expectations() {{
            messageService.findMessageByDomibusMessageId(domibusMessageId);
            times = 1;
            result = etxMessage;
        }};

        inboxStatusMessageTransmissionNodeHandler.handleSendMessageSuccess(getDto());

        new Verifications() {{
            messageService.update(etxMessage);
            times = 1;
        }};

        assertThat(etxMessage.getMessageState(), is(MessageState.SENT_TO_BACKEND));

    }

    @Test
    public void testHandleSendMessageSuccess_Exception() {

        new Expectations() {{
            messageService.findMessageByDomibusMessageId(domibusMessageId);
            times = 1;
            result = null;
        }};

        try {
            inboxStatusMessageTransmissionNodeHandler.handleSendMessageSuccess(getDto());
            Assert.fail("It should throw " + ETrustExPluginException.class.getCanonicalName());
        } catch (ETrustExPluginException ex) {
            assertThat(ETrustExError.DEFAULT, is(ex.getError()));
        }

    }

    @Test
    public void testHandleSendMessageFailed_Ok(final @Mocked ETrustExAdapterDTO mockedDto) {

        final EtxMessage etxMessage = getEtxMessage(domibusMessageId);
        final ETrustExAdapterDTO realDto = getDtoWithPayload();

        new Expectations() {{
            messageService.findMessageByDomibusMessageId(mockedDto.getAs4MessageId());
            times = 1;
            result = etxMessage;

            mockedDto.getPayloadFromCID(CONTENT_FAULT);
            times = 1;
            result = realDto.getPayloadFromCID(CONTENT_FAULT);

            mockedDto.getPayloadFromCID(CONTENT_FAULT).getPayloadAsString();
            times = 1;
            result = realDto.getPayloadFromCID(CONTENT_FAULT).getPayloadAsString();
        }};

        inboxStatusMessageTransmissionNodeHandler.handleSendMessageFailed(realDto);

        new FullVerifications() {{
            InboxMessageTransmissionQueueMessage queueMessageActual;
            inboxErrorProducer.triggerError(queueMessageActual = withCapture());
            times = 1;
            Assert.assertNotNull(queueMessageActual);
            assertThat(queueMessageActual.getMessageType(), is(MessageType.MESSAGE_STATUS));
            assertThat(queueMessageActual.getMessageDirection(), is(MessageDirection.NODE_TO_BACKEND));
            assertThat(queueMessageActual.getErrorCode(), is(ETrustExError.DEFAULT.getCode()));
        }};

    }

    @Test
    public void testHandleSendMessageFailed_Exception(final @Mocked ETrustExAdapterDTO mockedDto) {

        new Expectations() {{
            mockedDto.getPayloadFromCID(CONTENT_FAULT);
            times = 1;
            result = null;
        }};

        try {
            inboxStatusMessageTransmissionNodeHandler.handleSendMessageFailed(mockedDto);
            Assert.fail("It should throw " + ETrustExPluginException.class.getCanonicalName());
        } catch (ETrustExPluginException ex) {
            assertThat(ETrustExError.DEFAULT, is(ex.getError()));
        }

    }

    private EtxMessage getEtxMessage(String domibusMessageId) {
        SubmitApplicationResponseRequest submitApplicationResponseRequest =
                getObjectFromXml(readResource("data/inbox/statusmessage/SubmitApplicationResponseRequest.xml"));
        final EtxMessage etxMessage = new EtxMessage();
        etxMessage.setDomibusMessageId(domibusMessageId);
        etxMessage.setMessageType(MessageType.MESSAGE_STATUS);
        etxMessage.setMessageState(MessageState.SUBMITTED_TO_DOMIBUS);
        etxMessage.setXml(serializeObjToXML(submitApplicationResponseRequest));
        return etxMessage;
    }

    private ETrustExAdapterDTO getDto() {
        ETrustExAdapterDTO dto = new ETrustExAdapterDTO();
        dto.setAs4MessageId(domibusMessageId);
        return dto;
    }

    private ETrustExAdapterDTO getDtoWithPayload() {
        ETrustExAdapterDTO dto = new ETrustExAdapterDTO();
        dto.setAs4MessageId(domibusMessageId);
        PayloadDTO payloadDTO = new PayloadDTO(ContentId.CONTENT_FAULT.getValue(), "XML Payload to TEST".getBytes(), "mimeType", false, "JUNIT test");
        dto.addAs4Payload(payloadDTO);
        return dto;
    }

}
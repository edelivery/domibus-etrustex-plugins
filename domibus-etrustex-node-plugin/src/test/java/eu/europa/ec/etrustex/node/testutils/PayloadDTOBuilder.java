package eu.europa.ec.etrustex.node.testutils;

import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.util.ContentId;

import java.util.Locale;

/**
 * @author François Gautier
 * @version 1.0
 * @since 20-Nov-17
 */
public class PayloadDTOBuilder {
    private String contentId;
    private String description;
    private Locale locale;
    private String mimeType;
    private boolean inBody;
    private byte[] payload;

    private PayloadDTOBuilder() {
    }

    public static PayloadDTOBuilder getInstance() {
        return new PayloadDTOBuilder();
    }

    public static PayloadDTO getDto(ContentId contentId) {
        return getInstance()
                .withContentId(contentId.getValue())
                .withDescription("")
                .withInBody(false)
                .withLocale(new Locale(""))
                .withMimeType("")
                .withPayload("".getBytes())
                .build();
    }

    public PayloadDTO build() {
        return new PayloadDTO(contentId, payload, mimeType, inBody, description);
    }

    public PayloadDTOBuilder withContentId(String contentId) {
        this.contentId = contentId;
        return this;
    }

    public PayloadDTOBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public PayloadDTOBuilder withLocale(Locale locale) {
        this.locale = locale;
        return this;
    }

    public PayloadDTOBuilder withMimeType(String mimeType) {
        this.mimeType = mimeType;
        return this;
    }

    public PayloadDTOBuilder withInBody(boolean inBody) {
        this.inBody = inBody;
        return this;
    }

    public PayloadDTOBuilder withPayload(byte[] payload) {
        this.payload = payload;
        return this;
    }
}

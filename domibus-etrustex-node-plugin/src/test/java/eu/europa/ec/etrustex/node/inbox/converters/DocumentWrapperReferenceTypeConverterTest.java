package eu.europa.ec.etrustex.node.inbox.converters;

import ec.schema.xsd.commonaggregatecomponents_2.DocumentWrapperReferenceType;
import ec.schema.xsd.commonaggregatecomponents_2.ResourceInformationReferenceType;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import eu.europa.ec.etrustex.node.testutils.ReflectionUtils;
import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.xml.bind.DatatypeConverter;
import java.math.BigDecimal;

/**
 * JUnit for {@link DocumentWrapperReferenceTypeConverter}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 20/11/2017
 */
@RunWith(JMockit.class)
public class DocumentWrapperReferenceTypeConverterTest {
    private final String attachmentUuid = "TEST_ATT_uuid_12345";
    private final String attachmentType = "BINARY";
    private final String attFileName = "/tmp/document.pdf";
    private final long attFileSize = 32421;
    private final String attHashMethod = "SHA-512";
    private final String attHashValue = "26316A5CAE50C2234CA16717C1F1CBE7B0330F7AA663BB94BEB622F1D4751634B4CA4A8CF4298974214C40353C5E6FAEE954561830871B5EFAC02776E9F925CB";

    @Test
    public void testPrivateConstructor_HappyFlow() throws Exception {
        ReflectionUtils.testPrivateConstructor(DocumentWrapperReferenceTypeConverter.class);
    }

    @Test
    public void testToEtxAttachment_HappyFlow(final @Mocked DocumentWrapperReferenceType documentWrapperReferenceType,
                                              final @Mocked ResourceInformationReferenceType resourceInformationReferenceType) throws Exception {

        new Expectations() {{
            documentWrapperReferenceType.getID().getValue();
            times = 1;
            result = attachmentUuid;

            documentWrapperReferenceType.getDocumentTypeCode().getValue();
            times = 1;
            result = attachmentType;

            documentWrapperReferenceType.getResourceInformationReference();
            times = 1;
            result = resourceInformationReferenceType;

            resourceInformationReferenceType.getName().getValue();
            times = 1;
            result = attFileName;

            resourceInformationReferenceType.getDocumentSize().getValue();
            times = 1;
            result = new BigDecimal(attFileSize);

            resourceInformationReferenceType.getDocumentHashMethod().getValue();
            times = 1;
            result = attHashMethod;

            resourceInformationReferenceType.getDocumentHash().getValue();
            times = 1;
            result = attHashValue;

        }};

        //tested method
        EtxAttachment etxAttachment = DocumentWrapperReferenceTypeConverter.toEtxAttachment(documentWrapperReferenceType);

        Assert.assertNotNull("resulting object shouldn't be null", etxAttachment);
        Assert.assertEquals("attachment Uuid didn't match", attachmentUuid, etxAttachment.getAttachmentUuid());
        Assert.assertEquals("attachment type didn't match", attachmentType, etxAttachment.getAttachmentType().name());
        Assert.assertEquals("attachment filename didn't match", attFileName, etxAttachment.getFileName());
        Assert.assertTrue("attachment filesize didn't match", attFileSize == etxAttachment.getFileSize());
        Assert.assertEquals("attachment checksum algo type didn't match", attHashMethod, etxAttachment.getChecksumAlgorithmType().getValue());
        Assert.assertEquals("attachment checksum didn't match", attHashValue, DatatypeConverter.printHexBinary(etxAttachment.getChecksum()));

    }

    @Test
    public void testToEtxAttachment_NullInput() throws Exception {

        final DocumentWrapperReferenceType documentWrapperReferenceType = null;

        //tested method
        EtxAttachment etxAttachment = DocumentWrapperReferenceTypeConverter.toEtxAttachment(documentWrapperReferenceType);

        Assert.assertNull("resulting object should be null", etxAttachment);

    }

}
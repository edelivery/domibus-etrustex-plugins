package eu.europa.ec.etrustex.node.service;

import eu.europa.ec.etrustex.node.data.dao.AttachmentDao;
import eu.europa.ec.etrustex.node.data.model.attachment.AttachmentState;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.junit.runner.RunWith;

import static eu.europa.ec.etrustex.node.data.model.common.ErrorType.TECHNICAL;
import static eu.europa.ec.etrustex.node.testutils.EtxMessageBuilder.formEtxMessageInboxBundle1Wrapper;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 20-Nov-17
 */
@RunWith(JMockit.class)
public class AttachmentServiceNodeImplTest {

    private static final String ERROR_MESSAGE = "errorMessage";

    @Injectable
    private AttachmentDao attachmentDao;

    @Tested
    private AttachmentServiceNodeImpl messageService;

    @Test
    public void findAttachmentById() throws Exception {
        final long id = RandomUtils.nextLong(0, 99999);
        final EtxAttachment etxAttachment = formEtxMessageInboxBundle1Wrapper().getAttachmentList().get(0);

        new Expectations() {{
            attachmentDao.findById(id);
            times = 1;
            this.result = etxAttachment;
        }};

        EtxAttachment attachmentById = messageService.findAttachmentById(id);

        new FullVerifications() {{
        }};

        assertThat(attachmentById, is(etxAttachment));
    }

    @Test
    public void updateAttachmentState_OK() throws Exception {
        final long id = RandomUtils.nextLong(0, 99999);
        final EtxAttachment etxAttachment = formEtxMessageInboxBundle1Wrapper().getAttachmentList().get(0);
        etxAttachment.setStateType(AttachmentState.CREATED);

        messageService.updateAttachmentState(etxAttachment, AttachmentState.CREATED, AttachmentState.DOWNLOADED);

        new FullVerifications() {{
            attachmentDao.update(etxAttachment);
            times = 1;
        }};

        assertThat(etxAttachment.getStateType(), is(AttachmentState.DOWNLOADED));
    }
    @Test
    public void updateAttachmentState_NoChange() throws Exception {
        final long id = RandomUtils.nextLong(0, 99999);
        final EtxAttachment etxAttachment = formEtxMessageInboxBundle1Wrapper().getAttachmentList().get(0);
        etxAttachment.setStateType(AttachmentState.SUBMITTED_TO_DOMIBUS);

        messageService.updateAttachmentState(etxAttachment, AttachmentState.CREATED, AttachmentState.DOWNLOADED);

        new FullVerifications() {{
        }};

        assertThat(etxAttachment.getStateType(), is(AttachmentState.SUBMITTED_TO_DOMIBUS));
    }

    @Test
    public void update() throws Exception {
        final EtxAttachment etxAttachment = formEtxMessageInboxBundle1Wrapper().getAttachmentList().get(0);

        messageService.update(etxAttachment);

        new FullVerifications() {{
            attachmentDao.update(etxAttachment);
            times = 1;
        }};

    }

    @Test
    public void inError_happyFlow() throws Exception {
        final long id = RandomUtils.nextLong(0, 99999);
        final EtxAttachment etxAttachment = formEtxMessageInboxBundle1Wrapper().getAttachmentList().get(0);

        new Expectations() {{
            attachmentDao.findById(id);
            times = 1;
            this.result = etxAttachment;
        }};

        messageService.attachmentInError(id, ERROR_MESSAGE);

        new FullVerifications() {{
        }};

        assertThat(etxAttachment.getError().getResponseCode(), is(TECHNICAL.getCode()));
        assertThat(etxAttachment.getError().getDetail(), is(ERROR_MESSAGE));
        assertThat(etxAttachment.getStateType(), is(AttachmentState.FAILED));
    }

    @Test
    public void inError_NotFound() throws Exception {
        final long id = RandomUtils.nextLong(0, 99999);

        new Expectations() {{
            attachmentDao.findById(id);
            times = 1;
            this.result = null;
        }};

        messageService.attachmentInError(id, ERROR_MESSAGE);

        new FullVerifications() {{
        }};
    }
}
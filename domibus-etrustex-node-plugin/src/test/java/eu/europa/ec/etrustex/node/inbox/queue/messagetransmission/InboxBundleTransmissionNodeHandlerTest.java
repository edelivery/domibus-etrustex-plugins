package eu.europa.ec.etrustex.node.inbox.queue.messagetransmission;

import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleRequest;
import eu.domibus.messaging.MessagingProcessingException;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.util.ConversationIdKey;
import eu.europa.ec.etrustex.common.util.ETrustExPluginAS4Properties;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import eu.europa.ec.etrustex.node.inbox.queue.error.InboxErrorProducer;
import eu.europa.ec.etrustex.node.service.MessageServicePlugin;
import eu.europa.ec.etrustex.node.service.NodeConnectorSubmissionService;
import eu.europa.ec.etrustex.node.testutils.ResourcesUtils;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.UUID;

import static eu.europa.ec.etrustex.common.util.TransformerUtils.getObjectFromXml;

/**
 * JUnit for {@link InboxBundleTransmissionNodeHandler}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 04/12/2017
 */
@RunWith(JMockit.class)
public class InboxBundleTransmissionNodeHandlerTest {

    private String domibusMessageId;
    private Long messageId;

    @Injectable
    private ETrustExPluginAS4Properties etxPluginAS4Properties;

    @Injectable
    private MessageServicePlugin messageService;

    @Injectable
    private InboxErrorProducer inboxErrorProducer;

    @SuppressWarnings("unused")
    @Injectable
    private InboxMessageTransmissionProducer inboxMessageTransmissionProducer;

    @Injectable
    private NodeConnectorSubmissionService nodeConnectorSubmissionService;

    @Tested
    private InboxBundleTransmissionNodeHandler inboxBundleTransmissionNodeHandler;

    @Before
    public void setUp() {
        domibusMessageId = UUID.randomUUID().toString();
        messageId = RandomUtils.nextLong(0, 99999);
    }

    @Test
    public void testHandleMessageTransmissionToBackend_Happyflow() {

        final InboxMessageTransmissionQueueMessage queueMessage = new InboxMessageTransmissionQueueMessage(
                MessageType.MESSAGE_BUNDLE,
                MessageDirection.NODE_TO_BACKEND,
                messageId);
        final EtxMessage etxMessage = getEtxMessage(domibusMessageId);
        final ETrustExAdapterDTO dto = getDto();

        new Expectations(inboxBundleTransmissionNodeHandler) {{
            inboxBundleTransmissionNodeHandler.loadValidateBundleForTransmission(messageId);
            times = 1;
            result = etxMessage;

            inboxBundleTransmissionNodeHandler.populateETXAdapterDTOForSendingBundleToBackend(etxMessage);
            times = 1;
            result = dto;

            nodeConnectorSubmissionService.submitToDomibus(dto);
            times = 1;
            result = domibusMessageId;
        }};

        //tested method
        inboxBundleTransmissionNodeHandler.handleMessageTransmissionToBackend(queueMessage);

        new FullVerifications(inboxBundleTransmissionNodeHandler) {{
            EtxMessage etxMessageActual;
            String domibusMsgIdActual;
            inboxBundleTransmissionNodeHandler.handleDomibusSubmissionSuccess(etxMessageActual = withCapture(), domibusMsgIdActual = withCapture());
            Assert.assertEquals("etx message should match", etxMessage, etxMessageActual);
            Assert.assertEquals("domibus message id should match", domibusMessageId, domibusMsgIdActual);
        }};
    }

    @Test
    public void testHandleMessageTransmissionToBackend_ExceptionThrown() {

        final InboxMessageTransmissionQueueMessage queueMessage = new InboxMessageTransmissionQueueMessage(
                MessageType.MESSAGE_BUNDLE,
                MessageDirection.NODE_TO_BACKEND,
                messageId);
        final EtxMessage etxMessage = getEtxMessage(domibusMessageId);
        final ETrustExAdapterDTO dto = getDto();

        new Expectations(inboxBundleTransmissionNodeHandler) {{
            inboxBundleTransmissionNodeHandler.loadValidateBundleForTransmission(messageId);
            times = 1;
            result = etxMessage;

            inboxBundleTransmissionNodeHandler.populateETXAdapterDTOForSendingBundleToBackend(etxMessage);
            times = 1;
            result = dto;

            nodeConnectorSubmissionService.submitToDomibus(dto);
            times = 1;
            result = new MessagingProcessingException("domibus exception");
        }};

        //tested method
        inboxBundleTransmissionNodeHandler.handleMessageTransmissionToBackend(queueMessage);

        new FullVerifications(inboxBundleTransmissionNodeHandler) {{
            InboxMessageTransmissionQueueMessage queueMessageActual;
            Exception exceptionActual;
            inboxBundleTransmissionNodeHandler.handleError(queueMessageActual = withCapture(), exceptionActual = withCapture());
            Assert.assertEquals("queue message should match", queueMessage, queueMessageActual);
            Assert.assertEquals("MessagingProcessingException expected", MessagingProcessingException.class, exceptionActual.getClass());
        }};
    }

    @Test
    public void testLoadValidateBundleForTransmission_HappyFlow() {
        final EtxMessage etxMessage = getEtxMessage(domibusMessageId);
        etxMessage.setMessageType(MessageType.MESSAGE_BUNDLE);

        new Expectations(inboxBundleTransmissionNodeHandler) {{
            inboxBundleTransmissionNodeHandler.loadValidateEtxMessageForTransmission(messageId);
            times = 1;
            result = etxMessage;
        }};

        //tested method
        inboxBundleTransmissionNodeHandler.loadValidateBundleForTransmission(messageId);

        new FullVerifications() {{

        }};
    }

    @Test
    public void testLoadValidateBundleForTransmission_WrongMessageType() {
        final EtxMessage etxMessage = getEtxMessage(domibusMessageId);
        etxMessage.setMessageType(MessageType.MESSAGE_STATUS);

        new Expectations(inboxBundleTransmissionNodeHandler) {{
            inboxBundleTransmissionNodeHandler.loadValidateEtxMessageForTransmission(messageId);
            times = 1;
            result = etxMessage;
        }};

        try {
            //tested method
            inboxBundleTransmissionNodeHandler.loadValidateBundleForTransmission(messageId);
            Assert.fail("ETrustExPluginException expected");
        } catch (ETrustExPluginException e) {
            Assert.assertEquals("error type must match", ETrustExError.DEFAULT, e.getError());
            Assert.assertEquals("error message should match", "Message ID:[" + messageId + "] specified for Inbox Message transmission to backend is not a Bundle.", e.getMessage());
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void testPopulateETXAdapterDTOForSendingBundleToBackend_HappyFlow(final @Mocked ETrustExAdapterDTO dto) {
        final EtxMessage etxMessage = getEtxMessage(domibusMessageId);

        new Expectations(inboxBundleTransmissionNodeHandler) {{
            inboxBundleTransmissionNodeHandler.createEtxAdapterDTOForInboxTransmission(etxMessage);
            times = 1;
            result = dto;
        }};

        //tested method
        inboxBundleTransmissionNodeHandler.populateETXAdapterDTOForSendingBundleToBackend(etxMessage);

        new Verifications() {{
            String as4ServiceActual;
            dto.setAs4Service(as4ServiceActual = withCapture());
            times = 1;
            Assert.assertEquals("as4 service must match", etxPluginAS4Properties.getAs4ServiceInboxBundleTransmission(), as4ServiceActual);

            String as4ServiceTypeActual;
            dto.setAs4ServiceType(as4ServiceTypeActual = withCapture());
            times = 1;
            Assert.assertEquals("as4 service type must match", etxPluginAS4Properties.getAs4ServiceInboxBundleTransmissionServiceType(), as4ServiceTypeActual);

            String as4ActionActual;
            dto.setAs4Action(as4ActionActual = withCapture());
            times = 1;
            Assert.assertEquals("as4 action must match", etxPluginAS4Properties.getAs4ActionInboxBundleTransmissionRequest(), as4ActionActual);

            PayloadDTO payloadDTOActual;
            dto.addAs4Payload(payloadDTOActual = withCapture());
            times = 1;
            Assert.assertEquals("payload DTO must match", TransformerUtils.getGenericPayloadDTO(etxMessage.getXml()), payloadDTOActual);

            String as4ConversationIdActual;
            dto.setAs4ConversationId(as4ConversationIdActual = withCapture());
            times = 1;
            Assert.assertEquals("as4 conversation id must match", ConversationIdKey.INBOX_MESSAGEBUNDLE.getConversationIdKey() + etxMessage.getMessageUuid(), as4ConversationIdActual);

        }};
    }

    @Test
    public void testHandleSendMessageSuccess_HappyFlow() {

        final EtxMessage etxMessage = getEtxMessage(domibusMessageId);

        new Expectations() {{
            messageService.findMessageByDomibusMessageId(domibusMessageId);
            times = 1;
            result = etxMessage;

        }};

        //tested method
        inboxBundleTransmissionNodeHandler.handleSendMessageSuccess(getDto());

        new FullVerifications() {{

            EtxMessage etxMessageActual;
            messageService.update(etxMessageActual = withCapture());
            times = 1;
            Assert.assertEquals("etx message should match", etxMessage, etxMessageActual);
            Assert.assertEquals("message state should match", MessageState.SENT_TO_BACKEND, etxMessageActual.getMessageState());

        }};
    }

    @Test
    public void handleSendMessageSuccess_DomibusIdEmpty(final @Mocked ETrustExAdapterDTO dto) {

        new Expectations() {{
            //noinspection ResultOfMethodCallIgnored
            dto.getAs4MessageId();
            times = 1;
            result = StringUtils.EMPTY;
        }};

        try {
            //tested method
            inboxBundleTransmissionNodeHandler.handleSendMessageSuccess(dto);
            Assert.fail("ETrustExPluginException expected");
        } catch (Exception e) {
            Assert.assertEquals("ETrustExPluginException type expected", ETrustExPluginException.class, e.getClass());
            ETrustExPluginException eTrustExPluginException = (ETrustExPluginException) e;
            Assert.assertEquals("error type should match", ETrustExError.DEFAULT, eTrustExPluginException.getError());
            Assert.assertEquals("error message should match",
                    "The Domibus message id required to retrieve the message bundle has not been set",
                    eTrustExPluginException.getMessage());
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void handleSendMessageSuccess_DomibusIdNotFoundDB() {

        new Expectations() {{
            messageService.findMessageByDomibusMessageId(domibusMessageId);
            times = 1;
            result = null;
        }};

        try {
            //tested method
            inboxBundleTransmissionNodeHandler.handleSendMessageSuccess(getDto());
            Assert.fail("ETrustExPluginException expected");
        } catch (Exception e) {
            Assert.assertEquals("ETrustExPluginException type expected", ETrustExPluginException.class, e.getClass());
            ETrustExPluginException eTrustExPluginException = (ETrustExPluginException) e;
            Assert.assertEquals("error type should match", ETrustExError.DEFAULT, eTrustExPluginException.getError());
            Assert.assertEquals("error message should match",
                    "For Domibus message with id [" + domibusMessageId + "] no message bundle was found",
                    eTrustExPluginException.getMessage());
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void testHandleSendMessageFailed_HappyFlow() {
        final ETrustExAdapterDTO dto = getDto();
        final InboxMessageTransmissionQueueMessage queueMessage = new InboxMessageTransmissionQueueMessage(
                MessageType.MESSAGE_BUNDLE,
                MessageDirection.NODE_TO_BACKEND,
                messageId);

        new Expectations(inboxBundleTransmissionNodeHandler) {{
            inboxBundleTransmissionNodeHandler.createMsgSendFailedQueueMessage(dto, MessageType.MESSAGE_BUNDLE);
            times = 1;
            result = queueMessage;
        }};

        //tested method
        inboxBundleTransmissionNodeHandler.handleSendMessageFailed(dto);

        new Verifications() {{
            InboxMessageTransmissionQueueMessage queueMessageActual;
            inboxErrorProducer.triggerError(queueMessageActual = withCapture());
            times = 1;
            Assert.assertNotNull("result shouldn't be null", queueMessageActual);
            Assert.assertEquals("queue message must match", queueMessage, queueMessageActual);

        }};
    }


    private EtxMessage getEtxMessage(String domibusMessageId) {
        SubmitDocumentBundleRequest submitDocumentBundleRequest = getObjectFromXml(ResourcesUtils.readResource("data/inbox/documentbundle/SubmitDocumentBundleRequest.xml")
        );
        final EtxMessage etxMessage = new EtxMessage();
        etxMessage.setId(messageId);
        etxMessage.setDomibusMessageId(domibusMessageId);
        etxMessage.setMessageState(MessageState.SUBMITTED_TO_DOMIBUS);
        etxMessage.setXml(TransformerUtils.serializeObjToXML(submitDocumentBundleRequest));
        return etxMessage;
    }

    private ETrustExAdapterDTO getDto() {
        ETrustExAdapterDTO dto = new ETrustExAdapterDTO();
        dto.setAs4MessageId(domibusMessageId);
        return dto;
    }

}
package eu.europa.ec.etrustex.node.service;

import eu.europa.ec.etrustex.node.data.dao.MessageDao;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.UUID;

import static eu.europa.ec.etrustex.node.data.model.common.ErrorType.TECHNICAL;
import static eu.europa.ec.etrustex.node.testutils.EtxMessageBuilder.formEtxMessageInboxBundle1Wrapper;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author François Gautier
 * @version 1.0
 * @since 20-Nov-17
 */
@RunWith(JMockit.class)
public class MessageServicePluginImplTest {

    private static final String ERROR_MESSAGE = "errorMessage";

    @Injectable
    private MessageDao messageDao;

    @Tested
    private MessageServicePluginImpl messageService;

    @Test
    public void createMessage() throws Exception {
        final EtxMessage message = formEtxMessageInboxBundle1Wrapper();

        messageService.createMessage(message);

        new FullVerifications() {{
            messageDao.save(message);
            times = 1;
        }};
    }

    @Test
    public void findMessageById() throws Exception {
        final EtxMessage message = formEtxMessageInboxBundle1Wrapper();
        final long id = RandomUtils.nextLong(0, 99999);

        new Expectations() {{
            messageDao.findById(id);
            times = 1;
            this.result = message;
        }};

        EtxMessage messageById = messageService.findMessageById(id);
        new FullVerifications() {{
        }};
        assertThat(messageById, is(message));
    }

    @Test
    public void update() throws Exception {
        final EtxMessage message = formEtxMessageInboxBundle1Wrapper();

        messageService.update(message);

        new FullVerifications() {{
            messageDao.update(message);
            times = 1;
        }};
    }

    @Test
    public void findMessageByDomibusMessageId() throws Exception {
        final String id = UUID.randomUUID().toString();
        final EtxMessage message = formEtxMessageInboxBundle1Wrapper();
        new Expectations() {{
            messageDao.findMessageByDomibusMessageId(id);
            times = 1;
            this.result = message;
        }};
        EtxMessage messageByDomibusMessageId = messageService.findMessageByDomibusMessageId(id);

        new FullVerifications() {{
        }};
        assertThat(messageByDomibusMessageId, is(message));
    }

    @Test
    public void inError() throws Exception {
        final long id = RandomUtils.nextLong(0, 99999);
        final EtxMessage message = formEtxMessageInboxBundle1Wrapper();

        new Expectations() {{
            messageDao.findById(id);
            times = 1;
            this.result = message;
        }};

        messageService.messageInError(id, ERROR_MESSAGE);

        new FullVerifications() {{
        }};

        assertThat(message.getError().getResponseCode(), is(TECHNICAL.getCode()));
        assertThat(message.getError().getDetail(), is(ERROR_MESSAGE));
        assertThat(message.getMessageState(), is(MessageState.FAILED));
    }

    @Test
    public void inError_notFound() throws Exception {
        final long id = RandomUtils.nextLong(0, 99999);

        new Expectations() {{
            messageDao.findById(id);
            times = 1;
            this.result = null;
        }};

        try {
            messageService.messageInError(id, ERROR_MESSAGE);
            fail();
        } catch (NullPointerException e) {
            //OK
        }

        new FullVerifications() {{
        }};
    }
}
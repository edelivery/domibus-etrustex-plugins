package eu.europa.ec.etrustex.node.testutils;

import ec.schema.xsd.commonaggregatecomponents_2.BusinessHeaderType;
import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import eu.europa.ec.etrustex.common.converters.node.NodePartnerConverter;

import javax.xml.ws.Holder;

/**
 * @author François Gautier
 * @version 1.0
 * @since 12/02/2018
 */
public class HeaderUtils {
    private HeaderUtils() {
    }

    /**
     * builds the header for input data
     *
     * @param senderPartyName   sender party
     * @param receiverPartyName receiver party
     * @return {@link HeaderType} holder
     */
    public static Holder<HeaderType> buildHeader(String senderPartyName, String receiverPartyName) {

        Holder<HeaderType> authorizationHeaderHolder = new Holder<>();
        authorizationHeaderHolder.value = new HeaderType();
        BusinessHeaderType businessHeaderType = new BusinessHeaderType();
        if (receiverPartyName != null) {
            businessHeaderType.getReceiver().add(NodePartnerConverter.buildPartnerType(receiverPartyName));
        }
        businessHeaderType.getSender().add(NodePartnerConverter.buildPartnerType(senderPartyName));

        authorizationHeaderHolder.value.setBusinessHeader(businessHeaderType);
        return authorizationHeaderHolder;
    }
}

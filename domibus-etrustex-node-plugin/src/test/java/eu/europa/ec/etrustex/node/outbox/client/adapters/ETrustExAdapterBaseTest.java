package eu.europa.ec.etrustex.node.outbox.client.adapters;

import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ETrustExPluginAS4Properties;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.data.model.administration.EtxUser;
import eu.europa.ec.etrustex.node.service.security.IdentityManager;
import eu.europa.ec.etrustex.node.webservice.client.NodeWebServiceProvider;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jms.core.JmsOperations;

import java.util.UUID;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 16-Nov-17
 */
@RunWith(JMockit.class)
public class ETrustExAdapterBaseTest {

    private static final String PARTY_UUID = UUID.randomUUID().toString();
    private static final String NAME = "NAME";
    private static final String PASSWORD = "PASSWORD";
    @Tested
    private ETrustExAdapterBase eTrustExAdapterBase;

    @Injectable
    private ETrustExPluginAS4Properties etxPluginAS4Properties;
    @Injectable
    private IdentityManager identityManager;
    @Injectable
    private JmsOperations etxNodePluginNodeToBackendJmsTemplate;
    @Injectable
    private JmsOperations etxNodePluginBackendToNodeJmsTemplate;
    @Injectable
    private NodeWebServiceProvider webServiceProvider;

    @Before
    public void setUp() {
        eTrustExAdapterBase = getETrustExAdapterBaseInTest();
    }

    private EtxParty getEtxParty(String name, String password) {
        final EtxParty etxParty = new EtxParty();
        etxParty.setNodeUser(getNodeUser(name, password));
        return etxParty;
    }

    private EtxUser getNodeUser(String name, String password) {
        EtxUser etxUser = new EtxUser();
        etxUser.setName(name);
        etxUser.setPassword(password);
        return etxUser;
    }

    @Test
    public void getUser() {
        final EtxParty etxParty = getEtxParty(NAME, PASSWORD);
        new Expectations() {{
            identityManager.searchPartyByUuid(PARTY_UUID);
            times = 1;
            result = etxParty;
        }};
        EtxUser user = eTrustExAdapterBase.getUser(PARTY_UUID);
        new FullVerifications() {{

        }};

        assertThat(user.getName(), is(NAME));
        assertThat(user.getPassword(), is(PASSWORD));
    }

    @Test(expected = IllegalStateException.class)
    public void getUser_notFound() {
        new Expectations() {{
            identityManager.searchPartyByUuid(PARTY_UUID);
            times = 1;
            result = null;
        }};
        eTrustExAdapterBase.getUser(PARTY_UUID);
        new FullVerifications() {{

        }};
    }

    private ETrustExAdapterBase getETrustExAdapterBaseInTest() {
        return new ETrustExAdapterBase() {

            @Override
            public String getServiceEndpoint() {
                throw new IllegalStateException("Do not test this method!");
            }

            @Override
            public void performOperation(ETrustExAdapterDTO dto) throws RuntimeException {
                throw new IllegalStateException("Do not test this method!");
            }
        };
    }
}
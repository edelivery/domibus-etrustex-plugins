package eu.europa.ec.etrustex.node.inbox.client.adapter;

import ec.schema.xsd.ack_2.AcknowledgmentType;
import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.schema.xsd.commonbasiccomponents_1.AckIndicatorType;
import ec.services.wsdl.applicationresponse_2.ApplicationResponsePortType;
import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseRequest;
import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseResponse;
import eu.europa.ec.etrustex.common.util.CalendarHelper;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import eu.europa.ec.etrustex.node.data.model.AuditEntity;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.webservice.client.NodeWebServiceProvider;
import eu.europa.ec.etrustex.node.webservice.client.WSClient;
import mockit.*;
import mockit.integration.junit4.JMockit;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IssueDateType;
import org.hamcrest.CustomMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.xml.ws.Holder;
import java.util.ArrayList;
import java.util.List;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.getObjectFromXml;
import static eu.europa.ec.etrustex.node.testutils.EtxMessageBuilder.formEtxMessageInboxBundle1Wrapper;
import static eu.europa.ec.etrustex.node.testutils.ResourcesUtils.readResource;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 14-Nov-17
 */
@SuppressWarnings("unused")
@RunWith(JMockit.class)
public class SubmitApplicationResponseFaultAdapterTest {

    private static final String NAME = "John Doe";
    private static final String PASSWORD = "xxxx";
    private static final String PARTY_UUID = "PARTY_UUID";
    private static final String MESSAGE_UUID = "MESSAGE_UUID";
    @Injectable
    private ETrustExNodePluginProperties eTrustExNodePluginProperties;

    @Injectable
    private NodeWebServiceProvider webServiceProvider;

    @Injectable
    private String serviceEndpoint;

    @Tested
    private SubmitApplicationResponseFaultAdapter adapter;

    private EtxParty sender;
    private EtxParty receiver;

    @Mocked
    private ApplicationResponsePortType applicationResponseService;
    private Holder<HeaderType> headerTypeHolder;

    @Before
    public void setUp() {
        EtxMessage etxMessage = formEtxMessageInboxBundle1Wrapper();
        sender = etxMessage.getSender();
        receiver = etxMessage.getReceiver();

        headerTypeHolder = new Holder<>();
        eTrustExNodePluginProperties = new ETrustExNodePluginProperties(){
            public String getApplicationResponseServiceEndpoint(){
                return "endPoint";
            }
        };
        new MockUp<NodeServicesAdapterBase>() {

            //Override the private/protected method
            //Dont provide any ACCESSS MODIFIER!
            @Mock
            Holder<HeaderType> fillAuthorisationHeader(String senderPartyName, String receiverPartyName) {
                return headerTypeHolder;
            }

        };
        new MockUp<WSClient>() {

            //Override the private/protected method
            //Dont provide any ACCESSS MODIFIER!
            @Mock
            ApplicationResponsePortType buildApplicationResponsePort(String user, String pwd) {
                return applicationResponseService;
            }

        };
    }

    @Test
    public void sendNotification() throws Exception {
        final List<SubmitApplicationResponseRequest> requests = new ArrayList<>();
        final List<Holder<HeaderType>> headers = new ArrayList<>();

        new Expectations() {{
            applicationResponseService.submitApplicationResponse(withCapture(requests), withCapture(headers));
            times = 1;
            result = getOkResult();
        }};

        adapter.sendNotification(sender, receiver, MESSAGE_UUID);

        new FullVerifications() {{
        }};

        assertThat(requests.size(), is(1));
        assertThat(requests.get(0),
                sameBeanAs(getObjectFromXml(readResource("data/inbox/error/RequestSendNotification.xml")))
                        // applicationResponse.id is an ID automatically generated
                        .with("applicationResponse.id", notNullValue())
                        .with("applicationResponse.issueDate", isToday())
        );

        assertThat(headers.size(), is(1));
        assertThat(headers.get(0), is(headerTypeHolder));
    }

    private CustomMatcher isToday() {
        return new CustomMatcher("Compare jodaTime DateTime with today") {
            @Override
            public boolean matches(Object item) {
                return ((IssueDateType) item).getValue().equals(CalendarHelper.getJodaDateFromCalendar(AuditEntity.getGMTCalendar()));
            }
        };
    }

    /**
     * The Acknowledgement is printed in the logs
     *
     * @return {@link SubmitApplicationResponseResponse} with ack = true
     */
    private SubmitApplicationResponseResponse getOkResult() {
        SubmitApplicationResponseResponse submitApplicationResponseResponse = new SubmitApplicationResponseResponse();
        AcknowledgmentType ack = new AcknowledgmentType();
        AckIndicatorType value = new AckIndicatorType();
        value.setValue(true);
        ack.setAckIndicator(value);
        submitApplicationResponseResponse.setAck(ack);
        return submitApplicationResponseResponse;
    }

}
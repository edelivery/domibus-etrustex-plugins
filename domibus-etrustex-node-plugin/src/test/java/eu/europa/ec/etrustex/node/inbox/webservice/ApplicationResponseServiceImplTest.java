package eu.europa.ec.etrustex.node.inbox.webservice;

import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseRequest;
import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseResponse;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import eu.europa.ec.etrustex.node.inbox.converters.NodeAcknowledgmentTypeConverter;
import eu.europa.ec.etrustex.node.inbox.converters.NodeMessageStatusConverter;
import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxMessageTransmissionProducer;
import eu.europa.ec.etrustex.node.service.MessageServicePlugin;
import eu.europa.ec.etrustex.node.service.security.IdentityManager;
import mockit.*;
import mockit.integration.junit4.JMockit;
import oasis.names.specification.ubl.schema.xsd.applicationresponse_2.ApplicationResponseType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import org.hamcrest.core.IsNull;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.unece.cefact.namespaces.standardbusinessdocumentheader.Partner;

import javax.xml.ws.Holder;
import java.util.List;

import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_MESSAGE_UUID;
import static eu.europa.ec.etrustex.node.testutils.HeaderUtils.buildHeader;
import static org.hamcrest.core.Is.is;

/**
 * JUnit for {@link ApplicationResponseServiceImpl}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 24/11/2017
 */
@RunWith(JMockit.class)
public class ApplicationResponseServiceImplTest {

    @Injectable
    private IdentityManager identityManager;
    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Injectable
    private DomainNodePluginService domainNodePluginService;
    @Injectable
    private MessageServicePlugin messageService;

    @Injectable
    private InboxMessageTransmissionProducer inboxMessageTransmissionProducer;

    @Tested
    private ApplicationResponseServiceImpl applicationResponseService;

    @Test
    public void testSubmitApplicationResponse_HappyFlow() throws Exception {
        final SubmitApplicationResponseRequest submitApplicationResponseRequest = new SubmitApplicationResponseRequest();
        final SubmitApplicationResponseResponse submitApplicationResponseResponse = new SubmitApplicationResponseResponse();

        final String senderPartyUuid = "TEST_EGREFFE_APP_PARTY_B4";
        final String receiverPartyUuid = "TEST_EGREFFE_NP_PARTY_B4";

        final Holder<HeaderType> headerTypeHolder = buildHeader(senderPartyUuid, receiverPartyUuid);
        final EtxParty senderParty = new EtxParty();
        senderParty.setPartyUuid(senderPartyUuid);
        final EtxParty receiverParty = new EtxParty();
        receiverParty.setPartyUuid(receiverPartyUuid);
        receiverParty.setRequiresWrappers(true);

        final EtxMessage etxMessage = new EtxMessage();
        final Long messageId = 20L;
        etxMessage.setId(messageId);

        //noinspection Duplicates
        new Expectations(applicationResponseService) {{

            applicationResponseService.extractValidateParty(headerTypeHolder.value.getBusinessHeader().getSender());
            times = 1;
            result = senderParty;

            applicationResponseService.extractValidateParty(headerTypeHolder.value.getBusinessHeader().getReceiver());
            times = 1;
            result = receiverParty;

            applicationResponseService.createPersistMessage(submitApplicationResponseRequest, senderParty, receiverParty);
            times = 1;
            result = etxMessage;

            applicationResponseService.buildAckNodeResponse(submitApplicationResponseRequest, headerTypeHolder);
            times = 1;
            result = submitApplicationResponseResponse;

        }};

        //tested method
        applicationResponseService.submitApplicationResponse(submitApplicationResponseRequest, headerTypeHolder);

        new FullVerificationsInOrder() {{
            List<Partner> senderPartyListActual;
            applicationResponseService.extractValidateParty(senderPartyListActual = withCapture());
            Assert.assertEquals("sender party uuid must match", senderPartyUuid, senderPartyListActual.get(0).getIdentifier().getValue());

            List<Partner> receiverPartyListActual;
            applicationResponseService.extractValidateParty(receiverPartyListActual = withCapture());
            Assert.assertEquals("receiver party uuid must match", receiverPartyUuid, receiverPartyListActual.get(0).getIdentifier().getValue());

            Long msgIdActual;
            inboxMessageTransmissionProducer.triggerMessageTransmissionToBackend(msgIdActual = withCapture());
            times = 1;
            Assert.assertEquals("message id should match", messageId, msgIdActual);

            Holder<HeaderType> holderActual;
            applicationResponseService.buildHeaderResponse(holderActual = withCapture());
            times = 1;
            Assert.assertEquals("header type holder must match", headerTypeHolder, holderActual);

            clearEtxLogKeysService.clearSpecificKeys(MDC_ETX_MESSAGE_UUID);
            times = 1;
        }};
    }

    @Test
    public void testCreatePersistMessage_HappyFlow(final @Mocked EtxMessage etxMessage) {

        final SubmitApplicationResponseRequest submitApplicationResponseRequest = new SubmitApplicationResponseRequest();
        final EtxParty senderParty = new EtxParty();
        final EtxParty receiverParty = new EtxParty();
        final String senderPartyUuid = "TEST_EGREFFE_APP_PARTY_B4";
        final String receiverPartyUuid = "TEST_EGREFFE_NP_PARTY_B4";
        senderParty.setPartyUuid(senderPartyUuid);
        receiverParty.setPartyUuid(receiverPartyUuid);

        //stubbing the converter
        new MockUp<NodeMessageStatusConverter>() {

            @Mock
            EtxMessage toEtxMessage(SubmitApplicationResponseRequest submitApplicationResponseRequest) {
                return etxMessage;
            }
        };

        //method to test
        applicationResponseService.createPersistMessage(submitApplicationResponseRequest, senderParty, receiverParty);

        new FullVerifications() {{
            MessageType messageTypeActual;
            etxMessage.setMessageType(messageTypeActual = withCapture());
            times = 1;
            Assert.assertEquals("message type should match", MessageType.MESSAGE_STATUS, messageTypeActual);

            MessageState messageStateActual;
            etxMessage.setMessageState(messageStateActual = withCapture());
            times = 1;
            Assert.assertEquals("message state should match", MessageState.CREATED, messageStateActual);

            MessageDirection messageDirectionActual;
            etxMessage.setDirectionType(messageDirectionActual = withCapture());
            times = 1;
            Assert.assertEquals("message direction should match", MessageDirection.NODE_TO_BACKEND, messageDirectionActual);

            EtxParty etxPartyActual;
            etxMessage.setSender(etxPartyActual = withCapture());
            times = 1;
            Assert.assertEquals("sender party should match", senderParty, etxPartyActual);

            etxMessage.setReceiver(etxPartyActual = withCapture());
            times = 1;
            Assert.assertEquals("receiver party should match", receiverParty, etxPartyActual);

            messageService.createMessage(etxMessage);
            times = 1;

        }};
    }

    @Test
    public void testBuildAckNodeResponse_HappyFlow(final @Mocked NodeAcknowledgmentTypeConverter nodeAcknowledgmentTypeConverter) throws Exception {

        final String messageId = "TEST_msg_id";
        final String senderPartyUuid = "TEST_EGREFFE_APP_PARTY_B4";
        final String receiverPartyUuid = "TEST_EGREFFE_NP_PARTY_B4";

        final Holder<HeaderType> headerTypeHolder = buildHeader(senderPartyUuid, receiverPartyUuid);

        SubmitApplicationResponseRequest submitApplicationResponseRequest = new SubmitApplicationResponseRequest();
        ApplicationResponseType applicationResponse = new ApplicationResponseType();
        applicationResponse.setID(getIdType(messageId));
        submitApplicationResponseRequest.setApplicationResponse(applicationResponse);

        //tested method
        SubmitApplicationResponseResponse result = applicationResponseService.buildAckNodeResponse(submitApplicationResponseRequest, headerTypeHolder);

        Assert.assertThat(result.getAck(), is(IsNull.notNullValue()));
        new FullVerifications() {{

            String senderPartyUuidActual, receiverPartyUuidActual, documentTypeCodeActual, documenRefIdActual;
            DateTime issueDateActual;
            NodeAcknowledgmentTypeConverter.buildAcknowledgmentType(issueDateActual = withCapture(),
                    documenRefIdActual = withCapture(), documentTypeCodeActual = withCapture(), senderPartyUuidActual = withCapture(), receiverPartyUuidActual = withCapture());
            times = 1;
            Assert.assertEquals("issue date should match", DateTime.now().toLocalDate(), issueDateActual.toLocalDate());
            Assert.assertEquals("doc ref id must match", messageId, documenRefIdActual);
            Assert.assertEquals("document type code should match", ApplicationResponseServiceImpl.NODE_DOCUMENT_TYPE, documentTypeCodeActual);
            Assert.assertEquals("sender party should match", senderPartyUuid, senderPartyUuidActual);
            Assert.assertEquals("receiver party should match", receiverPartyUuid, receiverPartyUuidActual);
        }};
    }

    private IDType getIdType(String messageId) {
        IDType value = new IDType();
        value.setValue(messageId);
        return value;
    }
}
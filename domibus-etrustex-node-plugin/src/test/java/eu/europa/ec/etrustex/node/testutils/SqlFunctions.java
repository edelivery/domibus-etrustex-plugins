package eu.europa.ec.etrustex.node.testutils;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 26-Jan-18
 */
public class SqlFunctions {

    public static String toDate(String date, String pattern){
        String s = pattern.replaceAll("MON", "MMM").replaceAll("RR", "YY");
        return LocalDate.parse(date, DateTimeFormat.forPattern(s)).toString("yyyy-MM-dd");
    }
}

package eu.europa.ec.etrustex.node.outbox.factory;

import eu.europa.ec.etrustex.common.adapters.Adapter;
import eu.europa.ec.etrustex.common.handlers.Action;
import eu.europa.ec.etrustex.common.handlers.Service;
import eu.europa.ec.etrustex.node.outbox.client.adapters.RetrieveICAAdapter;
import eu.europa.ec.etrustex.node.outbox.client.adapters.UnknownOperationAdapter;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Map;

import static org.hamcrest.core.Is.is;

/**
 * @author François Gautier
 * @version 1.0
 * @since 21-Nov-17
 */
@RunWith(JMockit.class)
public class ETrustExOperationAdapterFactoryTest {
    @Injectable
    private Map<Action, Adapter> adaptersFactory;
    @Tested
    private ETrustExOperationAdapterFactory eTrustExOperationAdapterFactory;

    @Test
    public void getOperationAdapter_ok() {
        final Action retrieveIcaResponse = Action.RETRIEVE_ICA_RESPONSE;
        final RetrieveICAAdapter retrieveICAAdapter = new RetrieveICAAdapter();
        new Expectations() {{
            adaptersFactory.get(retrieveIcaResponse);
            times = 1;
            result = retrieveICAAdapter;
        }};
        Adapter operationAdapter = eTrustExOperationAdapterFactory.getOperationAdapter(Service.RETRIEVE_ICA_SERVICE, retrieveIcaResponse);

        Assert.assertThat(operationAdapter, is(retrieveICAAdapter));
    }

    @Test
    public void getOperationAdapter_notFound() {
        final Action retrieveIcaResponse = Action.RETRIEVE_ICA_RESPONSE;
        final UnknownOperationAdapter unknownOperationAdapter = new UnknownOperationAdapter();

        new Expectations() {{
            adaptersFactory.get(retrieveIcaResponse);
            times = 1;
            result = null;

            adaptersFactory.get(Action.UNKNOWN);
            times = 1;
            result = unknownOperationAdapter;
        }};
        Adapter operationAdapter = eTrustExOperationAdapterFactory.getOperationAdapter(Service.RETRIEVE_ICA_SERVICE, retrieveIcaResponse);

        Assert.assertThat(operationAdapter, is(unknownOperationAdapter));
    }
}
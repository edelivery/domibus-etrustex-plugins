package eu.europa.ec.etrustex.node.outbox.client.adapters;

import ec.services.wsdl.applicationresponse_2.ApplicationResponsePortType;
import ec.services.wsdl.documentbundle_2.DocumentBundlePortType;
import ec.services.wsdl.documentwrapper_2.DocumentWrapperPortType;
import ec.services.wsdl.inboxrequest_2.InboxRequestPortType;
import ec.services.wsdl.retrieveinterchangeagreementsrequest_2.RetrieveInterchangeAgreementsRequestPortType;
import ec.services.wsdl.retrieverequest_2.RetrieveRequestPortType;
import eu.europa.ec.etrustex.common.util.ETrustExPluginAS4Properties;
import eu.europa.ec.etrustex.node.data.model.administration.EtxUser;
import eu.europa.ec.etrustex.node.service.security.IdentityManager;
import eu.europa.ec.etrustex.node.testutils.ETrustExPluginAS4PropertiesBuilder;
import eu.europa.ec.etrustex.node.webservice.client.NodeWebServiceProvider;
import eu.europa.ec.etrustex.node.webservice.client.WSClient;
import mockit.Injectable;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.jms.core.JmsOperations;

import javax.xml.ws.BindingProvider;

import static org.apache.commons.codec.binary.StringUtils.getBytesUtf8;

/**
 * @author François Gautier
 * @version 1.0
 * @since 08/03/2018
 */
@RunWith(JMockit.class)
public abstract class BaseAdapterTest<
        DWPortType extends DocumentWrapperPortType & BindingProvider,
        ARPortType extends ApplicationResponsePortType & BindingProvider,
        DBPortType extends DocumentBundlePortType & BindingProvider,
        IRPortType extends InboxRequestPortType & BindingProvider,
        RIARequestPortType extends RetrieveInterchangeAgreementsRequestPortType & BindingProvider,
        RRPortType extends RetrieveRequestPortType & BindingProvider
        > {

    static final byte[] HEADER = getBytesUtf8("<ec.schema.xsd.commonaggregatecomponents_2.HeaderType/>");

    @Injectable
    ETrustExPluginAS4Properties etxPluginAS4Properties;
    @Injectable
    NodeWebServiceProvider webServiceProvider;
    @Injectable
    IdentityManager identityManager;
    @Injectable
    JmsOperations etxNodePluginNodeToBackendJmsTemplate;
    @Injectable
    JmsOperations etxNodePluginBackendToNodeJmsTemplate;

    @Mocked
    DWPortType dwPortType;
    @Mocked
    ARPortType arPortType;
    @Mocked
    DBPortType dbPortType;
    @Mocked
    IRPortType irPortType;
    @Mocked
    RIARequestPortType riaRequestPortType;
    @Mocked
    RRPortType rrPortType;

    private EtxUser etxUser;

    @Before
    public void setUp() {
        etxPluginAS4Properties = ETrustExPluginAS4PropertiesBuilder.getInstance();
        etxUser = new EtxUser();
        new MockUp<WSClient>() {
            @Mock
            void validateEndPoint() {
                //Shhh, everything will be alright
            }

            @Mock
            DWPortType buildDocumentWrapperPort(String user, String pwd) {
                return dwPortType;
            }
            @Mock
            ARPortType buildApplicationResponsePort(String user, String pwd) {
                return arPortType;
            }
            @Mock
            DBPortType buildDocumentBundlePort(String user, String pwd) {
                return dbPortType;
            }
            @Mock
            IRPortType buildInboxRequestPort(String user, String pwd) {
                return irPortType;
            }
            @Mock
            RIARequestPortType getRetrieveInterchangeAgreementsRequestPortType() {
                return riaRequestPortType;
            }
            @Mock
            RRPortType buildRetrieveRequestPort(String user, String pwd) {
                return rrPortType;
            }

            @Mock
            void setCredentials(String user, String pwd, BindingProvider port) {
            }
        };
        new MockUp<ETrustExAdapterBase>() {
            @Mock
            EtxUser getUser(String partyUuid) {
                return etxUser;
            }
        };
    }

}

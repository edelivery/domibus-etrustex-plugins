package eu.europa.ec.etrustex.node.outbox.client.adapters;

import ec.schema.xsd.ack_2.AcknowledgmentType;
import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.services.wsdl.documentbundle_2.FaultResponse;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleRequest;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleResponse;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.jms.TextMessageCreator;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import eu.europa.ec.etrustex.node.testutils.FaultResponseUtils;
import eu.europa.ec.etrustex.node.testutils.PayloadDTOBuilder;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import org.hamcrest.core.IsNull;
import org.junit.Test;
import org.springframework.jms.core.MessageCreator;

import javax.xml.ws.Holder;

import static eu.europa.ec.etrustex.common.util.ContentId.*;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.deserializeXMLToObj;
import static eu.europa.ec.etrustex.node.testutils.ETrustExAdapterDTOBuilder.getEmpty;
import static eu.europa.ec.etrustex.node.testutils.ETrustExPluginPropertiesTestConstants.TEST_AS4_ACTION_SUBMITDOCUMENTBUNDLE_RESPONSE;
import static eu.europa.ec.etrustex.node.testutils.ReflectionUtils.getPrivateField;
import static eu.europa.ec.etrustex.node.testutils.ResourcesUtils.readResource;
import static org.apache.commons.codec.binary.StringUtils.getBytesUtf8;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author François Gautier
 * @version 1.0
 * @since 08/03/2018
 */
public class SubmitDocumentBundleAdapterTest extends BaseAdapterTest {
    private static final byte[] PAYLOAD = getBytesUtf8("<ec.services.wsdl.documentbundle__2.SubmitDocumentBundleRequest><documentBundle><profileID><value>TEST Message Subject</value></profileID><id><value>BUNDLE_ID_1</value></id><uuid><value>EUPL V1.2</value></uuid><issueDate><value>2020-04-09T00:00:00.000+02:00</value></issueDate><issueTime><value>02:00:00.000</value></issueTime><note><oasis.names.specification.ubl.schema.xsd.commonbasiccomponents__2.NoteType><value>TEST Message content</value></oasis.names.specification.ubl.schema.xsd.commonbasiccomponents__2.NoteType></note><senderParty><endpointID><value>TEST_SENDER1</value></endpointID></senderParty><receiverParty><oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents__2.PartyType><endpointID><value>TEST_RECEIVER1</value></endpointID></oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents__2.PartyType></receiverParty><documentWrapperReference><ec.schema.xsd.commonaggregatecomponents__2.DocumentWrapperReferenceType><id><value>REF_BUNDLE_ID_1</value></id><documentTypeCode><value>BINARY</value></documentTypeCode><resourceInformationReference><name><value>BinaryFile0.bin</value></name><documentSize><value>10</value></documentSize><documentHashMethod><value>SHA-512</value></documentHashMethod><documentHash><value>663A0D226F2384C68D91C510A549CE0F84EFE07EACC25BA843C1B0D1C26C71C38A57D5395F3DDDF1AAF72383CE6870EE67082C404C0EBE73EBBEA66569B9D36E</value></documentHash></resourceInformationReference></ec.schema.xsd.commonaggregatecomponents__2.DocumentWrapperReferenceType></documentWrapperReference></documentBundle></ec.services.wsdl.documentbundle__2.SubmitDocumentBundleRequest>");

    @Tested
    private SubmitDocumentBundleAdapter submitDocumentBundleAdapter;

    @Injectable
    private ETrustExNodePluginProperties eTrustExNodePluginProperties;

    @Test
    public void performOperation_NoHeaderPayload() throws NoSuchFieldException, IllegalAccessException {
        submitDocumentBundleAdapter.performOperation(getEmpty());

        new FullVerifications() {{
            TextMessageCreator messageCreator;
            etxNodePluginBackendToNodeJmsTemplate.send(messageCreator = withCapture());
            times = 1;

            ETrustExAdapterDTO textMessage =
                    (ETrustExAdapterDTO) deserializeXMLToObj(
                            (String) getPrivateField(TextMessageCreator.class, messageCreator, "dto"));

            assertThat(textMessage.getPayloadFromCID(CONTENT_FAULT), IsNull.notNullValue());
        }};
    }

    @Test
    public void performOperation_NoPayload() throws NoSuchFieldException, IllegalAccessException {

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(CONTENT_ID_HEADER.getValue())
                .withPayload(getBytesUtf8("<ec.schema.xsd.commonaggregatecomponents__2.HeaderType/>"))
                .build());

        submitDocumentBundleAdapter.performOperation(dto);

        new FullVerifications() {{
            TextMessageCreator messageCreator;
            etxNodePluginBackendToNodeJmsTemplate.send(messageCreator = withCapture());
            times = 1;

            ETrustExAdapterDTO textMessage =
                    (ETrustExAdapterDTO) deserializeXMLToObj(
                            (String) getPrivateField(TextMessageCreator.class, messageCreator, "dto"));

            assertThat(textMessage.getPayloadFromCID(CONTENT_FAULT), IsNull.notNullValue());
        }};
    }

    @Test
    public void performOperation_AcknowledgeOk() throws Exception {
        new Expectations() {{

            dbPortType.submitDocumentBundle(((SubmitDocumentBundleRequest) any), (Holder<HeaderType>) any);
            times = 1;
            result = getOkResponse();

        }};

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_HEADER.getValue())
                .withPayload(getBytesUtf8(readResource("data/outbox/document/Header.xml")))
                .build());

        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_GENERIC.getValue())
                .withPayload(PAYLOAD)
                .build());

        submitDocumentBundleAdapter.performOperation(dto);

        new FullVerifications() {{
            etxNodePluginNodeToBackendJmsTemplate.send((MessageCreator) any);
            times = 1;
        }};
    }

    @Test
    public void performOperation_Exception_AlreadyWithFault() throws Exception {
        new Expectations() {{
            dbPortType.submitDocumentBundle(((SubmitDocumentBundleRequest) any), (Holder<HeaderType>) any);
            times = 1;
            result = new Exception();
        }};

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_HEADER.getValue())
                .withPayload(getBytesUtf8(readResource("data/outbox/document/Header.xml")))
                .build());

        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_GENERIC.getValue())
                .withPayload(PAYLOAD)
                .build());

        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(CONTENT_FAULT.getValue())
                .withPayload(getBytesUtf8("ERROR"))
                .build());

        try {
            submitDocumentBundleAdapter.performOperation(dto);
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getError(), is(ETrustExError.DEFAULT));
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void performOperation_Exception() throws Exception {
        new Expectations() {{

            dbPortType.submitDocumentBundle(((SubmitDocumentBundleRequest) any), (Holder<HeaderType>) any);
            times = 1;
            result = new Exception();

        }};

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_HEADER.getValue())
                .withPayload(getBytesUtf8(readResource("data/outbox/document/Header.xml")))
                .build());

        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_GENERIC.getValue())
                .withPayload(PAYLOAD)
                .build());

        submitDocumentBundleAdapter.performOperation(dto);

        new FullVerifications() {{
            TextMessageCreator messageCreator;
            etxNodePluginBackendToNodeJmsTemplate.send(messageCreator = withCapture());
            times = 1;

            ETrustExAdapterDTO textMessage =
                    (ETrustExAdapterDTO) deserializeXMLToObj(
                            (String) getPrivateField(TextMessageCreator.class, messageCreator, "dto"));

            assertThat(textMessage.getPayloadFromCID(CONTENT_FAULT), IsNull.notNullValue());
        }};
    }

    @Test
    public void performOperation_AcknowledgeFault() throws Exception {
        new Expectations() {{

            dbPortType.submitDocumentBundle(((SubmitDocumentBundleRequest) any), (Holder<HeaderType>) any);
            times = 1;
            result = new FaultResponse("", FaultResponseUtils.getFaultType());

        }};

        ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_HEADER.getValue())
                .withPayload(getBytesUtf8(readResource("data/outbox/document/Header.xml")))
                .build());

        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_GENERIC.getValue())
                .withPayload(PAYLOAD)
                .build());

        submitDocumentBundleAdapter.performOperation(dto);

        new FullVerifications() {{

            etxNodePluginNodeToBackendJmsTemplate.send((MessageCreator) any);
            times = 1;

        }};

    }

    @Test
    public void acknowledgeFault() throws NoSuchFieldException, IllegalAccessException {
        ETrustExAdapterDTO dto = getEmpty();

        submitDocumentBundleAdapter.acknowledgeFault(dto, new FaultResponse("ERROR", FaultResponseUtils.getFaultType()));

        new FullVerifications() {{
            TextMessageCreator messageCreator;
            etxNodePluginNodeToBackendJmsTemplate.send(messageCreator = withCapture());
            times = 1;

            ETrustExAdapterDTO textMessage =
                    (ETrustExAdapterDTO) deserializeXMLToObj(
                            (String) getPrivateField(TextMessageCreator.class, messageCreator, "dto"));
            assertThat(textMessage.getAs4Action(), is(TEST_AS4_ACTION_SUBMITDOCUMENTBUNDLE_RESPONSE));

            assertThat(textMessage.getPayloadFromCID(CONTENT_ID_FAULT_RESPONSE), IsNull.notNullValue());
        }};
    }

    @Test
    public void acknowledgeFault_withException() throws NoSuchFieldException, IllegalAccessException {
        ETrustExAdapterDTO dto = getEmpty();

        submitDocumentBundleAdapter.acknowledgeFault(dto, new FaultResponse("ERROR", FaultResponseUtils.getFaultType(), new Exception("TEST")));

        new FullVerifications() {{
            TextMessageCreator messageCreator;
            etxNodePluginNodeToBackendJmsTemplate.send(messageCreator = withCapture());
            times = 1;

            ETrustExAdapterDTO textMessage =
                    (ETrustExAdapterDTO) deserializeXMLToObj(
                            (String) getPrivateField(TextMessageCreator.class, messageCreator, "dto"));
            assertThat(textMessage.getAs4Action(), is(TEST_AS4_ACTION_SUBMITDOCUMENTBUNDLE_RESPONSE));

            assertThat(textMessage.getPayloadFromCID(CONTENT_ID_FAULT_RESPONSE), IsNull.notNullValue());
        }};
    }

    @Test
    public void acknowledgeOk() throws NoSuchFieldException, IllegalAccessException {

        ETrustExAdapterDTO dto = getEmpty();

        submitDocumentBundleAdapter.acknowledgeOk(dto, new HeaderType(), new SubmitDocumentBundleResponse());

        new FullVerifications() {{
            TextMessageCreator messageCreator;
            etxNodePluginNodeToBackendJmsTemplate.send(messageCreator = withCapture());
            times = 1;

            ETrustExAdapterDTO textMessage =
                    (ETrustExAdapterDTO) deserializeXMLToObj(
                            (String) getPrivateField(TextMessageCreator.class, messageCreator, "dto"));
            assertThat(textMessage.getAs4Action(), is(TEST_AS4_ACTION_SUBMITDOCUMENTBUNDLE_RESPONSE));

            assertThat(textMessage.getPayloadFromCID(CONTENT_ID_HEADER), IsNull.notNullValue());
            assertThat(textMessage.getPayloadFromCID(CONTENT_ID_GENERIC), IsNull.notNullValue());
        }};
    }

    private SubmitDocumentBundleResponse getOkResponse() {
        SubmitDocumentBundleResponse response = new SubmitDocumentBundleResponse();
        AcknowledgmentType ack = new AcknowledgmentType();
        response.setAck(ack);
        return response;
    }

}
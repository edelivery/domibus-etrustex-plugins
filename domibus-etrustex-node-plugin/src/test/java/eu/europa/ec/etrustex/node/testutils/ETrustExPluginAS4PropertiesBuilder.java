package eu.europa.ec.etrustex.node.testutils;

import eu.europa.ec.etrustex.common.util.ETrustExPluginAS4Properties;

import static eu.europa.ec.etrustex.node.testutils.ETrustExPluginPropertiesTestConstants.*;

/**
 * @author François Gautier
 * @version 1.0
 * @since 16/02/2018
 */
public class ETrustExPluginAS4PropertiesBuilder {

    public static ETrustExPluginAS4Properties getInstance(){
        ETrustExPluginAS4Properties eTrustExBackendPluginProperties = new ETrustExPluginAS4Properties(){
            public String getAs4ETrustExNodePartyId(){
                return TEST_AS4_ETRUSTEXNODEPARTYID;
            }
            public String getAs4FromPartyIdType(){
                return TEST_AS4_FROMPARTYIDTYPE;
            }
            public String getAs4FromPartyRole(){
                return TEST_AS4_FROMPARTYROLE;
            }
            public String getAs4ToPartyIdType(){
                return TEST_AS4_TOPARTYIDTYPE;
            }
            public String getAs4ToPartyRole(){
                return TEST_AS4_TOPARTYROLE;
            }
            public String getAs4ServiceInboxBundleTransmission(){
                return TEST_AS4_SERVICE_INBOXBUNDLETRANSMISSION;
            }
            public String getAs4ServiceInboxBundleTransmissionServiceType(){
                return TEST_AS4_SERVICE_INBOXBUNDLETRANSMISSION_SERVICETYPE;
            }
            public String getAs4ActionInboxBundleTransmissionAck(){
                return TEST_AS4_SERVICE_INBOXBUNDLETRANSMISSION_ACK;
            }
            public String getAs4_Action_RetrieveICA_Response(){
                return TEST_AS4_ACTION_RETRIEVEICA_RESPONSE;
            }
            public String getAs4_Action_SubmitApplicationResponse_Response(){
                return TEST_AS4_ACTION_SUBMITAPPLICATIONRESPONSE_RESPONSE;
            }
            public String getAs4_Action_StoreDocumentWrapper_Response(){
                return TEST_AS4_ACTION_STOREDOCUMENTWRAPPER_RESPONSE;
            }
            public String getAs4_Action_SubmitDocumentBundle_Response(){
                return TEST_AS4_ACTION_SUBMITDOCUMENTBUNDLE_RESPONSE;
            }
        };

        return eTrustExBackendPluginProperties;
    }
}

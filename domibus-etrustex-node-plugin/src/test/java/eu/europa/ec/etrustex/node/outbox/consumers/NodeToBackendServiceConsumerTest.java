package eu.europa.ec.etrustex.node.outbox.consumers;

import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import eu.europa.ec.etrustex.node.service.NodeConnectorSubmissionService;
import eu.europa.ec.etrustex.node.testutils.ETrustExAdapterDTOBuilder;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.jms.TextMessage;

/**
 * @author François Gautier
 * @version 1.0
 * @since 09/03/2018
 */
@RunWith(JMockit.class)
public class NodeToBackendServiceConsumerTest {

    @Injectable
    private NodeConnectorSubmissionService nodeConnectorSubmissionService;
    @Tested
    private NodeToBackendServiceConsumer nodeToBackendServiceConsumer;
    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Injectable
    private DomainNodePluginService domainNodePluginService;
    @Test
    public void onMessage(final @Mocked TextMessage txtMessage, final @Mocked TextMessageUtil textMessageUtil) {
        final ETrustExAdapterDTO eTrustExAdapterDTO = ETrustExAdapterDTOBuilder.getEmpty();
        new Expectations(){{
            TextMessageUtil.getETrustExAdapterDTO(txtMessage);
            times = 1;
            result = eTrustExAdapterDTO;
        }};

        nodeToBackendServiceConsumer.onMessage(txtMessage);

        new FullVerifications() {{
            nodeConnectorSubmissionService.submitToDomibus(eTrustExAdapterDTO);
            times = 1;
            domainNodePluginService.setNodePluginDomain();
            times = 1;
            clearEtxLogKeysService.clearKeys();
            times = 1;
        }};
    }
}
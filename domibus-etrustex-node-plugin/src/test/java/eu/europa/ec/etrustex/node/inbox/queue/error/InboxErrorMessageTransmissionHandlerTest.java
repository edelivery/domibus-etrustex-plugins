package eu.europa.ec.etrustex.node.inbox.queue.error;

import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.inbox.client.adapter.SubmitApplicationResponseFaultAdapter;
import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxMessageTransmissionQueueMessage;
import eu.europa.ec.etrustex.node.service.AttachmentService;
import eu.europa.ec.etrustex.node.service.MessageServicePlugin;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.UUID;

import static eu.europa.ec.etrustex.node.data.model.message.MessageDirection.NODE_TO_BACKEND;
import static eu.europa.ec.etrustex.node.data.model.message.MessageType.MESSAGE_STATUS;
import static eu.europa.ec.etrustex.node.inbox.queue.error.InboxErrorHandlerBase.getErrorMessage;
import static eu.europa.ec.etrustex.node.testutils.EtxMessageBuilder.formEtxMessageInboxBundle1Wrapper;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * This is a duplicate of {@link InboxErrorDownloadHandlerTest} to verify the implementation with
 * {@link InboxMessageTransmissionQueueMessage}
 *
 * @author François Gautier
 * @version 1.0
 * @since 10-Nov-17
 */
@SuppressWarnings("Duplicates")
@RunWith(JMockit.class)
public class InboxErrorMessageTransmissionHandlerTest {

    @Injectable
    private MessageServicePlugin messageService;

    @Injectable
    private AttachmentService attachmentService;
    @Injectable
    private SubmitApplicationResponseFaultAdapter submitApplicationResponseFaultAdapter;

    @Tested
    private InboxErrorMessageTransmissionHandler inboxErrorMessageTransmissionHandler;

    private Long attachmentId;
    private Long messageId;
    private InboxMessageTransmissionQueueMessage queueMessage;


    @Before
    public void setUp() throws Exception {
        messageId = RandomUtils.nextLong(0, 99999);
        attachmentId = RandomUtils.nextLong(0, 99999);
    }

    @Test
    public void handleErrorWithoutNotification_messageStatus_ok() throws Exception {
        queueMessage = new InboxMessageTransmissionQueueMessage(
                MESSAGE_STATUS,
                NODE_TO_BACKEND,
                messageId,
                null);

        final String messageUuid = UUID.randomUUID().toString();
        final EtxMessage etxMessage = formEtxMessageInboxBundle1Wrapper();
        etxMessage.setId(messageId);
        etxMessage.setMessageUuid(messageUuid);

        new Expectations() {{
            messageService.findMessageById(messageId);
            times = 1;
            result = etxMessage;
        }};

        inboxErrorMessageTransmissionHandler.handleErrorWithoutNotification(queueMessage);

        new FullVerifications() {{
            messageService.messageInError(messageId, getErrorMessage(queueMessage, "message", messageUuid));
            times = 1;
        }};
    }

    @Test
    public void handleErrorWithoutNotification_messageBundleWithAttachment_ok() throws Exception {
        queueMessage = new InboxMessageTransmissionQueueMessage(
                MESSAGE_STATUS,
                NODE_TO_BACKEND,
                messageId,
                attachmentId);

        final String messageUuid = UUID.randomUUID().toString();
        final String attachmentUuid = UUID.randomUUID().toString();

        final EtxMessage etxMessage = formEtxMessageInboxBundle1Wrapper();
        etxMessage.setMessageUuid(messageUuid);
        etxMessage.setId(messageId);

        final EtxAttachment etxAttachment = etxMessage.getAttachmentList().get(0);
        etxAttachment.setAttachmentUuid(attachmentUuid);
        etxAttachment.setId(attachmentId);

        new Expectations() {{
            messageService.findMessageById(messageId);
            times = 1;
            result = etxMessage;

            attachmentService.findAttachmentById(attachmentId);
            times = 1;
            result = etxAttachment;
        }};

        inboxErrorMessageTransmissionHandler.handleErrorWithoutNotification(queueMessage);

        new FullVerifications() {{
            messageService.messageInError(messageId, getErrorMessage(queueMessage, "message", messageUuid));
            times = 1;

            attachmentService.attachmentInError(attachmentId, getErrorMessage(queueMessage, "wrapper", attachmentUuid));
            times = 1;
        }};
    }

    @Test
    public void handleErrorWithNotification_messageBundleWithAttachment_ok() throws Exception {
        queueMessage = new InboxMessageTransmissionQueueMessage(
                MESSAGE_STATUS,
                NODE_TO_BACKEND,
                messageId,
                attachmentId);

        final String messageUuid = UUID.randomUUID().toString();
        final String attachmentUuid = UUID.randomUUID().toString();


        final EtxMessage etxMessage = formEtxMessageInboxBundle1Wrapper();
        etxMessage.setMessageUuid(messageUuid);
        etxMessage.setId(messageId);

        final EtxParty sender = etxMessage.getSender();
        final EtxParty receiver = etxMessage.getReceiver();

        final EtxAttachment etxAttachment = etxMessage.getAttachmentList().get(0);
        etxAttachment.setAttachmentUuid(attachmentUuid);
        etxAttachment.setId(attachmentId);

        new Expectations() {{
            messageService.findMessageById(messageId);
            times = 1;
            result = etxMessage;

            attachmentService.findAttachmentById(attachmentId);
            times = 1;
            result = etxAttachment;
        }};

        inboxErrorMessageTransmissionHandler.handleErrorWithNotification(queueMessage);

        new FullVerifications() {{
            submitApplicationResponseFaultAdapter.sendNotification(receiver, sender, messageUuid);
            times = 1;

            messageService.messageInError(messageId, getErrorMessage(queueMessage, "message", messageUuid));
            times = 1;
            attachmentService.attachmentInError(attachmentId, getErrorMessage(queueMessage, "wrapper", attachmentUuid));
            times = 1;
        }};

    }

    @Test
    public void handleErrorWithNotification_messageBundleWithAttachment_nok() throws Exception {
        queueMessage = new InboxMessageTransmissionQueueMessage(
                MESSAGE_STATUS,
                NODE_TO_BACKEND,
                messageId,
                attachmentId);

        final String messageUuid = UUID.randomUUID().toString();
        final String attachmentUuid = UUID.randomUUID().toString();

        final EtxMessage etxMessage = formEtxMessageInboxBundle1Wrapper();
        etxMessage.setMessageUuid(messageUuid);
        etxMessage.setId(messageId);

        final EtxParty sender = etxMessage.getSender();
        final EtxParty receiver = etxMessage.getReceiver();
        final EtxAttachment etxAttachment = etxMessage.getAttachmentList().get(0);
        etxAttachment.setAttachmentUuid(attachmentUuid);
        etxAttachment.setId(attachmentId);

        new Expectations() {{
            messageService.findMessageById(messageId);
            times = 1;
            result = etxMessage;

            attachmentService.findAttachmentById(attachmentId);
            times = 1;
            result = etxAttachment;

            submitApplicationResponseFaultAdapter.sendNotification(receiver, sender, messageUuid);
            times = 1;
            result = new IllegalArgumentException();
        }};

        try {
            inboxErrorMessageTransmissionHandler.handleErrorWithNotification(queueMessage);
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getError(), is(ETrustExError.DEFAULT));
        }

        new FullVerifications() {{
            messageService.messageInError(messageId, getErrorMessage(queueMessage, "message", messageUuid));
            times = 1;
            attachmentService.attachmentInError(attachmentId, getErrorMessage(queueMessage, "wrapper", attachmentUuid));
            times = 1;
        }};
    }
}
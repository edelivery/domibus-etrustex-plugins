package eu.europa.ec.etrustex.node.inbox.client.adapter;

import ec.schema.xsd.commonaggregatecomponents_2.Base64BinaryType;
import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.schema.xsd.commonaggregatecomponents_2.ResourceInformationReferenceType;
import ec.services.wsdl.documentwrapper_2.DocumentWrapperPortType;
import ec.services.wsdl.documentwrapper_2.RetrieveDocumentWrapperRequestRequest;
import ec.services.wsdl.documentwrapper_2.RetrieveDocumentWrapperRequestResponse;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.service.WorkspaceService;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import eu.europa.ec.etrustex.node.data.model.attachment.AttachmentState;
import eu.europa.ec.etrustex.node.data.model.attachment.ChecksumAlgorithmType;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import eu.europa.ec.etrustex.node.service.AttachmentService;
import eu.europa.ec.etrustex.node.testutils.EtxMessageBuilder;
import eu.europa.ec.etrustex.node.webservice.client.NodeWebServiceProvider;
import eu.europa.ec.etrustex.node.webservice.client.WSClient;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.xml.bind.DatatypeConverter;
import javax.xml.ws.Holder;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;


/**
 * Junit for {@link RetrieveDocumentWrapperAdapter}
 *
 * @author François Gautier, Catalin Enache
 * @version 1.0
 * @since 17-Nov-17
 */
@RunWith(JMockit.class)
public class RetrieveDocumentWrapperAdapterTest {

    private static final String SERVICE_ENDPOINT_TEST = "ServiceEndPoint";

    private static final Path TEST_EUPL_FILE = Paths.get("src/test/resources/data/inbox/retrievedocwrapper/eupl1.1.-licence-en_0.pdf");

    private final String attachmentUuid = UUID.randomUUID().toString();
    private final String mimeCode = "application/pdf";
    private final String hashMethod = "SHA-512";
    private final String documentHash = "26316A5CAE50C2234CA16717C1F1CBE7B0330F7AA663BB94BEB622F1D4751634B4CA4A8CF4298974214C40353C5E6FAEE954561830871B5EFAC02776E9F925CB";

    @Injectable
    private NodeWebServiceProvider webServiceProvider;

    @Injectable
    private ETrustExNodePluginProperties eTrustExNodePluginProperties;

    @Injectable
    private AttachmentService attachmentService;

    @Injectable
    private WorkspaceService etxNodePluginWorkspaceService;

    @Tested
    private RetrieveDocumentWrapperAdapter retrieveDocumentWrapperAdapter;

    @Mocked
    private DocumentWrapperPortType documentWrapperPortType;

    @Before
    public void setUp() throws Exception {
        new MockUp<WSClient>() {
            @Mock
            void validateEndPoint() {
                //It's OK
            }

            @Mock
            DocumentWrapperPortType buildDocumentWrapperPort(String user, String pwd) {
                return documentWrapperPortType;
            }
        };
        new MockUp<NodeServicesAdapterBase>() {
            @Mock
            Holder<HeaderType> fillAuthorisationHeader(String senderPartyName, String receiverPartyName) {
                return new Holder<>();
            }
        };
    }

    @Test
    public void downloadAttachment() throws Exception {
        //retrieveDocumentWrapperAdapter.initServiceEndpoint(SERVICE_ENDPOINT_TEST);
        final List<RetrieveDocumentWrapperRequestRequest> requests = new ArrayList<>();
        final List<Holder<HeaderType>> headers = new ArrayList<>();
        new Expectations() {{

            documentWrapperPortType.retrieveDocumentWrapperRequest(withCapture(requests), withCapture(headers));
            times = 1;
            result = new RetrieveDocumentWrapperRequestResponse(); // TODO FGA: 20-Nov-17  ok result from 2656
        }};

        EtxAttachment etxAttachment = EtxMessageBuilder.formEtxMessageInboxBundle1Wrapper().getAttachmentList().get(0);
        retrieveDocumentWrapperAdapter.downloadAttachment(etxAttachment);

        new FullVerifications() {{

        }};
    }

    @Test
    public void updateAttachment_RetrieveWrapperSuccessfully_WriteFileUpdateAttachment(final @Mocked EtxAttachment etxAttachment,
                                                                                       final @Mocked RetrieveDocumentWrapperRequestResponse retrieveDocumentWrapperRequestResponse,
                                                                                       final @Mocked ResourceInformationReferenceType resourceInformationReferenceType,
                                                                                       final @Mocked Base64BinaryType binaryType,
                                                                                       final @Mocked InputStream inputStream) throws Exception {
        final long fileSize = 34271L;

        new Expectations() {{
            retrieveDocumentWrapperRequestResponse.getDocumentWrapper().getResourceInformationReference();
            times = 1;
            result = resourceInformationReferenceType;

            resourceInformationReferenceType.getLargeAttachment().getStreamBase64Binary();
            times = 1;
            result = binaryType;

            binaryType.getMimeCode();
            times = 1;
            result = mimeCode;

            resourceInformationReferenceType.getDocumentHash().getValue();
            times = 1;
            result = documentHash;

            resourceInformationReferenceType.getDocumentHashMethod().getValue();
            times = 1;
            result = hashMethod;

            resourceInformationReferenceType.getDocumentSize().getValue();
            times = 2;
            result = new BigDecimal(fileSize);

            etxNodePluginWorkspaceService.writeFile(etxAttachment.getId(), binaryType.getValue().getInputStream());
            times = 1;
            result = TEST_EUPL_FILE;

            inputStream.close();
            times = 1;
        }};

        //tested method
        retrieveDocumentWrapperAdapter.updateAttachment(etxAttachment, retrieveDocumentWrapperRequestResponse);

        new FullVerifications() {{
            String actualValue;
            etxAttachment.setMimeType(actualValue = withCapture());
            Assert.assertEquals(mimeCode, actualValue);

            byte[] checksumActual;
            etxAttachment.setChecksum(checksumActual = withCapture());
            Assert.assertTrue(Objects.equals(documentHash, DatatypeConverter.printHexBinary(checksumActual)));

            ChecksumAlgorithmType checksumAlgorithmTypeActual;
            etxAttachment.setChecksumAlgorithmType(checksumAlgorithmTypeActual = withCapture());
            Assert.assertEquals(ChecksumAlgorithmType.fromValue(hashMethod), checksumAlgorithmTypeActual);

            etxAttachment.setFilePath(actualValue = withCapture());
            Assert.assertEquals(TEST_EUPL_FILE.toAbsolutePath().toString(), actualValue);

            AttachmentState oldAttachmentStateActual, newAttachmentStateActual;
            attachmentService.updateAttachmentState(etxAttachment, oldAttachmentStateActual = withCapture(), newAttachmentStateActual = withCapture());
            Assert.assertEquals(AttachmentState.CREATED, oldAttachmentStateActual);
            Assert.assertEquals(AttachmentState.DOWNLOADED, newAttachmentStateActual);

        }};
    }


    @Test
    public void updateAttachment_WrapperDifferentSize_ExceptionThrown(final @Mocked EtxAttachment etxAttachment,
                                                                      final @Mocked RetrieveDocumentWrapperRequestResponse retrieveDocumentWrapperRequestResponse,
                                                                      final @Mocked ResourceInformationReferenceType resourceInformationReferenceType,
                                                                      final @Mocked Base64BinaryType binaryType,
                                                                      final @Mocked InputStream inputStream) throws Exception {
        final long fileSize = 34272L;

        new Expectations() {{
            retrieveDocumentWrapperRequestResponse.getDocumentWrapper().getResourceInformationReference();
            times = 1;
            result = resourceInformationReferenceType;

            resourceInformationReferenceType.getLargeAttachment().getStreamBase64Binary();
            times = 1;
            result = binaryType;

            binaryType.getMimeCode();
            times = 1;
            result = mimeCode;

            resourceInformationReferenceType.getDocumentHash().getValue();
            times = 1;
            result = documentHash;

            resourceInformationReferenceType.getDocumentHashMethod().getValue();
            times = 1;
            result = hashMethod;

            resourceInformationReferenceType.getDocumentSize().getValue();
            times = 2;
            result = new BigDecimal(fileSize);

            etxNodePluginWorkspaceService.writeFile(etxAttachment.getId(), binaryType.getValue().getInputStream());
            times = 1;
            result = TEST_EUPL_FILE;

            inputStream.close();
            times = 1;

        }};

        try {
            //tested method
            retrieveDocumentWrapperAdapter.updateAttachment(etxAttachment, retrieveDocumentWrapperRequestResponse);
        } catch (ETrustExPluginException ex) {
            Assert.assertEquals(ETrustExError.DEFAULT, ex.getError());
            Assert.assertEquals("Download size mismatch. Actual size (" + Files.size(TEST_EUPL_FILE) + ") <> expected size (" + fileSize + ")", ex.getMessage());
        }

        new FullVerifications() {{

            String actualValue;
            etxAttachment.setMimeType(actualValue = withCapture());
            Assert.assertEquals(mimeCode, actualValue);

            byte[] checksumActual;
            etxAttachment.setChecksum(checksumActual = withCapture());
            Assert.assertEquals(documentHash, DatatypeConverter.printHexBinary(checksumActual));

            ChecksumAlgorithmType checksumAlgorithmTypeActual;
            etxAttachment.setChecksumAlgorithmType(checksumAlgorithmTypeActual = withCapture());
            Assert.assertEquals(ChecksumAlgorithmType.fromValue(hashMethod), checksumAlgorithmTypeActual);
        }};
    }


    @Test
    public void updateAttachment_WriteFileFails_ExceptionThrown(final @Mocked EtxAttachment etxAttachment,
                                                                final @Mocked RetrieveDocumentWrapperRequestResponse retrieveDocumentWrapperRequestResponse,
                                                                final @Mocked ResourceInformationReferenceType resourceInformationReferenceType,
                                                                final @Mocked Base64BinaryType binaryType,
                                                                final @Mocked InputStream inputStream) throws Exception {
        final long fileSize = 34272L;
        final String ioExceptionError = "unable to write file";

        new Expectations() {{
            retrieveDocumentWrapperRequestResponse.getDocumentWrapper().getResourceInformationReference();
            times = 1;
            result = resourceInformationReferenceType;

            resourceInformationReferenceType.getLargeAttachment().getStreamBase64Binary();
            times = 1;
            result = binaryType;

            binaryType.getMimeCode();
            times = 1;
            result = mimeCode;

            resourceInformationReferenceType.getDocumentHash().getValue();
            times = 1;
            result = documentHash;

            resourceInformationReferenceType.getDocumentHashMethod().getValue();
            times = 1;
            result = hashMethod;

            resourceInformationReferenceType.getDocumentSize().getValue();
            times = 2;
            result = new BigDecimal(fileSize);

            etxNodePluginWorkspaceService.writeFile(etxAttachment.getId(), binaryType.getValue().getInputStream());
            times = 1;
            result = new IOException(ioExceptionError);

            inputStream.close();
            times = 1;

        }};

        try {
            //tested method
            retrieveDocumentWrapperAdapter.updateAttachment(etxAttachment, retrieveDocumentWrapperRequestResponse);
        } catch (ETrustExPluginException ex) {
            Assert.assertEquals(ETrustExError.DEFAULT, ex.getError());
            Assert.assertEquals(ioExceptionError, ex.getMessage());
        }

        new FullVerifications() {{

            String actualValue;
            etxAttachment.setMimeType(actualValue = withCapture());
            Assert.assertEquals(mimeCode, actualValue);

            byte[] checksumActual;
            etxAttachment.setChecksum(checksumActual = withCapture());
            Assert.assertTrue(Objects.equals(documentHash, DatatypeConverter.printHexBinary(checksumActual)));

            ChecksumAlgorithmType checksumAlgorithmTypeActual;
            etxAttachment.setChecksumAlgorithmType(checksumAlgorithmTypeActual = withCapture());
            Assert.assertEquals(ChecksumAlgorithmType.fromValue(hashMethod), checksumAlgorithmTypeActual);
        }};
    }
}
package eu.europa.ec.etrustex.node.outbox.client.adapters;

import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author François Gautier
 * @version 1.0
 * @since 09/03/2018
 */
public class UnknownOperationAdapterTest {

    @Test(expected = NullPointerException.class)
    public void performOperation_NPE() {
            new UnknownOperationAdapter().performOperation(null);
    }

    @Test
    public void performOperation() {
        try {
            new UnknownOperationAdapter().performOperation(new ETrustExAdapterDTO());
        } catch (ETrustExPluginException e) {
            Assert.assertThat(e.getError(), Is.is(ETrustExError.DEFAULT));
        }
    }
}
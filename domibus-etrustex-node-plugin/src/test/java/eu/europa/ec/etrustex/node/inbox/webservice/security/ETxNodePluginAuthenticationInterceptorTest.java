package eu.europa.ec.etrustex.node.inbox.webservice.security;

import eu.domibus.ext.services.AuthenticationExtService;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.fail;

/**
 * JUnit for {@link ETxNodePluginAuthenticationInterceptor}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 12/12/2017
 */
@RunWith(JMockit.class)
public class ETxNodePluginAuthenticationInterceptorTest {

    @Injectable
    private AuthenticationExtService authenticationExtService;

    @Tested
    private ETxNodePluginAuthenticationInterceptor authenticationInterceptor;


    @Test
    public void handleMessage_UserAuthenticated(final @Mocked Message cxfMessage,
                                                final @Mocked HttpServletRequest httpRequest) {

        new Expectations() {{
            cxfMessage.get(AbstractHTTPDestination.HTTP_REQUEST);
            times = 1;
            result = httpRequest;

            httpRequest.getPathInfo();
            times = 1;
            result = "/e-trustex/integration/services/inbox/DocumentBundleService/v2.0";

        }};

        //tested method
        authenticationInterceptor.handleMessage(cxfMessage);

        new FullVerifications() {{
            authenticationExtService.authenticate(httpRequest);
            times = 1;
        }};
    }

    @Test
    public void handleMessage_CredentialsNotProvided(final @Mocked Message cxfMessage,
                                                     final @Mocked HttpServletRequest httpRequest) {

        //stub the static method

        new Expectations() {{
            cxfMessage.get(AbstractHTTPDestination.HTTP_REQUEST);
            times = 1;
            result = httpRequest;

            httpRequest.getPathInfo();
            result = "/e-trustex/integration/services/inbox/DocumentBundleService/v2.0";

            authenticationExtService.authenticate(httpRequest);
            times = 1;
            result = new Fault(new Exception());
        }};

        try {
            //tested method
            authenticationInterceptor.handleMessage(cxfMessage);
            fail("Fault expected");
        } catch (Fault fault) {
            //OK
        }

        new FullVerifications() {{
        }};
    }

}
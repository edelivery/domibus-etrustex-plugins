package eu.europa.ec.etrustex.node.outbox.handlers;

import ec.schema.xsd.ack_2.AcknowledgmentType;
import ec.services.wsdl.documentwrapper_2.StoreDocumentWrapperResponse;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.LargePayloadDTO;
import eu.europa.ec.etrustex.common.service.WorkspaceFileService;
import eu.europa.ec.etrustex.common.util.AS4MessageConstants;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.node.testutils.PayloadDTOBuilder;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jms.core.JmsOperations;
import org.springframework.jms.core.MessageCreator;

import javax.activation.DataHandler;
import javax.mail.util.ByteArrayDataSource;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;

import static eu.europa.ec.etrustex.node.testutils.ETrustExAdapterDTOBuilder.getEmpty;
import static eu.europa.ec.etrustex.node.testutils.ResourcesUtils.readResource;

/**
 * @author Federico Martini
 * @version 1.0
 * @since 10-Jan-18
 */
@SuppressWarnings({"Duplicates", "unused", "ResultOfMethodCallIgnored"})
@RunWith(JMockit.class)
public class StoreDocumentWrapperHandlerTest {

    private static final String SERVICE_ENDPOINT_TEST = "ServiceEndPoint";
    private static final String MESSAGE_FAULT = "MessageFault";
    private static final String FAULT_CODE = "FaultCode";

    @Injectable
    private JmsOperations jmsOperations;

    @Injectable
    private WorkspaceFileService etxNodePluginWorkspaceService;

    @Tested
    private StoreDocumentWrapperHandler storeDocumentWrapperHandler;

    @Test
    public void handle_Ok() throws Exception {

        DataHandler dataHandler = new DataHandler(new ByteArrayDataSource(readResource("data/outbox/document/LargePayloadDTO.xml"), "text/xml"));
        final LargePayloadDTO largePayloadDTO = new LargePayloadDTO(ContentId.CONTENT_ID_DOCUMENT.getValue(), AS4MessageConstants.MIME_TYPE_APP_OCTET, dataHandler, "JUNIT TEST", Locale.getDefault(), "certificate.pdf");
        largePayloadDTO.setFileUID("LargePayloadUID");
        Path workspaceRoot = Paths.get("data/outbox/document").normalize().toAbsolutePath();
        final Path path = workspaceRoot.resolve("LargePayloadDTO.xml");
        largePayloadDTO.setFilePath(path.toString());
        largePayloadDTO.setFileName(String.valueOf(path.getFileName()));
        final String fileUID = largePayloadDTO.getFileName();

        final ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_HEADER.getValue())
                .withPayload(readResource("data/outbox/document/Header.xml").getBytes(StandardCharsets.UTF_8))
                .build());

        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_GENERIC.getValue())
                .withPayload(readResource("data/outbox/document/StoreDocWrapperRequest.xml").getBytes(StandardCharsets.UTF_8))
                .build());

        dto.addAs4Payload(largePayloadDTO);

        new Expectations() {{

            String fileUID1 = fileUID + "-" + LargePayloadDTO.PAYLOAD_SUFFIX;
            etxNodePluginWorkspaceService.getFile(largePayloadDTO.getFilePath(), fileUID1);
            result = path;
            times = 1;

        }};

        storeDocumentWrapperHandler.handle(dto);

        new FullVerifications() {{

            etxNodePluginWorkspaceService.writeLargeFile((Path) any, (InputStream) any);
            times = 1;

            jmsOperations.send((MessageCreator) any);
            times = 1;

        }};

    }

    @Test
    public void handle_Exception() throws Exception {

        DataHandler dataHandler = new DataHandler(new ByteArrayDataSource(readResource("data/outbox/document/LargePayloadDTO.xml"), "text/xml"));
        final LargePayloadDTO largePayloadDTO = new LargePayloadDTO(ContentId.CONTENT_ID_DOCUMENT.getValue(), AS4MessageConstants.MIME_TYPE_APP_OCTET, dataHandler, "JUNIT TEST", Locale.getDefault(), "certificate.pdf");
        largePayloadDTO.setFileUID("LargePayloadUID");
        Path workspaceRoot = Paths.get("data/outbox/document").normalize().toAbsolutePath();
        final Path path = workspaceRoot.resolve("LargePayloadDTO.xml");
        largePayloadDTO.setFileName(String.valueOf(path.getFileName()));
        final String fileUID = largePayloadDTO.getFileName();

        final ETrustExAdapterDTO dto = getEmpty();
        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_HEADER.getValue())
                .withPayload(readResource("data/outbox/document/Header.xml").getBytes(StandardCharsets.UTF_8))
                .build());

        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_GENERIC.getValue())
                .withPayload(readResource("data/outbox/document/StoreDocWrapperRequest.xml").getBytes(StandardCharsets.UTF_8))
                .build());

        dto.addAs4Payload(largePayloadDTO);

        new Expectations() {{

            etxNodePluginWorkspaceService.getFile(anyString, anyString);
            result = new Exception("TEST");
            times = 1;

        }};

        try {
            storeDocumentWrapperHandler.handle(dto);
            Assert.fail();
        } catch (ETrustExPluginException ex) {
            Assert.assertEquals(ETrustExError.DEFAULT, ex.getError());
            Assert.assertEquals("Could not save payload to file [" + fileUID + "-.payload]", ex.getMessage());
        }

        new FullVerifications() {{
        }};

    }

    private StoreDocumentWrapperResponse getOkResponse() {
        StoreDocumentWrapperResponse response = new StoreDocumentWrapperResponse();
        AcknowledgmentType ack = new AcknowledgmentType();
        response.setAck(ack);
        return response;
    }

}
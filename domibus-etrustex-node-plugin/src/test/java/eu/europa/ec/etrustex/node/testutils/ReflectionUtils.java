package eu.europa.ec.etrustex.node.testutils;

import org.junit.Assert;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * @author François Gautier, Catalin Enache
 * @version 1.0
 * @since 20-Nov-17
 */
public class ReflectionUtils {

    public static <T> Object getPrivateField(Class<T> clazz, T instance, String fieldName) throws NoSuchFieldException, IllegalAccessException {
        Field privateStringField = clazz.getDeclaredField(fieldName);
        privateStringField.setAccessible(true);
        return privateStringField.get(instance);
    }

    /**
     * Testing of private constructors for Utils classes - like this one :)
     * @param clazz Class having private constructor
     * @throws Exception
     */
    public static void testPrivateConstructor(Class clazz) throws Exception {
        final Constructor<?>[] constructors = clazz.getDeclaredConstructors();

        // check that all constructors are 'private':
        for (final Constructor<?> constructor : constructors) {
            Assert.assertTrue(Modifier.isPrivate(constructor.getModifiers()));
        }
        // call the private constructor:
        constructors[0].setAccessible(true);
        constructors[0].newInstance((Object[]) null);
    }
}

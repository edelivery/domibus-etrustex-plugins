package eu.europa.ec.etrustex.node.testutils;

import eu.europa.ec.etrustex.common.model.entity.attachment.AttachmentType;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.data.model.administration.EtxUser;
import eu.europa.ec.etrustex.node.data.model.attachment.AttachmentDirection;
import eu.europa.ec.etrustex.node.data.model.attachment.AttachmentState;
import eu.europa.ec.etrustex.node.data.model.attachment.ChecksumAlgorithmType;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;

/**
 * @author Arun Raj
 * @version 1.0
 * @since 18/10/2017
 */
public class EtxMessageBuilder {

    private static final Long testEtxMessageId = 1L;
    private static final Long testEtxAttachmentId1 = 1L;
    private static final String NAME = "NAME";
    private static final String PWD = "PASSWORD";

    /**
     * @return dummy instance of {@link EtxMessage} for InboxService {@link MessageDirection#NODE_TO_BACKEND}, {@link MessageType#MESSAGE_BUNDLE}
     * having 1 {@link EtxAttachment}
     */
    public static EtxMessage formEtxMessageInboxBundle1Wrapper() {
        EtxMessage etxMessage = new EtxMessage();
        etxMessage.setId(testEtxMessageId);
        etxMessage.setMessageType(MessageType.MESSAGE_BUNDLE);
        etxMessage.setMessageUuid("NodeToBackend_MessageId_1");
        etxMessage.setMessageState(MessageState.CREATED);
        etxMessage.setDirectionType(MessageDirection.NODE_TO_BACKEND);
        etxMessage.setMessageReferenceUuid("Msg Ref Id");
        etxMessage.setXml("<ns7:SubmitDocumentBundleRequest xmlns=\"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2\" xmlns:ns2=\"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2\" xmlns:ns3=\"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2\" xmlns:ns4=\"ec:schema:xsd:CommonAggregateComponents-2\" xmlns:xmime=\"http://www.w3.org/2005/05/xmlmime\" xmlns:ns6=\"ec:schema:xsd:CommonBasicComponents-1\" xmlns:ns7=\"ec:services:wsdl:DocumentBundle-2\" xmlns:ns8=\"http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader\" xmlns:ns9=\"urn:oasis:names:specification:ubl:schema:xsd:SignatureBasicComponents-2\" xmlns:ns10=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:ns11=\"urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2\" xmlns:ns12=\"http://uri.etsi.org/01903/v1.3.2#\" xmlns:ns13=\"ec:schema:xsd:Ack-2\" xmlns:ns14=\"ec:schema:xsd:DocumentBundle-1\" xmlns:ns15=\"urn:oasis:names:specification:ubl:schema:xsd:Fault-1\" xmlns:ns16=\"http://uri.etsi.org/01903/v1.4.1#\"><ns7:DocumentBundle><ProfileID>ARUN EUPL TEST SUBJECT</ProfileID><ID>ARUN_TEST_SndMsgBundle_ID201709111530</ID><UUID>EUPL V1.2</UUID><IssueDate>2017-06-27</IssueDate><IssueTime>12:12:12.000</IssueTime><Note>ARUN EUPL TEST MSG CONTENT</Note><ns3:SenderParty><EndpointID>TEST_EGREFFE_APP_PARTY_B4</EndpointID></ns3:SenderParty><ns3:ReceiverParty><EndpointID>TEST_EGREFFE_APP_PARTY-2_B4</EndpointID></ns3:ReceiverParty><ns4:DocumentWrapperReference><ID>ARUN_TEST_AT201709111530_1</ID><DocumentTypeCode>BINARY</DocumentTypeCode><ns4:ResourceInformationReference><Name>eupl1.1.-licence-en_0.pdf</Name><ns4:DocumentSize>34271</ns4:DocumentSize><ns4:DocumentHashMethod>SHA-512</ns4:DocumentHashMethod><DocumentHash>26316A5CAE50C2234CA16717C1F1CBE7B0330F7AA663BB94BEB622F1D4751634B4CA4A8CF4298974214C40353C5E6FAEE954561830871B5EFAC02776E9F925CB</DocumentHash></ns4:ResourceInformationReference></ns4:DocumentWrapperReference></ns7:DocumentBundle></ns7:SubmitDocumentBundleRequest>");

        EtxAttachment etxAttachment = new EtxAttachment();
        etxAttachment.setId(testEtxAttachmentId1);
        etxAttachment.setMessage(etxMessage);
        etxAttachment.setAttachmentUuid("NodeToBackend_AttachmentId_1");
        etxAttachment.setAttachmentType(AttachmentType.BINARY);
        etxAttachment.setStateType(AttachmentState.DOWNLOADED);
        etxAttachment.setDirectionType(AttachmentDirection.NODE_TO_BACKEND);
        etxAttachment.setFileName("eupl1.1.-licence-en_0.pdf.payload");
        etxAttachment.setFilePath("./src/test/resources");
        etxAttachment.setFileSize(34271L);
        etxAttachment.setMimeType("application/pdf");
        etxAttachment.setChecksum("26316A5CAE50C2234CA16717C1F1CBE7B0330F7AA663BB94BEB622F1D4751634B4CA4A8CF4298974214C40353C5E6FAEE954561830871B5EFAC02776E9F925CB".getBytes());
        etxAttachment.setChecksumAlgorithmType(ChecksumAlgorithmType.SHA_512);
        etxAttachment.setXml("<ns8:RetrieveDocumentWrapperRequestResponse xmlns:ns8=\"ec:services:wsdl:DocumentWrapper-2\" xmlns:ns10=\"ec:schema:xsd:CommonAggregateComponents-2_1\" xmlns:ns11=\"ec:schema:xsd:CommonBasicComponents-0.1\" xmlns:ns2=\"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2\" xmlns:ns3=\"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2\" xmlns:ns4=\"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2\" xmlns:ns5=\"ec:schema:xsd:CommonAggregateComponents-2\" xmlns:ns7=\"ec:schema:xsd:CommonBasicComponents-1\" xmlns:ns9=\"ec:schema:xsd:CommonAggregateComponents-0.1\" xmlns:xmime=\"http://www.w3.org/2005/05/xmlmime\"><ns8:DocumentWrapper><ns2:ID>ARUN_TEST_AT201709111530_1</ns2:ID><ns2:IssueDate>2017-06-27</ns2:IssueDate><ns2:DocumentTypeCode>BINARY</ns2:DocumentTypeCode><ns4:SenderParty><ns2:EndpointID>TEST_EGREFFE_APP_PARTY_B4</ns2:EndpointID></ns4:SenderParty><ns5:ResourceInformationReference><ns5:DocumentSize>34271</ns5:DocumentSize><ns5:LargeAttachment><ns5:StreamBase64Binary mimeCode=\"application/pdf\" xmime:contentType=\"application/octet-stream\"><xop:Include href=\"cid:c84e4e26-4ec5-4a27-a794-a49116ad8972%40null\" xmlns:xop=\"http://www.w3.org/2004/08/xop/include\"/></ns5:StreamBase64Binary></ns5:LargeAttachment></ns5:ResourceInformationReference></ns8:DocumentWrapper></ns8:RetrieveDocumentWrapperRequestResponse>");
        etxMessage.addAttachment(etxAttachment);

        EtxParty senderParty = new EtxParty();
        senderParty.setId(20L);
        senderParty.setPartyUuid("TEST_EGREFFE_APP_PARTY_B4");


        EtxUser nodeUser = new EtxUser();
        nodeUser.setName(NAME);
        nodeUser.setPassword(PWD);
        senderParty.setNodeUser(nodeUser);
        etxMessage.setSender(senderParty);

        EtxParty receiverParty = new EtxParty();
        receiverParty.setId(25L);
        receiverParty.setPartyUuid("TEST_EGREFFE_APP_PARTY-2_B4");
        receiverParty.setNodeUser(getNodeUser());
        etxMessage.setReceiver(receiverParty);

        etxMessage.setDomibusMessageId("DOMIBUS_ID");
        return etxMessage;
    }

    private static EtxUser getNodeUser() {
        EtxUser etxUser = new EtxUser();
        etxUser.setPassword(PWD);
        etxUser.setName(NAME);
        return etxUser;
    }
}

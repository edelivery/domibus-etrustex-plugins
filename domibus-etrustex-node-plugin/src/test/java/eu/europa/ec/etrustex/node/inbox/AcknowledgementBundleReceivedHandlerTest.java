package eu.europa.ec.etrustex.node.inbox;

import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.data.model.attachment.EtxAttachment;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.inbox.queue.downloadattachment.InboxDownloadAttachmentProducer;
import eu.europa.ec.etrustex.node.service.MessageServicePlugin;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

/**
 * @author François Gautier
 * @version 1.0
 * @since 04-Dec-17
 */
@SuppressWarnings("Duplicates")
@RunWith(JMockit.class)
public class AcknowledgementBundleReceivedHandlerTest {

    private static final String AS_4_REF_TO_MESSAGE_ID = UUID.randomUUID().toString();
    private static final long MESSAGE_BUNDLE_ID = RandomUtils.nextLong(0, 99999);
    @Injectable
    private InboxDownloadAttachmentProducer inboxDownloadAttachmentProducer;
    @Injectable
    private MessageServicePlugin messageService;

    @Tested
    private AcknowledgementBundleReceivedHandler acknowledgementBundleReceivedHandler;

    @Test
    public void handle_NoAttachment() {
        ETrustExAdapterDTO dataTransferObject = new ETrustExAdapterDTO();
//        dataTransferObject.addAs4Payload(getHeaderPayloadDTO(readResource("data/inbox/acknowledgement/Header.xml"), Locale.getDefault()));
        dataTransferObject.setAs4RefToMessageId(AS_4_REF_TO_MESSAGE_ID);
        final EtxMessage message = getMessage(MESSAGE_BUNDLE_ID, new ArrayList<EtxAttachment>(), getReceiver(true));

        new Expectations() {{

            messageService.findMessageByDomibusMessageId(AS_4_REF_TO_MESSAGE_ID);
            times = 1;
            result = message;
        }};

        acknowledgementBundleReceivedHandler.handle(dataTransferObject);

        new FullVerifications() {{
        }};
    }

    @Test
    public void handle_OneAttachment_receiverNotFound() {
        ETrustExAdapterDTO dataTransferObject = new ETrustExAdapterDTO();
//        dataTransferObject.addAs4Payload(getHeaderPayloadDTO(readResource("data/inbox/acknowledgement/Header.xml"), Locale.getDefault()));
        dataTransferObject.setAs4RefToMessageId(AS_4_REF_TO_MESSAGE_ID);
        final long attachmentId = RandomUtils.nextLong(0, 99999);
        final EtxMessage message = getMessage(MESSAGE_BUNDLE_ID, singletonList(getEtxAttachment(attachmentId)), null);

        new Expectations() {{
            messageService.findMessageByDomibusMessageId(AS_4_REF_TO_MESSAGE_ID);
            times = 1;
            result = message;
        }};

        acknowledgementBundleReceivedHandler.handle(dataTransferObject);

        new FullVerifications() {{
        }};
    }
    @Test
    public void handle_OneAttachment_requiresWrapper() {
        ETrustExAdapterDTO dataTransferObject = new ETrustExAdapterDTO();
//        dataTransferObject.addAs4Payload(getHeaderPayloadDTO(readResource("data/inbox/acknowledgement/Header.xml"), Locale.getDefault()));
        dataTransferObject.setAs4RefToMessageId(AS_4_REF_TO_MESSAGE_ID);
        final long attachmentId = RandomUtils.nextLong(0, 99999);
        final EtxMessage message = getMessage(MESSAGE_BUNDLE_ID, singletonList(getEtxAttachment(attachmentId)), getReceiver(true));

        new Expectations() {{
            messageService.findMessageByDomibusMessageId(AS_4_REF_TO_MESSAGE_ID);
            times = 1;
            result = message;
        }};

        acknowledgementBundleReceivedHandler.handle(dataTransferObject);

        new FullVerifications() {{
            inboxDownloadAttachmentProducer.triggerDownloadAttachment(MESSAGE_BUNDLE_ID, attachmentId);
            times = 1;
        }};
    }

    @Test
    public void handle_OneAttachment_requiresWrapperNo() {
        ETrustExAdapterDTO dataTransferObject = new ETrustExAdapterDTO();
//        dataTransferObject.addAs4Payload(getHeaderPayloadDTO(readResource("data/inbox/acknowledgement/Header.xml"), Locale.getDefault()));
        dataTransferObject.setAs4RefToMessageId(AS_4_REF_TO_MESSAGE_ID);
        final long attachmentId = RandomUtils.nextLong(0, 99999);
        final EtxMessage message = getMessage(MESSAGE_BUNDLE_ID,
                singletonList(getEtxAttachment(attachmentId)),
                getReceiver(false));

        new Expectations() {{
            messageService.findMessageByDomibusMessageId(AS_4_REF_TO_MESSAGE_ID);
            times = 1;
            result = message;
        }};

        acknowledgementBundleReceivedHandler.handle(dataTransferObject);

        new FullVerifications() {{
        }};
    }

    @Test
    public void handle_TwoAttachments() {
        ETrustExAdapterDTO dataTransferObject = new ETrustExAdapterDTO();
//        dataTransferObject.addAs4Payload(getHeaderPayloadDTO(readResource("data/inbox/acknowledgement/Header.xml"), Locale.getDefault()));
        dataTransferObject.setAs4RefToMessageId(AS_4_REF_TO_MESSAGE_ID);
        final long attachmentId1 = RandomUtils.nextLong(0, 99999);
        final long attachmentId2 = RandomUtils.nextLong(0, 99999);
        final EtxMessage message = getMessage(
                MESSAGE_BUNDLE_ID,
                asList(
                        getEtxAttachment(attachmentId1),
                        getEtxAttachment(attachmentId2)),
                getReceiver(true));

        new Expectations() {{
            messageService.findMessageByDomibusMessageId(AS_4_REF_TO_MESSAGE_ID);
            times = 1;
            result = message;
        }};

        acknowledgementBundleReceivedHandler.handle(dataTransferObject);

        new FullVerifications() {{
            inboxDownloadAttachmentProducer.triggerDownloadAttachment(MESSAGE_BUNDLE_ID, attachmentId1);
            times = 1;
            inboxDownloadAttachmentProducer.triggerDownloadAttachment(MESSAGE_BUNDLE_ID, attachmentId2);
            times = 1;
        }};
    }

    private EtxAttachment getEtxAttachment(long id) {
        EtxAttachment etxAttachment = new EtxAttachment();
        etxAttachment.setId(id);
        return etxAttachment;
    }

    private EtxMessage getMessage(long id, List<EtxAttachment> attachmentList, EtxParty receiver) {
        EtxMessage message = new EtxMessage();
        message.setId(id);
        message.setAttachmentList(attachmentList);
        message.setReceiver(receiver);
        return message;
    }

    private EtxParty getReceiver(boolean requiresWrappers) {
        EtxParty etxParty = new EtxParty();
        etxParty.setRequiresWrappers(requiresWrappers);
        return etxParty;
    }
}
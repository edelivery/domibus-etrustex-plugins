package eu.europa.ec.etrustex.node.outbox.consumers;

import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.handlers.Action;
import eu.europa.ec.etrustex.common.handlers.Service;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.FaultDTO;
import eu.europa.ec.etrustex.common.model.LargePayloadDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.service.WorkspaceFileService;
import eu.europa.ec.etrustex.common.util.AS4MessageConstants;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.common.util.PayloadDescription;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import eu.europa.ec.etrustex.node.service.NodeConnectorSubmissionService;
import eu.europa.ec.etrustex.node.testutils.ETrustExAdapterDTOBuilder;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.activation.DataHandler;
import javax.jms.TextMessage;
import javax.mail.util.ByteArrayDataSource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Locale;

import static eu.europa.ec.etrustex.node.testutils.ResourcesUtils.readResource;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 09/03/2018
 */
@SuppressWarnings({"Duplicates", "unused"})
@RunWith(JMockit.class)
public class FaultServiceConsumerTest {

    private static final String FINAL_RECIPIENT = "Recipient";
    private static final String ORIGINAL_SENDER = "Sender";

    @Injectable
    private NodeConnectorSubmissionService nodeConnectorSubmissionService;

    @Injectable
    private WorkspaceFileService etxNodePluginWorkspaceService;
    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Injectable
    private DomainNodePluginService domainNodePluginService;
    @Tested
    private FaultServiceConsumer faultServiceConsumer;

    private Path path;

    @Before
    public void setUp() {
        path = new File("").toPath();
    }

    @Test
    public void onMessage_noPayload(final @Mocked TextMessage txtMessage, final @Mocked TextMessageUtil textMessageUtil) {
        final ETrustExAdapterDTO eTrustExAdapterDTO = ETrustExAdapterDTOBuilder
                .getInstance()
                .withAction(Action.SUBMIT_APPLICATION_RESPONSE_REQUEST.getCode())
                .withService(Service.APPLICATION_RESPONSE_SERVICE.getCode())
                .withFinalRecipient(FINAL_RECIPIENT)
                .withOriginalSender(ORIGINAL_SENDER)
                .build();

        new Expectations() {{
            TextMessageUtil.getETrustExAdapterDTO(txtMessage);
            times = 1;
            result = eTrustExAdapterDTO;
        }};

        faultServiceConsumer.onMessage(txtMessage);

        new FullVerifications() {{
            ETrustExAdapterDTO dto;
            nodeConnectorSubmissionService.submitToDomibus(dto = withCapture());
            times = 1;

            assertThat(dto.getAs4Action(), is(FaultDTO.FAULT_ACTION));
            assertThat(dto.getAs4FinalRecipient(), is(ORIGINAL_SENDER));
            assertThat(dto.getAs4OriginalSender(), is(FINAL_RECIPIENT));
            domainNodePluginService.setNodePluginDomain();
            times = 1;
            clearEtxLogKeysService.clearKeys();
            times = 1;
        }};
    }

    @Test
    public void onMessage_withPayload(final @Mocked TextMessage txtMessage, final @Mocked TextMessageUtil textMessageUtil) throws IOException {
        final ETrustExAdapterDTO eTrustExAdapterDTO = ETrustExAdapterDTOBuilder
                .getInstance()
                .withAction(Action.SUBMIT_APPLICATION_RESPONSE_REQUEST.getCode())
                .withService(Service.APPLICATION_RESPONSE_SERVICE.getCode())
                .build();

        final LargePayloadDTO largePayloadDTO = getPayloadDTO();
        eTrustExAdapterDTO.addAs4Payload(largePayloadDTO);

        new Expectations() {{
            TextMessageUtil.getETrustExAdapterDTO(txtMessage);
            times = 1;
            result = eTrustExAdapterDTO;

            etxNodePluginWorkspaceService.getFile(largePayloadDTO.getFilePath(), largePayloadDTO.getFileUID());
            times = 1;
            result = path;

        }};

        faultServiceConsumer.onMessage(txtMessage);

        new FullVerifications() {{
            ETrustExAdapterDTO dto;
            nodeConnectorSubmissionService.submitToDomibus(dto = withCapture());
            times = 1;
            etxNodePluginWorkspaceService.deleteFile(path);
            times = 1;
            assertThat(dto.getAs4Action(), is(FaultDTO.FAULT_ACTION));
            domainNodePluginService.setNodePluginDomain();
            times = 1;
            clearEtxLogKeysService.clearKeys();
            times = 1;
        }};
    }

    @Test
    public void onMessage_withPayload_exception(final @Mocked TextMessage txtMessage, final @Mocked TextMessageUtil textMessageUtil) throws IOException {
        final ETrustExAdapterDTO eTrustExAdapterDTO = ETrustExAdapterDTOBuilder
                .getInstance()
                .withAction(Action.SUBMIT_APPLICATION_RESPONSE_REQUEST.getCode())
                .withService(Service.APPLICATION_RESPONSE_SERVICE.getCode())
                .build();

        final LargePayloadDTO largePayloadDTO = getPayloadDTO();
        eTrustExAdapterDTO.addAs4Payload(largePayloadDTO);

        new Expectations() {{
            TextMessageUtil.getETrustExAdapterDTO(txtMessage);
            times = 1;
            result = eTrustExAdapterDTO;

            etxNodePluginWorkspaceService.getFile(largePayloadDTO.getFilePath(), largePayloadDTO.getFileUID());
            times = 1;
            result = new Exception();
        }};

        faultServiceConsumer.onMessage(txtMessage);

        new FullVerifications() {{
            ETrustExAdapterDTO dto;
            nodeConnectorSubmissionService.submitToDomibus(dto = withCapture());
            times = 1;

            assertThat(dto.getAs4Action(), is(FaultDTO.FAULT_ACTION));
            domainNodePluginService.setNodePluginDomain();
            times = 1;
            clearEtxLogKeysService.clearKeys();
            times = 1;
        }};
    }

    @Test
    public void onMessage_withFault(final @Mocked TextMessage txtMessage, final @Mocked TextMessageUtil textMessageUtil) {
        final ETrustExAdapterDTO eTrustExAdapterDTO = ETrustExAdapterDTOBuilder
                .getInstance()
                .withAction(Action.SUBMIT_APPLICATION_RESPONSE_REQUEST.getCode())
                .withService(Service.APPLICATION_RESPONSE_SERVICE.getCode())
                .build();

        final PayloadDTO payloadFault = new FaultDTO(ETrustExError.DEFAULT, new Exception("First Exception", new Exception("Second Exception")));
        eTrustExAdapterDTO.addAs4Payload(payloadFault);

        new Expectations() {{
            TextMessageUtil.getETrustExAdapterDTO(txtMessage);
            times = 1;
            result = eTrustExAdapterDTO;
        }};

        faultServiceConsumer.onMessage(txtMessage);

        new FullVerifications() {{
            ETrustExAdapterDTO dto;
            nodeConnectorSubmissionService.submitToDomibus(dto = withCapture());
            times = 1;

            assertThat(dto.getAs4Action(), is(Action.FAULT_ACTION.code));
            assertThat(dto.getPayloadFromCID(ContentId.CONTENT_FAULT).getDescription(), is(PayloadDescription.ETRUSTEX_FAULT_PAYLOAD.getValue()));
            domainNodePluginService.setNodePluginDomain();
            times = 1;
            clearEtxLogKeysService.clearKeys();
            times = 1;
        }};
    }

    @Test
    public void onMessage_withFault2(final @Mocked TextMessage txtMessage, final @Mocked TextMessageUtil textMessageUtil) {
        final ETrustExAdapterDTO eTrustExAdapterDTO = ETrustExAdapterDTOBuilder
                .getInstance()
                .withAction(Action.SUBMIT_APPLICATION_RESPONSE_REQUEST.getCode())
                .withService(Service.APPLICATION_RESPONSE_SERVICE.getCode())
                .build();

        final PayloadDTO payloadFault = new FaultDTO(ETrustExError.DEFAULT, new Exception("First Exception"));
        eTrustExAdapterDTO.addAs4Payload(payloadFault);

        new Expectations() {{
            TextMessageUtil.getETrustExAdapterDTO(txtMessage);
            times = 1;
            result = eTrustExAdapterDTO;
        }};

        faultServiceConsumer.onMessage(txtMessage);

        new FullVerifications() {{
            ETrustExAdapterDTO dto;
            nodeConnectorSubmissionService.submitToDomibus(dto = withCapture());
            times = 1;

            assertThat(dto.getAs4Action(), is(Action.FAULT_ACTION.code));
            assertThat(dto.getPayloadFromCID(ContentId.CONTENT_FAULT).getDescription(), is(PayloadDescription.ETRUSTEX_FAULT_PAYLOAD.getValue()));
            domainNodePluginService.setNodePluginDomain();
            times = 1;
            clearEtxLogKeysService.clearKeys();
            times = 1;
        }};
    }

    private LargePayloadDTO getPayloadDTO() throws IOException {
        DataHandler dataHandler =
                new DataHandler(
                        new ByteArrayDataSource(
                                readResource("data/outbox/document/LargePayloadDTO.xml"),
                                "text/xml"));
        return new LargePayloadDTO(
                ContentId.CONTENT_ID_DOCUMENT.getValue(),
                AS4MessageConstants.MIME_TYPE_APP_OCTET,
                dataHandler, "JUNIT TEST",
                Locale.ENGLISH,
                "certificate.pdf");
    }
}
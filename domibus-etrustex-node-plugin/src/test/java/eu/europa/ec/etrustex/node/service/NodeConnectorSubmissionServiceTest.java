package eu.europa.ec.etrustex.node.service;

import eu.domibus.ext.services.AuthenticationExtService;
import eu.europa.ec.etrustex.common.exceptions.DomibusSubmissionException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.node.DomibusETrustExNodeConnector;
import eu.europa.ec.etrustex.node.application.ETrustExNodePluginProperties;
import eu.europa.ec.etrustex.node.domain.DomainNodePluginService;
import mockit.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

import static org.junit.Assert.fail;


public class NodeConnectorSubmissionServiceTest {

    @Injectable
    private AuthenticationExtService authenticationExtService;

    @Injectable
    private DomibusETrustExNodeConnector domibusETrustExNodeConnector;
    @Injectable
    private String user;
    @Injectable
    private String pwd;
    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Injectable
    private DomainNodePluginService domainNodePluginService;
    @Injectable
    private ETrustExNodePluginProperties eTrustExNodePluginProperties;

    @Tested
    private NodeConnectorSubmissionService nodeConnectorSubmissionService;

    @Before
    public void setUp() throws Exception {
        user = "user";
        pwd = "pwd";
    }

    @Test
    public void submit_nullAuth(@Mocked final SecurityContextHolder securityContextHolder) throws Exception {
        final ETrustExAdapterDTO dto = new ETrustExAdapterDTO();

        final SecurityContextImpl securityContext = new SecurityContextImpl();
        new Expectations() {{
            SecurityContextHolder.getContext();
            result = securityContext;
            times = 1;

            securityContext.getAuthentication();
            result = null;
            times = 1;

            domibusETrustExNodeConnector.submit(dto);
            result = null;
            times = 1;

            eTrustExNodePluginProperties.getUser();
            result = "user";
            times = 1;
            eTrustExNodePluginProperties.getPwd();
            result = "pwd";
            times = 1;
        }};

        nodeConnectorSubmissionService.submitToDomibus(dto);

        new FullVerifications() {{
            authenticationExtService.basicAuthenticate("user", "pwd");
            times = 1;
            domainNodePluginService.setNodePluginDomain();
            times = 1;
            clearEtxLogKeysService.clearKeys();
            times = 1;
        }};
    }

    @Test
    public void submit_notAuth(@Mocked final SecurityContextHolder securityContextHolder, @Mocked final SecurityContext securityContext, @Mocked final org.springframework.security.core.Authentication authentication) throws Exception {
        final ETrustExAdapterDTO dto = new ETrustExAdapterDTO();

        new Expectations() {{
            SecurityContextHolder.getContext();
            result = securityContext;
            times = 2;

            securityContext.getAuthentication();
            result = authentication;
            times = 2;

            authentication.isAuthenticated();
            result = false;
            times = 1;

            domibusETrustExNodeConnector.submit(dto);
            result = null;
            times = 1;

            eTrustExNodePluginProperties.getUser();
            result = "user";
            times = 1;
            eTrustExNodePluginProperties.getPwd();
            result = "pwd";
            times = 1;
        }};

        nodeConnectorSubmissionService.submitToDomibus(dto);

        new FullVerifications() {{
            authenticationExtService.basicAuthenticate("user", "pwd");
            times = 1;
            domainNodePluginService.setNodePluginDomain();
            clearEtxLogKeysService.clearKeys();
        }};
    }

    @Test
    public void submit_Auth(@Mocked final SecurityContextHolder securityContextHolder, @Mocked final SecurityContext securityContext, @Mocked final org.springframework.security.core.Authentication authentication) throws Exception {
        final ETrustExAdapterDTO dto = new ETrustExAdapterDTO();

        new Expectations() {{
            SecurityContextHolder.getContext();
            result = securityContext;
            times = 2;

            securityContext.getAuthentication();
            result = authentication;
            times = 2;

            authentication.isAuthenticated();
            result = true;
            times = 1;

            domibusETrustExNodeConnector.submit(dto);
            result = null;
            times = 1;
        }};

        nodeConnectorSubmissionService.submitToDomibus(dto);

        new FullVerifications() {{
            domainNodePluginService.setNodePluginDomain();
            clearEtxLogKeysService.clearKeys();
        }};
    }

    @Test
    public void submit_Error(@Mocked final SecurityContextHolder securityContextHolder, @Mocked final SecurityContext securityContext, @Mocked final org.springframework.security.core.Authentication authentication) throws Exception {
        final ETrustExAdapterDTO dto = new ETrustExAdapterDTO();

        new Expectations() {{
            SecurityContextHolder.getContext();
            result = securityContext;
            times = 2;

            securityContext.getAuthentication();
            result = authentication;
            times = 2;

            authentication.isAuthenticated();
            result = true;
            times = 1;

            domibusETrustExNodeConnector.submit(dto);
            result = new IllegalArgumentException("ERROR");
            times = 1;
        }};

        try {
            nodeConnectorSubmissionService.submitToDomibus(dto);
            fail();
        } catch (DomibusSubmissionException e) {
            //OK
        }

        new FullVerifications() {{
            domainNodePluginService.setNodePluginDomain();
            clearEtxLogKeysService.clearKeys();
        }};
    }

}
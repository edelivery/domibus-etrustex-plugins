package eu.europa.ec.etrustex.node.testutils.h2;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author François Gautier
 * @version 1.0
 * @since 02/02/2018
 */
@Service("etxNodePluginStatementExecutor")
public class StatementExecutor {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(StatementExecutor.class);

    @Autowired
    private DataSource dataSource;

    public void updateEtxUser(final Long usrId, String userName) throws SQLException {
        LOG.info("+++++ Update " + usrId + " with value " + userName);
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            statement = connection.prepareStatement(
                    "UPDATE ETX_USER " +
                            "SET USR_NAME = ? " +
                            "WHERE USR_ID = ?");
            statement.setString(1, userName);
            statement.setLong(2, usrId);
            statement.executeUpdate();
            connection.commit();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }
}

package eu.europa.ec.etrustex.node.inbox.client.adapter;

import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import eu.europa.ec.etrustex.node.webservice.client.NodeWebServiceProvider;
import eu.europa.ec.etrustex.node.webservice.client.WSClient;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.xml.ws.Holder;

/**
 * JUnit for {@link NodeServicesAdapterBase}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 14/12/2017
 */
@RunWith(JMockit.class)
public class NodeServicesAdapterBaseTest {

    private final String senderParty = "TEST_EGREFFE_APP_PARTY_B4";
    private final String receiverParty = "TEST_EGREFFE_APP_PARTY-2_B4";

    @Injectable
    private NodeWebServiceProvider webServiceProvider;

    @Tested
    private NodeServicesAdapterBase nodeServicesAdapterBase;

    @Before
    public void setUp() throws Exception {
        //stubbing post-construct method
        new MockUp<WSClient>() {
            @Mock
            void validateEndPoint() {
            }
        };
    }

    @Test
    public void fillAuthorisationHeader() throws Exception {

        //tested method
        Holder<HeaderType> headerTypeHolder = nodeServicesAdapterBase.fillAuthorisationHeader(senderParty, receiverParty);

        verifyHeader(senderParty, receiverParty, headerTypeHolder);

        new FullVerifications() {{
        }};

    }


    @Test
    public void fillAuthorisationHeaderWithStatusScope() throws Exception {

        //tested method
        Holder<HeaderType> headerTypeHolder = nodeServicesAdapterBase.fillAuthorisationHeaderWithStatusScope(senderParty, receiverParty);

        verifyHeader(senderParty, receiverParty, headerTypeHolder);

        HeaderType headerType = headerTypeHolder.value;
        Assert.assertNotNull(headerType.getBusinessHeader().getBusinessScope());
        Assert.assertTrue(CollectionUtils.isNotEmpty(headerType.getBusinessHeader().getBusinessScope().getScope()));
        Assert.assertEquals("status scope must match",
                NodeServicesAdapterBase.STATUS_SCOPE_IDENTIFIER,
                headerType.getBusinessHeader().getBusinessScope().getScope().get(0).getType());

        new FullVerifications() {{
        }};

    }


    private void verifyHeader(String senderParty, String receiverParty, Holder<HeaderType> headerTypeHolder) {
        Assert.assertNotNull(headerTypeHolder);
        Assert.assertNotNull(headerTypeHolder.value);
        HeaderType headerType = headerTypeHolder.value;
        Assert.assertNotNull(headerType.getBusinessHeader());
        Assert.assertTrue(CollectionUtils.isNotEmpty(headerType.getBusinessHeader().getSender()));
        Assert.assertEquals("sender party must match", senderParty, headerType.getBusinessHeader().getSender().get(0).getIdentifier().getValue());

        Assert.assertTrue(CollectionUtils.isNotEmpty(headerType.getBusinessHeader().getReceiver()));
        Assert.assertEquals("receiver party must match", receiverParty, headerType.getBusinessHeader().getReceiver().get(0).getIdentifier().getValue());

        Assert.assertNotNull(headerType.getTechnicalHeader());
        Assert.assertNotNull(headerType.getTechnicalHeader().getSignatureInformation());
    }

}
package eu.europa.ec.etrustex.node.inbox.webservice;

import ec.schema.xsd.commonaggregatecomponents_2.BusinessHeaderType;
import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import eu.europa.ec.etrustex.common.converters.node.NodePartnerConverter;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.service.security.IdentityManager;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.xml.ws.Holder;

/**
 * Junit for {@link NodeWebserviceBase}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 27/11/2017
 */
@RunWith(JMockit.class)
public class NodeWebserviceBaseTest {

    @Tested
    private NodeWebserviceBase nodeWebserviceBase;

    @Injectable
    private IdentityManager identityManager;

    @Test
    public void testBuildHeaderResponse_HappyFlow() {

        Holder<HeaderType> headerTypeHolderRequest = new Holder<>();
        HeaderType headerTypeRequest = new HeaderType();
        BusinessHeaderType businessHeaderTypeRequest = new BusinessHeaderType();
        headerTypeRequest.setBusinessHeader(businessHeaderTypeRequest);
        final String senderPartyName = "TEST sender";
        final String receiverPartyName = "TEST receiver";
        businessHeaderTypeRequest.getSender().add(NodePartnerConverter.buildPartnerType(senderPartyName));
        businessHeaderTypeRequest.getReceiver().add(NodePartnerConverter.buildPartnerType(receiverPartyName));

        headerTypeHolderRequest.value = headerTypeRequest;

        //tested method
        HeaderType headerTypeResponse = nodeWebserviceBase.buildHeaderResponse(headerTypeHolderRequest);
        Assert.assertNotNull(headerTypeResponse);
        Assert.assertNotNull(headerTypeResponse.getBusinessHeader());
        Assert.assertEquals("sender list should match", businessHeaderTypeRequest.getSender(), headerTypeResponse.getBusinessHeader().getSender());
        Assert.assertEquals("receiver list should match", businessHeaderTypeRequest.getReceiver(), headerTypeResponse.getBusinessHeader().getReceiver());

    }

    @Test
    public void testExtractValidateParty_HappyFlow() {
        BusinessHeaderType businessHeaderTypeRequest = new BusinessHeaderType();
        final String senderPartyUuid = "TEST sender";
        businessHeaderTypeRequest.getSender().add(NodePartnerConverter.buildPartnerType(senderPartyUuid));


        final EtxParty senderParty = new EtxParty();
        senderParty.setPartyUuid(senderPartyUuid);

        new Expectations() {{
            identityManager.searchPartyByUuid(senderPartyUuid);
            times = 1;
            result = senderParty;
        }};

        //tested method
        EtxParty etxParty = nodeWebserviceBase.extractValidateParty(businessHeaderTypeRequest.getSender());
        Assert.assertNotNull("result shouldn't be null", etxParty);

        new Verifications() {{
            String senderPartyUuidActual;
            identityManager.searchPartyByUuid(senderPartyUuidActual = withCapture());
            times = 1;
            Assert.assertEquals("sender party uuid should match", senderPartyUuid, senderPartyUuidActual);

        }};
    }


    @Test
    public void testExtractValidateParty_InvalidPartyUuid() {

        BusinessHeaderType businessHeaderTypeRequest = new BusinessHeaderType();
        final String senderPartyUuid = " ";
        businessHeaderTypeRequest.getSender().add(NodePartnerConverter.buildPartnerType(senderPartyUuid));

        new Expectations() {{
            identityManager.searchPartyByUuid(senderPartyUuid);
            times = 1;
            result = null;

        }};

        //tested method
        EtxParty etxParty = nodeWebserviceBase.extractValidateParty(businessHeaderTypeRequest.getSender());
        Assert.assertNull("etxParty should be null", etxParty);

    }

    @Test
    public void testExtractValidateParty_InvalidBusinessHeader() {

        //no sender or receiver in header
        BusinessHeaderType businessHeaderTypeRequest = new BusinessHeaderType();

        new Expectations(){{
            identityManager.searchPartyByUuid(null);
            times = 1;
            result = null;
        }};
        //tested method
        EtxParty etxParty = nodeWebserviceBase.extractValidateParty(businessHeaderTypeRequest.getSender());

        //tested method
        Assert.assertNull("etxParty should be null", etxParty);

        new FullVerifications() {{
        }};
    }

    @Test
    public void testBuildNow_HappyFlow() {

        //tested method
        DateTime dateTimeActual = nodeWebserviceBase.buildIssueDate();

        //ignoring milliseconds :(
        Assert.assertEquals("issue date should match", DateTime.now().millisOfSecond ().setCopy("0"), dateTimeActual.millisOfSecond ().setCopy("0"));
    }
}
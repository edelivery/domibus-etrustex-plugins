package eu.europa.ec.etrustex.node.inbox.queue.messagetransmission;

import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseRequest;
import eu.domibus.common.ErrorCode;
import eu.domibus.messaging.MessagingProcessingException;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ETrustExPluginAS4Properties;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import eu.europa.ec.etrustex.node.data.model.administration.EtxParty;
import eu.europa.ec.etrustex.node.data.model.message.EtxMessage;
import eu.europa.ec.etrustex.node.data.model.message.MessageDirection;
import eu.europa.ec.etrustex.node.data.model.message.MessageState;
import eu.europa.ec.etrustex.node.data.model.message.MessageType;
import eu.europa.ec.etrustex.node.inbox.queue.error.InboxErrorProducer;
import eu.europa.ec.etrustex.node.service.MessageServicePlugin;
import eu.europa.ec.etrustex.node.testutils.ResourcesUtils;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.UUID;

import static eu.europa.ec.etrustex.common.util.TransformerUtils.getObjectFromXml;
import static org.junit.Assert.fail;

@RunWith(JMockit.class)
public class InboxMessageTransmissionHandlerBaseTest {

    @Injectable
    private ETrustExPluginAS4Properties etxPluginAS4Properties;

    @Injectable
    private MessageServicePlugin messageService;

    @Injectable
    private InboxErrorProducer inboxErrorProducer;

    @Injectable
    private InboxMessageTransmissionProducer inboxMessageTransmissionProducer;

    @Tested
    private InboxMessageTransmissionHandlerBase inboxMessageTransmissionHandlerBase;

    private final String domibusMessageId = UUID.randomUUID().toString();
    private final Long messageId = RandomUtils.nextLong(0, 99999);
    private final String receiverPartyUuid = "TEST_EGREFFE_NP_PARTY_B4";
    private final String senderPartyUuid = "TEST_EGREFFE_APP_PARTY_B4";


    @Test
    public void testHandleError_DomibusException(final @Mocked InboxMessageTransmissionQueueMessage queueMessage) {

        final String errorDescription = "action must not be empty";
        final Exception ex = new Exception(new MessagingProcessingException(new IllegalArgumentException(errorDescription)));

        //tested method
        inboxMessageTransmissionHandlerBase.handleError(queueMessage, ex);

        new FullVerifications() {{

            String actualValue;
            queueMessage.setErrorCode(actualValue = withCapture());
            times = 1;
            Assert.assertNotNull(actualValue);
            Assert.assertEquals("error code must match", ErrorCode.EBMS_0004.getErrorCodeName(), actualValue);

            queueMessage.setErrorDescription(actualValue = withCapture());
            times = 1;
            Assert.assertNotNull(actualValue);
            Assert.assertEquals("error description must match", "java.lang.IllegalArgumentException: " + errorDescription, actualValue);

            inboxErrorProducer.triggerError(queueMessage);
            times = 1;
        }};
    }

    @Test
    public void testHandleError_ETrustExPluginException(final @Mocked InboxMessageTransmissionQueueMessage queueMessage) {

        final String errorDescription = "action must not be empty";
        final Exception ex = new ETrustExPluginException(ETrustExError.DEFAULT, errorDescription);

        new Expectations() {{
            queueMessage.getErrorCode();
            times = 1;
            result = StringUtils.EMPTY;

            queueMessage.getErrorDescription();
            times = 1;
            result = StringUtils.EMPTY;
        }};

        //tested method
        inboxMessageTransmissionHandlerBase.handleError(queueMessage, ex);

        new FullVerifications() {{

            String actualValue;
            queueMessage.setErrorCode(actualValue = withCapture());
            times = 1;
            Assert.assertNotNull(actualValue);
            Assert.assertEquals("error code must match", ETrustExError.DEFAULT.getCode(), actualValue);

            queueMessage.setErrorDescription(actualValue = withCapture());
            times = 1;
            Assert.assertNotNull(actualValue);
            Assert.assertEquals("error description must match", errorDescription, actualValue);

            inboxMessageTransmissionProducer.triggerMessageWithError(queueMessage);
            times = 1;

        }};
    }

    @Test
    public void testLoadValidateEtxMessageForTransmission_HappyFlow() {
        final EtxMessage etxMessage = getEtxMessage(domibusMessageId);

        new Expectations() {{

            messageService.findMessageById(messageId);
            times = 1;
            result = etxMessage;

        }};

        //tested method
        inboxMessageTransmissionHandlerBase.loadValidateEtxMessageForTransmission(messageId);

        new FullVerifications() {{
        }};
    }

    @Test
    public void testLoadValidateEtxMessageForTransmission_NullEtxMessage() {
        new Expectations() {{
            messageService.findMessageById(messageId);
            times = 1;
            result = null;
        }};

        try {
            //tested method
            inboxMessageTransmissionHandlerBase.loadValidateEtxMessageForTransmission(messageId);
            fail("ETrustExPluginException expected");
        } catch (ETrustExPluginException e) {
            Assert.assertEquals("error type should match", ETrustExError.DEFAULT, e.getError());
            Assert.assertEquals("error message should match",
                    "Message with id[" + messageId + "] specified for Inbox Message transmission to backend is not known.",
                    e.getMessage());
        }
        new FullVerifications() {{
        }};
    }

    @Test
    public void testLoadValidateEtxMessageForTransmission_MessageStateFailed() {
        final EtxMessage etxMessage = getEtxMessage(domibusMessageId);
        etxMessage.setMessageState(MessageState.FAILED);

        new Expectations() {{
            messageService.findMessageById(messageId);
            times = 1;
            result = etxMessage;
        }};

        try {
            //tested method
            inboxMessageTransmissionHandlerBase.loadValidateEtxMessageForTransmission(messageId);
            fail("ETrustExPluginException expected");
        } catch (ETrustExPluginException e) {
            Assert.assertEquals("error type should match", ETrustExError.ETX_009, e.getError());
            Assert.assertEquals("error message should match",
                    "Message with id[" + messageId + "] intended for Inbox Message transmission is already in FAILED state.",
                    e.getMessage());
        }
        new FullVerifications() {{
        }};
    }

    @Test
    public void testLoadValidateEtxMessageForTransmission_MessageWrongDirection() {
        final EtxMessage etxMessage = getEtxMessage(domibusMessageId);
        etxMessage.setDirectionType(MessageDirection.BACKEND_TO_NODE);

        new Expectations() {{
            messageService.findMessageById(messageId);
            times = 1;
            result = etxMessage;
        }};

        try {
            //tested method
            inboxMessageTransmissionHandlerBase.loadValidateEtxMessageForTransmission(messageId);
            fail("ETrustExPluginException expected");
        } catch (ETrustExPluginException e) {
            Assert.assertEquals("error type should match", ETrustExError.DEFAULT, e.getError());
            Assert.assertEquals("error message should match",
                    "Message with id[" + messageId + "] specified for Inbox Message transmission to backend is not intended for " + MessageDirection.NODE_TO_BACKEND,
                    e.getMessage());
        }
        new FullVerifications() {{
        }};
    }

    @Test
    public void testHandleDomibusSubmissionSuccess_HappyFlow(final @Mocked EtxMessage etxMessage) {

        etxMessage.setDomibusMessageId(StringUtils.EMPTY);

        new Expectations() {{
            etxMessage.getDomibusMessageId();
            times = 1;
            result = StringUtils.EMPTY;

        }};

        //tested method
        inboxMessageTransmissionHandlerBase.handleDomibusSubmissionSuccess(etxMessage, domibusMessageId);

        new Verifications() {{
            String domibusMsgIdActual;
            etxMessage.setDomibusMessageId(domibusMsgIdActual = withCapture());
            times = 1;
            Assert.assertEquals("domibus message id should match", domibusMessageId, domibusMsgIdActual);

            MessageState messageStateActual;
            etxMessage.setMessageState(messageStateActual = withCapture());
            Assert.assertEquals("message state should match", MessageState.SUBMITTED_TO_DOMIBUS, messageStateActual);
        }};
    }

    @Test
    public void testHandleDomibusSubmissionSuccess_MessageNull() {

        final EtxMessage etxMessage = null;

        try {
            //tested method
            inboxMessageTransmissionHandlerBase.handleDomibusSubmissionSuccess(etxMessage, domibusMessageId);
            fail("ETrustExPluginException expected");
        } catch (ETrustExPluginException ex) {
            Assert.assertEquals("error code should match", ETrustExError.DEFAULT , ex.getError());
            Assert.assertEquals("error message should match", "EtxMessage object should not be null on successful submission to Domibus!" , ex.getMessage());
        }

        new FullVerifications() {{}};
    }

    @Test
    public void testCreateEtxAdapterDTOForInboxTransmission_HappyFlow(final @Mocked ETrustExAdapterDTO dto) {

        new Expectations() {{
            new ETrustExAdapterDTO();
            times = 1;
            result = dto;
        }};

        //tested method
        inboxMessageTransmissionHandlerBase.createEtxAdapterDTOForInboxTransmission(getEtxMessage(domibusMessageId));

        new Verifications() {{
            String actualValue;
            dto.setAs4FromPartyId(actualValue = withCapture());
            times = 1;
            Assert.assertEquals("As4FromPartyId must match", etxPluginAS4Properties.getAs4ETrustExNodePartyId(), actualValue);

            dto.setAs4FromPartyIdType(actualValue = withCapture());
            times = 1;
            Assert.assertEquals("As4FromPartyIdType must match", etxPluginAS4Properties.getAs4FromPartyIdType(), actualValue);

            dto.setAs4FromPartyRole(actualValue = withCapture());
            times = 1;
            Assert.assertEquals("As4FromPartyRole must match", etxPluginAS4Properties.getAs4FromPartyRole(), actualValue);

            dto.setAs4ToPartyId(actualValue = withCapture());
            times = 1;
            Assert.assertEquals("As4ToPartyId must match", receiverPartyUuid, actualValue);

            dto.setAs4ToPartyIdType(actualValue = withCapture());
            times = 1;
            Assert.assertEquals("As4ToPartyIdType must match", etxPluginAS4Properties.getAs4ToPartyIdType(), actualValue);

            dto.setAs4ToPartyRole(actualValue = withCapture());
            times = 1;
            Assert.assertEquals("As4FromPartyRole must match", etxPluginAS4Properties.getAs4ToPartyRole(), actualValue);

            dto.setAs4OriginalSender(actualValue = withCapture());
            times = 1;
            Assert.assertEquals("As4OriginalSender must match", senderPartyUuid, actualValue);

            dto.setAs4FinalRecipient(actualValue = withCapture());
            times = 1;
            Assert.assertEquals("As4FinalRecipient must match", receiverPartyUuid, actualValue);

        }};

    }

    private EtxMessage getEtxMessage(final String domibusMessageId) {
        SubmitApplicationResponseRequest submitApplicationResponseRequest = getObjectFromXml(ResourcesUtils.readResource("data/inbox/statusmessage/SubmitApplicationResponseRequest.xml")
        );
        final EtxMessage etxMessage = new EtxMessage();
        etxMessage.setDomibusMessageId(domibusMessageId);
        etxMessage.setMessageType(MessageType.MESSAGE_STATUS);
        etxMessage.setMessageState(MessageState.SUBMITTED_TO_DOMIBUS);
        etxMessage.setDirectionType(MessageDirection.NODE_TO_BACKEND);
        final EtxParty receiverParty = new EtxParty();
        receiverParty.setPartyUuid(receiverPartyUuid);
        etxMessage.setReceiver(receiverParty);
        final EtxParty senderParty = new EtxParty();
        senderParty.setPartyUuid(senderPartyUuid);
        etxMessage.setSender(senderParty);

        etxMessage.setXml(TransformerUtils.serializeObjToXML(submitApplicationResponseRequest));
        return etxMessage;
    }

}
package eu.europa.ec.etrustex.node.inbox;

import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.handlers.Action;
import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxBundleTransmissionNodeHandler;
import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxStatusMessageTransmissionNodeHandler;
import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxWrapperTransmissionNodeHandler;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static eu.europa.ec.etrustex.common.handlers.Action.*;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 08-Dec-17
 */
@RunWith(Parameterized.class)
public class FollowUpOperationHandlerFactoryTest {

    @Injectable
    private InboxStatusMessageTransmissionNodeHandler inboxStatusMessageTransmissionNodeHandler;
    @Injectable
    private InboxBundleTransmissionNodeHandler inboxBundleTransmissionNodeHandler;
    @Injectable
    private InboxWrapperTransmissionNodeHandler wrapperTransmissionHandler;

    @Tested
    private FollowUpOperationHandlerFactory followUpOperationHandlerFactory;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Parameter
    public Action action;
    @Parameter(1)
    public Class<? extends FollowUpOperationHandler> clazz;
    @Parameter(2)
    public Class<? extends Exception> expectedException;

    @Parameters( name = "{index}: Action({0})" )
    public static Collection combinations() {
        return Arrays.asList(new Object[][]{
                // Happy flow scenario
                {STATUS_MESSAGE_TRANSMISSION, InboxStatusMessageTransmissionNodeHandler.class, null},
                {NOTIFY_BUNDLE_TO_BACKEND_REQUEST, InboxBundleTransmissionNodeHandler.class, null},
                {TRANSMIT_WRAPPER_TO_BACKEND, InboxWrapperTransmissionNodeHandler.class, null},
                // returns null
                {UNKNOWN, null, ETrustExPluginException.class},

                //exception
                {null, null, ETrustExPluginException.class},
                {SUBMIT_APPLICATION_RESPONSE_RESPONSE, FollowUpOperationHandler.class, null},
                {STORE_DOCUMENT_WRAPPER_RESPONSE, FollowUpOperationHandler.class, null},
                {SUBMIT_DOCUMENT_BUNDLE_RESPONSE, FollowUpOperationHandler.class, null},
                {RETRIEVE_ICA_RESPONSE, FollowUpOperationHandler.class, null},
        });
    }

    @Test
    public void getFollowUp() throws Exception {
        if (expectedException != null) {
            thrown.expect(expectedException);
        }

        FollowUpOperationHandler operationHandler = followUpOperationHandlerFactory.getOperationHandler(action);

        new FullVerifications() {{
        }};

        if ( clazz != null) {
            assertThat(operationHandler, notNullValue());
        } else {
            assertThat(operationHandler, nullValue());
        }
    }
}
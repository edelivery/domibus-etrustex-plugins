package eu.europa.ec.etrustex.node.inbox.queue.error;

import eu.europa.ec.etrustex.node.inbox.queue.messagetransmission.InboxMessageTransmissionQueueMessage;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.Queue;

/**
 * @author François Gautier
 * @version 1.0
 * @since 14-Nov-17
 */
@RunWith(JMockit.class)
public class InboxErrorProducerTest {

    @Injectable
    private Queue etxNodePluginInboxErrorQueue;

    @Injectable
    private JmsTemplate etxNodePluginInboxErrorQueueJmsTemplate;

    @Tested
    private InboxErrorProducer inboxErrorProducer;

    @Test
    public void produceError() throws Exception {
        final InboxMessageTransmissionQueueMessage queueMessage = new InboxMessageTransmissionQueueMessage();
        new Expectations() {{
            etxNodePluginInboxErrorQueueJmsTemplate.convertAndSend(etxNodePluginInboxErrorQueue, queueMessage);
        }};

        inboxErrorProducer.triggerError(queueMessage);

        new FullVerifications() {{
        }};
    }
}
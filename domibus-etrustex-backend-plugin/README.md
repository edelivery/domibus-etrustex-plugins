# README

## Release
### Application servers
#### WebLogic

#### Wildfly

    standalone.bat --server-config=standalone-full.xml -Djboss.http.port=7001
or

    domain.bat -Djboss.http.port=7001 -Djboss.management.http.port=7601 --host-config=host.xml

### Databases

#### Oracle

#####Liquibase generation

In order to generate the full SQL statements of a release, use the profile 'liquibase' and run 
   
    mvn compile -Pliquibase

This profile is only meant for the release process. DO NOT USE in the bamboo build.
In the 'liquibase' profile of pom.xml, 2 new executions should be added (1 for oracle and 1 for mysql) with the new 'changelog' file entry for liquibase generation.
The file should be created in the folder 
    
    src/main/conf/oracle

package eu.europa.ec.etrustex.adapter.integration.client.backend.ws.adapter;

import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;

/**
 * @author: micleva
 * @date: 6/26/12 2:01 PM
 * @project: ETX
 *
 * @author Arun Raj
 * @version 1.0
 */
public interface InboxNotificationServiceAdapter {

    void notifyMessageBundle(EtxMessage messageBundle);

    void notifyMessageStatus(EtxMessage messageStatus, MessageType refMessageType);
}

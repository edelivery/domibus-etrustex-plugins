package eu.europa.ec.etrustex.adapter.web.retriggeroutgoingbundle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RetriggerBundleResponse implements Serializable {

    private static final long serialVersionUID = 5321467071350794584L;
    private List<String> retriggerBundleSuccessList;
    private List<String> otherImpactedBundlesList;
    private List<String> retriggerBundleExceptionList;

    public RetriggerBundleResponse() {
        retriggerBundleSuccessList = new ArrayList<>();
        otherImpactedBundlesList = new ArrayList<>();
        retriggerBundleExceptionList = new ArrayList<>();
    }

    public List<String> getRetriggerBundleSuccessList() {
        return retriggerBundleSuccessList;
    }

    public void setRetriggerBundleSuccessList(List<String> retriggerBundleSuccessList) {
        this.retriggerBundleSuccessList = retriggerBundleSuccessList;
    }

    public void addRetriggerBundleSuccessList(String retriggerBundleSuccessMsg) {
        if(retriggerBundleSuccessList ==null){
            retriggerBundleSuccessList = new ArrayList<>();
        }
        this.retriggerBundleSuccessList.add(retriggerBundleSuccessMsg);
    }

    public List<String> getOtherImpactedBundlesList() {
        return otherImpactedBundlesList;
    }

    public void setOtherImpactedBundlesList(List<String> otherImpactedBundlesList) {
        this.otherImpactedBundlesList = otherImpactedBundlesList;
    }

    public void addOtherImpactedBundlesList(String otherImpactedBundleMsg) {
        if(otherImpactedBundlesList == null){
            otherImpactedBundlesList = new ArrayList<>();
        }
        this.otherImpactedBundlesList.add(otherImpactedBundleMsg);
    }

    public List<String> getRetriggerBundleExceptionList() {
        return retriggerBundleExceptionList;
    }

    public void setRetriggerBundleExceptionList(List<String> retriggerBundleExceptionList) {
        this.retriggerBundleExceptionList = retriggerBundleExceptionList;
    }

    public void addRetriggerBundleExceptionList(Exception e) {
        if(retriggerBundleExceptionList == null){
            retriggerBundleExceptionList = new ArrayList<>();
        }
        this.retriggerBundleExceptionList.add(e.getMessage());
    }

    public String buildResponse() {
        StringBuilder responseBuilder = new StringBuilder();
        retriggerBundleSuccessList.stream().forEach(successmessage -> responseBuilder.append(successmessage));
        retriggerBundleExceptionList.stream().forEach(exceptionMsg -> responseBuilder.append(exceptionMsg));
        return responseBuilder.toString();
    }
}

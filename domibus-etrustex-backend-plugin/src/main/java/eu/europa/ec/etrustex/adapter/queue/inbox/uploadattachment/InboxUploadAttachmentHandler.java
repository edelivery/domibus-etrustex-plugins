package eu.europa.ec.etrustex.adapter.queue.inbox.uploadattachment;

/**
 * @author Arun Raj
 * @version 1.0
 * @since Oct 2017
 */
public interface InboxUploadAttachmentHandler {

    /**
     * Validates the {@link eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment} instance.
     * Main validations are:
     * <ol>
     *     <li>Attachment id is known</li>
     *     <li>{@link eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment} is in state {@link eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState#ATTS_DOWNLOADED}</li>
     *     <li>{@link eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage} is in state {@link eu.europa.ec.etrustex.adapter.model.entity.message.MessageState#MS_CREATED}</li>
     * </ol>
     *
     * @param attachmentId  ID of ETX_ADT_ATTACHMENT
     */
    void validateUploadAttachment(Long attachmentId);

    /**
     * <p>To upload Attachment to the Backend.</p>
     * Find the instances of {@link eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment},
     * corresponding receiver party {@link eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty} and
     * receiver party system {@link eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem}.
     * Based on the system configuration in {@link eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystemConfig}
     * upload the attachment to the {@link eu.europa.ec.etrustex.adapter.integration.client.backend.repository.FileRepository}
     *
     *
     * @param attachmentId ID of ETX_ADT_ATTACHMENT
     */
    void handleUploadAttachment(Long attachmentId);

    /**
     * To handle errors which may occur during upload of attachment to backend.
     *
     * @param attachmentId
     */
    void onError(Long attachmentId);
}

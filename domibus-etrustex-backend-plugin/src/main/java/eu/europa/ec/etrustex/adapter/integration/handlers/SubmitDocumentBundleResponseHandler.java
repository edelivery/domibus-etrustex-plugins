package eu.europa.ec.etrustex.adapter.integration.handlers;

import ec.services.wsdl.documentbundle_2.FaultResponse;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleResponse;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxErrorBuilder;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static eu.europa.ec.etrustex.adapter.integration.converters.FaultTypeConverter.getFaultInfo;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.extractETrustExXMLPayload;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.extractETrustExXMLPayloadFaultResponse;

/**
 * @author Arun Raj
 * @version 1.0
 * @since June 2017
 */
@Service("etxBackendPluginSubmitDocumentBundleResponseHandler")
public class SubmitDocumentBundleResponseHandler
        extends BackendResponseHandlerBase<SubmitDocumentBundleResponse, EtxMessage, FaultResponse>
        implements OperationHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SubmitDocumentBundleResponseHandler.class);

    @Autowired
    private MessageServicePlugin messageService;

    @Autowired
    private PostProcessProducer postProcessProducer;

    @Override
    public boolean handle(ETrustExAdapterDTO eTrustExAdapterResponseDTO) {

        /*Extract Payloads*/
        SubmitDocumentBundleResponse response = extractETrustExXMLPayload(ContentId.CONTENT_ID_GENERIC, eTrustExAdapterResponseDTO);
        FaultResponse faultResponse = extractETrustExXMLPayloadFaultResponse(eTrustExAdapterResponseDTO, new FaultResponse("Default FaultResponse", getFaultInfo()));
        if (response == null && faultResponse == null) {
            throw new ETrustExPluginException(ETrustExError.ETX_002, "Either SubmitDocumentBundleResponse or FaultResponse should be provided!");
        }

        EtxMessage etxMessage = messageService.findMessageByDomibusMessageId(eTrustExAdapterResponseDTO.getAs4RefToMessageId());

        processNodeResponse(faultResponse, response, etxMessage);

        LOG.info("Processing SubmitDocumentBundle response message sent by ETrustEx Node Plugin for Message ID: {}",
                etxMessage.getId());

        postProcessProducer.triggerPostProcess(etxMessage.getId());

        return true;
    }

    @Override
    protected void processNodeFault(FaultResponse faultResponse, EtxMessage msgEntity) {
        String errorDescription = faultResponse.getMessage();
        String faultResponseCode = getNodeFaultResponseCode(faultResponse.getFaultInfo());
        String errorLog = String.format("Sending of Bundle for message with ID %s  returns the fault response code: %s; error details: %s",
                msgEntity.getId(),
                faultResponseCode,
                errorDescription);
        LOG.error(errorLog);
        msgEntity.setError(EtxErrorBuilder.getInstance().withResponseCode(faultResponseCode).withDetail(errorLog).build());
        messageService.updateMessage(msgEntity);
    }

    @Override
    protected void processNodeSuccess(SubmitDocumentBundleResponse response, EtxMessage etxMessage) {
        boolean result = response.getAck().getAckIndicator().isValue();
        LOG.info("Received response from NODE for DocumentBundleService submitDocumentBundle response acknowledgement[{}]",
                result);
    }
}

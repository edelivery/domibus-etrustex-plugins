package eu.europa.ec.etrustex.adapter.domain;

import eu.europa.ec.etrustex.common.jobs.DomainInitializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DomainInitializerBackEnd implements DomainInitializer {

    @Autowired
    DomainBackEndPluginService domainBackEndPluginService;

    @Override
    @DomainBackEndPlugin
    public void init(){
        domainBackEndPluginService.setBackendPluginDomain();
    }
}

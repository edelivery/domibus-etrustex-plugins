package eu.europa.ec.etrustex.adapter.application;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.client.backend.repository.LocalSharedFileRepositoryService;
import eu.europa.ec.etrustex.adapter.model.entity.administration.AdapterConfigurationProperty;
import eu.europa.ec.etrustex.adapter.service.AdapterConfigurationService;
import eu.europa.ec.etrustex.common.util.FileUtils;
import eu.europa.ec.etrustex.common.util.attachment.FileRepositoryUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static eu.europa.ec.etrustex.adapter.model.entity.administration.AdapterConfigurationProperty.ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS;
import static eu.europa.ec.etrustex.common.util.FileUtils.deleteIfOld;

/**
 * Cleans internal file repository (specified by system configuration property 'etx.adapter.repository.root.path').
 * Cleaning is done by deleting old files and directories of 'bundle level'.
 * Old files are considered to be files with 'lastModified' value older than specified by system configuration
 * property 'etx.adapter.repository.ttl.days'.
 */

@DisallowConcurrentExecution
public class MaintenanceManagerJob extends BackEndQuartzJobBean {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(MaintenanceManagerJob.class);

    protected static final int IN_BUNDLE_LEVEL = 3;
    protected static final int OUT_BUNDLE_LEVEL = 2;

    @Autowired
    private AdapterConfigurationService adapterConfigurationService;

    @Autowired
    private LocalSharedFileRepositoryService fileRepository;

    @Override
    public void process(JobExecutionContext jobExecutionContext) {
        Map<AdapterConfigurationProperty, String> adapterConfig = adapterConfigurationService.getAdapterConfigurations();
        Path repositoryRoot = fileRepository.getRootDirectory();
        Long ttlInDays = getTtlInDays(adapterConfig);
        if (ttlInDays != null && ttlInDays > 0) {
            LOG.info("Repository cleanup started [TTL: {} day(s); path: {} {}]", ttlInDays,
                    repositoryRoot,
                    FileRepositoryUtil.getHumanReadableStorageInfo(repositoryRoot));
            long startTime = System.nanoTime();
            long ttlInMs = TimeUnit.MILLISECONDS.convert(ttlInDays, TimeUnit.DAYS);
            deleteOldBundlesInDirectory(repositoryRoot, ttlInMs);
            FileUtils.deleteOldFilesInDirectory(repositoryRoot, ttlInMs);
            long endTime = System.nanoTime();
            LOG.info("Repository cleanup finished [execution time (s): {} {}]", (endTime - startTime) * 0.000000001,
                    FileRepositoryUtil.getHumanReadableStorageInfo(repositoryRoot));

            LOG.info("Root File Repository content AFTER cleanup is: " + FileRepositoryUtil.getPathContent(repositoryRoot));
        } else {
            LOG.warn("Repository cleanup skipped [TTL: {}]", ttlInDays);
        }
    }

    // Locates direction directories (ROOT/SYSTEM/DIRECTION)
    // Then calls deleting subroutine with different depth for OUT and IN
    void deleteOldBundlesInDirectory(Path rootDir, long ttlInMs) {

        final DirectoryStream.Filter<Path> directoryFilter = getDirectoryFilter();

        try (DirectoryStream<Path> systemDirStream = Files.newDirectoryStream(rootDir, directoryFilter)) {
            for (Path aSystemDirStream : systemDirStream) {
                try (final DirectoryStream<Path> directionDirStream = Files.newDirectoryStream(aSystemDirStream, directoryFilter)) {
                    for (final Path directionDir : directionDirStream) {
                        final String dirName = directionDir.getFileName().toString();

                        switch (dirName) {
                            case LocalSharedFileRepositoryService.ETX_ADAPTER_OUT_DIRECTORY:
                                deleteOldDirectoriesFromLevel(directionDir, ttlInMs, OUT_BUNDLE_LEVEL);
                                break;
                            case LocalSharedFileRepositoryService.ETX_ADAPTER_IN_DIRECTORY:
                                deleteOldDirectoriesFromLevel(directionDir, ttlInMs, IN_BUNDLE_LEVEL);
                                break;
                            default:
                                LOG.warn("Unexpected directory name: {}", directionDir.toAbsolutePath());
                                break;
                        }
                    }
                }
            }
        } catch (IOException e) {
            LOG.warn("Failed to delete old bundles!", e);
        }
    }

    private DirectoryStream.Filter<Path> getDirectoryFilter() {
        return entry -> entry != null && entry.toFile() != null && entry.toFile().isDirectory();
    }

    void deleteOldDirectoriesFromLevel(Path dir, long ttlInMs, int remainingLevels) throws IOException {
        if (remainingLevels > 0) {
            final DirectoryStream.Filter<Path> directoryFilter = getDirectoryFilter();

            try (DirectoryStream<Path> systemDirStream = Files.newDirectoryStream(dir, directoryFilter)) {
                for (Path aSystemDirStream : systemDirStream) {
                    deleteOldDirectoriesFromLevel(aSystemDirStream, ttlInMs, remainingLevels - 1);
                }
            } catch (IOException e) {
                LOG.warn("Failed to delete old directories from level: {}", remainingLevels, e);
            }
        }

        //delete bundle folder including subdirectories and files if old
        if (remainingLevels == 0) {
            if(Files.isDirectory(dir)){
                //get the original modification date time of the directory
                FileTime originalBundleModifiedTime = Files.getLastModifiedTime(dir);
                LOG.debug("For bundle directory:{}, original modified time: {}", dir,  originalBundleModifiedTime);

                //Walk the bundle directory and delete all files and directories
                Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        LOG.debug("Inside directory:{}, initiating delete of file:{}", dir, file);
                        deleteIfOld(file, ttlInMs);
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                        LOG.debug("Inside postVisitDirectory resetting lastModifiedTime of:{} to:{}", dir, originalBundleModifiedTime);
                        Files.setLastModifiedTime(dir, originalBundleModifiedTime);
                        LOG.debug("Initiating delete of directory:{}", dir);
                        deleteIfOld(dir, ttlInMs);
                        return FileVisitResult.CONTINUE;
                    }
                });
            }
        }
    }

    Long getTtlInDays(Map<AdapterConfigurationProperty, String> config) {
        Long ttlInDays = null;
        String ttlInDaysValue = config.get(ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS);
        if (ttlInDaysValue != null && !ttlInDaysValue.isEmpty()) {
            try {
                ttlInDays = Long.valueOf(ttlInDaysValue);
            } catch (NumberFormatException e) {
                LOG.warn("{} property format is incorrect [{}]", ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS, ttlInDaysValue, e);
            }
        }
        return ttlInDays;
    }
}

package eu.europa.ec.etrustex.adapter.queue.out.downloadattachment;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.client.backend.repository.FileRepository;
import eu.europa.ec.etrustex.adapter.integration.client.backend.repository.FileRepositoryProvider;
import eu.europa.ec.etrustex.adapter.model.entity.administration.BackendFileRepositoryLocationType;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxError;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.model.vo.FileAttachmentVO;
import eu.europa.ec.etrustex.adapter.queue.out.uploadattachment.OutUploadAttachmentProducer;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import eu.europa.ec.etrustex.adapter.support.Util;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.service.WorkspaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Map;

/**
 * @author micleva
 * @project ETX
 */

@Service("etxBackendPluginOutDownloadAttachmentHandler")
@Transactional
public class OutDownloadAttachmentHandlerImpl implements OutDownloadAttachmentHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(OutDownloadAttachmentHandlerImpl.class);

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private FileRepositoryProvider fileRepositoryProvider;

    @Autowired
    private OutUploadAttachmentProducer outUploadAttachmentProducer;

    @Autowired
    private PostProcessProducer postProcessProducer;

    @Autowired
    @Qualifier("etxBackendPluginWorkspaceService")
    private WorkspaceService workspaceService;

    @Override
    public void handleDownloadAttachment(Long attachmentId) {

        EtxAttachment attachmentEntity = attachmentService.findById(attachmentId);

        EtxMessage messageBundleEntity = attachmentEntity.getMessage();

        EtxSystem senderSystem = messageBundleEntity.getSender().getSystem();

        if (!attachmentService.checkAttachmentForSystemInState(attachmentEntity, senderSystem,
                Arrays.asList(AttachmentState.ATTS_DOWNLOADED, AttachmentState.ATTS_UPLOADED))) {

            Map<SystemConfigurationProperty, String> senderSystemConfig = Util.buildSystemConfig(senderSystem.getSystemConfigList());

            BackendFileRepositoryLocationType backendFileRepositoryLocation = BackendFileRepositoryLocationType.valueOf(
                    senderSystemConfig.get(SystemConfigurationProperty.ETX_BACKEND_FILE_REPOSITORY_LOCATION)
            );

            // Download attachments from back-end repository location
            FileRepository fileRepository = fileRepositoryProvider.getFileRepository(backendFileRepositoryLocation);
            FileAttachmentVO fileAttachmentVO = getFileAttachmentVO(attachmentEntity, senderSystemConfig, fileRepository);

            //can be null if error during reading zip files occur (LocalSharedFileRepositoryService)
            if (fileAttachmentVO == null) {
                throw new ETrustExPluginException(ETrustExError.ETX_007, "Reading of attachment file failed: " + attachmentEntity.getId(), null);
            }

            try (InputStream is = fileAttachmentVO.getDataHandler().getInputStream()) {
                workspaceService.writeFile(attachmentId, is);
            } catch (IOException ex) {
                LOG.error("Failed to write file : " + attachmentEntity, ex);
                throw new ETrustExPluginException(ETrustExError.DEFAULT, ex.getMessage(), ex);
            } finally {
                try {
                    fileRepository.releaseResources(fileAttachmentVO);
                } catch (Exception ex) {
                    LOG.error("File correctly downloaded, error while closing Input Stream: " + ex);
                }
            }

            attachmentEntity.setSessionKey(fileAttachmentVO.getSessionKey());
            attachmentEntity.setX509SubjectName(fileAttachmentVO.getX509SubjectName());
            LOG.info("Attachment successfully downloaded; sender system: [{}]", senderSystem.getName());
        } else {
            LOG.warn("Attachment was already downloaded; sender system: [{}]; download ignored.", senderSystem.getName());
        }

        attachmentService.updateAttachmentState(attachmentEntity, AttachmentState.ATTS_CREATED, AttachmentState.ATTS_DOWNLOADED);
        outUploadAttachmentProducer.triggerUpload(attachmentId);
    }

    private FileAttachmentVO getFileAttachmentVO(EtxAttachment attachmentEntity, Map<SystemConfigurationProperty, String> senderSystemConfig, FileRepository fileRepository) {
        try {
            return fileRepository.readFile(senderSystemConfig, attachmentEntity);
        } catch (IOException ex) {
            LOG.error("Failed to read attachment file : " + attachmentEntity, ex);
            throw new ETrustExPluginException(ETrustExError.DEFAULT, ex.getMessage(), ex);
        }
    }

    @Override
    public void validateDownloadAttachment(Long attachmentId) {

        EtxAttachment attachmentEntity = attachmentService.findById(attachmentId);

        if (attachmentEntity == null) {
            String errorDetails = String.format("Attachment [ID: %s] not found", attachmentId);
            LOG.warn(errorDetails);
            throw new ETrustExPluginException(ETrustExError.DEFAULT, errorDetails, null);
        }

        EtxMessage messageBundleEntity = attachmentEntity.getMessage();

        boolean isExpectedMessageState = messageBundleEntity.getMessageState() == MessageState.MS_CREATED;
        boolean isExpectedAttachmentState = attachmentEntity.getStateType() == AttachmentState.ATTS_CREATED;

        if (!isExpectedMessageState || !isExpectedAttachmentState) {
            StringBuilder errorDetails = new StringBuilder();
            errorDetails
                    .append("Invalid message bundle and attachment states (expected message bundle state: MS_CREATED, expected attachment state: ATTS_CREATED), skipping download (from backend system) the attachment with ID: ");
            errorDetails.append(attachmentId);
            errorDetails.append(", state: ");
            errorDetails.append(attachmentEntity.getStateType());
            errorDetails.append(" regarding message bundle with ID: ");
            errorDetails.append(messageBundleEntity.getId());
            errorDetails.append(", state: ");
            errorDetails.append(messageBundleEntity.getMessageState());

            if (LOG.isWarnEnabled()) {
                LOG.warn(errorDetails.toString());
            }

            throw new ETrustExPluginException(ETrustExError.ETX_009, errorDetails.toString(), null);
        }
    }

    @Override
    public void onError(Long attachmentId) {
        EtxAttachment attachmentEntity = attachmentService.findById(attachmentId);
        attachmentEntity.setStateType(AttachmentState.ATTS_FAILED);
        attachmentEntity.setError(new EtxError(ETrustExError.DEFAULT.getCode(), "Failed to download attachment " + attachmentEntity.getAttachmentUuid()));

        postProcessProducer.triggerPostProcess(attachmentEntity.getMessage().getId(), attachmentId);
    }
}
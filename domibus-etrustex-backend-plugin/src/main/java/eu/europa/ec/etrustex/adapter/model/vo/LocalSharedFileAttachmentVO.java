package eu.europa.ec.etrustex.adapter.model.vo;

import java.nio.file.FileSystem;


/**
 * @author tenapju
 * @project ETX
 */
public class LocalSharedFileAttachmentVO extends FileAttachmentVO {

    private FileSystem zipFileSystem;

    public LocalSharedFileAttachmentVO(FileSystem zipFileSystem) {
        this.zipFileSystem = zipFileSystem;
    }

    public FileSystem getZipFileSystem() {
        return zipFileSystem;
    }

    public void setZipFileSystem(FileSystem zipFileSystem) {
        this.zipFileSystem = zipFileSystem;
    }
}

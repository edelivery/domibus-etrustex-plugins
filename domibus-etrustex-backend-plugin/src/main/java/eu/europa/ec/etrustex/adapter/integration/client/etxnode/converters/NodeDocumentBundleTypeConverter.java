package eu.europa.ec.etrustex.adapter.integration.client.etxnode.converters;

import ec.schema.xsd.commonaggregatecomponents_2.DocumentWrapperReferenceType;
import ec.schema.xsd.documentbundle_1.DocumentBundleType;
import eu.europa.ec.etrustex.common.converters.node.NodePartyConverter;
import eu.europa.ec.etrustex.common.util.CalendarHelper;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.*;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.ExtensionContentType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionType;
import oasis.names.specification.ubl.schema.xsd.commonextensioncomponents_2.UBLExtensionsType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.List;

public class NodeDocumentBundleTypeConverter {

    private static final String SIGNATURE_EXTENSION_ID = "signature";
    private static final String SIGNED_DATE_EXTENSION_ID = "signedData";
    private static final String EXTENSION_CONTENT_VALUE_ELEMENT = "valueElement";

    private NodeDocumentBundleTypeConverter() {
    }

    public static DocumentBundleType convertEtxMessage(EtxMessage etxMessage) throws ParserConfigurationException {

        DocumentBundleType documentBundleType = new DocumentBundleType();

        IDType id = new IDType();
        id.setValue(etxMessage.getMessageUuid());
        documentBundleType.setID(id);

        if (etxMessage.getMessageParentUuid() != null) {
            ParentDocumentIDType parentId = new ParentDocumentIDType();
            parentId.setValue(etxMessage.getMessageParentUuid());
            documentBundleType.setParentDocumentID(parentId);
        }

        if (etxMessage.getMessageReferenceUuid() != null) {
            UUIDType uuid = new UUIDType();
            uuid.setValue(etxMessage.getMessageReferenceUuid());
            documentBundleType.setUUID(uuid);
        }

        IssueDateType issueDate = new IssueDateType();
        issueDate.setValue(CalendarHelper.getJodaDateFromCalendar(etxMessage.getIssueDateTime()));
        documentBundleType.setIssueDate(issueDate);

        IssueTimeType issueTime = new IssueTimeType();
        issueTime.setValue(CalendarHelper.getJodaLocalTimeFromCalendar(etxMessage.getIssueDateTime()));
        documentBundleType.setIssueTime(issueTime);

        if (etxMessage.getSubject() != null) {
            ProfileIDType profileId = new ProfileIDType();
            profileId.setValue(etxMessage.getSubject());
            documentBundleType.setProfileID(profileId);
        }

        if (etxMessage.getMessageContent() != null) {
            NoteType note = new NoteType();
            note.setValue(etxMessage.getMessageContent());
            documentBundleType.getNote().add(note);
        }

        if (etxMessage.getSignature() != null || etxMessage.getSignedData() != null) {

            UBLExtensionsType ublExtensionsType = new UBLExtensionsType();

            if (etxMessage.getSignature() != null) {
                ublExtensionsType.getUBLExtension().add(buildExtension(SIGNATURE_EXTENSION_ID, etxMessage.getSignature()));
            }

            if (etxMessage.getSignedData() != null) {
                ublExtensionsType.getUBLExtension().add(buildExtension(SIGNED_DATE_EXTENSION_ID, etxMessage.getSignedData()));
            }

            documentBundleType.setUBLExtensions(ublExtensionsType);
        }

        documentBundleType.setSenderParty(NodePartyConverter.buildPartyType(etxMessage.getSender().getPartyUuid()));
        documentBundleType.getReceiverParty().add(NodePartyConverter.buildPartyType(etxMessage.getReceiver().getPartyUuid()));

        List<DocumentWrapperReferenceType> docReferenceList = documentBundleType.getDocumentWrapperReference();
        docReferenceList.addAll(NodeDocumentWrapperReferenceConverter.convertEtxAttachmentList(etxMessage.getAttachmentList()));

        return documentBundleType;
    }

    public static EtxMessage toEtxMessage(DocumentBundleType documentBundle) {
        if (documentBundle == null) {
            return null;
        }
        EtxMessage etxMessage = new EtxMessage();

        if (documentBundle.getID() != null) {
            etxMessage.setMessageUuid(documentBundle.getID().getValue());
        }

        if (documentBundle.getParentDocumentID() != null) {
            etxMessage.setMessageParentUuid(documentBundle.getParentDocumentID().getValue());
        }

        if (documentBundle.getUUID() != null) {
            etxMessage.setMessageReferenceUuid(documentBundle.getUUID().getValue());
        }

        etxMessage.setIssueDateTime(CalendarHelper.getCalendarIssueDateFromDocumentBundle(documentBundle));

        if (documentBundle.getProfileID() != null) {
            etxMessage.setSubject(documentBundle.getProfileID().getValue());
        }

        if (documentBundle.getNote() != null && !documentBundle.getNote().isEmpty()) {
            etxMessage.setMessageContent(documentBundle.getNote().get(0).getValue());
        }

        Element signatureValueElement = getExtensionContentById(documentBundle, SIGNATURE_EXTENSION_ID);
        if (signatureValueElement != null && signatureValueElement.getTagName().equals(EXTENSION_CONTENT_VALUE_ELEMENT)) {
            NodeList signatureNodeList = signatureValueElement.getChildNodes();
            if (signatureNodeList != null && signatureNodeList.getLength() == 1) {
                String signature = signatureNodeList.item(0).getTextContent();
                etxMessage.setSignature(signature);
            }
        }
        Element signedDataValueElement = getExtensionContentById(documentBundle, SIGNED_DATE_EXTENSION_ID);
        if (signedDataValueElement != null && signedDataValueElement.getTagName().equals(EXTENSION_CONTENT_VALUE_ELEMENT)) {
            NodeList signatureDataNodeList = signedDataValueElement.getChildNodes();
            if (signatureDataNodeList != null) {
                String signedData = signatureDataNodeList.item(0).getTextContent();
                etxMessage.setSignedData(signedData);
            }
        }

        for (DocumentWrapperReferenceType documentWrapperReferenceType : documentBundle.getDocumentWrapperReference()) {
            etxMessage.addAttachment(NodeDocumentWrapperReferenceConverter.toEtxAttachment(documentWrapperReferenceType));
        }

        return etxMessage;
    }

    private static Element getExtensionContentById(DocumentBundleType documentBundle, String extensionId) {
        Element result = null;

        if (documentBundle.getUBLExtensions() != null) {
            for (UBLExtensionType ublExtensionType : documentBundle.getUBLExtensions().getUBLExtension()) {
                if (ublExtensionType.getID().getValue().equals(extensionId)) {
                    result = ublExtensionType.getExtensionContent().getAny();
                }
            }
        }
        return result;
    }

    private static UBLExtensionType buildExtension(String extensionId, String contentValue) throws ParserConfigurationException {

        UBLExtensionType extensionType = new UBLExtensionType();
        IDType idType = new IDType();
        idType.setValue(extensionId);
        extensionType.setID(idType);

        ExtensionContentType extensionContentType = new ExtensionContentType();

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // root elements
        Document doc = docBuilder.newDocument();
        Element valueElement = doc.createElement(EXTENSION_CONTENT_VALUE_ELEMENT);
        valueElement.appendChild(doc.createTextNode(contentValue));

        extensionContentType.setAny(valueElement);
        extensionType.setExtensionContent(extensionContentType);

        return extensionType;
    }
}

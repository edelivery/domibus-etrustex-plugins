package eu.europa.ec.etrustex.adapter.model.entity.administration;

import eu.europa.ec.etrustex.adapter.model.entity.AuditEntity;

import javax.persistence.*;
import java.io.Serializable;
/**
 * <p>
 * Title: SystemConfig
 * </p>
 * <p/>
 * <p>
 * Description: Domain Object describing a SystemConfig entity
 * </p>
 */
@Entity(name = "EtxSystemConfig")
@AttributeOverrides({
        @AttributeOverride(name = "creationDate", column = @Column(name = "SCG_CREATED_ON", insertable = false, updatable = false, nullable = false)),
        @AttributeOverride(name = "modificationDate", column = @Column(name = "SCG_UPDATED_ON"))})
@Table(name = "ETX_ADT_SYSTEM_CONFIG")
//@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class EtxSystemConfig extends AuditEntity<Long> implements Serializable {

    private static final long serialVersionUID = 7747588392106099726L;

    @Id
    @Column(name = "SCG_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "SCG_PROPERTY_NAME", length = 50, nullable = false)
    private String propertyName;

    @Column(name = "SCG_PROPERTY_VALUE", length = 250, nullable = false)
    private String propertyValue;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "SYS_ID", nullable = false)
    private EtxSystem system;

    /**
     * Default constructor
     */
    public EtxSystemConfig() {
        //empty constructor
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getPropertyValue() {
        return this.propertyValue;
    }

    public void setPropertyValue(String propertyValue) {
        this.propertyValue = propertyValue;
    }

    public EtxSystem getSystem() {
        return this.system;
    }

    public void setSystem(EtxSystem system) {
        this.system = system;
    }

    public SystemConfigurationProperty getSystemConfigurationProperty() {
        return SystemConfigurationProperty.fromCode(propertyName);
    }

    public void setSystemConfigurationProperty(SystemConfigurationProperty systemConfigurationProperty) {
        propertyName = systemConfigurationProperty.getCode();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("EtxSystemConfig ");
        sb.append("{id=").append(id);
        sb.append(", propertyName='").append(propertyName).append('\'');
        sb.append(", propertyValue='").append(propertyValue).append('\'');
        sb.append('}');
        return sb.toString();
    }

}

package eu.europa.ec.etrustex.adapter.queue.postupload;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.application.BackEndQuartzJobBean;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.persistence.api.message.MessageDao;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_MESSAGE_UUID;

/**
 * Cron job to list all pending {@link MessageType#MESSAGE_BUNDLE} for which all attachments are in final state and trigger each for PostUpload process.
 * The job will put the message in the temporary state - {@link MessageState#MS_UPLOAD_REQUESTED}
 *
 * @author Arun Raj
 * @version 1.1.1
 * @since 27/06/2019
 */
@DisallowConcurrentExecution
public class TriggerPostUploadJob extends BackEndQuartzJobBean {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(TriggerPostUploadJob.class);

    @Autowired
    private MessageDao messageDao;

    @Autowired
    private PostUploadProducer postUploadProducer;

    @Autowired
    private ClearEtxLogKeysService clearEtxLogKeysService;

    @Override
    @Transactional
    @ClearEtxLogKeys
    public void process(JobExecutionContext context) {
        LOG.debug("TriggerPostUploadJob started..");
        List<EtxMessage> etxMessageList = messageDao.retrieveMessageBundlesForPostUpload();
        for (EtxMessage message : etxMessageList) {
            LOG.putMDC(MDC_ETX_MESSAGE_UUID, message.getMessageUuid());
            try {
                postUploadProducer.triggerBundlePostUpload(message);
            } catch (Exception e) {
                LOG.error("Could not trigger post upload for bundle:[ID:{}]", message.getId());
                throw e;
            }
        }
        LOG.debug("TriggerPostUploadJob completed..");
        clearEtxLogKeysService.clearKeys();
    }
}

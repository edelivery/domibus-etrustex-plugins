package eu.europa.ec.etrustex.adapter.queue.notify;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.client.backend.BackendServiceClient;
import eu.europa.ec.etrustex.adapter.integration.client.backend.repository.WitnessFileServiceClientImpl;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.NodeInvocationManager;
import eu.europa.ec.etrustex.adapter.model.entity.administration.AdapterConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.BackendNotificationType;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.message.*;
import eu.europa.ec.etrustex.adapter.service.AdapterConfigurationService;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.adapter.support.Util;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Map;

@Service("etxBackendPluginNotifyHandler")
public class NotifyHandlerImpl implements NotifyHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(NotifyHandlerImpl.class);

    private static final int DEFAULT_NOTIFICATION_EXPIRATION_DAYS = 7;
    private static final int MS_IN_DAY = 24 * 60 * 60 * 1000;

    @Autowired
    private MessageServicePlugin messageService;

    @Autowired
    private AdapterConfigurationService adapterConfigurationService;

    @Autowired
    private NodeInvocationManager nodeServiceClient;

    @Autowired
    @Qualifier("etxBackendPluginServiceWithWebService")
    private BackendServiceClient webServiceBackendServiceClient;

    @Autowired
    @Qualifier("etxBackendPluginWitnessFileServiceClient")
    private BackendServiceClient witnessFileBackendServiceClient;

    @PostConstruct
    private void init() {
        //todo: drop this when the support for witness file will be dropped
        Map<AdapterConfigurationProperty, String> adapterConfigurations = adapterConfigurationService.getAdapterConfigurations();
        ((WitnessFileServiceClientImpl) witnessFileBackendServiceClient).setAdapterRootPath(
                adapterConfigurations.get(AdapterConfigurationProperty.ETX_ADAPTER_REPOSITORY_ROOT_PATH));
    }

    @Override
    @Transactional(timeout = 300)
    public void handleNotify(final Long messageId) {
        EtxMessage messageEntity = messageService.findById(messageId);
        try {
            if (messageEntity.getNotificationState() != NotificationState.NF_NOTIFIED) {
                notify(messageEntity);
                messageEntity.setNotificationState(NotificationState.NF_NOTIFIED);
            }
        } catch (Exception e) {
            messageOnError(messageEntity);
        }
    }

    protected void notify(EtxMessage messageEntity) {
        if (messageEntity.getDirectionType() == MessageDirection.IN) {
            notifyBackend(messageEntity);
            LOG.info("Backend notification for message completed.");
        } else {
            notifyNode(messageEntity);
            LOG.info("Node notification for message completed.");
        }
    }

    private void notifyBackend(EtxMessage messageEntity) {
        EtxSystem receiverSystem = messageEntity.getReceiver().getSystem();

        Map<SystemConfigurationProperty, String> receiverSystemConfig = Util.buildSystemConfig(receiverSystem.getSystemConfigList());
        BackendNotificationType bundleNotificationType = BackendNotificationType.fromCode(
                receiverSystemConfig.get(SystemConfigurationProperty.ETX_BACKEND_NOTIFICATION_TYPE)
        );

        BackendServiceClient backendServiceClient;
        switch (bundleNotificationType) {
            case WEB_SERVICE:
                backendServiceClient = webServiceBackendServiceClient;
                break;
            case WITNESS_FILE:
                backendServiceClient = witnessFileBackendServiceClient;
                break;
            default:
                throw new IllegalArgumentException("Unknown bundleNotificationType!");
        }

        // Invoke back-end web service
        if (messageEntity.getMessageType() == MessageType.MESSAGE_BUNDLE) {
            backendServiceClient.notifyMessageBundle(receiverSystemConfig, messageEntity);
        } else if (messageEntity.getMessageType() == MessageType.MESSAGE_STATUS
                || messageEntity.getMessageType() == MessageType.MESSAGE_ADAPTER_STATUS) {
            MessageType refMessageType = messageService.referenceMessageTypeFor(messageEntity);
            backendServiceClient.notifyMessageStatus(receiverSystemConfig, messageEntity, refMessageType);
        }
    }

    private void notifyNode(EtxMessage messageEntity) {

        nodeServiceClient.sendMessageStatus(messageEntity);
    }

    @Override
    public void validateNotify(Long messageId) {
        EtxMessage messageEntity = messageService.findById(messageId);

        if (messageEntity == null) {
            String errorDetails = String.format("Message [ID: %s] not found", messageId);
            LOG.warn(errorDetails);
            throw new ETrustExPluginException(ETrustExError.DEFAULT, errorDetails, null);
        }

        if (messageEntity.getMessageState() != MessageState.MS_PROCESSED) {
            StringBuilder errorDetails = new StringBuilder();
            errorDetails.append("Invalid message state (expected: MS_PROCESSED [2]), skipping processing of message ID: ");
            errorDetails.append(messageId);
            errorDetails.append(", state: ");
            errorDetails.append(messageEntity.getMessageState());

            LOG.warn(errorDetails.toString());

            throw new ETrustExPluginException(ETrustExError.ETX_009, errorDetails.toString(), null);
        }
    }

    @Override
    public void onError(Long messageId) {
        EtxMessage messageEntity = messageService.findById(messageId);

        messageOnError(messageEntity);
    }

    private void messageOnError(EtxMessage messageEntity) {
        if (isNotificationExpired(messageEntity)) {
            messageEntity.setNotificationState(NotificationState.NF_EXPIRED);
            messageService.updateMessage(messageEntity);
        }
    }

    private boolean isNotificationExpired(EtxMessage messageEntity) {
        long currentInMs = System.currentTimeMillis();
        long creationInMs = messageEntity.getCreationDate().getTimeInMillis();

        Map<AdapterConfigurationProperty, String> adapterConfig = adapterConfigurationService.getAdapterConfigurations();
        int ttlDays;
        try {
            ttlDays = Integer.valueOf(adapterConfig.get(AdapterConfigurationProperty.ETX_ADAPTER_NOTIFICATION_EXPIRATION_DAYS));
        } catch (NumberFormatException e) {
            LOG.warn("Configuration property " + AdapterConfigurationProperty.ETX_ADAPTER_NOTIFICATION_EXPIRATION_DAYS.getCode() +
                    " is not set. Using default value: " + DEFAULT_NOTIFICATION_EXPIRATION_DAYS);
            ttlDays = DEFAULT_NOTIFICATION_EXPIRATION_DAYS;
        }
        return (currentInMs - creationInMs) > (ttlDays * MS_IN_DAY);
    }
}

package eu.europa.ec.etrustex.adapter.integration.client.backend;

import eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.model.vo.FileAttachmentVO;

import java.io.File;
import java.util.Map;

/**
 * @author micleva
 * @project ETX
 */

public interface BackendServiceClient {

    FileAttachmentVO downloadAttachment(Map<SystemConfigurationProperty, String> systemConfig, EtxMessage messageBundle, EtxAttachment etxAttachment);

    void notifyMessageBundle(Map<SystemConfigurationProperty, String> systemConfig, EtxMessage messageBundle);

    void notifyMessageStatus(Map<SystemConfigurationProperty, String> systemConfig, EtxMessage messageStatus, MessageType refMessageType);

    void uploadAttachment(Map<SystemConfigurationProperty, String> systemConfig, EtxMessage messageBundle, EtxAttachment attachment, File attachmentFile);
}

package eu.europa.ec.etrustex.adapter.web.retriggeroutgoingbundle;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class RetriggerBundleRequest {

    private String messageUuid;
    private String messageType;

    private String senderUuid;
    private String receiverUuid;
    private String messageRefUUID;
    private String messageState;
    private String messageDirection;

    public RetriggerBundleRequest() {
    }

    public RetriggerBundleRequest(String messageUuid, String messageType, String senderUuid, String receiverUuid, String messageRefUUID, String messageState, String messageDirection) {
        this.messageUuid = messageUuid;
        this.messageType = messageType;
        this.senderUuid = senderUuid;
        this.receiverUuid = receiverUuid;
        this.messageRefUUID = messageRefUUID;
        this.messageState = messageState;
        this.messageDirection = messageDirection;
    }

    public String getMessageUuid() {
        return messageUuid;
    }

    public void setMessageUuid(String messageUuid) {
        this.messageUuid = messageUuid;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getSenderUuid() {
        return senderUuid;
    }

    public void setSenderUuid(String senderUuid) {
        this.senderUuid = senderUuid;
    }

    public String getReceiverUuid() {
        return receiverUuid;
    }

    public void setReceiverUuid(String receiverUuid) {
        this.receiverUuid = receiverUuid;
    }

    public String getMessageRefUUID() {
        return messageRefUUID;
    }

    public void setMessageRefUUID(String messageRefUUID) {
        this.messageRefUUID = messageRefUUID;
    }

    public String getMessageState() {
        return messageState;
    }

    public void setMessageState(String messageState) {
        this.messageState = messageState;
    }

    public String getMessageDirection() {
        return messageDirection;
    }

    public void setMessageDirection(String messageDirection) {
        this.messageDirection = messageDirection;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof RetriggerBundleRequest)) return false;

        RetriggerBundleRequest that = (RetriggerBundleRequest) o;

        return new EqualsBuilder().append(messageUuid, that.messageUuid).append(messageType, that.messageType).append(senderUuid, that.senderUuid).append(receiverUuid, that.receiverUuid).append(messageRefUUID, that.messageRefUUID).append(messageState, that.messageState).append(messageDirection, that.messageDirection).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(messageUuid).append(messageType).append(senderUuid).append(receiverUuid).append(messageRefUUID).append(messageState).append(messageDirection).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("messageUuid", messageUuid)
                .append("messageType", messageType)
                .append("senderUuid", senderUuid)
                .append("receiverUuid", receiverUuid)
                .append("messageRefUUID", messageRefUUID)
                .append("messageState", messageState)
                .append("messageDirection", messageDirection)
                .toString();
    }
}

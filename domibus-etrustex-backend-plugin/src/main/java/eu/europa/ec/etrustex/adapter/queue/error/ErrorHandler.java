package eu.europa.ec.etrustex.adapter.queue.error;

public interface ErrorHandler {

    void onError(String asMsgId);

}

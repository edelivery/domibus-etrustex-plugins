/**
 *
 */
package eu.europa.ec.etrustex.adapter.model.entity.message;

/**
 * @author apladap
 */
public enum MessageType {
    MESSAGE_BUNDLE,
    MESSAGE_STATUS,
    MESSAGE_ADAPTER_STATUS
}

package eu.europa.ec.etrustex.adapter.integration.handlers;

import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.jms.SelectableTextMessageCreator;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import oasis.names.specification.ubl.schema.xsd.fault_1.FaultType;

/**
 * @author Arun Raj, Federico Martini
 * @since June 2017
 * @version 1.0
 */
public abstract class BackendResponseHandlerBase<R, I, F> {

    protected String getNodeFaultResponseCode(FaultType faultInfo) {
        return (faultInfo != null) ? faultInfo.getResponseCode().getValue() : "\'\'";
    }

    SelectableTextMessageCreator buildJMSMessage(Object obj, String selectingProp, String selectingValue) {
        String objAsXml = TransformerUtils.serializeObjToXML(obj);
        return new SelectableTextMessageCreator(objAsXml, selectingProp, selectingValue);
    }

    /**
     * In case of Soap fault
     *
     * @param faultResponse soap fault
     * @param identifier could be {@link String}, {@link EtxMessage}, {@link EtxAttachment}, etc.
     */
    protected abstract void processNodeFault(F faultResponse, I identifier);

    /**
     * In case of success (no soap Fault)
     *
     * @param response if no soap fault
     * @param identifier could be {@link String}, {@link EtxMessage}, {@link EtxAttachment}, etc.
     */
    protected abstract void processNodeSuccess(R response, I identifier);

    /**
     * Processes the ETX node response
     *
     * @param faultResponse soap fault
     * @param response if no soap fault
     * @param identifier could be {@link String}, {@link EtxMessage}, {@link EtxAttachment}, etc.
     */
    void processNodeResponse(F faultResponse, R response, I identifier) {
        try {
            if (null != faultResponse) {
                processNodeFault(faultResponse, identifier);
            } else {
                processNodeSuccess(response, identifier);
            }
        } catch (Exception ex) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "Error while processing response from ETX node", ex);
        }
    }
}



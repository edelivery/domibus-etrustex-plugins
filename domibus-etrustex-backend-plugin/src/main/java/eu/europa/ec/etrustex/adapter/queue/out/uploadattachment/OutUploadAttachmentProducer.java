package eu.europa.ec.etrustex.adapter.queue.out.uploadattachment;

import eu.europa.ec.etrustex.adapter.queue.JmsProducer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.jms.Queue;

/**
 * @author micleva
 * @project ETX
 */

@Service("etxBackendPluginOutUploadAttachmentProducer")
public class OutUploadAttachmentProducer {

    private final Queue jmsQueue;

    private final JmsProducer<OutUploadAttachmentQueueMessage> jmsProducer;

    public OutUploadAttachmentProducer(@Qualifier("etxBackendPluginOutboxUploadAttachment") Queue jmsQueue,
                                       JmsProducer<OutUploadAttachmentQueueMessage> jmsProducer) {
        this.jmsQueue = jmsQueue;
        this.jmsProducer = jmsProducer;
    }

    public void triggerUpload(Long attachmentId) {
        OutUploadAttachmentQueueMessage queueMessage = new OutUploadAttachmentQueueMessage();
        queueMessage.setAttachmentId(attachmentId);
        jmsProducer.postMessage(jmsQueue, queueMessage);
    }
}

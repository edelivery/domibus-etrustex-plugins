package eu.europa.ec.etrustex.adapter.persistence.api.administration;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBase;

import java.util.List;

public interface PartyDao extends DaoBase<EtxParty> {
    /**
     * Find the {@link EtxParty} for which {@link EtxParty#partyUuid} = {@param partyUUID}
     *
     * @param partyUUID {@link EtxParty#partyUuid}
     * @return {@link EtxParty} linked with the {@param partyUUID}
     */
    EtxParty findByPartyUUID(String partyUUID);

    /**
     * Find the List of all {@link EtxParty}
     * @return List<EtxParty>
     */
    List<EtxParty> findAll();
}

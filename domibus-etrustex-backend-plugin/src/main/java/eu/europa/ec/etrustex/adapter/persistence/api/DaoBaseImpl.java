package eu.europa.ec.etrustex.adapter.persistence.api;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static java.time.ZoneId.systemDefault;
import static java.util.Date.from;

/**
 * An abstract JPA-based DAO. Each DAO instance is bound to a specific Entity
 * type, which is managed by the given DAO.
 *
 * @param <T> the entity type which is handled by this DAO
 */
public abstract class DaoBaseImpl<T> implements DaoBase<T> {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DaoBaseImpl.class);

    private final String modelName;
    private final Class<T> entityClass;

    @PersistenceContext(unitName = "domibusJTA")
    private EntityManager entityManager;

    protected DaoBaseImpl(Class<T> entityClass) {
        this.modelName = entityClass.getAnnotation(Entity.class).name();
        this.entityClass = entityClass;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * eu.europa.ec.etrustex.adapter.persistence.DaoBaseLocal#findById(java.lang
     * .Long)
     */
    @Override
    public T findById(Long id) {
        LOG.debug("finding {} instance with ID: {}", modelName, id);
        return entityManager.find(getEntityClass(), id);
    }

    @Override
    public List<T> findAll() {
        LOG.debug("finding all  {}", modelName);
        CriteriaQuery<T> cq = entityManager.getCriteriaBuilder().createQuery(getEntityClass());
        CriteriaQuery<T> all = cq.select(cq.from(getEntityClass()));
        TypedQuery<T> allQuery = entityManager.createQuery(all);
        return allQuery.getResultList();
    }

    private Class<T> getEntityClass() {
        return entityClass;
    }

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    /*
     * (non-Javadoc)
     *
     * @see eu.europa.ec.etrustex.adapter.persistence.DaoBaseLocal#save(T)
     */
    @Override
    @Transactional
    public void save(T instance) {
        LOG.debug("saving {} instance  {}", modelName, instance);
        entityManager.persist(instance);
        LOG.debug("save successful");
    }

    /*
     * (non-Javadoc)
     *
     * @see eu.europa.ec.etrustex.adapter.persistence.DaoBaseLocal#remove(T)
     */
    @Override
    @Transactional
    public void remove(T instance) {
        LOG.debug("remove {} instance {}", modelName, instance);
        entityManager.remove(instance);
        LOG.debug("remove successful");
    }

    /*
     * (non-Javadoc)
     *
     * @see eu.europa.ec.etrustex.adapter.persistence.DaoBaseLocal#remove(T)
     */
    @Transactional
    @Override
    public void removeById(Long id) {
        LOG.debug("remove [{}] instance [{}] for id [{}]", modelName, this.entityClass, id);
        entityManager.remove(entityManager.find(this.entityClass, id));
        LOG.debug("remove successful");
    }

    /*
     * (non-Javadoc)
     *
     * @see eu.europa.ec.etrustex.adapter.persistence.DaoBaseLocal#update(T)
     */
    @Transactional
    @Override
    public T update(T detachedInstance) {
        LOG.debug("updating {} detached instance", modelName);
        T result = entityManager.merge(detachedInstance);
        LOG.debug("update successful");
        return result;
    }

    public static Calendar getCalendar(LocalDateTime localDateTime) {
        return getCalendar(from(localDateTime.atZone(systemDefault()).toInstant()));
    }

    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

}

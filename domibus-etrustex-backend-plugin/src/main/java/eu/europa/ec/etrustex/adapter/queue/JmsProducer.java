package eu.europa.ec.etrustex.adapter.queue;

import eu.europa.ec.etrustex.common.jms.QueueMessage;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.ProducerCallback;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.Queue;
import java.util.List;

/**
 * @author micleva
 * @project ETX
 */

@Service("etxBackendPluginJmsProducer")
public class JmsProducer<T extends QueueMessage> {

    private final JmsTemplate jmsTemplate;

    public JmsProducer(@Qualifier("etxBackendPluginJmsTemplate") JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Transactional
    public void postMessage(Queue queue, T message) {
        jmsTemplate.convertAndSend(queue, message);
    }

    @Transactional
    public void postMessageList(Queue queue, final List<T> messageList) {
        if (messageList.isEmpty()) {
            return;
        }
        MessageConverter messageConverter = jmsTemplate.getMessageConverter();
        jmsTemplate.execute(queue, (ProducerCallback<T>) (session, producer) -> {
            for (T message : messageList) {
                producer.send(messageConverter.toMessage(message, session));
            }
            return null;
        });
    }
}

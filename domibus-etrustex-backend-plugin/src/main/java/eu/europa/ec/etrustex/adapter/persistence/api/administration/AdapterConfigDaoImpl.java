package eu.europa.ec.etrustex.adapter.persistence.api.administration;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxAdapterConfig;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class AdapterConfigDaoImpl extends DaoBaseImpl<EtxAdapterConfig> implements AdapterConfigDao {

    public AdapterConfigDaoImpl() {
        super(EtxAdapterConfig.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public List<EtxAdapterConfig> findAdapterConfigs() {
        TypedQuery<EtxAdapterConfig> query = getEntityManager().createNamedQuery("findAllAdapterConfigs", EtxAdapterConfig.class);
        return query.getResultList();
    }
}

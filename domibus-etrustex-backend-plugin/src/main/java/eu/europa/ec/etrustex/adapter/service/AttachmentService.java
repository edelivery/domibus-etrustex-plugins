package eu.europa.ec.etrustex.adapter.service;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import org.springframework.transaction.annotation.Propagation;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @author micleva
 * @project ETX
 */

public interface AttachmentService {

    boolean areAllAttachmentsInFinalState(EtxMessage message);

    /**
     * Finds an {@link EtxAttachment} by its numeric ID (primary key).
     *
     * @param id an ID
     * @return the persistent {@link EtxAttachment} with the given ID, or null, if not found
     */
    EtxAttachment findById(Long id);

    boolean areAllAttachmentsUploaded(EtxMessage message);

    boolean hasFailedAttachment(EtxMessage message);

    /**
     * Set the {@link EtxAttachment} of a given id in {@link AttachmentState#ATTS_FAILED} state and add an error with a given
     * {@param errorMessage} and a given {@param errorCode}
     * This method has {@link Propagation#REQUIRES_NEW}
     *  @param id           of a {@link EtxAttachment}
     * @param errorCode    {@link ETrustExError#code}
     * @param errorMessage Message to be included in the Error object
     */
    void attachmentInError(Long id, String errorCode, String errorMessage);

    void updateAttachmentState(EtxAttachment attachmentEntity, AttachmentState oldState, AttachmentState newState);

    boolean checkAttachmentForSystemInState(EtxAttachment attachment, EtxSystem senderSystem, List<AttachmentState> attachmentState);

    /**
     * Return the number of expired {@link EtxAttachment}
     * An {@link EtxAttachment} is expired when {@link EtxAttachment#creationDate} < {@param date}
     *
     * @param date to compare the {@link EtxAttachment#creationDate} to
     * @return Number of expired {@link EtxAttachment}
     */
    long countNextAttachmentsToExpire(Date date);

    /**
     * Perform the expiration process for the expired {@link EtxAttachment} of a given {@param date}.
     * Only perform the operation for the {@link EtxAttachment} at the {@param index} with a given {@param pageSize}
     * of the pagination
     * <p>
     * An {@link EtxAttachment} is expired when {@link EtxAttachment#creationDate} < {@param date}
     *
     * @param date     to compare the {@link EtxAttachment#creationDate} to
     * @param pageSize of the pagination
     * @param index    of the pagination
     */
    void expireAttachments(Date date, int pageSize, int index);

    /**
     * To retrieve the entry from ETX_ADT_ATTACHMENT with a matching Domibus message Id
     *
     * @param domibusMessageID domibusId of the {@link EtxAttachment}
     * @return EtxAttachment with the domibusId
     */
    EtxAttachment findAttachmentByDomibusMessageId(String domibusMessageID);

    void updateStuckAttachments(LocalDateTime startDate, LocalDateTime endDate);
}

package eu.europa.ec.etrustex.adapter.integration.handlers;

import eu.europa.ec.etrustex.common.handlers.Action;
import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.handlers.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 * Factory to instantiate the handler service class to handle specific AS4 {@link Service} and AS4 {@link Action}<br/>
 * These handlers will be responsible to handle the messages reaching the backend plugin from Domibus. <br/>
 *
 * @author Arun Raj
 * @version 1.0
 */
@org.springframework.stereotype.Service("etxBackendPluginBackendActionHandlerFactory")
public class BackendActionHandlerFactory {

    @Autowired
    private ApplicationContext context;

    /**
     * Instantiate the handlers responsible to handle the messages reaching the backend plugin from Domibus. <br/>
     *
     * @param service instance of {@link Service}
     * @param action  instance of {@link Action}
     * @return instance of {@link OperationHandler}
     */
    public OperationHandler getOperationHandler(Service service, Action action) {

        OperationHandler operationHandler;

        switch (service) {
            case APPLICATION_RESPONSE_SERVICE:
                switch (action) {
                    case SUBMIT_APPLICATION_RESPONSE_RESPONSE:
                        operationHandler = context.getBean(SubmitApplicationResponseResponseHandler.class);
                        break;
                    case FAULT_ACTION:
                        operationHandler = context.getBean(ResponseFaultHandler.class);
                        break;
                    default:
                        throw new UnsupportedOperationException(composeUnsupportedErrorMsg(service, action));
                }
                break;

            case DOCUMENT_WRAPPER_SERVICE:
                switch (action) {
                    case STORE_DOCUMENT_WRAPPER_RESPONSE:
                        operationHandler = context.getBean(StoreDocumentWrapperResponseHandler.class);
                        break;
                    case FAULT_ACTION:
                        operationHandler = context.getBean(StoreDocumentWrapperResponseFaultHandler.class);
                        break;
                    default:
                        throw new UnsupportedOperationException(composeUnsupportedErrorMsg(service, action));
                }
                break;

            case DOCUMENT_BUNDLE_SERVICE:
                switch (action) {
                    case SUBMIT_DOCUMENT_BUNDLE_RESPONSE:
                        operationHandler = context.getBean(SubmitDocumentBundleResponseHandler.class);
                        break;
                    case FAULT_ACTION:
                        operationHandler = context.getBean(ResponseFaultHandler.class);
                        break;
                    default:
                        throw new UnsupportedOperationException(composeUnsupportedErrorMsg(service, action));
                }
                break;
            case RETRIEVE_ICA_SERVICE:
                switch (action) {
                    case RETRIEVE_ICA_RESPONSE:
                        operationHandler = context.getBean(RetrieveICAResponseHandler.class);
                        break;
                    case FAULT_ACTION:
                        operationHandler = context.getBean(RetrieveICAResponseFaultHandler.class);
                        break;
                    default:
                        throw new UnsupportedOperationException(composeUnsupportedErrorMsg(service, action));
                }
                break;
            case WRAPPER_TRANSMISSION_SERVICE:
                switch (action) {
                    case TRANSMIT_WRAPPER_TO_BACKEND:
                        operationHandler = context.getBean(InboxWrapperTransmissionBackendHandler.class);
                        break;
                    case FAULT_ACTION:
                        operationHandler = context.getBean(ResponseFaultHandler.class);
                        break;
                    default:
                        throw new UnsupportedOperationException(composeUnsupportedErrorMsg(service, action));
                }
                break;
            case BUNDLE_TRANSMISSION_SERVICE:
                switch (action) {
                    case NOTIFY_BUNDLE_TO_BACKEND_REQUEST:
                        operationHandler = context.getBean(InboxBundleTransmissionBackendHandler.class);
                        break;
                    case FAULT_ACTION:
                        operationHandler = context.getBean(InboxBundleTransmissionFaultHandler.class);
                        break;
                    default:
                        throw new UnsupportedOperationException(composeUnsupportedErrorMsg(service, action));
                }
                break;
            case STATUS_MESSAGE_TRANSMISSION_SERVICE:
                if (Action.STATUS_MESSAGE_TRANSMISSION == action) {
                    operationHandler = context.getBean(InboxMessageStatusTransmissionBackendHandler.class);
                } else {
                    throw new UnsupportedOperationException(composeUnsupportedErrorMsg(service, action));
                }
                break;
            case UNKNOWN:
            default:
                throw new UnsupportedOperationException(composeUnsupportedErrorMsg(service, action));
        }
        return operationHandler;
    }

    private String composeUnsupportedErrorMsg(Service service, Action action) {
        return "Operation Unsupported for Service:[" + service + "] Action:[" + action + "]";
    }
}

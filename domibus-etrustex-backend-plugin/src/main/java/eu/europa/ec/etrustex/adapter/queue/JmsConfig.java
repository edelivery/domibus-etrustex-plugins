package eu.europa.ec.etrustex.adapter.queue;

import eu.domibus.common.JMSConstants;
import eu.domibus.messaging.MessageConstants;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.queue.deadletter.DeadLetterConsumer;
import eu.europa.ec.etrustex.adapter.queue.error.ErrorConsumer;
import eu.europa.ec.etrustex.adapter.queue.inbox.uploadattachment.InboxUploadAttachmentConsumer;
import eu.europa.ec.etrustex.adapter.queue.notify.NotifyConsumer;
import eu.europa.ec.etrustex.adapter.queue.out.downloadattachment.OutDownloadAttachmentConsumer;
import eu.europa.ec.etrustex.adapter.queue.out.sendmessage.SendMessageConsumer;
import eu.europa.ec.etrustex.adapter.queue.out.uploadattachment.OutUploadAttachmentConsumer;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessConsumer;
import eu.europa.ec.etrustex.adapter.queue.postupload.PostUploadConsumer;
import eu.europa.ec.etrustex.common.jms.TextMessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.transaction.PlatformTransactionManager;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Queue;

import static eu.europa.ec.etrustex.adapter.queue.out.downloadattachment.OutDownloadAttachmentProducer.ETX_BACKEND_PLUGIN_OUTBOX_DOWNLOAD_ATTACHMENT_QUEUE;
import static eu.europa.ec.etrustex.adapter.queue.out.sendmessage.SendMessageProducer.ETX_BACKEND_PLUGIN_OUTBOX_SEND_MESSAGE_BUNDLE_QUEUE;

/**
 * This Java class based Spring configuration of queue producer & consumers is introduced in 1.1.<br/>
 * It was changed from the JmsConfig XML configuration of 1.0 in order to be able to specify the queue connection parameters
 * like count of consumers, localized transaction timeouts per message container.
 *
 * @author Francois Gautier
 * @version 1.1
 * @since October 2018
 */
@Configuration
public class JmsConfig {

    private static final String SINGLE_CONSUMER = "1-1";

    private final ConnectionFactory connectionFactory;

    private final ETrustExBackendPluginProperties eTrustExBackendPluginProperties;

    private final PlatformTransactionManager transactionManager;

    private final ETrustExBackendPluginJMSErrorHandler eTrustExBackendPluginJMSErrorHandler;

    public JmsConfig(@Qualifier("etxBackendPluginProperties") ETrustExBackendPluginProperties properties,
                     @Qualifier(JMSConstants.DOMIBUS_JMS_XACONNECTION_FACTORY) ConnectionFactory connectionFactory,
                     PlatformTransactionManager transactionManager,
                     @Qualifier("etxBackendPluginJMSErrorHandler") ETrustExBackendPluginJMSErrorHandler eTrustExBackendPluginJMSErrorHandler) {
        this.eTrustExBackendPluginProperties = properties;
        this.connectionFactory = connectionFactory;
        this.transactionManager = transactionManager;
        this.eTrustExBackendPluginJMSErrorHandler = eTrustExBackendPluginJMSErrorHandler;
    }

    @Bean("etxBackendPluginTextMessageCreator")
    public TextMessageConverter textMessageCreator() {
        TextMessageConverter textMessageConverter = new TextMessageConverter();
        textMessageConverter.setDomain(eTrustExBackendPluginProperties.getDomain());
        return textMessageConverter;
    }

    @Bean("etxBackendPluginJmsTemplate")
    public JmsTemplate jmsTemplate() {
        JmsTemplate jmsTemplate = new JmsTemplate();
        jmsTemplate.setConnectionFactory(connectionFactory);
        jmsTemplate.setReceiveTimeout(eTrustExBackendPluginProperties.getDefaultReceiveTimeout());
        jmsTemplate.setSessionTransacted(true);
        jmsTemplate.setMessageConverter(textMessageCreator());
        jmsTemplate.setSessionAcknowledgeModeName("AUTO_ACKNOWLEDGE");
        return jmsTemplate;
    }

    @Bean("etxBackendPluginOutDownloadAttachmentListenerContainer")
    public DefaultMessageListenerContainer outDownloadAttachmentListenerContainer(
            @Qualifier(ETX_BACKEND_PLUGIN_OUTBOX_DOWNLOAD_ATTACHMENT_QUEUE) Queue destination,
            @Qualifier("etxBackendPluginOutDownloadAttachmentConsumer") OutDownloadAttachmentConsumer consumer) {
        return getDefaultMessageListenerContainer(consumer, destination, eTrustExBackendPluginProperties.getOutDownloadAttachmentListenerContainerConcurrentConsumers(), eTrustExBackendPluginProperties.getOutDownloadAttachmentTransactionTimeout());
    }

    @Bean("etxBackendPluginSendMessageListenerContainer")
    public DefaultMessageListenerContainer sendMessageListenerContainer(
            @Qualifier(ETX_BACKEND_PLUGIN_OUTBOX_SEND_MESSAGE_BUNDLE_QUEUE) Queue destination,
            @Qualifier("etxBackendPluginSendMessageConsumer") SendMessageConsumer consumer) {
        return getDefaultMessageListenerContainer(consumer, destination, eTrustExBackendPluginProperties.getSendMessageListenerContainerConcurrentConsumers(), eTrustExBackendPluginProperties.getSendMessageTransactionTimeout());
    }

    @Bean("etxBackendPluginOutUploadAttachmentListenerContainer")
    public DefaultMessageListenerContainer outUploadAttachmentListenerContainer(
            @Qualifier("etxBackendPluginOutboxUploadAttachment") Queue destination,
            @Qualifier("etxBackendPluginOutUploadAttachmentConsumer") OutUploadAttachmentConsumer consumer) {
        return getDefaultMessageListenerContainer(consumer, destination, eTrustExBackendPluginProperties.getOutUploadAttachmentListenerContainerConcurrentConsumers(), eTrustExBackendPluginProperties.getOutUploadAttachmentTransactionTimeout());
    }

    @Bean("etxBackendPluginInboxUploadAttachmentListenerContainer")
    public DefaultMessageListenerContainer inboxUploadAttachmentListenerContainer(
            @Qualifier("etxBackendPluginInboxUploadAttachment") Queue destination,
            @Qualifier("etxBackendPluginInboxUploadAttachmentConsumer") InboxUploadAttachmentConsumer consumer) {
        return getDefaultMessageListenerContainer(consumer, destination, eTrustExBackendPluginProperties.getInboxUploadAttachmentListenerContainerConcurrentConsumers(), eTrustExBackendPluginProperties.getInboxUploadAttachmentTransactionTimeout());
    }

    @Bean("etxBackendPluginPostUploadListenerContainer")
    public DefaultMessageListenerContainer postUploadListenerContainer(
            @Qualifier("etxBackendPluginPostUpload") Queue destination,
            @Qualifier("etxBackendPluginPostUploadConsumer") PostUploadConsumer consumer) {
        return getDefaultMessageListenerContainer(consumer, destination, eTrustExBackendPluginProperties.getPostUploadTimeout());
    }

    @Bean("etxBackendPluginNotifyListenerContainer")
    public DefaultMessageListenerContainer notifyListenerContainer(
            @Qualifier("etxBackendPluginNotifyQueue") Queue destination,
            @Qualifier("etxBackendPluginNotifyConsumer") NotifyConsumer consumer) {
        return getDefaultMessageListenerContainer(consumer, destination, eTrustExBackendPluginProperties.getNotifyTransactionTimeout());
    }

    @Bean("etxBackendPluginPostProcessListenerContainer")
    public DefaultMessageListenerContainer postProcessListenerContainer(
            @Qualifier("etxBackendPluginPostProcessing") Queue destination,
            @Qualifier("etxBackendPluginPostProcessConsumer") PostProcessConsumer consumer) {
        return getDefaultMessageListenerContainer(consumer, destination, eTrustExBackendPluginProperties.getPostProcessListenerContainerConcurrentConsumers(), eTrustExBackendPluginProperties.getPostProcessTimeout());
    }

    @Bean("etxBackendPluginErrorListenerContainer")
    public DefaultMessageListenerContainer errorListenerContainer(
            @Qualifier("etxBackendPluginError") Queue destination,
            @Qualifier("etxBackendPluginErrorConsumer") ErrorConsumer consumer) {
        return getDefaultMessageListenerContainer(consumer, destination, "1", eTrustExBackendPluginProperties.getErrorTransactionTimeout());
    }

    @Bean("etxBackendPluginDeadLetterListenerContainer")
    public DefaultMessageListenerContainer deadLetterListenerContainer(
            @Qualifier("etxBackendPluginDeadLetter") Queue destination,
            @Qualifier("etxBackendPluginDeadLetterConsumer") DeadLetterConsumer consumer) {
        return getDefaultMessageListenerContainer(consumer, destination, eTrustExBackendPluginProperties.getDlqConcurrentConsumers(), eTrustExBackendPluginProperties.getDlqTransactionTimeout());
    }

    private DefaultMessageListenerContainer getDefaultMessageListenerContainer(
            AbstractQueueListener<?> consumer,
            Destination destination,
            String concurrentConsumer,
            Integer transactionTimeout){
        return getDefaultMessageListenerContainer(consumer, destination, concurrentConsumer, transactionTimeout,eTrustExBackendPluginProperties.getInternalQueueConcurrency());
    }

    private DefaultMessageListenerContainer getDefaultMessageListenerContainer(
            AbstractQueueListener<?> consumer,
            Destination destination,
            Integer transactionTimeout){
        return getDefaultMessageListenerContainer(consumer, destination, null, transactionTimeout, SINGLE_CONSUMER);
    }

    private DefaultMessageListenerContainer getDefaultMessageListenerContainer(
            AbstractQueueListener<?> consumer,
            Destination destination,
            String concurrentConsumer,
            Integer transactionTimeout,
            String defaultConcurrency) {
        DefaultMessageListenerContainer result = new DefaultMessageListenerContainer();
        result.setMessageSelector(MessageConstants.DOMAIN + "='"+eTrustExBackendPluginProperties.getDomain()+"'");
        result.setConnectionFactory(connectionFactory);
        result.setTransactionManager(transactionManager);
        result.setTransactionTimeout(transactionTimeout);
        result.setReceiveTimeout(eTrustExBackendPluginProperties.getListenerReceiveTimeout());
        result.setRecoveryInterval(org.springframework.util.backoff.BackOffExecution.STOP);
        result.setConcurrency(defaultConcurrency);
        result.setSessionTransacted(true);
        result.setSessionAcknowledgeMode(0);
        result.setMessageListener(consumer);
        if (concurrentConsumer != null) {
            result.setConcurrency(concurrentConsumer);
        }
        result.setDestination(destination);
        result.setErrorHandler(eTrustExBackendPluginJMSErrorHandler);
        return result;
    }
}

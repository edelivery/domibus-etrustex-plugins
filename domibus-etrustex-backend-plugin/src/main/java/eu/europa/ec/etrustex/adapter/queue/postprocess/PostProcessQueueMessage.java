package eu.europa.ec.etrustex.adapter.queue.postprocess;

import eu.europa.ec.etrustex.common.jms.QueueMessage;

/**
 * @author micleva
 * @project ETX
 */

public class PostProcessQueueMessage extends QueueMessage {

    private final Long messageId;
    private final Long attachmentId;

    public PostProcessQueueMessage(Long messageId) {
        this.messageId = messageId;
        this.attachmentId = null;
    }

    public PostProcessQueueMessage(Long messageId, Long attachmentId) {
        this.messageId = messageId;
        this.attachmentId = attachmentId;
    }

    public Long getMessageId() {
        return messageId;
    }

    public Long getAttachmentId() {
        return attachmentId;
    }

    @Override
    public String toString() {
        return "PostProcessQueueMessage{" +
                "messageId=" + messageId +
                ", attachmentId=" + attachmentId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PostProcessQueueMessage that = (PostProcessQueueMessage) o;

        if (attachmentId != null ? !attachmentId.equals(that.attachmentId) : that.attachmentId != null) return false;
        if (messageId != null ? !messageId.equals(that.messageId) : that.messageId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = messageId != null ? messageId.hashCode() : 0;
        result = 31 * result + (attachmentId != null ? attachmentId.hashCode() : 0);
        return result;
    }
}

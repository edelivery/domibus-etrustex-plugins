package eu.europa.ec.etrustex.adapter.persistence.api.attachment;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentDirection;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxError;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBase;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface AttachmentDao extends DaoBase<EtxAttachment> {

    /**
     * Count the number of {@link EtxAttachment}
     * with {@link EtxMessage#id} of {@link EtxAttachment#message} = {@param messageId}
     * and {@link EtxAttachment#activeState}    = {@code true}
     * and {@link EtxAttachment#stateType}      NOT in {@param stateList}
     *
     * @param messageId Id of the {@link EtxMessage}
     * @param stateList {@link List} of {@link AttachmentState} to exclude from the search
     * @return count of {@link EtxAttachment}
     */
    Long countMessageAttachmentsNotInStateList(Long messageId, List<AttachmentState> stateList);

    /**
     * Count the number of {@link EtxAttachment}
     * with {@link EtxMessage#id} of {@link EtxAttachment#message} = {@param messageId}
     * and {@link EtxAttachment#activeState}    = {@code true}
     * and {@link EtxAttachment#stateType}      in {@param stateList}
     *
     * @param messageId Id of the {@link EtxMessage}
     * @param stateList {@link List} of {@link AttachmentState} to exclude from the search
     * @return count of {@link EtxAttachment}
     */
    Long countMessageAttachmentsInStateList(Long messageId, List<AttachmentState> stateList);

    /**
     * Count the number of {@link EtxAttachment}
     * with {@link EtxAttachment#attachmentUuid}    = {@param attachmentUUID}
     * and {@link EtxAttachment#directionType}      = {@link AttachmentDirection#OUTGOING}
     * and {@link EtxAttachment#activeState}        = {@code true}
     * and {@link EtxAttachment#stateType}          in {@param stateList}
     * <p>
     * and {@link EtxAttachment#message}->
     * {@link EtxMessage#sender}->
     * {@link EtxParty#system}-> = {@param senderSystemId}
     *
     * @param attachmentUUID      UUID of the {@link EtxAttachment}
     * @param attachmentStateList {@link List} of {@link AttachmentState} to exclude from the search
     * @param senderSystemId      Id of the {@link EtxSystem}
     * @return count of {@link EtxAttachment}
     */
    Long countAttachmentsBySystemAndUUIDAndState(String attachmentUUID, List<AttachmentState> attachmentStateList, Long senderSystemId);

    /**
     * Pageable list of {@link EtxAttachment} to expire at a given date
     * (i.e. {@link EtxAttachment#creationDate} < {@param endDate})
     *
     * @param endDate  to compare the {@link EtxAttachment#creationDate} with
     * @param pageSize size of the page
     * @param index    of the page (starting 0)
     * @return a list of {@link EtxAttachment} with {@link EtxAttachment#creationDate} < {@param endDate}
     */
    List<EtxAttachment> getNextAttachmentsToExpire(Date endDate, int pageSize, int index);

    /**
     * Count the number of {@link EtxAttachment} to expire at a given date
     * (i.e. {@link EtxAttachment#creationDate} < {@param endDate})
     *
     * @param endDate to compare the {@link EtxAttachment#creationDate} with
     * @return the number of {@link EtxAttachment} with {@link EtxAttachment#creationDate} < {@param endDate}
     */
    Long countNextAttachmentsToExpire(Date endDate);

    /**
     * Find {@link EtxAttachment} with {@link EtxAttachment#domibusMessageId} = {@param domibusMessageID}
     *
     * @param domibusMessageID DomibusId of the {@link EtxAttachment}
     * @return {@link EtxAttachment} with given {@param domibusMessageID}
     */
    EtxAttachment findAttachmentByDomibusMessageId(String domibusMessageID);

    /***
     * Update {@link EtxAttachment} that are in the states
     * {@link eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState#ATTS_CREATED}
     * or  {@link eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState#ATTS_DOWNLOADED}
     *
     * @param startDate lower limit of {@link EtxAttachment#modificationDate}
     * @param endDate higher limit of {@link EtxAttachment#modificationDate}
     * @param error {@link eu.europa.ec.etrustex.adapter.model.entity.common.EtxError} to link to the attachments
     * @return number of {@link EtxAttachment} updated
     */
    int updateStuckAttachments(LocalDateTime startDate, LocalDateTime endDate, EtxError error);

    /**
     * Find list of {@link EtxAttachment} filtered by list of {@link EtxAttachment#attachmentUuid}, {@link EtxAttachment#directionType} and {@link EtxAttachment#stateType}
     *
     * @param attachmentUUIDs
     * @param attachmentDirections
     * @param attachmentStates
     * @return List<EtxAttachment>
     */
    List<EtxAttachment> findAttachmentsByUUIDsDirectionState(List<String> attachmentUUIDs, List<AttachmentDirection> attachmentDirections, List<AttachmentState> attachmentStates);

}

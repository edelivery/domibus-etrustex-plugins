package eu.europa.ec.etrustex.adapter.application.passwordencryption;

import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.services.DomainContextExtService;
import eu.domibus.ext.services.DomibusConfigurationExtService;
import eu.domibus.ext.services.PasswordEncryptionExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.plugin.encryption.PluginPropertyEncryptionListener;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPlugin;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPluginService;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxUser;
import eu.europa.ec.etrustex.adapter.service.security.BackendUserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service to listen if Domibus is configured to encrypt passwords and implements the password encryption
 * for the passwords of the eTrustExBackendPlugin on receipt of the signal.
 * Passwords to be encrypted are:
 * <ol>
 *     <li>in table ETX_ADT_USER for backend user credentials</li>
 *     <li>in domibus-etrustex-backend-plugin.properties for credentials with which the plugin submits to Domibus</li>
 * </ol>
 *
 * @author Arun Raj
 * @version 1.0
 * @since 21/11/2019
 */
@Service("etxBackendPluginPasswordEncryptionListener")
public class ETrustExBackendPluginPasswordEncryptionListener implements PluginPropertyEncryptionListener {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ETrustExBackendPluginPasswordEncryptionListener.class);

    private ETrustExBackendPluginProperties etxBackendPluginProperties;
    private DomainContextExtService domainContextExtService;
    private PasswordEncryptionExtService pluginPasswordEncryptionService;
    private DomibusConfigurationExtService domibusConfigurationExtService;
    private ApplicationContext context;
    private DomainBackEndPluginService domainBackEndPluginService;

    public ETrustExBackendPluginPasswordEncryptionListener(
            @Lazy
            @Qualifier("etxBackendPluginProperties") ETrustExBackendPluginProperties etxBackendPluginProperties,
            DomainContextExtService domainContextExtService,
            PasswordEncryptionExtService pluginPasswordEncryptionService,
            DomibusConfigurationExtService domibusConfigurationExtService,
            ApplicationContext context,
            DomainBackEndPluginService domainBackEndPluginService) {
        this.etxBackendPluginProperties = etxBackendPluginProperties;
        this.domainContextExtService = domainContextExtService;
        this.pluginPasswordEncryptionService = pluginPasswordEncryptionService;
        this.domibusConfigurationExtService = domibusConfigurationExtService;
        this.context = context;
        this.domainBackEndPluginService = domainBackEndPluginService;
    }

    @Override
    @DomainBackEndPlugin
    public void encryptPasswords() {
        domainBackEndPluginService.setBackendPluginDomain();
        boolean passwordEncryptionActiveDomibus = etxBackendPluginProperties.isPasswordEncryptionActiveDomibus(domainContextExtService.getCurrentDomainSafely());
        LOG.info("Password Encryption: active in Domibus? [{}]", passwordEncryptionActiveDomibus);

        if (!passwordEncryptionActiveDomibus) {
            LOG.info("eTrustEx Backend Plugin: No password encryption will be performed." +
                    "eTrustEx Backend Plugin depends on Domibus password encryption to encrypt its passwords." +
                    "(Refer Domibus Admin guide, section '5.2 Domibus Properties' for property domibus.password.encryption.active in the file domibus.properties).");
            return;
        }

        LOG.info("eTrustEx Backend Plugin: Encrypting passwords");

        DomainDTO domain = domainContextExtService.getCurrentDomainSafely();
        pluginPasswordEncryptionService.encryptPasswordsInFile(
                new ETrustExBackendPluginPasswordEncryptionContext(
                        etxBackendPluginProperties,
                        pluginPasswordEncryptionService,
                        domibusConfigurationExtService.getConfigLocation(),
                        domain));

        encryptPasswordsInDatabase(domain);

        LOG.info("eTrustEx Backend Plugin: Finished encrypting passwords");
    }

    protected void encryptPasswordsInDatabase(DomainDTO domain) {
        // Access of BackendUserService cannot be accessed on Autowired
        // TODO: 13/12/2019 FGA Add autowire on BackendUserService after the implementation of the story EDELIVERY-6080
        BackendUserService backendUserService = context.getBean(BackendUserService.class);
        List<EtxUser> allBackEndUsers = backendUserService.getAllBackEndUsers();
        for (EtxUser user : allBackEndUsers) {
            if (!pluginPasswordEncryptionService.isValueEncrypted(user.getPassword())) {
                user.setPassword(getFormattedBase64EncryptedValue(domain, user));
                backendUserService.updateUser(user);
                LOG.debug("eTrustEx Backend Plugin: password in the database for user:[{}] is encrypted");
            }
        }
    }

    private String getFormattedBase64EncryptedValue(DomainDTO domain, EtxUser u) {
        return pluginPasswordEncryptionService.encryptProperty(domain, u.getId() + u.getName(), u.getPassword())
                .getFormattedBase64EncryptedValue();
    }
}

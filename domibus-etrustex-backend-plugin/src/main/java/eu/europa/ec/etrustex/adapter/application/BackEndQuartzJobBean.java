package eu.europa.ec.etrustex.adapter.application;

import eu.europa.ec.etrustex.adapter.domain.DomainInitializerBackEnd;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.common.jobs.DomainInitializer;
import eu.europa.ec.etrustex.common.jobs.QuartzJobBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;

import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

public abstract class BackEndQuartzJobBean extends QuartzJobBean {

    @Autowired
    @Lazy
    @Qualifier("etxBackendPluginProperties")
    private ETrustExBackendPluginProperties properties;

    @Autowired
    private DomainInitializerBackEnd domainInitializerBackEnd;

    protected DomainInitializer getDomainInitializer() {
        return domainInitializerBackEnd;
    }

    protected boolean isAllowedToRun(String domainName){
        return properties.getDomain() == null || equalsIgnoreCase(properties.getDomain(), domainName);
    }
}

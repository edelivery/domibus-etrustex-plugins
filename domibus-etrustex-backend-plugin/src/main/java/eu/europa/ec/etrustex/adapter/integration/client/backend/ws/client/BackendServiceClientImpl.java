package eu.europa.ec.etrustex.adapter.integration.client.backend.ws.client;

import eu.domibus.ext.services.DomainContextExtService;
import eu.domibus.ext.services.PasswordEncryptionExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.client.backend.BackendServiceClient;
import eu.europa.ec.etrustex.adapter.integration.client.backend.ws.BackendWebServiceBase;
import eu.europa.ec.etrustex.adapter.integration.client.backend.ws.adapter.*;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxUser;
import eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.model.vo.FileAttachmentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Map;

/**
 * @author micleva
 * @project ETX
 */

@Service("etxBackendPluginServiceWithWebService")
public class BackendServiceClientImpl implements BackendServiceClient {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(BackendServiceClientImpl.class);

    @Autowired
    private DomainContextExtService domainContextExtService;

    @Autowired
    private PasswordEncryptionExtService passwordEncryptionExtService;

    @Autowired
    private BackendWebServiceProvider webServiceProvider;

    @Override
    public FileAttachmentVO downloadAttachment(Map<SystemConfigurationProperty, String> systemConfig, EtxMessage messageBundle, EtxAttachment etxAttachment) {
        LOG.debug("downloadAttachment: {}", etxAttachment);

        FileRepositoryServiceAdapter fileRepositoryService = new FileRepositoryServiceAdapterImpl(webServiceProvider);

        setBackendConnectionDetails(messageBundle.getSender(), (BackendWebServiceBase) fileRepositoryService,
                systemConfig, SystemConfigurationProperty.ETX_BACKEND_SERVICE_FILE_REPOSITORY_URL);

        return fileRepositoryService.downloadAttachment(messageBundle, etxAttachment);
    }

    @Override
    public void uploadAttachment(Map<SystemConfigurationProperty, String> systemConfig, EtxMessage messageBundle, EtxAttachment etxAttachment, File attachmentFile) {
        LOG.debug("uploadAttachment: ");

        FileRepositoryServiceAdapter fileRepositoryService = new FileRepositoryServiceAdapterImpl(webServiceProvider);
        setBackendConnectionDetails(messageBundle.getReceiver(), (BackendWebServiceBase) fileRepositoryService,
                systemConfig, SystemConfigurationProperty.ETX_BACKEND_SERVICE_FILE_REPOSITORY_URL);

        fileRepositoryService.uploadAttachment(messageBundle, etxAttachment, attachmentFile);
    }

    @Override
    public void notifyMessageBundle(Map<SystemConfigurationProperty, String> systemConfig, EtxMessage messageBundle) {
        LOG.debug("notifyMessageBundle: {}", messageBundle);

        InboxNotificationServiceAdapter inboxNotificationService = new InboxNotificationServiceAdapterImpl(webServiceProvider);
        setBackendConnectionDetails(messageBundle.getReceiver(), (BackendWebServiceBase) inboxNotificationService,
                systemConfig, SystemConfigurationProperty.ETX_BACKEND_SERVICE_INBOX_NOTIFICATION_URL);

        inboxNotificationService.notifyMessageBundle(messageBundle);
    }

    @Override
    public void notifyMessageStatus(Map<SystemConfigurationProperty, String> systemConfig, EtxMessage messageStatus, MessageType refMessageType) {
        LOG.debug("notifyMessageStatus: {}", messageStatus);

        InboxNotificationServiceAdapter inboxNotificationService = new InboxNotificationServiceAdapterImpl(webServiceProvider);
        setBackendConnectionDetails(messageStatus.getReceiver(), (BackendWebServiceBase) inboxNotificationService,
                systemConfig, SystemConfigurationProperty.ETX_BACKEND_SERVICE_INBOX_NOTIFICATION_URL);

        inboxNotificationService.notifyMessageStatus(messageStatus, refMessageType);
    }

    private void setBackendConnectionDetails(EtxParty etxParty, BackendWebServiceBase backendWebServiceBase,
                                             Map<SystemConfigurationProperty, String> systemConfig, SystemConfigurationProperty serviceName) {
        EtxUser backendUser = etxParty.getSystem().getBackendUser();
        backendWebServiceBase.setUserName(backendUser.getName());
        backendWebServiceBase.setPassword(passwordEncryptionExtService.decryptProperty(
                domainContextExtService.getCurrentDomain(),
                backendUser.getId() + backendUser.getName(),
                backendUser.getPassword()));

        backendWebServiceBase.setServiceEndpoint(systemConfig.get(serviceName));
    }
}

package eu.europa.ec.etrustex.adapter.queue.out.uploadattachment;

import eu.europa.ec.etrustex.common.jms.QueueMessage;

/**
 * @author micleva
 * @project ETX
 */

public class OutUploadAttachmentQueueMessage extends QueueMessage {

    private static final long serialVersionUID = 2437593253368789754L;

    private Long attachmentId;

    public Long getAttachmentId() {
        return this.attachmentId;
    }

    public void setAttachmentId(Long attachmentId) {
        this.attachmentId = attachmentId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("OutUploadAttachmentQueueMessage");
        sb.append("{attachmentId=").append(attachmentId);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OutUploadAttachmentQueueMessage that = (OutUploadAttachmentQueueMessage) o;

        if (!attachmentId.equals(that.attachmentId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return attachmentId.hashCode();
    }
}

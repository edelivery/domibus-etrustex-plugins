package eu.europa.ec.etrustex.adapter.integration.handlers;

import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import org.apache.commons.lang3.StringUtils;

/**
 * Generic class to handle a response fault
 *
 * @author François Gautier
 * @since 26-Sep-17
 * @version 1.0
 */
public abstract class ResponseFaultHandlerBase implements OperationHandler {


    @Override
    public boolean handle(ETrustExAdapterDTO eTrustExAdapterResponseDTO) throws ETrustExPluginException {
        /*Extract Payloads*/
        PayloadDTO payloadFromCID = eTrustExAdapterResponseDTO.getPayloadFromCID(ContentId.CONTENT_FAULT);
        if (payloadFromCID == null || StringUtils.isBlank(payloadFromCID.getPayloadAsString())) {
            throw new ETrustExPluginException(ETrustExError.ETX_002, "No payload provided!");
        }

        process(payloadFromCID.getPayloadAsString(), eTrustExAdapterResponseDTO);

        return true;
    }

    protected abstract void process(String payload, ETrustExAdapterDTO eTrustExAdapterDTO);
}

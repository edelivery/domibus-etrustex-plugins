package eu.europa.ec.etrustex.adapter.service;

import ec.schema.xsd.commonaggregatecomponents_2.InterchangeAgreementType;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.NodeInvocationManager;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.converters.NodeRetrieveInterchangeAgreementsResponseConverter;
import eu.europa.ec.etrustex.adapter.model.dto.IcaDTO;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.ica.EtxIca;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.PartyDao;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.SystemDao;
import eu.europa.ec.etrustex.adapter.persistence.api.ica.IcaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static eu.europa.ec.etrustex.common.exceptions.EtrustExMarker.NON_RECOVERABLE;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.serializeObjToXML;
import static eu.europa.ec.etrustex.common.util.Validate.notBlank;
import static eu.europa.ec.etrustex.common.util.Validate.notNull;

/**
 * Implementation of interface to manage ICAs information & cache reloading
 *
 * @author Francois Gautier
 * @version 1.1
 * @since 10/12/2018
 */

@Service("etxBackendPluginIcaService")
public class IcaServiceImpl implements IcaService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(IcaServiceImpl.class);

    @Autowired
    @Lazy
    @Qualifier("etxBackendPluginProperties")
    private ETrustExBackendPluginProperties eTrustExBackendPluginProperties;

    @Autowired
    private NodeInvocationManager nodeInvocationManager;

    @Autowired
    private PartyDao partyDao;
    @Autowired
    private EtxPartyService etxPartyService;
    @Autowired
    private SystemDao systemDao;

    @Autowired
    private IcaDao icaDao;

    /**
     * {@inheritDoc}
     */
    @Transactional
    public List<IcaDTO> reloadIcas() {
        LOG.info("Proceeding to submit request to reload ICAs.");

        List<IcaDTO> partiesRequested = new ArrayList<>();
        List<EtxParty> all = partyDao.findAll();
        for (EtxParty etxParty : all) {
            if (etxParty.getSystem() != null && etxParty.getSystem().getSystemConfigList().size() > 1) {
                IcaDTO icaDTO = new IcaDTO(etxParty);
                partiesRequested.add(icaDTO);
                try {
                    LOG.info("Submitting reloadICAs for party:" + etxParty.getPartyUuid());
                    nodeInvocationManager.sendInterchangeAgreementRequest(etxParty);
                } catch (Exception e) {
                    LOG.error("Error in one of the retrieve ICA (on " + all.size() + "calls)", e);
                    icaDTO.setErrorDetails("Error: " + e.getMessage());
                }
            }
        }
        LOG.info("End of submit request to reload ICAs.");
        return partiesRequested;
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    public List<EtxIca> getIcas() {
        LOG.info("Proceeding to submit request to retrieve ICAs.");
        List<EtxIca> all = icaDao.findAll();
        LOG.info("End of submit request to retrieve ICAs.");
        return all;
    }

    /**
     * {@inheritDoc}
     */
    public void createIca(String senderUuid, String receiverUuid, InterchangeAgreementType interchangeAgreementType) {
        LOG.info("Request to create ICA for sender [{}] and receiver [{}]", senderUuid, receiverUuid);
        List<EtxIca> ica = icaDao.findIca(senderUuid, receiverUuid);
        for (EtxIca etxIca : ica) {
            icaDao.remove(etxIca);
        }
        EtxIca newIca = new EtxIca();
        newIca.setXml(serializeObjToXML(NodeRetrieveInterchangeAgreementsResponseConverter.convert(interchangeAgreementType)));
        newIca.setSender(getParty(senderUuid));
        newIca.setReceiver(getOrCreateEtxParty(receiverUuid));

        if (newIca.getSender() != null) {
            icaDao.save(newIca);
            LOG.info("ICA created for sender [{}] and receiver [{}]", senderUuid, receiverUuid);
        }
    }

    private EtxParty getOrCreateEtxParty(String receiverUuid) {
        EtxParty receiver = getParty(receiverUuid);
        if (receiver == null) {
            EtxParty instance = new EtxParty();
            instance.setPartyUuid(receiverUuid);
            String sysDefault = eTrustExBackendPluginProperties.getSystemDefaultName();
            notBlank(sysDefault, "etx.adapter.backend.defaultThirdPartySystem is not set in domibus-etrustex-backendPlugin.properties ");
            EtxSystem system = systemDao.findSystemByName(sysDefault);
            notNull(system, "System " + sysDefault + " not found.");
            instance.setSystem(system);
            receiver = etxPartyService.create(instance);
        }
        return receiver;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EtxIca getIca(EtxParty senderParty, EtxParty receiverParty) {
        if (senderParty == null || receiverParty == null) {
            return null;
        }
        List<EtxIca> ica = icaDao.findIca(senderParty.getPartyUuid(), receiverParty.getPartyUuid());
        if (ica == null || ica.isEmpty()) {
            return null;
        } else if (ica.size() > 1) {
            LOG.error("There is more than one ICA for sender UUID [{}] and receiver UUID [{}]", senderParty.getPartyUuid(), senderParty.getPartyUuid());
        }
        return ica.get(0);
    }

    private EtxParty getParty(String uuid) {
        EtxParty byPartyUUID = partyDao.findByPartyUUID(uuid);
        if (byPartyUUID == null) {
            LOG.error(NON_RECOVERABLE, "Party with Uuid [{}] not found for ICA reload. " +
                    "Make sure the party exists in the Database", uuid);
        }
        return byPartyUUID;
    }

}

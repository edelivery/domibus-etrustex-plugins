package eu.europa.ec.etrustex.adapter.integration.client.etxnode.converters;

import ec.schema.xsd.commonaggregatecomponents_2.CertificateType;
import ec.schema.xsd.commonaggregatecomponents_2.InterchangeAgreementType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NodeRetrieveInterchangeAgreementsResponseConverter {

    private NodeRetrieveInterchangeAgreementsResponseConverter() {
    }

    public static eu.europa.ec.etrustex.integration.model.common.v2.InterchangeAgreementType convert(InterchangeAgreementType interchangeAgreementType){
        eu.europa.ec.etrustex.integration.model.common.v2.InterchangeAgreementType result = new eu.europa.ec.etrustex.integration.model.common.v2.InterchangeAgreementType();

        if (interchangeAgreementType.getSecurityInformation() != null) {
            result.setAvailabilityLevelCode(interchangeAgreementType.getSecurityInformation().getAvailabilityLevelCode().getValue());
            result.setConfidentialityLevelCode(interchangeAgreementType.getSecurityInformation().getConfidentialityLevelCode().getValue());
            result.setIntegrityLevelCode(interchangeAgreementType.getSecurityInformation().getIntegrityLevelCode().getValue());

            List<CertificateType> certificateList = interchangeAgreementType.getSecurityInformation().getReceiverPartyCertificate();
            List<eu.europa.ec.etrustex.integration.model.common.v2.CertificateType> certificateTypeList = new ArrayList<>();
            for (CertificateType certificateType : certificateList) {
                eu.europa.ec.etrustex.integration.model.common.v2.CertificateType receiverCertificateType = toCertificateSDO(certificateType);
                certificateTypeList.add(receiverCertificateType);
            }
            result.getReceiverPartyCertificate().addAll(certificateTypeList);
        }
        return result;
    }

    private static eu.europa.ec.etrustex.integration.model.common.v2.CertificateType toCertificateSDO(CertificateType certificateType) {
        eu.europa.ec.etrustex.integration.model.common.v2.CertificateType result =
                new eu.europa.ec.etrustex.integration.model.common.v2.CertificateType();

        result.setKeyUsageCode(certificateType.getKeyUsageCode().getValue());
        byte[] array = certificateType.getX509Certificate().getValue();
        result.setX509Certificate(Arrays.copyOf(array, array.length));
        if (certificateType.getX509CRL() != null) {
            array = certificateType.getX509CRL().getValue();
            result.setX509CRL(Arrays.copyOf(array, array.length));
        }
        if (certificateType.getX509SubjectName() != null) {
            result.setX509SubjectName(certificateType.getX509SubjectName().getValue());
        }
        return result;
    }


}

package eu.europa.ec.etrustex.adapter.integration.converters;

import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.integration.model.common.v2.ErrorType;

public class ErrorTypeConverter {

    private ErrorTypeConverter() {
    }

    public static ErrorType buildErrorType(ETrustExPluginException epEx) {
        return buildErrorType(epEx.getError().getCode(), epEx.getError().getDescription(), epEx.getMessage());
    }

    private static ErrorType buildErrorType(String errorCode, String description, String details) {
        ErrorType error = new ErrorType();

        error.setCode(errorCode);
        error.setDescription(description);
        error.setDetail(details);

        return error;
    }

}

package eu.europa.ec.etrustex.adapter.domain;

import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.services.DomainContextExtService;
import eu.domibus.ext.services.DomainExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import static eu.domibus.logging.DomibusLogger.MDC_DOMAIN;

/**
 * Service responsible for setting the domain MDC keys to eTrustEx Backend Plugin domain in multi-tenant environment.
 *
 * @author Francois Gautier
 * @since 1.2
 */
@Service
public class DomainBackEndPluginService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DomainBackEndPluginService.class);

    @Autowired
    private DomainContextExtService domainContextExtService;

    @Autowired
    private DomainExtService domainExtService;

    @Autowired
    @Lazy
    @Qualifier("etxBackendPluginProperties")
    private ETrustExBackendPluginProperties eTrustExBackendPluginProperties;

    public void setBackendPluginDomain() {
        if (StringUtils.isNotBlank(eTrustExBackendPluginProperties.getDomain())) {
            DomainDTO domain = domainExtService.getDomain(eTrustExBackendPluginProperties.getDomain());
            domainContextExtService.setCurrentDomain(domain);
            LOG.debug("Set domain to {}", eTrustExBackendPluginProperties.getDomain());
            LOG.putMDC(MDC_DOMAIN, domain.getCode());
        }
    }
}
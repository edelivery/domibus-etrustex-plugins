package eu.europa.ec.etrustex.adapter.queue.out.sendmessage;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.queue.AbstractQueueListener;
import org.springframework.stereotype.Component;

/**
 * @author micleva
 * @project ETX
 */
@Component("etxBackendPluginSendMessageConsumer")
public class SendMessageConsumer extends AbstractQueueListener<SendMessageQueueMessage> {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SendMessageConsumer.class);

    private final SendMessageHandler handler;

    public SendMessageConsumer(SendMessageHandler handler) {
        this.handler = handler;
    }

    @Override
    public void handleMessage(SendMessageQueueMessage queueMessage) {
        if (queueMessage.getType().equals(SendMessageQueueMessage.Type.BUNDLE)) {
            handler.handleSendBundle(queueMessage.getMessageId());
        } else if (queueMessage.getType().equals(SendMessageQueueMessage.Type.STATUS)) {
            handler.handleSendStatus(queueMessage.getMessageId());
        } else {
            // unknown message type, log error & drop message
            LOG.error("skip processing: {}", queueMessage);
        }
    }

    @Override
    public void validateMessage(SendMessageQueueMessage queueMessage) {
        if (queueMessage.getType().equals(SendMessageQueueMessage.Type.BUNDLE)) {
            handler.validateSendBundle(queueMessage.getMessageId());
        } else if (queueMessage.getType().equals(SendMessageQueueMessage.Type.STATUS)) {
            handler.validateSendStatus(queueMessage.getMessageId());
        }
    }
}

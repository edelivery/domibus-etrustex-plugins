package eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter;

import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.schema.xsd.retrieveinterchangeagreementsrequest_2.RetrieveInterchangeAgreementsRequestType;
import ec.services.wsdl.retrieveinterchangeagreementsrequest_2.SubmitRetrieveInterchangeAgreementsRequestRequest;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.common.converters.node.NodePartyConverter;
import eu.europa.ec.etrustex.common.exceptions.DomibusSubmissionException;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.util.ConversationIdKey;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.ws.Holder;
import java.util.UUID;

/**
 * This class is responsible for the creation and submission of a DTO object to the ETX backend Domibus connector.
 * Since it handles asynch requests it uses a JMS template with receiveSelected() in order to wait for the AS4 response coming from the ETX Node plugin.
 *
 * @author Federico Martini
 * @version 1.0
 * @since 16/06/2017
 */
//TODO:EDELIVERY-6608 - Remove ETX BE Plugin NodeInvocationManager facade & NodeServiceAdapters & refactor code functionally

@Service("etxBackendPluginNodeRetrieveInterchangeAgreementsServiceAdapter")
public class NodeRetrieveInterchangeAgreementsServiceAdapter extends NodeServicesAdapterBase {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(NodeRetrieveInterchangeAgreementsServiceAdapter.class);

    /**
     * Formulate the retrieveICA request and submit request to Domibus.
     *
     * @param senderParty       party of the sender
     * @return The UUID created internally and assigned for this message
     * @throws ETrustExPluginException thrown in case anything goes wrong
     */
    @Transactional
    public String sendInterchangeAgreementRequest(EtxParty senderParty) {

        LOG.debug("SendInterchangeAgreementRequest start ...");

        return sendMessage(senderParty);

    }


    /**
     * Send message to Domibus to retrieve the Interchange Agreements
     *
     * @param senderParty       party of the sender
     * @return messageId of the EtxMessage
     */
    private String sendMessage(EtxParty senderParty) {

        try {
            // Fill in the Authorisation Header
            Holder<HeaderType> authorisationHeaderHolder = fillAuthorisationHeader(senderParty.getPartyUuid(), null);

            // Prepare WS request
            SubmitRetrieveInterchangeAgreementsRequestRequest request = new SubmitRetrieveInterchangeAgreementsRequestRequest();

            RetrieveInterchangeAgreementsRequestType icaRequest = new RetrieveInterchangeAgreementsRequestType();
            icaRequest.setReceiverParty(null);
            icaRequest.setSenderParty(NodePartyConverter.buildPartyType(senderParty.getPartyUuid()));
            request.setRetrieveInterchangeAgreementsRequest(icaRequest);

            ETrustExAdapterDTO eTrustExAdapterDTO = populateETrustExAdapterDTOForRetrieveICARequest(authorisationHeaderHolder, request, senderParty);
            backendConnectorSubmissionService.submit(eTrustExAdapterDTO);
            LOG.info("For sender party UUID:[{}], Submitted RetrieveInterChangeAgreements request message to backend connector.", senderParty.getPartyUuid());
            return eTrustExAdapterDTO.getEtxBackendMessageId();
        } catch (DomibusSubmissionException | ETrustExPluginException etEx) {
            LOG.error("For sender party UUID:[{}], Exception encoutered when submitting RetrieveInterChangeAgreements request message to backend connector.", senderParty.getPartyUuid());
            throw etEx;
        } catch (Exception ex) {
            LOG.error("For sender party UUID:[{}], Exception encoutered when submitting RetrieveInterChangeAgreements request message to backend connector.", senderParty.getPartyUuid());
            throw new ETrustExPluginException(ETrustExError.DEFAULT, ex.getMessage(), ex);
        }
    }



    private ETrustExAdapterDTO populateETrustExAdapterDTOForRetrieveICARequest(Holder<HeaderType> authorisationHeaderHolder, SubmitRetrieveInterchangeAgreementsRequestRequest request, EtxParty senderParty) {

        ETrustExAdapterDTO etxAdapterDTO = createETrustExAdapterDTOWithCommonDataForDomibusSubmission(senderParty, 0);
        String conversationId = ConversationIdKey.RETRIEVE_ICA.getConversationIdKey() + UUID.randomUUID();
        etxAdapterDTO.setEtxBackendMessageId(conversationId);
        etxAdapterDTO.setAs4ConversationId(conversationId);

        etxAdapterDTO.setAs4Service(eTrustExBackendPluginProperties.getAs4_Service_RetrieveICA());
        etxAdapterDTO.setAs4ServiceType(eTrustExBackendPluginProperties.getAs4_Service_RetrieveICA_ServiceType());
        etxAdapterDTO.setAs4Action(eTrustExBackendPluginProperties.getAs4_Action_RetrieveICA_Request());

        String strETrustExHeaderXML = TransformerUtils.serializeObjToXML(authorisationHeaderHolder.value);
        LOG.debug("Header Payload XML: {}", strETrustExHeaderXML);
        PayloadDTO headerPayloadDTO = TransformerUtils.getHeaderPayloadDTO(strETrustExHeaderXML);
        etxAdapterDTO.addAs4Payload(headerPayloadDTO);

        String retrieveInterchangeAgreementsRequestXML = TransformerUtils.serializeObjToXML(request);
        LOG.debug("Generic Payload XML: {}", retrieveInterchangeAgreementsRequestXML);
        PayloadDTO retrieveInterchangeAgreementsRequestDTO = TransformerUtils.getGenericPayloadDTO(retrieveInterchangeAgreementsRequestXML);
        etxAdapterDTO.addAs4Payload(retrieveInterchangeAgreementsRequestDTO);

        return etxAdapterDTO;
    }
}

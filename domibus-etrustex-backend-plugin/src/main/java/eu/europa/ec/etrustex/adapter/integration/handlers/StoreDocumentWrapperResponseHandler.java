package eu.europa.ec.etrustex.adapter.integration.handlers;

import ec.services.wsdl.documentwrapper_2.FaultResponse;
import ec.services.wsdl.documentwrapper_2.StoreDocumentWrapperResponse;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.common.FaultResponseCode;
import eu.europa.ec.etrustex.adapter.queue.postupload.PostUploadProducer;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static eu.europa.ec.etrustex.adapter.integration.converters.FaultTypeConverter.getFaultInfo;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.extractETrustExXMLPayload;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.extractETrustExXMLPayloadFaultResponse;

/**
 * @author Arun Raj
 * @version 1.0
 */
@Service("etxBackendPluginStoreDocumentWrapperResponseHandler")
public class StoreDocumentWrapperResponseHandler extends BackendResponseHandlerBase<StoreDocumentWrapperResponse, EtxAttachment, FaultResponse> implements OperationHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(StoreDocumentWrapperResponseHandler.class);

    @Autowired
    private PostUploadProducer postUploadProducer;

    @Autowired
    private AttachmentService attachmentService;

    @Override
    public boolean handle(ETrustExAdapterDTO eTrustExAdapterResponseDTO) {

        LOG.info("Reached StoreDocumentWrapperResponseHandler");

        /*Extract Payloads*/
        StoreDocumentWrapperResponse response = extractETrustExXMLPayload(ContentId.CONTENT_ID_GENERIC, eTrustExAdapterResponseDTO);
        FaultResponse faultResponse = extractETrustExXMLPayloadFaultResponse(eTrustExAdapterResponseDTO, new FaultResponse("Default FaultResponse", getFaultInfo()));

        if (response == null && faultResponse == null) {
            throw new ETrustExPluginException(ETrustExError.ETX_002, "Either StoreDocumentWrapperResponse or FaultResponse should be provided!");
        }

        EtxAttachment etxAttachment = attachmentService.findAttachmentByDomibusMessageId(eTrustExAdapterResponseDTO.getAs4RefToMessageId());

        LOG.info("Processing StoreDocumentWrapper response message sent by ETrustEx Node Plugin for Attachment");

        processNodeResponse(faultResponse, response, etxAttachment);

        postUploadProducer.checkAttachmentsAndTriggerPostUpload(etxAttachment);
        return true;
    }

    @Override
    protected void processNodeFault(FaultResponse faultResponse, EtxAttachment etxAttach) {
        String serverErrorCode = getNodeFaultResponseCode(faultResponse.getFaultInfo());
        if (serverErrorCode.equals(FaultResponseCode.ERROR_DUPLICATE.getCode())) {
            LOG.warn("Attachment with ID {} already uploaded! Continue process as wrapper upload success... ", etxAttach.getAttachmentUuid());
            attachmentService.updateAttachmentState(etxAttach, AttachmentState.ATTS_DOWNLOADED, AttachmentState.ATTS_UPLOADED);
        } else {
            String errorDescription = faultResponse.getMessage();
            String errorLog = String.format("Failed to upload attachment Response Code from Node: %s ; Error Details: %s",
                    serverErrorCode,
                    errorDescription);
            LOG.error(errorLog);
            attachmentService.attachmentInError(etxAttach.getId(), serverErrorCode, errorLog);
        }
    }

    @Override
    protected void processNodeSuccess(StoreDocumentWrapperResponse response, EtxAttachment etxAttach) {
        LOG.info("Attachment successfully uploaded in Node.");
        attachmentService.updateAttachmentState(etxAttach, AttachmentState.ATTS_DOWNLOADED, AttachmentState.ATTS_UPLOADED);
    }
}

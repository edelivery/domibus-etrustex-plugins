package eu.europa.ec.etrustex.adapter.web;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.dto.IcaDTO;
import eu.europa.ec.etrustex.adapter.model.entity.ica.EtxIca;
import eu.europa.ec.etrustex.adapter.service.IcaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.StringUtils.*;

/**
 * Controller to expose an URL for administration activities related to backend plugin.
 *
 * @author Arun Raj
 * @version 1.0
 * @since 18/07/2017
 */

@RestController
public class ResetCacheICAController {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ResetCacheICAController.class);
    public static final String RETURN_CARRIAGE = "</br>";

    @Autowired
    protected IcaService icaService;

    /**
     * Reload the cache of backend plugin
     *
     * @return String
     */
    @GetMapping(value = {"/resetIcaCache"})
    @ResponseBody
    @Transactional
    public String handleRequest(HttpServletResponse response) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null &&
                authentication.getAuthorities()
                        .stream()
                        .noneMatch(grantedAuthority -> grantedAuthority.getAuthority().contains("ROLE_ANONYMOUS"))) {
            List<IcaDTO> etxParties = icaService.reloadIcas();

            return "Request to reload cached ICAs has been sent for the parties:" +
                    "</br> </br> <a href=\"resetIcaCache\"> <button>Reset ICA</button> </a>" +
                    "</br> </br> <a href=\"checkIca\">      <button>Check ICA</button> </a>" +
                    RETURN_CARRIAGE + getString(etxParties);
        }
        try {
            response.sendRedirect("login?returnUrl=%2FresetIcaCache");
            return "REDIRECT TO LOGIN";
        } catch (IOException e) {
            throw new IllegalStateException("Could not redirect to login", e);
        }
    }
    /**
     * Retrieve ICAs of backend plugin
     *
     * @return String
     */
    @GetMapping(value = {"/checkIca"})
    @ResponseBody
    @Transactional
    public String checkIca2(HttpServletResponse response) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null &&
                authentication.getAuthorities()
                        .stream()
                        .noneMatch(grantedAuthority -> grantedAuthority.getAuthority().contains("ROLE_ANONYMOUS"))) {
            List<EtxIca> icas = icaService.getIcas();

            return "Request to get ICAs:" +
                    "</br> </br> <a href=\"resetIcaCache\"> <button>Reset ICA</button> </a>" +
                    "</br> </br> <a href=\"checkIca\">      <button>Check ICA</button> </a>" +
                    "</br> </br>" + getIcaString(icas);
        }
        try {
            response.sendRedirect("login?returnUrl=%2FcheckIca");
            return "REDIRECT TO LOGIN";
        } catch (IOException e) {
            throw new IllegalStateException("Could not redirect to login", e);
        }
    }

    private String getIcaString(List<EtxIca> etxIcas) {
        StringBuilder result = new StringBuilder();
        for (EtxIca etxIca : etxIcas) {
            result.append(etxIca.getSender().getPartyUuid()).append("<->").append(etxIca.getReceiver().getPartyUuid()).append(RETURN_CARRIAGE);
        }
        return result.toString();
    }

    private String getString(List<IcaDTO> icas) {
        StringBuilder result = new StringBuilder();
        Set<IcaDTO> inSuccess = icas.stream().filter(t -> isBlank(t.getErrorDetails())).collect(toSet());
        if(!inSuccess.isEmpty()) {
            result.append("</br></br><b>SUCCESS:</b></br>");
        }
        for (IcaDTO etxParty : inSuccess) {
            result.append(etxParty.getParty().getPartyUuid()).append(getDetails(etxParty.getErrorDetails())).append(RETURN_CARRIAGE);
            LOG.info("ICA reload for EtxParty ({}) had been requested.", etxParty.getParty().getPartyUuid());
        }
        Set<IcaDTO> inErrors = icas.stream().filter(t -> isNotBlank(t.getErrorDetails())).collect(toSet());
        if(!inErrors.isEmpty()) {
            result.append("</br></br><b>ERROR:</b></br>");
        }
        for (IcaDTO etxParty : inErrors) {
            result.append(etxParty.getParty().getPartyUuid()).append(getDetails(etxParty.getErrorDetails())).append(RETURN_CARRIAGE);
            LOG.info("ICA reload for EtxParty ({}) had been requested.", etxParty.getParty().getPartyUuid());
        }
        return result.toString();
    }

    private String getDetails(String details) {
        if(isNotBlank(details)) {
            return " - " + details;
        }
        return EMPTY;
    }
}

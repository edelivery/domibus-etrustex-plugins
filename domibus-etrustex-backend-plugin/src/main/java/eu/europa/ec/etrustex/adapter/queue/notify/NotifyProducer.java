package eu.europa.ec.etrustex.adapter.queue.notify;

import eu.europa.ec.etrustex.adapter.queue.JmsProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.JMSException;
import javax.jms.Queue;

@Service("etxBackendPluginNotifyProducer")
public class NotifyProducer {

    @Autowired
    @Qualifier("etxBackendPluginNotifyQueue")
    private Queue jmsQueue;

    @Autowired
    private JmsProducer<NotifyQueueMessage> jmsProducer;

    /**
     * Create a trigger of type {@link NotifyQueueMessage} to {@code}notify{@code} JMS queue.
     *
     * @param messageId ID of ETX_MESSAGE table
     * @throws JMSException in case of error
     */
    @Transactional
    public void triggerNotify(Long messageId) {
        NotifyQueueMessage queueMessage = new NotifyQueueMessage();
        queueMessage.setMessageId(messageId);
        jmsProducer.postMessage(jmsQueue, queueMessage);
    }
}

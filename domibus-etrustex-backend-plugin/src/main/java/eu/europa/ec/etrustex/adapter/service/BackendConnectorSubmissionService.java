package eu.europa.ec.etrustex.adapter.service;

import eu.domibus.ext.services.AuthenticationExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.MessagingProcessingException;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.integration.ETrustExDomibusBackendConnector;
import eu.europa.ec.etrustex.common.exceptions.DomibusSubmissionException;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_CONVERSATION_ID;

/**
 * Service class to submit through the {@link ETrustExDomibusBackendConnector} an AS4 message given a {@link ETrustExAdapterDTO} object.
 * <p>
 * The call to Domibus must be handled in a new transaction in order to be able to rollback only the submit call and
 * the MessagingProcessingException has to be converted to {@link ETrustExPluginException} to avoid the weblogic.transaction.RollbackException.
 *
 * @author Federico Martini
 * @version 1.0
 * @since 21/12/2017
 */
@Service("etxBackendPluginBackendConnectorSubmissionService")
public class BackendConnectorSubmissionService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(BackendConnectorSubmissionService.class);

    @Lazy
    private final ETrustExBackendPluginProperties properties;

    private final AuthenticationExtService authenticationExtService;

    private final ETrustExDomibusBackendConnector eTrustExDomibusBackendConnector;

    public BackendConnectorSubmissionService(@Qualifier("etxBackendPluginProperties") ETrustExBackendPluginProperties properties,
                                             AuthenticationExtService authenticationExtService,
                                             ETrustExDomibusBackendConnector eTrustExDomibusBackendConnector) {
        this.properties = properties;
        this.authenticationExtService = authenticationExtService;
        this.eTrustExDomibusBackendConnector = eTrustExDomibusBackendConnector;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public String submit(ETrustExAdapterDTO dto) {
        LOG.putMDC(MDC_ETX_CONVERSATION_ID, dto.getAs4ConversationId());
        if (SecurityContextHolder.getContext().getAuthentication() == null ||
                !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            authenticationExtService.basicAuthenticate(properties.getUser(), properties.getPwd());
        }

        LOG.debug("Submitting DTO: {}", dto);
        try {
            String submit = eTrustExDomibusBackendConnector.submit(dto);
            LOG.putMDC(DomibusLogger.MDC_MESSAGE_ID, submit);
            return submit;
        } catch (MessagingProcessingException | IllegalArgumentException ex) {
            LOG.error("Message submit to Domibus has failed due to: ", ex);
            throw new DomibusSubmissionException(ex.getMessage(), ex);
        }
    }
}

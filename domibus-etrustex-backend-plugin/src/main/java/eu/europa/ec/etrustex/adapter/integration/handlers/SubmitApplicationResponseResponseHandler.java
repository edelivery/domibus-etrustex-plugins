package eu.europa.ec.etrustex.adapter.integration.handlers;

import ec.services.wsdl.applicationresponse_2.FaultResponse;
import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseResponse;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxErrorBuilder;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.queue.notify.NotifyHandler;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static eu.europa.ec.etrustex.adapter.integration.converters.FaultTypeConverter.getFaultInfo;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.extractETrustExXMLPayload;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.extractETrustExXMLPayloadFaultResponse;

/**
 * @author Arun Raj
 * @version 1.0
 * @since May 2017
 */
@Service("etxBackendPluginSubmitApplicationResponseResponseHandler")
public class SubmitApplicationResponseResponseHandler extends BackendResponseHandlerBase<SubmitApplicationResponseResponse, EtxMessage, FaultResponse> implements OperationHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SubmitApplicationResponseResponseHandler.class);

    @Autowired
    private MessageServicePlugin messageService;

    @Autowired
    private PostProcessProducer postProcessProducer;

    @Autowired
    private NotifyHandler notifyHandler;

    @Override
    public boolean handle(ETrustExAdapterDTO eTrustExAdapterResponseDTO) {

        LOG.info("Reached SubmitApplicationResponseResponseHandler");

        /*Extract Payloads*/
        SubmitApplicationResponseResponse response = extractETrustExXMLPayload(ContentId.CONTENT_ID_GENERIC, eTrustExAdapterResponseDTO);
        FaultResponse faultResponse = extractETrustExXMLPayloadFaultResponse(eTrustExAdapterResponseDTO, new FaultResponse("Default FaultResponse", getFaultInfo()));

        if (response == null && faultResponse == null) {
            throw new ETrustExPluginException(ETrustExError.ETX_002, "Either SubmitApplicationResponseResponse or FaultResponse should be provided!");
        }

        EtxMessage etxMessage = messageService.findMessageByDomibusMessageId(eTrustExAdapterResponseDTO.getAs4RefToMessageId());

        processNodeResponse(faultResponse, response, etxMessage);

        postProcessProducer.triggerPostProcess(etxMessage.getId());

        return true;
    }

    @Override
    protected void processNodeSuccess(SubmitApplicationResponseResponse response, EtxMessage etxMessage) {
        boolean result = response.getAck().getAckIndicator().isValue();
        LOG.info("Received response from NODE for ApplicationResponseService sendMessageStatus with UUID[{}] and response acknowledgement [{}]",
                etxMessage.getMessageUuid(),
                result);
    }

    @Override
    protected void processNodeFault(FaultResponse faultInfo, EtxMessage msgEntity) {
        /*On fault response, the message in adapter DB should be updated to ERROR and the message should be posted to the error queue*/
        String errorDescription = faultInfo.getMessage();
        String faultResponseCode = getNodeFaultResponseCode(faultInfo.getFaultInfo());
        String errorLog = String.format("SubmitApplicationResponse for message with ID %s  returns the fault response code: %s; error details: %s", msgEntity.getId(), faultResponseCode, errorDescription);
        LOG.error(errorLog);
        msgEntity.setError(EtxErrorBuilder.getInstance().withResponseCode(faultResponseCode).withDetail(errorLog).build());
        messageService.updateMessage(msgEntity);
        //For InboxService operations (e.g:getMessageStatus, getMessageBundle), if notification to backend fails, the ETrustEx Node is notified using a sendMessageStatus message.
        //For such a notification message, if Node invocation has failed, the corresponding message of type MESSAGE_ADAPTER_STATUS should be updated with notification expiry status.
        if (MessageType.MESSAGE_ADAPTER_STATUS.equals(msgEntity.getMessageType())) {
            notifyHandler.onError(msgEntity.getId());
        }
    }

}

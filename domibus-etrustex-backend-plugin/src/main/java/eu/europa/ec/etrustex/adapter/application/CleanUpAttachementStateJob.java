package eu.europa.ec.etrustex.adapter.application;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.time.LocalDateTime;

/**
 * Find the expired {@link EtxAttachment} (if too old and non-final state)
 * Change the state to {@link eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState#ATTS_FAILED}
 * Add {@link eu.europa.ec.etrustex.adapter.model.entity.common.EtxError}
 */
@DisallowConcurrentExecution
public class CleanUpAttachementStateJob extends BackEndQuartzJobBean {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(CleanUpAttachementStateJob.class);

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    @Qualifier("etxBackendPluginProperties")
    public ETrustExBackendPluginProperties eTrustExBackendPluginProperties;

    @Override
    public void process(JobExecutionContext jobExecutionContext) {
        LOG.info("### CleanUpAttachementsJob start ###");
        long startTime = System.currentTimeMillis();

        LocalDateTime startDate = eTrustExBackendPluginProperties.getCleanAttachementStateIgnoreBeforeDate();
        LocalDateTime endDate = LocalDateTime.now().minusMinutes(eTrustExBackendPluginProperties.getCleanAttachementStateMinAge());

        LOG.info("### CleanUpAttachementsJob for startDate : [{}] and endDate : [{}] ###", startDate, endDate);

        attachmentService.updateStuckAttachments(startDate, endDate);

        LOG.info("### CleanUpAttachementsJob finished after [{}] sec ###", (double) (System.currentTimeMillis() - startTime) / 1000);
    }

}

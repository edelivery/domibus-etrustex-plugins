package eu.europa.ec.etrustex.adapter.persistence.api.ica;

import eu.europa.ec.etrustex.adapter.model.entity.ica.EtxIca;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBaseImpl;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class IcaDaoImpl extends DaoBaseImpl<EtxIca> implements IcaDao {

    public IcaDaoImpl() {
        super(EtxIca.class);
    }

    @Override
    public List<EtxIca> findIca(String senderUuid, String receiverUuid) {
        TypedQuery<EtxIca> query = getEntityManager().createNamedQuery("EtxIca.findBySenderUUIDAndReceiverUUID", EtxIca.class);

        query.setParameter("senderUuid", senderUuid);
        query.setParameter("receiverUuid", receiverUuid);
        return query.getResultList();
    }
}

package eu.europa.ec.etrustex.adapter.integration.client.backend.ws.adapter;

import eu.europa.ec.etrustex.common.ws.CxfWebServiceProvider;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.xml.ws.BindingProvider;
import java.util.Map;

/**
 * @author micleva
 * @project ETX
 *
 * @author Arun Raj
 * @version 1.0
 */

@Component
public class BackendWebServiceProvider extends CxfWebServiceProvider {

    @Autowired
    @Qualifier("etxBackendPluginProperties")
    ETrustExBackendPluginProperties eTrustExBackendPluginProperties;

    @Override
    public void setupConnectionCredentials(BindingProvider bindingProvider, String username, byte[] password) {
        super.setupConnectionCredentials(bindingProvider, username, password);
        Map<String, Object> map = bindingProvider.getRequestContext();
        map.put(BindingProvider.SOAPACTION_USE_PROPERTY, Boolean.TRUE);
    }

    @Override
    public Object buildJaxWsProxy(Class portType, boolean isMtomEnabled, String serviceEndpoint) {
        Object proxyObject = super.buildJaxWsProxy(portType, isMtomEnabled, serviceEndpoint);
        Client wsClient = ClientProxy.getClient(proxyObject);
        setTimeouts(wsClient);
        return proxyObject;
    }

    private void setTimeouts(Client endpointClient) {
        HTTPClientPolicy policy = getHttpClientPolicy(endpointClient);
        policy.setConnectionTimeout(Long.parseLong(eTrustExBackendPluginProperties.getConnectionTimeout()));
        policy.setReceiveTimeout(Long.parseLong(eTrustExBackendPluginProperties.getReceiveTimeout()));
    }
}

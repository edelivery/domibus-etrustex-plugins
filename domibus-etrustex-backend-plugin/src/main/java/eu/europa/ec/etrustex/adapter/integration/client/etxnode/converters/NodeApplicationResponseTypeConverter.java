package eu.europa.ec.etrustex.adapter.integration.client.etxnode.converters;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.common.StatusResponseCode;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageReferenceState;
import eu.europa.ec.etrustex.common.util.CalendarHelper;
import oasis.names.specification.ubl.schema.xsd.applicationresponse_2.ApplicationResponseType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.DocumentReferenceType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.DocumentResponseType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.ResponseType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.*;

import javax.xml.datatype.DatatypeConfigurationException;
import java.util.Calendar;

import static eu.europa.ec.etrustex.adapter.model.entity.common.StatusResponseCode.fromCode;

/**
 * Utility class for converting {@link ApplicationResponseType} object which is coming from/going to Node into or from
 * Backend {@link EtxMessage} object
 * <p>
 * From legacy Adapter source code
 *
 * @author Valer Micle, Catalin Enache
 * @version 1.0
 * @since 31/10/2017
 */
public class NodeApplicationResponseTypeConverter {

    public static final String MESSAGE_BUNDLE = "BDL";

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(NodeApplicationResponseTypeConverter.class);

    /**
     * private constructor, all methods are static
     */
    private NodeApplicationResponseTypeConverter() {
    }

    public static ApplicationResponseType convertEtxMessage(EtxMessage etxMessage) throws DatatypeConfigurationException {
        ApplicationResponseType sdo = new ApplicationResponseType();

        IDType id = new IDType();
        sdo.setID(id);
        id.setValue(etxMessage.getMessageUuid());

        IssueDateType issueDate = new IssueDateType();
        issueDate.setValue(CalendarHelper.getJodaDateFromCalendar(etxMessage.getIssueDateTime()));
        sdo.setIssueDate(issueDate);

        DocumentResponseType documentResponse = new DocumentResponseType();

        ResponseType response = new ResponseType();

        ReferenceIDType referenceID = new ReferenceIDType();
        String messageReferenceUuid = etxMessage.getMessageReferenceUuid();
        referenceID.setValue(messageReferenceUuid);
        response.setReferenceID(referenceID);

        ResponseCodeType responseCode = new ResponseCodeType();
        switch (etxMessage.getMessageReferenceStateType()) {
            case FAILED:
                responseCode.setValue(StatusResponseCode.BDL4.getCode());
                break;
            case READ:
                responseCode.setValue(StatusResponseCode.BDL7.getCode());
                break;
            default:
                throw new IllegalStateException("Unexpected message reference state: " + etxMessage.getMessageReferenceStateType());
        }
        response.setResponseCode(responseCode);
        documentResponse.setResponse(response);

        DocumentReferenceType documentReference = new DocumentReferenceType();
        IDType refId = new IDType();
        refId.setValue(messageReferenceUuid);
        documentReference.setID(refId);

        DocumentTypeCodeType documentTypeCode = new DocumentTypeCodeType();
        documentTypeCode.setValue(MESSAGE_BUNDLE);//currently we expect only status messages for bundles
        documentReference.setDocumentTypeCode(documentTypeCode);
        documentResponse.setDocumentReference(documentReference);

        sdo.setDocumentResponse(documentResponse);

        return sdo;
    }

    /**
     * At backend level when there is a Node {@link ApplicationResponseType} coming through Domibus
     * We are converting this to {@link EtxMessage} object for saving in DB
     *
     * @param applicationResponse Node original request for status message
     * @return {@link EtxMessage} object for DB saving
     */
    public static EtxMessage toEtxMessage(ApplicationResponseType applicationResponse) {

        if (applicationResponse == null) {
            return null;
        }

        DocumentReferenceType documentReference = null;

        ResponseType response = null;
        String responseCode = null;

        DocumentResponseType documentResponse = applicationResponse.getDocumentResponse();
        if (documentResponse != null) {
            documentReference = documentResponse.getDocumentReference();
            response = documentResponse.getResponse();
            if (response != null) {
                responseCode = response.getResponseCode().getValue();
            }
        }

        EtxMessage messageStatus = new EtxMessage();

        //message uuid
        if (applicationResponse.getID() != null) {
            messageStatus.setMessageUuid(applicationResponse.getID().getValue());
        }

        // Set message reference UUID
        if (documentReference != null && documentReference.getID() != null) {
            messageStatus.setMessageReferenceUuid(documentReference.getID().getValue());
        }

        StatusResponseCode statusResponseCode = fromCode(responseCode);

        // Set message reference business state (FAILED, READ or DELIVERED).
        messageStatus.setStatusReasonCode(responseCode);
        if (statusResponseCode == StatusResponseCode.BDL1) {
            messageStatus.setMessageReferenceStateType(MessageReferenceState.AVAILABLE);
        } else if (statusResponseCode == StatusResponseCode.BDL7) {
            messageStatus.setMessageReferenceStateType(MessageReferenceState.READ);
        } else {
            messageStatus.setMessageReferenceStateType(MessageReferenceState.FAILED);
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("documentReferenceId: [" + messageStatus.getMessageReferenceUuid() + "] and responseCode: [" + responseCode + "] " +
                    "was treated as: [" + messageStatus.getMessageReferenceStateType().name() + "]");
        }
        if (response != null && response.getDescription() != null) {
            StringBuilder content = new StringBuilder();
            for (DescriptionType description : response.getDescription()) {
                content.append(description.getValue());
            }
            messageStatus.setMessageContent(content.toString());
        }

        if (applicationResponse.getIssueDate() != null) {
            Calendar issueDateTime = applicationResponse.getIssueDate().getValue().toGregorianCalendar();
            messageStatus.setIssueDateTime(issueDateTime);
        }

        return messageStatus;
    }
}

package eu.europa.ec.etrustex.adapter.integration.converters;

import eu.europa.ec.etrustex.adapter.model.entity.common.StatusResponseCode;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageReferenceState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.integration.model.common.v2.*;
import org.joda.time.DateTime;

/**
 * @author: micleva
 * @date: 6/19/12 4:21 PM
 * @project: ETX
 */
public class BackendMessageStatusTypeConverter {

    private BackendMessageStatusTypeConverter() {
    }

    /**
     *  Convert {@link EtxMessage} to {@link EtxMessageStatusType}
     * @param messageStatus {@link EtxMessage} to convert
     * @param refMessageType {@link MessageType} of the given {@link EtxMessage}
     * @return the converted {@link EtxMessage} in {@link EtxMessageStatusType}
     */
    public static EtxMessageStatusType convertEtxMessage(EtxMessage messageStatus, MessageType refMessageType) {
        EtxMessageStatusType etxMessageStatusType = new EtxMessageStatusType();

        etxMessageStatusType.setId(messageStatus.getMessageUuid());

        RecipientType sender = RecipientTypeConverter.convertEtxParty(messageStatus.getSender());
        etxMessageStatusType.setSender(sender);

        RecipientType receiver = RecipientTypeConverter.convertEtxParty(messageStatus.getReceiver());
        etxMessageStatusType.setReceiver(receiver);

        EtxMessageStatusCodeType statusCode = messageStatusCodeTypeFrom(messageStatus.getMessageReferenceStateType());

        etxMessageStatusType.setStatusCode(statusCode);

        EtxMessageReferenceType messageReference = new EtxMessageReferenceType();
        messageReference.setId(messageStatus.getMessageReferenceUuid());

        messageReference.setSender(receiver);
        messageReference.setReceiver(sender);
        messageReference.setType(refMessageType == MessageType.MESSAGE_BUNDLE ? EtxMessageCodeType.MESSAGE_BUNDLE : EtxMessageCodeType.MESSAGE_STATUS);

        etxMessageStatusType.setMessageReference(messageReference);

        etxMessageStatusType.setIssueDateTime(new DateTime(messageStatus.getIssueDateTime()));
        etxMessageStatusType.setReceivedDateTime(new DateTime(messageStatus.getCreationDate()));

        //work-around not to depend on EtxError
        if (messageStatus.getMessageReferenceStateType() == MessageReferenceState.FAILED) {
            ErrorType errorType = new ErrorType();
            errorType.setCode(messageStatus.getStatusReasonCode());
            errorType.setDetail(messageStatus.getMessageContent());
            String description = null;
            eu.europa.ec.etrustex.adapter.model.entity.common.ErrorType commonErrorType = eu.europa.ec.etrustex.adapter.model.entity.common.ErrorType.fromCode(messageStatus.getStatusReasonCode());
            if (commonErrorType != null) {
                description = commonErrorType.getDescription();
            } else {
                StatusResponseCode statusResponseCode = StatusResponseCode.fromCode(messageStatus.getStatusReasonCode());
                if (statusResponseCode != null) {
                    description = statusResponseCode.getDescription();
                }
            }
            errorType.setDescription(description);
            etxMessageStatusType.setErrorReason(errorType);
        }

        return etxMessageStatusType;
    }

    public static EtxMessage toEtxMessage(EtxMessageStatusType messageStatusSdo) {

        EtxMessage messageStatus = new EtxMessage();

        messageStatus.setMessageUuid(messageStatusSdo.getId());
        messageStatus.setMessageReferenceUuid(messageStatusSdo.getMessageReference().getId());
        messageStatus.setMessageReferenceStateType(MessageReferenceState.valueOf(messageStatusSdo.getStatusCode().value()));
        messageStatus.setIssueDateTime(messageStatusSdo.getIssueDateTime().toGregorianCalendar());

        return messageStatus;
    }

    private static EtxMessageStatusCodeType messageStatusCodeTypeFrom(MessageReferenceState referenceState) {
        switch (referenceState) {
            case AVAILABLE:
                return EtxMessageStatusCodeType.fromValue(MessageReferenceState.DELIVERED.name());
            default:
                return EtxMessageStatusCodeType.fromValue(referenceState.name());
        }
    }
}

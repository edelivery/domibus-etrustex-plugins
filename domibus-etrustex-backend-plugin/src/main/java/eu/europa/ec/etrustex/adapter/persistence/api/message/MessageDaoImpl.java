package eu.europa.ec.etrustex.adapter.persistence.api.message;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.message.*;
import eu.europa.ec.etrustex.adapter.model.vo.MessageReferenceVO;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBaseImpl;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Data Access Object for {@link EtxMessage}
 *
 * @author François Gautier
 * @version 1.0
 * @since 12-Oct-17
 */
@Repository
public class MessageDaoImpl extends DaoBaseImpl<EtxMessage> implements MessageDao {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(MessageDaoImpl.class);

    public MessageDaoImpl() {
        super(EtxMessage.class);
    }

    @Transactional
    @Override
    public EtxMessage findEtxMessage(String messageUuid, List<MessageType> types, MessageDirection direction, String senderPartyUuid, String receiverPartyUuid) {
        LOG.debug("START: Finding EtxMessage unread instances from senderPartyUuid:{} to receiverUuid:{}.", senderPartyUuid, receiverPartyUuid);

        TypedQuery<EtxMessage> query = getEntityManager().createNamedQuery("findUnreadMessageOrderedByDateDesc", EtxMessage.class);

        query.setParameter("messageUuid", messageUuid);
        query.setParameter("messageTypes", types);
        query.setParameter("messageDirection", direction);
        query.setParameter("senderUuid", senderPartyUuid);
        query.setParameter("receiverUuid", receiverPartyUuid);

        LOG.debug("DONE: Finding EtxMessage unread instances from senderPartyUuid:{} to receiverUuid:{}.", senderPartyUuid, receiverPartyUuid);

        try {
            return DataAccessUtils.singleResult(query.setMaxResults(1).getResultList());
        } catch (IncorrectResultSizeDataAccessException e) {
            LOG.warn("Query findUnreadMessageOrderedByDateDesc returned more than 1 result when only 1 was expected:", e);
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MessageReferenceVO> findUnreadMessagesByReceiverPartyUuid(String partyUuid) {
        LOG.debug("START: finding EtxMessage instances fore receiver PartyUuid:{}", partyUuid);

        TypedQuery<MessageReferenceVO> query = getEntityManager().createNamedQuery("findUnreadMessagesByReceiver", MessageReferenceVO.class);
        query.setParameter("receiverPartyUuid", partyUuid);
        query.setParameter("messageState", MessageState.MS_PROCESSED);
        query.setParameter("ignoredNotificationState", NotificationState.NF_NOTIFIED);
        query.setParameter("messageDirection", MessageDirection.IN);

        LOG.debug("DONE: finding EtxMessage instances fore receiver PartyUuid:{}", partyUuid);

        return query.getResultList();
    }

    @Override
    @Transactional(readOnly = true)
    public List<EtxMessage> findMessagesByNotificationState(NotificationState notificationState) {
        TypedQuery<EtxMessage> query = getEntityManager().createNamedQuery("findMessagesByNotificationState", EtxMessage.class);
        query.setParameter("notificationState", notificationState);

        return query.getResultList();
    }

    @Override
    public List<MessageReferenceVO> findMessagesByUuidAndSenderAndReceiver(String messageUuid, Long senderPartyId, Long receiverPartyId) {
        LOG.debug("START finding EtxMessage instances from senderPartyId:{} to receiverPartyId:{}", senderPartyId, receiverPartyId);

        TypedQuery<MessageReferenceVO> query = getEntityManager().createNamedQuery("findMessagesByUuidAndSenderAndReceiver", MessageReferenceVO.class);
        query.setParameter("messageUuid", messageUuid);
        query.setParameter("senderId", senderPartyId);
        query.setParameter("receiverId", receiverPartyId);

        LOG.debug("DONE: finding EtxMessage instances from senderPartyId:{} to receiverPartyId:{}", senderPartyId, receiverPartyId);

        return query.getResultList();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EtxMessage findMessageByDomibusMessageId(String domibusMessageID) {
        LOG.debug("Finding EtxMessage instance with Domibus Message ID: {}", domibusMessageID);
        TypedQuery<EtxMessage> query = getEntityManager().createNamedQuery("findMessageByDomibusMessageId", EtxMessage.class);
        query.setParameter("domibusMessageID", domibusMessageID);
        List<EtxMessage> resultList = query.getResultList();
        if (resultList.isEmpty()) {
            return null;
        }
        return resultList.get(0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public List<EtxMessage> retrieveMessageBundlesForPostUpload() {
        LOG.debug("Starting to retrieve Message Bundles pending for PostUpload");
        TypedQuery<EtxMessage> query = getEntityManager().createNamedQuery("retrieveMessageBundlesReadyForPostUpload", EtxMessage.class);
        List<EtxMessage> resultList = query.getResultList();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Finished retrieving MessageBundles for PostUpload. Number of bundles:" + ((resultList != null) ? resultList.size() : 0));
        }
        return resultList;
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public List<MessageReferenceVO> findImpactedBundlesDueToOutgoingMessageBundleRetrigger(List<String> attachmentUUIDList, List<String> retriggerMessageBundleUUIDList) {
        TypedQuery<MessageReferenceVO> attachmentsQuery = getEntityManager().createNamedQuery("BEPlugin.EtxMessage.retrieveImpactedBundlesDueToOutgoingMessageBundleRetrigger", MessageReferenceVO.class);
        attachmentsQuery.setParameter("attachmentUUIDList", attachmentUUIDList);
        attachmentsQuery.setParameter("messageBundleUUIDList", retriggerMessageBundleUUIDList);
        return attachmentsQuery.getResultList();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public List<MessageReferenceVO> retrieveFailedMessageBundles(Date startDate, Date endDate, Date endDateMinAge, int limit) {
        LOG.debug("Starting to retrieve failed Message Bundles");
        TypedQuery<MessageReferenceVO> query = getEntityManager().createNamedQuery("BEPlugin.EtxMessage.retrieveFailedMessageBundles", MessageReferenceVO.class);

        query.setParameter("START_DATE", getCalendar(startDate));
        query.setParameter("END_DATE", getCalendar(endDate));
        query.setParameter("END_DATE_MIN_AGE", getCalendar(endDateMinAge));
        query.setMaxResults(limit);
        List<MessageReferenceVO> resultList = query.getResultList();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Finished retrieving failed MessageBundles. Number of bundles:" + ((resultList != null) ? resultList.size() : 0));
        }
        return resultList;
    }

    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }
}

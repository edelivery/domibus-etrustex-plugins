package eu.europa.ec.etrustex.adapter.queue.postupload;

import eu.europa.ec.etrustex.adapter.queue.AbstractQueueListener;
import org.springframework.stereotype.Component;

/**
 * @author micleva
 * @project ETX
 */
@Component("etxBackendPluginPostUploadConsumer")
public class PostUploadConsumer extends AbstractQueueListener<PostUploadQueueMessage> {

    private final PostUploadHandler handler;

    public PostUploadConsumer(PostUploadHandler handler) {
        this.handler = handler;
    }

    @Override
    public void handleMessage(PostUploadQueueMessage queueMessage) {
        handler.handlePostUpload(queueMessage.getMessageId());
    }

    @Override
    public void validateMessage(PostUploadQueueMessage queueMessage) {
        handler.validatePostUpload(queueMessage.getMessageId());
    }
}

package eu.europa.ec.etrustex.adapter.model.entity.administration;

import eu.europa.ec.etrustex.adapter.model.entity.AuditEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * <p>
 * Title: System
 * </p>
 * <p/>
 * <p>
 * Description: Domain Object describing a System entity
 * </p>
 */
@Entity(name = "EtxSystem")
@AttributeOverrides({
        @AttributeOverride(name = "creationDate", column = @Column(name = "SYS_CREATED_ON", insertable = false, updatable = false, nullable = false)),
        @AttributeOverride(name = "modificationDate", column = @Column(name = "SYS_UPDATED_ON"))})
@Table(name = "ETX_ADT_SYSTEM")
@NamedQueries({
        @NamedQuery(name = "systemByLocalUserName", query = "SELECT s FROM EtxSystem s WHERE s.localUsername = :localUserName",
                hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")}),
        @NamedQuery(name = "systemByName", query = "SELECT s FROM EtxSystem s WHERE s.name = :systemName",
                hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
})
public class EtxSystem extends AuditEntity<Long> implements Serializable {

    private static final long serialVersionUID = 2115878036259121559L;

    @Id
    @Column(name = "SYS_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "SYS_NAME", length = 150, nullable = false, unique = true)
    private String name;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "SYS_BACKEND_USR_ID", unique = true)
    private EtxUser backendUser;

    @OneToMany(mappedBy = "system", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<EtxSystemConfig> systemConfigList;

    @Column(name = "USERNAME")
    private String localUsername;

    /**
     * Default constructor
     */
    public EtxSystem() {
        //empty constructor
    }

    public EtxUser getBackendUser() {
        return backendUser;
    }

    public String getLocalUsername() {
        return localUsername;
    }

    public void setLocalUsername(String localUsername) {
        this.localUsername = localUsername;
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<EtxSystemConfig> getSystemConfigList() {
        if (systemConfigList == null) {
            systemConfigList = new ArrayList<>();
        }

        return systemConfigList;
    }

    public void setBackendUser(EtxUser backendUser) {
        this.backendUser = backendUser;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSystemConfigList(List<EtxSystemConfig> systemConfigList) {
        this.systemConfigList = systemConfigList;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("EtxSystem");
        sb.append("{id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public boolean requiresAutoNotification() {
        List<EtxSystemConfig> configList = getSystemConfigList();
        BackendNotificationType notificationType = null;
        String notificationUrl = null;
        for (EtxSystemConfig config : configList) {
            if (config.getSystemConfigurationProperty() == SystemConfigurationProperty.ETX_BACKEND_NOTIFICATION_TYPE) {
                notificationType = BackendNotificationType.fromCode(config.getPropertyValue());
            }
            if (config.getSystemConfigurationProperty() == SystemConfigurationProperty.ETX_BACKEND_SERVICE_INBOX_NOTIFICATION_URL) {
                notificationUrl = config.getPropertyValue();
            }
        }

        return notificationType == BackendNotificationType.WITNESS_FILE
                || (notificationType == BackendNotificationType.WEB_SERVICE &&
                (notificationUrl != null && !notificationUrl.isEmpty()));
    }
}

package eu.europa.ec.etrustex.adapter.integration.client.backend.repository;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.client.backend.BackendServiceClient;
import eu.europa.ec.etrustex.adapter.integration.converters.BackendMessageBundleTypeConverter;
import eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.model.vo.FileAttachmentVO;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.integration.model.common.v2.EtxMessageBundleType;
import eu.europa.ec.etrustex.integration.service.notification.v2.ObjectFactory;
import eu.europa.ec.etrustex.integration.service.notification.v2.OnMessageBundleRequestType;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

/**
 * @deprecated It will be dropped 'soon'
 */
@Service("etxBackendPluginWitnessFileServiceClient")
@Deprecated
public class WitnessFileServiceClientImpl implements BackendServiceClient {

    public static final String ETX_ADAPTER_IN_DIRECTORY = "in";

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(WitnessFileServiceClientImpl.class);

    /**
     * default file separator
     */
    private static final String FILE_SEPARATOR = FileSystems.getDefault().getSeparator();
    public static final String NOT_IMPLEMENTED = "This operation is not yet implemented";

    private JAXBContext messageBundleRequestJAXBContext;
    private String adapterRootPath;

    @PostConstruct
    protected void init() {
        try {
            messageBundleRequestJAXBContext = JAXBContext.newInstance(OnMessageBundleRequestType.class);
        } catch (JAXBException e) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, e.getMessage(), e);
        }
    }

    @Override
    public FileAttachmentVO downloadAttachment(Map<SystemConfigurationProperty, String> systemConfig, EtxMessage messageBundle, EtxAttachment etxAttachment) {
        throw new UnsupportedOperationException(NOT_IMPLEMENTED);
    }

    @Override
    public void notifyMessageBundle(Map<SystemConfigurationProperty, String> systemConfig, EtxMessage messageBundle) {
        LOG.debug("writeWitnessFile: {}", messageBundle);

        String receiverSystemName = messageBundle.getReceiver().getSystem().getName();
        try {
            // build the directory folder
            String receiverPartyName = messageBundle.getReceiver().getPartyUuid();
            String senderPartyName = messageBundle.getSender().getPartyUuid();
            String messageUuid = messageBundle.getMessageUuid();
            Path fileDirectory = buildFileDirectory(receiverSystemName, receiverPartyName, senderPartyName, messageUuid);

            //create the Bundle xml file
            String bundleFileName = "Bundle.xml";
            Path xmlFile = fileDirectory.resolve(bundleFileName);
            Files.deleteIfExists(xmlFile);
            Files.createFile(xmlFile);

            // prepare what needs to be written on the file
            EtxMessageBundleType messageBundleSdo = BackendMessageBundleTypeConverter.fromEtxMessage(messageBundle);
            OnMessageBundleRequestType onMessageBundleRequest = new OnMessageBundleRequestType();
            onMessageBundleRequest.setMessageBundle(messageBundleSdo);

            //convert sdo to Xml String
            String xmlString = jaxbToXmlString(onMessageBundleRequest);

            //write in the bundle xml file the String
            Files.write(xmlFile, xmlString.getBytes(StandardCharsets.UTF_8));

            //write the EOT file
            if (Files.exists(xmlFile) && Files.size(xmlFile) > 0) {
                String eotFileName = "EOT";
                Path eotFile = fileDirectory.resolve(eotFileName);
                Files.createFile(eotFile);
            }
        } catch (IOException | JAXBException e) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "Unable to write the witness file", e);
        }
    }

    @Override
    public void notifyMessageStatus(Map<SystemConfigurationProperty, String> systemConfig, EtxMessage messageStatus, MessageType refMessageType) {
        LOG.warn(NOT_IMPLEMENTED);
    }

    @Override
    public void uploadAttachment(Map<SystemConfigurationProperty, String> systemConfig, EtxMessage messageBundle, EtxAttachment etxAttachment, File attachmentFile) {
        LOG.warn(NOT_IMPLEMENTED);
    }

    private Path buildFileDirectory(String receiverSystemName,
                                    String receiverPartyName, String senderPartyName, String messageUuid) throws IOException {
        Path rootDirectory = Paths.get(getRepositoryRootPath());
        if (Files.notExists(rootDirectory)) {
            Files.createDirectories(rootDirectory);
        }

        StringBuilder filePath = new StringBuilder();
        filePath.append(receiverSystemName);
        filePath.append(FILE_SEPARATOR);

        filePath.append(ETX_ADAPTER_IN_DIRECTORY);
        filePath.append(FILE_SEPARATOR);
        filePath.append(receiverPartyName);
        filePath.append(FILE_SEPARATOR);
        filePath.append(senderPartyName);

        filePath.append(FILE_SEPARATOR);
        filePath.append(messageUuid);

        filePath.append(FILE_SEPARATOR);
        filePath.append("meta");

        Path fileDirectory = rootDirectory.resolve(filePath.toString());
        if (Files.notExists(fileDirectory)) {
            Files.createDirectories(fileDirectory);
        }
        return fileDirectory;
    }

    private String getRepositoryRootPath() {

        StringBuilder rootPath = new StringBuilder();
        rootPath.append(adapterRootPath);
        rootPath.append(FILE_SEPARATOR);

        return rootPath.toString();
    }

    protected String jaxbToXmlString(OnMessageBundleRequestType bundleRequestType) throws JAXBException {

        Marshaller marshaller = messageBundleRequestJAXBContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        ObjectFactory objectFactory = new ObjectFactory();

        StringWriter sw = new StringWriter();
        marshaller.marshal(objectFactory.createOnMessageBundleRequest(bundleRequestType), sw);

        return sw.toString();
    }

    public void setAdapterRootPath(String adapterRootPath) {
        this.adapterRootPath = adapterRootPath;
    }
}

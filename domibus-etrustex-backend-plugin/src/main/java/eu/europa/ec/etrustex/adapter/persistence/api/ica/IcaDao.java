package eu.europa.ec.etrustex.adapter.persistence.api.ica;

import eu.europa.ec.etrustex.adapter.model.entity.ica.EtxIca;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBase;

import java.util.List;

public interface IcaDao extends DaoBase<EtxIca> {

    List<EtxIca> findIca(String senderUuid, String receiverUuid);
}

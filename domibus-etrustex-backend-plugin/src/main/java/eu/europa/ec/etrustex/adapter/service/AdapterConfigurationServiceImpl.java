package eu.europa.ec.etrustex.adapter.service;

import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPlugin;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPluginService;
import eu.europa.ec.etrustex.adapter.model.entity.administration.AdapterConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxAdapterConfig;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.AdapterConfigDao;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author micleva
 * @project ETX
 */

@Service("etxBackendPluginAdapterConfigurationService")
@Transactional
public class AdapterConfigurationServiceImpl implements AdapterConfigurationService {

    @Autowired
    private AdapterConfigDao adapterConfigDao;

    @Autowired
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Autowired
    private DomainBackEndPluginService domainBackEndPluginService;

    @Override
    @Cacheable("config")
    @Transactional
    @DomainBackEndPlugin
    @ClearEtxLogKeys
    public Map<AdapterConfigurationProperty, String> getAdapterConfigurations() {
        domainBackEndPluginService.setBackendPluginDomain();
        Map<AdapterConfigurationProperty, String> properties = new HashMap<>();
        List<EtxAdapterConfig> adapterConfigs = adapterConfigDao.findAdapterConfigs();
        for (EtxAdapterConfig config : adapterConfigs) {
            properties.put(
                    config.getPropertyName(),
                    config.getPropertyValue()
            );
        }
        clearEtxLogKeysService.clearKeys();
        return properties;
    }
}

package eu.europa.ec.etrustex.adapter.model.entity.administration;

import eu.europa.ec.etrustex.adapter.model.entity.AbstractEntity;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

/**
 * <p>
 * Title: Adapter config
 * </p>
 * <p/>
 * <p>
 * Description: Domain Object describing a Adapter Config entity
 * </p>
 */
@Entity(name = "EtxAdapterConfig")
@Table(name = "ETX_ADT_CONFIG")
@NamedQueries({
        @NamedQuery(name = "findAllAdapterConfigs", query = "SELECT o FROM EtxAdapterConfig o",
                hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
})
public class EtxAdapterConfig extends AbstractEntity<Long> implements Serializable {

    private static final long serialVersionUID = -5351303548409044662L;

    @Id
    @Column(name = "ACG_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ACG_PROPERTY_NAME", length = 250, nullable = false)
    private String propertyName;

    @Column(name = "ACG_PROPERTY_VALUE", length = 250, nullable = false)
    private String propertyValue;

    public EtxAdapterConfig() {
        //empty constructor
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AdapterConfigurationProperty getPropertyName() {
        return AdapterConfigurationProperty.fromCode(propertyName);
    }

    public void setPropertyName(AdapterConfigurationProperty adapterConfigurationProperty) {
        this.propertyName = adapterConfigurationProperty.getCode();
    }

    public String getPropertyValue() {
        return propertyValue;
    }

    public void setPropertyValue(String propertyValue) {
        this.propertyValue = propertyValue;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("EtxSystemConfig ");
        sb.append("{id=").append(id);
        sb.append(", propertyName='").append(propertyName).append('\'');
        sb.append(", propertyValue='").append(propertyValue).append('\'');
        sb.append('}');
        return sb.toString();
    }
}



package eu.europa.ec.etrustex.adapter.queue.error;

import eu.domibus.ext.services.AuthenticationExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.MessageNotFoundException;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.integration.ETrustExDomibusBackendConnector;
import eu.europa.ec.etrustex.adapter.integration.handlers.BackendActionHandlerFactory;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.handlers.Service;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.context.SecurityContextHolder;

import static eu.domibus.logging.DomibusLogger.MDC_MESSAGE_ID;
import static eu.europa.ec.etrustex.common.exceptions.ETrustExError.DEFAULT;
import static eu.europa.ec.etrustex.common.handlers.Action.FAULT_ACTION;
import static eu.europa.ec.etrustex.common.handlers.Service.fromString;
import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_CONVERSATION_ID;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.getFaultPayloadDTO;

@org.springframework.stereotype.Service("etxBackendPluginErrorHandlerImpl")
public class ErrorHandlerImpl implements ErrorHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ErrorHandlerImpl.class);

    @Autowired
    private ETrustExDomibusBackendConnector eTrustExDomibusBackendConnector;

    @Autowired
    private AuthenticationExtService authenticationExtService;

    @Autowired
    @Lazy
    private ETrustExBackendPluginProperties eTrustExBackendPluginProperties;

    @Autowired
    private BackendActionHandlerFactory backendActionHandlerFactory;

    @Override
    public void onError(String as4MsgId) {

        if (SecurityContextHolder.getContext().getAuthentication() == null ||
                !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            authenticationExtService.basicAuthenticate(eTrustExBackendPluginProperties.getUser(), eTrustExBackendPluginProperties.getPwd());
        }
        ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        try {
            eTrustExAdapterDTO = eTrustExDomibusBackendConnector.downloadMessage(as4MsgId, eTrustExAdapterDTO);
            LOG.putMDC(MDC_ETX_CONVERSATION_ID, eTrustExAdapterDTO.getAs4ConversationId());
            LOG.putMDC(MDC_MESSAGE_ID, eTrustExAdapterDTO.getAs4MessageId());
        } catch (MessageNotFoundException e) {
            throw new ETrustExPluginException(DEFAULT, "Domibus Message not found with ID: " + as4MsgId, e);
        }

        eTrustExAdapterDTO.addAs4Payload(getFaultPayloadDTO(String.format("Error on Domibus message %s", as4MsgId)));

        Service service = fromString(eTrustExAdapterDTO.getAs4Service());
        try {
            OperationHandler responseFaultHandler = backendActionHandlerFactory.getOperationHandler(service, FAULT_ACTION);
            responseFaultHandler.handle(eTrustExAdapterDTO);
        } catch (ETrustExPluginException e) {
            LOG.error("Could not perform on error operation with {}", eTrustExAdapterDTO, e);
            throw e;
        }
    }
}

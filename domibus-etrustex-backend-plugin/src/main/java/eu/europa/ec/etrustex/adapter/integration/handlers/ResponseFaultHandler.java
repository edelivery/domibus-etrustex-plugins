package eu.europa.ec.etrustex.adapter.integration.handlers;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxErrorBuilder;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.queue.postupload.PostUploadProducer;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static eu.europa.ec.etrustex.adapter.integration.handlers.InboxWrapperTransmissionBackendHandler.getAttachment;
import static eu.europa.ec.etrustex.common.exceptions.EtrustExMarker.NON_RECOVERABLE;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Handle Error on the node side after sending a messageStatus and messageBundle
 *
 * @author François GAUTIER
 * @version 1.0
 * @since 22-09-2017
 */
@Service("etxBackendPluginResponseFaultHandler")
public class ResponseFaultHandler extends ResponseFaultHandlerBase {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ResponseFaultHandler.class);

    @Autowired
    private PostProcessProducer postProcessProducer;
    @Autowired
    private PostUploadProducer postUploadProducer;

    @Autowired
    private MessageServicePlugin messageService;
    @Autowired
    private AttachmentService attachmentService;

    protected void process(String payload, ETrustExAdapterDTO eTrustExAdapterResponseDTO) {
        EtxMessage etxMessage = messageService.findOrCreateMessageByDomibusMessageId(eTrustExAdapterResponseDTO);
        LOG.error("Message in error: {}", payload);
        if (etxMessage != null) {
            if (isNotBlank(eTrustExAdapterResponseDTO.getAttachmentId())) {
                processAttachment(payload, getAttachment(eTrustExAdapterResponseDTO.getAttachmentId(), etxMessage));
            } else {
                process(payload, etxMessage);
            }
        } else {
            LOG.error(NON_RECOVERABLE, "Error for the ResponseFaultHandler. Payload: {}, uuid: {}", payload, ETrustExAdapterDTO.getUuid(eTrustExAdapterResponseDTO));
        }
    }

    /**
     * Set the error to the message and trigger the post process to put the message to failed
     *
     * @param payload    of the error received
     * @param etxMessage the error is concerned
     */
    public void process(String payload, EtxMessage etxMessage) {
        etxMessage.setError(EtxErrorBuilder
                .getInstance()
                .withResponseCode(ETrustExError.DEFAULT.getCode())
                .withDetail(payload)
                .build());
        postProcessProducer.triggerPostProcess(etxMessage.getId());
    }

    private void processAttachment(String payload, EtxAttachment etxAttachment) {
        attachmentService.attachmentInError(etxAttachment.getId(), ETrustExError.DEFAULT.getCode(), payload);
        postUploadProducer.checkAttachmentsAndTriggerPostUpload(etxAttachment);
    }
}

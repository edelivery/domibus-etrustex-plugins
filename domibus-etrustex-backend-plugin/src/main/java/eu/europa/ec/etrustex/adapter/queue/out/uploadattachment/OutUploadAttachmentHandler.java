package eu.europa.ec.etrustex.adapter.queue.out.uploadattachment;

/**
 * @author micleva
 * @project ETX
 */

public interface OutUploadAttachmentHandler {

    void validateUploadAttachment(Long attachmentId);

    void handleUploadAttachment(Long attachmentId);

    void onError(Long attachmentId);
}
package eu.europa.ec.etrustex.adapter.integration.client.backend.ws.adapter;

import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.vo.FileAttachmentVO;

import java.io.File;

/**
 * @author Arun Raj
 * @version 1.0
 * @author: micleva
 * @date: 6/26/12 1:59 PM
 * @project: ETX
 */
public interface FileRepositoryServiceAdapter {

    FileAttachmentVO downloadAttachment(EtxMessage messageBundle, EtxAttachment etxAttachment);

    void uploadAttachment(EtxMessage messageBundle, EtxAttachment etxAttachment, File attachmentFile);
}

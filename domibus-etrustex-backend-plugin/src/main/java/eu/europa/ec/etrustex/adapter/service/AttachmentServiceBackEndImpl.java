package eu.europa.ec.etrustex.adapter.service;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxError;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxErrorBuilder;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.persistence.api.attachment.AttachmentDao;
import eu.europa.ec.etrustex.adapter.persistence.api.common.ErrorDao;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.service.WorkspaceService;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState.*;
import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_ATT_UUID;
import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_MESSAGE_UUID;
import static eu.europa.ec.etrustex.common.util.Validate.notBlank;
import static eu.europa.ec.etrustex.common.util.Validate.notNull;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

/**
 * @author micleva
 * @project ETX
 */

@Service("etxBackendPluginAttachmentServiceImpl")
public class AttachmentServiceBackEndImpl implements AttachmentService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(AttachmentServiceBackEndImpl.class);

    @Autowired
    private AttachmentDao attachmentDao;

    @Autowired
    private ErrorDao errorDao;

    @Autowired
    @Qualifier("etxBackendPluginWorkspaceService")
    private WorkspaceService workspaceService;

    @Autowired
    private ClearEtxLogKeysService clearEtxLogKeysService;

    /**
     * {@inheritDoc}
     */
    @Override
    public EtxAttachment findById(Long id) {
        EtxAttachment byId = attachmentDao.findById(id);
        if (byId != null) {
            LOG.putMDC(MDC_ETX_ATT_UUID, byId.getAttachmentUuid());
            if (byId.getMessage() != null) {
                LOG.putMDC(MDC_ETX_MESSAGE_UUID, byId.getMessage().getMessageUuid());
            }
        }
        return byId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean areAllAttachmentsUploaded(EtxMessage message) {
        // Count all attachments that are not in UPLOADED, ALREADY_UPLOADED state
        Long attachmentsNotUploadedCnt = attachmentDao.countMessageAttachmentsNotInStateList(message.getId(),
                Arrays.asList(ATTS_UPLOADED, ATTS_ALREADY_UPLOADED));
        return attachmentsNotUploadedCnt == 0L;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasFailedAttachment(EtxMessage message) {
        Long failedAttachmentCnt = attachmentDao.countMessageAttachmentsInStateList(message.getId(),
                singletonList(ATTS_FAILED));
        return failedAttachmentCnt > 0L;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean areAllAttachmentsInFinalState(EtxMessage message) {
        Long attachmentsNotInFinalStateCnt = attachmentDao.countMessageAttachmentsNotInStateList(message.getId(),
                Arrays.asList(ATTS_FAILED, ATTS_UPLOADED, ATTS_ALREADY_UPLOADED, ATTS_IGNORED));
        return attachmentsNotInFinalStateCnt == 0L;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void attachmentInError(Long attachmentEntityId, String errorCode, String error) {
        EtxAttachment etxAttachment = findById(attachmentEntityId);
        etxAttachment.setStateType(ATTS_FAILED);
        etxAttachment.setError(EtxErrorBuilder
                .getInstance()
                .withResponseCode(errorCode)
                .withDetail(error)
                .build());
        attachmentDao.update(etxAttachment);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void updateAttachmentState(EtxAttachment attachmentEntity, AttachmentState oldState, AttachmentState newState) {
        if (attachmentEntity.getStateType() == oldState) {
            attachmentEntity.setStateType(newState);

            attachmentDao.update(attachmentEntity);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean checkAttachmentForSystemInState(EtxAttachment attachment, EtxSystem senderSystem, List<AttachmentState> attachmentStateList) {
        return attachmentDao.countAttachmentsBySystemAndUUIDAndState(attachment.getAttachmentUuid(), attachmentStateList, senderSystem.getId()) > 0L;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long countNextAttachmentsToExpire(Date date) {
        return defaultIfNull(attachmentDao.countNextAttachmentsToExpire(date), 0L);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    @ClearEtxLogKeys
    public void expireAttachments(Date date, int pageSize, int index) {
        List<EtxAttachment> attachments = attachmentDao.getNextAttachmentsToExpire(date, pageSize, index);
        for (EtxAttachment attachment : attachments) {
            LOG.putMDC(MDC_ETX_ATT_UUID, attachment.getAttachmentUuid());
            LOG.putMDC(MDC_ETX_MESSAGE_UUID, attachment.getMessage().getMessageUuid());
            attachment.setActiveState(false);
            attachmentDao.update(attachment);
            workspaceService.deleteFile(attachment.getId());
            LOG.info("attachment with ID: [{}] was made expired", attachment.getId());
        }
        clearEtxLogKeysService.clearKeys();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EtxAttachment findAttachmentByDomibusMessageId(String domibusMessageID) {
        EtxAttachment etxAttachment;
        domibusMessageID = StringUtils.trim(domibusMessageID);
        notBlank(domibusMessageID, "The Domibus Message ID required to retrieve the eTrustEx Backend Attachment has not been sent!");
        etxAttachment = attachmentDao.findAttachmentByDomibusMessageId(domibusMessageID);
        if (etxAttachment != null) {
            LOG.putMDC(MDC_ETX_ATT_UUID, etxAttachment.getAttachmentUuid());
            if (etxAttachment.getMessage() != null) {
                LOG.putMDC(MDC_ETX_MESSAGE_UUID, etxAttachment.getMessage().getMessageUuid());
            }
        }
        notNull(etxAttachment, ETrustExError.ETX_005, "Missing entry in eTrustEx Backend Attachment DB for Domibus Message Id: " + domibusMessageID);
        return etxAttachment;
    }

    @Override
    public void updateStuckAttachments(LocalDateTime startDate,
                                       LocalDateTime endDate) {
        EtxError error = EtxErrorBuilder.getInstance().withResponseCode(ETrustExError.ETX_010.getCode())
                .withDetail("Attachment set to ATTS_FAILED by CleanUpAttachementStateJob as it was pending for long time. Attachments with modification date > [" + startDate + "] and < [" + endDate + "] considered.")
                .build();
        errorDao.save(error);

        int attachmentUpdated = attachmentDao.updateStuckAttachments(startDate, endDate, error);

        if (attachmentUpdated < 1) {
            LOG.info("No attachement updated");
            errorDao.removeById(error.getId());
        } else {
            LOG.info("[{}] attachement updated", attachmentUpdated);
        }
    }
}

package eu.europa.ec.etrustex.adapter.integration.converters;

import eu.europa.ec.etrustex.common.model.entity.attachment.AttachmentType;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.ChecksumAlgorithmType;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.integration.model.common.v2.ChecksumType;
import eu.europa.ec.etrustex.integration.model.common.v2.DigestAlgorithmType;
import eu.europa.ec.etrustex.integration.model.common.v2.EtxFileCodeType;
import eu.europa.ec.etrustex.integration.model.common.v2.FileMetadataType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: micleva
 * @date: 6/17/13 9:43 AM
 * @project: ETX
 */
public class FileMetadataTypeConverter {

    private FileMetadataTypeConverter() {
    }

    public static List<FileMetadataType> convertEtxAttachmentList(List<EtxAttachment> etxAttachmentList) {
        List<FileMetadataType> fileMetadataTypeList = null;

        if (etxAttachmentList != null) {
            fileMetadataTypeList = new ArrayList<>();
            for (EtxAttachment etxAttachment : etxAttachmentList) {
                fileMetadataTypeList.add(convertEtxAttachment(etxAttachment));
            }
        }

        return fileMetadataTypeList;
    }

    public static FileMetadataType convertEtxAttachment(EtxAttachment etxAttachment) {
        FileMetadataType fileMetadataType = new FileMetadataType();

        fileMetadataType.setId(etxAttachment.getAttachmentUuid());
        fileMetadataType.setFileType(EtxFileCodeType.fromValue(etxAttachment.getAttachmentType().name()));
        fileMetadataType.setRelativePath(etxAttachment.getFilePath());
        fileMetadataType.setName(etxAttachment.getFileName());
        fileMetadataType.setMimeType(etxAttachment.getMimeType());
        fileMetadataType.setSize(etxAttachment.getFileSize());
        ChecksumType checksum = new ChecksumType();

        if (etxAttachment.getChecksumAlgorithmType() != null) {
            checksum.setValue(etxAttachment.getChecksum());
            checksum.setAlgorithm(DigestAlgorithmType.fromValue(etxAttachment.getChecksumAlgorithmType().getValue()));
        }

        fileMetadataType.setChecksum(checksum);

        return fileMetadataType;
    }


    public static EtxAttachment toEtxAttachment(FileMetadataType attachmentSdo) {
        EtxAttachment etxAttachment = new EtxAttachment();
        etxAttachment.setAttachmentUuid(attachmentSdo.getId());

        if (attachmentSdo.getFileType() != null) {
            etxAttachment.setAttachmentType(AttachmentType.valueOf(attachmentSdo.getFileType().value()));
        }

        etxAttachment.setFilePath(attachmentSdo.getRelativePath());
        etxAttachment.setFileName(attachmentSdo.getName());
        etxAttachment.setMimeType(attachmentSdo.getMimeType());
        etxAttachment.setFileSize(attachmentSdo.getSize());
        etxAttachment.setChecksum(attachmentSdo.getChecksum().getValue());

        if (attachmentSdo.getChecksum() != null && attachmentSdo.getChecksum().getAlgorithm() != null) {
            etxAttachment.setChecksumAlgorithmType(ChecksumAlgorithmType.fromValue(attachmentSdo.getChecksum().getAlgorithm().value()));
        }

        return etxAttachment;
    }
}

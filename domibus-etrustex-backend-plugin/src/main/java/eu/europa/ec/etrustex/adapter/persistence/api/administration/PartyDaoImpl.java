package eu.europa.ec.etrustex.adapter.persistence.api.administration;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBaseImpl;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class PartyDaoImpl extends DaoBaseImpl<EtxParty> implements PartyDao {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PartyDaoImpl.class);

    public PartyDaoImpl() {
        super(EtxParty.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EtxParty findByPartyUUID(String partyUUID) {
        TypedQuery<EtxParty> query = getEntityManager().createNamedQuery("EtxParty.findByUUID", EtxParty.class);
        query.setParameter("partyUUID", partyUUID);

        List<EtxParty> resultList = query.getResultList();
        if (resultList.isEmpty()) {
            LOG.warn("No EtxSystem found for PartyUUID {}", partyUUID);
            return null;
        }
        return resultList.get(0);
    }

}

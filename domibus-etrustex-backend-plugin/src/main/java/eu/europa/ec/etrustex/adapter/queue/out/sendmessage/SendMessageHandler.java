package eu.europa.ec.etrustex.adapter.queue.out.sendmessage;

/**
 * @author micleva
 * @project ETX
 */

public interface SendMessageHandler {
    // methods for send message bundle
    void validateSendBundle(Long messageId) ;

    void handleSendBundle(Long messageId);

    // methods for send message status
    void validateSendStatus(Long messageId) ;

    void handleSendStatus(Long messageId);

    //common methods
    void onError(Long messageId);
}
package eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter;

import ec.schema.xsd.commonaggregatecomponents_2.BusinessHeaderType;
import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.schema.xsd.commonaggregatecomponents_2.TechnicalHeaderType;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.service.BackendConnectorSubmissionService;
import eu.europa.ec.etrustex.common.converters.node.NodePartnerConverter;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import oasis.names.specification.ubl.schema.xsd.signatureaggregatecomponents_2.SignatureInformationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.unece.cefact.namespaces.standardbusinessdocumentheader.BusinessScope;
import org.unece.cefact.namespaces.standardbusinessdocumentheader.Scope;

import javax.xml.ws.Holder;

/**
 * @author Arun Raj
 * @version 1.0
 * @since May 2017
 */
//TODO:EDELIVERY-6608 - Remove ETX BE Plugin NodeInvocationManager facade & NodeServiceAdapters & refactor code functionally

@Component
public abstract class NodeServicesAdapterBase {

    public static final String STATUS_SCOPE_IDENTIFIER = "CREATE_STATUS_AVAILABLE";

    @Autowired
    @Qualifier("etxBackendPluginProperties")
    public ETrustExBackendPluginProperties eTrustExBackendPluginProperties;

    @Autowired
    protected BackendConnectorSubmissionService backendConnectorSubmissionService;

    /**
     * Form the Header parameter element for eTrustEx service invocations without scope
     *
     * @param senderPartyName   the name of the sender party
     * @param receiverPartyName the name of the receiver party
     * @return a header with the authorization
     */
    Holder<HeaderType> fillAuthorisationHeader(String senderPartyName, String receiverPartyName) {

        Holder<HeaderType> authorizationHeaderHolder = new Holder<>();
        authorizationHeaderHolder.value = new HeaderType();
        BusinessHeaderType businessHeaderType = new BusinessHeaderType();
        if (receiverPartyName != null) {
            businessHeaderType.getReceiver().add(NodePartnerConverter.buildPartnerType(receiverPartyName));
        }
        businessHeaderType.getSender().add(NodePartnerConverter.buildPartnerType(senderPartyName));

        authorizationHeaderHolder.value.setBusinessHeader(businessHeaderType);

        //create the technical header
        TechnicalHeaderType technicalHeaderType = new TechnicalHeaderType();
        technicalHeaderType.getSignatureInformation().add(new SignatureInformationType());
        authorizationHeaderHolder.value.setTechnicalHeader(technicalHeaderType);

        return authorizationHeaderHolder;
    }

    /**
     * Form the Header parameter element for eTrustEx service invocations with scope
     *
     * @param senderPartyName   the name of the sender party
     * @param receiverPartyName the name of the receiver party
     * @return a header with the authorization and scope
     */
    Holder<HeaderType> fillAuthorisationHeaderWithStatusScope(String senderPartyName, String receiverPartyName) {
        Holder<HeaderType> authorizationHeaderHolder = fillAuthorisationHeader(senderPartyName, receiverPartyName);

        BusinessScope businessScope = new BusinessScope();
        Scope scope = new Scope();
        scope.setType(STATUS_SCOPE_IDENTIFIER);
        businessScope.getScope().add(scope);
        authorizationHeaderHolder.value.getBusinessHeader().setBusinessScope(businessScope);

        return authorizationHeaderHolder;
    }

    ETrustExAdapterDTO createETrustExAdapterDTOWithCommonDataForDomibusSubmission(EtxParty senderParty, long backendMessageId) {
        return createETrustExAdapterDTOWithCommonDataForDomibusSubmission(senderParty, null, backendMessageId);
    }

    ETrustExAdapterDTO createETrustExAdapterDTOWithCommonDataForDomibusSubmission(EtxParty senderParty, EtxParty receiverParty, long backendMessageId) {
        String receiverPartyUuid = eTrustExBackendPluginProperties.getAs4ETrustExNodePartyId();
        if (receiverParty != null) {
            receiverPartyUuid = receiverParty.getPartyUuid();
        }
        return createETrustExAdapterDTOWithCommonDataForDomibusSubmission(senderParty, backendMessageId, receiverPartyUuid);
    }

    private ETrustExAdapterDTO createETrustExAdapterDTOWithCommonDataForDomibusSubmission(EtxParty senderParty, long backendMessageId, String receiverPartyUuid) {
        ETrustExAdapterDTO etxAdapterDTO = new ETrustExAdapterDTO();

        //From Party
        etxAdapterDTO.setAs4FromPartyId(senderParty.getPartyUuid());
        etxAdapterDTO.setAs4FromPartyIdType(eTrustExBackendPluginProperties.getAs4FromPartyIdType());
        etxAdapterDTO.setAs4FromPartyRole(eTrustExBackendPluginProperties.getAs4FromPartyRole());

        //To Party
        etxAdapterDTO.setAs4ToPartyId(eTrustExBackendPluginProperties.getAs4ETrustExNodePartyId());
        etxAdapterDTO.setAs4ToPartyIdType(eTrustExBackendPluginProperties.getAs4ToPartyIdType());
        etxAdapterDTO.setAs4ToPartyRole(eTrustExBackendPluginProperties.getAs4ToPartyRole());

        //message properties
        etxAdapterDTO.setAs4OriginalSender(senderParty.getPartyUuid());
        etxAdapterDTO.setAs4FinalRecipient(receiverPartyUuid);
        etxAdapterDTO.setEtxBackendMessageId(Long.toString(backendMessageId));

        return etxAdapterDTO;
    }

}

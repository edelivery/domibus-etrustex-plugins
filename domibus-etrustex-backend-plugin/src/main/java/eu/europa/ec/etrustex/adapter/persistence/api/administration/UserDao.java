package eu.europa.ec.etrustex.adapter.persistence.api.administration;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxUser;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBase;

public interface UserDao extends DaoBase<EtxUser> {

    /**
     * Find the {@link EtxUser} for which {@link EtxUser#name} = {@param userName}
     *
     * @param userName Id of the {@link EtxUser}
     * @return {@link EtxUser} with the {@param userName}
     */
    EtxUser findUserByName(String userName);
}
package eu.europa.ec.etrustex.adapter.integration;

import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.domain.DomibusPropertyMetadataDTO;
import eu.domibus.ext.services.DomainContextExtService;
import eu.domibus.ext.services.PasswordEncryptionExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.util.ETrustExPluginAS4Properties;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static eu.domibus.ext.domain.DomibusPropertyMetadataDTO.Type.*;
import static eu.domibus.ext.domain.DomibusPropertyMetadataDTO.Usage.GLOBAL;
import static eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties.ETXBACKEND_PLUGIN_BEAN_NAME;

/**
 * @author Arun Raj
 * @version 1.0
 */
@Service(ETXBACKEND_PLUGIN_BEAN_NAME)
public class ETrustExBackendPluginProperties extends ETrustExPluginAS4Properties {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ETrustExBackendPluginProperties.class);
    public static final String ETXBACKEND_PLUGIN_BEAN_NAME = "etxBackendPluginProperties";

    public static final String DEFAULT = "default";
    public static final String ETX_BACKEND_PLUGIN_MODULE = "ETrustEx_Backend_Plugin_Module";

    private Map<String, DomibusPropertyMetadataDTO> knownProperties;

    @Autowired
    private DomainContextExtService domainContextExtService;
    @Autowired
    private PasswordEncryptionExtService passwordEncryptionExtService;


    @PostConstruct
    public void init() {
        LOG.info("");
        LOG.info("                            ######## ######## ########  ##     ##  ######  ######## ######## ##     ## ");
        LOG.info("                            ##          ##    ##     ## ##     ## ##    ##    ##    ##        ##   ##  ");
        LOG.info("                            ##          ##    ##     ## ##     ## ##          ##    ##         ## ##   ");
        LOG.info("                            ######      ##    ########  ##     ##  ######     ##    ######      ###    ");
        LOG.info("                            ##          ##    ##   ##   ##     ##       ##    ##    ##         ## ##   ");
        LOG.info("                            ##          ##    ##    ##  ##     ## ##    ##    ##    ##        ##   ##  ");
        LOG.info("                            ########    ##    ##     ##  #######   ######     ##    ######## ##     ## ");
        LOG.info("");
        LOG.info(" ########     ###     ######  ##    ## ######## ##    ## ########              ########  ##       ##     ##  ######   #### ##    ## ");
        LOG.info(" ##     ##   ## ##   ##    ## ##   ##  ##       ###   ## ##     ##             ##     ## ##       ##     ## ##    ##   ##  ###   ## ");
        LOG.info(" ##     ##  ##   ##  ##       ##  ##   ##       ####  ## ##     ##             ##     ## ##       ##     ## ##         ##  ####  ## ");
        LOG.info(" ########  ##     ## ##       #####    ######   ## ## ## ##     ##   #######   ########  ##       ##     ## ##   ####  ##  ## ## ## ");
        LOG.info(" ##     ## ######### ##       ##  ##   ##       ##  #### ##     ##             ##        ##       ##     ## ##    ##   ##  ##  #### ");
        LOG.info(" ##     ## ##     ## ##    ## ##   ##  ##       ##   ### ##     ##             ##        ##       ##     ## ##    ##   ##  ##   ### ");
        LOG.info(" ########  ##     ##  ######  ##    ## ######## ##    ## ########              ##        ########  #######   ######   #### ##    ## ");
        LOG.info("");
        LOG.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        LOG.info("+         " + getDisplayVersion() + "        +");
        LOG.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    }

    public ETrustExBackendPluginProperties() {
        List<DomibusPropertyMetadataDTO> allProperties = Arrays.asList(
                new DomibusPropertyMetadataDTO(ARTIFACT_VERSION, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(ARTIFACT_NAME, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(BUILD_TIME, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(USER, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, false),
                new DomibusPropertyMetadataDTO(PWD, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, false),
                new DomibusPropertyMetadataDTO(DOMAIN, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, false),
                new DomibusPropertyMetadataDTO(ENCRYPTION_PROPS, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(DEFAULT_THIRD_PARTY_SYSTEM, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(CONNECTION_TIMEOUT, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(RECEIVE_TIMEOUT, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(POST_UPLOAD_FREQUENCY_CRON, CRON, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(CLEAN_REPOSITORY_FREQUENCY_CRON, CRON, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(NOTIFY_BACKEND_FREQUENCY_CRON, CRON, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(CLEAN_ATTACHMENT_FREQUENCY_CRON, CRON, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(CLEAN_ATTACHMENT_STATE_FREQUENCY_CRON, CRON, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(CLEAN_ATTACHMENT_STATE_IGNORE_BEFORE, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(CLEAN_ATTACHMENT_STATE_MIN_AGE, NUMERIC, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(CLEAN_ATTACHMENT_STATE_BATCH_SIZE, NUMERIC, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(CLEANUP_TIMEOUT, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(CLEAN_UP_MILLIS, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(DEFAULT_RECEIVE_TIMEOUT, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(LISTENER_RECEIVE_TIMEOUT, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(OUT_DOWNLOAD_ATT_LISTENER_CONCURRENT_CONSUMERS, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(OUT_DOWNLOAD_ATT_LISTENER_TIMEOUT, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(SEND_MESSAGE_LISTENER_CONCURRENT_CONSUMERS, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(SEND_MESSAGE_LISTENER_TIMEOUT, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(OUT_UPLOAD_ATT_LISTENER_CONCURRENT_CONSUMERS, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(OUT_UPLOAD_ATT_LISTENER_TIMEOUT, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(INBOX_UPLOAD_ATT_LISTENER_CONCURRENT_CONSUMERS, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(INBOX_UPLOAD_ATT_LISTENER_TIMEOUT, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(POST_UPLOAD_LISTENER_TIMEOUT, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(POST_PROCESS_LISTENER_CONCURRENT_CONSUMERS, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(POST_PROCESS_LISTENER_TIMEOUT, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(NOTIFY_LISTENER_TIMEOUT, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(ERROR_TRANSACTION_TIMEOUT, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(DLQ_TRANSACTION_CONCURRENT_CONSUMERS, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(DLQ_TRANSACTION_TIMEOUT, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(PLUGIN_OUTBOX_SERVICE_ENDPOINT_SCHEMA_VALIDATION_ENABLED, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(PLUGIN_INBOX_SERVICE_ENDPOINT_SCHEMA_VALIDATION_ENABLED, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(PLUGIN_RICA_SERVICE_ENDPOINT_SCHEMA_VALIDATION_ENABLED, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(REST_SERVICE_LIST_PENDING_OUTGOING_BUNDLE_MIN_AGE, STRING, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(REST_SERVICE_RETRIGGER_OTHER_IMPACTED_OUTGOING_BUNDLES, BOOLEAN, ETX_BACKEND_PLUGIN_MODULE, GLOBAL, true)

        );
        knownProperties = allProperties.stream().collect(Collectors.toMap(DomibusPropertyMetadataDTO::getName, x -> x));
        knownProperties.putAll(super.getKnownProperties());
    }

    public static final String ARTIFACT_VERSION = "Etx-BE-Artifact-Version";
    public static final String ARTIFACT_NAME = "Etx-BE-Artifact-Name";
    public static final String BUILD_TIME = "Etx-BE-Build-Time";

    public static final String USER = "etx.adapter.backend.user";
    public static final String PWD = "etx.adapter.backend.pwd";
    public static final String DOMAIN = "etx.adapter.backend.domain";

    public static final String ENCRYPTION_PROPS = "etx.adapter.password.encryption.properties";

    public static final String DEFAULT_THIRD_PARTY_SYSTEM = "etx.adapter.backend.defaultThirdPartySystem";

    public static final String CONNECTION_TIMEOUT = "etx.adapter.backend.connection.timeout";
    public static final String RECEIVE_TIMEOUT = "etx.adapter.backend.receive.timeout";

    public static final String POST_UPLOAD_FREQUENCY_CRON = "etx.adapter.postupload.frequency.cron";
    public static final String CLEAN_REPOSITORY_FREQUENCY_CRON = "etx.adapter.clean.repository.frequency.cron";
    public static final String NOTIFY_BACKEND_FREQUENCY_CRON = "etx.adapter.notify.frequency.cron";
    public static final LocalDate CLEAN_ATTACHMENT_STATE_IGNORE_BEFORE_DEFAULT = LocalDate.of(2022, 11, 4);
    public static final String CLEAN_ATTACHMENT_FREQUENCY_CRON = "etx.adapter.backend.clean.attachment.cron";
    public static final String CLEAN_ATTACHMENT_STATE_FREQUENCY_CRON = "etx.adapter.backend.clean.attachment.state.cron";
    public static final String CLEAN_ATTACHMENT_STATE_IGNORE_BEFORE = "etx.adapter.backend.clean.attachment.state.ignoreBefore";
    public static final String CLEAN_ATTACHMENT_STATE_MIN_AGE = "etx.adapter.backend.clean.attachment.state.minAge";
    public static final String CLEAN_ATTACHMENT_STATE_BATCH_SIZE = "etx.adapter.backend.clean.attachment.state.batchSize";
    public static final String CLEANUP_TIMEOUT = "etx.adapter.backend.cleanup.timeout";
    public static final String CLEAN_UP_MILLIS = "etx.adapter.cleanup.sleep";


    public static final String DEFAULT_RECEIVE_TIMEOUT = "etx.adapter.default.receive.timeout";
    public static final String LISTENER_RECEIVE_TIMEOUT = "etx.adapter.listener.receive.timeout";

    public static final String OUT_DOWNLOAD_ATT_LISTENER_CONCURRENT_CONSUMERS = "etx.adapter.outDownloadAttachmentListenerContainer.concurrentConsumers";
    public static final String OUT_DOWNLOAD_ATT_LISTENER_TIMEOUT = "etx.adapter.outDownloadAttachmentListenerContainer.transaction.timeout";
    public static final String SEND_MESSAGE_LISTENER_CONCURRENT_CONSUMERS = "etx.adapter.sendMessageListenerContainer.concurrentConsumers";
    public static final String SEND_MESSAGE_LISTENER_TIMEOUT = "etx.adapter.sendMessageListenerContainer.transaction.timeout";
    public static final String OUT_UPLOAD_ATT_LISTENER_CONCURRENT_CONSUMERS = "etx.adapter.outUploadAttachmentListenerContainer.concurrentConsumers";
    public static final String OUT_UPLOAD_ATT_LISTENER_TIMEOUT = "etx.adapter.outUploadAttachmentListenerContainer.transaction.timeout";
    public static final String INBOX_UPLOAD_ATT_LISTENER_CONCURRENT_CONSUMERS = "etx.adapter.inboxUploadAttachmentListenerContainer.concurrentConsumers";
    public static final String INBOX_UPLOAD_ATT_LISTENER_TIMEOUT = "etx.adapter.inboxUploadAttachmentListenerContainer.transaction.timeout";
    public static final String POST_UPLOAD_LISTENER_TIMEOUT = "etx.adapter.postUploadListenerContainer.transaction.timeout";
    public static final String POST_PROCESS_LISTENER_CONCURRENT_CONSUMERS = "etx.adapter.postProcessListenerContainer.concurrentConsumers";
    public static final String POST_PROCESS_LISTENER_TIMEOUT = "etx.adapter.postProcessListenerContainer.transaction.timeout";
    public static final String NOTIFY_LISTENER_TIMEOUT = "etx.adapter.notifyListenerContainer.transaction.timeout";
    public static final String ERROR_TRANSACTION_TIMEOUT = "etx.adapter.error.transaction.timeout";
    public static final String DLQ_TRANSACTION_CONCURRENT_CONSUMERS = "etx.adapter.dlq.concurrentConsumers";
    public static final String DLQ_TRANSACTION_TIMEOUT = "etx.adapter.dlq.transaction.timeout";

    public static final String PLUGIN_OUTBOX_SERVICE_ENDPOINT_SCHEMA_VALIDATION_ENABLED = "etx.adapter.pluginOutBoxServiceEndPoint.schemaValidationEnabled";
    public static final String PLUGIN_INBOX_SERVICE_ENDPOINT_SCHEMA_VALIDATION_ENABLED = "etx.adapter.pluginInBoxServiceEndPoint.schemaValidationEnabled";
    public static final String PLUGIN_RICA_SERVICE_ENDPOINT_SCHEMA_VALIDATION_ENABLED = "etx.adapter.retrieveInterchangeAgreementsEndPoint.schemaValidationEnabled";

    public static final String REST_SERVICE_LIST_PENDING_OUTGOING_BUNDLE_MIN_AGE = "etx.adapter.listPendingOutgoingBundle.minAge";
    public static final String REST_SERVICE_RETRIGGER_OTHER_IMPACTED_OUTGOING_BUNDLES = "etx.adapter.retriggerOtherImpactedOutgoingBundles";

    //Domibus properties referred
    public static final String ENCRYPTION_DOMIBUS_ACTIVE = "domibus.password.encryption.active";
    public static final String INTERNAL_QUEUE_CONCURRENCY = "domibus.internal.queue.concurency";


    public String getConnectionTimeout() {
        return domibusPropertyExtService.getProperty(CONNECTION_TIMEOUT);
    }

    public String getReceiveTimeout() {
        return domibusPropertyExtService.getProperty(RECEIVE_TIMEOUT);
    }

    public String getSystemDefaultName() {
        return domibusPropertyExtService.getProperty(DEFAULT_THIRD_PARTY_SYSTEM);
    }

    public String getUser() {
        return domibusPropertyExtService.getProperty(USER);
    }

    public String getPwd() {
        return passwordEncryptionExtService.decryptProperty(
                domainContextExtService.getCurrentDomain(),
                PWD,
                domibusPropertyExtService.getProperty(PWD));
    }

    public String getDomain() {
        String resolvedProperty = domibusPropertyExtService.getProperty(DOMAIN);
        if (StringUtils.isBlank(resolvedProperty)) {
            return DEFAULT;
        }
        return resolvedProperty;
    }

    public int getCleanupTimeout() {
        return getInt(domibusPropertyExtService.getProperty(CLEANUP_TIMEOUT));
    }

    public String getInternalQueueConcurrency() {
        return domibusPropertyExtService.getProperty(INTERNAL_QUEUE_CONCURRENCY);
    }

    public Long getListenerReceiveTimeout() {
        return getLong(domibusPropertyExtService.getProperty(LISTENER_RECEIVE_TIMEOUT));
    }

    public long getDefaultReceiveTimeout() {
        return getLong(domibusPropertyExtService.getProperty(DEFAULT_RECEIVE_TIMEOUT));
    }

    private long getLong(String resolvedProperty) {
        if (StringUtils.isBlank(resolvedProperty)) {
            return 0;
        }
        return Long.parseLong(resolvedProperty);
    }

    private int getInt(String resolvedProperty) {
        if (StringUtils.isBlank(resolvedProperty)) {
            return 0;
        }
        return Integer.parseInt(resolvedProperty);
    }

    public String getOutDownloadAttachmentListenerContainerConcurrentConsumers() {
        return domibusPropertyExtService.getProperty(OUT_DOWNLOAD_ATT_LISTENER_CONCURRENT_CONSUMERS);
    }

    public int getOutDownloadAttachmentTransactionTimeout() {
        return getInt(domibusPropertyExtService.getProperty(OUT_DOWNLOAD_ATT_LISTENER_TIMEOUT));
    }

    public String getSendMessageListenerContainerConcurrentConsumers() {
        return domibusPropertyExtService.getProperty(SEND_MESSAGE_LISTENER_CONCURRENT_CONSUMERS);
    }

    public int getSendMessageTransactionTimeout() {
        return getInt(domibusPropertyExtService.getProperty(SEND_MESSAGE_LISTENER_TIMEOUT));
    }

    public String getOutUploadAttachmentListenerContainerConcurrentConsumers() {
        return domibusPropertyExtService.getProperty(OUT_UPLOAD_ATT_LISTENER_CONCURRENT_CONSUMERS);
    }

    public int getOutUploadAttachmentTransactionTimeout() {
        return getInt(domibusPropertyExtService.getProperty(OUT_UPLOAD_ATT_LISTENER_TIMEOUT));
    }

    public String getInboxUploadAttachmentListenerContainerConcurrentConsumers() {
        return domibusPropertyExtService.getProperty(INBOX_UPLOAD_ATT_LISTENER_CONCURRENT_CONSUMERS);
    }

    public int getInboxUploadAttachmentTransactionTimeout() {
        return getInt(domibusPropertyExtService.getProperty(INBOX_UPLOAD_ATT_LISTENER_TIMEOUT));
    }

    public int getPostUploadTimeout() {
        return getInt(domibusPropertyExtService.getProperty(POST_UPLOAD_LISTENER_TIMEOUT));
    }

    public int getNotifyTransactionTimeout() {
        return getInt(domibusPropertyExtService.getProperty(NOTIFY_LISTENER_TIMEOUT));
    }

    public String getPostProcessListenerContainerConcurrentConsumers() {
        return domibusPropertyExtService.getProperty(POST_PROCESS_LISTENER_CONCURRENT_CONSUMERS);
    }

    public int getPostProcessTimeout() {
        return getInt(domibusPropertyExtService.getProperty(POST_PROCESS_LISTENER_TIMEOUT));
    }

    public LocalDateTime getCleanAttachementStateIgnoreBeforeDate() {
        return getDate(domibusPropertyExtService.getProperty(CLEAN_ATTACHMENT_STATE_IGNORE_BEFORE), CLEAN_ATTACHMENT_STATE_IGNORE_BEFORE_DEFAULT);
    }

    public int getCleanAttachementStateMinAge() {
        return getInt(domibusPropertyExtService.getProperty(CLEAN_ATTACHMENT_STATE_MIN_AGE));
    }
    public int getCleanAttachementStateBatchSize() {
        return getInt(domibusPropertyExtService.getProperty(CLEAN_ATTACHMENT_STATE_BATCH_SIZE));
    }

    private LocalDateTime getDate(String stringDate, LocalDate defaultValue) {
        try {
            return LocalDate.parse(stringDate).atStartOfDay();
        } catch (Exception e) {
            LOG.warn("Could not parse the date [{}]. use default Value: [{}]", stringDate, defaultValue);
            return defaultValue.atStartOfDay();
        }
    }

    public int getErrorTransactionTimeout() {
        return getInt(domibusPropertyExtService.getProperty(ERROR_TRANSACTION_TIMEOUT));
    }

    public String getDlqConcurrentConsumers() {
        return domibusPropertyExtService.getProperty(DLQ_TRANSACTION_CONCURRENT_CONSUMERS);
    }

    public int getDlqTransactionTimeout() {
        return getInt(domibusPropertyExtService.getProperty(DLQ_TRANSACTION_TIMEOUT));
    }

    public int getCleanupSleepMillis() {
        return getInt(domibusPropertyExtService.getProperty(CLEAN_UP_MILLIS));
    }

    public Integer getListPendingOutgoingBundleMinAge() {
        return domibusPropertyExtService.getIntegerProperty(REST_SERVICE_LIST_PENDING_OUTGOING_BUNDLE_MIN_AGE);
    }

    public Boolean shouldRetriggerOtherImpactedOutgoingMessageBundles(){
        return domibusPropertyExtService.getBooleanProperty(REST_SERVICE_RETRIGGER_OTHER_IMPACTED_OUTGOING_BUNDLES);
    }

    private String getDisplayVersion() {
        StringBuilder display = new StringBuilder();
        display.append(domibusPropertyExtService.getProperty(ARTIFACT_NAME));
        display.append(" Version [");
        display.append(domibusPropertyExtService.getProperty(ARTIFACT_VERSION));
        display.append("] Build-Time [");
        display.append(domibusPropertyExtService.getProperty(BUILD_TIME));
        display.append("]");
        return display.toString();
    }

    public boolean isPasswordEncryptionActiveDomibus(DomainDTO domain) {
        return BooleanUtils.isTrue(BooleanUtils.toBoolean(domibusPropertyExtService.getProperty(domain, ENCRYPTION_DOMIBUS_ACTIVE)));
    }

    public String getProperty(String propertyName) {
        return domibusPropertyExtService.getProperty(propertyName);
    }

    public String getEncryptedProperties() {
        return domibusPropertyExtService.getProperty(ENCRYPTION_PROPS);
    }

}
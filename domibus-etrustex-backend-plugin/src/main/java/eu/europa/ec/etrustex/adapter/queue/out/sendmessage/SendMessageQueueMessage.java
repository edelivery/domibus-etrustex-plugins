package eu.europa.ec.etrustex.adapter.queue.out.sendmessage;

import eu.europa.ec.etrustex.common.jms.QueueMessage;

/**
 * @author: micleva
 * @date: 11/15/12 2:21 PM
 * @project: ETX
 */
public class SendMessageQueueMessage extends QueueMessage {

    public enum Type {
        BUNDLE,

        STATUS
    }

    private Long messageId;

    private final Type type;

    public SendMessageQueueMessage(Type type) {
        this.type = type;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SendMessageQueueMessage");
        sb.append("{messageId=").append(messageId);
        sb.append(", type=").append(type);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SendMessageQueueMessage that = (SendMessageQueueMessage) o;

        if (messageId != null ? !messageId.equals(that.messageId) : that.messageId != null) return false;
        if (type != that.type) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = messageId != null ? messageId.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}

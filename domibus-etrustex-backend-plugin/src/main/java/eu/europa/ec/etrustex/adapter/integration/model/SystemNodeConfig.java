package eu.europa.ec.etrustex.adapter.integration.model;

/**
 * @author: micleva
 * @date: 6/21/13 8:31 AM
 * @project: ETX
 */
public class SystemNodeConfig {

    private String nodeUsername;
    private String nodePassword;

    public String getNodeUsername() {
        return nodeUsername;
    }

    public void setNodeUsername(String nodeUsername) {
        this.nodeUsername = nodeUsername;
    }

    public String getNodePassword() {
        return nodePassword;
    }

    public void setNodePassword(String nodePassword) {
        this.nodePassword = nodePassword;
    }
}

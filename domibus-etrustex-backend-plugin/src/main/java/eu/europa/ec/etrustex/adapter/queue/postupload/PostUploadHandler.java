package eu.europa.ec.etrustex.adapter.queue.postupload;

/**
 * @author micleva
 * @project ETX
 */

public interface PostUploadHandler {
    void handlePostUpload(Long messageId);

    void validatePostUpload(Long messageId);

    void onError(Long messageId);
}

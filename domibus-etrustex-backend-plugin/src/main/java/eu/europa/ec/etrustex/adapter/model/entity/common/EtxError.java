package eu.europa.ec.etrustex.adapter.model.entity.common;

import eu.europa.ec.etrustex.adapter.model.entity.AuditEntity;

import javax.persistence.*;
import java.io.Serializable;
/**
 * @author micleva
 * @project ETX
 */

/**
 * <p>
 * Title: Error
 * </p>
 * <p/>
 * <p>
 * Description: Domain Object describing a Error entity
 * </p>
 */
@Entity(name = "EtxError")
@AttributeOverrides({
        @AttributeOverride(name = "creationDate", column = @Column(name = "ERR_CREATED_ON", insertable = false, updatable = false, nullable = false)),
        @AttributeOverride(name = "modificationDate", column = @Column(name = "ERR_UPDATED_ON"))})
@Table(name = "ETX_ADT_ERROR")
public class EtxError extends AuditEntity<Long> implements Serializable {

    private static final long serialVersionUID = -7075544100295913081L;
    static final int MAX_DETAIL = 2500;

    @Id
    @Column(name = "ERR_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ERR_RSP_CODE", nullable = false, updatable = false)
    private String responseCode;

    @Column(name = "ERR_DETAIL", length = MAX_DETAIL,updatable = false)
    private String detail;

    /**
     * Default constructor
     */
    public EtxError() {
        // Default Constructor
    }

    public EtxError(String responseCode, String detail) {
        this.responseCode = responseCode;
        this.detail = detail;
    }

    public String getDetail() {
        return detail;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String errorType) {
        this.responseCode = errorType;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("EtxError");
        sb.append("{id=").append(id);
        sb.append(", responseCode=").append(responseCode);
        sb.append(", detail='").append(detail).append('\'');
        sb.append('}');
        return sb.toString();
    }

}

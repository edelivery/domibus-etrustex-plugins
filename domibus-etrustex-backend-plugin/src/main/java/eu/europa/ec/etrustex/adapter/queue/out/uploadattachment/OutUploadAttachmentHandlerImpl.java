package eu.europa.ec.etrustex.adapter.queue.out.uploadattachment;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.NodeInvocationManager;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.queue.postupload.PostUploadProducer;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.service.WorkspaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.Path;
import java.util.Collections;

/**
 * @author micleva, Arun Raj
 * @version 1.0
 */

@Service("etxBackendPluginOutUploadAttachmentHandler")
@Transactional
public class OutUploadAttachmentHandlerImpl implements OutUploadAttachmentHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(OutUploadAttachmentHandlerImpl.class);
    public static final String ATTACHMENT_FILE_NOT_IN_WORKSPACE_ERROR_PREFIX = "Wrapper upload to ETX Node failed for attachment ID [";
    public static final String ATTACHMENT_FILE_NOT_IN_WORKSPACE_ERROR_SUFFIX = "], since attachment file was not found in the backend plugin workspace. Probably download was skipped in previous step to avoid duplicate transmissions.";

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private NodeInvocationManager nodeInvocationManager;

    @Autowired
    private PostProcessProducer postProcessProducer;

    @Autowired
    private PostUploadProducer postUploadProducer;

    @Autowired
    @Qualifier("etxBackendPluginWorkspaceService")
    private WorkspaceService workspaceService;

    @Override
    public void handleUploadAttachment(Long attachmentId) {
        EtxAttachment attachmentEntity = attachmentService.findById(attachmentId);

        if (attachmentEntity.getStateType() == AttachmentState.ATTS_DOWNLOADED) {
            EtxMessage messageBundleEntity = attachmentEntity.getMessage();
            EtxParty sender = messageBundleEntity.getSender();
            EtxSystem senderSystem = sender.getSystem();
            EtxParty receiver = messageBundleEntity.getReceiver();

            if (!attachmentService.checkAttachmentForSystemInState(attachmentEntity, senderSystem, Collections.singletonList(AttachmentState.ATTS_UPLOADED))) {

                //read from WKS
                Path attachmentFile = workspaceService.getFile(attachmentId);
                if (attachmentFile == null || !attachmentFile.toFile().exists()) {
                    LOG.warn("Attachment id [{}] having UUID [{}] from sender system [{}] between sender [{}] and receiver [{}] was not found in the workspace folder of the backend plugin. Probably download was skipped in previous step to avoid duplicate transmissions.",
                            attachmentEntity.getId(),
                            attachmentEntity.getAttachmentUuid(),
                            senderSystem.getName(),
                            sender.getPartyUuid(),
                            receiver.getPartyUuid());
                    throw new ETrustExPluginException(ETrustExError.ETX_007, ATTACHMENT_FILE_NOT_IN_WORKSPACE_ERROR_PREFIX + attachmentEntity.getAttachmentUuid() + "] between sender [" + sender.getPartyUuid() + "] and receiver [" + receiver.getPartyUuid() + ATTACHMENT_FILE_NOT_IN_WORKSPACE_ERROR_SUFFIX);
                }

                //Upload the attachment to ETX_Node
                nodeInvocationManager.uploadAttachment(attachmentEntity, attachmentFile.toFile());
                LOG.info("Successfully sent StoreDocumentWrapper request to ETrustEx Node plugin for Attachment with UUID: [{}]; sender system: [{}]",
                        attachmentEntity.getAttachmentUuid(),
                        senderSystem.getName());
            } else {
                // If attachment was already uploaded, no Domibus interaction is required. Mark attachment as already uploaded and proceed with next stage.
                attachmentService.updateAttachmentState(attachmentEntity, AttachmentState.ATTS_DOWNLOADED, AttachmentState.ATTS_ALREADY_UPLOADED);
                LOG.warn("Attachment with UUID: [{}] was already uploaded; sender system: [{}]; upload invocation ignored.", attachmentEntity.getAttachmentUuid(), senderSystem.getName());
                postUploadProducer.checkAttachmentsAndTriggerPostUpload(attachmentEntity);
            }
        }
    }

    @Override
    public void validateUploadAttachment(Long attachmentId) {
        EtxAttachment attachmentEntity = attachmentService.findById(attachmentId);

        if (attachmentEntity == null) {
            String errorDetails = String.format("Attachment [ID: %s] not found", attachmentId);
            LOG.warn(errorDetails);
            throw new ETrustExPluginException(ETrustExError.DEFAULT, errorDetails, null);
        }

        EtxMessage messageBundleEntity = attachmentEntity.getMessage();

        //get the message and attachment state
        final MessageState messageState = messageBundleEntity.getMessageState();
        final AttachmentState attachmentState = attachmentEntity.getStateType();

        boolean isExpectedMessageState = messageState == MessageState.MS_CREATED;
        boolean isExpectedAttachmentState = attachmentState == AttachmentState.ATTS_DOWNLOADED;

        if (!isExpectedMessageState || !isExpectedAttachmentState) {
            StringBuilder errorDetails = new StringBuilder();
            errorDetails
                    .append("Invalid message bundle or attachment states (expected message bundle state: MS_CREATED, expected attachment states: ATTS_DOWNLOADED, ATTS_IGNORED, skipping uploading attachment (to Node) ID: ");
            errorDetails.append(attachmentId);
            errorDetails.append(", state: ");
            errorDetails.append(attachmentState);
            errorDetails.append(" regarding message bundle with ID: ");
            errorDetails.append(messageBundleEntity.getId());
            errorDetails.append(", state: ");
            errorDetails.append(messageState);

            LOG.warn(errorDetails.toString());
            throw new ETrustExPluginException(ETrustExError.ETX_009, errorDetails.toString(), null);
        }
    }

    @Override
    public void onError(Long attachmentId) {

        EtxAttachment etxAttachment = attachmentService.findById(attachmentId);
        if (etxAttachment.getError() == null) {
            attachmentService.attachmentInError(attachmentId, ETrustExError.DEFAULT.getCode(), "Failed to upload attachment " + etxAttachment.getAttachmentUuid());
        }
        postProcessProducer.triggerPostProcess(etxAttachment.getMessage().getId(), attachmentId);
    }

}
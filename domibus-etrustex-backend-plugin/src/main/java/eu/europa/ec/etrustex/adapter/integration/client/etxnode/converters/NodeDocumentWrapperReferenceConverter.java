package eu.europa.ec.etrustex.adapter.integration.client.etxnode.converters;

import ec.schema.xsd.commonaggregatecomponents_2.DigestMethodType;
import ec.schema.xsd.commonaggregatecomponents_2.DocumentSizeType;
import ec.schema.xsd.commonaggregatecomponents_2.DocumentWrapperReferenceType;
import ec.schema.xsd.commonaggregatecomponents_2.ResourceInformationReferenceType;
import eu.europa.ec.etrustex.common.model.entity.attachment.AttachmentType;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.ChecksumAlgorithmType;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DocumentHashType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DocumentTypeCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.NameType;

import javax.xml.bind.DatatypeConverter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class NodeDocumentWrapperReferenceConverter {

    private NodeDocumentWrapperReferenceConverter() {
    }

    public static List<DocumentWrapperReferenceType> convertEtxAttachmentList(List<EtxAttachment> etxAttachmentList) {
        List<DocumentWrapperReferenceType> documentWrapperReferenceTypeList = null;
        if (etxAttachmentList != null) {
            documentWrapperReferenceTypeList = new ArrayList<>();
            for (EtxAttachment etxAttachment : etxAttachmentList) {
                documentWrapperReferenceTypeList.add(convertEtxAttachment(etxAttachment));
            }
        }
        return documentWrapperReferenceTypeList;
    }

    public static DocumentWrapperReferenceType convertEtxAttachment(EtxAttachment etxAttachment) {
        DocumentWrapperReferenceType attachmentSdo = new DocumentWrapperReferenceType();

        IDType id = new IDType();
        id.setValue(etxAttachment.getAttachmentUuid());
        attachmentSdo.setID(id);

        DocumentTypeCodeType documentTypeCode = new DocumentTypeCodeType();
        documentTypeCode.setValue(etxAttachment.getAttachmentType().name());
        attachmentSdo.setDocumentTypeCode(documentTypeCode);

        ResourceInformationReferenceType resourceInformationReferenceType = new ResourceInformationReferenceType();

        DocumentHashType documentHashType = new DocumentHashType();
        documentHashType.setValue(DatatypeConverter.printHexBinary(etxAttachment.getChecksum()));
        resourceInformationReferenceType.setDocumentHash(documentHashType);

        DocumentSizeType documentSizeType = new DocumentSizeType();
        documentSizeType.setValue(new BigDecimal(etxAttachment.getFileSize()));
        resourceInformationReferenceType.setDocumentSize(documentSizeType);

        DigestMethodType digestMethodType = new DigestMethodType();
        digestMethodType.setValue(etxAttachment.getChecksumAlgorithmType().getValue());
        resourceInformationReferenceType.setDocumentHashMethod(digestMethodType);

        NameType nameType = new NameType();
        nameType.setValue(etxAttachment.getFileName());
        resourceInformationReferenceType.setName(nameType);

        attachmentSdo.setResourceInformationReference(resourceInformationReferenceType);

        return attachmentSdo;
    }

    public static EtxAttachment toEtxAttachment(DocumentWrapperReferenceType documentWrapperReference) {
        EtxAttachment etxAttachment = new EtxAttachment();

        if (documentWrapperReference.getID() != null) {
            etxAttachment.setAttachmentUuid(documentWrapperReference.getID().getValue());
        }


        if (documentWrapperReference.getDocumentTypeCode() != null) {
            etxAttachment.setAttachmentType(AttachmentType.valueOf(documentWrapperReference.getDocumentTypeCode().getValue()));
        }

        if (documentWrapperReference.getResourceInformationReference() != null) {
            ResourceInformationReferenceType referenceType = documentWrapperReference.getResourceInformationReference();
            if (referenceType.getDocumentHash() != null) {
                etxAttachment.setChecksum(DatatypeConverter.parseHexBinary(referenceType.getDocumentHash().getValue()));
            }

            if (referenceType.getDocumentHashMethod() != null) {
                etxAttachment.setChecksumAlgorithmType(ChecksumAlgorithmType.fromValue(referenceType.getDocumentHashMethod().getValue()));
            }

            if (referenceType.getDocumentSize() != null) {
                etxAttachment.setFileSize(referenceType.getDocumentSize().getValue().longValue());
            }

            if (referenceType.getName() != null) {
                etxAttachment.setFileName(referenceType.getName().getValue());
            }
        }

        return etxAttachment;
    }
}

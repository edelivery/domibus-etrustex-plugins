package eu.europa.ec.etrustex.adapter.queue.out.downloadattachment;

/**
 * @author micleva
 * @project ETX
 */

public interface OutDownloadAttachmentHandler {

    void validateDownloadAttachment(Long attachmentId);

    void handleDownloadAttachment(Long attachmentId);

    void onError(Long attachmentId);
}
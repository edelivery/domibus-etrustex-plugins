package eu.europa.ec.etrustex.adapter.queue.deadletter;

import eu.europa.ec.etrustex.adapter.queue.JmsProducer;
import eu.europa.ec.etrustex.common.jms.QueueMessage;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.jms.Queue;

@Service("etxBackendPluginDeadLetterProducer")
public class DeadLetterProducer {

    private final Queue jmsQueue;

    private final JmsProducer<QueueMessage> jmsProducer;

    public DeadLetterProducer(@Qualifier("etxBackendPluginDeadLetter") Queue jmsQueue,
                              JmsProducer<QueueMessage> jmsProducer) {
        this.jmsQueue = jmsQueue;
        this.jmsProducer = jmsProducer;
    }

    public void triggerDeadLetter(QueueMessage queueMessage) {
        jmsProducer.postMessage(jmsQueue, queueMessage);
    }
}
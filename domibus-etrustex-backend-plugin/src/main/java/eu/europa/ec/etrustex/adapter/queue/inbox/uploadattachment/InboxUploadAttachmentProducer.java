package eu.europa.ec.etrustex.adapter.queue.inbox.uploadattachment;

import eu.europa.ec.etrustex.adapter.queue.JmsProducer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Queue;

/**
 * Service to centralize triggers to {@code}InboxUploadAttachment{@code} JMS queue.
 *
 * @author  Arun Raj
 * @version 1.0
 * @since Oct 2017
 */
@Service("etxBackendPluginInboxUploadAttachmentProducer")
public class InboxUploadAttachmentProducer {

    private final Queue jmsQueue;

    private final JmsProducer<InboxUploadAttachmentQueueMessage> jmsProducer;

    public InboxUploadAttachmentProducer(@Qualifier("etxBackendPluginInboxUploadAttachment") Queue jmsQueue,
                                         JmsProducer<InboxUploadAttachmentQueueMessage> jmsProducer) {
        this.jmsQueue = jmsQueue;
        this.jmsProducer = jmsProducer;
    }

    /**
     * Create a trigger of type {@link InboxUploadAttachmentQueueMessage} to {@code}InboxUploadAttachment{@code} JMS queue.
     *
     * @param attachmentId ID of ETX_ADT_ATTACHMENT table
     * @throws JMSException
     */
    public void triggerUpload(Long attachmentId) {
        InboxUploadAttachmentQueueMessage queueMessage = new InboxUploadAttachmentQueueMessage();
        queueMessage.setAttachmentId(attachmentId);
        jmsProducer.postMessage(jmsQueue, queueMessage);
    }
}

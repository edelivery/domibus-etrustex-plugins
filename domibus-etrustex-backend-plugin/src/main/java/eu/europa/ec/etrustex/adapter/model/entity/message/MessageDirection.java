package eu.europa.ec.etrustex.adapter.model.entity.message;

/**
 * @author apladap
 */
public enum MessageDirection {
    IN, OUT;

    public MessageDirection getOpposite() {
        return this == IN ? OUT : IN;
    }
}

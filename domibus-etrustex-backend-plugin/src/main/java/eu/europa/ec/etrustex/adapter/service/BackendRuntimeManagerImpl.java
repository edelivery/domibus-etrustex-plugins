package eu.europa.ec.etrustex.adapter.service;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.client.backend.repository.LocalSharedFileRepositoryService;
import eu.europa.ec.etrustex.common.service.CacheService;
import eu.europa.ec.etrustex.common.service.RuntimeManager;
import eu.europa.ec.etrustex.common.service.WorkspaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Implementation of facade to combine several runtime operations
 *
 * @author Valer Micle
 * @project ETX
 *
 * @author Arun Raj
 * @version 1.0
 * @since 18/07/2017
 */
@Service("etxBackendPluginRuntimeManager")
public class BackendRuntimeManagerImpl implements RuntimeManager {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(BackendRuntimeManagerImpl.class);

    @Autowired
    private CacheService cacheService;

    @Autowired
    @Qualifier("etxBackendPluginWorkspaceService")
    private WorkspaceService workspaceService;

    @Autowired
    private LocalSharedFileRepositoryService fileRepositoryService;

    /**
     * <ol>
     *     <li>Clears all the ehCache entries</li>
     *     <li>Reloads the workspace folder configured</li>
     *     <li>Reloads the local shared file repository configured</li>
     * </ol>
     * <br/>
     */
    @Override
    public void resetCache() {
        LOG.info("Proceed with resetting all caches");
        cacheService.clearCache();
        workspaceService.loadConfiguration();
        fileRepositoryService.loadConfiguration();
        LOG.info("All caches successfully reset");
    }
}

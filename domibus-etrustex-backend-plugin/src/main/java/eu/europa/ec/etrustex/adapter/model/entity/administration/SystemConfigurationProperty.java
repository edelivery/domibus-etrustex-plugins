package eu.europa.ec.etrustex.adapter.model.entity.administration;

/**
 * @author micleva
 * @project ETX
 */

public enum SystemConfigurationProperty {

    //Backend-Systems system configurations
    ETX_BACKEND_SERVICE_INBOX_NOTIFICATION_URL("etx.backend.services.InboxNotificationService.url"),
    ETX_BACKEND_SERVICE_FILE_REPOSITORY_URL("etx.backend.services.FileRepositoryService.url"),
    ETX_BACKEND_FILE_REPOSITORY_LOCATION("etx.backend.filerepositorylocation"),
    ETX_BACKEND_NOTIFICATION_TYPE("etx.backend.notification.type"),

    // required when attachments needs to be downloaded from the node
    ETX_BACKEND_DOWNLOAD_ATTACHMENTS("etx.backend.download.attachments");

    private String code;

    public static SystemConfigurationProperty fromCode(String code) {
        if (code != null) {
            for (SystemConfigurationProperty configurationProperty : SystemConfigurationProperty.values()) {
                if (configurationProperty.getCode().equals(code)) {
                    return configurationProperty;
                }
            }
        }

        throw new IllegalArgumentException("No configuration found for the input code: " + code);
    }

    SystemConfigurationProperty(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}

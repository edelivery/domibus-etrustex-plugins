package eu.europa.ec.etrustex.adapter.queue.postupload;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.persistence.api.message.MessageDao;
import eu.europa.ec.etrustex.adapter.queue.JmsProducer;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.Queue;

/**
 * Service handling triggers for PostUpload
 *
 * @author Arun Raj
 * @version 1.1.1
 * @since 28/06/2019
 */

@Service("etxBackendPluginPostUploadProducer")
public class PostUploadProducer {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PostUploadProducer.class);

    private final Queue jmsQueue;

    private final JmsProducer<PostUploadQueueMessage> jmsProducer;

    private final MessageDao messageDao;

    private final AttachmentService attachmentService;

    public PostUploadProducer(@Qualifier("etxBackendPluginPostUpload") Queue jmsQueue,
                              JmsProducer<PostUploadQueueMessage> jmsProducer,
                              MessageDao messageDao,
                              AttachmentService attachmentService) {
        this.jmsQueue = jmsQueue;
        this.jmsProducer = jmsProducer;
        this.messageDao = messageDao;
        this.attachmentService = attachmentService;
    }

    /**
     * For the specified {@link MessageType#MESSAGE_BUNDLE} update the state as {@link MessageState#MS_UPLOAD_REQUESTED} to avoid getting triggered again
     * and generate the JMS trigger for PostUpload.<br/>
     * <p>
     * NOTE: This function should only be called from {@link PostUploadHandlerImpl#handlePostUpload(Long)} or checkAttachmentsAndTriggerPostUpload(EtxAttachment).
     * Visibility is kept as public only for transaction management.<br/>
     *
     * @param etxMessage - The {@link MessageType#MESSAGE_BUNDLE} for which the PostUpload should be raised
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void triggerBundlePostUpload(EtxMessage etxMessage) {
        LOG.info("Triggering PostUpload for Message[ID: {}].", etxMessage.getId());
        etxMessage.setMessageState(MessageState.MS_UPLOAD_REQUESTED);
        messageDao.update(etxMessage);
        LOG.info("For Message[ID: {}], message state updated to MS_UPLOAD_REQUESTED.", etxMessage.getId());
        PostUploadQueueMessage queueMessage = new PostUploadQueueMessage(etxMessage.getId());
        jmsProducer.postMessage(jmsQueue, queueMessage);
        LOG.info("For Message[ID: {}], Successfully triggered PostUpload.", etxMessage.getId());
    }


    /**
     * For an {@link EtxAttachment} check in the corresponding MessageBundle, if all attachments are in final state and if true then trigger postupload for the bundle.
     *
     */
    @Transactional
    public void checkAttachmentsAndTriggerPostUpload(EtxAttachment etxAttachment) {
        LOG.debug("For Attachment:[ID: {}] checking if PostUpload can be triggered.", etxAttachment.getId());
        if (attachmentService.areAllAttachmentsInFinalState(etxAttachment.getMessage())) {
            triggerBundlePostUpload(etxAttachment.getMessage());
        }
    }
}

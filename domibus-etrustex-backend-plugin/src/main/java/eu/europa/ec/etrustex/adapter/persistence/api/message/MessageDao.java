package eu.europa.ec.etrustex.adapter.persistence.api.message;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.message.*;
import eu.europa.ec.etrustex.adapter.model.vo.MessageReferenceVO;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBase;

import java.util.Date;
import java.util.List;

/**
 * Data Access Object for {@link EtxMessage}
 *
 * @author François Gautier
 * @version 1.0
 * @since 12-Oct-17
 */
public interface MessageDao extends DaoBase<EtxMessage> {

    /**
     * Find unread {@link MessageReferenceVO} for a given {@link EtxParty} UUID
     * <p>
     * {@link EtxMessage#messageState} = {@link eu.europa.ec.etrustex.adapter.model.entity.message.MessageState#MS_PROCESSED}
     * {@link EtxMessage#notificationState} different from {@link NotificationState#NF_NOTIFIED});
     * {@link EtxMessage#directionType} is {@link MessageDirection#IN}
     *
     * @param partyUuid UUID of the {@link EtxParty}
     * @return list of {@link MessageReferenceVO}
     */
    List<MessageReferenceVO> findUnreadMessagesByReceiverPartyUuid(String partyUuid);

    /**
     * Return {@link EtxMessage}
     * with {@link EtxMessage#messageUuid}                              =  {@param messageUuid}
     * and  {@link EtxParty#partyUuid} of {@link EtxMessage#sender}     =  {@param senderPartyUuid}
     * and  {@link EtxParty#partyUuid} of {@link EtxMessage#receiver}   =  {@param receiverPartyUuid}
     * and  {@link EtxMessage#directionType}                            =  {@param direction}
     * and  {@link EtxMessage#messageType}                              in {@param types}
     *
     * @param messageUuid       UUID
     * @param types             {@link MessageType}
     * @param direction         {@link MessageDirection}
     * @param senderPartyUuid   {@link EtxParty#partyUuid} of {@link EtxMessage#sender}
     * @param receiverPartyUuid {@link EtxParty#partyUuid} of {@link EtxMessage#receiver}
     * @return {@link EtxMessage}
     */
    EtxMessage findEtxMessage(String messageUuid, List<MessageType> types, MessageDirection direction, String senderPartyUuid, String receiverPartyUuid);

    /**
     *
     * @param messageUuidList
     * @param types           = types of {@link MessageType}
     * @param direction       = types of {@link MessageDirection}
     * @param senderPartyUuid
     * @param receiverPartyUuid
     * @return list of {@link EtxMessage}
     */

    /**
     * @param notificationState of type {@link NotificationState} to filter out the {@link EtxMessage}
     * @return a {@link List<EtxMessage>} with the given {@link NotificationState}
     */
    List<EtxMessage> findMessagesByNotificationState(NotificationState notificationState);

    /**
     * Find {@link EtxMessage}
     * with {@link EtxMessage#messageUuid}                       =  {@param messageUuid}
     * and  {@link EtxParty#id} of {@link EtxMessage#sender}     =  {@param senderPartyId}
     * and  {@link EtxParty#id} of {@link EtxMessage#receiver}   =  {@param receiverPartyId}
     *
     * @param messageUuid     {@link EtxMessage#messageUuid}
     * @param senderPartyId   {@link EtxParty#id} of {@link EtxMessage#sender}
     * @param receiverPartyId {@link EtxParty#id} of {@link EtxMessage#sender}
     * @return {@link MessageReferenceVO} from found {@link EtxMessage}
     */
    List<MessageReferenceVO> findMessagesByUuidAndSenderAndReceiver(String messageUuid, Long senderPartyId, Long receiverPartyId);

    /**
     * To retrieve the entry from ETX_ADT_MESSAGE with a matching Domibus message Id
     *
     * @param domibusMessageID {@link EtxMessage#domibusMessageId}
     * @return EtxMessage
     */
    EtxMessage findMessageByDomibusMessageId(String domibusMessageID);


    /**
     * To find the list of {@link MessageType#MESSAGE_BUNDLE} that are in {@link MessageState#MS_CREATED} state which has
     * no more attachments in non final states - i.e. there are no attachments that are NOT in the states -
     * ({@link AttachmentState#ATTS_UPLOADED}, {@link AttachmentState#ATTS_ALREADY_UPLOADED}, {@link AttachmentState#ATTS_FAILED}, {@link AttachmentState#ATTS_IGNORED} )
     *
     * @return {@Link List<EtxMessage>}
     */
    List<EtxMessage> retrieveMessageBundlesForPostUpload();


    /**
     * For a given set of Attachment UUIDs that are not in ATTS_UPLOADED or ATTS_ALREADY_UPLOADED state,
     * find the list of impacted Outgoing Message Bundles other than those included in the retrigger list
     *
     * @param attachmentUUIDList
     * @param retriggerMessageBundleUUIDList
     * @return MessageReferenceVO list
     */
    List<MessageReferenceVO> findImpactedBundlesDueToOutgoingMessageBundleRetrigger(List<String> attachmentUUIDList, List<String> retriggerMessageBundleUUIDList);

    /**
     * To find the list of {@link MessageType#MESSAGE_BUNDLE} that are in {@link MessageState#MS_FAILED} state  ( {@param startDate} < {@link EtxMessage#modificationDate} AND ( {@param endDate} < {@link EtxMessage#modificationDate})
     * OR with {@link eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment} in state:
     * ({@link AttachmentState#ATTS_FAILED}, {@link AttachmentState#ATTS_CREATED}, {@link AttachmentState#ATTS_DOWNLOADED})
     * ( {@param startDate} < {@link EtxMessage#modificationDate} AND ( {@param endDateMinAge} < {@link EtxMessage#modificationDate})
     *
     * @param startDate lower limit of {@link EtxMessage#modificationDate}
     * @param endDateMinAge   higher limit of {@link EtxMessage#modificationDate} for not
     * @param endDate   higher limit of {@link EtxMessage#modificationDate}
     * @param limit     max results
     * @return failed message bundles
     */
    List<MessageReferenceVO> retrieveFailedMessageBundles(Date startDate, Date endDate, Date endDateMinAge, int limit);

}

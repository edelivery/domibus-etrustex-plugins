package eu.europa.ec.etrustex.adapter.service.security;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.PartyDao;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.SystemDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Arun Raj, Catalin Enache
 * @version 1.0
 * @since Oct 19, 2011
 */
@Service("etxBackendPluginIdentityManager")
@Transactional(readOnly = true)
public class IdentityManagerImpl implements IdentityManager {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(IdentityManagerImpl.class);

    @Autowired
    private PartyDao partyDao;

    @Autowired
    private SystemDao systemDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasAccess(String partyUuid, String userName) {
        LOG.debug("checkParty with name:" + userName + " and referenceId:" + partyUuid + " if belongs to the System starting...");

        EtxSystem systemEntity = searchSystemByLocalUserName(userName);
        EtxParty partyEntity = searchPartyByUuid(partyUuid);

        boolean checkPartyBelongsToSystem = systemEntity != null && partyEntity != null && partyEntity.getSystem().getId().equals(systemEntity.getId());

        LOG.debug("checkPartyBelongsToSystem done");

        return checkPartyBelongsToSystem;
    }

    @Override
    public EtxParty searchPartyByUuid(String partyUuid) {
        return partyDao.findByPartyUUID(partyUuid);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String retrieveAuthenticatedUserName() {
        if (SecurityContextHolder.getContext() == null || SecurityContextHolder.getContext().getAuthentication() == null) {
            LOG.warn("Authentication is missing from the security context. No user was specified.");
            return null;
        }
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    private EtxSystem searchSystemByLocalUserName(String userName) {
        return systemDao.findSystemByLocalUserName(userName);
    }
}

package eu.europa.ec.etrustex.adapter.integration.client.backend.repository;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPlugin;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPluginService;
import eu.europa.ec.etrustex.adapter.model.entity.administration.AdapterConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection;
import eu.europa.ec.etrustex.adapter.model.vo.FileAttachmentVO;
import eu.europa.ec.etrustex.adapter.model.vo.LocalSharedFileAttachmentVO;
import eu.europa.ec.etrustex.adapter.service.AdapterConfigurationService;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.common.util.Validate;
import eu.europa.ec.etrustex.common.util.WorkSpaceUtils;
import eu.europa.ec.etrustex.common.util.attachment.FileRepositoryUtil;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.attachment.LazyDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;
import java.util.Map;


/**
 * @author Federico Martini
 * @author Catalin Enache
 * <p>
 * Original version
 * @version 1.0
 * @author: micleva
 * @date: 9/12/13 9:56 AM
 * @since 30/01/2018
 * <p>
 * Added releaseResources method in order to close the input stream of the attachment data source.
 * When an input stream is closed the underlying resource is released and can be garbage collected.
 */
@Service("etxBackendPluginLocalSharedFileRepository")
public class LocalSharedFileRepositoryService implements FileRepository {

    public static final String ETX_ADAPTER_IN_DIRECTORY = "in";
    public static final String ETX_ADAPTER_OUT_DIRECTORY = "out";

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(LocalSharedFileRepositoryService.class);

    /**
     * default file separator
     */
    private static final String FILE_SEPARATOR = FileSystems.getDefault().getSeparator();

    @Autowired
    private AdapterConfigurationService adapterConfigurationService;

    @Autowired
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Autowired
    private DomainBackEndPluginService domainBackEndPluginService;

    protected Path rootDirectory;

    @PostConstruct
    @DomainBackEndPlugin
    @ClearEtxLogKeys
    public void loadConfiguration() {
        domainBackEndPluginService.setBackendPluginDomain();
        LOG.info("Loading File Repository configuration");
        rootDirectory = getRepositoryRootDirectory();

        LOG.info("Root folder fully initialized on path: {} ; {}",
                rootDirectory.toAbsolutePath(),
                FileRepositoryUtil.getHumanReadableStorageInfo(rootDirectory));
        LOG.info("Root File Repository content is: {}", FileRepositoryUtil.getPathContent(rootDirectory));
        clearEtxLogKeysService.clearKeys();
    }

    /**
     * The rootDirectory path is determined at application startup by the property 'etx.adapter.repository.root.path'
     * The property 'etx.adapter.repository.root.path' of the table {@code}ETX_ADT_CONFIG{@code}
     *
     * @return the rootDirectory Path in Database
     */
    public Path getRootDirectory() {
        return rootDirectory;
    }

    private Path getRepositoryRootDirectory() {
        Map<AdapterConfigurationProperty, String> config = adapterConfigurationService.getAdapterConfigurations();

        String rootPath = config.get(AdapterConfigurationProperty.ETX_ADAPTER_REPOSITORY_ROOT_PATH);
        Validate.notBlank(rootPath, "Repository root path is not configured");

        return WorkSpaceUtils.getPath(Paths.get(rootPath));
    }

    @Override
    public LocalSharedFileAttachmentVO readFile(Map<SystemConfigurationProperty, String> systemConfig, EtxAttachment etxAttachment) throws IOException {
        Path zipFile = getAttachmentZipFile(etxAttachment);
        LocalSharedFileAttachmentVO localSharedFileAttachmentVO = null;
        if (Files.exists(zipFile)) {
            localSharedFileAttachmentVO = readZipFile(zipFile);
        } else {
            LOG.error("File {} NOT found in the repository path.", zipFile.toAbsolutePath());
        }
        return localSharedFileAttachmentVO;
    }

    @Override
    public void writeFile(Map<SystemConfigurationProperty, String> systemConfig, EtxAttachment etxAttachment, Path attachmentFile) throws IOException {

        String filePath = buildBundlePath(etxAttachment.getMessage());
        etxAttachment.setFilePath(filePath);

        Path fileDirectory = rootDirectory.resolve(filePath);
        if (Files.notExists(fileDirectory)) {
            Files.createDirectories(fileDirectory);
        }

        //ZIPFILE CREATION
        String zipFileName = etxAttachment.getAttachmentUuid() + ".zip";

        writeZipFile(fileDirectory, zipFileName, etxAttachment.getFileName(),
                etxAttachment.getSessionKey(), attachmentFile);

    }

    private String buildBundlePath(EtxMessage messageEntity) {

        EtxParty receiver = messageEntity.getReceiver();
        EtxParty sender = messageEntity.getSender();

        StringBuilder filePathBuilder = new StringBuilder();

        if (messageEntity.getDirectionType() == MessageDirection.IN) {
            filePathBuilder.append(receiver.getSystem().getName());
            filePathBuilder.append(FILE_SEPARATOR);
            filePathBuilder.append(LocalSharedFileRepositoryService.ETX_ADAPTER_IN_DIRECTORY);
            filePathBuilder.append(FILE_SEPARATOR);
            filePathBuilder.append(receiver.getPartyUuid());
        } else {
            filePathBuilder.append(sender.getSystem().getName());
            filePathBuilder.append(FILE_SEPARATOR);
            filePathBuilder.append(LocalSharedFileRepositoryService.ETX_ADAPTER_OUT_DIRECTORY);
        }
        filePathBuilder.append(FILE_SEPARATOR);
        filePathBuilder.append(sender.getPartyUuid());
        filePathBuilder.append(FILE_SEPARATOR);
        filePathBuilder.append(messageEntity.getMessageUuid());

        return filePathBuilder.toString();
    }

    private Path writeZipFile(Path directory, String zipFileName, String fileName, byte[] sessionKey, Path srcFile) throws IOException {
        Path zipFile = directory.resolve(zipFileName);
        Files.deleteIfExists(zipFile);

        final URI uri = URI.create("jar:file:" + zipFile.toUri().getPath());

        try (FileSystem zipFileSystem = FileSystems.newFileSystem(uri, Collections.singletonMap("create", "true"))) {
            Files.copy(srcFile, zipFileSystem.getPath(fileName));
            if (sessionKey != null) {
                Files.copy(new ByteArrayInputStream(sessionKey), zipFileSystem.getPath("SessionKey"));
            }
        }
        return zipFile;
    }

    private LocalSharedFileAttachmentVO readZipFile(Path file) throws IOException {
        FileSystem zipFileSystem = FileSystems.newFileSystem(file, null);
        final LocalSharedFileAttachmentVO localSharedFileAttachmentVO = new LocalSharedFileAttachmentVO(zipFileSystem);

        for (Path path : zipFileSystem.getRootDirectories()) {
            Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if (StringUtils.equals("SessionKey", String.valueOf(file.getFileName()))) {
                        localSharedFileAttachmentVO.setSessionKey(Files.readAllBytes(file));
                    } else {
                        localSharedFileAttachmentVO.setDataHandler(new DataHandler(new IsDataSource(Files.newInputStream(file))));
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        }

        return localSharedFileAttachmentVO;
    }

    private Path getAttachmentZipFile(EtxAttachment etxAttachment) {
        String filePath = buildBundlePath(etxAttachment.getMessage()) +
                FILE_SEPARATOR +
                etxAttachment.getAttachmentUuid() + ".zip";
        return rootDirectory.resolve(filePath);
    }

    private Path getBundleDirectory(EtxMessage message) {
        return rootDirectory.resolve(buildBundlePath(message));
    }

    @Override
    public boolean deleteDirectory(EtxMessage message) {
        Path directory = getBundleDirectory(message);
        LOG.info("Deleted bundle directory for message: {} for message with ID: {}", directory, message.getId());
        boolean isDeleted = false;

        try {
            Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });

            isDeleted = true;
        } catch (IOException e) {
            LOG.error("Failed to delete directory for [messageId: {}]: {}", message.getId(), directory.toAbsolutePath().toString(), e);
        }

        return isDeleted;
    }

    @Override
    public void releaseResources(FileAttachmentVO faVo) throws IOException {

        DataHandler dataHandler = faVo.getDataHandler();
        if (dataHandler == null || dataHandler.getDataSource() == null)
            return;

        DataSource ds = dataHandler.getDataSource();
        if (ds instanceof LazyDataSource) {
            InputStream is = ds.getInputStream();
            //close will delete resource(etc: temporary file) held by the input stream
            is.close();
            LOG.debug("Input stream successfully closed");
        } else {
            LOG.debug("Data source is [{}] and for content type [{}]", ds.getClass().getName(), ds.getContentType());
        }

        LocalSharedFileAttachmentVO localSharedFileAttachmentVO = (LocalSharedFileAttachmentVO) faVo;
        IOUtils.closeQuietly(localSharedFileAttachmentVO.getZipFileSystem());
    }
}

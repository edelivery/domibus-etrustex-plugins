package eu.europa.ec.etrustex.adapter.persistence.api.attachment;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentDirection;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxError;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState.ATTS_CREATED;
import static eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState.ATTS_DOWNLOADED;
import static java.util.Arrays.asList;

@Repository
public class AttachmentDaoImpl extends DaoBaseImpl<EtxAttachment> implements AttachmentDao {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(AttachmentDaoImpl.class);
    private static final String ATTACHMENT_STATE_LIST = "attachmentStateList";

    public AttachmentDaoImpl() {
        super(EtxAttachment.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long countMessageAttachmentsNotInStateList(Long messageId, List<AttachmentState> stateList) {
        TypedQuery<Long> query = getEntityManager().createNamedQuery("countMessageAttachmentsNotInState", Long.class);

        query.setParameter("messageId", messageId);
        query.setParameter(ATTACHMENT_STATE_LIST, stateList);
        return query.getSingleResult();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long countMessageAttachmentsInStateList(Long messageId, List<AttachmentState> stateList) {
        TypedQuery<Long> query = getEntityManager().createNamedQuery("countMessageAttachmentsInState", Long.class);

        query.setParameter("messageId", messageId);
        query.setParameter(ATTACHMENT_STATE_LIST, stateList);
        return query.getSingleResult();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long countAttachmentsBySystemAndUUIDAndState(String attachmentUUID, List<AttachmentState> attachmentStateList, Long senderSystemId) {
        TypedQuery<Long> query = getEntityManager().createNamedQuery("countAttachmentsBySystemAndUUIDAndState", Long.class);
        query.setParameter("attachmentUUID", attachmentUUID);
        query.setParameter(ATTACHMENT_STATE_LIST, attachmentStateList);
        query.setParameter("senderSystemId", senderSystemId);
        return query.getSingleResult();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public List<EtxAttachment> getNextAttachmentsToExpire(Date endDate, int pageSize, int index) {
        TypedQuery<EtxAttachment> query = getEntityManager().createNamedQuery("expiredAttachments", EtxAttachment.class);
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        query.setParameter("endDate", cal);
        query.setFirstResult(index * pageSize);
        query.setMaxResults(pageSize);
        return query.getResultList();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public Long countNextAttachmentsToExpire(Date endDate) {
        TypedQuery<Long> query = getEntityManager().createNamedQuery("countExpiredAttachments", Long.class);
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        query.setParameter("endDate", cal);
        return query.getSingleResult();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EtxAttachment findAttachmentByDomibusMessageId(String domibusMessageID) {
        LOG.debug("Finding Attachment by Domibus Message ID : {}", domibusMessageID);
        TypedQuery<EtxAttachment> query = getEntityManager().createNamedQuery("findAttachmentByDomibusMessageId", EtxAttachment.class);
        query.setParameter("domibusMessageId", domibusMessageID);
        try {
            return query.getSingleResult();
        } catch (NoResultException nrEx) {
            LOG.debug("Could not find any attachment related to Domibus message with id [{}]", domibusMessageID, nrEx);
            return null;
        }
    }

    @Override
    @Transactional
    public int updateStuckAttachments(LocalDateTime startDate, LocalDateTime endDate, EtxError error) {
        LOG.debug("Update attachments with a state not final: startDate: [{}], endDate: [{}] ", startDate, endDate);
        Query updateQuery = getEntityManager().createNamedQuery("BEPlugin.EtxAttachment.updateStuckAttachment");
        updateQuery.setParameter("ERROR", error);
        updateQuery.setParameter("START_DATE", getCalendar(startDate));
        updateQuery.setParameter("END_DATE", getCalendar(endDate));
        updateQuery.setParameter("ATTACHMENT_STATE_LIST", asList(ATTS_CREATED, ATTS_DOWNLOADED));

        return updateQuery.executeUpdate();
    }

    /**
     * {@inheritDoc}
     */
    @Transactional
    @Override
    public List<EtxAttachment> findAttachmentsByUUIDsDirectionState(List<String> attachmentUUIDs, List<AttachmentDirection> attachmentDirections, List<AttachmentState> attachmentStates) {
        LOG.debug("Finding attachments by Attachment UUIDs list: [{}], Directions: [{}] and States:[{}]", attachmentUUIDs, attachmentDirections, attachmentStates);
        TypedQuery<EtxAttachment> attachmentsQuery = getEntityManager().createNamedQuery("BEPlugin.EtxAttachment.findAttachmentsByUUIDs_Direction_State", EtxAttachment.class);
        attachmentsQuery.setParameter("attachmentUUIDList", attachmentUUIDs);
        attachmentsQuery.setParameter("attachmentDirectionList", attachmentDirections);
        attachmentsQuery.setParameter("attachmentStateList", attachmentStates);
        return attachmentsQuery.getResultList();
    }
}

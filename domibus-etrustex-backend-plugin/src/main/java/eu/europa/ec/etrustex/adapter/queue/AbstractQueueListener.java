package eu.europa.ec.etrustex.adapter.queue;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPlugin;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPluginService;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.jms.QueueMessage;
import eu.europa.ec.etrustex.common.jms.TextMessageConverter;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * @author: micleva, François Gautier
 * @date: 11/15/12 2:08 PM
 * @updatedDate: 13/07/2017
 * @Version 1.0
 * @project: ETX
 */
public abstract class AbstractQueueListener<T extends QueueMessage> implements MessageListener {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(AbstractQueueListener.class);

    @Resource(name = "etxBackendPluginTextMessageCreator")
    private TextMessageConverter textMessageConverter;

    @Autowired
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Autowired
    private DomainBackEndPluginService domainBackEndPluginService;
    @Autowired
    private ETrustExBackendPluginJMSErrorUtils eTrustExBackendPluginJMSErrorUtils;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    @DomainBackEndPlugin
    @ClearEtxLogKeys
    public void onMessage(Message message) {
        domainBackEndPluginService.setBackendPluginDomain();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Queue Consumer: " + this.getClass().getName() + "; Incoming Message: " + message);
        }

        T objectMessage = null;
        try {
            if (message instanceof TextMessage) {
                objectMessage = (T) textMessageConverter.fromMessage(message);
                validateMessage(objectMessage);
                handleMessage(objectMessage);
            } else {
                throw new IllegalArgumentException("Unexpected message type for " + message);
            }
        } catch (Exception e) {
            if(e instanceof ETrustExPluginException){
                if(!eTrustExBackendPluginJMSErrorUtils.handleOutDownloadAttachmentHandlerFileNotFoundErrorLogging(e)){
                    LOG.error("Unexpected exception on JMS Consumer:[{}] for object:[{}], with message:[{}]", this.getClass().getName(), objectMessage, e.getMessage(), e);
                }
                throw (ETrustExPluginException) e;
            }
            throw new ETrustExPluginException(ETrustExError.DEFAULT, e.getMessage(), e);
        } finally {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Exiting Queue Consumer: {}; Incoming Message: {}", this.getClass().getName(), message);
            }
        }
        clearEtxLogKeysService.clearKeys();
    }

    public abstract void handleMessage(T object);

    public abstract void validateMessage(T object);
}

package eu.europa.ec.etrustex.adapter.integration.handlers;

import ec.schema.xsd.documentbundle_1.DocumentBundleType;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleRequest;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentDirection;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxErrorBuilder;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.service.BackendConnectorSubmissionService;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.adapter.service.security.IdentityManager;
import eu.europa.ec.etrustex.adapter.support.Util;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.common.util.ConversationIdKey;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.Map;

import static eu.europa.ec.etrustex.adapter.integration.client.etxnode.converters.NodeDocumentBundleTypeConverter.toEtxMessage;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.extractETrustExXMLPayload;

// @formatter:off

/**
 * <b>Backend service to receive transmission of bundle and wrapper from Node to Backend.<br/></b>
 *
 * <p>This service is currently designed to handle the NotifyBundle transmission.<br/>
 * <ul>
 * <li>
 * For the sender and receiver party if a {@link MessageType#MESSAGE_BUNDLE} with the Bundle UUID
 * is received ({@link MessageDirection#IN}) for the first time, a Bundle entry is created in the backend database.
 * If there are attachments they are also created in the database.
 * </li>
 * <li><b>If no attachments are transmitted with the bundle:</b> it is treated that the bundle has no attachments
 * and trigger will be raised to {@code}PostProcessing{@code} JMS queue </li>
 *
 * <li><b>Backend system configured NOT to download attachments:</b> For the backend system of the receiver party,
 * if the entry {@link SystemConfigurationProperty#ETX_BACKEND_DOWNLOAD_ATTACHMENTS} in ETX_ADT_SYSTEM_CONFIG is set to false,
 * then on receipt of bundle for the first time, all attachments of the bundle will be updated as {@link AttachmentState#ATTS_IGNORED}
 * and trigger will be raised to {@code}PostProcessing{@code} JMS queue. Thus download of attachment will be ignored.</li>
 *
 * <li>It also handles the reply to Node plugin attempting to send a bundle notification acknowledgment</li>
 * </ul>
 * </p>
 *
 * @author Federico Martini
 * @version 1.0
 * @since 4 Dec 2017
 */
// @formatter:on
@Service("etxBackendPluginInboxBundleTransmissionHandler")
public class InboxBundleTransmissionBackendHandler implements OperationHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxBundleTransmissionBackendHandler.class);

    @Autowired
    private IdentityManager identityManager;

    @Autowired
    private MessageServicePlugin messageService;

    @Autowired
    private PostProcessProducer postProcessProducer;

    @Autowired
    private BackendConnectorSubmissionService backendConnectorSubmissionService;

    @Autowired
    @Lazy
    @Qualifier("etxBackendPluginProperties")
    private ETrustExBackendPluginProperties eTrustExBackendPluginProperties;

    /**
     * <p>Handles the notification from Domibus on Bundle transmission reception at backend.</p>
     * The instance of {@link EtxMessage} is created first.<br/>
     * Depending on whether attachment processing is required or not, further triggers are raised either for attachment processing
     * or bundle processing.<br/>
     * Finally attempts to send an ACK message to Node plugin but if it fails (for a validation issue) no rollback will happen, since it is no worth it retrying
     * and the {@link EtxMessage} will be set to FAILED status + a post process will be called to ensure that a notification of the failure arrives to the Node plugin.
     *
     * @param etxAdapterDTO Instance of {@link ETrustExAdapterDTO} notified by Domibus to the backend plugin
     * @return
     */
    @Override
    public boolean handle(ETrustExAdapterDTO etxAdapterDTO) {
        LOG.debug("Reached InboxBundleTransmissionHandler ");

        //retrieve sender and receiver party
        EtxParty etxSenderParty = retrieveParty(etxAdapterDTO.getAs4OriginalSender());
        EtxParty etxReceiverParty = retrieveParty(etxAdapterDTO.getAs4FinalRecipient());
        Map<SystemConfigurationProperty, String> receiverSystemConfig = Util.buildSystemConfig(etxReceiverParty.getSystem().getSystemConfigList());
        LOG.info("Handling InboxBundle Notification at Backend for SenderParty:[{}] ReceiverParty:[{}]",
                etxAdapterDTO.getAs4OriginalSender(),
                etxAdapterDTO.getAs4FinalRecipient());

        EtxMessage etxMessage = createEtxMessage(etxAdapterDTO, etxSenderParty, etxReceiverParty);
        try {
            // checks whether backend system wants to download attachments or not
            if (!Boolean.valueOf(receiverSystemConfig.get(SystemConfigurationProperty.ETX_BACKEND_DOWNLOAD_ATTACHMENTS))) {
                handleIgnoreAttachments(etxMessage);
            } else if (etxMessage.getAttachmentList().isEmpty()) {
                LOG.info("No wrappers present in the InboxBundle Notification with UUID:[{}]", etxMessage.getMessageUuid());
                postProcessProducer.triggerPostProcess(etxMessage.getId());
            }
            handleBundleNotificationAck(etxMessage);
        } catch (Exception e) {
            handleError(etxMessage, e);
        }

        return true;
    }

    /**
     * Retrieve the instance of {@link EtxParty} corresponding to the Party UUID.<br/>
     *
     * @param strParty         eTrustEx party UUID
     * @return instance of {@link EtxParty}
     */
    EtxParty retrieveParty(String strParty) {
        if (StringUtils.isBlank(strParty)) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "eTrustEx party is empty!");
        }
        EtxParty etxParty = identityManager.searchPartyByUuid(strParty);
        if (etxParty == null) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "eTrustEx party:[" + strParty + "] is not known at Backend!");
        }
        return etxParty;
    }

    //@formatter:off

    /**
     * <p>To get an instance of the {@link EtxMessage}.</p>
     * <ol>
     * <li>The Bundle UUID is extracted from the {@link SubmitDocumentBundleRequest} message in the payload</li>
     * <ul>
     * <li>Converts the {@link SubmitDocumentBundleRequest} to an instance of {@link EtxMessage} and creates the DB entries for the Message Bundle and Attachments in {@code}CREATED{@code} state.</li>
     * </ul>
     * </ol>
     *
     * @param etxAdapterDTO    Instance of {@link ETrustExAdapterDTO} notified by Domibus to the backend plugin
     * @param etxSenderParty   {@link EtxParty} instance of sender
     * @param etxReceiverParty {@link EtxParty} instance of receiver
     * @return instance of {@link EtxMessage} either retrieved from DB or created
     */
    //@formatter:on
    final EtxMessage createEtxMessage(ETrustExAdapterDTO etxAdapterDTO, EtxParty etxSenderParty, EtxParty etxReceiverParty) {

        EtxMessage etxMessage = getEtxMessage(etxAdapterDTO, etxSenderParty, etxReceiverParty);
        messageService.createMessage(etxMessage);
        LOG.info("Handling NotifyInboxBundle transmission at Backend, Created ETX_ADT_MESSAGE");
        return etxMessage;
    }

    public static EtxMessage getEtxMessage(ETrustExAdapterDTO etxAdapterDTO, EtxParty etxSenderParty, EtxParty etxReceiverParty) {
        SubmitDocumentBundleRequest submitDocumentBundleRequest = extractETrustExXMLPayload(ContentId.CONTENT_ID_GENERIC, etxAdapterDTO);
        LOG.info("Handling NotifyInboxBundle transmission at Backend from SenderParty:[{}] to ReceiverParty:[{}]",
                etxSenderParty.getPartyUuid(),
                etxReceiverParty.getPartyUuid());

        EtxMessage etxMessage = toEtxMessage(getDocumentBundle(submitDocumentBundleRequest));
        if (etxMessage == null) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "Document Bundle is required");
        }
        etxMessage.setMessageType(MessageType.MESSAGE_BUNDLE);
        etxMessage.setMessageState(MessageState.MS_CREATED);
        etxMessage.setDirectionType(MessageDirection.IN);
        etxMessage.setSender(etxSenderParty);
        etxMessage.setReceiver(etxReceiverParty);
        etxMessage.setDomibusMessageId(etxAdapterDTO.getAs4MessageId());

        for (EtxAttachment etxAttachment : etxMessage.getAttachmentList()) {
            etxAttachment.setStateType(AttachmentState.ATTS_CREATED);
            etxAttachment.setDirectionType(AttachmentDirection.INCOMING);
            etxAttachment.setActiveState(true);
            etxAttachment.setMessage(etxMessage);
        }
        return etxMessage;
    }

    private static DocumentBundleType getDocumentBundle(SubmitDocumentBundleRequest submitDocumentBundleRequest) {
        return submitDocumentBundleRequest != null ? submitDocumentBundleRequest.getDocumentBundle() : null;
    }

    /**
     * When attachments need not be downloaded at the receiver party backend, the {@link EtxAttachment} are set to {@link AttachmentState#ATTS_IGNORED}
     * and the {@link EtxMessage} is sent for {@code}PostProcessing{@code}.<br/>
     *
     * @param etxMessage Instance of {@link EtxMessage}
     */
    private void handleIgnoreAttachments(EtxMessage etxMessage) {
        LOG.info("For Inbox MESSAGE_BUNDLE with Id:[{}], Receiver Party System :[{}] is configured not to receive wrappers. Marking wrappers as Ignored.", etxMessage.getId(), etxMessage.getReceiver().getSystem().getName());
        for (EtxAttachment etxAttachment : etxMessage.getAttachmentList()) {
            etxAttachment.setStateType(AttachmentState.ATTS_IGNORED);
        }
        postProcessProducer.triggerPostProcess(etxMessage.getId());
    }

    /**
     * Attempts to send an ACK to the Node plugin to confirm the well received Bundle.
     * The parties are inverted since it is a message from receiver Backend plugin to Node plugin.
     * The call to Domibus must be handled in a new transaction in order to be able to update the entities correctly in case of an exception.
     *
     * @param etxMessage the created {@link EtxMessage}
     */
    void handleBundleNotificationAck(EtxMessage etxMessage) {

        ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();

        // To the Node
        eTrustExAdapterDTO.setAs4ToPartyId(eTrustExBackendPluginProperties.getAs4ETrustExNodePartyId());
        eTrustExAdapterDTO.setAs4ToPartyIdType(eTrustExBackendPluginProperties.getAs4ToPartyIdType());
        eTrustExAdapterDTO.setAs4ToPartyRole(eTrustExBackendPluginProperties.getAs4ToPartyRole());

        // From Backend
        eTrustExAdapterDTO.setAs4FromPartyId(etxMessage.getReceiver().getPartyUuid());
        eTrustExAdapterDTO.setAs4FromPartyIdType(eTrustExBackendPluginProperties.getAs4FromPartyIdType());
        eTrustExAdapterDTO.setAs4FromPartyRole(eTrustExBackendPluginProperties.getAs4FromPartyRole());

        // Message Properties
        eTrustExAdapterDTO.setAs4OriginalSender(etxMessage.getReceiver().getPartyUuid());
        eTrustExAdapterDTO.setAs4FinalRecipient(etxMessage.getSender().getPartyUuid());

        // Collaboration Info
        eTrustExAdapterDTO.setAs4Service(eTrustExBackendPluginProperties.getAs4ServiceInboxBundleTransmission());
        eTrustExAdapterDTO.setAs4ServiceType(eTrustExBackendPluginProperties.getAs4ServiceInboxBundleTransmissionServiceType());
        eTrustExAdapterDTO.setAs4Action(eTrustExBackendPluginProperties.getAs4ActionInboxBundleTransmissionAck());

        StringBuilder as4ConversationID = new StringBuilder().append(ConversationIdKey.INBOX_MESSAGEBUNDLE.getConversationIdKey()).append(etxMessage.getMessageUuid());
        eTrustExAdapterDTO.setAs4ConversationId(as4ConversationID.toString());
        eTrustExAdapterDTO.setAs4RefToMessageId(etxMessage.getDomibusMessageId());

        LOG.info("For Inbox MESSAGE_BUNDLE with Id:[{}], Calling backend connector to send bundle notification ack", etxMessage.getId());
        LOG.debug("{}", eTrustExAdapterDTO);
        backendConnectorSubmissionService.submit(eTrustExAdapterDTO);
    }

    /**
     * On error in Inbox NotifyBundle workflow, update the {@link EtxMessage} and linked {@link EtxAttachment}s to FAILED state
     * and trigger PostProcess workflow.
     *
     * @param etxMessage
     * @param throwable
     */
    void handleError(EtxMessage etxMessage, Throwable throwable) {

        String errorDetail = ETrustExError.DEFAULT.getDescription();
        if (throwable != null) {
            errorDetail = throwable.getMessage();
        }
        etxMessage.setMessageState(MessageState.MS_FAILED);
        etxMessage.setError(EtxErrorBuilder
                .getInstance()
                .withResponseCode(ETrustExError.DEFAULT.getCode())
                .withDetail(errorDetail)
                .build());
        // Check for the attachments
        if (etxMessage.hasAttachments()) {
            for (EtxAttachment etxAttachment : etxMessage.getAttachmentList()) {
                etxAttachment.setStateType(AttachmentState.ATTS_FAILED);
                etxAttachment.setError(
                        EtxErrorBuilder
                                .getInstance()
                                .withResponseCode(ETrustExError.DEFAULT.getCode())
                                .withDetail("Failed due to failure in handling inbox notification of bundle with UUID [" + etxMessage.getMessageUuid() + "]")
                                .build());
            }
        }
        messageService.updateMessage(etxMessage);
        postProcessProducer.triggerPostProcess(etxMessage.getId());
    }


}
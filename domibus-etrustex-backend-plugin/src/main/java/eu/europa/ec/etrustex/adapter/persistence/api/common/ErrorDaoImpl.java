package eu.europa.ec.etrustex.adapter.persistence.api.common;

import eu.europa.ec.etrustex.adapter.model.entity.common.EtxError;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBaseImpl;
import org.springframework.stereotype.Repository;

@Repository
public class ErrorDaoImpl extends DaoBaseImpl<EtxError> implements ErrorDao {

    public ErrorDaoImpl() {
        super(EtxError.class);
    }

}

package eu.europa.ec.etrustex.adapter.service.security;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxUser;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.SystemDao;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Arun Raj, Catalin Enache
 * @version 1.0
 * @since Oct 19, 2011
 */
@Service("etxBackendBackendUserService")
@Transactional
public class BackendUserService {

    @Autowired
    private SystemDao systemDao;
    @Autowired
    private UserDao userDao;

    public List<EtxUser> getAllBackEndUsers(){
        return systemDao.findAll().stream()
                .map(EtxSystem::getBackendUser)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public void updateUser(EtxUser etxUser){
        userDao.update(etxUser);
    }
}

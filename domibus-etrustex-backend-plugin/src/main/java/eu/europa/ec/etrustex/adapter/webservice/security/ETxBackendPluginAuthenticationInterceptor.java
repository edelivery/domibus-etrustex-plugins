package eu.europa.ec.etrustex.adapter.webservice.security;

import eu.domibus.ext.exceptions.AuthenticationExtException;
import eu.domibus.ext.services.AuthenticationExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import java.io.IOException;

/**
 * CXF interceptor for authenticating the User credentials.
 * As of 1.0, only Basic authentication has been implemented.
 *
 * @author Arun Raj
 * @version 1.0
 */
@Component(value = "etxBackendPluginAuthenticationInterceptor")
public class ETxBackendPluginAuthenticationInterceptor extends AbstractPhaseInterceptor<Message> {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ETxBackendPluginAuthenticationInterceptor.class);
    public static final QName FAULT_CODE_UNAUTHORIZED_ACCESS = new QName(XMLConstants.NULL_NS_URI, "Error 401--Unauthorized");

    @Autowired
    private AuthenticationExtService authenticationExtService;

    public ETxBackendPluginAuthenticationInterceptor() {
        super(Phase.PRE_PROTOCOL);
    }

    @Override
    public void handleMessage(Message message) throws Fault {
        HttpServletRequest httpRequest = (HttpServletRequest) message.get("HTTP.REQUEST");

        LOG.debug("Intercepted request for " + httpRequest.getPathInfo());

        try {
            authenticationExtService.authenticate(httpRequest);
        } catch (AuthenticationExtException e) {
            /*
            The legacy ETrustEx adapter throws an HTML page when authentication fails. To meet this error, the httpResponse.sendError is used.
            However httpResponse.sendError() is not a fault which will unwind the CXF interceptors and stop further execution of the web service.
            To stop further execution, a SOAP  fault has to be raised.
            However using the httpResponse.sendError() will close the HTTPResponse output stream. Hence the response stream is not available for the CXF interceptor to write the soap fault.
            This will cause an unwanted exception in the Domibus log - java.lang.IllegalStateException: strict servlet API: cannot call getOutputStream() after getWriter()
            */
            //TODO Either fix write error in org.apache.cxf.binding.soap.interceptor.SoapOutInterceptor$SoapOutEndingInterceptor.handleMessage(SoapOutInterceptor.java:302) or With next version of the ETrustEx Backend plugin, migrate the users from the HTML error response to the SOAP fault response.
            HttpServletResponse httpResponse = (HttpServletResponse) message.get("HTTP.RESPONSE");
            try {
                httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            } catch (IOException ioEx) {
                LOG.error("", ioEx);
            }

            Fault fault = new Fault(e, FAULT_CODE_UNAUTHORIZED_ACCESS);
            fault.setStatusCode(HttpServletResponse.SC_UNAUTHORIZED);
            LOG.error("", fault);
            throw fault;
        }
    }
}
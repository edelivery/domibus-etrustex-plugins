package eu.europa.ec.etrustex.adapter.queue.out.downloadattachment;

import eu.europa.ec.etrustex.common.jms.QueueMessage;

/**
 * @author micleva
 * @project ETX
 */

public class OutDownloadAttachmentQueueMessage extends QueueMessage {

    private static final long serialVersionUID = -1775547352351965127L;

    private Long attachmentId;

    public Long getAttachmentId() {
        return this.attachmentId;
    }

    public void setAttachmentId(Long attachmentId) {
        this.attachmentId = attachmentId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("OutDownloadAttachmentQueueMessage");
        sb.append("{attachmentId=").append(attachmentId);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OutDownloadAttachmentQueueMessage that = (OutDownloadAttachmentQueueMessage) o;

        if (attachmentId != null ? !attachmentId.equals(that.attachmentId) : that.attachmentId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return attachmentId != null ? attachmentId.hashCode() : 0;
    }
}

package eu.europa.ec.etrustex.adapter.queue.notify;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.queue.AbstractQueueListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("etxBackendPluginNotifyConsumer")
public class NotifyConsumer extends AbstractQueueListener<NotifyQueueMessage> {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(NotifyConsumer.class);

    @Autowired
    private NotifyHandler handler;

    @Override
    public void handleMessage(NotifyQueueMessage queueMessage) {
        try {
            handler.handleNotify(queueMessage.getMessageId());
        } catch (Exception e) {
            LOG.warn("Unexpected exception on JMS Consumer: " + this.getClass().getName() + " for object: " + queueMessage + ", with message: " + e.getMessage(), e);

            handler.onError(queueMessage.getMessageId());
        }
    }

    @Override
    public void validateMessage(NotifyQueueMessage queueMessage) {
        handler.validateNotify(queueMessage.getMessageId());
    }
}

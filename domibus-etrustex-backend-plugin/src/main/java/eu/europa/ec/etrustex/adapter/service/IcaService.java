package eu.europa.ec.etrustex.adapter.service;

import ec.schema.xsd.commonaggregatecomponents_2.InterchangeAgreementType;
import eu.europa.ec.etrustex.adapter.model.dto.IcaDTO;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.ica.EtxIca;

import java.util.List;

/**
 * Interface to manage ICA information & cache reloading
 *
 * @author Francois Gautier
 * @version 1.1
 * @since 10/12/2018
 */
public interface IcaService {

    /**
     * Requests to reload ICAs for all sender parties associated with a backend instance.<br/>
     * Sender parties are identified as all parties that have a system associated in the backend plugin
     * and the system has more than 1 system configurations associated with it.
     */
    List<IcaDTO> reloadIcas();

    /**
     * Return ICA available in the backend plugin
     */
    List<EtxIca> getIcas();

    /**
     * Create/Update ICA
     */
    void createIca(String senderUuid, String receiverUuid, InterchangeAgreementType interchangeAgreementType);

    /**
     * Get cached ICA from DB
     */
    EtxIca getIca(EtxParty senderParty, EtxParty receiverParty);
}

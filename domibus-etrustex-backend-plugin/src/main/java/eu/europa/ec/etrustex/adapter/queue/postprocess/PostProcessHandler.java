package eu.europa.ec.etrustex.adapter.queue.postprocess;

/**
 * @author micleva
 * @project ETX
 */

public interface PostProcessHandler {
    void handlePostProcess(Long messageId);

    void validatePostProcess(Long messageId, Long attachmentId);

    void onError(Long messageId);
}

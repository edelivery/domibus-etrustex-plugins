package eu.europa.ec.etrustex.adapter.service;


import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentDirection;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxError;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxErrorBuilder;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.model.vo.MessageReferenceVO;
import eu.europa.ec.etrustex.adapter.persistence.api.attachment.AttachmentDao;
import eu.europa.ec.etrustex.adapter.persistence.api.message.MessageDao;
import eu.europa.ec.etrustex.adapter.queue.out.downloadattachment.OutDownloadAttachmentProducer;
import eu.europa.ec.etrustex.adapter.queue.out.sendmessage.SendMessageProducer;
import eu.europa.ec.etrustex.adapter.service.dto.RetriggerMessageBundleDTO;
import eu.europa.ec.etrustex.adapter.web.retriggeroutgoingbundle.RetriggerBundleRequest;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

import static eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties.ETXBACKEND_PLUGIN_BEAN_NAME;
import static eu.europa.ec.etrustex.common.exceptions.ETrustExError.ETX_005;
import static eu.europa.ec.etrustex.common.exceptions.ETrustExError.ETX_009;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;

@Service(MessageBundleService.MESSAGEBUNDLESERVICEBEANNAME)
public class MessageBundleService {
    public static final String MESSAGEBUNDLESERVICEBEANNAME = "etxBackendPluginMessageBundleService";
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(MessageBundleService.class);
    public static final int MAX_RETURN = 100;
    public static final int DEFAULT_AGE_MIN = 65;

    private ETrustExBackendPluginProperties properties;
    private final MessageDao messageDao;

    private final AttachmentDao attachmentDao;

    private final SendMessageProducer sendMessageProducer;

    private final OutDownloadAttachmentProducer outDownloadAttachmentProducer;

    public MessageBundleService(
            @Qualifier(ETXBACKEND_PLUGIN_BEAN_NAME) ETrustExBackendPluginProperties properties,
            MessageDao messageDao,
            AttachmentDao attachmentDao,
            SendMessageProducer sendMessageProducer,
            OutDownloadAttachmentProducer outDownloadAttachmentProducer) {
        this.properties = properties;
        this.messageDao = messageDao;
        this.attachmentDao = attachmentDao;
        this.sendMessageProducer = sendMessageProducer;
        this.outDownloadAttachmentProducer = outDownloadAttachmentProducer;
    }

    public RetriggerMessageBundleDTO reTriggerOutgoingBundle(List<RetriggerBundleRequest> retriggerBundleRequestList) {
        LOG.info("Received retrigger request with list: [{}]", retriggerBundleRequestList);
        if (isEmpty(retriggerBundleRequestList)) {
            return new RetriggerMessageBundleDTO();
        }

        RetriggerMessageBundleDTO retriggerMessageBundleDTO = validateOutgoingBundlesForRetrigger(retriggerBundleRequestList);

        findOtherImpactedMessageBundlesDuringReTrigger(retriggerMessageBundleDTO);

        setPendingAttachmentsToFailedForAllBundlesToRetrigger(retriggerMessageBundleDTO);

        resetMessageAttachmentStateAndRetrigger(retriggerMessageBundleDTO);
        return retriggerMessageBundleDTO;
    }

    /**
     * Validate if the outgoing message bundles are known and can be retriggered.
     *
     * @param retriggerBundleRequestList
     * @return
     */
    private RetriggerMessageBundleDTO validateOutgoingBundlesForRetrigger(List<RetriggerBundleRequest> retriggerBundleRequestList) {
        RetriggerMessageBundleDTO reTriggerMessageBundleDTO = new RetriggerMessageBundleDTO();

        for (RetriggerBundleRequest retriggerBundleRequest : retriggerBundleRequestList) {
            EtxMessage etxOutgoingMessageBundleForRetrigger = messageDao.findEtxMessage(retriggerBundleRequest.getMessageUuid(), Collections.singletonList(MessageType.MESSAGE_BUNDLE), MessageDirection.OUT, retriggerBundleRequest.getSenderUuid(), retriggerBundleRequest.getReceiverUuid());
            if (etxOutgoingMessageBundleForRetrigger == null) {
                LOG.error("No Outgoing MessageBundle with UUID:[{}], Ref_UUID:[{}], Sender Party: [{}], Receiver Party:[{}] found.", retriggerBundleRequest.getMessageUuid(), retriggerBundleRequest.getMessageRefUUID(), retriggerBundleRequest.getSenderUuid(), retriggerBundleRequest.getReceiverUuid());
                reTriggerMessageBundleDTO.addRetriggerBundleExceptionsList(retriggerBundleRequest.getMessageUuid(), new ETrustExPluginException(ETX_005, "No Outgoing MessageBundle with UUID:[" + retriggerBundleRequest.getMessageUuid() + "], MSG_REF_UUID:[" + retriggerBundleRequest.getMessageRefUUID() + "], Sender Party: [" + retriggerBundleRequest.getSenderUuid() + "], Receiver Party:[" + retriggerBundleRequest.getReceiverUuid() + "] found."));
                continue;
            }
            if (etxOutgoingMessageBundleForRetrigger.getMessageState() == MessageState.MS_PROCESSED) {
                LOG.error("Outgoing MessageBundle with MSG_ID:[{}], MSG_UUID:[{}], MSG_REF_UUID:[{}], Sender Party: [{}], Receiver Party:[{}] is already in MS_PROCESSED. Retriggering transmission will result in business failure from eTrustEx Node due to duplicate bundle.", etxOutgoingMessageBundleForRetrigger.getId(), retriggerBundleRequest.getMessageUuid(), retriggerBundleRequest.getMessageRefUUID(), retriggerBundleRequest.getSenderUuid(), retriggerBundleRequest.getReceiverUuid());
                reTriggerMessageBundleDTO.addRetriggerBundleExceptionsList(retriggerBundleRequest.getMessageUuid(), new ETrustExPluginException(ETX_009, "Outgoing MessageBundle with MSG_ID:[" + etxOutgoingMessageBundleForRetrigger.getId() + "], MSG_UUID:[" + retriggerBundleRequest.getMessageUuid() + "], MSG_REF_UUID:[" + retriggerBundleRequest.getMessageRefUUID() + "], Sender Party: [" + retriggerBundleRequest.getMessageUuid() + "], Receiver Party:[" + retriggerBundleRequest.getReceiverUuid() + "] is already in MS_PROCESSED. Retriggering transmission will result in business failure from eTrustEx Node due to duplicate bundle."));
                continue;
            }
            reTriggerMessageBundleDTO.addValidEtxMessageBundlesList(etxOutgoingMessageBundleForRetrigger);
        }
        if (LOG.isDebugEnabled()) {
            StringBuilder validMessageBundlesLog = new StringBuilder("Following outgoing message bundles are validated for retrigger:\n");
            for (EtxMessage validEtxMessage : reTriggerMessageBundleDTO.getValidEtxMessageBundlesList()) {
                validMessageBundlesLog.append("MSG_ID:[").append(validEtxMessage.getId()).append("] ")
                                      .append("MSG_UUID:[").append(validEtxMessage.getMessageUuid()).append("] ")
                                      .append("MSG_STATE:[").append(validEtxMessage.getMessageState()).append("] ")
                                      .append("MSG_REF_UUID:[").append(validEtxMessage.getMessageReferenceUuid()).append("] ")
                                      .append("Sender:[").append(validEtxMessage.getMessageReferenceUuid()).append("] ")
                                      .append("Receiver:[").append(validEtxMessage.getMessageReferenceUuid()).append("] ")
                                      .append("\n");
                LOG.debug(validMessageBundlesLog.toString());
            }
        }
        return reTriggerMessageBundleDTO;
    }

    /**
     * @param reTriggerMessageBundleDTO
     */
    private void findOtherImpactedMessageBundlesDuringReTrigger(RetriggerMessageBundleDTO reTriggerMessageBundleDTO) {
        List<String> uniqueAttachmentUUIDs = findUniqueAttachmentUUIDs(reTriggerMessageBundleDTO);
        List<String> uniqueBundleUUIDs = getUniqueBundleUUIDs(reTriggerMessageBundleDTO);

        List<MessageReferenceVO> impactedBundlesDueToOutgoingMessageBundleRetrigger = messageDao.findImpactedBundlesDueToOutgoingMessageBundleRetrigger(uniqueAttachmentUUIDs, uniqueBundleUUIDs);
        if (CollectionUtils.isEmpty(impactedBundlesDueToOutgoingMessageBundleRetrigger)) {
            return;
        }
        reTriggerMessageBundleDTO.setOtherImpactedBundlesVOList(impactedBundlesDueToOutgoingMessageBundleRetrigger);
    }

    private ArrayList<String> getUniqueBundleUUIDs(RetriggerMessageBundleDTO reTriggerMessageBundleDTO) {
        return new ArrayList<>(reTriggerMessageBundleDTO.getValidEtxMessageBundlesList().stream().map(EtxMessage::getMessageUuid).collect(Collectors.toSet()));
    }

    private List<String> findUniqueAttachmentUUIDs(RetriggerMessageBundleDTO reTriggerMessageBundleDTO) {
        if (reTriggerMessageBundleDTO == null || CollectionUtils.isEmpty(reTriggerMessageBundleDTO.getValidEtxMessageBundlesList())) {
            return Collections.emptyList();
        }
        Set<String> uniqueAttachmentUUIDs = new HashSet<>();
        for (EtxMessage etxMessage : reTriggerMessageBundleDTO.getValidEtxMessageBundlesList()) {
            if (CollectionUtils.isNotEmpty(etxMessage.getAttachmentList())) {
                for (EtxAttachment etxAttachment : etxMessage.getAttachmentList()) {
                    uniqueAttachmentUUIDs.add(etxAttachment.getAttachmentUuid());
                }
            }
        }
        LOG.debug("Found unique attachment UUIDs:" + uniqueAttachmentUUIDs);
        return new ArrayList<>(uniqueAttachmentUUIDs);
    }

    /**
     * The same attachment UUIDs could be linked with multiple bundles by the original sender.
     * Those message bundles that are not submitted for retrigger, could still be impacted, because the attachments that are in intermediate(not final) state, might be set to {@link AttachmentState#ATTS_FAILED}.
     * Hence those bundles would also fail. Those impacted bundles are listed to inform support admins.
     *
     * @param reTriggerMessageBundleDTO
     */
    private void setPendingAttachmentsToFailedForAllBundlesToRetrigger(RetriggerMessageBundleDTO reTriggerMessageBundleDTO) {
        if (reTriggerMessageBundleDTO == null || CollectionUtils.isEmpty(reTriggerMessageBundleDTO.getValidEtxMessageBundlesList())) {
            return;
        }
        List<String> uniqueAttachmentUUIDs = findUniqueAttachmentUUIDs(reTriggerMessageBundleDTO);
        List<EtxAttachment> pendingAttachmentsForBundlesToRetrigger = attachmentDao.findAttachmentsByUUIDsDirectionState(uniqueAttachmentUUIDs, Arrays.asList(AttachmentDirection.OUTGOING), Arrays.asList(AttachmentState.ATTS_CREATED, AttachmentState.ATTS_DOWNLOADED));
        for (EtxAttachment etxAttachmentInPendingState : pendingAttachmentsForBundlesToRetrigger) {
            LOG.info("For MSG_ID:[{}], EtxAttachment with ATT_ID:[{}] and ATT_UUID:[{}], is being set from state:[{}] to ATTS_FAILED to facilitate retransmission.",
                     etxAttachmentInPendingState.getMessage().getId(),
                     etxAttachmentInPendingState.getId(),
                     etxAttachmentInPendingState.getAttachmentUuid(),
                     etxAttachmentInPendingState.getStateType());
            etxAttachmentInPendingState.setStateType(AttachmentState.ATTS_FAILED);
            EtxError pendingAttachmentSetToFailedError = EtxErrorBuilder.getInstance()
                                                                        .withResponseCode("TECHNICAL")
                                                                        .withDetail("Pending Attachment set to Failed by Retrigger Outgoing Bundle service.")
                                                                        .build();
            etxAttachmentInPendingState.setError(pendingAttachmentSetToFailedError);
            attachmentDao.update(etxAttachmentInPendingState);
        }
    }

    /**
     * Reset the current message bundle and all related attachments to re-initiate the process.
     *
     * @param reTriggerMessageBundleDTO
     */
    private void resetMessageAttachmentStateAndRetrigger(RetriggerMessageBundleDTO reTriggerMessageBundleDTO) {
        if (reTriggerMessageBundleDTO == null || CollectionUtils.isEmpty(reTriggerMessageBundleDTO.getValidEtxMessageBundlesList())) {
            return;
        }

        for (EtxMessage etxMessageToReset : reTriggerMessageBundleDTO.getValidEtxMessageBundlesList()) {
            try {
                String resetMessageSuccessResponse = resetMessageAttachmentStateAndRetriggerEtxMessage(etxMessageToReset);
                reTriggerMessageBundleDTO.addRetriggerBundleSuccessResponseList(resetMessageSuccessResponse);
            } catch (Exception e) {
                LOG.error("Faced error in RetriggerOutgoingMessageBundle, while resetting Message Attachment State and triggerring Download:", e);
                reTriggerMessageBundleDTO.addRetriggerBundleExceptionsList(etxMessageToReset.getMessageUuid(), e);
            }
        }

        if(properties.shouldRetriggerOtherImpactedOutgoingMessageBundles()){
            triggerOtherImpactedBundles(reTriggerMessageBundleDTO);
        }
    }

    private String resetMessageAttachmentStateAndRetriggerEtxMessage(EtxMessage etxMessageToReset) {
        if(etxMessageToReset == null){
            return null;
        }
        List<Long> attachmentIdsToReset = etxMessageToReset.getAttachmentList().stream().map(EtxAttachment::getId).collect(Collectors.toList());
        LOG.info("State of EtxMessage with MSG_ID:[{}], MSG_UUID:[{}], MSG_REF_UUID:[{}] and all related attachments - ATT_IDs:[{}] is being set to CREATED to facilitate retransmission.",
                 etxMessageToReset.getId(),
                 etxMessageToReset.getMessageUuid(),
                 etxMessageToReset.getMessageReferenceUuid(),
                 attachmentIdsToReset);
        etxMessageToReset.setMessageState(MessageState.MS_CREATED);
        etxMessageToReset.setError(null);
        for (EtxAttachment etxAttachmentToReset : etxMessageToReset.getAttachmentList()) {
            etxAttachmentToReset.setStateType(AttachmentState.ATTS_CREATED);
            etxAttachmentToReset.setError(null);
        }
        messageDao.update(etxMessageToReset);

        triggerSendMessageBundle(etxMessageToReset);

        StringBuilder successResponseMessage = new StringBuilder();
        successResponseMessage.append("Reinitiated outgoing transmission for Bundle with MSG_ID: [").append(etxMessageToReset.getId()).append("], ")
                              .append("uuid:[").append(etxMessageToReset.getMessageUuid()).append("], ")
                              .append("ref_uuid:[").append(etxMessageToReset.getMessageReferenceUuid()).append("], ")
                              .append("between sender party :[").append(etxMessageToReset.getSender().getPartyUuid()).append("] ")
                              .append("and receiver party:[").append(etxMessageToReset.getReceiver().getPartyUuid()).append("]");
        return successResponseMessage.toString();
    }

    private void triggerOtherImpactedBundles(RetriggerMessageBundleDTO reTriggerMessageBundleDTO) {
        if (reTriggerMessageBundleDTO == null || CollectionUtils.isEmpty(reTriggerMessageBundleDTO.getOtherImpactedBundlesVOList())) {
            return;
        }
        LOG.info("Proceeding to retrigger bundles in other impacted bundle VO List:" + reTriggerMessageBundleDTO.getOtherImpactedBundlesVOList());
        for (MessageReferenceVO otherImpactedBundleVO : reTriggerMessageBundleDTO.getOtherImpactedBundlesVOList()) {
            EtxMessage otherImpactedBundleEtxMsg = messageDao.findEtxMessage(otherImpactedBundleVO.getMessageUuid(), Collections.singletonList(MessageType.MESSAGE_BUNDLE), MessageDirection.OUT, otherImpactedBundleVO.getSenderUuid(), otherImpactedBundleVO.getReceiverUuid());
            String successResponseMessage = resetMessageAttachmentStateAndRetriggerEtxMessage(otherImpactedBundleEtxMsg);
            reTriggerMessageBundleDTO.addRetriggerBundleSuccessResponseList(successResponseMessage);
        }
    }

    public void triggerSendMessageBundle(EtxMessage messageBundle) {
        LOG.debug("sendMessageBundle invoke - START");

        if (!isEmpty(messageBundle.getAttachmentList())) {
            List<Long> attachmentIds = messageBundle.getAttachmentList().stream().map(EtxAttachment::getId)
                                                    .collect(Collectors.toList());
            outDownloadAttachmentProducer.triggerDownload(attachmentIds);
        } else {
            //if no atts exists send directly a status message
            sendMessageProducer.triggerSendBundle(messageBundle.getId());
        }
        LOG.debug("sendMessageBundle invoke - END");
    }

    public List<MessageReferenceVO> getFailedMessages(Date startDate, Date endDate, Integer limit) {
        return messageDao.retrieveFailedMessageBundles(startDate, getEndDate(endDate), getEndDateMinAge(endDate), getLimit(limit));
    }

    private Date getEndDate(Date endDate) {
        if(endDate == null) {
            return Date.from(LocalDateTime.now()
                    .atZone(ZoneId.systemDefault()).toInstant());
        }
        return endDate;
    }

    private Date getEndDateMinAge(Date endDate) {
        Integer listPendingOutgoingBundleMinAge = properties.getListPendingOutgoingBundleMinAge();
        if (listPendingOutgoingBundleMinAge == null) {
            LOG.warn("getListPendingOutgoingBundleMinAge not found. Use of default [{}] minutes", DEFAULT_AGE_MIN);
            listPendingOutgoingBundleMinAge = DEFAULT_AGE_MIN;
        }
        Instant minAge = LocalDateTime.now()
                .minusMinutes(listPendingOutgoingBundleMinAge)
                .atZone(ZoneId.systemDefault()).toInstant();
        if (endDate == null || minAge.isBefore(endDate.toInstant())) {
            LOG.info("EndDate provided [{}] is NOT before now - [{}] minutes", endDate, listPendingOutgoingBundleMinAge);
            return Date.from(minAge);
        }
        return endDate;
    }

    private Integer getLimit(Integer limit) {
        if (limit == null) {
            return MAX_RETURN;
        }
        return limit;
    }
}

package eu.europa.ec.etrustex.adapter.model.entity.message;

import eu.europa.ec.etrustex.adapter.model.entity.AuditEntity;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxError;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * Domain Object describing a Message entity
 *
 * @author Valer Micle, Arun Raj
 * @version 1.0
 * @since 23-Feb-2017
 * findUnreadMessage
 * findMessagesByNotificationState
 */
@Entity(name = "EtxMessage")
@AttributeOverrides({
        @AttributeOverride(name = "creationDate", column = @Column(name = "MSG_CREATED_ON", insertable = false, updatable = false, nullable = false)),
        @AttributeOverride(name = "modificationDate", column = @Column(name = "MSG_UPDATED_ON"))})
@NamedQueries({
        @NamedQuery(name = "findUnreadMessagesByReceiver", query = "SELECT new eu.europa.ec.etrustex.adapter.model.vo.MessageReferenceVO(msg.messageUuid, msg.messageType, sender.partyUuid, receiver.partyUuid) " +
                "FROM EtxMessage msg " +
                "INNER JOIN msg.sender sender " +
                "INNER JOIN msg.receiver receiver " +
                "WHERE receiver.partyUuid = :receiverPartyUuid " +
                "AND msg.messageState = :messageState " +
                "AND msg.notificationState <> :ignoredNotificationState " +
                "AND msg.directionType = :messageDirection"),
        @NamedQuery(name = "findMessagesByNotificationState", query = "SELECT msg " +
                "FROM EtxMessage msg " +
                "WHERE msg.notificationState = :notificationState"),
        @NamedQuery(
                name = "findUnreadMessageOrderedByDateDesc", query = "SELECT msg " +
                "FROM EtxMessage msg " +
                "join fetch msg.sender " +
                "join fetch msg.receiver " +
                "left join fetch msg.attachmentList " +
                "WHERE  msg.messageUuid = :messageUuid AND msg.sender.partyUuid = :senderUuid AND msg.receiver.partyUuid = :receiverUuid " +
                "AND msg.directionType = :messageDirection AND msg.messageType IN (:messageTypes)" +
                "ORDER BY msg.creationDate desc"),
        @NamedQuery(name = "findMessagesByUuidAndSenderAndReceiver", query = "" +
                "SELECT DISTINCT NEW eu.europa.ec.etrustex.adapter.model.vo.MessageReferenceVO(msg.messageUuid, msg.messageType, sender.partyUuid, receiver.partyUuid) " +
                "FROM EtxMessage msg " +
                "INNER JOIN msg.sender sender " +
                "INNER JOIN msg.receiver receiver " +
                "WHERE msg.messageUuid = :messageUuid AND sender.id = :senderId AND receiver.id = :receiverId"),
        @NamedQuery(name = "findMessageByDomibusMessageId", query = "SELECT msg " +
                "FROM EtxMessage msg " +
                "WHERE msg.domibusMessageId = :domibusMessageID"),
        @NamedQuery(name = "retrieveMessageBundlesReadyForPostUpload", query = "SELECT msg " +
                "FROM EtxMessage msg " +
                "WHERE  msg.messageType = eu.europa.ec.etrustex.adapter.model.entity.message.MessageType.MESSAGE_BUNDLE " +
                "       AND msg.messageState = eu.europa.ec.etrustex.adapter.model.entity.message.MessageState.MS_CREATED " +
                "       AND NOT EXISTS (" +
                "                       SELECT att " +
                "                       FROM EtxAttachment att " +
                "                       WHERE att.message.id = msg.id " +
                "                               AND att.stateType NOT IN ( " +
                "                                    eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState.ATTS_UPLOADED, " +
                "                                    eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState.ATTS_ALREADY_UPLOADED, " +
                "                                    eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState.ATTS_FAILED, " +
                "                                    eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState.ATTS_IGNORED " +
                "                                   ) " +
                "                       ) "
        ),
        @NamedQuery(name = "BEPlugin.EtxMessage.retrieveImpactedBundlesDueToOutgoingMessageBundleRetrigger", query = "SELECT DISTINCT new eu.europa.ec.etrustex.adapter.model.vo.MessageReferenceVO(msg.messageUuid, msg.messageType, sender.partyUuid, receiver.partyUuid, msg.messageReferenceUuid, msg.messageState, msg.directionType) " +
                "FROM EtxMessage msg " +
                "INNER JOIN msg.attachmentList atts " +
                "INNER JOIN msg.sender sender " +
                "INNER JOIN msg.receiver receiver " +
                "WHERE atts.attachmentUuid IN (:attachmentUUIDList) " +
                "AND atts.stateType NOT IN (eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState.ATTS_UPLOADED, " +
                "                           eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState.ATTS_ALREADY_UPLOADED) " +
                "AND msg.directionType = eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection.OUT " +
                "AND msg.messageType = eu.europa.ec.etrustex.adapter.model.entity.message.MessageType.MESSAGE_BUNDLE " +
                "AND msg.messageUuid NOT IN (:messageBundleUUIDList) "),
        @NamedQuery(name = "BEPlugin.EtxMessage.retrieveFailedMessageBundles", query =
                "SELECT DISTINCT new eu.europa.ec.etrustex.adapter.model.vo.MessageReferenceVO(                          " +
                        "msg.messageUuid,                                                                                " +
                        "msg.messageType,                                                                                " +
                        "msg.sender.partyUuid,                                                                           " +
                        "msg.receiver.partyUuid,                                                                         " +
                        "msg.messageReferenceUuid,                                                                       " +
                        "msg.messageState,                                                                               " +
                        "msg.directionType                                                                               " +
                        ")                                                                                               " +
                        "FROM EtxMessage msg                                                                                     " +
                        "WHERE  msg.messageType = eu.europa.ec.etrustex.adapter.model.entity.message.MessageType.MESSAGE_BUNDLE  " +
                        "       AND msg.directionType =  eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection.OUT " +
                        "       AND msg.modificationDate > :START_DATE                                                           " +
                        "       AND " +
                        "       (( msg.modificationDate < :END_DATE_MIN_AGE                                                             " +
                        "       AND msg.messageState <> eu.europa.ec.etrustex.adapter.model.entity.message.MessageState.MS_PROCESSED     " +
                        "       AND msg.messageState <> eu.europa.ec.etrustex.adapter.model.entity.message.MessageState.MS_FAILED )" +
                        "       OR ( msg.modificationDate < :END_DATE                                                             " +
                        "             AND  msg.messageState = eu.europa.ec.etrustex.adapter.model.entity.message.MessageState.MS_FAILED ))"
        )
})
@Table(name = "ETX_ADT_MESSAGE")
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EtxMessage extends AuditEntity<Long> implements Serializable {

    private static final long serialVersionUID = -3189489701457347332L;

    @Id
    @Column(name = "MSG_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "MSG_TYPE", nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private MessageType messageType;

    @Column(name = "MSG_UUID", length = 250, nullable = false, updatable = false)
    private String messageUuid;

    @Column(name = "MSG_STATE", nullable = false)
    @Enumerated(EnumType.STRING)
    private MessageState messageState;

    @Column(name = "MSG_DIRECTION", nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private MessageDirection directionType;

    @Column(name = "MSG_PARENT_UUID", length = 250, updatable = false)
    private String messageParentUuid;

    @Column(name = "MSG_REF_UUID", length = 250, updatable = false)
    private String messageReferenceUuid;

    @Column(name = "MSG_REF_STATE", updatable = false)
    @Enumerated(EnumType.STRING)
    private MessageReferenceState messageReferenceStateType;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "MSG_ISSUE_DT", nullable = false, updatable = false)
    private Calendar issueDateTime;

    @Column(name = "MSG_SUBJECT", length = 250)
    private String subject;

    @Column(name = "MSG_CONTENT", length = 4000)
    private String messageContent;

    @Column(name = "MSG_STATUS_REASON_CODE", length = 250)
    private String statusReasonCode;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "MSG_SENDER_PAR_ID", nullable = false, updatable = false)
    private EtxParty sender;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "MSG_RECEIVER_PAR_ID", nullable = false, updatable = false)
    private EtxParty receiver;

    @Column(name = "MSG_NOTIFICATION_STATE")
    @Enumerated(EnumType.STRING)
    private NotificationState notificationState;

    @OneToMany(mappedBy = "message", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<EtxAttachment> attachmentList;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "ERR_ID", unique = true)
    private EtxError error;

    @Column(name = "MSG_SIGNED_DATA")
    @Lob
    private String signedData;

    @Column(name = "MSG_SIGNATURE")
    @Lob
    private String signature;

    @Column(name = "DOMIBUS_MESSAGEID")
    private String domibusMessageId;

    public boolean hasAttachments() {
        return !getAttachmentList().isEmpty();
    }

    public void addAttachment(EtxAttachment extAtt) {
        getAttachmentList().add(extAtt);
    }

    public List<EtxAttachment> getAttachmentList() {
        if (attachmentList == null) {
            attachmentList = new ArrayList<>();
        }
        return attachmentList;
    }

    public void setAttachmentList(List<EtxAttachment> attachmentList) {
        this.attachmentList = attachmentList;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public String getMessageUuid() {
        return messageUuid;
    }

    public void setMessageUuid(String messageUuid) {
        this.messageUuid = messageUuid;
    }

    public MessageState getMessageState() {
        return messageState;
    }

    public void setMessageState(MessageState messageState) {
        this.messageState = messageState;
    }

    public MessageDirection getDirectionType() {
        return directionType;
    }

    public void setDirectionType(MessageDirection directionType) {
        this.directionType = directionType;
    }

    public String getMessageParentUuid() {
        return messageParentUuid;
    }

    public void setMessageParentUuid(String messageParentUuid) {
        this.messageParentUuid = messageParentUuid;
    }

    public String getMessageReferenceUuid() {
        return messageReferenceUuid;
    }

    public void setMessageReferenceUuid(String messageReferenceUuid) {
        this.messageReferenceUuid = messageReferenceUuid;
    }

    public MessageReferenceState getMessageReferenceStateType() {
        return messageReferenceStateType;
    }

    public void setMessageReferenceStateType(MessageReferenceState messageReferenceStateType) {
        this.messageReferenceStateType = messageReferenceStateType;
    }

    public Calendar getIssueDateTime() {
        return issueDateTime;
    }

    public void setIssueDateTime(Calendar issueDateTime) {
        this.issueDateTime = issueDateTime;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getStatusReasonCode() {
        return statusReasonCode;
    }

    public void setStatusReasonCode(String statusReasonCode) {
        this.statusReasonCode = statusReasonCode;
    }

    public EtxParty getSender() {
        return sender;
    }

    public void setSender(EtxParty sender) {
        this.sender = sender;
    }

    public EtxParty getReceiver() {
        return receiver;
    }

    public void setReceiver(EtxParty receiver) {
        this.receiver = receiver;
    }

    public NotificationState getNotificationState() {
        return notificationState;
    }

    public void setNotificationState(NotificationState notificationState) {
        this.notificationState = notificationState;
    }

    public EtxError getError() {
        return error;
    }

    public void setError(EtxError error) {
        this.error = error;
    }

    public String getSignedData() {
        return signedData;
    }

    public void setSignedData(String signedData) {
        this.signedData = signedData;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getDomibusMessageId() {
        return domibusMessageId;
    }

    public void setDomibusMessageId(String domibusMessageId) {
        this.domibusMessageId = domibusMessageId;
    }

    @Override
    public String toString() {

        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", id)
                .append("messageType", messageType)
                .append("messageUuid", messageUuid)
                .append("messageState", messageState)
                .append("directionType", directionType)
                .append("messageParentUuid", messageParentUuid)
                .append("messageReferenceUuid", messageReferenceUuid)
                .append("messageReferenceStateType", messageReferenceStateType)
                .append("issueDateTime", issueDateTime != null ? issueDateTime.getTime() : null)
                .append("subject", subject)
                .append("messageContent", messageContent)
                .append("statusReasonCode", statusReasonCode)
                .append("sender", sender)
                .append("receiver", receiver)
                .append("notificationState", notificationState)
                .append("signedData", signedData)
                .append("signature", signature)
                .append("domibusMessageId", domibusMessageId)
                .append("error", error)
                .toString();
    }
}

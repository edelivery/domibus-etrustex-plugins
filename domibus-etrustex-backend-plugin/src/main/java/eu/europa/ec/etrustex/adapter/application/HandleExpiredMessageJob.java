package eu.europa.ec.etrustex.adapter.application;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.administration.AdapterConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.service.AdapterConfigurationService;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import eu.europa.ec.etrustex.common.service.WorkspaceFileService;
import eu.europa.ec.etrustex.common.util.FileUtils;
import org.joda.time.DateTime;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.nio.file.Path;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static eu.europa.ec.etrustex.adapter.model.entity.administration.AdapterConfigurationProperty.ETX_NODE_RETENTION_POLICY_WEEKS;
import static java.math.BigDecimal.valueOf;
import static java.math.RoundingMode.UP;
import static org.apache.commons.lang3.math.NumberUtils.toInt;

/**
 * Find the expired {@link EtxAttachment}
 * Delete the files related to the expired {@link EtxAttachment}
 * (older than {@link AdapterConfigurationProperty#ETX_NODE_RETENTION_POLICY_WEEKS})
 * Delete the files in the workspace that are older than
 * {@link AdapterConfigurationProperty#ETX_NODE_RETENTION_POLICY_WEEKS} +1 day
 */
@DisallowConcurrentExecution
public class HandleExpiredMessageJob extends BackEndQuartzJobBean {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(HandleExpiredMessageJob.class);

    static final int PAGE_SIZE = 10;
    static final int DEFAULT_RETENTION_WEEKS = 12;

    @Autowired
    private AttachmentService attachmentService;
    @Autowired
    @Qualifier("etxBackendPluginWorkspaceService")
    private WorkspaceFileService workspaceFileService;
    @Autowired
    private AdapterConfigurationService adapterConfigurationService;

    @Override
    public void process(JobExecutionContext jobExecutionContext) {
        LOG.info("### HandleExpiredMessageJob start ###");
        long startTime = System.currentTimeMillis();

        int retentionPolicyWeeks = getRetentionPolicyWeeks();
        Date date = DateTime.now()
                .withTimeAtStartOfDay()
                .minusWeeks(retentionPolicyWeeks)
                .toDate();
        int numberOfPages = valueOf(attachmentService.countNextAttachmentsToExpire(date))
                .divide(valueOf(PAGE_SIZE), UP)
                .intValue();
        for (int i = 0; i < numberOfPages; i++) {
            LOG.info("### HandleExpiredMessageJob: processing page: {}/{} ###", (i + 1), numberOfPages);
            attachmentService.expireAttachments(date, PAGE_SIZE, i);
        }
        Path repositoryRoot = workspaceFileService.getRoot();
        FileUtils.deleteOldFilesInDirectory(repositoryRoot, getTtlInMs(retentionPolicyWeeks));
        LOG.info("### HandleExpiredMessageJob finished after {} sec ###", (double) (System.currentTimeMillis() - startTime) / 1000);
    }

    private long getTtlInMs(int retentionPolicyWeeks) {
        return TimeUnit.MILLISECONDS.convert(retentionPolicyWeeks * 7L + 1L, TimeUnit.DAYS);
    }

    private int getRetentionPolicyWeeks() {
        final String retentionPolicyProperty = adapterConfigurationService.getAdapterConfigurations()
                .get(ETX_NODE_RETENTION_POLICY_WEEKS);
        int retentionPolicyFiles = toInt(retentionPolicyProperty, DEFAULT_RETENTION_WEEKS);
        LOG.info("Retention policy value read from DB: {} got converted to int value: {}", retentionPolicyProperty, retentionPolicyFiles);
        return retentionPolicyFiles;
    }
}

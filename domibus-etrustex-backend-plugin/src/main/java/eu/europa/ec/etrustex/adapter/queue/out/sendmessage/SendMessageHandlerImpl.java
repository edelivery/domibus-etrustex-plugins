package eu.europa.ec.etrustex.adapter.queue.out.sendmessage;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.NodeInvocationManager;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxError;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author micleva, Arun Raj
 * @version 1.0
 */
@Service("etxBackendPluginSendMessageHandler")
@Transactional
public class SendMessageHandlerImpl implements SendMessageHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SendMessageHandlerImpl.class);

    @Autowired
    private MessageServicePlugin messageService;

    @Autowired
    private NodeInvocationManager nodeInvocationManager;

    @Autowired
    private PostProcessProducer postProcessProducer;

    @Override
    public void handleSendBundle(Long messageId) {
        EtxMessage messageBundleEntity = messageService.findById(messageId);
        nodeInvocationManager.sendMessageBundle(messageBundleEntity);
    }

    @Override
    public void handleSendStatus(Long messageId) {
        EtxMessage messageStatusEntity = messageService.findById(messageId);
        nodeInvocationManager.sendMessageStatus(messageStatusEntity);
    }

    @Override
    public void validateSendBundle(Long messageId) {
        EtxMessage messageBundleEntity = messageService.findById(messageId);

        if (messageBundleEntity == null) {
            String errorDetails = String.format("Message bundle [ID: %s] not found", messageId);
            LOG.warn(errorDetails);
            throw new ETrustExPluginException(ETrustExError.DEFAULT, errorDetails, null);
        }

        final MessageState messageState = messageBundleEntity.getMessageState();

        boolean isExpectedMessageState = messageState == MessageState.MS_CREATED
                || messageState == MessageState.MS_UPLOADED;

        if (!isExpectedMessageState) {
            StringBuilder errorDetails = new StringBuilder();
            errorDetails.append("Invalid message bundle state (expected message bundle state: MS_CREATED, skipping send message bundle (to Node) with ID: ");
            errorDetails.append(messageId);
            errorDetails.append(", state: ");
            errorDetails.append(messageState);
            if(LOG.isWarnEnabled()) {
                LOG.warn(errorDetails.toString());
            }

            throw new ETrustExPluginException(ETrustExError.ETX_009, errorDetails.toString(), null);
        }
    }

    @Override
    public void validateSendStatus(Long messageId) {

        EtxMessage messageStatusEntity = messageService.findById(messageId);

        if (messageStatusEntity == null) {
            String errorDetails = String.format("Message status [ID: %s] not found", messageId);
            LOG.warn(errorDetails);
            throw new ETrustExPluginException(ETrustExError.DEFAULT, errorDetails, null);
        }

        boolean isExpectedMessageState =
                (messageStatusEntity.getMessageType() == MessageType.MESSAGE_STATUS && messageStatusEntity.getMessageState() == MessageState.MS_CREATED)
                        || (messageStatusEntity.getMessageType() == MessageType.MESSAGE_ADAPTER_STATUS && messageStatusEntity.getMessageState() == MessageState.MS_PROCESSED);

        if (!isExpectedMessageState) {
            StringBuilder errorDetails = new StringBuilder();
            errorDetails.append("Invalid message status state, skipping send message status (to Node) with ID: ");
            errorDetails.append(messageId);
            errorDetails.append(", state: ");
            errorDetails.append(messageStatusEntity.getMessageState());
            if(LOG.isWarnEnabled()) {
                LOG.warn(errorDetails.toString());
            }

            throw new ETrustExPluginException(ETrustExError.ETX_009, errorDetails.toString(), null);
        }
    }

    @Override
    public void onError(Long messageId) {
        EtxMessage messageEntity = messageService.findById(messageId);
        messageEntity.setError(new EtxError(ETrustExError.DEFAULT.getCode(), "Failed to send message " + messageEntity.getMessageUuid()));

        postProcessProducer.triggerPostProcess(messageId);
    }
}
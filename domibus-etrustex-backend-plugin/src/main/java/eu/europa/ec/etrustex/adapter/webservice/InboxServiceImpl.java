package eu.europa.ec.etrustex.adapter.webservice;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.converters.BackendMessageBundleTypeConverter;
import eu.europa.ec.etrustex.adapter.integration.converters.BackendMessageReferenceConverter;
import eu.europa.ec.etrustex.adapter.integration.converters.BackendMessageStatusTypeConverter;
import eu.europa.ec.etrustex.adapter.integration.converters.ErrorTypeConverter;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.model.entity.message.NotificationState;
import eu.europa.ec.etrustex.adapter.model.vo.MessageReferenceVO;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.adapter.service.security.IdentityManager;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.integration.model.common.v2.*;
import eu.europa.ec.etrustex.integration.service.inbox.v2.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Arrays;
import java.util.List;

import static java.util.Collections.singletonList;

/**
 * Implementation of the wsdl for the InboxService
 *
 * @author Arun Raj, François Gautier
 * @version 1.0
 * @since 12-Oct-17
 */
@Service("etxBackendPluginInboxService")
@WebService(
        serviceName = "Inbox",
        targetNamespace = "urn:eu:europa:ec:etrustex:integration:service:inbox:v2.0",
        endpointInterface = "eu.europa.ec.etrustex.integration.service.inbox.v2.InboxPortType",
        portName = "InboxSoap11"
)
public class InboxServiceImpl implements InboxPortType {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxServiceImpl.class);

    @Autowired
    private IdentityManager identityManager;

    @Autowired
    private MessageServicePlugin messageService;


    /**
     * For a given party retrieve the list of messages that are not  notified yet, i.e. - not in notification status {@link NotificationState#NF_NOTIFIED}.<br/>
     * NOTE: {@link MessageType#MESSAGE_ADAPTER_STATUS} are also returned as {@link EtxMessageCodeType#MESSAGE_STATUS}
     *
     * <p>The system user should be authorized to request the information.<br/></p>
     *
     * @param getMessageReferenceListRequestType receiver party requesting the list of messages
     * @return list of messages of type {@link MessageType#MESSAGE_BUNDLE} & {@link MessageType#MESSAGE_STATUS} with receiver that are not in Notified state
     * @throws FaultResponse in case of error
     */
    @Override
    @Transactional
    public GetMessageReferenceListResponseType getMessageReferenceList(
            @WebParam(name = "getMessageReferenceListRequest",
                    targetNamespace = "urn:eu:europa:ec:etrustex:integration:service:inbox:v2.0",
                    partName = "getMessageReferenceListRequest")
                    GetMessageReferenceListRequestType getMessageReferenceListRequestType) throws FaultResponse {
        LOG.debug("InboxService.getMessageReferenceList invocation - START");

        try {
            GetMessageReferenceListResponseType result = new GetMessageReferenceListResponseType();
            EtxMessageReferenceListType etxRefs = new EtxMessageReferenceListType();

            List<EtxMessageReferenceType> refs = etxRefs.getMessageReference();
            String receiverPartyUuid = getMessageReferenceListRequestType.getReceiver().getId();
            String username = identityManager.retrieveAuthenticatedUserName();

            if (identityManager.hasAccess(receiverPartyUuid, username)) {
                List<MessageReferenceVO> messages = messageService.findMessagesForReceiverPartyUuid(receiverPartyUuid);
                if (messages != null && !messages.isEmpty()) {
                    for (MessageReferenceVO message : messages) {
                        LOG.debug("message uuid: " + message.getMessageUuid());
                        EtxMessageReferenceType wsMessage = BackendMessageReferenceConverter.convertEtxMessage(message);
                        refs.add(wsMessage);
                    }
                }
                result.setMessageReferenceList(etxRefs);
            } else {
                String errorMessage = String.format("Authenticated user: %s does not have access to party: %s", username, receiverPartyUuid);
                LOG.warn(errorMessage);
                throw new ETrustExPluginException(ETrustExError.ETX_001, errorMessage, null);
            }

            LOG.debug("InboxService.getMessageReferenceList invocation - END");

            return result;

        } catch (ETrustExPluginException e) {
            LOG.error("Error retrieving the message reference list!", e);
            ErrorType businessError = ErrorTypeConverter.buildErrorType(e);
            throw new FaultResponse(businessError.getDescription(), businessError);
        } catch (Exception e) {
            LOG.error("Error retrieving the message reference list!", e);
            throw new FaultResponse(e.getMessage(), null);
        }
    }

    /**
     * Retrieve a {@link MessageType#MESSAGE_BUNDLE} for a given receiver party and sender party.<br/>
     * The system user should be authorized to request the information.<br/>
     *
     * @param getMessageBundleRequestType a given message id, UUID of sender and receiver party
     * @return Message bundle
     * @throws FaultResponse in case of error
     */
    @Override
    @Transactional
    public GetMessageBundleResponseType getMessageBundle(
            @WebParam(name = "getMessageBundleRequest",
                    targetNamespace = "urn:eu:europa:ec:etrustex:integration:service:inbox:v2.0",
                    partName = "getMessageBundleRequest")
                    GetMessageBundleRequestType getMessageBundleRequestType) throws FaultResponse {

        try {
            LOG.debug("InboxService.getMessageBundle invocation - START");
            GetMessageBundleResponseType result = new GetMessageBundleResponseType();
            EtxMessageReferenceType messageReference = getMessageBundleRequestType.getMessageReference();

            String messageUuid = messageReference.getId();
            String senderPartyUuid = messageReference.getSender().getId();
            String receiverPartyUuid = messageReference.getReceiver().getId();

            String username = identityManager.retrieveAuthenticatedUserName();

            if (identityManager.hasAccess(receiverPartyUuid, username)) {
                EtxMessage messageBundle = messageService.readMessage(senderPartyUuid, receiverPartyUuid, messageUuid, singletonList(MessageType.MESSAGE_BUNDLE));
                if (messageBundle != null) {
                    EtxMessageBundleType etxMessageBundle = BackendMessageBundleTypeConverter.fromEtxMessage(messageBundle);
                    result.setMessageBundle(etxMessageBundle);
                }
            } else {
                String errorMessage = String.format("Authenticated user: %s does not have access to party: %s", username, receiverPartyUuid);
                LOG.warn(errorMessage);
                throw new ETrustExPluginException(ETrustExError.ETX_001, errorMessage, null);
            }

            LOG.debug("InboxService.getMessageBundle invocation - END");
            return result;

        } catch (ETrustExPluginException e) {
            LOG.error("Error retrieving the message bundle!", e);
            ErrorType businessError = ErrorTypeConverter.buildErrorType(e);
            throw new FaultResponse(businessError.getDescription(), businessError);
        } catch (Exception e) {
            LOG.error("Error retrieving the message bundle!", e);
            throw new FaultResponse(e.getMessage(), null);
        }
    }

    /**
     * Retrieve a status message - {@link MessageType#MESSAGE_STATUS} or a failure notification status message - {@link MessageType#MESSAGE_ADAPTER_STATUS} for a given sender and receiver party.<br/>
     * The system user should be authorized to request the information.<br/>
     *
     * @param getMessageStatusRequestType a given message id, UUID of sender and receiver party
     * @return Details of Status message or Failure Notification Status message
     * @throws FaultResponse in case of error
     */
    @Override
    @Transactional
    public GetMessageStatusResponseType getMessageStatus(
            @WebParam(name = "getMessageStatusRequest",
                    targetNamespace = "urn:eu:europa:ec:etrustex:integration:service:inbox:v2.0",
                    partName = "getMessageStatusRequest")
                    GetMessageStatusRequestType getMessageStatusRequestType) throws FaultResponse {
        try {
            LOG.debug("InboxService.getMessageStatus invocation - START");
            GetMessageStatusResponseType result = new GetMessageStatusResponseType();
            EtxMessageReferenceType messageReference = getMessageStatusRequestType.getMessageReference();

            String messageUuid = messageReference.getId();
            String senderPartyUuid = messageReference.getSender().getId();
            String receiverPartyUuid = messageReference.getReceiver().getId();
            String username = identityManager.retrieveAuthenticatedUserName();

            if (identityManager.hasAccess(receiverPartyUuid, username)) {
                EtxMessage messageStatus = messageService.readMessage(senderPartyUuid, receiverPartyUuid, messageUuid, Arrays.asList(MessageType.MESSAGE_STATUS, MessageType.MESSAGE_ADAPTER_STATUS));
                if (messageStatus != null) {
                    MessageType refMessageType = messageService.referenceMessageTypeFor(messageStatus);
                    EtxMessageStatusType etxMessageStatus = BackendMessageStatusTypeConverter.convertEtxMessage(messageStatus, refMessageType);
                    result.setMessageStatus(etxMessageStatus);
                }
            } else {
                String errorMessage = "Authenticated user: " + username + " does not have access to party: " + receiverPartyUuid;
                LOG.warn(errorMessage);
                throw new ETrustExPluginException(ETrustExError.ETX_001, errorMessage, null);
            }

            LOG.debug("InboxService.getMessageStatus invocation - END");
            return result;

        } catch (ETrustExPluginException e) {
            LOG.error("Error retrieving the message status!", e);
            ErrorType businessError = ErrorTypeConverter.buildErrorType(e);
            throw new FaultResponse(businessError.getDescription(), businessError);
        } catch (Exception e) {
            LOG.error("Error retrieving the message status!", e);
            throw new FaultResponse(e.getMessage(), null);
        }
    }
}

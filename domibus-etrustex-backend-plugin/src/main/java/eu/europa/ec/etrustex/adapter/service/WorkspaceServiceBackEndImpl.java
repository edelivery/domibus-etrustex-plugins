package eu.europa.ec.etrustex.adapter.service;

import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.model.entity.administration.AdapterConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.common.service.AbstractWorkspaceService;
import eu.europa.ec.etrustex.common.util.WorkSpaceUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

/**
 * Service to access files in the backEnd node
 *
 * @author François Gautier
 * @version 1.0
 * @since 19-Dec-17
 */
@Service("etxBackendPluginWorkspaceService")
public class WorkspaceServiceBackEndImpl extends AbstractWorkspaceService {

    @Autowired
    private AdapterConfigurationService adapterConfigurationService;

    @Autowired
    @Lazy
    @Qualifier("etxBackendPluginProperties")
    private ETrustExBackendPluginProperties eTrustExBackendPluginProperties;

    /**
     * For a given {@link MessageType#MESSAGE_BUNDLE}, iterates over each attachment and tries to delete the physical file in the workspace.<br/>
     * <p>If exception is faced with an attachment then:
     * <ul>
     * <li>it proceeds with the remaining attachments,
     * <li>then waits for X number of milliseconds defined in the property - workspaceCleanUpWorker.sleepTimeOnException</li>
     * <li>and then retries the whole process again for the Bundle for a retry count defined in the property - workspaceCleanUpWorker.retryCountPerBundleOnException </li>
     * </ul>
     * </p>
     *
     */
    public void cleanWorkspaceFilesForBundleAsynchronously(EtxMessage etxMessage) {
        //For huge attachment list the entities get updated before the thread execution is completed.
        for (EtxAttachment attachmentEntity : etxMessage.getAttachmentList()) {
            try {
                if (!(attachmentEntity.getStateType().equals(AttachmentState.ATTS_IGNORED) ||
                        attachmentEntity.getStateType().equals(AttachmentState.ATTS_ALREADY_UPLOADED))) {
                    //delete the file from the workspace folder if is not ignored
                    deleteFile(attachmentEntity.getId());
                }
            } catch (Exception e) {
                LOG.warn("Faced exception while trying to delete attachment: [Id:{}, UUID:{}, path:{}] ",
                        attachmentEntity.getId(),
                        attachmentEntity.getAttachmentUuid(),
                        attachmentEntity.getFilePath(),
                        e);
            }
        }
        LOG.info("WorkspaceCleanUpWorker.cleanWorkspaceFilesForBundleAsynchronously for message: [Id:{}, UUID:{}] Completed",
                etxMessage.getId(),
                etxMessage.getMessageUuid());
    }

    /**
     * @return the root for the backEnd plugin
     */
    @Transactional(readOnly = true)
    public Path getRoot() {
        Map<AdapterConfigurationProperty, String> config = adapterConfigurationService.getAdapterConfigurations();
        String path = config.get(AdapterConfigurationProperty.ETX_ADAPTER_WORKSPACE_ROOT_PATH);
        Validate.notBlank(path, "Workspace workspaceRoot path is not configured");

        LOG.info("Using root folder: {}", path);

        return WorkSpaceUtils.getPath(Paths.get(path));
    }

    @Override
    public int getSleepMillis(){
        return eTrustExBackendPluginProperties.getCleanupSleepMillis();
    }

    @Override
    public int getTimeOut(){
        return eTrustExBackendPluginProperties.getCleanupTimeout();
    }
}

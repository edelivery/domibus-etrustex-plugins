package eu.europa.ec.etrustex.adapter.integration.client.etxnode;

import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;

import java.io.File;

/**
 *
 * @author Arun Raj
 * @since May 2017
 * @version 1.0
 */
//TODO:EDELIVERY-6608 - Remove ETX BE Plugin NodeInvocationManager facade & NodeServiceAdapters & refactor code functionally
@Deprecated
public interface NodeInvocationManager {

    /**
     * Invoke the adapter to formulate the submitDocumentBundleRequest and submit to Domibus.
     *
     * @param etxMessage
     */
    void sendMessageBundle(EtxMessage etxMessage);

    /**
     * Invoke the adapter to formulate the submitApplicationResponseRequest and submit to Domibus.
     *
     * @param statusMessage
     */
    void sendMessageStatus(EtxMessage statusMessage);

    /**
     * Invoke the adapter to formulate the storeDocumentWrapperRequest and submit to Domibus.
     *
     * @param etxAttachment
     * @param attachmentFile
     */
    void uploadAttachment(EtxAttachment etxAttachment, File attachmentFile);

    /**
     * Formulate the retrieveICA request and submit request to Domibus.
     *
     * @param senderParty
     * @return The UUID created internally and assigned for this message
     * @throws ETrustExPluginException
     */
    String sendInterchangeAgreementRequest(EtxParty senderParty);

}

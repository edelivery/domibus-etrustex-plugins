package eu.europa.ec.etrustex.adapter.queue.out.downloadattachment;

import eu.europa.ec.etrustex.adapter.queue.JmsProducer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.jms.Queue;
import java.util.ArrayList;
import java.util.List;

/**
 * @author micleva
 * @project ETX
 */

@Service("etxBackendPluginOutDownloadAttachmentProducer")
public class OutDownloadAttachmentProducer {

    public static final String ETX_BACKEND_PLUGIN_OUTBOX_DOWNLOAD_ATTACHMENT_QUEUE = "etxBackendPluginOutboxDownloadAttachment";
    private final Queue jmsQueue;

    private final JmsProducer<OutDownloadAttachmentQueueMessage> jmsProducer;

    public OutDownloadAttachmentProducer(@Qualifier(ETX_BACKEND_PLUGIN_OUTBOX_DOWNLOAD_ATTACHMENT_QUEUE) Queue jmsQueue,
                                         JmsProducer<OutDownloadAttachmentQueueMessage> jmsProducer) {
        this.jmsQueue = jmsQueue;
        this.jmsProducer = jmsProducer;
    }

    public void triggerDownload(List<Long> attachmentIds) {
        List<OutDownloadAttachmentQueueMessage> queueMessages = new ArrayList<>();

        for (Long attachmentId : attachmentIds) {
            OutDownloadAttachmentQueueMessage queueMessage = new OutDownloadAttachmentQueueMessage();
            queueMessage.setAttachmentId(attachmentId);
            queueMessages.add(queueMessage);
        }

        jmsProducer.postMessageList(jmsQueue, queueMessages);
    }
}

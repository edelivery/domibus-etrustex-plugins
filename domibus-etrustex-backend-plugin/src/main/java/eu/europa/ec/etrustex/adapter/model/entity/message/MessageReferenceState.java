package eu.europa.ec.etrustex.adapter.model.entity.message;

/**
 * @author apladap
 */
public enum MessageReferenceState {
    FAILED, DELIVERED, AVAILABLE, READ;
}

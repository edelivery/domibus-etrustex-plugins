package eu.europa.ec.etrustex.adapter.model.entity.message;

/**
 * @author: micleva
 * @date: 6/19/13 9:42 AM
 * @project: ETX
 */
public enum NotificationState {
    /**
     * An automatic notification is not used but the message still waits for being sent/consumed to/by a system
     */
    NF_PENDING,

    /**
     * An automatic notification process is pending
     */
    NF_AUTO_PENDING,

    /**
     * The receiver is notified about message (by automatic process or by consuming it from the inbox).
     * This is a final status.
     */
    NF_NOTIFIED,

    /**
     * The message was created more than X days ago where X is the value of etx.adapter.notification.expiration.days
     * from DB.
     */
    NF_EXPIRED
}

package eu.europa.ec.etrustex.adapter.model.entity.administration;

import eu.europa.ec.etrustex.adapter.model.entity.AuditEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;
/**
 * <p>
 * Title: User
 * </p>
 * <p/>
 * <p>
 * Description: Domain Object describing a User entity
 * </p>
 */
@Entity(name = "etxBackendPluginUser")
@AttributeOverrides({
        @AttributeOverride(name = "creationDate", column = @Column(name = "USR_CREATED_ON", insertable = false, updatable = false, nullable = false)),
        @AttributeOverride(name = "modificationDate", column = @Column(name = "USR_UPDATED_ON"))})
@Table(name = "ETX_ADT_USER")
@NamedQueries({
        @NamedQuery(name = "findUserByName", query = "Select usr from etxBackendPluginUser usr where usr.name = :userName",
                hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")})
})
//@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class EtxUser extends AuditEntity<Long> implements Serializable {

    private static final long serialVersionUID = 3739900488217422562L;

    @Id
    @Column(name = "USR_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "USR_NAME", length = 250, nullable = false)
    private String name;

    @Column(name = "USR_PASSWORD", length = 250, nullable = false)
    private String password;

    /**
     * Default constructor
     */
    public EtxUser() {
        //empty constructor
    }

    @Override
    public Long getId() {
        return this.id;
    }


    public String getName() {
        return this.name;
    }

    public String getPassword() {
        return this.password;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
                .append("id", id)
                .append("name", name)
                .toString();
    }
}

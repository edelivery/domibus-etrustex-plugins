package eu.europa.ec.etrustex.adapter.queue;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.queue.out.uploadattachment.OutUploadAttachmentHandlerImpl;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import static eu.europa.ec.etrustex.adapter.queue.ETrustExBackendPluginJMSErrorUtils.ETX_BACKEND_PLUGIN_JMS_ERROR_UTILS_SERVICE_NAME;

/**
 * Utility class for JMS error logging
 * @author Arun Raj
 * @since 1.3.4
 */
@Service(ETX_BACKEND_PLUGIN_JMS_ERROR_UTILS_SERVICE_NAME)
public class ETrustExBackendPluginJMSErrorUtils {
    public static final String ETX_BACKEND_PLUGIN_JMS_ERROR_UTILS_SERVICE_NAME = "etxBackendPluginJMSErrorUtils";
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ETrustExBackendPluginJMSErrorUtils.class);

    /**
     * JMS Error logging to handle special case of "file not found in workspace" error originating from {@link eu.europa.ec.etrustex.adapter.queue.out.downloadattachment.OutDownloadAttachmentHandlerImpl}.
     *
     * @param Throwable
     * @return true - if error originated from OutDownloadAttachmentHandlerImpl - File not found in workspace else returns false
     */
    public boolean handleOutDownloadAttachmentHandlerFileNotFoundErrorLogging(Throwable t){
        if (t instanceof ETrustExPluginException) {
            ETrustExPluginException etxPluginException = (ETrustExPluginException) t;
            if (etxPluginException.getError() == ETrustExError.ETX_007
                    && StringUtils.startsWith(etxPluginException.getMessage(), OutUploadAttachmentHandlerImpl.ATTACHMENT_FILE_NOT_IN_WORKSPACE_ERROR_PREFIX)
                    && StringUtils.contains(etxPluginException.getMessage(), OutUploadAttachmentHandlerImpl.ATTACHMENT_FILE_NOT_IN_WORKSPACE_ERROR_SUFFIX)) {
                //skip logging stack trace as it causes confusion to Ops that it is actual error. Warning already raised in OutUploadAttachment handler.
                LOG.trace("Error:", t);
                return true;
            }
        }
        return false;
    }

}

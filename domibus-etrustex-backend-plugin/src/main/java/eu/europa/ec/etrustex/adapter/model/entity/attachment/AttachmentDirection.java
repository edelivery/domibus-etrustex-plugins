/**
 *
 */
package eu.europa.ec.etrustex.adapter.model.entity.attachment;

/**
 * @author apladap
 */
public enum AttachmentDirection {
    INCOMING, OUTGOING;
}

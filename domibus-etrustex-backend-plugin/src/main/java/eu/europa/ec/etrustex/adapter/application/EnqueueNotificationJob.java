package eu.europa.ec.etrustex.adapter.application;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.NotificationState;
import eu.europa.ec.etrustex.adapter.persistence.api.message.MessageDao;
import eu.europa.ec.etrustex.adapter.queue.notify.NotifyProducer;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Cron job to PUSH notifications to the backEnd.
 * The notification sent to the backEnd concerned only the {@link EtxMessage} that has NOT been notified yet
 *
 * Unnotified message have a {@link NotificationState#NF_AUTO_PENDING}
 *
 * @author François Gautier
 * @version 1.0
 * @since 05-Jan-18
 */
@DisallowConcurrentExecution
public class EnqueueNotificationJob extends BackEndQuartzJobBean {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(EnqueueNotificationJob.class);

    @Autowired
    private MessageDao messageDao;

    @Autowired
    private NotifyProducer notifyProducer;

    /**
     * Use of a jmsQueue notify to send notification to the backEnd of ALL {@link EtxMessage} that have
     * {@link NotificationState#NF_AUTO_PENDING}
     */
    @Override
    public void process(JobExecutionContext context) {
        LOG.debug("enqueueForNotification starting ...");
        List<EtxMessage> messages = messageDao.findMessagesByNotificationState(NotificationState.NF_AUTO_PENDING);
        for (EtxMessage message : messages) {
            try {
                notifyProducer.triggerNotify(message.getId());
            } catch (Exception e) {
                LOG.error("Unable to trigger the notify message [ID: {}]", message.getId());
                throw e;
            }
        }

        LOG.debug("enqueueForNotification done.");
    }
}

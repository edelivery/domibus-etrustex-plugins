package eu.europa.ec.etrustex.adapter.service;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.PartyDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation for services related to Cache management
 *
 * @author Arun Raj
 * @version 1.0
 * @since 18/07/2017
 */

@Service("etxBackendPluginPartyService")
public class EtxPartyServiceImpl implements EtxPartyService {

    @Autowired
    private PartyDao partyDao;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public EtxParty create(EtxParty etxParty) {
        partyDao.save(etxParty);
        return etxParty;
    }
}

package eu.europa.ec.etrustex.adapter.model.entity.attachment;

/**
 * @author: micleva
 * @date: 6/17/13 9:47 AM
 * @project: ETX
 */
public enum ChecksumAlgorithmType {
    SHA_512("SHA-512");

    private String value;

    ChecksumAlgorithmType(String value) {
        this.value = value;
    }

    public static ChecksumAlgorithmType fromValue(String value) {
        for (ChecksumAlgorithmType checksumAlgorithmType : values()) {
            if (checksumAlgorithmType.value.equals(value)) {
                return checksumAlgorithmType;
            }
        }
        throw new IllegalArgumentException("No matching enum found for value: " + value);
    }

    public String getValue() {
        return value;
    }
}

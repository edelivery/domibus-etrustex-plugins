package eu.europa.ec.etrustex.adapter.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * An audited and versioned entity.
 */
@MappedSuperclass
public abstract class AuditEntity<T extends Serializable> extends AbstractEntity<T> {

    @Temporal(TemporalType.TIMESTAMP)
    private Calendar creationDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Calendar modificationDate;

    public Calendar getCreationDate() {
        return this.creationDate;
    }

    public Calendar getModificationDate() {
        return this.modificationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    public void setModificationDate(Calendar modificationDate) {
        this.modificationDate = modificationDate;
    }

    @PrePersist
    protected void onCreate() {
        creationDate = modificationDate = getGMTCalendar();
    }

    @PreUpdate
    protected void onUpdate() {
        modificationDate = getGMTCalendar();
    }

    // MySQL doesn't support datetime in ISO8601 (only yyyy-MM-dd HH:mm:ss, instead of yyyy-MM-dd'T'HH:mm:ssZ),
    // create a calendar with no timezone info (default GMT)
    public static Calendar getGMTCalendar() {
        Calendar calendar = Calendar.getInstance();
        Calendar gregorianCalendar = new GregorianCalendar(calendar.get(GregorianCalendar.YEAR), calendar.get(GregorianCalendar.MONTH), calendar.get(GregorianCalendar.DAY_OF_MONTH),
                calendar.get(GregorianCalendar.HOUR_OF_DAY), calendar.get(GregorianCalendar.MINUTE), calendar.get(GregorianCalendar.SECOND));
        return gregorianCalendar;
    }
}

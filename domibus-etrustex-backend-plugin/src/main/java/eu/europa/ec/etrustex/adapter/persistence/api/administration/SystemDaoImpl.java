package eu.europa.ec.etrustex.adapter.persistence.api.administration;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBaseImpl;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class SystemDaoImpl extends DaoBaseImpl<EtxSystem> implements SystemDao {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(SystemDaoImpl.class);

    public SystemDaoImpl() {
        super(EtxSystem.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EtxSystem findSystemByLocalUserName(String userName) {
        TypedQuery<EtxSystem> query = getEntityManager().createNamedQuery("systemByLocalUserName", EtxSystem.class);
        query.setParameter("localUserName", userName);
        List<EtxSystem> resultList = query.getResultList();
        if (resultList.isEmpty()) {
            LOG.warn("No EtxSystem found for LocalUserName {}", userName);
            return null;
        }
        return resultList.get(0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EtxSystem findSystemByName(String systemName) {
        TypedQuery<EtxSystem> query = getEntityManager().createNamedQuery("systemByName", EtxSystem.class);
        query.setParameter("systemName", systemName);
        List<EtxSystem> resultList = query.getResultList();
        if (resultList.isEmpty()) {
            LOG.warn("No EtxSystem found for SystemByName {}", systemName);
            return null;
        }
        return resultList.get(0);
    }
}

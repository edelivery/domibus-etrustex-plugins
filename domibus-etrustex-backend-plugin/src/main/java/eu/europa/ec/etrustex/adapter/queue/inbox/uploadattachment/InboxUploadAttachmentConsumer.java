package eu.europa.ec.etrustex.adapter.queue.inbox.uploadattachment;

import eu.europa.ec.etrustex.adapter.queue.AbstractQueueListener;
import org.springframework.stereotype.Component;

/**
 * Consumer for messages in JMS queue {@code}InboxUploadAttachment{@code}
 *
 * @author François GAUTIER
 * @version 1.0
 * @since Oct 2017
 */
@Component("etxBackendPluginInboxUploadAttachmentConsumer")
public class InboxUploadAttachmentConsumer extends AbstractQueueListener<InboxUploadAttachmentQueueMessage> {

    private final InboxUploadAttachmentHandler handler;

    public InboxUploadAttachmentConsumer(InboxUploadAttachmentHandler handler) {
        this.handler = handler;
    }

    /**
     * Handle triggers from Inbox Upload Attachment Queue at backend plugin
     *
     * @param queueMessage {@link InboxUploadAttachmentQueueMessage}
     */
    @Override
    public void handleMessage(InboxUploadAttachmentQueueMessage queueMessage) {
        handler.handleUploadAttachment(queueMessage.getAttachmentId());
    }

    /**
     * Validate the message in Inbox Upload Attachment Queue prior to handling
     *
     * @param queueMessage {@link InboxUploadAttachmentQueueMessage}
     */
    @Override
    public void validateMessage(InboxUploadAttachmentQueueMessage queueMessage) {
        handler.validateUploadAttachment(queueMessage.getAttachmentId());
    }
}

package eu.europa.ec.etrustex.adapter.service.security;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;

/**
 * @author ARUN
 * @version 1.0
 */
public interface IdentityManager {

    /**
     * The system user (specified in basic authentication of the webservice) is authorized to send messages on behalf of a sender party if the following condition is satisfied:<br/>
     * The system related to the sender party = The system linked with the user initiating the operation<br/>
     *
     * @param partyUuid UUID of the {@link EtxParty}
     * @param userName username used
     * @return true if the username has access to the given {@link EtxParty}
     */
    boolean hasAccess(String partyUuid, String userName);

    EtxParty searchPartyByUuid(String partyUuid);

    /**
     * Searches the Spring Security Context for user authentication and returns the authenticated user name.
     *
     * @return (nullable) authenticated user name
     */
    String retrieveAuthenticatedUserName();
}

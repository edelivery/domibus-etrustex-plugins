package eu.europa.ec.etrustex.adapter.queue.out.sendmessage;

import eu.europa.ec.etrustex.adapter.queue.JmsProducer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.jms.Queue;

/**
 * @author micleva
 * @project ETX
 */

@Service("etxBackendPluginSendMessageProducer")
public class SendMessageProducer {

    public static final String ETX_BACKEND_PLUGIN_OUTBOX_SEND_MESSAGE_BUNDLE_QUEUE = "etxBackendPluginOutboxSendMessageBundle";
    private final Queue jmsQueue;

    private final JmsProducer<SendMessageQueueMessage> jmsProducer;

    public SendMessageProducer(@Qualifier(ETX_BACKEND_PLUGIN_OUTBOX_SEND_MESSAGE_BUNDLE_QUEUE) Queue jmsQueue,
                               JmsProducer<SendMessageQueueMessage> jmsProducer) {
        this.jmsQueue = jmsQueue;
        this.jmsProducer = jmsProducer;
    }

    public void triggerSendBundle(Long messageId) {
        SendMessageQueueMessage queueMessage = new SendMessageQueueMessage(SendMessageQueueMessage.Type.BUNDLE);
        queueMessage.setMessageId(messageId);

        jmsProducer.postMessage(jmsQueue, queueMessage);
    }

    public void triggerSendStatus(Long messageId) {
        SendMessageQueueMessage queueMessage = new SendMessageQueueMessage(SendMessageQueueMessage.Type.STATUS);
        queueMessage.setMessageId(messageId);

        jmsProducer.postMessage(jmsQueue, queueMessage);
    }
}

package eu.europa.ec.etrustex.adapter.webservice;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.converters.ErrorTypeConverter;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.ica.EtxIca;
import eu.europa.ec.etrustex.adapter.service.IcaService;
import eu.europa.ec.etrustex.adapter.service.security.IdentityManager;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import eu.europa.ec.etrustex.integration.model.common.v2.ErrorType;
import eu.europa.ec.etrustex.integration.model.common.v2.InterchangeAgreementType;
import eu.europa.ec.etrustex.integration.service.retrieveinterchangeagreements.v2.FaultResponse;
import eu.europa.ec.etrustex.integration.service.retrieveinterchangeagreements.v2.RetrieveInterchangeAgreementsPortType;
import eu.europa.ec.etrustex.integration.service.retrieveinterchangeagreements.v2.RetrieveInterchangeAgreementsRequestType;
import eu.europa.ec.etrustex.integration.service.retrieveinterchangeagreements.v2.RetrieveInterchangeAgreementsResponseType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * Implementation of the wsdl to retrieve InterchangeAgreements
 *
 * @author Arun Raj, Federico Martini
 * @version 1.0
 * @since 16/06/2017
 */
@WebService(
        name = "RetrieveInterchangeAgreementsServiceV2",
        portName = "RetrieveInterchangeAgreementsSoap11",
        serviceName = "RetrieveInterchangeAgreementsService",
        targetNamespace = "urn:eu:europa:ec:etrustex:integration:service:RetrieveInterchangeAgreements:v2.0",
        endpointInterface = "eu.europa.ec.etrustex.integration.service.retrieveinterchangeagreements.v2.RetrieveInterchangeAgreementsPortType"
)
public class RetrieveInterchangeAgreementsServiceImpl implements RetrieveInterchangeAgreementsPortType {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(RetrieveInterchangeAgreementsServiceImpl.class);

    @Autowired
    private IdentityManager identityManager;

    @Autowired
    private IcaService icaService;

    @Override
    @Transactional
    public RetrieveInterchangeAgreementsResponseType retrieveInterchangeAgreements(
            @WebParam(name = "retrieveInterchangeAgreementsRequest", targetNamespace = "urn:eu:europa:ec:etrustex:integration:service:RetrieveInterchangeAgreements:v2.0",
                    partName = "retrieveInterchangeAgreementsRequest") RetrieveInterchangeAgreementsRequestType retrieveInterchangeAgreementsRequest) throws FaultResponse {

        LOG.debug("Entered into RetrieveInterchangeAgreementsServiceImpl");

        String senderPartyUuid = null;
        String receiverPartyUuid = null;

        RetrieveInterchangeAgreementsResponseType retrieveIcaResponseType = new RetrieveInterchangeAgreementsResponseType();

        try {
            senderPartyUuid = retrieveInterchangeAgreementsRequest.getSender().getId();
            receiverPartyUuid = retrieveInterchangeAgreementsRequest.getReceiver().getId();

            String username = identityManager.retrieveAuthenticatedUserName();

            if (identityManager.hasAccess(senderPartyUuid, username)) {
                LOG.debug("Passed identity check");
                EtxParty senderParty = identityManager.searchPartyByUuid(senderPartyUuid);
                EtxParty receiverParty = identityManager.searchPartyByUuid(receiverPartyUuid);
                // Attempts to get the response
                EtxIca etxIca = icaService.getIca(senderParty, receiverParty);
                if (etxIca != null) {
                    retrieveIcaResponseType.setInterchangeAgreement(TransformerUtils.<InterchangeAgreementType>getObjectFromXml(etxIca.getXml()));
                } else {
                    String errorMessage = "Security Information returned from EtrustEx node is empty for sender[" + senderPartyUuid + "] and receiver[" + receiverPartyUuid + "]";
                    LOG.warn(errorMessage);
                    // TODO a specific code should be used here but this is maintained for backward compatibility.
                    throw new ETrustExPluginException(ETrustExError.ETX_001, errorMessage);
                }
            } else {
                String errorMessage = "Authenticated user: " + username + " does not have access to party: " + senderPartyUuid;
                LOG.warn(errorMessage);
                throw new ETrustExPluginException(ETrustExError.ETX_001, errorMessage);
            }

        } catch (ETrustExPluginException pex) {
            LOG.error("Could not retrieve ICA", pex);
            ErrorType businessError = ErrorTypeConverter.buildErrorType(pex);
            throw new FaultResponse(pex.getError().getDescription(), businessError, pex);
        } catch (Exception ex) {
            String errorMessage = "Exception occurred while checking interchange agreements with sender[" + senderPartyUuid + "] and receiver[" + receiverPartyUuid + "]";
            LOG.error(errorMessage, ex);
            ErrorType error = new ErrorType();
            error.setCode(ETrustExError.DEFAULT.getCode());
            error.setDescription(ETrustExError.DEFAULT.getDescription());
            error.setDetail(ex.getMessage());
            throw new FaultResponse(errorMessage, error);
        }
        return retrieveIcaResponseType;
    }

}

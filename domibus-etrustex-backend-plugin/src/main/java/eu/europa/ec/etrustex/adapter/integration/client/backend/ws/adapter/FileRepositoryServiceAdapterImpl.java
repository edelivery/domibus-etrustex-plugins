package eu.europa.ec.etrustex.adapter.integration.client.backend.ws.adapter;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.client.backend.ws.BackendWebServiceBase;
import eu.europa.ec.etrustex.adapter.integration.converters.BackendMessageReferenceConverter;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.vo.FileAttachmentVO;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.integration.model.common.v2.EncryptionInformationType;
import eu.europa.ec.etrustex.integration.model.common.v2.EtxMessageReferenceType;
import eu.europa.ec.etrustex.integration.model.common.v2.FileWrapperType;
import eu.europa.ec.etrustex.integration.service.filerepository.v2.*;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.xml.ws.BindingProvider;
import java.io.File;

/**
 * Saving directly the data handler to avoid loading streams of data in memory, not efficient for large files.
 *
 * @author Arun Raj, Martini Federico
 * @version 2.0
 * @author: micleva
 * @date: 6/26/12 1:59 PM
 * @since 30/01/2018
 * <p>
 * First version from
 */
public class FileRepositoryServiceAdapterImpl extends BackendWebServiceBase implements FileRepositoryServiceAdapter {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(FileRepositoryServiceAdapterImpl.class);

    public FileRepositoryServiceAdapterImpl(BackendWebServiceProvider webServiceProvider) {
        super(webServiceProvider);
    }

    @Override
    public FileAttachmentVO downloadAttachment(EtxMessage messageBundle, EtxAttachment etxAttachment) {
        LOG.debug("downloadAttachment start ...");

        // Prepare WS connection
        FileRepositoryPortType port = buildFileRepositoryPortType();

        // Create get file request
        GetFileRequestType getFileRequest = new GetFileRequestType();

        EtxMessageReferenceType messageReferenceSdo = BackendMessageReferenceConverter.convertEtxMessage(messageBundle);
        getFileRequest.setMessageReference(messageReferenceSdo);
        getFileRequest.setFileId(etxAttachment.getAttachmentUuid());

        DataHandler dataHandler = null;

        // Invoke web service
        GetFileResponseType getFileResponseType;
        try {
            getFileResponseType = port.getFile(getFileRequest);
        } catch (FaultResponse fex) {
            LOG.error("",fex);
            throw new ETrustExPluginException(ETrustExError.ETX_001, fex.getMessage(), fex);
        } catch (Exception ex) {
            LOG.error("", ex);
            throw new ETrustExPluginException(ETrustExError.DEFAULT, ex.getMessage(), ex);
        }

        FileAttachmentVO fileAttachmentVO = new FileAttachmentVO();
        if (getFileResponseType.getFileWrapper() != null) {
            dataHandler = getFileResponseType.getFileWrapper().getContent();
            EncryptionInformationType encryptionInformationType = getFileResponseType.getFileWrapper().getEncryptionInformation();

            if (encryptionInformationType != null) {
                if (encryptionInformationType.getSessionKey() != null) {
                    fileAttachmentVO.setSessionKey(encryptionInformationType.getSessionKey());
                }

                if (encryptionInformationType.getX509SubjectName() != null) {
                    fileAttachmentVO.setX509SubjectName(encryptionInformationType.getX509SubjectName());
                }
            }
            LOG.info("BACKEND.FileRepositoryService.getFile ID: [{}]", etxAttachment.getId());
        }

        LOG.debug("downloadAttachment done.");

        fileAttachmentVO.setDataHandler(dataHandler);

        return fileAttachmentVO;
    }

    @Override
    public void uploadAttachment(EtxMessage messageBundle, EtxAttachment etxAttachment, File attachmentFile) {
        // Prepare WS connection
        FileRepositoryPortType port = buildFileRepositoryPortType();

        // Create send file request
        SendFileRequestType sendFileRequest = new SendFileRequestType();

        EtxMessageReferenceType messageReferenceSdo = BackendMessageReferenceConverter.convertEtxMessage(messageBundle);
        sendFileRequest.setMessageReference(messageReferenceSdo);
        sendFileRequest.setFileId(etxAttachment.getAttachmentUuid());

        FileWrapperType fileWrapper = new FileWrapperType();
        fileWrapper.setContent(new DataHandler(new FileDataSource(attachmentFile)));

        EncryptionInformationType encryptionInformationType = new EncryptionInformationType();
        if (etxAttachment.getSessionKey() != null) {
            encryptionInformationType.setSessionKey(etxAttachment.getSessionKey());
            fileWrapper.setEncryptionInformation(encryptionInformationType);
        }
        if (etxAttachment.getX509SubjectName() != null) {
            encryptionInformationType.setX509SubjectName(etxAttachment.getX509SubjectName());
            fileWrapper.setEncryptionInformation(encryptionInformationType);
        }

        sendFileRequest.setFileWrapper(fileWrapper);

        // Invoke web service
        try {
            port.sendFile(sendFileRequest);
        } catch (FaultResponse e) {
            throw new ETrustExPluginException(ETrustExError.ETX_001, e.getMessage(), e);
        } catch (Exception e) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, e.getMessage(), e);
        }
    }

    private FileRepositoryPortType buildFileRepositoryPortType() {
        FileRepositoryPortType port = (FileRepositoryPortType) webServiceProvider.buildJaxWsProxy(FileRepositoryPortType.class,
                true, serviceEndpoint);

        BindingProvider bindingProvider = (BindingProvider) port;
        webServiceProvider.setupConnectionCredentials(bindingProvider, userName, password.getBytes());

        return port;
    }
}

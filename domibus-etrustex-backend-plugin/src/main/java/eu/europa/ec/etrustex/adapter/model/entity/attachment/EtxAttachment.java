package eu.europa.ec.etrustex.adapter.model.entity.attachment;

import eu.europa.ec.etrustex.adapter.model.entity.AuditEntity;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxError;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.common.model.entity.attachment.AttachmentType;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Domain Object describing a Attachment entity
 *
 * @author Valer Micle, Arun Raj
 * @version 1.0
 * @since 23-Feb-2017
 */
@Entity(name = "EtxAttachment")
@AttributeOverrides({
        @AttributeOverride(name = "creationDate", column = @Column(name = "ATT_CREATED_ON", insertable = false, updatable = false, nullable = false)),
        @AttributeOverride(name = "modificationDate", column = @Column(name = "ATT_UPDATED_ON"))})
@NamedQueries({
        @NamedQuery(name = "countMessageAttachmentsNotInState", query = "SELECT COUNT(att) FROM EtxAttachment att where att.activeState = true and att.message.id = :messageId and att.stateType NOT IN (:attachmentStateList)"),
        @NamedQuery(name = "countMessageAttachmentsInState", query = "SELECT COUNT(att) FROM EtxAttachment att where att.activeState = true and att.message.id = :messageId and att.stateType IN (:attachmentStateList)"),
        @NamedQuery(name = "countAttachmentsBySystemAndUUIDAndState", query = "SELECT COUNT(att) FROM EtxAttachment att " +
                "WHERE att.activeState = true " +
                "AND att.directionType = 'OUTGOING' " +
                "AND att.attachmentUuid = :attachmentUUID " +
                "AND att.stateType IN (:attachmentStateList) " +
                "AND att.message.sender.system.id =:senderSystemId"),
        @NamedQuery(name = "expiredAttachments", query = "SELECT a FROM EtxAttachment a " +
                "WHERE  a.creationDate < :endDate " +
                "AND a.activeState = true"),
        @NamedQuery(name = "countExpiredAttachments", query = "SELECT COUNT(a) FROM EtxAttachment a " +
                "WHERE  a.creationDate < :endDate " +
                "AND a.activeState = true"),
        @NamedQuery(name = "findAttachmentByDomibusMessageId", query = "SELECT att " +
                "FROM EtxAttachment att " +
                "WHERE att.domibusMessageId = :domibusMessageId"),
        @NamedQuery(name = "BEPlugin.EtxAttachment.updateStuckAttachment", query =
                "Update EtxAttachment a " +
                        "SET a.stateType = eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState.ATTS_FAILED," +
                        "    a.error = :ERROR  " +
                        "WHERE a.modificationDate > :START_DATE " +
                        "AND a.modificationDate < :END_DATE " +
                        "AND a.directionType = eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentDirection.OUTGOING " +
                        "AND a.stateType IN (:ATTACHMENT_STATE_LIST)"),
        @NamedQuery(name = "BEPlugin.EtxAttachment.findAttachmentsByUUIDs_Direction_State", query = "SELECT a FROM EtxAttachment  a " +
                "WHERE a.attachmentUuid IN (:attachmentUUIDList) " +
                "AND a.directionType IN (:attachmentDirectionList) " +
                "AND a.stateType IN (:attachmentStateList)")
})
@Table(name = "ETX_ADT_ATTACHMENT")
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EtxAttachment extends AuditEntity<Long> implements Serializable {

    private static final long serialVersionUID = 5903119519214591854L;

    @Id
    @Column(name = "ATT_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ATT_UUID", length = 250, nullable = false,  updatable = false)
    private String attachmentUuid;

    @Column(name = "ATT_TYPE", nullable = false,  updatable = false)
    @Enumerated(EnumType.STRING)
    private AttachmentType attachmentType;

    @Column(name = "ATT_STATE", nullable = false)
    @Enumerated(EnumType.STRING)
    private AttachmentState stateType;

    @Column(name = "ATT_DIRECTION", nullable = false,  updatable = false)
    @Enumerated(EnumType.STRING)
    private AttachmentDirection directionType;

    @Column(name = "ATT_FILE_NAME", length = 250)
    private String fileName;

    @Column(name = "ATT_FILE_PATH", length = 250)
    private String filePath;

    @Column(name = "ATT_FILE_SIZE")
    private Long fileSize;

    @Column(name = "ATT_MIME_TYPE")
    private String mimeType;

    @Column(name = "ATT_CHECKSUM")
    private byte[] checksum;

    @Column(name = "ATT_CHECKSUM_ALGORITHM")
    @Enumerated(EnumType.STRING)
    private ChecksumAlgorithmType checksumAlgorithmType;

    @Column(name = "ATT_LANGUAGE_CODE", length = 3,  updatable = false)
    private String languageCode;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH}, optional = false)
    @JoinColumn(name = "MSG_ID", nullable = false)
    private EtxMessage message;

    @Column(name = "ATT_SESSION_KEY")
    private byte[] sessionKey;

    @Column(name = "ATT_X509_SUBJECT_NAME", length = 250)
    private String X509SubjectName;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL/*, orphanRemoval = false*/)
    @JoinColumn(name = "ERR_ID", unique = true)
    private EtxError error;

    @Column(name = "ATT_ACTIVE_STATE", nullable = false)
    private boolean activeState;

    @Column(name = "DOMIBUS_MESSAGEID")
    private String domibusMessageId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAttachmentUuid() {
        return attachmentUuid;
    }

    public void setAttachmentUuid(String attachmentUuid) {
        this.attachmentUuid = attachmentUuid;
    }

    public AttachmentType getAttachmentType() {
        return attachmentType;
    }

    public void setAttachmentType(AttachmentType attachmentType) {
        this.attachmentType = attachmentType;
    }

    public AttachmentState getStateType() {
        return stateType;
    }

    public void setStateType(AttachmentState stateType) {
        this.stateType = stateType;
    }

    public AttachmentDirection getDirectionType() {
        return directionType;
    }

    public void setDirectionType(AttachmentDirection directionType) {
        this.directionType = directionType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public byte[] getChecksum() {
        return checksum;
    }

    public void setChecksum(byte[] checksum) {
        this.checksum = checksum;
    }

    public ChecksumAlgorithmType getChecksumAlgorithmType() {
        return checksumAlgorithmType;
    }

    public void setChecksumAlgorithmType(ChecksumAlgorithmType checksumAlgorithmType) {
        this.checksumAlgorithmType = checksumAlgorithmType;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public EtxMessage getMessage() {
        return message;
    }

    public void setMessage(EtxMessage message) {
        this.message = message;
    }

    public byte[] getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(byte[] sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getX509SubjectName() {
        return X509SubjectName;
    }

    public void setX509SubjectName(String x509SubjectName) {
        X509SubjectName = x509SubjectName;
    }

    public EtxError getError() {
        return error;
    }

    public void setError(EtxError error) {
        this.error = error;
    }

    public boolean isActiveState() {
        return activeState;
    }

    public void setActiveState(boolean activeState) {
        this.activeState = activeState;
    }

    public String getDomibusMessageId() {
        return domibusMessageId;
    }

    public void setDomibusMessageId(String domibusMessageId) {
        this.domibusMessageId = domibusMessageId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", id)
                .append("attachmentUuid", attachmentUuid)
                .append("attachmentType", attachmentType)
                .append("stateType", stateType)
                .append("directionType", directionType)
                .append("fileName", fileName)
                .append("filePath", filePath)
                .append("fileSize", fileSize)
                .append("mimeType", mimeType)
                .append("checksumAlgorithmType", checksumAlgorithmType)
                .append("checksum", checksum != null ? StringUtils.newStringUtf8(checksum) : null)
                .append("languageCode", languageCode)
                .append("X509SubjectName", X509SubjectName)
                .append("activeState", activeState)
                .append("domibusMessageId", domibusMessageId)
                .append("message", message)
                .append("error", error)
                .toString();
    }

}

package eu.europa.ec.etrustex.adapter.queue.postprocess;

import eu.europa.ec.etrustex.adapter.queue.JmsProducer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.jms.Queue;

/**
 * @author micleva
 * @project ETX
 */

@Service("etxBackendPluginPostProcessProducer")
public class PostProcessProducer {

    private final Queue jmsQueue;

    private final JmsProducer<PostProcessQueueMessage> jmsProducer;

    public PostProcessProducer(@Qualifier("etxBackendPluginPostProcessing") Queue jmsQueue,
                               JmsProducer<PostProcessQueueMessage> jmsProducer) {
        this.jmsQueue = jmsQueue;
        this.jmsProducer = jmsProducer;
    }

    public void triggerPostProcess(Long messageId) {
        PostProcessQueueMessage queueMessage = new PostProcessQueueMessage(messageId);
        jmsProducer.postMessage(jmsQueue, queueMessage);
    }

    public void triggerPostProcess(Long messageId, Long attachmentId) {
        PostProcessQueueMessage queueMessage = new PostProcessQueueMessage(messageId, attachmentId);
        jmsProducer.postMessage(jmsQueue, queueMessage);
    }
}

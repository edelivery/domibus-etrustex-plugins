package eu.europa.ec.etrustex.adapter.model.entity.ica;

import eu.europa.ec.etrustex.adapter.model.entity.AuditEntity;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "EtxIca")
@AttributeOverrides({
        @AttributeOverride(name = "creationDate", column = @Column(name = "ICA_CREATED_ON", insertable = false, updatable = false, nullable = false)),
        @AttributeOverride(name = "modificationDate", column = @Column(name = "ICA_UPDATED_ON"))})
@NamedQueries({
        @NamedQuery(
                name = "EtxIca.findBySenderUUIDAndReceiverUUID",
                query = "SELECT p " +
                        "FROM EtxIca p " +
                        "WHERE p.sender.partyUuid = :senderUuid AND p.receiver.partyUuid = :receiverUuid"
        )
})
@Table(name = "ETX_ADT_ICA")
public class EtxIca extends AuditEntity<Long> implements Serializable {

    @Id
    @Column(name = "ICA_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ICA_SENDER_PAR_ID", nullable = false, updatable = false)
    private EtxParty sender;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ICA_RECEIVER_PAR_ID", nullable = false, updatable = false)
    private EtxParty receiver;

    @Column(name = "ICA_XML")
    private String xml;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public EtxParty getSender() {
        return sender;
    }

    public void setSender(EtxParty sender) {
        this.sender = sender;
    }

    public EtxParty getReceiver() {
        return receiver;
    }

    public void setReceiver(EtxParty receiver) {
        this.receiver = receiver;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

}

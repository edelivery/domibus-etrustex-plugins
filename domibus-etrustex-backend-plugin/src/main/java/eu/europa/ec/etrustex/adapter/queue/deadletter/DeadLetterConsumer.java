package eu.europa.ec.etrustex.adapter.queue.deadletter;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.queue.AbstractQueueListener;
import eu.europa.ec.etrustex.common.jms.QueueMessage;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import static eu.europa.ec.etrustex.common.exceptions.EtrustExMarker.NON_RECOVERABLE;

@Component("etxBackendPluginDeadLetterConsumer")
public class DeadLetterConsumer extends AbstractQueueListener<QueueMessage> {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DeadLetterConsumer.class);

    @Override
    public void handleMessage(QueueMessage queueMessage) {
        String text = queueMessage.toString();
        if (queueMessage instanceof TextMessage) {
            try {
                text = ((TextMessage) queueMessage).getText();
            } catch (JMSException e) {
                LOG.error(NON_RECOVERABLE, e.getMessage(), e);
            }
        }
        LOG.error(NON_RECOVERABLE, "Dead Letter: {}", text);
    }

    @Override
    public void validateMessage(QueueMessage object) {
        //nothing to validate
    }
}
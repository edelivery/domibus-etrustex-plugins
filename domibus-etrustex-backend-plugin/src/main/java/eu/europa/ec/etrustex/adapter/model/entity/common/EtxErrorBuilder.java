package eu.europa.ec.etrustex.adapter.model.entity.common;

import static eu.europa.ec.etrustex.adapter.model.entity.common.EtxError.MAX_DETAIL;

/**
 * @Author François Gautier
 * @Since 26-Sep-17
 * @Version 1.0
 */
public class EtxErrorBuilder {
    private String responseCode;

    private String detail;

    private EtxErrorBuilder() {
    }

    public static EtxErrorBuilder getInstance(){
        return new EtxErrorBuilder();
    }

    public EtxErrorBuilder withResponseCode(String responseCode) {
        this.responseCode = responseCode;
        return this;
    }

    public EtxErrorBuilder withDetail(String detail) {
        this.detail = detail;
        if(detail!= null && detail.length() > MAX_DETAIL) {
            this.detail = detail.substring(0, MAX_DETAIL);
        }
        return this;
    }

    public EtxError build(){
        return new EtxError(responseCode, detail);
    }
}

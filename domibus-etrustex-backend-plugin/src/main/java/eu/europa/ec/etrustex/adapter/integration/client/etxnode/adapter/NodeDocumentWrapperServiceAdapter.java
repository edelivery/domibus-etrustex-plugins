package eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter;

import ec.schema.xsd.commonaggregatecomponents_2.*;
import ec.schema.xsd.commonbasiccomponents_1.KeyUsageCodeType;
import ec.schema.xsd.documentwrapper_1.DocumentWrapperType;
import ec.services.wsdl.documentwrapper_2.StoreDocumentWrapperRequest;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.handlers.StoreDocumentWrapperResponseFaultHandler;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.common.converters.node.DocumentReferenceRequestTypeBuilder;
import eu.europa.ec.etrustex.common.converters.node.NodePartyConverter;
import eu.europa.ec.etrustex.common.exceptions.DomibusSubmissionException;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.LargePayloadDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.util.CalendarHelper;
import eu.europa.ec.etrustex.common.util.ConversationIdKey;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IssueDateType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import un.unece.uncefact.data.specification.unqualifieddatatypesschemamodule._2.BinaryObjectType;

import javax.xml.ws.Holder;
import java.io.File;

import static eu.europa.ec.etrustex.common.util.TransformerUtils.serializeObjToXML;

/**
 * This adapter is intended to convert Attachments in the direction from Backend to Node plugin to Domibus request.<br/>
 * From the backend plugin DB entity {@link EtxAttachment}, the request parameters to invoke the eTrustEx Node service - {@link ec.services.wsdl.documentwrapper_2.DocumentWrapperService}:storeDocumentWrapper are formulated and added as payload to Domibus.<br/>
 * AS4 request is formulated and submitted to Domibus for request leg - <b>nodeStoreDocumentWrapperRequest</b>.<br/>
 *
 * @author Arun Raj
 * @version 1.0
 * @since 02-Jun-2017
 */
//TODO:EDELIVERY-6608 - Remove ETX BE Plugin NodeInvocationManager facade & NodeServiceAdapters & refactor code functionally

@Service("etxBackendPluginNodeDocumentWrapperServiceAdapter")
public class NodeDocumentWrapperServiceAdapter extends NodeServicesAdapterBase {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(NodeDocumentWrapperServiceAdapter.class);

    @Autowired
    private StoreDocumentWrapperResponseFaultHandler storeDocumentWrapperResponseFaultHandler;

    /**
     * This operation - creates the input to send the AS4 request leg - <b>nodeSubmitDocumentBundleRequest</b> and submits to Domibus.<br/>
     * The following steps happen:
     * <ol>
     * <li>The request parameters required to invoke the eTrustEx Node service - {@link ec.services.wsdl.documentwrapper_2.DocumentWrapperService}:storeDocumentWrapper consist of {@link HeaderType} and {@link StoreDocumentWrapperRequest}. These are formulated from the {@link EtxAttachment} for {@link eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection}=OUT.</li>
     * <li>The above request parameters are added as payload to Domibus</li>
     * <li>AS4 Request is formed and submitted to Domibus</li>
     * <li>The eTrustEx backend plugin DB is updated with Domibus message id.</li>
     * </ol>
     *
     * @param etxAttachment  message wrapper we want to send
     * @param attachmentFile file attached to the message wrapper
     * @throws ETrustExPluginException thrown in case anything goes wrong
     */
    @Transactional
    public void uploadAttachment(EtxAttachment etxAttachment, File attachmentFile) throws ETrustExPluginException {
        LOG.debug("For Attachment ID: [{}], UUID:[{}], Start of upload attachment to Node plugin.", etxAttachment.getId(), etxAttachment.getAttachmentUuid());
        try {

            EtxMessage bundleOfAttachment = etxAttachment.getMessage();
            EtxParty senderParty = etxAttachment.getMessage().getSender();

            // Prepare WS connection
            // Fill in Authorisation Header
            Holder<HeaderType> authorisationHeader = fillAuthorisationHeader(senderParty.getPartyUuid(), null);

            // Prepare WS request
            StoreDocumentWrapperRequest storeDocumentWrapperRequest = new StoreDocumentWrapperRequest();
            DocumentWrapperType documentWrapperType = new DocumentWrapperType();

            IssueDateType issueDateType = new IssueDateType();
            issueDateType.setValue(CalendarHelper.getJodaDateFromCalendar(bundleOfAttachment.getIssueDateTime()));
            documentWrapperType.setIssueDate(issueDateType);

            documentWrapperType.setSenderParty(NodePartyConverter.buildPartyType(senderParty.getPartyUuid()));

            DocumentReferenceRequestType documentReferenceRequest = DocumentReferenceRequestTypeBuilder.buildDocumentReferenceRequest(etxAttachment.getAttachmentUuid(), etxAttachment.getAttachmentType());
            documentWrapperType.setID(documentReferenceRequest.getID());
            documentWrapperType.setDocumentTypeCode(documentReferenceRequest.getDocumentTypeCode());

            LargeAttachmentType largeAttachmentType = new LargeAttachmentType();
            if (etxAttachment.getSessionKey() != null) {
                EncryptionInformationType encryptionInformation = new EncryptionInformationType();

                BinaryObjectType sessionKey = new BinaryObjectType();
                sessionKey.setValue(etxAttachment.getSessionKey());
                sessionKey.setMimeCode("application/pgp-encrypted");
                encryptionInformation.setSessionKey(sessionKey);

                if (etxAttachment.getX509SubjectName() != null) {
                    CertificateType certificate = new CertificateType();
                    certificate.getX509SubjectName().setValue(etxAttachment.getX509SubjectName());
                    BinaryObjectType x509Certificate = new BinaryObjectType();
                    x509Certificate.setMimeCode("base64");
                    certificate.setX509Certificate(x509Certificate);
                    certificate.setKeyUsageCode(new KeyUsageCodeType());

                    encryptionInformation.setCertificate(certificate);
                }
                largeAttachmentType.setEncryptionInformation(encryptionInformation);
            }

            ResourceInformationReferenceType resourceInformationRef = new ResourceInformationReferenceType();
            resourceInformationRef.setLargeAttachment(largeAttachmentType);
            /* The actual file size is computed and set in the soap request by the node plugin.
               MimeType and DataHandler of the attachment will be set by the Node Plugin
             */
            documentWrapperType.setResourceInformationReference(resourceInformationRef);
            storeDocumentWrapperRequest.setDocumentWrapper(documentWrapperType);

            ETrustExAdapterDTO eTrustExAdapterDTO = populateETrustExAdapterDTOForStoreDocumentWrapper(authorisationHeader, storeDocumentWrapperRequest, senderParty, null, etxAttachment, attachmentFile);
            final String as4MsgId = backendConnectorSubmissionService.submit(eTrustExAdapterDTO);
            LOG.info("For Attachment ID: [{}], Submitted StoreDocumentWrapper request message to Domibus Backend connector.", etxAttachment.getId());
            etxAttachment.setDomibusMessageId(as4MsgId);
        } catch (DomibusSubmissionException dsEx) {
            LOG.error("For Attachment ID: [{}], Exception encountered in Outbox flow while sending document wrapper to Node plugin:", etxAttachment.getId(), dsEx);
            storeDocumentWrapperResponseFaultHandler.process(dsEx.getMessage(), etxAttachment);
        } catch (Exception ex) {
            LOG.error("For Attachment ID: [{}], Exception encountered in Outbox flow while sending document wrapper to Node plugin:", etxAttachment.getId(), ex);
            throw new ETrustExPluginException(ETrustExError.DEFAULT, ex.getMessage(), ex);
        }
    }

    ETrustExAdapterDTO populateETrustExAdapterDTOForStoreDocumentWrapper(Holder<HeaderType> authorisationHeader, StoreDocumentWrapperRequest storeDocumentWrapperRequest, EtxParty sender, EtxParty receiver, EtxAttachment etxAttachment, File attachmentFile) {

        ETrustExAdapterDTO etxAdapterDTO = createETrustExAdapterDTOWithCommonDataForDomibusSubmission(sender, receiver, etxAttachment.getId());
        etxAdapterDTO.setAs4ConversationId(ConversationIdKey.OUTBOX_SEND_ATTACHMENT.getConversationIdKey() + etxAttachment.getId());
        //Collaboration Info
        etxAdapterDTO.setAs4Service(eTrustExBackendPluginProperties.getAs4_Service_DocumentWrapper());
        etxAdapterDTO.setAs4ServiceType(eTrustExBackendPluginProperties.getAs4_Service_DocumentWrapper_ServiceType());
        etxAdapterDTO.setAs4Action(eTrustExBackendPluginProperties.getAs4_Action_StoreDocumentWrapper_Request());

        //populating the payload
        String strETrustExHeaderXML = TransformerUtils.serializeObjToXML(authorisationHeader.value);
        LOG.debug("For Attachment ID: [{}], UUID:[{}], StoreDocumentWrapperRequest:HeaderXML: [{}] ", etxAttachment.getId(), etxAttachment.getAttachmentUuid(), strETrustExHeaderXML);
        PayloadDTO headerPayloadDTO = TransformerUtils.getHeaderPayloadDTO(strETrustExHeaderXML);
        etxAdapterDTO.addAs4Payload(headerPayloadDTO);

        String strStoreDocumentWrapperRequestXML = serializeObjToXML(storeDocumentWrapperRequest);
        LOG.debug("For Attachment ID: [{}], UUID:[{}], StoreDocumentWrapper:RequestXML: [{}]", etxAttachment.getId(), etxAttachment.getAttachmentUuid(), strStoreDocumentWrapperRequestXML);
        PayloadDTO genericPayloadDTO = TransformerUtils.getGenericPayloadDTO(strStoreDocumentWrapperRequestXML);
        etxAdapterDTO.addAs4Payload(genericPayloadDTO);

        //Loading the file content
        LargePayloadDTO wrapperPayloadDTO = TransformerUtils.getLargePayloadDTO(attachmentFile, etxAttachment.getFileName(), etxAttachment.getMimeType());
        etxAdapterDTO.addAs4Payload(wrapperPayloadDTO);

        return etxAdapterDTO;
    }


}

package eu.europa.ec.etrustex.adapter.integration.converters;

import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.integration.model.common.v2.*;
import org.joda.time.DateTime;

import java.util.ArrayList;

/**
 * @author: micleva
 * @date: 6/19/12 1:40 PM
 * @project: ETX
 */
public class BackendMessageBundleTypeConverter {

    private BackendMessageBundleTypeConverter() {
    }

    public static EtxMessageBundleType fromEtxMessage(EtxMessage messageBundle) {
        EtxMessageBundleType messageBundleSdo = new EtxMessageBundleType();

        messageBundleSdo.setId(messageBundle.getMessageUuid());

        if(messageBundle.getMessageParentUuid() != null) {
            EtxMessageReferenceType parentMessageReference = new EtxMessageReferenceType();
            messageBundleSdo.setParentMessageReference(parentMessageReference);
            parentMessageReference.setId(messageBundle.getMessageParentUuid());
        }
        messageBundleSdo.setReferenceId(messageBundle.getMessageReferenceUuid());

        messageBundleSdo.setIssueDateTime(new DateTime(messageBundle.getIssueDateTime()));
        messageBundleSdo.setReceivedDateTime(new DateTime(messageBundle.getCreationDate()));

        messageBundleSdo.setSubject(messageBundle.getSubject());
        messageBundleSdo.setContent(messageBundle.getMessageContent());

        messageBundleSdo.setSender(RecipientTypeConverter.convertEtxParty(messageBundle.getSender()));
        messageBundleSdo.setReceiverList(new RecipientListType());
        messageBundleSdo.getReceiverList().getRecipient().add(RecipientTypeConverter.convertEtxParty(messageBundle.getReceiver()));

        messageBundleSdo.setFileMetadataList(new FileMetadataListType());
        messageBundleSdo.getFileMetadataList().getFile().addAll(FileMetadataTypeConverter.convertEtxAttachmentList(messageBundle.getAttachmentList()));


        if (messageBundle.getSignedData() != null) {
            messageBundleSdo.setSignedData(messageBundle.getSignedData());
        }

        if (messageBundle.getSignature() != null) {
            messageBundleSdo.setSignature(messageBundle.getSignature());
        }

        return messageBundleSdo;
    }

    public static EtxMessage toEtxMessage(EtxMessageBundleType messageBundleType) {
        EtxMessage etxMessageBundle = new EtxMessage();

        etxMessageBundle.setMessageUuid(messageBundleType.getId());

        if (messageBundleType.getParentMessageReference() != null) {
            etxMessageBundle.setMessageParentUuid(messageBundleType.getParentMessageReference().getId());
        }

        etxMessageBundle.setMessageReferenceUuid(messageBundleType.getReferenceId());
        etxMessageBundle.setIssueDateTime(messageBundleType.getIssueDateTime().toGregorianCalendar());

        etxMessageBundle.setSubject(messageBundleType.getSubject());
        etxMessageBundle.setMessageContent(messageBundleType.getContent());

        if (messageBundleType.getFileMetadataList() != null && messageBundleType.getFileMetadataList().getFile() != null) {
            etxMessageBundle.setAttachmentList(new ArrayList<EtxAttachment>());
            for (FileMetadataType fileMetadataType : messageBundleType.getFileMetadataList().getFile()) {
                EtxAttachment etxAttachment = FileMetadataTypeConverter.toEtxAttachment(fileMetadataType);
                etxMessageBundle.getAttachmentList().add(etxAttachment);
            }
        }

        if (messageBundleType.getSignedData() != null) {
            etxMessageBundle.setSignedData(messageBundleType.getSignedData());
        }

        if (messageBundleType.getSignature() != null) {
            etxMessageBundle.setSignature(messageBundleType.getSignature());
        }

        return etxMessageBundle;
    }
}

package eu.europa.ec.etrustex.adapter.integration.converters;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.integration.model.common.v2.RecipientType;

/**
 * @author: micleva
 * @date: 6/19/12 4:18 PM
 * @project: ETX
 */
public class RecipientTypeConverter {

    private RecipientTypeConverter() {
    }

    public static RecipientType convertEtxParty(EtxParty etxParty) {
        return convertEtxParty(etxParty.getPartyUuid());
    }

    public static RecipientType convertEtxParty(String partyUuid) {
        RecipientType partySdo = new RecipientType();

        partySdo.setId(partyUuid);
        partySdo.setName(partyUuid);

        return partySdo;
    }
}

package eu.europa.ec.etrustex.adapter.queue.out.downloadattachment;

import eu.europa.ec.etrustex.adapter.queue.AbstractQueueListener;
import org.springframework.stereotype.Component;

/**
 * @author micleva
 * @project ETX
 */
@Component("etxBackendPluginOutDownloadAttachmentConsumer")
public class OutDownloadAttachmentConsumer extends AbstractQueueListener<OutDownloadAttachmentQueueMessage> {

    private final OutDownloadAttachmentHandler handler;

    public OutDownloadAttachmentConsumer(OutDownloadAttachmentHandler handler) {
        this.handler = handler;
    }

    @Override
    public void handleMessage(OutDownloadAttachmentQueueMessage queueMessage) {
        handler.handleDownloadAttachment(queueMessage.getAttachmentId());
    }

    @Override
    public void validateMessage(OutDownloadAttachmentQueueMessage queueMessage) {
        handler.validateDownloadAttachment(queueMessage.getAttachmentId());
    }
}

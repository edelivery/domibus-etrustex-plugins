package eu.europa.ec.etrustex.adapter.queue.postupload;

import eu.europa.ec.etrustex.common.jms.QueueMessage;

/**
 * @author Arun Raj
 * @version 1.1.1
 * @since 27/06/2019
 */
public class PostUploadQueueMessage extends QueueMessage {

    private final Long messageId;

    public PostUploadQueueMessage(Long messageId) {
        this.messageId = messageId;
    }

    public Long getMessageId() {
        return messageId;
    }

    @Override
    public String toString() {
        return "PostUploadQueueMessage{" +
                "messageId=" + messageId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PostUploadQueueMessage that = (PostUploadQueueMessage) o;

        if (messageId != null ? !messageId.equals(that.messageId) : that.messageId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return messageId != null ? messageId.hashCode() : 0;
    }
}

package eu.europa.ec.etrustex.adapter.integration.handlers;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.queue.inbox.uploadattachment.InboxUploadAttachmentProducer;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.LargePayloadDTO;
import eu.europa.ec.etrustex.common.service.WorkspaceService;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.common.util.Validate;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import static eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty.ETX_BACKEND_DOWNLOAD_ATTACHMENTS;
import static eu.europa.ec.etrustex.adapter.support.Util.buildSystemConfig;
import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_ATT_UUID;
import static eu.europa.ec.etrustex.common.util.Validate.notNull;
import static java.lang.Boolean.valueOf;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

// @formatter:off

/**
 * <b>Backend service to receive transmission of wrapper from Node to Backend.<br/></b>
 * <p>
 * <p>This service is currently designed to handle the following scenarios in Wrapper transmission:<br/>
 * <ul>
 * <ul>
 * <li>
 * The attachment that is transmitted, is downloaded to the workspace folder and DB status is updated
 * in ETX_ADT_ATTACHMENT as {@link AttachmentState#ATTS_DOWNLOADED}. The UUID of the attachment should be included
 * as an AS4 message property.
 * </li>
 * <li>For each attachment, trigger is raised to {@code}InboxUploadAttachment{@code} JMS queue.</li>
 * </ul>
 * </li>
 * <p>
 * <li><b>Backend system configured NOT to download attachments:</b> For the backend system of the receiver party,
 * if the entry {@link SystemConfigurationProperty#ETX_BACKEND_DOWNLOAD_ATTACHMENTS} in ETX_ADT_SYSTEM_CONFIG is set to false,
 * then on receipt of bundle for the first time, all attachments of the bundle will be updated as {@link AttachmentState#ATTS_IGNORED}
 * and trigger will be raised to {@code}PostProcessing{@code} JMS queue. Thus download of attachment will be ignored.</li>
 * </ul>
 * </p>
 *
 * @author François Gautier
 * @version 1.0
 * @since Dec 2017
 */
// @formatter:on
@Service("etxBackendPluginInboxWrapperTransmissionHandler")
public class InboxWrapperTransmissionBackendHandler implements OperationHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxWrapperTransmissionBackendHandler.class);

    @Autowired
    private InboxUploadAttachmentProducer inboxUploadAttachmentProducer;
    @Autowired
    @Qualifier("etxBackendPluginWorkspaceService")
    private WorkspaceService workspaceService;
    @Autowired
    private MessageServicePlugin messageService;

    /**
     * <p>Handles the notification from Domibus on Wrapper transmission reception at backend.</p>
     * The instance of {@link EtxMessage} is obtained first.<br/>
     * Depending on whether attachment processing is required or not, further triggers are raised either for attachment processing<br/>
     *
     * @param etxAdapterDTO Instance of {@link ETrustExAdapterDTO} notified by Domibus to the backend plugin
     * @return true if the handling is a success
     */
    @Override
    public boolean handle(ETrustExAdapterDTO etxAdapterDTO) {
        LOG.info("Handling WrapperTransmission at Backend from SenderParty:[{}] to ReceiverParty:[{}]",
                etxAdapterDTO.getAs4OriginalSender(),
                etxAdapterDTO.getAs4FinalRecipient());
        EtxMessage etxMessage = getEtxMessage(etxAdapterDTO);
        EtxParty etxReceiverParty = etxMessage.getReceiver();

        Map<SystemConfigurationProperty, String> receiverSystemConfig = buildSystemConfig(etxReceiverParty.getSystem().getSystemConfigList());

        //check whether backend system wants to download attachments or not
        if (isTrue(valueOf(receiverSystemConfig.get(ETX_BACKEND_DOWNLOAD_ATTACHMENTS)))) {
            handleAttachmentReceipt(etxAdapterDTO, getEtxAttachment(etxAdapterDTO, etxMessage));
        }

        return true;
    }

    private EtxMessage getEtxMessage(ETrustExAdapterDTO etxAdapterDTO) {
        EtxMessage etxMessage = messageService.findMessageByDomibusMessageId(etxAdapterDTO.getAs4RefToMessageId());
        notNull(etxMessage, "EtxMessage not found for domibusMessageId = " + etxAdapterDTO.getAs4RefToMessageId());
        return etxMessage;
    }

    private EtxAttachment getEtxAttachment(ETrustExAdapterDTO etxAdapterDTO, EtxMessage etxMessage) {
        return getAttachment(etxAdapterDTO.getAttachmentId(), etxMessage);
    }

    /**
     * <p>Handles reception of Attachments at the backend plugin.</p>
     * If an attachment is transmitted in the payload:
     * <ol>
     * <li>The Wrapper UUID is expected in the AS4 message properties and the mime type is obtained from the Payload.</li>
     * <li>The {@link EtxAttachment} instance is identified from {@link EtxMessage} using Wrapper UUID.</li>
     * <li>The attachment is written in the {@code}workspace{@code} folder.</li>
     * <li>The {@link EtxAttachment} instance for the wrapper is updated with the mime type, Domibus Message Id and status is set to {@link AttachmentState#ATTS_DOWNLOADED}.</li>
     * <li>Trigger is raised to {@code}InboxUploadAttachment{@code} JMS queue.</li>
     * </ol>
     *
     * @param etxAdapterDTO Instance of {@link ETrustExAdapterDTO} notified by Domibus to the backend plugin
     * @param etxAttachment Instance of {@link EtxAttachment} to be handled (write file, change status, etc.)
     */
    private void handleAttachmentReceipt(ETrustExAdapterDTO etxAdapterDTO, EtxAttachment etxAttachment) {

        LargePayloadDTO largePayloadDTO = (LargePayloadDTO) etxAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_DOCUMENT);
        Validate.notNull(largePayloadDTO, "In Wrapper transmission from Node to Backend, the file for Wrapper with UUID:[" + etxAttachment.getAttachmentUuid() + "] was not transmitted in Domibus message with Id:[" + etxAdapterDTO.getAs4MessageId() + "]");

        Validate.notBlank(etxAdapterDTO.getAttachmentId(), "");
        //Domibus has an internal payload cleaning mechanism. On the receiver side, the payload files will be cleared after the retention period when the RetentionWorker sets the message as Deleted.
        // Hence the Backend plugin should write the attachment file to workspace folder separately. Clearing of this file will be governed by the plugin maintainence manager.
        try (InputStream is = largePayloadDTO.getPayloadDataHandler().getInputStream()){
            workspaceService.writeFile(etxAttachment.getId(), is);
        } catch (IOException e) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "IOException occurred in Wrapper transmission from Node to Backend, while writing attachment with UUID:[" + etxAttachment.getAttachmentUuid() + "] onto workspace folder!");
        }
        etxAttachment.setDomibusMessageId(etxAdapterDTO.getAs4MessageId());
        etxAttachment.setStateType(AttachmentState.ATTS_DOWNLOADED);
        etxAttachment.setMimeType(largePayloadDTO.getMimeType());

        inboxUploadAttachmentProducer.triggerUpload(etxAttachment.getId());
    }

    /**
     * To identify an instance of {@link EtxAttachment} matching a Wrapper UUID from an {@link EtxMessage} instance.
     *
     * @param strAttachmentUUID Wrapper UUID to search for
     * @param etxMessage        Instance of {@link EtxMessage} from which Attachment is to be searched
     * @return Instance of {@link EtxAttachment} if found else null
     */
    static EtxAttachment getAttachment(String strAttachmentUUID, EtxMessage etxMessage) {
        //find Corresponding EtxAttachment entity
        if (CollectionUtils.isNotEmpty(etxMessage.getAttachmentList())) {
            for (EtxAttachment etxAttachment : etxMessage.getAttachmentList()) {
                if (StringUtils.equals(strAttachmentUUID, etxAttachment.getAttachmentUuid())) {
                    LOG.putMDC(MDC_ETX_ATT_UUID, etxAttachment.getAttachmentUuid());
                    return etxAttachment;
                }
            }
        }
        throw new ETrustExPluginException(ETrustExError.DEFAULT,
                "In Wrapper transmission from Node to Backend, Wrapper with UUID:[" + strAttachmentUUID + "] is not found linked to Bundle with UUID:[" + etxMessage.getMessageUuid() + "]");
    }
}

package eu.europa.ec.etrustex.adapter.integration.handlers;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.exceptions.EtrustExMarker;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import org.springframework.stereotype.Service;

/**
 * Handle Error on the node side after sending a messageStatus and messageBundle
 *
 * @author François GAUTIER
 * @version 1.0
 * @since 22-09-2017
 */
@Service("etxBackendPluginRetrieveICAResponseFaultHandler")
public class RetrieveICAResponseFaultHandler extends ResponseFaultHandlerBase {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(RetrieveICAResponseFaultHandler.class);

    @Override
    protected void process(String payload, ETrustExAdapterDTO eTrustExAdapterResponseDTO) {
        LOG.error(EtrustExMarker.NON_RECOVERABLE, "Retrieve ICA failed {}, uuid: {}", payload, getUuid(eTrustExAdapterResponseDTO));
    }

    protected String getUuid(ETrustExAdapterDTO eTrustExAdapterResponseDTO) {
        return eTrustExAdapterResponseDTO.getAs4ConversationId();
    }
}

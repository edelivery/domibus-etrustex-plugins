package eu.europa.ec.etrustex.adapter.model.entity.common;

/**
 * @author micleva
 * @project ETX
 */

/**
 * Node response codes.
 */
public enum StatusResponseCode {
    // General Integration error types
    BDL1("BDL:1", "The bundle is available in the receiver's inbox"),
    BDL4("BDL:4", "Hard business rule violated"),
    BDL5("BDL:5", "Parent Message Bundle ID does not exist"),
    BDL6("BDL:6", "Missing File"),
    BDL7("BDL:7", "Status READ"),
    STS4("301:4", "Hard business rule violated"),
    STS5("301:5", "Parent document ID does not exist"),
    STS6("301:6", "Parent document is not in the expected state");

    private final String code;
    private final String description;

    StatusResponseCode(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public static StatusResponseCode fromCode(String code) {
        for (StatusResponseCode responseCode : values()) {
            if (responseCode.getCode().equals(code)) {
                return responseCode;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}

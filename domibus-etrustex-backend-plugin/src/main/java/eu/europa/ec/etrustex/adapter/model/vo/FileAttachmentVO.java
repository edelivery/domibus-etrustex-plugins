package eu.europa.ec.etrustex.adapter.model.vo;

import javax.activation.DataHandler;

/**
 * @author: micleva
 * @date: 9/12/13 11:15 AM
 * @project: ETX
 */
public class FileAttachmentVO {

    private byte[] sessionKey;
    private String X509SubjectName;
    private DataHandler dataHandler;

    public byte[] getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(byte[] sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getX509SubjectName() {
        return X509SubjectName;
    }

    public void setX509SubjectName(String x509SubjectName) {
        X509SubjectName = x509SubjectName;
    }

    public DataHandler getDataHandler() {
        return dataHandler;
    }

    public void setDataHandler(DataHandler dataHandler) {
        this.dataHandler = dataHandler;
    }
}

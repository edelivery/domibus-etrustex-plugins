package eu.europa.ec.etrustex.adapter.service;

import eu.europa.ec.etrustex.adapter.model.entity.administration.AdapterConfigurationProperty;

import java.util.Map;

/**
 * @author micleva
 * @project ETX
 */

public interface AdapterConfigurationService {

    Map<AdapterConfigurationProperty, String> getAdapterConfigurations();
}

package eu.europa.ec.etrustex.adapter.support;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystemConfig;
import eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty;

import java.io.Closeable;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author micleva
 * @project ETX
 */

public class Util {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(Util.class);

    private Util() {
        //do not build
    }

    public static void safeClose(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                LOG.warn("Failed to close stream", e);
            }
        }
    }

    public static Map<SystemConfigurationProperty, String> buildSystemConfig(List<EtxSystemConfig> etxSystemConfigs) {

        Map<SystemConfigurationProperty, String> properties = new HashMap<>();
        for (EtxSystemConfig systemConfigEntity : etxSystemConfigs) {
            properties.put(systemConfigEntity.getSystemConfigurationProperty(), systemConfigEntity.getPropertyValue());
        }

        return properties;
    }
}

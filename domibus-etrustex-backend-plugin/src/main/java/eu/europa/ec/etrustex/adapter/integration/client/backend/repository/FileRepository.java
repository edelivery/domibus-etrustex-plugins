package eu.europa.ec.etrustex.adapter.integration.client.backend.repository;

import eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.vo.FileAttachmentVO;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

/**
 * @author: micleva
 * @date: 9/12/13 9:07 AM
 * @project: ETX
 */
public interface FileRepository {

    FileAttachmentVO readFile(Map<SystemConfigurationProperty, String> systemConfig, EtxAttachment etxAttachment) throws IOException;

    void writeFile(Map<SystemConfigurationProperty, String> systemConfig, EtxAttachment etxAttachment, Path attachmentFile) throws IOException;

    boolean deleteDirectory(EtxMessage message);

    void releaseResources(FileAttachmentVO faVo) throws IOException;

}

package eu.europa.ec.etrustex.adapter.queue.postprocess;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.client.backend.repository.FileRepository;
import eu.europa.ec.etrustex.adapter.integration.client.backend.repository.FileRepositoryProvider;
import eu.europa.ec.etrustex.adapter.model.entity.administration.BackendFileRepositoryLocationType;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxError;
import eu.europa.ec.etrustex.adapter.model.entity.message.*;
import eu.europa.ec.etrustex.adapter.queue.notify.NotifyProducer;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.adapter.support.Util;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author micleva
 * @project ETX
 */

@Service("etxBackendPluginPostProcessHandler")
public class PostProcessHandlerImpl implements PostProcessHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PostProcessHandlerImpl.class);

    @Autowired
    private MessageServicePlugin messageService;

    @Autowired
    private FileRepositoryProvider fileRepositoryProvider;

    @Autowired
    private NotifyProducer notifyProducer;

    @Autowired
    private AttachmentService attachmentService;

    @Override
    public void handlePostProcess(Long messageId) {
        EtxMessage messageEntity = messageService.findById(messageId);
        LOG.info("PostProcessConsumer.handlePostProcess for messageId: [{} ID:{}] START", messageEntity.getMessageType(), messageId);

        boolean inFinalState = messageEntity.getMessageState().isFinal();
        if (!inFinalState) {
            if (messageEntity.getMessageType() == MessageType.MESSAGE_BUNDLE) {
                if (attachmentService.areAllAttachmentsInFinalState(messageEntity)) {
                    messageEntity.setMessageState(getFinalMessageState(messageEntity));
                    deleteBundleDirectoryIfUploaded(messageEntity);
                    inFinalState = true;
                }
            } else {
                messageEntity.setMessageState(getFinalMessageState(messageEntity));
                inFinalState = true;
            }
        }
        if (inFinalState) {
            EtxMessage notificationMessage = initNotificationMessage(messageEntity);
            if (notificationMessage != null && notificationMessage.getNotificationState() == NotificationState.NF_AUTO_PENDING) {
                notifyProducer.triggerNotify(notificationMessage.getId());
            }
        }
        LOG.info("PostProcessConsumer.handlePostProcess for messageId: [{} ID:{}] END. In final state: {}", messageEntity.getMessageType(), messageId, inFinalState);
    }

    private MessageState getFinalMessageState(EtxMessage message) {
        boolean hasError = message.getError() != null;
        boolean hasFailedAttachment = message.getMessageType() == MessageType.MESSAGE_BUNDLE && attachmentService.hasFailedAttachment(message);
        if (hasError || hasFailedAttachment) {
            return MessageState.MS_FAILED;
        } else {
            return MessageState.MS_PROCESSED;
        }
    }

    private EtxMessage initNotificationMessage(EtxMessage originalMessage) {
        EtxMessage notificationMessage = null;
        if (originalMessage.getMessageState() == MessageState.MS_FAILED) {
            notificationMessage = messageService.createMessageStatusFor(originalMessage, MessageReferenceState.FAILED);
        } else if (originalMessage.getMessageState() == MessageState.MS_PROCESSED && originalMessage.getDirectionType() == MessageDirection.IN) {
            notificationMessage = originalMessage;
        }

        if (notificationMessage != null && notificationMessage.getMessageState() == MessageState.MS_PROCESSED) {
            NotificationState initialNotificationState = getInitialNotificationState(notificationMessage.getReceiver(), notificationMessage.getDirectionType());
            notificationMessage.setNotificationState(initialNotificationState);
        }

        return notificationMessage;
    }

    private NotificationState getInitialNotificationState(EtxParty receiverParty, MessageDirection direction) {
        if (direction == MessageDirection.OUT || receiverParty.getSystem().requiresAutoNotification()) {
            return NotificationState.NF_AUTO_PENDING;
        } else {
            return NotificationState.NF_PENDING;
        }
    }

    private void deleteBundleDirectoryIfUploaded(EtxMessage messageEntity) {
        if (messageEntity.getDirectionType() == MessageDirection.OUT
                && messageEntity.getMessageState() == MessageState.MS_PROCESSED) {
            EtxSystem senderSystem = messageEntity.getSender().getSystem();
            Map<SystemConfigurationProperty, String> senderSystemConfig = Util.buildSystemConfig(senderSystem.getSystemConfigList());
            BackendFileRepositoryLocationType backendFileRepositoryLocation = BackendFileRepositoryLocationType.valueOf(
                    senderSystemConfig.get(SystemConfigurationProperty.ETX_BACKEND_FILE_REPOSITORY_LOCATION)
            );

            FileRepository fileRepository = fileRepositoryProvider.getFileRepository(backendFileRepositoryLocation);
            fileRepository.deleteDirectory(messageEntity);
        }
    }

    @Override
    public void validatePostProcess(Long messageId, Long attachmentId) {
        EtxMessage messageEntity = messageService.findById(messageId);

        if (messageEntity == null) {
            String errorDetails = String.format("Message [ID: %s] not found", messageId);
            LOG.warn(errorDetails);
            throw new ETrustExPluginException(ETrustExError.DEFAULT, errorDetails, null);
        }

        //Condition was added to protect PostProcess phase from JTA-related inconsistency
        //(DB changes are still not visible even after corresponding JMS Message is consumed)
        if (attachmentId != null) {
            EtxAttachment attachmentEntity = attachmentService.findById(attachmentId);
            if (!attachmentEntity.getStateType().isFinal()) {
                String errorDetails = String.format("Attachment [ID: %s] is not in final state", attachmentId);
                LOG.warn(errorDetails);
                throw new ETrustExPluginException(ETrustExError.DEFAULT, errorDetails, null);
            }
        }
    }

    @Override
    public void onError(Long messageId) {
        EtxMessage messageEntity = messageService.findById(messageId);
        if (messageEntity.getError() == null) {
            messageEntity.setError(new EtxError(ETrustExError.DEFAULT.getCode(), "Failed to post process message " + messageEntity.getMessageUuid()));
        } else {
            LOG.warn("Failed to post process message [ID: {}], message already has an error", messageId);
        }
        messageEntity.setMessageState(MessageState.MS_FAILED);
    }
}

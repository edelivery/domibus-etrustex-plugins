/**
 *
 */
package eu.europa.ec.etrustex.adapter.model.entity.attachment;

/**
 * @author apladap
 */
public enum AttachmentState {
    ATTS_FAILED(true),
    ATTS_CREATED(false),
    ATTS_DOWNLOADED(false),
    ATTS_UPLOADED(true),
    ATTS_ALREADY_UPLOADED(true),
    ATTS_IGNORED(true);

    private boolean isFinal;

    AttachmentState(boolean isFinal) {
        this.isFinal = isFinal;
    }

    public boolean isFinal() {
        return isFinal;
    }
}

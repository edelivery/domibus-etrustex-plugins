package eu.europa.ec.etrustex.adapter.queue.inbox.uploadattachment;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.client.backend.repository.FileRepository;
import eu.europa.ec.etrustex.adapter.integration.client.backend.repository.FileRepositoryProvider;
import eu.europa.ec.etrustex.adapter.model.entity.administration.BackendFileRepositoryLocationType;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxError;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.queue.postupload.PostUploadProducer;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import eu.europa.ec.etrustex.adapter.support.Util;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.service.WorkspaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;


/**
 * Service to handle upload of attachments to backend
 *
 * @author Arun Raj
 * @version 1.0
 * @since Oct 2017
 */

@Service("etxBackendPluginInboxUploadAttachmentHandler")
public class InboxUploadAttachmentHandlerImpl implements InboxUploadAttachmentHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxUploadAttachmentHandlerImpl.class);

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private FileRepositoryProvider fileRepositoryProvider;

    @Autowired
    private PostProcessProducer postProcessProducer;

    @Autowired
    private PostUploadProducer postUploadProducer;

    @Autowired
    @Qualifier("etxBackendPluginWorkspaceService")
    private WorkspaceService workspaceService;

    /**
     * {@inheritDoc}
     */
    @Override
    public void handleUploadAttachment(Long attachmentId) {
        EtxAttachment attachmentEntity = attachmentService.findById(attachmentId);

        EtxMessage messageBundleEntity = attachmentEntity.getMessage();
        EtxParty receiverEntity = messageBundleEntity.getReceiver();

        EtxSystem receiverSystem = receiverEntity.getSystem();

        //read from WKS
        Path attachmentFile = workspaceService.getFile(attachmentId);

        Map<SystemConfigurationProperty, String> receiverSystemConfig = Util.buildSystemConfig(receiverSystem.getSystemConfigList());
        BackendFileRepositoryLocationType backendFileRepositoryLocation = BackendFileRepositoryLocationType.valueOf(
                receiverSystemConfig.get(SystemConfigurationProperty.ETX_BACKEND_FILE_REPOSITORY_LOCATION)
        );

        FileRepository fileRepository = fileRepositoryProvider.getFileRepository(backendFileRepositoryLocation);
        try {
            fileRepository.writeFile(receiverSystemConfig, attachmentEntity, attachmentFile);
        } catch (IOException ex) {
            LOG.error("Failed write file: " + attachmentFile.toString(), ex);
            throw new ETrustExPluginException(ETrustExError.DEFAULT, ex.getMessage(), ex);
        }

        attachmentService.updateAttachmentState(attachmentEntity, AttachmentState.ATTS_DOWNLOADED, AttachmentState.ATTS_UPLOADED);

        LOG.info("Attachment with ID: [{}}] successfully uploaded.", attachmentId);

        postUploadProducer.checkAttachmentsAndTriggerPostUpload(attachmentEntity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateUploadAttachment(Long attachmentId) {
        EtxAttachment attachmentEntity = attachmentService.findById(attachmentId);

        if (attachmentEntity == null) {
            String errorDetails = "Attachment [ID: " + attachmentId + "] not found";
            LOG.warn(errorDetails);
            throw new ETrustExPluginException(ETrustExError.DEFAULT, errorDetails);
        }

        EtxMessage messageBundleEntity = attachmentEntity.getMessage();

        boolean isExpectedAttachmentState = attachmentEntity.getStateType() == AttachmentState.ATTS_DOWNLOADED;
        boolean isExpectedMessageState = messageBundleEntity.getMessageState() == MessageState.MS_CREATED;

        if (!isExpectedMessageState || !isExpectedAttachmentState) {

            StringBuilder errorDetails = new StringBuilder();
            errorDetails
                    .append("Invalid message and attachment states (expected message state: MS_CREATED, expected attachment state: ATTS_DOWNLOADED), skip downloading attachment (from Node) ID: ");
            errorDetails.append(attachmentId);
            errorDetails.append(", state: ");
            errorDetails.append(attachmentEntity.getStateType());
            errorDetails.append(" regarding message bundle with ID: ");
            errorDetails.append(messageBundleEntity.getId());
            errorDetails.append(", state: ");
            errorDetails.append(messageBundleEntity.getMessageState());

            if (LOG.isWarnEnabled()) {
                LOG.warn(errorDetails.toString());
            }
            throw new ETrustExPluginException(ETrustExError.ETX_009, errorDetails.toString(), null);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onError(Long attachmentId) {
        EtxAttachment attachmentEntity = attachmentService.findById(attachmentId);
        attachmentEntity.setStateType(AttachmentState.ATTS_FAILED);
        attachmentEntity.setError(new EtxError(ETrustExError.DEFAULT.getCode(), "Failed to upload attachment " + attachmentEntity.getAttachmentUuid()));

        postProcessProducer.triggerPostProcess(attachmentEntity.getMessage().getId(), attachmentId);
    }
}

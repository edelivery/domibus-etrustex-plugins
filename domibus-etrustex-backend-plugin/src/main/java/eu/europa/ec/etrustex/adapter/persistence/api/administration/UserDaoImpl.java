package eu.europa.ec.etrustex.adapter.persistence.api.administration;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxUser;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBaseImpl;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author JUAN TENA Created on Oct 19, 2011
 * @project ETX
 *
 * @author Arun Raj, Catalin Enache
 * @version 1.0
 * @since 08-August-2017
 */
@Repository("etxBackendPluginUserDaoImpl")
public class UserDaoImpl extends DaoBaseImpl<EtxUser> implements UserDao {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(UserDaoImpl.class);

    public UserDaoImpl() {
        super(EtxUser.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EtxUser findUserByName(String userName) {
        TypedQuery<EtxUser> query = getEntityManager().createNamedQuery("findUserByName", EtxUser.class);
        query.setParameter("userName", userName);
        List<EtxUser> resultList = query.getResultList();
        if (resultList.isEmpty()) {
            LOG.warn("No EtxSystem found for UserByName {}", userName);
            return null;
        }
        return resultList.get(0);
    }
}
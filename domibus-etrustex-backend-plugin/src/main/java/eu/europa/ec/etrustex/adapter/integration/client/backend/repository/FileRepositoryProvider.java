package eu.europa.ec.etrustex.adapter.integration.client.backend.repository;

import eu.europa.ec.etrustex.adapter.model.entity.administration.BackendFileRepositoryLocationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: micleva
 * @date: 9/12/13 12:02 PM
 * @project: ETX
 */
@Service("etxBackendPluginFileRepositoryProvider")
public class FileRepositoryProvider {

    @Autowired
    private LocalSharedFileRepositoryService localSharedFileRepository;

    @Autowired
    private WebServiceFileRepositoryService webServiceFileRepositoryService;

    public FileRepository getFileRepository(BackendFileRepositoryLocationType backendFileRepositoryLocation) {

        FileRepository fileRepository;

        switch (backendFileRepositoryLocation) {
            case BACKEND_FILE_REPOSITORY_SERVICE:
                fileRepository = webServiceFileRepositoryService;
                break;
            case LOCAL_SHARED_FILE_SYSTEM:
                fileRepository = localSharedFileRepository;
                break;
            default:
                throw new IllegalArgumentException("Unsupported backend file repository location detected: " + backendFileRepositoryLocation);
        }

        return fileRepository;
    }
}

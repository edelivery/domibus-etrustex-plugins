package eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter;

import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.services.wsdl.applicationresponse_2.ApplicationResponsePortType;
import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseRequest;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.converters.NodeApplicationResponseTypeConverter;
import eu.europa.ec.etrustex.adapter.integration.handlers.ResponseFaultHandler;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.common.exceptions.DomibusSubmissionException;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.util.ConversationIdKey;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.ws.Holder;

import static eu.europa.ec.etrustex.common.util.TransformerUtils.serializeObjToXML;

/**
 * This adapter is intended to convert messages of {@link MessageType#MESSAGE_STATUS} in the direction from Backend to Node plugin to Domibus request.<br/>
 * From the backend plugin DB entity {@link EtxMessage}, the request parameters to invoke the eTrustEx Node service - {@link ApplicationResponsePortType#submitApplicationResponse(SubmitApplicationResponseRequest, Holder)} are formulated and added as payload to Domibus.<br/>
 * AS4 request is formulated and submitted to Domibus for request leg - <b>nodeSubmitApplicationResponseRequest</b>.<br/>
 *
 * @author Arun Raj
 * @version 1.0
 * @since 15-Mar-2017
 */
//TODO:EDELIVERY-6608 - Remove ETX BE Plugin NodeInvocationManager facade & NodeServiceAdapters & refactor code functionally

@Service("etxBackendPluginNodeApplicationResponseServicesAdapter")
public class NodeApplicationResponseServicesAdapter extends NodeServicesAdapterBase {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(NodeApplicationResponseServicesAdapter.class);

    @Autowired
    private ResponseFaultHandler responseFaultHandler;

    /**
     * This operation - creates the input to send the AS4 request leg - <b>nodeSubmitApplicationResponseRequest</b> and submits to Domibus.<br/>
     * The following steps happen:
     * <ol>
     * <li>The request parameters required to invoke the eTrustEx Node service - {@link ec.services.wsdl.applicationresponse_2.ApplicationResponseService}:submitApplicationResponse consist of {@link HeaderType} and {@link SubmitApplicationResponseRequest}. These are formulated from the EtxMessage for {@link eu.europa.ec.etrustex.adapter.model.entity.message.MessageType}=MESSAGE_STATUS and {@link eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection}=OUT.</li>
     * <li>The above request parameters are added as payload to Domibus</li>
     * <li>AS4 Request is formed and submitted to Domibus</li>
     * <li>The eTrustEx backend plugin DB is updated with Domibus message id.</li>
     * </ol>
     *
     * @param statusMessage messageStatus we want to send
     * @throws ETrustExPluginException exception thrown if anything goes wrong
     */
    @Transactional
    public void sendMessageStatus(EtxMessage statusMessage) throws ETrustExPluginException {
        LOG.debug("For MESSAGE_STATUS with ID: [{}]], start of sendApplicationResponse to Node plugin.", statusMessage.getId());
        try {
            // Fill header & request
            Holder<HeaderType> authorisationHeaderHolder = fillAuthorisationHeaderWithStatusScope(statusMessage.getSender().getPartyUuid(), statusMessage.getReceiver().getPartyUuid());
            SubmitApplicationResponseRequest request = new SubmitApplicationResponseRequest();
            request.setApplicationResponse(NodeApplicationResponseTypeConverter.convertEtxMessage(statusMessage));

            ETrustExAdapterDTO eTrustExAdapterDTO = populateETrustExAdapterDTOForSubmitApplicationResponse(authorisationHeaderHolder, request, statusMessage);
            final String as4MsgId = backendConnectorSubmissionService.submit(eTrustExAdapterDTO);
            LOG.info("For MESSAGE_STATUS with ID: [{}], Submitted sendMessageStatus request to Domibus Backend connector.", statusMessage.getId());
            statusMessage.setDomibusMessageId(as4MsgId);
        } catch (DomibusSubmissionException dsEx) {
            LOG.error("For MESSAGE_STATUS with ID: [{}], Exception encountered in Outbox flow while sending Application response to Node plugin:", statusMessage.getId(), dsEx);
            responseFaultHandler.process(dsEx.getMessage(), statusMessage);
        } catch (Exception ex) {
            LOG.error("For MESSAGE_STATUS with ID: [{}], Exception encountered in Outbox flow while sending Application response to Node plugin:", statusMessage.getId(), ex);
            throw new ETrustExPluginException(ETrustExError.DEFAULT, ex.getMessage(), ex);
        }
    }

    ETrustExAdapterDTO populateETrustExAdapterDTOForSubmitApplicationResponse(Holder<HeaderType> authorisationHeaderHolder, SubmitApplicationResponseRequest request, EtxMessage statusMessage) {
        ETrustExAdapterDTO etxAdapterDTO = createETrustExAdapterDTOWithCommonDataForDomibusSubmission(statusMessage.getSender(), statusMessage.getReceiver(), statusMessage.getId());
        etxAdapterDTO.setAs4ConversationId(ConversationIdKey.OUTBOX_SEND_MESSAGE_STATUS.getConversationIdKey() + statusMessage.getId());

        //Collaboration Info
        etxAdapterDTO.setAs4Service(eTrustExBackendPluginProperties.getAs4_Service_ApplicationResponse());
        etxAdapterDTO.setAs4ServiceType(eTrustExBackendPluginProperties.getAs4_Service_ApplicationResponse_ServiceType());
        etxAdapterDTO.setAs4Action(eTrustExBackendPluginProperties.getAs4_Action_SubmitApplicationResponse_Request());

        //populating the payload
        String strETrustExHeaderXML = TransformerUtils.serializeObjToXML(authorisationHeaderHolder.value);
        LOG.debug("For MESSAGE_STATUS with ID: [{}], Header Payload XML:[{}]", statusMessage.getId(), strETrustExHeaderXML);
        PayloadDTO headerPayloadDTO = TransformerUtils.getHeaderPayloadDTO(strETrustExHeaderXML);
        etxAdapterDTO.addAs4Payload(headerPayloadDTO);

        String submitApplicationResponseRequestXML = serializeObjToXML(request);
        LOG.debug("For MESSAGE_STATUS with ID: [{}], Generic Payload XML: [{}]", statusMessage.getId(), submitApplicationResponseRequestXML);
        PayloadDTO submitApplicationResponseRequestDTO = TransformerUtils.getGenericPayloadDTO(submitApplicationResponseRequestXML);
        etxAdapterDTO.addAs4Payload(submitApplicationResponseRequestDTO);

        return etxAdapterDTO;
    }
}

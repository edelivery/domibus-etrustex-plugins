package eu.europa.ec.etrustex.adapter.service;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;

/**
 * Interface to reload ICAs information
 *
 * @author Arun Raj
 * @version 1.0
 * @since 18/07/2017
 */
public interface EtxPartyService {

    EtxParty create(EtxParty etxParty);

}

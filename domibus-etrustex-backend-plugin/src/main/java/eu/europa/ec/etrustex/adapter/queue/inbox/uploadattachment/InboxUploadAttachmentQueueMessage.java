package eu.europa.ec.etrustex.adapter.queue.inbox.uploadattachment;

import eu.europa.ec.etrustex.common.jms.QueueMessage;

/**
 * Queue message for Inbox Upload Attachment queue at backend plugin
 */
public class InboxUploadAttachmentQueueMessage extends QueueMessage {

    private static final long serialVersionUID = -8697050881444397680L;

    private Long attachmentId;


    public Long getAttachmentId() {
        return this.attachmentId;
    }

    public void setAttachmentId(Long attachmentId) {
        this.attachmentId = attachmentId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("InboxUploadAttachmentQueueMessage");
        sb.append("{attachmentId=").append(attachmentId);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InboxUploadAttachmentQueueMessage that = (InboxUploadAttachmentQueueMessage) o;

        if (attachmentId != null ? !attachmentId.equals(that.attachmentId) : that.attachmentId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return attachmentId != null ? attachmentId.hashCode() : 0;
    }
}

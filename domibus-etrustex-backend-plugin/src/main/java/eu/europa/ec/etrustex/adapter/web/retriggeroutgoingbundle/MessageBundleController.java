package eu.europa.ec.etrustex.adapter.web.retriggeroutgoingbundle;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.vo.MessageReferenceVO;
import eu.europa.ec.etrustex.adapter.service.MessageBundleService;
import eu.europa.ec.etrustex.adapter.service.dto.RetriggerMessageBundleDTO;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Arun Raj
 * @since 1.3.4
 */
@RestController
public class MessageBundleController {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(MessageBundleController.class);
    private final MessageBundleService messageBundleService;

    public MessageBundleController(MessageBundleService messageBundleService) {
        this.messageBundleService = messageBundleService;
    }

    /**
     * Service to enable support personnel to retrigger failed or stuck outgoing bundle transmissions.
     * Limitations:<br/>
     * 1. Only one user should submit a request at a time.<br/>
     * 2. Wait 10 minutes before you retrigger either the same message bundle or the impacted message bundles.<br/>
     * <p>
     * If these restrictions are not followed, messages that are already triggered could fail due to incorrect states in between transmission.
     *
     * @param retriggerBundleRequestList
     * @param response
     * @return
     */
    @PostMapping(value = "/ext/etxBackendPlugin/retriggerOutgoingBundles", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RetriggerBundleResponse> retriggerOutgoingMessageBundle(@RequestBody List<RetriggerBundleRequest> retriggerBundleRequestList, HttpServletResponse response) {
        LOG.info("Received retrigger outgoing message bundle request with list: [{}]", retriggerBundleRequestList);
        RetriggerBundleResponse retriggerBundleResponse = new RetriggerBundleResponse();
        if (CollectionUtils.isEmpty(retriggerBundleRequestList)) {
            retriggerBundleResponse.addRetriggerBundleSuccessList("Received retrigger bundle request with no data");
            return ResponseEntity.status(HttpServletResponse.SC_NO_CONTENT).body(retriggerBundleResponse);
        }

        RetriggerMessageBundleDTO reTriggerMessageBundleDTO = messageBundleService.reTriggerOutgoingBundle(retriggerBundleRequestList);
        retriggerBundleResponse.setRetriggerBundleSuccessList(reTriggerMessageBundleDTO.getRetriggerBundleSuccessResponseList());
        loadOtherImpactedBundlesList(retriggerBundleResponse, reTriggerMessageBundleDTO);
        loadExceptionsList(retriggerBundleResponse, reTriggerMessageBundleDTO);

        return ResponseEntity.status(HttpServletResponse.SC_ACCEPTED).body(retriggerBundleResponse);
    }

    private void loadOtherImpactedBundlesList(RetriggerBundleResponse retriggerBundleResponse, RetriggerMessageBundleDTO reTriggerMessageBundleDTO) {
        if(CollectionUtils.isEmpty(reTriggerMessageBundleDTO.getOtherImpactedBundlesVOList())){
            return;
        }
        for (MessageReferenceVO impactedMessageBundle : reTriggerMessageBundleDTO.getOtherImpactedBundlesVOList()) {
            StringBuilder impactedMessageBundleStringBuilder = new StringBuilder();
            impactedMessageBundleStringBuilder.append("Outgoing Message Bundle UUID :[").append(impactedMessageBundle.getMessageUuid()).append("]")
                                              .append(" with message ref uuid:[").append(impactedMessageBundle.getMessageRefUUID()).append("]")
                                              .append(" between sender party:[").append(impactedMessageBundle.getSenderUuid()).append("]")
                                              .append(" and receiver party:[").append(impactedMessageBundle.getReceiverUuid()).append("]")
                                              .append(" and message state:[").append(impactedMessageBundle.getMessageState()).append("]")
                                              .append(" is impacted additionally.");
            retriggerBundleResponse.addOtherImpactedBundlesList(impactedMessageBundleStringBuilder.toString());
        }
    }


    private void loadExceptionsList(RetriggerBundleResponse retriggerBundleResponse, RetriggerMessageBundleDTO reTriggerMessageBundleDTO) {
        if (retriggerBundleResponse == null || reTriggerMessageBundleDTO == null || CollectionUtils.isEmpty(reTriggerMessageBundleDTO.getRetriggerBundleExceptionsList())) {
            return;
        }
        Map<String, List<Exception>> retriggerBundleExceptionsList = reTriggerMessageBundleDTO.getRetriggerBundleExceptionsList();
        List<String> retriggerOutgoingMessageBundleErrors = new ArrayList<>();
        for (String bundleUUIDinError : retriggerBundleExceptionsList.keySet()) {
            retriggerOutgoingMessageBundleErrors.add("Faced error in Retrigger of Outgoing Message Bundle UUID:[" + bundleUUIDinError + "]. Errors are:");
            for (Exception exception : retriggerBundleExceptionsList.get(bundleUUIDinError)) {
                retriggerOutgoingMessageBundleErrors.add("    " + exception.getMessage());
            }
        }
        retriggerBundleResponse.setRetriggerBundleExceptionList(retriggerOutgoingMessageBundleErrors);
    }

    /**
     * List {@link eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage} that are either in state {@link eu.europa.ec.etrustex.adapter.model.entity.message.MessageState#MS_FAILED}
     * or is pending processing completion  for more than a minimum age.
     *
     * @param startDate [OPTIONAL] delimiting the query with format [yyyy-MM-dd'T'HH:mm:ss.SSSZ]
     * @param endDate [OPTIONAL] delimiting the query [yyyy-MM-dd'T'HH:mm:ss.SSSZ]
     * @param limit [OPTIONAL] max result
     * @return {@link List<MessageReferenceVO>}
     */
    @GetMapping(value = "/ext/etxBackendPlugin/listPendingAndFailedOutgoingMessageBundles")
    public ResponseEntity<List<RetriggerBundleRequest>> listPendingAndFailedOutgoingMessageBundles(
            @RequestParam(name = "startDate", required = false)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date startDate,
            @RequestParam(name = "endDate", required = false)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date endDate,
            @RequestParam(name = "limit", required = false) Integer limit) {
        LOG.info("List pending and failed outgoing messages bundles | startDate: [{}], endDate: [{}], limit:[{}]", startDate, endDate, limit);

        List<MessageReferenceVO> resultVOList = messageBundleService.getFailedMessages(
                startDate,
                endDate,
                limit);

        List<RetriggerBundleRequest> result = new ArrayList<>();
        if(!CollectionUtils.isEmpty(resultVOList)){
            for (MessageReferenceVO messageReferenceVO : resultVOList) {
                result.add(new RetriggerBundleRequest(messageReferenceVO.getMessageUuid(),
                                                      messageReferenceVO.getMessageType().toString(),
                                                      messageReferenceVO.getSenderUuid(),
                                                      messageReferenceVO.getReceiverUuid(),
                                                      messageReferenceVO.getMessageRefUUID(),
                                                      messageReferenceVO.getMessageState().toString(),
                                                      messageReferenceVO.getMessageDirection().toString()));
            }

        }

        return ResponseEntity.ok(result);
    }
}

package eu.europa.ec.etrustex.adapter.service;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.handlers.InboxBundleTransmissionBackendHandler;
import eu.europa.ec.etrustex.adapter.model.entity.AuditEntity;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxError;
import eu.europa.ec.etrustex.adapter.model.entity.message.*;
import eu.europa.ec.etrustex.adapter.model.vo.MessageReferenceVO;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.PartyDao;
import eu.europa.ec.etrustex.adapter.persistence.api.message.MessageDao;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.LoggingUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO.getUuid;
import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_MESSAGE_UUID;
import static org.apache.commons.lang3.StringUtils.trim;

/**
 * Implementation of the MessageServicePlugin to access and modify {@link EtxMessage}
 *
 * @author François Gautier
 * @version 1.0
 * @since 12-Oct-17
 */
@Service("etxBackendPluginMessageServicePlugin")
public class MessageServicePluginImpl implements MessageServicePlugin {

    private static final int MAX_ERR_DETAILS_LENGTH = 4000;
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(MessageServicePluginImpl.class);

    @Autowired
    private MessageDao messageDao;
    @Autowired
    private PartyDao partyDao;

    @Override
    public EtxMessage findById(Long id) {
        EtxMessage byId = messageDao.findById(id);
        if(byId != null) {
            LOG.putMDC(LoggingUtils.MDC_ETX_MESSAGE_UUID, byId.getMessageUuid());
        }
        return byId;
    }

    @Override
    public void createMessage(EtxMessage messageEntity) {
        if(messageEntity != null) {
            LOG.putMDC(LoggingUtils.MDC_ETX_MESSAGE_UUID, messageEntity.getMessageUuid());
            messageDao.save(messageEntity);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateMessage(EtxMessage messageEntity) {
        messageDao.update(messageEntity);
    }

    @Override
    public EtxMessage createMessageStatusFor(EtxMessage messageBundle, MessageReferenceState messageReferenceState) {

        EtxMessage notificationMessageStatus = new EtxMessage();
        notificationMessageStatus.setMessageUuid(UUID.randomUUID().toString());
        notificationMessageStatus.setMessageReferenceUuid(messageBundle.getMessageUuid());
        notificationMessageStatus.setMessageReferenceStateType(messageReferenceState);
        notificationMessageStatus.setIssueDateTime(AuditEntity.getGMTCalendar());
        notificationMessageStatus.setMessageType(MessageType.MESSAGE_ADAPTER_STATUS);
        notificationMessageStatus.setMessageState(MessageState.MS_PROCESSED);
        notificationMessageStatus.setDirectionType(messageBundle.getDirectionType().getOpposite());

        //message status is always for one receiver;
        notificationMessageStatus.setSender(messageBundle.getReceiver());
        notificationMessageStatus.setReceiver(messageBundle.getSender());

        List<EtxError> errors = getErrors(messageBundle);
        if (!errors.isEmpty()) {
            notificationMessageStatus.setStatusReasonCode(ETrustExError.DEFAULT.getCode());
            notificationMessageStatus.setMessageContent(createCompoundErrorDetails(errors));
        }

        createMessage(notificationMessageStatus);

        return notificationMessageStatus;
    }

    private List<EtxError> getErrors(EtxMessage message) {
        List<EtxError> errors = new ArrayList<>();

        if (message.getError() != null) {
            errors.add(message.getError());
        }

        for (EtxAttachment attachment : message.getAttachmentList()) {
            if (attachment.getError() != null) {
                errors.add(attachment.getError());
            }
        }

        return errors;
    }

    private String createCompoundErrorDetails(List<EtxError> errors) {
        StringBuilder errorDetails = new StringBuilder();

        for (EtxError error : errors) {
            if (errorDetails.length() > 0) {
                errorDetails.append("; ");
            }
            errorDetails.append(error.getDetail());
        }

        int end = errorDetails.length() > MAX_ERR_DETAILS_LENGTH ? MAX_ERR_DETAILS_LENGTH : errorDetails.length();
        return errorDetails.substring(0, end);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public EtxMessage readMessage(String senderPartyUuid, String receiverPartyUuid, String messageUuid, List<MessageType> messageTypes) {
        LOG.debug("findEtxMessage invoke - START");

        EtxMessage message = messageDao.findEtxMessage(messageUuid, messageTypes,
                MessageDirection.IN, senderPartyUuid, receiverPartyUuid);

        if(message != null) {
            LOG.putMDC(MDC_ETX_MESSAGE_UUID, message.getMessageUuid());
            if (message.getNotificationState() != NotificationState.NF_NOTIFIED) {
                message.setNotificationState(NotificationState.NF_NOTIFIED);
            }
        }
        LOG.debug("findEtxMessage invoke - END");

        return message;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MessageReferenceVO> findMessagesForReceiverPartyUuid(String partyUuid) {
        LOG.debug("retrieve unread messages for party uuid {}", partyUuid);

        return messageDao.findUnreadMessagesByReceiverPartyUuid(partyUuid);
    }

    @Override
    public MessageType referenceMessageTypeFor(EtxMessage messageEntity) {
        LOG.debug("referenceMessageTypeFor invoke - START with messageUid: " + messageEntity.getMessageUuid());
        Long receiverPartyId = messageEntity.getSender().getId();
        Long senderPartyId = messageEntity.getReceiver().getId();
        List<MessageReferenceVO> messageReferenceVOs = messageDao.findMessagesByUuidAndSenderAndReceiver(messageEntity.getMessageReferenceUuid(), senderPartyId, receiverPartyId);
        if (messageReferenceVOs.size() != 1) {
            throw new ETrustExPluginException(ETrustExError.ETX_009,
                    "message uuid: " + messageEntity.getMessageReferenceUuid() + " receiverPartyUuid:" + receiverPartyId + " senderPartyUuid: " + senderPartyId + ", records expected: 1, records found: " + messageReferenceVOs.size(), null);
        }
        LOG.debug("referenceMessageTypeFor invoke - END");
        return messageReferenceVOs.iterator().next().getMessageType();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EtxMessage findMessageByDomibusMessageId(String domibusMessageID) {
        EtxMessage etxMessage;
        domibusMessageID = trim(domibusMessageID);
        if (StringUtils.isBlank(domibusMessageID)) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "The Domibus Message ID required to retrieve the eTrustEx Backend Message has not been sent!");
        } else {
            etxMessage = messageDao.findMessageByDomibusMessageId(domibusMessageID);
            if (etxMessage == null) {
                throw new ETrustExPluginException(ETrustExError.ETX_005, "Missing entry in eTrustEx Backend Message DB for Domibus Message Id: " + domibusMessageID);
            }
            LOG.putMDC(LoggingUtils.MDC_ETX_MESSAGE_UUID, etxMessage.getMessageUuid());
            return etxMessage;
        }
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public EtxMessage findOrCreateMessageByDomibusMessageId(ETrustExAdapterDTO eTrustExAdapterDTO) {
        EtxMessage etxMessage;
        String domibusMessageID = trim(getUuid(eTrustExAdapterDTO));
        etxMessage = messageDao.findMessageByDomibusMessageId(domibusMessageID);

        if (etxMessage == null) {
            EtxParty etxSenderParty = partyDao.findByPartyUUID(eTrustExAdapterDTO.getAs4OriginalSender());
            EtxParty etxReceiverParty = partyDao.findByPartyUUID(eTrustExAdapterDTO.getAs4FinalRecipient());
            if (etxReceiverParty != null && etxSenderParty != null) {
                etxMessage = InboxBundleTransmissionBackendHandler.getEtxMessage(eTrustExAdapterDTO, etxSenderParty, etxReceiverParty);
                etxMessage.setDomibusMessageId(domibusMessageID);
            }
            createMessage(etxMessage);
        } else {
            LOG.putMDC(MDC_ETX_MESSAGE_UUID, etxMessage.getMessageUuid());
        }
        return etxMessage;
    }
}

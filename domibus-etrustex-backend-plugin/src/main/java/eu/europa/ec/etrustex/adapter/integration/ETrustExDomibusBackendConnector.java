package eu.europa.ec.etrustex.adapter.integration;

import eu.domibus.common.DeliverMessageEvent;
import eu.domibus.common.MessageReceiveFailureEvent;
import eu.domibus.common.MessageSendFailedEvent;
import eu.domibus.common.MessageSendSuccessEvent;
import eu.domibus.ext.domain.MessageAttemptDTO;
import eu.domibus.ext.services.MessageMonitorExtService;
import eu.domibus.ext.services.UserMessageExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.messaging.MessageNotFoundException;
import eu.domibus.plugin.AbstractBackendConnector;
import eu.domibus.plugin.transformer.MessageRetrievalTransformer;
import eu.domibus.plugin.transformer.MessageSubmissionTransformer;
import eu.europa.ec.etrustex.adapter.integration.handlers.BackendActionHandlerFactory;
import eu.europa.ec.etrustex.common.converters.ETrustExAdapterDTOConverter;
import eu.europa.ec.etrustex.common.converters.ETrustExAdapterDTOSubmissionConverter;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.handlers.Action;
import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.handlers.Service;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.common.util.ErrorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static eu.europa.ec.etrustex.common.util.AS4MessageConstants.MIME_TYPE_TEXT_XML;
import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_FAULT;
import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_CONVERSATION_ID;
import static eu.europa.ec.etrustex.common.util.PayloadDescription.ETRUSTEX_FAULT_RESPONSE_PAYLOAD;

/**
 * Implementation class for the backend connector to connect between ETrustEx backend plugin and Domibus for sending and receiving messages.
 *
 * @author Arun Raj
 * @version 1.0
 */
public class ETrustExDomibusBackendConnector extends AbstractBackendConnector<ETrustExAdapterDTO, ETrustExAdapterDTO> {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ETrustExDomibusBackendConnector.class);

    @Autowired
    private UserMessageExtService userMessageService;

    private static ETrustExAdapterDTOSubmissionConverter defaultTransformer = new ETrustExAdapterDTOSubmissionConverter();

    @Autowired
    private MessageMonitorExtService messageMonitorService;

    @Autowired
    private BackendActionHandlerFactory backendActionHandlerFactory;

    @Autowired
    private ClearEtxLogKeysService clearEtxLogKeysService;

    public ETrustExDomibusBackendConnector(String name) {
        super(name);
    }

    @Override
    public MessageSubmissionTransformer<ETrustExAdapterDTO> getMessageSubmissionTransformer() {
        //submission transformer
        return defaultTransformer;
    }

    @Override
    public MessageRetrievalTransformer<ETrustExAdapterDTO> getMessageRetrievalTransformer() {
        //retrieval transformer
        return defaultTransformer;
    }

    @Override
    public void messageSendSuccess(MessageSendSuccessEvent messageSendSuccessEvent) {
        LOG.info("Domibus message:[{}] submitted by eTrustEx Backend Plugin, was sent successfully.", messageSendSuccessEvent.getMessageId());
    }

    @Override
    @ClearEtxLogKeys
    public void messageSendFailed(MessageSendFailedEvent messageSendFailedEvent) {
        String messageId = messageSendFailedEvent.getMessageId();
        LOG.error("Sending message from Domibus ETrustEx Backend Plugin failed, for AS4 message id:[{}]", messageId);
        ETrustExAdapterDTO eTrustExAdapterDTO = retrieveMessageSentFailure(messageId);
        LOG.putMDC(MDC_ETX_CONVERSATION_ID, eTrustExAdapterDTO.getAs4ConversationId());
        try {
            Service service = Service.fromString(eTrustExAdapterDTO.getAs4Service());
            OperationHandler responseFaultHandler = backendActionHandlerFactory.getOperationHandler(service, Action.FAULT_ACTION);
            responseFaultHandler.handle(eTrustExAdapterDTO);
        } catch (Exception e) {
            LOG.error("For Domibus message in failure, the processing encountered exception:", e);
            throw e;
        }
        clearEtxLogKeysService.clearKeys();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    @ClearEtxLogKeys
    public void deliverMessage(DeliverMessageEvent deliverMessageEvent) {
        String messageId = deliverMessageEvent.getMessageId();
        ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();

        try {
            eTrustExAdapterDTO = downloadMessage(messageId, eTrustExAdapterDTO);
            LOG.putMDC(MDC_ETX_CONVERSATION_ID, eTrustExAdapterDTO.getAs4ConversationId());
        } catch (final MessageNotFoundException ex) {
            LOG.error("Message cannot be downloaded due to:", ex);
            throw new ETrustExPluginException(ETrustExError.ETX_005, "Message cannot be downloaded", ex);
        }

        try {
            final Service service = Service.fromString(eTrustExAdapterDTO.getAs4Service());
            final Action action = Action.fromString(eTrustExAdapterDTO.getAs4Action());
            LOG.debug("deliverMessage called for Service:[{}] and Action:[{}]", service.getCode(), action.getCode());

            OperationHandler backendPluginOperationHandler = backendActionHandlerFactory.getOperationHandler(service, action);
            backendPluginOperationHandler.handle(eTrustExAdapterDTO);
        } catch (Exception e) {
            LOG.error("Could not perform operation with [{}]", eTrustExAdapterDTO, e);
            throw e;
        }
        clearEtxLogKeysService.clearKeys();
    }

    @Override
    public void messageReceiveFailed(MessageReceiveFailureEvent messageReceiveFailureEvent) {
        ErrorUtils.logMessageReceiveFailureEventString(messageReceiveFailureEvent, this.getClass().getName());
    }

    private ETrustExAdapterDTO retrieveMessageSentFailure(String messageId) {
        ETrustExAdapterDTO eTrustExAdapterDTO = ETrustExAdapterDTOConverter.convertToETrustExAdapterDTO(userMessageService.getMessage(messageId));
        List<MessageAttemptDTO> attemptsHistory = messageMonitorService.getAttemptsHistory(messageId);
        StringBuilder errors = new StringBuilder();
        for (MessageAttemptDTO messageAttemptDTO : attemptsHistory) {
            errors.append(messageAttemptDTO.toString()).append("|");
        }
        PayloadDTO etxPayloadDTO = new PayloadDTO(CONTENT_FAULT.getValue(), errors.toString().getBytes(), MIME_TYPE_TEXT_XML, false, ETRUSTEX_FAULT_RESPONSE_PAYLOAD.getValue());
        eTrustExAdapterDTO.addAs4Payload(etxPayloadDTO);
        return eTrustExAdapterDTO;
    }
}

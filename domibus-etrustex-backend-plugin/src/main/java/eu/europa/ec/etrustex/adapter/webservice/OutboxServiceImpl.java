package eu.europa.ec.etrustex.adapter.webservice;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.converters.BackendMessageBundleTypeConverter;
import eu.europa.ec.etrustex.adapter.integration.converters.BackendMessageStatusTypeConverter;
import eu.europa.ec.etrustex.adapter.integration.converters.ErrorTypeConverter;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentDirection;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.queue.out.downloadattachment.OutDownloadAttachmentProducer;
import eu.europa.ec.etrustex.adapter.queue.out.sendmessage.SendMessageProducer;
import eu.europa.ec.etrustex.adapter.service.MessageBundleService;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.adapter.service.security.IdentityManager;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.integration.model.common.v2.ErrorType;
import eu.europa.ec.etrustex.integration.model.common.v2.EtxMessageBundleType;
import eu.europa.ec.etrustex.integration.model.common.v2.EtxMessageCodeType;
import eu.europa.ec.etrustex.integration.model.common.v2.EtxMessageStatusType;
import eu.europa.ec.etrustex.integration.service.outbox.v2.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * Implementation of the wsdl for the OutboxService
 *
 * @author Arun Raj
 * @version 1.0
 */
@Transactional
@WebService(
        name = "OutboxServiceV2",
        portName = "OutboxSoap11",
        serviceName = "OutboxService-2.0",
        targetNamespace = "urn:eu:europa:ec:etrustex:integration:service:outbox:v2.0",
        endpointInterface = "eu.europa.ec.etrustex.integration.service.outbox.v2.OutboxPortType"
)
public class OutboxServiceImpl implements OutboxPortType {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(OutboxServiceImpl.class);

    @Autowired
    private MessageServicePlugin messageService;

    @Autowired
    private IdentityManager identityManager;

    @Autowired
    private SendMessageProducer sendMessageProducer;

    @Autowired
    private OutDownloadAttachmentProducer outDownloadAttachmentProducer;

    @Autowired
    @Qualifier(eu.europa.ec.etrustex.adapter.service.MessageBundleService.MESSAGEBUNDLESERVICEBEANNAME)
    private MessageBundleService messageBundleService;

    @Override
    @Transactional
    public SendMessageBundleResponseType sendMessageBundle(@WebParam(name = "sendMessageBundleRequest", targetNamespace = "urn:eu:europa:ec:etrustex:integration:service:outbox:v2.0", partName = "sendMessageBundleRequest") SendMessageBundleRequestType sendMessageBundleRequestType) throws FaultResponse {
        try {
            String username = identityManager.retrieveAuthenticatedUserName();

            String senderUUID = sendMessageBundleRequestType.getMessageBundle().getSender().getId();

            // Check if the sender of the message bundle is the same with the authenticated user.
            if (identityManager.hasAccess(senderUUID, username)) {
                EtxMessage messageBundle = persistSendMessageBundleRequest(sendMessageBundleRequestType.getMessageBundle());
                LOG.info("identityManager.retrieveAuthenticatedUserName(): [{}]", username);
                messageBundleService.triggerSendMessageBundle(messageBundle);
            } else {
                String errorMessage = "Authenticated user: " + username + " does not have access to party: " + senderUUID;
                LOG.warn(errorMessage);
                throw new ETrustExPluginException(ETrustExError.ETX_001, errorMessage, null);
            }

            return new SendMessageBundleResponseType();
        } catch (ETrustExPluginException e) {
            LOG.error("Error sending the message bundle!", e);
            ErrorType businessError = ErrorTypeConverter.buildErrorType(e);
            throw new FaultResponse(businessError.getDescription(), businessError);
        } catch (Exception e) {
            LOG.error("Error sending the message bundle!", e);
            //TODO fix the general exception handling for faultinfo
            throw new FaultResponse(e.getMessage(), null, e);
        }
    }

    public EtxMessage persistSendMessageBundleRequest(EtxMessageBundleType messageBundleType) {
        EtxMessage messageBundle = BackendMessageBundleTypeConverter.toEtxMessage(messageBundleType);

        //set the state, type and direction
        messageBundle.setMessageType(MessageType.MESSAGE_BUNDLE);
        messageBundle.setMessageState(MessageState.MS_CREATED);
        messageBundle.setDirectionType(MessageDirection.OUT);

        //load and set the sender party
        String senderPartyUuid = messageBundleType.getSender().getId();
        EtxParty senderParty = loadEtxParty(senderPartyUuid);
        messageBundle.setSender(senderParty);

        //load and set the receiver party
        String receiverPartyUuid = messageBundleType.getReceiverList().getRecipient().get(0).getId();
        EtxParty receiverParty = loadEtxParty(receiverPartyUuid);
        messageBundle.setReceiver(receiverParty);

        for (EtxAttachment etxAttachment : messageBundle.getAttachmentList()) {
            etxAttachment.setMessage(messageBundle);
            etxAttachment.setStateType(AttachmentState.ATTS_CREATED);
            etxAttachment.setDirectionType(AttachmentDirection.OUTGOING);
            etxAttachment.setActiveState(true);
        }
        messageService.createMessage(messageBundle);
        return messageBundle;
    }

    @Override
    @Transactional
    public SendMessageStatusResponseType sendMessageStatus(@WebParam(name = "sendMessageStatusRequest", targetNamespace = "urn:eu:europa:ec:etrustex:integration:service:outbox:v2.0", partName = "sendMessageStatusRequest") SendMessageStatusRequestType sendMessageStatusRequestType) throws FaultResponse {
        SendMessageStatusResponseType sendMessageStatusResponseType = new SendMessageStatusResponseType();

        try {
            EtxMessageCodeType type = sendMessageStatusRequestType.getMessageStatus().getMessageReference().getType();
            if (type == EtxMessageCodeType.MESSAGE_STATUS) {
                throw new UnsupportedOperationException("Sending status for message with type MESSAGE_STATUS is not supported");
            }

            String username = identityManager.retrieveAuthenticatedUserName();
            LOG.info("identityManager.retrieveAuthenticatedUserName():{}", username);
            String senderUUID = sendMessageStatusRequestType.getMessageStatus().getSender().getId();
            LOG.debug("senderUUID:{}", senderUUID);
            if (identityManager.hasAccess(senderUUID, username)) {

                EtxMessage messageStatus = persistSendMessageStatusRequest(sendMessageStatusRequestType.getMessageStatus());
                sendMessageProducer.triggerSendStatus(messageStatus.getId());

            } else {
                String errorMessage = "Authenticated user: " + username + " does not have access to party: " + senderUUID;
                LOG.warn(errorMessage);
                throw new ETrustExPluginException(ETrustExError.ETX_001, errorMessage, null);
            }
        } catch (ETrustExPluginException e) {
            LOG.error("Error sending the message status!", e);
            ErrorType businessError = ErrorTypeConverter.buildErrorType(e);
            throw new FaultResponse(businessError.getDescription(), businessError);
        } catch (Exception e) {
            LOG.error("Error sending the message status!", e);
            //TODO fix the general exception handling for faultinfo
            throw new FaultResponse(e.getMessage(), null, e);
        }
        return sendMessageStatusResponseType;
    }

    public EtxMessage persistSendMessageStatusRequest(EtxMessageStatusType messageStatusType) {
        EtxMessage messageStatus = BackendMessageStatusTypeConverter.toEtxMessage(messageStatusType);
        //set the message state, type and direction
        messageStatus.setMessageType(MessageType.MESSAGE_STATUS);
        messageStatus.setMessageState(MessageState.MS_CREATED);
        messageStatus.setDirectionType(MessageDirection.OUT);

        //set the sender party
        String senderPartyUuid = messageStatusType.getSender().getId();
        EtxParty senderParty = loadEtxParty(senderPartyUuid);
        messageStatus.setSender(senderParty);
        LOG.debug("Sender party:{}", senderParty);

        //set the receiver party
        String receiverPartyUuid = messageStatusType.getReceiver().getId();
        EtxParty receiverParty = loadEtxParty(receiverPartyUuid);
        messageStatus.setReceiver(receiverParty);
        LOG.debug("Receiver party:{}", receiverParty);

        messageService.createMessage(messageStatus);
        return messageStatus;
    }


    EtxParty loadEtxParty(String partyUuid)  {
        EtxParty receiverParty = identityManager.searchPartyByUuid(partyUuid);
        if (receiverParty == null) {
            throw  new ETrustExPluginException(ETrustExError.ETX_004, "Party: " + partyUuid + " could not be located on the DB.", null);
        }
        return receiverParty;
    }

}

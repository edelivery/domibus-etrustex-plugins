package eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter;

import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleRequest;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.converters.NodeDocumentBundleTypeConverter;
import eu.europa.ec.etrustex.adapter.integration.handlers.ResponseFaultHandler;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.common.exceptions.DomibusSubmissionException;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.util.ConversationIdKey;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.ws.Holder;

import static eu.europa.ec.etrustex.common.util.TransformerUtils.serializeObjToXML;

/**
 * This adapter is intended to convert messages of {@link eu.europa.ec.etrustex.adapter.model.entity.message.MessageType}=MESSAGE_BUNDLE in the direction from Backend to Node plugin to Domibus request.<br/>
 * From the backend plugin DB entity {@link EtxMessage}, the request parameters to invoke the eTrustEx Node service - {@link ec.services.wsdl.documentbundle_2.DocumentBundleService}:submitDocumentBundle are formulated and added as payload to Domibus.<br/>
 * AS4 request is formulated and submitted to Domibus for request leg - <b>nodeSubmitDocumentBundleRequest</b>.<br/>
 *
 * @author Arun Raj
 * @version 1.0
 * @since 7-Jun-2017
 */
//TODO:EDELIVERY-6608 - Remove ETX BE Plugin NodeInvocationManager facade & NodeServiceAdapters & refactor code functionally

@Service("etxBackendPluginNodeDocumentBundleServiceAdapter")
public class NodeDocumentBundleServiceAdapter extends NodeServicesAdapterBase {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(NodeDocumentBundleServiceAdapter.class);

    @Autowired
    private ResponseFaultHandler responseFaultHandler;

    /**
     * This operation - creates the input to send the AS4 request leg - <b>nodeSubmitDocumentBundleRequest</b> and submits to Domibus.<br/>
     * The following steps happen:
     * <ol>
     * <li>The request parameters required to invoke the eTrustEx Node service - {@link ec.services.wsdl.documentbundle_2.DocumentBundleService}:submitDocumentBundle consist of {@link HeaderType} and {@link SubmitDocumentBundleRequest}. These are formulated from the EtxMessage for {@link eu.europa.ec.etrustex.adapter.model.entity.message.MessageType}=MESSAGE_BUNDLE and {@link eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection}=OUT.</li>
     * <li>The above request paramters are added as payload to Domibus</li>
     * <li>AS4 Request is formed and submitted to Domibus</li>
     * <li>The eTrustEx backend plugin DB is updated with Domibus message id.</li>
     * </ol>
     *
     * @param messageBundle message bundle we want to send
     * @throws ETrustExPluginException thrown in case anything goes wrong
     */
    @Transactional
    public void sendMessageBundle(EtxMessage messageBundle) throws ETrustExPluginException {
        LOG.info("For Bundle Message ID: [{}]], Start of Send Message Bundle to Node plugin.", messageBundle.getId());
        try {
            // Fill header & request
            Holder<HeaderType> authorisationHeaderHolder = fillAuthorisationHeaderWithStatusScope(messageBundle.getSender().getPartyUuid(), messageBundle.getReceiver().getPartyUuid());
            SubmitDocumentBundleRequest request = new SubmitDocumentBundleRequest();
            request.setDocumentBundle(NodeDocumentBundleTypeConverter.convertEtxMessage(messageBundle));

            ETrustExAdapterDTO eTrustExAdapterDTO = populateETrustExAdapterDTOForSubmitDocumentBundle(authorisationHeaderHolder, request, messageBundle);
            final String as4MsgId = backendConnectorSubmissionService.submit(eTrustExAdapterDTO);
            LOG.info("For Bundle Message ID: [{}], Submitted submitDocumentBundle request message to Domibus Backend Connector.", messageBundle.getId());
            messageBundle.setDomibusMessageId(as4MsgId);
        } catch (DomibusSubmissionException dsEx) {
            LOG.error("For Bundle Message ID: [{}], Exception encountered in Outbox flow while sending document bundle to Node plugin:", messageBundle.getId(), dsEx);
            responseFaultHandler.process(dsEx.getMessage(), messageBundle);
        } catch (Exception ex) {
            LOG.error("For Bundle Message ID: [{}], Exception encountered in Outbox flow while sending document bundle to Node plugin:", messageBundle.getId(), ex);
            throw new ETrustExPluginException(ETrustExError.DEFAULT, ex.getMessage(), ex);
        }
        LOG.info("For Bundle Message ID: [{}]], End of Send Message Bundle to Node plugin.", messageBundle.getId());
    }

    ETrustExAdapterDTO populateETrustExAdapterDTOForSubmitDocumentBundle(Holder<HeaderType> authorisationHeaderHolder, SubmitDocumentBundleRequest request, EtxMessage messageBundle) {

        ETrustExAdapterDTO etxAdapterDTO = createETrustExAdapterDTOWithCommonDataForDomibusSubmission(messageBundle.getSender(), messageBundle.getReceiver(), messageBundle.getId());
        etxAdapterDTO.setAs4ConversationId(ConversationIdKey.OUTBOX_SEND_MESSAGE_BUNDLE.getConversationIdKey() + messageBundle.getId());
        //Collaboration Info
        etxAdapterDTO.setAs4Service(eTrustExBackendPluginProperties.getAs4_Service_DocumentBundle());
        etxAdapterDTO.setAs4ServiceType(eTrustExBackendPluginProperties.getAs4_Service_DocumentBundle_ServiceType());
        etxAdapterDTO.setAs4Action(eTrustExBackendPluginProperties.getAs4_Action_SubmitDocumentBundle_Request());

        //populating the payload
        String strETrustExHeaderXML = TransformerUtils.serializeObjToXML(authorisationHeaderHolder.value);
        LOG.debug("For Bundle Message ID: [{}], SubmitDocumentBundleRequest:HeaderXML: [{}]", messageBundle.getId(),  strETrustExHeaderXML);
        PayloadDTO headerPayloadDTO = TransformerUtils.getHeaderPayloadDTO(strETrustExHeaderXML);
        etxAdapterDTO.addAs4Payload(headerPayloadDTO);

        String strSubmitDocumentBundleRequestXML = serializeObjToXML(request);
        LOG.debug("For Bundle Message ID: [{}]], SubmitDocumentBundle:RequestXML: [{}]", messageBundle.getId(), strSubmitDocumentBundleRequestXML);
        PayloadDTO genericPayloadDTO = TransformerUtils.getGenericPayloadDTO(strSubmitDocumentBundleRequestXML);
        etxAdapterDTO.addAs4Payload(genericPayloadDTO);

        return etxAdapterDTO;
    }
}

package eu.europa.ec.etrustex.adapter.queue.postupload;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.queue.out.sendmessage.SendMessageProducer;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.adapter.service.WorkspaceServiceBackEndImpl;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.common.util.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_ATT_UUID;

/**
 * @author micleva
 * @project ETX
 */

@Service("etxBackendPluginPostUploadHandler")
public class PostUploadHandlerImpl implements PostUploadHandler {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PostUploadHandlerImpl.class);

    @Autowired
    private MessageServicePlugin messageService;

    @Autowired
    private SendMessageProducer sendMessageProducer;

    @Autowired
    private PostProcessProducer postProcessProducer;

    @Autowired
    @Qualifier("etxBackendPluginWorkspaceService")
    private WorkspaceServiceBackEndImpl workspaceServiceBackEnd;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private ClearEtxLogKeysService clearEtxLogKeysService;

    @Override
    public void handlePostUpload(Long messageId) {
        long startTime = System.currentTimeMillis();

        EtxMessage messageEntity = messageService.findById(messageId);
        Validate.notNull(messageEntity, "EtxMessage Entity should not be null at handlePostUpload!");
        LOG.info("PostUploadConsumer.handlePostUpload for message: [{} ID: {}] START", messageEntity.getMessageType(), messageId);

        if (!messageEntity.getMessageState().isFinal() && attachmentService.areAllAttachmentsInFinalState(messageEntity)) {
            triggerNextPhase(messageEntity);
        }

        //Sending files for delete asynchronously
        LOG.info("Initiating files to be deleted asynchronously for messageId:[{}]", messageId);
        workspaceServiceBackEnd.cleanWorkspaceFilesForBundleAsynchronously(messageEntity);

        LOG.info("PostUploadConsumer.handlePostUpload for messageId: [{} ID:{}] completed in time:[{}ms].", messageEntity.getMessageType(), messageId, (System.currentTimeMillis() - startTime));
    }


    private void triggerNextPhase(EtxMessage messageEntity) {
        Long messageId = messageEntity.getId();
        if (messageEntity.getDirectionType() == MessageDirection.IN) {
            LOG.info("For Message [ID:{}], triggering PostProcess.", messageId);
            postProcessProducer.triggerPostProcess(messageId);
        } else if (messageEntity.getDirectionType() == MessageDirection.OUT) {
            if (attachmentService.areAllAttachmentsUploaded(messageEntity)) {
                triggerSendBundleIfNotTriggered(messageEntity);
            } else {
                LOG.info("For Message [ID:{}], triggering PostProcess.", messageId);
                postProcessProducer.triggerPostProcess(messageId);
            }
        }
    }

    private void triggerSendBundleIfNotTriggered(EtxMessage messageEntity) {
        if ((messageEntity.getMessageState().equals(MessageState.MS_CREATED)) || (messageEntity.getMessageState().equals(MessageState.MS_UPLOAD_REQUESTED))) {
            messageEntity.setMessageState(MessageState.MS_UPLOADED);
            messageService.updateMessage(messageEntity);
            LOG.info("For Message [ID:{}], triggering SendBundle.", messageEntity.getId());
            sendMessageProducer.triggerSendBundle(messageEntity.getId());
        }
    }

    @Override
    @ClearEtxLogKeys(clearCustomKeys = {MDC_ETX_ATT_UUID})
    public void validatePostUpload(Long messageId) {
        EtxMessage messageEntity = messageService.findById(messageId);
        Validate.notNull(messageEntity, "EtxMessage Entity should not be null at validatePostUpload!");

        for (EtxAttachment attachmentEntity : messageEntity.getAttachmentList()) {
            AttachmentState attachmentState = attachmentEntity.getStateType();
            if (!attachmentState.isFinal()) {
                LOG.putMDC(MDC_ETX_ATT_UUID, attachmentEntity.getAttachmentUuid());
                String errorDetails = String.format("For Message[ID: %s] Non-final attachment state [ID: %s; state: %s]",
                        messageId, attachmentEntity.getId(), attachmentState);
                LOG.warn(errorDetails);
                LOG.putMDC(MDC_ETX_ATT_UUID, "");
                throw new ETrustExPluginException(ETrustExError.DEFAULT, errorDetails, null);
            }
        }
        clearEtxLogKeysService.clearSpecificKeys(MDC_ETX_ATT_UUID);
    }

    @Override
    public void onError(Long messageId) {
        EtxMessage etxMessage = messageService.findById(messageId);
        Validate.notNull(etxMessage, "EtxMessage Entity should not be null at PostUpload onError!");
        if (attachmentService.areAllAttachmentsInFinalState(etxMessage)) {
            triggerNextPhase(etxMessage);
        }
    }
}


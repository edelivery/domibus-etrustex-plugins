package eu.europa.ec.etrustex.adapter.integration.client.backend.repository;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.client.backend.BackendServiceClient;
import eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.vo.FileAttachmentVO;
import org.apache.cxf.attachment.LazyDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Map;

/**
 *
 * @author Federico Martini
 * @version 2.0
 * @since 30/01/2018
 *
 * Added releaseResources method in order to close the input stream of the attachment data source.
 * When an input stream is closed the underlying resource is released and can be garbage collected.
 *
 * Original version
 * @author: micleva
 * @date: 9/12/13 9:56 AM
 */
@Service("etxBackendPluginWebServiceFileRepositoryService")
class WebServiceFileRepositoryService implements FileRepository {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(WebServiceFileRepositoryService.class);

    @Autowired
    @Qualifier("etxBackendPluginServiceWithWebService")
    private BackendServiceClient webServiceBackendServiceClient;

    @Override
    public FileAttachmentVO readFile(Map<SystemConfigurationProperty, String> systemConfig, EtxAttachment etxAttachment) {
        return webServiceBackendServiceClient.downloadAttachment(systemConfig, etxAttachment.getMessage(), etxAttachment);
    }

    @Override
    public void writeFile(Map<SystemConfigurationProperty, String> systemConfig, EtxAttachment etxAttachment, Path attachmentFile) {
        webServiceBackendServiceClient.uploadAttachment(systemConfig, etxAttachment.getMessage(), etxAttachment, attachmentFile.toFile());
    }

    @Override
    public boolean deleteDirectory(EtxMessage message) {
        return true;
    }


    @Override
    public void releaseResources(FileAttachmentVO faVo) throws IOException {

        DataHandler dataHandler = faVo.getDataHandler();
        if (dataHandler == null || dataHandler.getDataSource() == null)
            return;

        DataSource ds = dataHandler.getDataSource();
        if (ds instanceof LazyDataSource) {
            InputStream is = ds.getInputStream();
            /** close will delete resource(etc: temporary file) held by the input stream; */
            is.close();
            LOG.debug("Input stream successfully closed");
        } else {
            LOG.debug("Data source is [" + ds.getClass().getName() + "] and for content type [" + ds.getContentType() + "]");
        }

    }
}

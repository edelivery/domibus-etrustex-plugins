package eu.europa.ec.etrustex.adapter.model.entity.administration;

/**
 * @author micleva
 * @project ETX
 */

public enum BackendFileRepositoryLocationType {

    LOCAL_SHARED_FILE_SYSTEM,

    BACKEND_FILE_REPOSITORY_SERVICE
}
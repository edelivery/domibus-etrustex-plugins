package eu.europa.ec.etrustex.adapter.persistence.api;

import java.util.List;

/**
 * @see eu.europa.ec.etrustex.adapter.persistence.api.DaoBase
 */
public interface DaoBase<T> {

    /**
     * Finds an entity of the current type by its numeric ID (primary key).
     *
     * @param id an ID
     * @return the persistent object with the given ID, or null, if not found
     */
    T findById(Long id);

    /**
     * Finds all entities
     *
     * @return the persistent objects
     */
    List<T> findAll();

    /**
     * Saves the given, <i>new</i> entity.
     *
     * @param instance
     */
    void save(T instance);

    /**
     * Removes the given, <i>existing</i> entity.
     *
     * @param instance
     */
    void remove(T instance);

    /**
     * Removes the given, <i>existing</i> entity by its numeric ID (primary key).
     *
     * @param id an ID
     */
    void removeById(Long id);

    /**
     * Updates the given detached instance (must exist already).
     *
     * @param detachedInstance
     * @return
     */
    T update(T detachedInstance);

}
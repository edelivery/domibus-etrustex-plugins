package eu.europa.ec.etrustex.adapter.queue;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiObjectFactoryBean;

import javax.jms.Queue;
import javax.naming.NamingException;

import static eu.europa.ec.etrustex.adapter.queue.out.downloadattachment.OutDownloadAttachmentProducer.ETX_BACKEND_PLUGIN_OUTBOX_DOWNLOAD_ATTACHMENT_QUEUE;
import static eu.europa.ec.etrustex.adapter.queue.out.sendmessage.SendMessageProducer.ETX_BACKEND_PLUGIN_OUTBOX_SEND_MESSAGE_BUNDLE_QUEUE;

/**
 * This Java class based Spring configuration was introduced in 1.1,
 * replacing XML based JmsConfig of 1.0 as it is necessary to facilitate the {@link JmsConfig} configuration class.
 *
 * @author Francois Gautier
 * @version 1.1
 * @since October - 2018
 */
@Configuration
public class QueueConfig {

    private static final String DOMIBUS_NOTIFY_BE_JMS_QUEUE = "jms/domibus.notification.etrustex.backendPlugin";
    private static final String OUT_DOWNLOAD_ATTACHMENT_JMS_QUEUE = "jms/eTrustExBackendPlugin/out/outDownloadAttachmentJMSQueue";
    private static final String OUT_UPLOAD_ATTACHMENT_JMS_QUEUE = "jms/eTrustExBackendPlugin/out/outUploadAttachmentJMSQueue";
    private static final String OUT_SEND_MESSAGE_JMS_QUEUE = "jms/eTrustExBackendPlugin/out/sendMessageJMSQueue";
    private static final String IN_UPLOAD_ATTACHMENT_JMS_QUEUE = "jms/eTrustExBackendPlugin/in/inUploadAttachmentJMSQueue";
    private static final String POST_UPLOAD_JMS_QUEUE = "jms/eTrustExBackendPlugin/postUploadJMSQueue";
    private static final String POST_PROCESSING_JMS_QUEUE = "jms/eTrustExBackendPlugin/postProcessJMSQueue";
    private static final String NOTIFY_JMS_QUEUE     = "jms/eTrustExBackendPlugin/notifyJMSQueue";
    private static final String ERROR_JMS_QUEUE      = "jms/eTrustExBackendPlugin/errorJMSQueue";
    private static final String DEAD_LETTER_QUEUE    = "jms/eTrustExBackendPlugin/deadLetterJMSQueue";

    @Bean("etxBackendPluginNotifyQueue")
    public Queue notifyQueue() throws NamingException {
        return getQueue(NOTIFY_JMS_QUEUE);
    }

    @Bean(ETX_BACKEND_PLUGIN_OUTBOX_DOWNLOAD_ATTACHMENT_QUEUE)
    public Queue outboxDownloadAttachment() throws NamingException {
        return getQueue(OUT_DOWNLOAD_ATTACHMENT_JMS_QUEUE);
    }

    @Bean("etxBackendPluginNotifyETrustExBackendPluginQueue")
    public Queue notifyETrustExBackendPluginQueue() throws NamingException {
        return getQueue(DOMIBUS_NOTIFY_BE_JMS_QUEUE);
    }

    @Bean("etxBackendPluginOutboxUploadAttachment")
    public Queue outboxUploadAttachment() throws NamingException {
        return getQueue(OUT_UPLOAD_ATTACHMENT_JMS_QUEUE);
    }

    @Bean(ETX_BACKEND_PLUGIN_OUTBOX_SEND_MESSAGE_BUNDLE_QUEUE)
    public Queue outboxSendMessageBundle() throws NamingException {
        return getQueue(OUT_SEND_MESSAGE_JMS_QUEUE);
    }

    @Bean("etxBackendPluginInboxUploadAttachment")
    public Queue inboxUploadAttachment() throws NamingException {
        return getQueue(IN_UPLOAD_ATTACHMENT_JMS_QUEUE);
    }

    @Bean("etxBackendPluginPostUpload")
    public Queue postUpload() throws NamingException {
        return getQueue(POST_UPLOAD_JMS_QUEUE);
    }

    @Bean("etxBackendPluginPostProcessing")
    public Queue postProcessing() throws NamingException {
        return getQueue(POST_PROCESSING_JMS_QUEUE);
    }

    @Bean("etxBackendPluginError")
    public Queue error() throws NamingException {
        return getQueue(ERROR_JMS_QUEUE);
    }

    @Bean("etxBackendPluginDeadLetter")
    public Queue deadLetter() throws NamingException {
        return getQueue(DEAD_LETTER_QUEUE);
    }

    private Queue getQueue(String destination) throws NamingException {
        JndiObjectFactoryBean jndiObjectFactoryBean = new JndiObjectFactoryBean();
        jndiObjectFactoryBean.setJndiName(destination);
        jndiObjectFactoryBean.afterPropertiesSet();
        return (Queue) jndiObjectFactoryBean.getObject();
    }

}

package eu.europa.ec.etrustex.adapter.model.entity.administration;

import eu.europa.ec.etrustex.adapter.model.entity.AuditEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author micleva
 * @project ETX
 */

/**
 * <p>
 * Title: Party
 * </p>
 * <p/>
 * <p>
 * Description: Domain Object describing a Party entity
 * </p>
 */
@Entity(name = "EtxParty")
@AttributeOverrides({
        @AttributeOverride(name = "creationDate", column = @Column(name = "PAR_CREATED_ON", insertable = false, updatable = false, nullable = false)),
        @AttributeOverride(name = "modificationDate", column = @Column(name = "PAR_UPDATED_ON"))})
@NamedQueries({
        @NamedQuery(name = "EtxParty.findByUUID", query = "SELECT p FROM EtxParty p WHERE p.partyUuid = :partyUUID")
})
@Table(name = "ETX_ADT_PARTY")
public class EtxParty extends AuditEntity<Long> implements Serializable {

    private static final long serialVersionUID = 7904587272694082719L;
    @Id
    @Column(name = "PAR_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "PAR_UUID", length = 50, nullable = false, unique = true)
    private String partyUuid;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "SYS_ID", nullable = false)
    private EtxSystem system;

    public EtxParty() {
        //empty constructor
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getPartyUuid() {
        return partyUuid;
    }

    public EtxSystem getSystem() {
        return system;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public void setPartyUuid(String partyUuid) {
        this.partyUuid = partyUuid;
    }

    public void setSystem(EtxSystem system) {
        this.system = system;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("EtxParty");
        sb.append("{id=").append(id);
        sb.append(", partyUuid='").append(partyUuid).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

package eu.europa.ec.etrustex.adapter.integration.client.etxnode;

import eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter.NodeApplicationResponseServicesAdapter;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter.NodeDocumentBundleServiceAdapter;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter.NodeDocumentWrapperServiceAdapter;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter.NodeRetrieveInterchangeAgreementsServiceAdapter;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import org.springframework.stereotype.Service;

import java.io.File;

/**
 * @author micleva, Arun Raj
 * @version 1.0
 */

@Service("etxBackendPluginNodeInvocationManager")
public class NodeInvocationManagerImpl implements NodeInvocationManager {

    private final NodeDocumentBundleServiceAdapter nodeDocumentBundleServiceAdapter;

    private final NodeApplicationResponseServicesAdapter nodeApplicationResponseServiceAdapter;

    private final NodeDocumentWrapperServiceAdapter nodeDocumentWrapperServiceAdapter;

    private final NodeRetrieveInterchangeAgreementsServiceAdapter nodeRetrieveInterchangeAgreementsServiceAdapter;

    public NodeInvocationManagerImpl(NodeDocumentBundleServiceAdapter nodeDocumentBundleServiceAdapter, NodeApplicationResponseServicesAdapter nodeApplicationResponseServiceAdapter, NodeDocumentWrapperServiceAdapter nodeDocumentWrapperServiceAdapter, NodeRetrieveInterchangeAgreementsServiceAdapter nodeRetrieveInterchangeAgreementsServiceAdapter) {
        this.nodeDocumentBundleServiceAdapter = nodeDocumentBundleServiceAdapter;
        this.nodeApplicationResponseServiceAdapter = nodeApplicationResponseServiceAdapter;
        this.nodeDocumentWrapperServiceAdapter = nodeDocumentWrapperServiceAdapter;
        this.nodeRetrieveInterchangeAgreementsServiceAdapter = nodeRetrieveInterchangeAgreementsServiceAdapter;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void sendMessageBundle(EtxMessage message) {
        nodeDocumentBundleServiceAdapter.sendMessageBundle(message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendMessageStatus(EtxMessage statusMessage) {
        nodeApplicationResponseServiceAdapter.sendMessageStatus(statusMessage);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void uploadAttachment(EtxAttachment etxAttachment, File attachmentFile) {
        nodeDocumentWrapperServiceAdapter.uploadAttachment(etxAttachment, attachmentFile);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String sendInterchangeAgreementRequest(EtxParty senderParty) throws ETrustExPluginException {
        return nodeRetrieveInterchangeAgreementsServiceAdapter.sendInterchangeAgreementRequest(senderParty);
    }

}

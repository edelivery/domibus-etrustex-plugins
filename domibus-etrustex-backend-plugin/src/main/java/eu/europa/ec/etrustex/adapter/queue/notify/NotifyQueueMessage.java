package eu.europa.ec.etrustex.adapter.queue.notify;

import eu.europa.ec.etrustex.common.jms.QueueMessage;

public class NotifyQueueMessage extends QueueMessage {

    private static final long serialVersionUID = 2358216708220933538L;

    private Long messageId;

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("NotifyQueueMessage");
        sb.append("{messageId=").append(messageId);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NotifyQueueMessage that = (NotifyQueueMessage) o;

        if (messageId != null ? !messageId.equals(that.messageId) : that.messageId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return messageId != null ? messageId.hashCode() : 0;
    }
}

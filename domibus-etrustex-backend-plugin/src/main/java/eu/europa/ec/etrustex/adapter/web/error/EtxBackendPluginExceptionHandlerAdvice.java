package eu.europa.ec.etrustex.adapter.web.error;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;

/**
 * ControllerAdvice for handling exceptions in EXT REST Api
 *
 * @author Catalin Enache
 * @since 4.2
 */
@ControllerAdvice("eu.europa.ec.etrustex.adapter.web")
@RequestMapping(produces = "application/vnd.error+json")
public class EtxBackendPluginExceptionHandlerAdvice extends ResponseEntityExceptionHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(EtxBackendPluginExceptionHandlerAdvice.class);

    @ExceptionHandler(ETrustExPluginException.class)
    public ResponseEntity<String> handleETrustExPluginException(ETrustExPluginException e) {
        LOG.error( e.getError().getCode() + ":- " + e.getMessage(), e);
        return ResponseEntity.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).body(e.getError().getCode() + ": " + e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleException(Exception e) {
        LOG.error(e.getMessage(), e);
        return ResponseEntity.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).body(e.getMessage());
    }
}

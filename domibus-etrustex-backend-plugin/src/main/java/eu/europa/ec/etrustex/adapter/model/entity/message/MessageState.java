package eu.europa.ec.etrustex.adapter.model.entity.message;

/**
 * @author apladap
 * @project ETX
 */

public enum MessageState {
    MS_CREATED(false),

    MS_UPLOAD_REQUESTED(false),

    MS_UPLOADED(false),

    MS_PROCESSED(true),

    MS_FAILED(true);


    private boolean isFinal;

    MessageState(boolean isFinal) {
        this.isFinal = isFinal;
    }

    public boolean isFinal() {
        return isFinal;
    }
}

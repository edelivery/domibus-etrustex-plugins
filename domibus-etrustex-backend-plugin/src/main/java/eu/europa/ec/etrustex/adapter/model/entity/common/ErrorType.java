package eu.europa.ec.etrustex.adapter.model.entity.common;


/**
 * @author apladap
 * @deprecated use {@link eu.europa.ec.etrustex.common.exceptions.ETrustExError}
 */
@Deprecated
public enum ErrorType {
    // General Business error types
    TECHNICAL("TECHNICAL", "Technical error type"),
    MSG1("MSG:1", "Message already exist"),
    MSG2("MSG:2", "Invalid party"),
    MSG3("MSG:3", "Message not found"),
    FMD1("FMD:1", "File already exist"),
    FMD2("FMD:2", "Missing file"),
    FLT1("FLT:1", "Unauthorized access"),
    FLT2("FLT:2", "Invalid SOAP request"),

    // General Persistence error types
    DATATYPE_CONFIGURATION_EXCEPTION("DATATYPE_CONFIGURATION_EXCEPTION", "Data Conversion Error"),
    BUSINESS_OBJECT_NOT_FOUND("BUSINESS_OBJECT_NOT_FOUND", "Business object not found"),
    BUSINESS_OBJECT_ALREADY_EXISTS("BUSINESS_OBJECT_ALREADY_EXISTS", "Business object already exists"),
    BUSINESS_OBJECT_INVALID("BUSINESS_OBJECT_INVALID", "Business object invalid"),
    BUSINESS_MULTIPLE_OBJECTS_FOUND("BUSINESS_MULTIPLE_OBJECTS_FOUND", "Business multiple objects found");

    public static ErrorType fromCode(String code) {
        if (code != null) {
            for (ErrorType errorType : ErrorType.values()) {
                if (errorType.getCode().equals(code)) {
                    return errorType;
                }
            }
        }
        return null;
    }

    private String code;
    private String description;

    ErrorType(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}

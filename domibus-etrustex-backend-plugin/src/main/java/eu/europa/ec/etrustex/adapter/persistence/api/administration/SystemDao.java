package eu.europa.ec.etrustex.adapter.persistence.api.administration;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxUser;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBase;

public interface SystemDao extends DaoBase<EtxSystem> {

    /**
     * Find the {@link EtxSystem} for which {@link EtxSystem#localUsername} = {@param userName}
     *
     * @param userName Id of the {@link EtxUser}
     * @return {@link EtxSystem} linked with the {@param userName}
     */
    EtxSystem findSystemByLocalUserName(String userName);

    /**
     * Find the {@link EtxSystem} for which {@link EtxSystem#name} = {@param systemName}
     *
     * @param systemName name of the {@link EtxSystem}
     * @return {@link EtxSystem} with {@param systemName}
     */
    EtxSystem findSystemByName(String systemName);
}

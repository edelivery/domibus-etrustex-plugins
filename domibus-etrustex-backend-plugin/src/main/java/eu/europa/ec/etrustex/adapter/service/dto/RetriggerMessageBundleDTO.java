package eu.europa.ec.etrustex.adapter.service.dto;

import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.vo.MessageReferenceVO;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RetriggerMessageBundleDTO {
    private List<EtxMessage> validEtxMessageBundlesList;

    private List<String> retriggerBundleSuccessResponseList;

    private List<MessageReferenceVO> otherImpactedBundlesVOList;

    private Map<String, List<Exception>> retriggerBundleExceptionsList;

    public RetriggerMessageBundleDTO() {
        validEtxMessageBundlesList = new ArrayList<>();
        retriggerBundleSuccessResponseList = new ArrayList<>();
        otherImpactedBundlesVOList = new ArrayList<>();
        retriggerBundleExceptionsList = new HashMap<>();
    }

    public List<EtxMessage> getValidEtxMessageBundlesList() {
        return validEtxMessageBundlesList;
    }

    public void setValidEtxMessageBundlesList(List<EtxMessage> validEtxMessageBundlesList) {
        this.validEtxMessageBundlesList = validEtxMessageBundlesList;
    }

    public void addValidEtxMessageBundlesList(EtxMessage validEtxMessageBundlesForRetrigger) {
        if(validEtxMessageBundlesList == null){
            validEtxMessageBundlesList = new ArrayList<>();
        }
        this.validEtxMessageBundlesList.add(validEtxMessageBundlesForRetrigger);
    }

    public List<String> getRetriggerBundleSuccessResponseList() {
        return retriggerBundleSuccessResponseList;
    }

    public void setRetriggerBundleSuccessResponseList(List<String> retriggerBundleSuccessResponseList) {
        this.retriggerBundleSuccessResponseList = retriggerBundleSuccessResponseList;
    }

    public void addRetriggerBundleSuccessResponseList(String retriggerBundleSuccessResponse) {
        if(retriggerBundleSuccessResponseList == null){
            retriggerBundleSuccessResponseList = new ArrayList<>();
        }
        this.retriggerBundleSuccessResponseList.add(retriggerBundleSuccessResponse);
    }

    public List<MessageReferenceVO> getOtherImpactedBundlesVOList() {
        return otherImpactedBundlesVOList;
    }

    public void setOtherImpactedBundlesVOList(List<MessageReferenceVO> otherImpactedBundlesVOList) {
        this.otherImpactedBundlesVOList = otherImpactedBundlesVOList;
    }

    public void addOtherImpactedBundlesVO(MessageReferenceVO otherImpactedBundleVO){
        if(this.otherImpactedBundlesVOList == null){
            this.otherImpactedBundlesVOList = new ArrayList<>();
        }
        this.otherImpactedBundlesVOList.add(otherImpactedBundleVO);
    }

    public Map<String, List<Exception>> getRetriggerBundleExceptionsList() {
        return retriggerBundleExceptionsList;
    }

    public void setRetriggerBundleExceptionsList(Map<String, List<Exception>> retriggerBundleExceptionsList) {
        this.retriggerBundleExceptionsList = retriggerBundleExceptionsList;
    }

    public void addRetriggerBundleExceptionsList(String bundleUUID, Exception retriggerBundleException) {
        if(retriggerBundleExceptionsList == null){
            retriggerBundleExceptionsList = new HashMap<>();
        }
        if(CollectionUtils.isNotEmpty(retriggerBundleExceptionsList.get(bundleUUID))){
            retriggerBundleExceptionsList.get(bundleUUID).add(retriggerBundleException);
        }
        else {
            List<Exception> exceptions = new ArrayList<>();
            exceptions.add(retriggerBundleException);
            retriggerBundleExceptionsList.put(bundleUUID, exceptions);
        }
    }
}

package eu.europa.ec.etrustex.adapter.integration.client.backend.ws;

import eu.europa.ec.etrustex.adapter.integration.client.backend.ws.adapter.BackendWebServiceProvider;

/**
 * @author micleva
 * @project ETX
 * @version 1.0
 *
 * @author Arun Raj
 * @version 1.0
 */

public abstract class BackendWebServiceBase {

    protected String userName;
    protected String password;
    protected String serviceEndpoint;
    protected BackendWebServiceProvider webServiceProvider;

    public BackendWebServiceBase(BackendWebServiceProvider webServiceProvider) {
        this.webServiceProvider = webServiceProvider;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setServiceEndpoint(String serviceEndpoint) {
        this.serviceEndpoint = serviceEndpoint;
    }

}

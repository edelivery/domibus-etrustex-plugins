package eu.europa.ec.etrustex.adapter.integration.handlers;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxErrorBuilder;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static eu.europa.ec.etrustex.common.exceptions.EtrustExMarker.NON_RECOVERABLE;

/**
 * Handles an error on the Backend plugin after trying to send an AckInboxBundleNotification to Node plugin.
 *
 * @author Federico Martini
 * @version 1.0
 * @since 05 Dec 2017
 */
@Service("etxBackendPluginInboxBundleTransmissionFaultHandler")
public class InboxBundleTransmissionFaultHandler extends ResponseFaultHandlerBase {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxBundleTransmissionBackendHandler.class);

    @Autowired
    private MessageServicePlugin messageService;

    @Autowired
    private PostProcessProducer postProcessProducer;

    /**
     * <p>On error in Inbox NotifyBundle workflow, update the {@link EtxMessage} and linked {@link EtxAttachment}s to FAILED state
     * and trigger PostProcess workflow.</p>
     *
     * <p>In certain scenarios like PULL mode failed for NotifyBundle transmission, it is possible that the {@link EtxMessage}
     * is not created yet. In that scenario try to create {@link EtxMessage} to try automatic bundle failure handling.</p>
     *
     * <p>If no {@link EtxMessage} can be found or created, then automatic error handling fails and is logged as NON_RECOVERABLE eroor.</p>
     *
     * @param payload  the faultPayload containing the error details
     * @param eTrustExAdapterResponseDTO
     */
    @Override
    protected void process(String payload, ETrustExAdapterDTO eTrustExAdapterResponseDTO) {
        EtxMessage etxMessage = messageService.findOrCreateMessageByDomibusMessageId(eTrustExAdapterResponseDTO);
        if (etxMessage != null) {
            LOG.error("Message in error: " + payload);
            etxMessage.setMessageState(MessageState.MS_FAILED);
            etxMessage.setError(
                    EtxErrorBuilder
                            .getInstance()
                            .withResponseCode(ETrustExError.DEFAULT.getCode())
                            .withDetail("Failed to send AckInboxBundleNotification: " + payload)
                            .build());
            if (etxMessage.hasAttachments()) {
                for (EtxAttachment etxAttachment : etxMessage.getAttachmentList()) {
                    etxAttachment.setStateType(AttachmentState.ATTS_FAILED);
                    etxAttachment.setError(
                            EtxErrorBuilder
                                    .getInstance()
                                    .withResponseCode(ETrustExError.DEFAULT.getCode())
                                    .withDetail("Failed due to failed bundle with UUID [" + etxMessage.getMessageUuid() + "]")
                                    .build());
                }
            }
            messageService.updateMessage(etxMessage);

            postProcessProducer.triggerPostProcess(etxMessage.getId());
        } else {
            LOG.error(NON_RECOVERABLE, "Error for the InboxBundleTransmissionFaultHandler. Payload: {}, uuid: {}", payload, ETrustExAdapterDTO.getUuid(eTrustExAdapterResponseDTO));
        }
    }
}

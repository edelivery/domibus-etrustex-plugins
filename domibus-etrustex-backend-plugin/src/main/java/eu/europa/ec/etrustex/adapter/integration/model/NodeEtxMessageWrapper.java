package eu.europa.ec.etrustex.adapter.integration.model;

import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;

/**
 * @author: micleva
 * @date: 6/21/13 10:14 AM
 * @project: ETX
 */
public class NodeEtxMessageWrapper {
    private EtxMessage etxMessage;
    private String senderUuid;
    private String receiverUuid;

    public EtxMessage getEtxMessage() {
        return etxMessage;
    }

    public void setEtxMessage(EtxMessage etxMessage) {
        this.etxMessage = etxMessage;
    }

    public String getSenderUuid() {
        return senderUuid;
    }

    public void setSenderUuid(String senderUuid) {
        this.senderUuid = senderUuid;
    }

    public String getReceiverUuid() {
        return receiverUuid;
    }

    public void setReceiverUuid(String receiverUuid) {
        this.receiverUuid = receiverUuid;
    }
}

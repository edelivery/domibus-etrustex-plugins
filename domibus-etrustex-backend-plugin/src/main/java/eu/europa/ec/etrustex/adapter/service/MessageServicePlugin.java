package eu.europa.ec.etrustex.adapter.service;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.message.*;
import eu.europa.ec.etrustex.adapter.model.vo.MessageReferenceVO;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;

import java.util.List;

/**
 * A service to access and modify {@link EtxMessage}
 *
 * @author François Gautier
 * @version 1.0
 * @since 12-Oct-17
 */
public interface MessageServicePlugin {

    /**
     * Finds an {@link EtxMessage} by its numeric ID (primary key).
     *
     * @param id an ID
     * @return the persistent {@link EtxMessage} with the given ID, or null, if not found
     */
    EtxMessage findById(Long id);

    void createMessage(EtxMessage messageEntity);

    /**
     * Service to merge the updates to an {@link EtxMessage} entity object to the DB.
     * A detached entity instance is joined with the entity manager for the update.
     * Service has no specific transaction management.
     *
     * @param messageEntity message to update
     */
    void updateMessage(EtxMessage messageEntity);

    EtxMessage createMessageStatusFor(EtxMessage messageBundle, MessageReferenceState messageReferenceState);

    /**
     * Retrieve a given message by UUID, type, sender and receiver.
     * If successfully retrieved, the message is tagged as read (NotificationState = NOTIFIED)
     *
     * @param senderPartyUuid   UUID of the sender {@link EtxParty}
     * @param receiverPartyUuid UUID of the receiver {@link EtxParty}
     * @param messageUuid       UUID of the {@link EtxMessage}
     * @param messageTypes      list of {@link MessageType} to read
     * @return the {@link EtxMessage} read
     */
    EtxMessage readMessage(String senderPartyUuid, String receiverPartyUuid, String messageUuid, List<MessageType> messageTypes);

    /**
     * Retrieve all non read {@link MessageReferenceVO} of a {@link EtxParty}
     * <p>
     * A read {@link eu.europa.ec.etrustex.adapter.model.entity.message.MessageState#MS_PROCESSED} has a
     * {@link NotificationState#NF_NOTIFIED})
     *
     * @param partyUuid UUID of the {@link EtxParty}
     * @return list of non read {@link MessageReferenceVO} for a given party
     */
    List<MessageReferenceVO> findMessagesForReceiverPartyUuid(String partyUuid);

    /**
     * Retrieve the type of message for a given {@link EtxMessage}
     *
     * @param messageEntity {@link EtxMessage}
     * @return the {@link MessageType} of the {@link EtxMessage}
     */
    MessageType referenceMessageTypeFor(EtxMessage messageEntity);

    /**
     * To retrieve the entry from ETX_ADT_MESSAGE with a matching Domibus message Id
     *
     * @param domibusMessageID
     * @return EtxMessage
     * @throws eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException
     */
    EtxMessage findMessageByDomibusMessageId(String domibusMessageID);

    /**
     * Using an {@link ETrustExAdapterDTO} as input, looks up an {@link EtxMessage} having the same AS4 Message Id as contained in the {@link ETrustExAdapterDTO}.<br/>
     * If a record is found, return the {@link EtxMessage} else create an {@link EtxMessage} using the metadata
     * provided in the AS4 message (viz. OriginalSender, FinalRecipient)<br/>
     * Note: For creation of the {@link EtxMessage} the OriginalSender & FinalRecipient should be known as parties within the plugin.
     *
     * @param eTrustExAdapterDTO
     * @return the {@link EtxMessage} retrieved or created newly
     */
    EtxMessage findOrCreateMessageByDomibusMessageId(ETrustExAdapterDTO eTrustExAdapterDTO);
}

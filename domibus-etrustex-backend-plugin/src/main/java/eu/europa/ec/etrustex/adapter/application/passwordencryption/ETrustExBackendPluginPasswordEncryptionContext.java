package eu.europa.ec.etrustex.adapter.application.passwordencryption;

import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.services.PasswordEncryptionExtService;
import eu.domibus.ext.services.PluginPasswordEncryptionContext;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Lazy;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author François Gautier
 * @version 1.0
 * @since 21/11/2019
 */
public class ETrustExBackendPluginPasswordEncryptionContext implements PluginPasswordEncryptionContext {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ETrustExBackendPluginPasswordEncryptionContext.class);

    protected ETrustExBackendPluginProperties eTrustExBackendPluginProperties;

    private String configLocation;

    private PasswordEncryptionExtService pluginPasswordEncryptionService;

    protected DomainDTO domain;

    ETrustExBackendPluginPasswordEncryptionContext(@Lazy ETrustExBackendPluginProperties eTrustExBackendPluginProperties,
                                                   PasswordEncryptionExtService pluginPasswordEncryptionService,
                                                   String configLocation,
                                                   DomainDTO domain) {
        this.eTrustExBackendPluginProperties = eTrustExBackendPluginProperties;
        this.configLocation = configLocation;
        this.pluginPasswordEncryptionService = pluginPasswordEncryptionService;
        this.domain = domain;
    }

    @Override
    public DomainDTO getDomain() {
        return domain;
    }

    @Override
    public String getProperty(String propertyName) {
        return eTrustExBackendPluginProperties.getProperty(propertyName);
    }

    @Override
    public File getConfigurationFile() {
        final File configurationFile = new File(configLocation + File.separator + "plugins/config/domibus-etrustex-backend-plugin.properties");
        LOG.debug("eTrustEx Backend Plugin: Using configuration file [{}]", configurationFile);
        return configurationFile;
    }

    @Override
    public List<String> getPropertiesToEncrypt() {
        final String propertiesToEncryptString = eTrustExBackendPluginProperties.getEncryptedProperties();

        if (StringUtils.isEmpty(propertiesToEncryptString)) {
            LOG.info("eTrustEx Backend Plugin: No properties to encrypt");
            return new ArrayList<>();
        }
        final String[] propertiesToEncrypt = StringUtils.split(propertiesToEncryptString, ",");
        List<String> properties = Arrays.asList(propertiesToEncrypt);
        LOG.debug("eTrustEx Backend Plugin: The following file properties are configured for encryption [{}]", properties);

        List<String> result = properties
                .stream()
                .map(StringUtils::trim)
                .filter(propertyName -> {
                    final String propertyValue = getProperty(propertyName);
                    if (StringUtils.isBlank(propertyValue)) {
                        LOG.info("Property [{}] has empty value", propertyName);
                        return false;
                    }

                    if (!pluginPasswordEncryptionService.isValueEncrypted(propertyValue)) {
                        LOG.debug("Property [{}] is not encrypted", propertyName);
                        return true;
                    }
                    LOG.info("Property [{}] is already encrypted", propertyName);
                    return false;
                }).collect(Collectors.toList());

        LOG.info("eTrustEx Backend Plugin: The following file properties are scheduled for password encryption: [{}]:", result);

        return result;
    }

}

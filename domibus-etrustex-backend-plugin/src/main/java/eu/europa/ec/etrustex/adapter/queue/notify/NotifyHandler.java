package eu.europa.ec.etrustex.adapter.queue.notify;

public interface NotifyHandler {

    void handleNotify(Long messageId);

    void validateNotify(Long messageId);

    void onError(Long messageId);
}
package eu.europa.ec.etrustex.adapter.integration.handlers;

import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseRequest;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.converters.NodeApplicationResponseTypeConverter;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.adapter.service.security.IdentityManager;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_ID_GENERIC;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.extractETrustExXMLPayload;

/**
 * Handles the notification from Domibus of receiving a {@link MessageType#MESSAGE_STATUS}
 * <p>
 * It implements {@link OperationHandler}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 30/10/2017
 */
@Service("etxBackendPluginInboxMessageStatusTransmissionHandler")
public class InboxMessageStatusTransmissionBackendHandler implements OperationHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxMessageStatusTransmissionBackendHandler.class);

    @Autowired
    private IdentityManager identityManager;

    @Autowired
    private MessageServicePlugin messageService;

    @Autowired
    private PostProcessProducer postProcessProducer;

    /**
     * Handles the notification from Domibus of receiving a {@link MessageType#MESSAGE_STATUS}
     * Receiver and sender parties are validated and loaded from DB.
     * <p>
     * {@link EtxMessage} is obtained from {@link ETrustExAdapterDTO} object and then created in DB
     * It throws and {@link ETrustExPluginException}
     *
     * @param adapterDTO instance of {@link ETrustExAdapterDTO} received through Domibus
     * @return true in case of success
     */
    @Override
    public boolean handle(ETrustExAdapterDTO adapterDTO) {
        LOG.debug("handle for StatusMessage transmission -> start");

        final String domibusMsgId = adapterDTO.getAs4MessageId();

        //retrieve sender and receiver party
        final String senderPartyUuid = adapterDTO.getAs4OriginalSender();
        final String receiverPartyUuid = adapterDTO.getAs4FinalRecipient();
        final EtxParty senderParty = retrieveParty(senderPartyUuid, domibusMsgId);
        final EtxParty receiverParty = retrieveParty(receiverPartyUuid, domibusMsgId);

        LOG.info("handling StatusMessage transmission at Backend from senderParty: {} to receiverParty: {}",
                senderPartyUuid,
                receiverPartyUuid);

        //extract the message from adapter DTO
        final EtxMessage etxMessage = getMessage(adapterDTO, senderParty, receiverParty);

        //write to DB
        messageService.createMessage(etxMessage);

        //trigger post process
        postProcessProducer.triggerPostProcess(etxMessage.getId());

        LOG.debug("handle for StatusMessage transmission -> end");
        return true;
    }

    /**
     * Retrieves party from DB
     *
     * @param partyUuid    Uuid of the party
     * @param domibusMsgId Domibus message id
     * @return {@link EtxParty} from DB
     */
    EtxParty retrieveParty(final String partyUuid, final String domibusMsgId) {
        if (StringUtils.isBlank(partyUuid)) {
            throw new IllegalArgumentException("partyUuid is null or empty string");
        }

        //load from DB
        EtxParty party = identityManager.searchPartyByUuid(partyUuid);

        if (party == null) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "eTrustEx party: " + partyUuid + " communicated in Domibus message ID: "
                    + domibusMsgId + "] is not know at Backend database.");
        }

        return party;
    }

    /**
     * Extracts an {@link EtxMessage} from {@link ETrustExAdapterDTO}
     * <p>
     * Throws {@link ETrustExPluginException}
     *
     * @param adapterDTO    instance of {@link ETrustExAdapterDTO}
     * @param senderParty   sender party
     * @param receiverParty receiver party
     * @return instance of {@link EtxMessage}
     */
    EtxMessage getMessage(ETrustExAdapterDTO adapterDTO, EtxParty senderParty, EtxParty receiverParty) {
        EtxMessage etxMessage = null;

        SubmitApplicationResponseRequest submitApplicationResponseRequest = extractETrustExXMLPayload(CONTENT_ID_GENERIC, adapterDTO);

        if (submitApplicationResponseRequest != null && submitApplicationResponseRequest.getApplicationResponse() != null) {
            etxMessage = NodeApplicationResponseTypeConverter.toEtxMessage(submitApplicationResponseRequest.getApplicationResponse());

            if (etxMessage != null) {
                etxMessage.setMessageType(MessageType.MESSAGE_STATUS);
                etxMessage.setMessageState(MessageState.MS_CREATED);
                etxMessage.setDirectionType(MessageDirection.IN);
                etxMessage.setSender(senderParty);
                etxMessage.setReceiver(receiverParty);
                etxMessage.setDomibusMessageId(adapterDTO.getAs4MessageId());
            } else {
                final String errorMsg = "Not able to extract a Message Status from Domibus request having messageId: "
                        + adapterDTO.getAs4MessageId();
                LOG.error(errorMsg);
                throw new ETrustExPluginException(ETrustExError.ETX_001, errorMsg);
            }
        }

        return etxMessage;
    }
}

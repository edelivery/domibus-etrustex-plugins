package eu.europa.ec.etrustex.adapter.queue.error;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPlugin;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPluginService;
import eu.europa.ec.etrustex.adapter.queue.AbstractQueueListener;
import eu.europa.ec.etrustex.adapter.queue.deadletter.DeadLetterProducer;
import eu.europa.ec.etrustex.adapter.queue.inbox.uploadattachment.InboxUploadAttachmentHandler;
import eu.europa.ec.etrustex.adapter.queue.inbox.uploadattachment.InboxUploadAttachmentQueueMessage;
import eu.europa.ec.etrustex.adapter.queue.notify.NotifyHandler;
import eu.europa.ec.etrustex.adapter.queue.notify.NotifyQueueMessage;
import eu.europa.ec.etrustex.adapter.queue.out.downloadattachment.OutDownloadAttachmentHandler;
import eu.europa.ec.etrustex.adapter.queue.out.downloadattachment.OutDownloadAttachmentQueueMessage;
import eu.europa.ec.etrustex.adapter.queue.out.sendmessage.SendMessageHandler;
import eu.europa.ec.etrustex.adapter.queue.out.sendmessage.SendMessageQueueMessage;
import eu.europa.ec.etrustex.adapter.queue.out.uploadattachment.OutUploadAttachmentHandler;
import eu.europa.ec.etrustex.adapter.queue.out.uploadattachment.OutUploadAttachmentQueueMessage;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessHandler;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessQueueMessage;
import eu.europa.ec.etrustex.adapter.queue.postupload.PostUploadHandler;
import eu.europa.ec.etrustex.adapter.queue.postupload.PostUploadQueueMessage;
import eu.europa.ec.etrustex.common.jms.ErrorQueueMessage;
import eu.europa.ec.etrustex.common.jms.QueueMessage;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeys;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import org.springframework.stereotype.Component;

import static eu.domibus.logging.DomibusLogger.MDC_MESSAGE_ID;
import static eu.europa.ec.etrustex.common.util.LoggingUtils.*;


/**
 * Distribute to the right error handler depending on the queueMessage class
 *
 * @author François GAUTIER
 * @version 1.0
 * @since 11/10/2017
 */
@Component("etxBackendPluginErrorConsumer")
public class ErrorConsumer extends AbstractQueueListener<QueueMessage> {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ErrorConsumer.class);

    private final DeadLetterProducer deadLetterProducer;

    private final InboxUploadAttachmentHandler inboxUploadAttachmentHandler;

    private final NotifyHandler notifyHandler;

    private final OutDownloadAttachmentHandler outDownloadAttachmentHandler;

    private final OutUploadAttachmentHandler outUploadAttachmentHandler;

    private final SendMessageHandler sendMessageHandler;

    private final PostProcessHandler postProcessHandler;

    private final PostUploadHandler postUploadHandler;
    private final ErrorHandler errorHandler;

    private ClearEtxLogKeysService clearEtxLogKeysService;
    private DomainBackEndPluginService domainBackEndPluginService;

    public ErrorConsumer(DeadLetterProducer deadLetterProducer,
                         InboxUploadAttachmentHandler inboxUploadAttachmentHandler,
                         NotifyHandler notifyHandler,
                         OutDownloadAttachmentHandler outDownloadAttachmentHandler,
                         OutUploadAttachmentHandler outUploadAttachmentHandler,
                         SendMessageHandler sendMessageHandler,
                         PostProcessHandler postProcessHandler,
                         PostUploadHandler postUploadHandler,
                         ErrorHandler errorHandler,
                         ClearEtxLogKeysService clearEtxLogKeysService,
                         DomainBackEndPluginService domainBackEndPluginService) {
        this.deadLetterProducer = deadLetterProducer;
        this.inboxUploadAttachmentHandler = inboxUploadAttachmentHandler;
        this.notifyHandler = notifyHandler;
        this.outDownloadAttachmentHandler = outDownloadAttachmentHandler;
        this.outUploadAttachmentHandler = outUploadAttachmentHandler;
        this.sendMessageHandler = sendMessageHandler;
        this.postProcessHandler = postProcessHandler;
        this.postUploadHandler = postUploadHandler;
        this.errorHandler = errorHandler;
        this.clearEtxLogKeysService = clearEtxLogKeysService;
        this.domainBackEndPluginService = domainBackEndPluginService;
    }

    @Override
    @DomainBackEndPlugin
    @ClearEtxLogKeys (clearCustomKeys = {MDC_ETX_MESSAGE_UUID, MDC_ETX_ATT_UUID, MDC_ETX_CONVERSATION_ID, MDC_MESSAGE_ID})
    public void handleMessage(QueueMessage queueMessage) {
        domainBackEndPluginService.setBackendPluginDomain();
        LOG.warn("Handling error message for: {}", queueMessage);

        if (queueMessage instanceof InboxUploadAttachmentQueueMessage) {
            Long attachmentId = ((InboxUploadAttachmentQueueMessage) queueMessage).getAttachmentId();
            inboxUploadAttachmentHandler.onError(attachmentId);
        } else if (queueMessage instanceof NotifyQueueMessage) {
            // Not be used by default as per configuration (see: EDELIVERY-4665)
            Long messageId = ((NotifyQueueMessage) queueMessage).getMessageId();
            notifyHandler.onError(messageId);
        } else if (queueMessage instanceof OutDownloadAttachmentQueueMessage) {
            Long attachmentId = ((OutDownloadAttachmentQueueMessage) queueMessage).getAttachmentId();
            outDownloadAttachmentHandler.onError(attachmentId);
        } else if (queueMessage instanceof OutUploadAttachmentQueueMessage) {
            Long attachmentId = ((OutUploadAttachmentQueueMessage) queueMessage).getAttachmentId();
            outUploadAttachmentHandler.onError(attachmentId);
        } else if (queueMessage instanceof SendMessageQueueMessage) {
            Long messageId = ((SendMessageQueueMessage) queueMessage).getMessageId();
            sendMessageHandler.onError(messageId);
        } else if (queueMessage instanceof PostUploadQueueMessage) {
            Long messageId = ((PostUploadQueueMessage) queueMessage).getMessageId();
            postUploadHandler.onError(messageId);
        } else if (queueMessage instanceof PostProcessQueueMessage) {
            Long messageId = ((PostProcessQueueMessage) queueMessage).getMessageId();
            postProcessHandler.onError(messageId);
        } else if (queueMessage instanceof ErrorQueueMessage) {
            String id = ((ErrorQueueMessage) queueMessage).getId();
            errorHandler.onError(id);
        } else {
            LOG.error("Unexpected QueueMessage type: {}", queueMessage);
            deadLetterProducer.triggerDeadLetter(queueMessage);
        }
        clearEtxLogKeysService.clearKeys(MDC_MESSAGE_ID);
    }

    @Override
    public void validateMessage(QueueMessage object) {
        //nothing to validate
    }
}

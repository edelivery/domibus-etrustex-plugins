package eu.europa.ec.etrustex.adapter.queue.out.uploadattachment;

import eu.europa.ec.etrustex.adapter.queue.AbstractQueueListener;
import org.springframework.stereotype.Component;

/**
 * @author micleva
 * @project ETX
 */
@Component("etxBackendPluginOutUploadAttachmentConsumer")
public class OutUploadAttachmentConsumer extends AbstractQueueListener<OutUploadAttachmentQueueMessage> {

    private final OutUploadAttachmentHandler handler;

    public OutUploadAttachmentConsumer(OutUploadAttachmentHandler handler) {
        this.handler = handler;
    }

    @Override
    public void handleMessage(OutUploadAttachmentQueueMessage queueMessage) {
        handler.handleUploadAttachment(queueMessage.getAttachmentId());
    }

    @Override
    public void validateMessage(OutUploadAttachmentQueueMessage queueMessage) {
        handler.validateUploadAttachment(queueMessage.getAttachmentId());
    }
}

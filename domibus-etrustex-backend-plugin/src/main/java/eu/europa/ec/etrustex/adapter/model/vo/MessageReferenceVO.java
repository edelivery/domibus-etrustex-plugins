package eu.europa.ec.etrustex.adapter.model.vo;

import eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @author micleva
 * @project ETX
 */

public class MessageReferenceVO {

    private final String messageUuid;
    private final MessageType messageType;
    private final String senderUuid;
    private final String receiverUuid;
    private final String messageRefUUID;
    private final MessageState messageState;
    private final MessageDirection messageDirection;

    public MessageReferenceVO(String messageUuid, MessageType messageType, String senderUuid, String receiverUuid) {
        this.messageUuid = messageUuid;
        this.messageType = messageType;
        this.senderUuid = senderUuid;
        this.receiverUuid = receiverUuid;
        this.messageRefUUID = null;
        this.messageState = null;
        this.messageDirection = null;
    }

    public MessageReferenceVO(String messageUuid, MessageType messageType, String senderUuid, String receiverUuid, String messageRefUUID, MessageState messageState, MessageDirection messageDirection) {
        this.messageUuid = messageUuid;
        this.messageType = messageType;
        this.senderUuid = senderUuid;
        this.receiverUuid = receiverUuid;
        this.messageRefUUID = messageRefUUID;
        this.messageState = messageState;
        this.messageDirection = messageDirection;
    }

    public String getMessageUuid() {
        return messageUuid;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public String getSenderUuid() {
        return senderUuid;
    }

    public String getReceiverUuid() {
        return receiverUuid;
    }

    public String getMessageRefUUID() {
        return messageRefUUID;
    }

    public MessageState getMessageState() {
        return messageState;
    }

    public MessageDirection getMessageDirection() {
        return messageDirection;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof MessageReferenceVO)) return false;

        MessageReferenceVO that = (MessageReferenceVO) o;

        return new EqualsBuilder()
                .append(messageUuid, that.messageUuid)
                .append(messageType, that.messageType)
                .append(senderUuid, that.senderUuid)
                .append(receiverUuid, that.receiverUuid)
                .append(messageRefUUID, that.messageRefUUID)
                .append(messageState, that.messageState)
                .append(messageDirection, that.messageDirection)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(messageUuid)
                .append(messageType)
                .append(senderUuid)
                .append(receiverUuid)
                .append(messageRefUUID)
                .append(messageState)
                .append(messageDirection)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("messageUuid", messageUuid)
                .append("messageType", messageType)
                .append("senderUuid", senderUuid)
                .append("receiverUuid", receiverUuid)
                .append("messageRefUUID", messageRefUUID)
                .append("messageState", messageState)
                .append("messageDirection", messageDirection)
                .toString();
    }
}

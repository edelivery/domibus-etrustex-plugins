package eu.europa.ec.etrustex.adapter.queue;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPluginService;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import org.springframework.stereotype.Service;
import org.springframework.util.ErrorHandler;

/**
 * @author Arun Raj
 * @since 1.3.1
 */
@Service("etxBackendPluginJMSErrorHandler")
public class ETrustExBackendPluginJMSErrorHandler implements ErrorHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ETrustExBackendPluginJMSErrorHandler.class);

    private ClearEtxLogKeysService clearEtxLogKeysService;
    private DomainBackEndPluginService domainBackEndPluginService;
    private ETrustExBackendPluginJMSErrorUtils eTrustExBackendPluginJMSErrorUtils;

    public ETrustExBackendPluginJMSErrorHandler(ClearEtxLogKeysService clearEtxLogKeysService, DomainBackEndPluginService domainBackEndPluginService, ETrustExBackendPluginJMSErrorUtils eTrustExBackendPluginJMSErrorUtils) {
        this.clearEtxLogKeysService = clearEtxLogKeysService;
        this.domainBackEndPluginService = domainBackEndPluginService;
        this.eTrustExBackendPluginJMSErrorUtils = eTrustExBackendPluginJMSErrorUtils;
    }

    @Override
    public void handleError(Throwable t) {
        domainBackEndPluginService.setBackendPluginDomain();
        if(!eTrustExBackendPluginJMSErrorUtils.handleOutDownloadAttachmentHandlerFileNotFoundErrorLogging(t)){
            LOG.warn("Encountered JMS error in Backend Plugin:", t);
        }
        clearEtxLogKeysService.clearKeys();
    }
}

package eu.europa.ec.etrustex.adapter.model.entity;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
public abstract class AbstractEntity<T extends Serializable> {

    public abstract T getId();

    public abstract void setId(T id);
}

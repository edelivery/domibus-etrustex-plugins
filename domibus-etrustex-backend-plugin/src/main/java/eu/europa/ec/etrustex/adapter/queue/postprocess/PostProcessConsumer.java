package eu.europa.ec.etrustex.adapter.queue.postprocess;

import eu.europa.ec.etrustex.adapter.queue.AbstractQueueListener;
import org.springframework.stereotype.Component;

/**
 * @author micleva
 * @project ETX
 */
@Component("etxBackendPluginPostProcessConsumer")
public class PostProcessConsumer extends AbstractQueueListener<PostProcessQueueMessage> {

    private final PostProcessHandler handler;

    public PostProcessConsumer(PostProcessHandler handler) {
        this.handler = handler;
    }

    @Override
    public void handleMessage(PostProcessQueueMessage queueMessage) {
        handler.handlePostProcess(queueMessage.getMessageId());
    }

    @Override
    public void validateMessage(PostProcessQueueMessage queueMessage) {
        handler.validatePostProcess(queueMessage.getMessageId(), queueMessage.getAttachmentId());
    }
}

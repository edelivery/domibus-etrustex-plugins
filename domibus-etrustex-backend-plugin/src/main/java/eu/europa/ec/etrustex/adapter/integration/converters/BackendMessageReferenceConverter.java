package eu.europa.ec.etrustex.adapter.integration.converters;

import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.model.vo.MessageReferenceVO;
import eu.europa.ec.etrustex.integration.model.common.v2.EtxMessageCodeType;
import eu.europa.ec.etrustex.integration.model.common.v2.EtxMessageReferenceType;

/**
 * @author: micleva
 * @date: 6/19/12 4:26 PM
 * @project: ETX
 */
public class BackendMessageReferenceConverter {

    private BackendMessageReferenceConverter() {
    }

    public static EtxMessageReferenceType convertEtxMessage(EtxMessage message) {

        MessageReferenceVO messageReferenceVO = new MessageReferenceVO(message.getMessageUuid(),
                message.getMessageType(), message.getSender().getPartyUuid(), message.getReceiver().getPartyUuid(), message.getMessageReferenceUuid(), message.getMessageState(), message.getDirectionType() );

        return convertEtxMessage(messageReferenceVO);
    }

    public static EtxMessageReferenceType convertEtxMessage(MessageReferenceVO message) {
        EtxMessageReferenceType messageReferenceType = new EtxMessageReferenceType();

        messageReferenceType.setId(message.getMessageUuid());
        messageReferenceType.setSender(RecipientTypeConverter.convertEtxParty(message.getSenderUuid()));
        messageReferenceType.setReceiver(RecipientTypeConverter.convertEtxParty(message.getReceiverUuid()));
        if (message.getMessageType() == MessageType.MESSAGE_ADAPTER_STATUS) {
            messageReferenceType.setType(EtxMessageCodeType.MESSAGE_STATUS);
        } else {
            messageReferenceType.setType(EtxMessageCodeType.fromValue(message.getMessageType().name()));
        }

        return messageReferenceType;
    }
}

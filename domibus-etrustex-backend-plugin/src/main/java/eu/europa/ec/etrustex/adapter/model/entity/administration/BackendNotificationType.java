package eu.europa.ec.etrustex.adapter.model.entity.administration;

/**
 * @author micleva
 * @project ETX
 */

public enum BackendNotificationType {

    WITNESS_FILE,

    WEB_SERVICE;

    public static BackendNotificationType fromCode(String code) {
        if (code != null) {
            for (BackendNotificationType backendNotificationType : BackendNotificationType.values()) {
                if (backendNotificationType.name().equals(code)) {
                    return backendNotificationType;
                }
            }
        }
        return null;
    }
}
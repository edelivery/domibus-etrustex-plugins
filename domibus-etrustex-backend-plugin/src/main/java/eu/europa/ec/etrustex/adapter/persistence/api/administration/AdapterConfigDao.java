package eu.europa.ec.etrustex.adapter.persistence.api.administration;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxAdapterConfig;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBase;

import java.util.List;

public interface AdapterConfigDao extends DaoBase<EtxAdapterConfig> {

    /**
     * find a list of adapter configurations
     *
     * @return the list of all {@link EtxAdapterConfig}
     */
    List<EtxAdapterConfig> findAdapterConfigs();
}

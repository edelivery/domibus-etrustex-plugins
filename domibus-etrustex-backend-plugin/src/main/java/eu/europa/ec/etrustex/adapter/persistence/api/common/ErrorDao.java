package eu.europa.ec.etrustex.adapter.persistence.api.common;

import eu.europa.ec.etrustex.adapter.model.entity.common.EtxError;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBase;

public interface ErrorDao extends DaoBase<EtxError> {

}

package eu.europa.ec.etrustex.adapter.model.dto;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;

public class IcaDTO {

    private String errorDetails;
    private EtxParty party;

    public IcaDTO(EtxParty party) {
        this.party = party;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(String errorDetails) {
        this.errorDetails = errorDetails;
    }

    public EtxParty getParty() {
        return party;
    }

    public void setParty(EtxParty party) {
        this.party = party;
    }
}

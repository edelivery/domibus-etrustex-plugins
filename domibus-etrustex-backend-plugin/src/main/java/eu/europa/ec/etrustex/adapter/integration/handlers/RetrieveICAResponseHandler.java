package eu.europa.ec.etrustex.adapter.integration.handlers;

import ec.schema.xsd.commonaggregatecomponents_2.InterchangeAgreementType;
import ec.schema.xsd.retrieveinterchangeagreementsresponse_2.RetrieveInterchangeAgreementsResponseType;
import ec.services.wsdl.retrieveinterchangeagreementsrequest_2.FaultResponse;
import ec.services.wsdl.retrieveinterchangeagreementsrequest_2.SubmitRetrieveInterchangeAgreementsRequestResponse;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.service.IcaService;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.exceptions.EtrustExMarker;
import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static eu.europa.ec.etrustex.adapter.integration.converters.FaultTypeConverter.getFaultInfo;
import static eu.europa.ec.etrustex.common.converters.PartyTypeConverter.getUuid;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.extractETrustExXMLPayload;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.extractETrustExXMLPayloadFaultResponse;

/**
 * @author Federico Martini
 * @version 1.0
 * @since 19/06/2017
 */
@Service("etxBackendPluginRetrieveICAResponseHandler")
public class RetrieveICAResponseHandler
        extends BackendResponseHandlerBase<SubmitRetrieveInterchangeAgreementsRequestResponse, String, FaultResponse>
        implements OperationHandler {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(RetrieveICAResponseHandler.class);

    @Autowired
    private IcaService icaService;

    @Override
    public boolean handle(ETrustExAdapterDTO eTrustExAdapterResponseDTO) {
        LOG.info("Reached RetrieveICAResponseHandler");

        final String etxBackendMsgId = eTrustExAdapterResponseDTO.getAs4ConversationId();

        SubmitRetrieveInterchangeAgreementsRequestResponse response = extractETrustExXMLPayload(ContentId.CONTENT_ID_GENERIC, eTrustExAdapterResponseDTO);
        FaultResponse faultResponse = extractETrustExXMLPayloadFaultResponse(eTrustExAdapterResponseDTO, new FaultResponse("Default FaultResponse", getFaultInfo()));
        if (response == null && faultResponse == null) {
            throw new ETrustExPluginException(ETrustExError.ETX_002, "Either SubmitRetrieveInterchangeAgreementsRequestResponse or FaultResponse should be provided!");
        }

        processNodeResponse(faultResponse, response, etxBackendMsgId);
        return true;
    }

    @Override
    protected void processNodeFault(FaultResponse faultResponse, String identifier) {
        String errorDescription = faultResponse.getMessage();
        String faultResponseCode = getNodeFaultResponseCode(faultResponse.getFaultInfo());
        String errorLog = String.format("SubmitRetrieveInterchangeAgreement returned the fault response code: %s; error details: %s", faultResponseCode, errorDescription);
        LOG.error(EtrustExMarker.NON_RECOVERABLE, errorLog);
    }

    @Override
    protected void processNodeSuccess(SubmitRetrieveInterchangeAgreementsRequestResponse response, String identifier) {
        RetrieveInterchangeAgreementsResponseType retrieveResponse = response.getRetrieveInterchangeAgreementsResponse();

        for (InterchangeAgreementType interchangeAgreementType : retrieveResponse.getInterchangeAgreement()) {
            icaService.createIca(
                    getUuid(interchangeAgreementType.getSenderParty()),
                    getUuid(interchangeAgreementType.getReceiverParty()),
                    interchangeAgreementType);
            LOG.info("Processed SubmitRetrieveInterchangeAgreementsRequestResponse received from ETrustEx Node plugin about sender [{}] and backend message uuid [{}]",
                    getUuid(interchangeAgreementType.getSenderParty()),
                    identifier);
        }
        if (retrieveResponse.getInterchangeAgreement().isEmpty()) {
            LOG.warn("Received an empty SubmitRetrieveInterchangeAgreementsRequestResponse response from ETrustEx Node plugin about backend message uuid [{}]", identifier);
        }
    }

}

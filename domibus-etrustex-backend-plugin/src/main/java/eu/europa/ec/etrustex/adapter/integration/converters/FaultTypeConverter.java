package eu.europa.ec.etrustex.adapter.integration.converters;

import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseCodeType;
import oasis.names.specification.ubl.schema.xsd.fault_1.FaultType;

public class FaultTypeConverter {

    private static final String UNKNOWN_ERROR = "Unknown Error";

    private FaultTypeConverter() {
        //do nothing
    }

    public static FaultType getFaultInfo() {
        FaultType faultType = new FaultType();
        ResponseCodeType value = new ResponseCodeType();
        value.setValue(ETrustExError.DEFAULT.getCode());
        DescriptionType e = new DescriptionType();
        e.setValue(UNKNOWN_ERROR);
        faultType.getDescription().add(e);
        faultType.setResponseCode(value);
        return faultType;
    }

}

package eu.europa.ec.etrustex.adapter.integration.client.backend.ws.adapter;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.integration.client.backend.ws.BackendWebServiceBase;
import eu.europa.ec.etrustex.adapter.integration.converters.BackendMessageBundleTypeConverter;
import eu.europa.ec.etrustex.adapter.integration.converters.BackendMessageStatusTypeConverter;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.integration.model.common.v2.EtxMessageBundleType;
import eu.europa.ec.etrustex.integration.model.common.v2.EtxMessageStatusType;
import eu.europa.ec.etrustex.integration.service.notification.v2.FaultResponse;
import eu.europa.ec.etrustex.integration.service.notification.v2.InboxNotificationPortType;
import eu.europa.ec.etrustex.integration.service.notification.v2.OnMessageBundleRequestType;
import eu.europa.ec.etrustex.integration.service.notification.v2.OnMessageStatusRequestType;

import javax.xml.ws.BindingProvider;

/**
 * @author Arun Raj
 * @version 1.0
 * @author: micleva
 * @date: 6/26/12 2:42 PM
 * @project: ETX
 */
public class InboxNotificationServiceAdapterImpl extends BackendWebServiceBase implements InboxNotificationServiceAdapter {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(InboxNotificationServiceAdapterImpl.class);

    public InboxNotificationServiceAdapterImpl(BackendWebServiceProvider webServiceProvider) {
        super(webServiceProvider);
    }

    @Override
    public void notifyMessageBundle(EtxMessage messageBundle) {
        LOG.debug("notifyMessageBundle start ...");

        // Prepare WS connection
        InboxNotificationPortType port = buildInboxNotificationPort();

        // Create on message bundle request
        OnMessageBundleRequestType onMessageBundleRequest = new OnMessageBundleRequestType();
        EtxMessageBundleType messageBundleSdo = BackendMessageBundleTypeConverter.fromEtxMessage(messageBundle);
        onMessageBundleRequest.setMessageBundle(messageBundleSdo);

        try {
            // Invoke web service
            port.onMessageBundle(onMessageBundleRequest);
            LOG.info("BACKEND.InboxNotificationService.onMessageBundle ID: [{}]", messageBundle.getId());
        } catch (FaultResponse e) {
            throw new ETrustExPluginException(ETrustExError.ETX_001, e.getMessage(), e);
        } catch (Exception e) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, e.getMessage(), e);
        }
        LOG.debug("notifyMessageBundle done.");
    }

    @Override
    public void notifyMessageStatus(EtxMessage messageStatus, MessageType refMessageType) {
        LOG.debug("notifyMessageStatus start ...");

        // Prepare WS connection
        InboxNotificationPortType port = buildInboxNotificationPort();

        // Create on message status request
        OnMessageStatusRequestType onMessageStatusRequest = new OnMessageStatusRequestType();
        EtxMessageStatusType messageStatusSdo = BackendMessageStatusTypeConverter.convertEtxMessage(messageStatus, refMessageType);
        onMessageStatusRequest.setMessageStatus(messageStatusSdo);

        try {
            // Invoke web service
            port.onMessageStatus(onMessageStatusRequest);
            LOG.info("BACKEND.InboxNotificationService.onMessageStatus ID: [{}]", messageStatus.getId());
        } catch (FaultResponse e) {
            throw new ETrustExPluginException(ETrustExError.ETX_001, e.getMessage(), e);
        } catch (Exception e) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, e.getMessage(), e);
        }
        LOG.debug("notifyMessageStatus done.");
    }

    private InboxNotificationPortType buildInboxNotificationPort() {
        InboxNotificationPortType port = (InboxNotificationPortType) webServiceProvider.buildJaxWsProxy(InboxNotificationPortType.class,
                false, serviceEndpoint);

        BindingProvider bindingProvider = (BindingProvider) port;
        webServiceProvider.setupConnectionCredentials(bindingProvider, userName, password.getBytes());

        return port;
    }
}

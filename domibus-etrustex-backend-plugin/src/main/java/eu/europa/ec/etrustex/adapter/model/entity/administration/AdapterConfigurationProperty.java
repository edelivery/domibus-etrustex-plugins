package eu.europa.ec.etrustex.adapter.model.entity.administration;

/**
 * @author micleva
 * @project ETX
 */

public enum AdapterConfigurationProperty {

    //Node related configurations
    ETX_NODE_RETENTION_POLICY_WEEKS("etx.node.attachment.retentionPolicy.weeks"),
    ETX_NODE_SECURITY_P2P_ENABLED("etx.node.security.p2p.enabled"),

    //the following configurations are restricted to Adapter
    ETX_ADAPTER_REPOSITORY_ROOT_PATH("etx.adapter.repository.root.path"),
    ETX_ADAPTER_WORKSPACE_ROOT_PATH("etx.adapter.workspace.root.path"),
    ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS("etx.adapter.repository.ttl.days"),
    ETX_ADAPTER_NOTIFICATION_EXPIRATION_DAYS("etx.adapter.notification.expiration.days");

    private String code;

    /**
     * constructor
     *
     * @param code
     */
    AdapterConfigurationProperty(String code) {
        this.code = code;
    }

    /**
     * get the enum from the String code
     *
     * @param code
     * @return AdapterConfigurationProperty instance
     */
    public static AdapterConfigurationProperty fromCode(String code) {
        if (code != null) {
            for (AdapterConfigurationProperty configurationProperty : AdapterConfigurationProperty.values()) {
                if (configurationProperty.getCode().equals(code)) {
                    return configurationProperty;
                }
            }
        }

        throw new IllegalArgumentException("No configuration found for the input code: " + code);
    }

    /**
     * return the code for the given enum
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }
}

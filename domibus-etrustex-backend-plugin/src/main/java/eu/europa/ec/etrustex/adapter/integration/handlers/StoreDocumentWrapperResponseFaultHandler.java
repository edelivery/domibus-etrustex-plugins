package eu.europa.ec.etrustex.adapter.integration.handlers;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.queue.postupload.PostUploadProducer;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO.getUuid;

/**
 * Handle Error on the node side after sending a messageStatus and messageBundle
 *
 * @author François GAUTIER
 * @version 1.0
 * @since 22-09-2017
 */
@Service("etxBackendPluginStoreDocumentWrapperResponseFaultHandler")
public class StoreDocumentWrapperResponseFaultHandler extends ResponseFaultHandlerBase {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(StoreDocumentWrapperResponseFaultHandler.class);

    @Autowired
    private PostUploadProducer postUploadProducer;

    @Autowired
    private AttachmentService attachmentService;

    @Override
    protected void process(String payload, ETrustExAdapterDTO eTrustExAdapterResponseDTO) {
        EtxAttachment etxAttachment = attachmentService.findAttachmentByDomibusMessageId(getUuid(eTrustExAdapterResponseDTO));
        process(payload, etxAttachment);
    }

    public void process(String payload, EtxAttachment etxAttachment) {
        LOG.error("Document Wrapper failed: {}", payload);
        attachmentService.attachmentInError(etxAttachment.getId(), ETrustExError.DEFAULT.getCode(), payload);
        postUploadProducer.checkAttachmentsAndTriggerPostUpload(etxAttachment);

    }
}

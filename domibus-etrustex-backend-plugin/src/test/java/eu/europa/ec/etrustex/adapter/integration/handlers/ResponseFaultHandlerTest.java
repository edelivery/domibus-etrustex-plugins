package eu.europa.ec.etrustex.adapter.integration.handlers;

import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.queue.postupload.PostUploadProducer;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;

import static eu.europa.ec.etrustex.adapter.testutils.ETrustExAdapterDTOBuilder.buildETxAdapterDTOResponse;
import static eu.europa.ec.etrustex.adapter.testutils.ResourcesUtils.readResource;
import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_FAULT;
import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_ID_GENERIC;

/**
 * @Author François Gautier
 * @Since 25-Sep-17
 * @Version 1.0
 */
@RunWith(JMockit.class)
public class ResponseFaultHandlerTest {

    @Injectable
    private MessageServicePlugin messageService;
    @Injectable
    private AttachmentService attachmentService;

    @Injectable
    private PostUploadProducer postUploadProducer;
    @Injectable
    private PostProcessProducer postProcessProducer;

    @Tested
    private ResponseFaultHandler faultHandler;

    @Test
    public void testHandle_HappyFlow_msg() {

        //object is build by helper method
        final ETrustExAdapterDTO eTrustExAdapterResponseDTO = buildETxAdapterDTOResponse(CONTENT_FAULT, readResource("data/SubmitDocumentBundleResponse.xml"));

        final Long messageId = 10L;
        final String messageUuid = "message test UUID";
        final EtxMessage etxMessage = getEtxMessage(messageId, messageUuid);

        new Expectations() {{
            messageService.findOrCreateMessageByDomibusMessageId(eTrustExAdapterResponseDTO);
            times = 1;
            result = etxMessage;

        }};

        //method to test
        faultHandler.handle(eTrustExAdapterResponseDTO);

        verifPostProcessOK(messageId);
    }

    @Test
    public void testHandle_HappyFlow_att() {

        //object is build by helper method
        final ETrustExAdapterDTO eTrustExAdapterResponseDTO = buildETxAdapterDTOResponse(CONTENT_FAULT, readResource("data/SubmitDocumentBundleResponse.xml"));
        String att_id = "ATT_ID";
        eTrustExAdapterResponseDTO.setAttachmentId(att_id);

        final Long messageId = 10L;
        final String messageUuid = "message test UUID";
        final EtxMessage etxMessage = getEtxMessage(messageId, messageUuid);
        final Long attId = 1L;
        EtxAttachment etxAttachment = getEtxAttachment(att_id, attId);
        etxMessage.setAttachmentList(Arrays.asList(etxAttachment));

        new Expectations() {{
            messageService.findOrCreateMessageByDomibusMessageId(eTrustExAdapterResponseDTO);
            times = 1;
            result = etxMessage;
        }};

        //method to test
        faultHandler.handle(eTrustExAdapterResponseDTO);

        new FullVerifications() {{
            attachmentService.attachmentInError(attId, ETrustExError.DEFAULT.getCode(), eTrustExAdapterResponseDTO.getPayloadFromCID(ContentId.CONTENT_FAULT).getPayloadAsString());
            times = 1;

            postUploadProducer.checkAttachmentsAndTriggerPostUpload(etxAttachment);
            times = 1;
        }};
    }

    private EtxAttachment getEtxAttachment(String uuid, Long id) {
        EtxAttachment etxAttachment = new EtxAttachment();
        etxAttachment.setAttachmentUuid(uuid);
        etxAttachment.setId(id);
        return etxAttachment;
    }

    private EtxMessage getEtxMessage(Long messageId, String messageUuid) {
        final EtxMessage etxMessage = new EtxMessage();
        etxMessage.setId(messageId);
        etxMessage.setMessageUuid(messageUuid);
        return etxMessage;
    }

    @Test
    public void testHandle_HappyFlow_StackTrace_Too_long() {

        //object is build by helper method
        final ETrustExAdapterDTO eTrustExAdapterResponseDTO = buildETxAdapterDTOResponse(CONTENT_FAULT, readResource("data/stackTrace.st"));

        final Long messageId = 10L;
        final String messageUuid = "message test UUID";
        final EtxMessage etxMessage = getEtxMessage(messageId, messageUuid);

        new Expectations() {{
            messageService.findOrCreateMessageByDomibusMessageId(eTrustExAdapterResponseDTO);
            times = 1;
            result = etxMessage;

        }};

        //method to test
        faultHandler.handle(eTrustExAdapterResponseDTO);

        verifPostProcessOK(messageId);
    }

    private void verifPostProcessOK(final Long messageId) {
        new Verifications() {{
            Long messageIdActual;
            postProcessProducer.triggerPostProcess(messageIdActual = withCapture());
            times = 1;
            Assert.assertEquals("message id must match", messageId, messageIdActual);
        }};
    }

    @Test
    public void testProcessNodeSuccess_ExceptionInvalidNodeResponse() {
        final ETrustExAdapterDTO eTrustExAdapterResponseDTO = buildETxAdapterDTOResponse(CONTENT_ID_GENERIC, readResource("data/SubmitDocumentBundleResponse.xml"));
        try {
            //method to be tested
            faultHandler.handle(eTrustExAdapterResponseDTO);
            Assert.fail();
        } catch (Exception e) {
            Assert.assertEquals("ETrustExPluginException type is expected", ETrustExPluginException.class, e.getClass());
            ETrustExPluginException eTrustExPluginException = (ETrustExPluginException) e;
            Assert.assertEquals("error type should match", ETrustExError.ETX_002, eTrustExPluginException.getError());
            Assert.assertEquals("error message should match", "No payload provided!", eTrustExPluginException.getMessage());
        }
    }
}
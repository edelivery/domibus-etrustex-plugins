package eu.europa.ec.etrustex.adapter.web;

import eu.europa.ec.etrustex.common.service.CacheService;
import eu.europa.ec.etrustex.adapter.AbstractSpringContextIT;
import eu.europa.ec.etrustex.adapter.service.AdapterConfigurationService;
import eu.europa.ec.etrustex.adapter.testutils.h2.StatementExecutor;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockServletContext;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import java.sql.SQLException;

import static eu.europa.ec.etrustex.adapter.model.entity.administration.AdapterConfigurationProperty.ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author François Gautier
 * @version 1.0
 * @since 31/01/2018
 */
public class ResetBackEndPluginCacheControllerIT extends AbstractSpringContextIT {

    private static final String DEFAULT_VALUE = "7";
    private static final String NEW_VALUE = "1000";

    @Autowired
    private WebApplicationContext webAppContext;
    @Autowired
    private AdapterConfigurationService adapterConfigurationService;
    @Autowired
    private CacheService cacheService;
    @Autowired
    private StatementExecutor statementExecutor;

    private MockMvc mvc;

    @SuppressWarnings("Duplicates")
    @Before
    public void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(webAppContext).build();

        MockServletContext sc = new MockServletContext("");
        ServletContextListener listener = new ContextLoaderListener(webAppContext);
        ServletContextEvent event = new ServletContextEvent(sc);
        listener.contextInitialized(event);
    }

    /**
     * Reset the value modify by the test in the DB and reset the cache to nt impact the other test in this class
     */
    @After
    public void tearDown() throws SQLException {
        statementExecutor.updateAdapterConfig(ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS.getCode(), DEFAULT_VALUE);
        cacheService.clearCache();
    }

    @Test
    @WithMockUser(roles={"ADMIN"})
    public void resetCache_user() throws Exception {
        String s = adapterConfigurationService.getAdapterConfigurations().get(ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS);
        assertThat(s, is(DEFAULT_VALUE));

        statementExecutor.updateAdapterConfig(ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS.getCode(), "1000");
        mvc.perform(get("/resetCache")).andExpect(status().isOk());

        s = adapterConfigurationService.getAdapterConfigurations().get(ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS);
        assertThat(s, is("1000"));
    }

    @Test
    @WithMockUser(roles={"ANONYMOUS"})
    public void resetCache_redirect_notAuthorized() throws Exception {
        redirect();
    }

    private void redirect() throws Exception {
        String s = adapterConfigurationService.getAdapterConfigurations().get(ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS);
        assertThat(s, is(DEFAULT_VALUE));

        statementExecutor.updateAdapterConfig(ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS.getCode(), "1000");
        mvc.perform(get("/resetCache"))
                .andExpect(status().isFound())
                .andExpect(header().string("location", "login?returnUrl=%2FresetCache"))
                .andExpect(content().string("REDIRECT TO LOGIN"));

        s = adapterConfigurationService.getAdapterConfigurations().get(ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS);
        assertThat(s, is(DEFAULT_VALUE));
    }

    @Test
    public void resetCache_redirect_notAuthenticated() throws Exception {
        redirect();
    }

    @Test
    public void noResetCache() throws SQLException {
        String s = adapterConfigurationService.getAdapterConfigurations().get(ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS);
        assertThat(s, is(DEFAULT_VALUE));

        statementExecutor.updateAdapterConfig(ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS.getCode(), NEW_VALUE);

        s = adapterConfigurationService.getAdapterConfigurations().get(ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS);
        assertThat(s, is(DEFAULT_VALUE));
    }
}
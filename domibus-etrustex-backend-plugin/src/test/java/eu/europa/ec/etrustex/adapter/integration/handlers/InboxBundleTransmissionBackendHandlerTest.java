package eu.europa.ec.etrustex.adapter.integration.handlers;

import ec.schema.xsd.documentbundle_1.DocumentBundleType;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleRequest;
import eu.domibus.messaging.MessagingProcessingException;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.converters.NodeDocumentBundleTypeConverter;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystemConfig;
import eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentDirection;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.service.BackendConnectorSubmissionService;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.adapter.service.security.IdentityManager;
import eu.europa.ec.etrustex.adapter.support.Util;
import eu.europa.ec.etrustex.adapter.testutils.ETrustExBackendPluginPropertiesBuilder;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static eu.europa.ec.etrustex.adapter.testutils.XsdCommonsUtils.getIdType;

/**
 * JUnit test for {@link InboxBundleTransmissionBackendHandler}
 *
 * @author Federico Martini
 * @version 1.0
 * @since 07 Dec 2017
 */
@RunWith(JMockit.class)
public class InboxBundleTransmissionBackendHandlerTest {

    @Injectable
    private IdentityManager identityManager;

    @Injectable
    private MessageServicePlugin messageService;

    @Injectable
    private PostProcessProducer postProcessProducer;

    @Injectable
    private BackendConnectorSubmissionService backendConnectorSubmissionService;

    @Injectable
    @SuppressWarnings("unused")
    private ETrustExBackendPluginProperties etxBackendPluginProperties;

    @Tested
    private InboxBundleTransmissionBackendHandler inboxBundleTransmissionBackendHandler;

    private String domibusMessageId;
    private Long messageId;
    private EtxMessage etxMessage;
    private SubmitDocumentBundleRequest submitDocumentBundleRequest;
    private static final String RECEIVER_PARTY_UUID = "receiverPartyUuidTest2";
    private static final String SENDER_PARTY_UUID = "senderPartyUuidTest1";

    @Before
    public void setUp() {
        domibusMessageId = UUID.randomUUID().toString();
        messageId = RandomUtils.nextLong(0, 99999);
        etxMessage = getEtxMessage();

        submitDocumentBundleRequest = new SubmitDocumentBundleRequest();
        submitDocumentBundleRequest.setDocumentBundle(getDocumentBundleType());

        etxBackendPluginProperties = ETrustExBackendPluginPropertiesBuilder.getInstance();
        // static methods mockup
        new MockUp<NodeDocumentBundleTypeConverter>() {
            @Mock
            EtxMessage toEtxMessage(DocumentBundleType documentBundle) {
                return etxMessage;
            }
        };

        new MockUp<TransformerUtils>() {
            @Mock
            SubmitDocumentBundleRequest extractETrustExXMLPayload(ContentId requestContentId, ETrustExAdapterDTO eTrustExAdapterDTO) {
                return submitDocumentBundleRequest;
            }
        };

        new MockUp<Util>() {
            @Mock
            Map<SystemConfigurationProperty, String> buildSystemConfig(List<EtxSystemConfig> etxSystemConfigs) {
                Map<SystemConfigurationProperty, String> config = new HashMap<>();
                config.put(SystemConfigurationProperty.ETX_BACKEND_DOWNLOAD_ATTACHMENTS, "True");
                return config;
            }
        };
    }

    @Test
    public void retrieveParty_notOK_blank() {
        try {
            inboxBundleTransmissionBackendHandler.retrieveParty("");
            Assert.fail();
        } catch (Exception e) {
            //OK
        }
    }

    @Test
    public void retrieveParty_notOK_notFound() {
        final String strParty = "AA";

        new Expectations(){{
            identityManager.searchPartyByUuid(strParty);
            times = 1;
            result = null;
        }};

        try {
            inboxBundleTransmissionBackendHandler.retrieveParty(strParty);
            Assert.fail();
        } catch (Exception e) {
            //OK
        }

        new FullVerifications(){{}};
    }

    private DocumentBundleType getDocumentBundleType() {
        final DocumentBundleType documentBundle = new DocumentBundleType();
        documentBundle.setID(getIdType("123"));
        return documentBundle;
    }

    @Test
    public void testHandle_HappyFlow() {
        final ETrustExAdapterDTO etxAdapterDTO = getDto();

        final EtxParty senderParty = getEtxParty(SENDER_PARTY_UUID);
        final EtxParty receiverParty = getEtxParty(RECEIVER_PARTY_UUID, new EtxSystem());

        new Expectations() {{

            identityManager.searchPartyByUuid(SENDER_PARTY_UUID);
            times = 1;
            result = senderParty;

            identityManager.searchPartyByUuid(RECEIVER_PARTY_UUID);
            times = 1;
            result = receiverParty;

            messageService.createMessage(etxMessage);
            times = 1;

        }};

        inboxBundleTransmissionBackendHandler.handle(etxAdapterDTO);

        new FullVerifications() {{

            EtxMessage etxMessageActual;
            messageService.createMessage(etxMessageActual = withCapture());

            Assert.assertNotNull("etxMessage shouldn't be null", etxMessageActual);
            Assert.assertEquals("etxMessage should match", etxMessage, etxMessageActual);
            Assert.assertEquals("message id should match", messageId, etxMessageActual.getId());

            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            times = 1;

        }};
    }

    private EtxParty getEtxParty(String receiverPartyUuid, EtxSystem system) {
        final EtxParty receiverParty = getEtxParty(receiverPartyUuid);
        receiverParty.setSystem(system);
        return receiverParty;
    }

    @Test
    public void testHandle_Ignored_Wrappers() {

        final ETrustExAdapterDTO etxAdapterDTO = getDto();

        final EtxParty senderParty = getEtxParty(SENDER_PARTY_UUID);
        final EtxParty receiverParty = getEtxParty(RECEIVER_PARTY_UUID, new EtxSystem());

        new MockUp<Util>() {
            @Mock
            Map<SystemConfigurationProperty, String> buildSystemConfig(List<EtxSystemConfig> etxSystemConfigs) {
                Map<SystemConfigurationProperty, String> config = new HashMap<>();
                config.put(SystemConfigurationProperty.ETX_BACKEND_DOWNLOAD_ATTACHMENTS, "False");
                return config;
            }
        };

        new Expectations() {{

            identityManager.searchPartyByUuid(SENDER_PARTY_UUID);
            times = 1;
            result = senderParty;

            identityManager.searchPartyByUuid(RECEIVER_PARTY_UUID);
            times = 1;
            result = receiverParty;
        }};

        inboxBundleTransmissionBackendHandler.handle(etxAdapterDTO);

        new FullVerifications() {{

            postProcessProducer.triggerPostProcess(anyLong);
            times = 1;

            EtxMessage etxMessageActual;
            messageService.createMessage(etxMessageActual = withCapture());

            Assert.assertNotNull("etxMessage shouldn't be null", etxMessageActual);
            Assert.assertEquals("etxMessage should match", etxMessage, etxMessageActual);
            Assert.assertEquals("message id should match", messageId, etxMessageActual.getId());

            Long messageIdActual;
            postProcessProducer.triggerPostProcess(messageIdActual = withCapture());
            times = 1;
            Assert.assertNotNull("message id shouldn't be null", messageIdActual);
            Assert.assertEquals("message id should match", messageId, messageIdActual);

            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            times = 1;

        }};
    }


    @Test
    public void testHandleError() {

        final ETrustExAdapterDTO etxAdapterDTO = getDto();

        final EtxParty senderParty = getEtxParty(SENDER_PARTY_UUID);
        final EtxParty receiverParty = getEtxParty(RECEIVER_PARTY_UUID, new EtxSystem());

        final MessagingProcessingException mpEx = new MessagingProcessingException(new IllegalArgumentException("Wrong party!"));

        new Expectations() {{

            identityManager.searchPartyByUuid(SENDER_PARTY_UUID);
            times = 1;
            result = senderParty;

            identityManager.searchPartyByUuid(RECEIVER_PARTY_UUID);
            times = 1;
            result = receiverParty;

            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            times = 1;
            result = new ETrustExPluginException(ETrustExError.DEFAULT, mpEx.getMessage(), mpEx);

        }};

        inboxBundleTransmissionBackendHandler.handle(etxAdapterDTO);

        new FullVerifications() {{

            messageService.createMessage(etxMessage);
            times = 1;

            postProcessProducer.triggerPostProcess(anyLong);
            times = 1;

            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            times = 1;

            EtxMessage etxMessageActual;
            messageService.updateMessage(etxMessageActual = withCapture());
            times = 1;

            Assert.assertEquals("error code must match", ETrustExError.DEFAULT.getCode(), etxMessageActual.getError().getResponseCode());
            Assert.assertEquals("error message must match", "java.lang.IllegalArgumentException: Wrong party!", etxMessageActual.getError().getDetail());

            Assert.assertEquals(etxMessageActual.getMessageState(), MessageState.MS_FAILED);
        }};
    }

    private EtxParty getEtxParty(String senderPartyUuid) {
        EtxParty etxParty = new EtxParty();
        etxParty.setPartyUuid(senderPartyUuid);
        return etxParty;
    }

    private EtxMessage getEtxMessage() {
        final EtxMessage etxMessage = new EtxMessage();
        etxMessage.setId(messageId);
        etxMessage.setDomibusMessageId(domibusMessageId);
        etxMessage.setMessageState(MessageState.MS_CREATED);
        etxMessage.addAttachment(getEtxAttachment(etxMessage));
        etxMessage.addAttachment(getEtxAttachment(etxMessage));
        etxMessage.addAttachment(getEtxAttachment(etxMessage));
        return etxMessage;
    }

    private EtxAttachment getEtxAttachment(EtxMessage etxMessage) {
        EtxAttachment etxAttachment = new EtxAttachment();
        etxAttachment.setStateType(AttachmentState.ATTS_CREATED);
        etxAttachment.setDirectionType(AttachmentDirection.INCOMING);
        etxAttachment.setActiveState(true);
        etxAttachment.setMessage(etxMessage);
        return etxAttachment;
    }

    private ETrustExAdapterDTO getDto() {
        ETrustExAdapterDTO dto = new ETrustExAdapterDTO();
        dto.setAs4MessageId(domibusMessageId);
        dto.setAs4OriginalSender("senderPartyUuidTest1");
        dto.setAs4FinalRecipient("receiverPartyUuidTest2");
        return dto;
    }

}
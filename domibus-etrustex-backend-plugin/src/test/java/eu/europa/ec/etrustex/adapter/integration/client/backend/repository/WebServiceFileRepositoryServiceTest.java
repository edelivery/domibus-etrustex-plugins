package eu.europa.ec.etrustex.adapter.integration.client.backend.repository;

import eu.europa.ec.etrustex.adapter.integration.client.backend.BackendServiceClient;
import eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.vo.FileAttachmentVO;
import eu.europa.ec.etrustex.adapter.model.vo.LocalSharedFileAttachmentVO;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.apache.commons.io.IOUtils;
import org.apache.cxf.attachment.LazyDataSource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Map;

import static org.apache.commons.codec.binary.StringUtils.getBytesUtf8;

/**
 * JUnit class for <code>WebServiceFileRepositoryService</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@RunWith(JMockit.class)
public class WebServiceFileRepositoryServiceTest {

    @Injectable
    private BackendServiceClient etxBackendPluginServiceWithWebService;

    @Tested
    private WebServiceFileRepositoryService webServiceFileRepositoryService;
    @Mocked
    private DataHandler dataHandler;
    @Mocked
    private DataSource dataSource;

    @Mocked
    private LazyDataSource lazyDataSource;
    @Test
    public void testReadFile_HappyFlow(final @Injectable FileAttachmentVO fileAttachmentVO) throws Exception {
        final Map<SystemConfigurationProperty, String> systemConfigMap =
                Collections.singletonMap(SystemConfigurationProperty.ETX_BACKEND_DOWNLOAD_ATTACHMENTS, "config.value");
        final EtxAttachment etxAttachment = new EtxAttachment();

        new Expectations() {{
            etxBackendPluginServiceWithWebService.downloadAttachment(systemConfigMap, etxAttachment.getMessage(), etxAttachment);
            result = fileAttachmentVO;

        }};

        FileAttachmentVO fileAttachment = webServiceFileRepositoryService.readFile(systemConfigMap, etxAttachment);
        Assert.assertNotNull("return object shouldn't be null", fileAttachment);
        Assert.assertEquals("returned object must match", fileAttachment, fileAttachmentVO);

    }

    @Test
    public void testWriteFile_HappyFlow() throws Exception {

        final Map<SystemConfigurationProperty, String> systemConfigMap =
                Collections.singletonMap(SystemConfigurationProperty.ETX_BACKEND_DOWNLOAD_ATTACHMENTS, "config.value");
        final EtxAttachment etxAttachment = new EtxAttachment();
        final EtxMessage etxMessage = new EtxMessage();
        final Long messageId = 10L;
        etxMessage.setId(messageId);

        final Path attachmentFile = Paths.get("/tmp/test");

        webServiceFileRepositoryService.writeFile(systemConfigMap, etxAttachment, attachmentFile);

        new Verifications() {{
            Map<SystemConfigurationProperty, String> systemConfigMapActual;
            EtxMessage etxMessageActual;
            EtxAttachment etxAttachmentActual;

            etxBackendPluginServiceWithWebService.uploadAttachment(systemConfigMapActual = withCapture(),
                    etxMessageActual = withCapture(), etxAttachmentActual = withCapture(), attachmentFile.toFile());
            times = 1;
            Assert.assertEquals(systemConfigMap, systemConfigMapActual);
            Assert.assertEquals(etxAttachment.getMessage(), etxMessageActual);
            Assert.assertEquals(etxAttachment, etxAttachmentActual);

        }};
    }

    @Test
    public void testDeleteDirectory_HappyFlow() throws Exception {
        final EtxMessage etxMessage = new EtxMessage();

        Assert.assertEquals(true, webServiceFileRepositoryService.deleteDirectory(etxMessage));
    }

    @Test
    public void releaseResources_doNothing() throws IOException {
        LocalSharedFileAttachmentVO faVo = new LocalSharedFileAttachmentVO(null);
        webServiceFileRepositoryService.releaseResources(faVo);
    }

    @Test
    public void releaseResources_normalDataSource(@Mocked IOUtils ioUtils) throws IOException {
        final LocalSharedFileAttachmentVO faVo = new LocalSharedFileAttachmentVO(null);
        faVo.setDataHandler(dataHandler);
        new Expectations(){{
            dataHandler.getDataSource();
            times = 2;
            result = dataSource;

            dataSource.getContentType();
            times = 1;
            result = "ContentType";
        }};

        webServiceFileRepositoryService.releaseResources(faVo);

        new FullVerifications(){{
        }};
    }

    @Test
    public void releaseResources_lazyDataSource(@Mocked IOUtils ioUtils) throws IOException {
        final LocalSharedFileAttachmentVO faVo = new LocalSharedFileAttachmentVO(null);
        faVo.setDataHandler(dataHandler);
        new Expectations(){{
            dataHandler.getDataSource();
            times = 2;
            result = lazyDataSource;

            lazyDataSource.getInputStream();
            times = 1;
            result = new ByteArrayInputStream(getBytesUtf8(""));
        }};

        webServiceFileRepositoryService.releaseResources(faVo);

        new FullVerifications(){{
        }};
    }

}
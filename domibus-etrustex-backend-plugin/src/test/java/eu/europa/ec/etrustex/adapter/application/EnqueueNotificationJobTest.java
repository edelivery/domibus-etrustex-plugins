package eu.europa.ec.etrustex.adapter.application;

import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.NotificationState;
import eu.europa.ec.etrustex.adapter.persistence.api.message.MessageDao;
import eu.europa.ec.etrustex.adapter.queue.notify.NotifyProducer;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.jms.JMSException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author François Gautier
 * @version 1.0
 * @since 05-Jan-18
 */
@RunWith(JMockit.class)
public class EnqueueNotificationJobTest extends AbstractBackEndQuartzJobBeanTest {

    @Injectable
    private MessageDao messageDao;

    @Injectable
    private NotifyProducer notifyProducer;

    @Tested
    private EnqueueNotificationJob enqueueNotificationJob;

    @Test
    public void enqueueForNotification_noneFound() {
        new Expectations() {{
            messageDao.findMessagesByNotificationState(NotificationState.NF_AUTO_PENDING);
            times = 1;
            result = new ArrayList<>();
        }};

        enqueueNotificationJob.process(null);

        new FullVerifications() {{
        }};
    }

    @Test
    public void enqueueForNotification() throws JMSException {
        final Long id1 = RandomUtils.nextLong(0, 99999);
        final Long id2 = RandomUtils.nextLong(0, 99999);

        new Expectations() {{
            messageDao.findMessagesByNotificationState(NotificationState.NF_AUTO_PENDING);
            times = 1;
            result = Arrays.asList(newMessage(id1), newMessage(id2));
        }};

        enqueueNotificationJob.process(null);

        new FullVerifications() {{
            notifyProducer.triggerNotify(id1);
            times = 1;

            notifyProducer.triggerNotify(id2);
            times = 1;
        }};
    }

    @Test(expected = JMSException.class)
    public void enqueueForNotification_errorOnFirst() throws JMSException {
        final Long id1 = RandomUtils.nextLong(0, 99999);
        final Long id2 = RandomUtils.nextLong(0, 99999);

        new Expectations() {{
            messageDao.findMessagesByNotificationState(NotificationState.NF_AUTO_PENDING);
            times = 1;
            result = Arrays.asList(newMessage(id1), newMessage(id2));

            notifyProducer.triggerNotify(id1);
            times = 1;
            result = new JMSException("ERROR");
        }};

        enqueueNotificationJob.process(null);

        new FullVerifications() {{
            notifyProducer.triggerNotify(id2);
            times = 1;
        }};
    }

    private EtxMessage newMessage(Long id){
        EtxMessage etxMessage = new EtxMessage();
        etxMessage.setId(id);
        return etxMessage;
    }
}
package eu.europa.ec.etrustex.adapter.testutils;

import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;

import static eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginPropertiesTestConstants.*;

/**
 * @author François Gautier
 * @version 1.0
 * @since 16/02/2018
 */
public class ETrustExBackendPluginPropertiesBuilder {

    public static ETrustExBackendPluginProperties getInstance() {

        return new ETrustExBackendPluginProperties() {
            public String getUser() {
                return TEST_USER;
            }

            public String getPwd() {
                return TEST_PWD;
            }

            public String getDomain() {
                return TEST_DOMAIN;
            }
            public String getAs4ETrustExNodePartyId() {
                return TEST_AS4_ETRUSTEXNODEPARTYID;
            }

            public String getAs4FromPartyIdType() {
                return TEST_AS4_FROMPARTYIDTYPE;
            }

            public String getAs4FromPartyRole() {
                return TEST_AS4_FROMPARTYROLE;
            }

            public String getAs4ToPartyIdType() {
                return TEST_AS4_TOPARTYIDTYPE;
            }

            public String getAs4ToPartyRole() {
                return TEST_AS4_TOPARTYROLE;
            }

            public String getAs4_Service_ApplicationResponse() {
                return TEST_AS4_SERVICE_APPLICATIONRESPONSE;
            }

            public String getAs4_Service_ApplicationResponse_ServiceType() {
                return TEST_AS4_SERVICE_APPLICATIONRESPONSE_SERVICETYPE;
            }

            public String getAs4_Action_SubmitApplicationResponse_Request() {
                return TEST_AS4_ACTION_SUBMITAPPLICATIONRESPONSE_REQUEST;
            }

            public String getAs4_Action_SubmitApplicationResponse_Response() {
                return TEST_AS4_ACTION_SUBMITAPPLICATIONRESPONSE_RESPONSE;
            }

            public String getAs4_Service_DocumentWrapper() {
                return TEST_AS4_SERVICE_DOCUMENTWRAPPER;
            }

            public String getAs4_Service_DocumentWrapper_ServiceType() {
                return TEST_AS4_SERVICE_DOCUMENTWRAPPER_SERVICETYPE;
            }

            public String getAs4_Action_StoreDocumentWrapper_Response() {
                return TEST_AS4_ACTION_STOREDOCUMENTWRAPPER_RESPONSE;
            }

            public String getAs4_Service_DocumentBundle() {
                return TEST_AS4_SERVICE_DOCUMENTBUNDLE;
            }

            public String getAs4_Service_DocumentBundle_ServiceType() {
                return TEST_AS4_SERVICE_DOCUMENTBUNDLE_SERVICETYPE;
            }

            public String getAs4_Action_SubmitDocumentBundle_Request() {
                return TEST_AS4_ACTION_SUBMITDOCUMENTBUNDLE_REQUEST;
            }

            public String getAs4_Action_SubmitDocumentBundle_Response() {
                return TEST_AS4_ACTION_SUBMITDOCUMENTBUNDLE_RESPONSE;
            }

            public String getAs4_Service_RetrieveICA() {
                return TEST_AS4_SERVICE_RETRIEVEICA;
            }
            public String getAs4_Service_RetrieveICA_ServiceType() {
                return TEST_AS4_SERVICE_RETRIEVEICA_SERVICETYPE;
            }
            public String getAs4_Action_RetrieveICA_Request() {
                return TEST_AS4_ACTION_RETRIEVEICA_REQUEST;
            }
            public String getAs4_Action_RetrieveICA_Response() {
                return TEST_AS4_ACTION_RETRIEVEICA_RESPONSE;
            }

            public String getAs4_Action_StoreDocumentWrapper_Request() {
                return TEST_AS4_ACTION_STOREDOCUMENTWRAPPER_REQUEST;
            }

            public String getAs4ServiceInboxBundleTransmission() {
                return TEST_AS4_SERVICE_INBOXBUNDLETRANSMISSION;
            }

            public String getAs4ServiceInboxBundleTransmissionServiceType() {
                return TEST_AS4_SERVICE_INBOXBUNDLETRANSMISSION_SERVICETYPE;
            }

            public String getAs4ActionInboxBundleTransmissionAck() {
                return TEST_AS4_SERVICE_INBOXBUNDLETRANSMISSION_ACK;
            }

            public String getSystemDefaultName() {
                return SYSTEM_DEFAULT;
            }
        };
    }
}

package eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter;

import ec.schema.xsd.commonaggregatecomponents_2.BusinessHeaderType;
import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import mockit.Tested;
import org.junit.Test;

import javax.xml.ws.Holder;

import static eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter.NodeServicesAdapterBase.STATUS_SCOPE_IDENTIFIER;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @Author François Gautier
 * @Since 10-Oct-17
 * @Version 1.0
 */
public class NodeServicesAdapterBaseTest extends BaseAdapterUnitTest {

    private static final String SENDER = "SENDER";
    private static final String RECEIVER = "RECEIVER";

    @Tested
    private NodeServicesAdapterBase nodeServicesAdapterBase;

    @Test
    public void fillAuth() throws Exception {
        Holder<HeaderType> headerTypeHolder = nodeServicesAdapterBase.fillAuthorisationHeader(SENDER, RECEIVER);
        BusinessHeaderType businessHeader = headerTypeHolder.value.getBusinessHeader();
        assertThat(businessHeader.getSender().size(), is(1));
        assertThat(businessHeader.getSender().get(0).getIdentifier().getValue(), is(SENDER));
        assertThat(businessHeader.getReceiver().size(), is(1));
        assertThat(businessHeader.getReceiver().get(0).getIdentifier().getValue(), is(RECEIVER));
    }

    @Test
    public void fillAuthorisationHeaderWithStatusScope() throws Exception {
        Holder<HeaderType> headerTypeHolder = nodeServicesAdapterBase.fillAuthorisationHeaderWithStatusScope(SENDER, RECEIVER);
        BusinessHeaderType businessHeader = headerTypeHolder.value.getBusinessHeader();
        assertThat(businessHeader.getBusinessScope().getScope().size(), is(1));
        assertThat(businessHeader.getBusinessScope().getScope().get(0).getType(), is(STATUS_SCOPE_IDENTIFIER));
    }
}
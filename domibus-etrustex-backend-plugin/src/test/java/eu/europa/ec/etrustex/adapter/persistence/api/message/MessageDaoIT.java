package eu.europa.ec.etrustex.adapter.persistence.api.message;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.model.vo.MessageReferenceVO;
import eu.europa.ec.etrustex.adapter.persistence.api.AbstractDaoIT;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBase;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.PartyDao;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection.IN;
import static eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection.OUT;
import static eu.europa.ec.etrustex.adapter.model.entity.message.MessageType.MESSAGE_BUNDLE;
import static eu.europa.ec.etrustex.adapter.model.entity.message.MessageType.MESSAGE_STATUS;
import static eu.europa.ec.etrustex.adapter.model.entity.message.NotificationState.NF_AUTO_PENDING;
import static eu.europa.ec.etrustex.adapter.service.AttachmentServiceBackEndImplIT.formEtxMessageInboxBundle1Wrapper;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.*;

/**
 * @author François Gautier
 * @version 1.0
 * @since 25-Jan-18
 */
public class MessageDaoIT extends AbstractDaoIT<EtxMessage> {

    @Autowired
    private MessageDao messageDao;

    @Autowired
    private PartyDao partyDao;
    private final Date from = Date.from(LocalDateTime.now().minusDays(1)
            .atZone(ZoneId.systemDefault()).toInstant());
    private final Date to = Date.from(LocalDateTime.now().plusDays(1).atZone(ZoneId.systemDefault()).toInstant());


    @Before
    public void setUp() throws Exception {

        EtxParty sender = partyDao.findById(1001L);
        EtxParty receiver = partyDao.findById(1002L);
        messageDao.save(formEtxMessageInboxBundle1Wrapper(sender, receiver, MessageState.MS_CREATED));
        messageDao.save(formEtxMessageInboxBundle1Wrapper(sender, receiver, MessageState.MS_FAILED));
        messageDao.save(formEtxMessageInboxBundle1Wrapper(sender, receiver, MessageState.MS_PROCESSED));
        messageDao.save(formEtxMessageInboxBundle1Wrapper(sender, receiver, MessageState.MS_UPLOADED));
        messageDao.save(formEtxMessageInboxBundle1Wrapper(sender, receiver, MessageState.MS_UPLOAD_REQUESTED));

    }

    @Test
    @Ignore("EDELIVERY-10357")
    public void retrieveFailedMessageBundles_all() {
        List<MessageReferenceVO> messageReferenceVOS = messageDao.retrieveFailedMessageBundles(from, to, to, 1000);
        assertEquals(7, messageReferenceVOS.size());
    }

    @Test
    @Ignore("EDELIVERY-10357")
    public void retrieveFailedMessageBundles_onlyFailed() {
        List<MessageReferenceVO> messageReferenceVOS = messageDao.retrieveFailedMessageBundles(Date.from(LocalDateTime.now().minusDays(100)
                .atZone(ZoneId.systemDefault()).toInstant()), Date.from(LocalDateTime.now().plusDays(100)
                .atZone(ZoneId.systemDefault()).toInstant()), Date.from(LocalDateTime.now().minusDays(100)
                .atZone(ZoneId.systemDefault()).toInstant()), 1000);
        assertEquals(1, messageReferenceVOS.size());
    }

    @Test
    @Ignore("EDELIVERY-10357")
    public void retrieveFailedMessageBundles_limit() {
        List<MessageReferenceVO> messageReferenceVOS = messageDao.retrieveFailedMessageBundles(from, to, to, 2);
        assertEquals(2, messageReferenceVOS.size());
    }

    @Test
    public void findMessageByDomibusMessageId_ok() {
        EtxMessage result = messageDao.findMessageByDomibusMessageId("2e09ab8a-3802-4318-9de6-b2a74af8e82c@domibus.eu");
        assertThat(result, is(notNullValue()));
    }

    @Test
    public void findMessageByDomibusMessageId_notFound() {
        EtxMessage result = messageDao.findMessageByDomibusMessageId("NOT_FOUND");
        assertThat(result, is(nullValue()));
    }

    @Test
    public void findMessagesByUuidAndSenderAndReceiver_found() {
        List<MessageReferenceVO> results = messageDao.findMessagesByUuidAndSenderAndReceiver("44466", 1001L, 1002L);
        assertThat(results.size(), is(1));
    }

    @Test
    public void findMessagesByUuidAndSenderAndReceiver_wrongUUID() {
        List<MessageReferenceVO> results = messageDao.findMessagesByUuidAndSenderAndReceiver("NOPE", 1001L, 1002L);
        assertThat(results.size(), is(0));
    }

    @Test
    public void findMessagesByUuidAndSenderAndReceiver_wrongSender() {
        List<MessageReferenceVO> results = messageDao.findMessagesByUuidAndSenderAndReceiver("44466", 1002L, 1002L);
        assertThat(results.size(), is(0));
    }

    @Test
    public void findMessagesByUuidAndSenderAndReceiver_wrongReceiver() {
        List<MessageReferenceVO> results = messageDao.findMessagesByUuidAndSenderAndReceiver("44466", 1001L, 1001L);
        assertThat(results.size(), is(0));
    }

    @Test
    public void findMessagesByNotificationState_found() {
        List<EtxMessage> messagesByNotificationState = messageDao.findMessagesByNotificationState(NF_AUTO_PENDING);
        assertThat(messagesByNotificationState.size(), is(1));
    }

    @Test
    public void findUnreadMessagesByReceiverPartyUuid_found() {
        List<MessageReferenceVO> results = messageDao.findUnreadMessagesByReceiverPartyUuid("PARTY_2");
        assertThat(results.size(), is(5));
        MessageReferenceVO result = results.get(0);
        assertThat(result.getMessageUuid(), is("44476"));
        assertThat(result.getMessageType(), is(MessageType.MESSAGE_STATUS));
        assertThat(result.getReceiverUuid(), is("PARTY_2"));
        assertThat(result.getSenderUuid(), is("PARTY_1"));
    }

    @Test
    public void findUnreadMessagesByReceiverPartyUuid_notFound() {
        List<MessageReferenceVO> results = messageDao.findUnreadMessagesByReceiverPartyUuid("NOPE");
        assertThat(results.size(), is(0));
    }

    @Test
    public void findEtxMessage_found() {
        EtxMessage result = messageDao.findEtxMessage("44466", singletonList(MESSAGE_STATUS), OUT, "PARTY_1", "PARTY_2");
        assertThat(result, is(notNullValue()));
    }

    @Test
    public void findEtxMessage_notFound_wrongMsgUuid() {
        EtxMessage result = messageDao.findEtxMessage("NOPE", singletonList(MESSAGE_STATUS), OUT, "PARTY_1", "PARTY_2");
        assertThat(result, is(nullValue()));
    }

    @Test
    public void findEtxMessage_notFound_wrongDirection() {
        EtxMessage result = messageDao.findEtxMessage("44466", singletonList(MESSAGE_BUNDLE), IN, "PARTY_1", "PARTY_2");
        assertThat(result, is(nullValue()));
    }

    @Test
    public void findEtxMessage_notFound_wrongSender() {
        EtxMessage result = messageDao.findEtxMessage("44466", singletonList(MESSAGE_STATUS), OUT, "PARTY_2", "PARTY_2");
        assertThat(result, is(nullValue()));
    }

    @Test
    public void findEtxMessage_notFound_wrongReceiver() {
        EtxMessage result = messageDao.findEtxMessage("44466", singletonList(MESSAGE_STATUS), OUT, "PARTY_1", "PARTY_1");
        assertThat(result, is(nullValue()));
    }

    @Test
    public void findEtxMessage_duplicateBundleEntry() {
        EtxMessage etxMessage = messageDao.findEtxMessage("DUPL_BNDL1", singletonList(MESSAGE_BUNDLE), IN, "PARTY_1", "PARTY_2");
        assertNotNull(etxMessage);
        assertEquals(etxMessage.getId(), Long.valueOf("23"));
    }

    @Test
    public void findEtxMessage_duplicateStatusEntry() {
        EtxMessage etxMessage = messageDao.findEtxMessage("DUPL_STATUS1", singletonList(MESSAGE_STATUS), IN, "PARTY_1", "PARTY_2");
        assertNotNull(etxMessage);
        assertEquals(etxMessage.getId(), Long.valueOf("25"));
    }

    @Test
    public void update() {
        EtxMessage etxMessage = getDao().findById(getExistingId());
        etxMessage.setMessageUuid("TEST");
        getDao().update(etxMessage);
    }

    /**
     * Test that only MessageBundles with all attachments in final states are selected for PostUpload job.
     */
    @Test
    @Ignore("EDELIVERY-10357")
    public void testRetrieveMessageBundlesForPostUpload() {
        List<EtxMessage> etxMessageList = messageDao.retrieveMessageBundlesForPostUpload();

        assertThat(etxMessageList, containsInAnyOrder(
                        hasProperty("messageUuid", is("BNDL_16")),
                        hasProperty("messageUuid", is("BNDL_17")),
                        hasProperty("messageUuid", is("BNDL_19")),
                        hasProperty("messageUuid", is("BNDL_20"))
                )
        );
    }

    @Override
    protected Long getExistingId() {
        return 15L;
    }

    @Override
    protected DaoBase<EtxMessage> getDao() {
        return messageDao;
    }

    @Override
    protected EtxMessage getNewEntity() {
        EtxMessage message = new EtxMessage();
        message.setId(null);
        message.setDirectionType(OUT);
        message.setSender(partyDao.findById(1001L));
        message.setReceiver(partyDao.findById(1002L));
        message.setIssueDateTime(Calendar.getInstance());
        message.setMessageState(MessageState.MS_CREATED);
        message.setMessageType(MESSAGE_BUNDLE);
        message.setMessageUuid(UUID.randomUUID().toString());
        message.setAttachmentList(new ArrayList<EtxAttachment>());
        return message;
    }


}
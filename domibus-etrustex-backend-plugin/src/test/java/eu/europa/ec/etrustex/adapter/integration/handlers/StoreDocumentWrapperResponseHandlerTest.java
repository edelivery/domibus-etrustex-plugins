package eu.europa.ec.etrustex.adapter.integration.handlers;

import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.queue.postupload.PostUploadProducer;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import eu.europa.ec.etrustex.adapter.testutils.PayloadDTOBuilder;
import eu.europa.ec.etrustex.adapter.testutils.ResourcesUtils;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.charset.StandardCharsets;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

//import eu.europa.ec.etrustex.adapter.queue.postupload.PostUploadProducer;

/**
 * @author François Gautier
 * @version 1.0
 * @since 21/03/2018
 */
@SuppressWarnings("Duplicates")
@RunWith(JMockit.class)
public class StoreDocumentWrapperResponseHandlerTest {

    @Injectable
    private PostUploadProducer postUploadProducer;

    @Injectable
    private AttachmentService attachmentService;

    @Tested
    private StoreDocumentWrapperResponseHandler storeDocumentWrapperResponseHandler;

    @Test
    public void handle_happyFlow() {
        final ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        eTrustExAdapterDTO.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_GENERIC)
                .withPayload(ResourcesUtils.readResource("data/StoreDocumentWrapperResponse.xml").getBytes(StandardCharsets.UTF_8))
                .build());
        final EtxAttachment etxAttachment = new EtxAttachment();

        new Expectations() {{
            attachmentService.findAttachmentByDomibusMessageId(eTrustExAdapterDTO.getAs4RefToMessageId());
            times = 1;
            result = etxAttachment;

        }};

        storeDocumentWrapperResponseHandler.handle(eTrustExAdapterDTO);

        new FullVerifications(){{
            attachmentService.updateAttachmentState(etxAttachment, AttachmentState.ATTS_DOWNLOADED, AttachmentState.ATTS_UPLOADED);
            times = 1;

            postUploadProducer.checkAttachmentsAndTriggerPostUpload(etxAttachment);
            times = 1;
        }};
    }

    @Test
    public void handle_fault_duplicate() {
        final ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        eTrustExAdapterDTO.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_FAULT_RESPONSE)
                .withPayload(ResourcesUtils.readResource("data/fault/soapFault.xml").getBytes(StandardCharsets.UTF_8))
                .build());
        final EtxAttachment etxAttachment = new EtxAttachment();

        new Expectations() {{
            attachmentService.findAttachmentByDomibusMessageId(eTrustExAdapterDTO.getAs4RefToMessageId());
            times = 1;
            result = etxAttachment;

        }};

        storeDocumentWrapperResponseHandler.handle(eTrustExAdapterDTO);

        new FullVerifications(){{
            attachmentService.updateAttachmentState(etxAttachment, AttachmentState.ATTS_DOWNLOADED, AttachmentState.ATTS_UPLOADED);
            times = 1;

            postUploadProducer.checkAttachmentsAndTriggerPostUpload(etxAttachment);
            times = 1;
        }};
    }

    @Test
    public void handle_fault_NotDuplicate() {
        final ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        eTrustExAdapterDTO.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_ID_FAULT_RESPONSE)
                .withPayload(ResourcesUtils.readResource("data/fault/soapFault.xml")
                        .replaceAll("error.duplicate", "error.notDuplicate")
                        .getBytes(StandardCharsets.UTF_8))
                .build());
        final EtxAttachment etxAttachment = new EtxAttachment();

        new Expectations() {{
            attachmentService.findAttachmentByDomibusMessageId(eTrustExAdapterDTO.getAs4RefToMessageId());
            times = 1;
            result = etxAttachment;

        }};

        storeDocumentWrapperResponseHandler.handle(eTrustExAdapterDTO);

        new FullVerifications(){{
            attachmentService.attachmentInError(etxAttachment.getId(), "error.notDuplicate", anyString);
            times = 1;

            postUploadProducer.checkAttachmentsAndTriggerPostUpload(etxAttachment);
            times = 1;
        }};
    }

    @Test
    public void handle_faultNull() {
        try {
            storeDocumentWrapperResponseHandler.handle(null);
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getError(), is(ETrustExError.ETX_002));
            assertThat(e.getMessage(), is("Either StoreDocumentWrapperResponse or FaultResponse should be provided!"));
        }
    }

}
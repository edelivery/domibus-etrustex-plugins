package eu.europa.ec.etrustex.adapter.integration.handlers;

import ec.schema.xsd.retrieveinterchangeagreementsresponse_2.RetrieveInterchangeAgreementsResponseType;
import ec.services.wsdl.retrieveinterchangeagreementsrequest_2.SubmitRetrieveInterchangeAgreementsRequestResponse;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.adapter.service.IcaService;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.common.util.PayloadDescription;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import static eu.europa.ec.etrustex.common.util.AS4MessageConstants.MIME_TYPE_TEXT_XML;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.*;
import static eu.europa.ec.etrustex.adapter.testutils.ResourcesUtils.readResource;
import static org.apache.commons.codec.binary.StringUtils.getBytesUtf8;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;

@RunWith(JMockit.class)
public class RetrieveICAResponseHandlerTest {

    @Injectable
    private IcaService icaService;

    @Tested
    private RetrieveICAResponseHandler retrieveICAResponseHandler;

    @Test
    public void processNodeSuccess_happyFlow() {
        SubmitRetrieveInterchangeAgreementsRequestResponse response =
                getObjectFromXml(readResource("data/ica/retrieveInterchangeAgreementsResponse.xml"));
        retrieveICAResponseHandler.processNodeSuccess(response, "123456");
    }

    @Test
    public void processNodeSuccess_empty() {
        SubmitRetrieveInterchangeAgreementsRequestResponse response = new SubmitRetrieveInterchangeAgreementsRequestResponse();
        response.setRetrieveInterchangeAgreementsResponse(new RetrieveInterchangeAgreementsResponseType());
        retrieveICAResponseHandler.processNodeSuccess(response, "123456");
    }

    @Test
    public void handle_fault_ok() {
        ETrustExAdapterDTO eTrustExAdapterResponseDTO = new ETrustExAdapterDTO();
        eTrustExAdapterResponseDTO.addAs4Payload(new PayloadDTO(
                ContentId.CONTENT_ID_FAULT_RESPONSE.getValue(),
                getBytesUtf8(readResource("data/fault/soapIcaFault.xml")),
                MIME_TYPE_TEXT_XML,
                false,
                PayloadDescription.ETRUSTEX_FAULT_RESPONSE_PAYLOAD.getValue()
        ));
        retrieveICAResponseHandler.handle(eTrustExAdapterResponseDTO);
    }
    @Test
    public void handle_fault_exception() {
        ETrustExAdapterDTO eTrustExAdapterResponseDTO = new ETrustExAdapterDTO();
        try {
            retrieveICAResponseHandler.handle(eTrustExAdapterResponseDTO);
            Assert.fail();
        } catch (ETrustExPluginException e) {
            Assert.assertThat(e,is(instanceOf(ETrustExPluginException.class)));
        }
    }

}
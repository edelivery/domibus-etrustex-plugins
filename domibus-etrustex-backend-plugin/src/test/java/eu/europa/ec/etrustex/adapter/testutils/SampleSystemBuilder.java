package eu.europa.ec.etrustex.adapter.testutils;

import eu.europa.ec.etrustex.adapter.model.entity.administration.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Arun Raj
 * @version 1.0
 * @since 30/08/2017
 */
public class SampleSystemBuilder {

    public static EtxSystem formValid_EGREFFE_SYS() {
        EtxSystem system_EGREFFE_SYS = new EtxSystem();
        system_EGREFFE_SYS.setId(3L);
        system_EGREFFE_SYS.setName("EGREFFE-SYS");

        EtxUser egreffeSystemLocalUser = new EtxUser();
        egreffeSystemLocalUser.setId(3L);
        egreffeSystemLocalUser.setName("TEST-EGREFFE-SYS-USER");
        egreffeSystemLocalUser.setPassword("XXXXXXXXXXXX");
        system_EGREFFE_SYS.setLocalUsername("TEST-EGREFFE-SYS-USER");

        EtxUser egreffeSystemBackendUser = new EtxUser();
        egreffeSystemBackendUser.setId(5L);
        egreffeSystemBackendUser.setName("etrustexwsuser");
        egreffeSystemBackendUser.setPassword("etrustexwsuser");
        system_EGREFFE_SYS.setBackendUser(egreffeSystemBackendUser);

        List<EtxSystemConfig> egreffe_sys_SystemConfigList = new ArrayList<>();
        EtxSystemConfig egreffeSystemConfig1 = new EtxSystemConfig();
        egreffeSystemConfig1.setId(10L);
        egreffeSystemConfig1.setSystem(system_EGREFFE_SYS);
        egreffeSystemConfig1.setSystemConfigurationProperty(SystemConfigurationProperty.ETX_BACKEND_SERVICE_INBOX_NOTIFICATION_URL);
        egreffeSystemConfig1.setPropertyValue("http://localhost:8088/mockInboxNotificationSoap11Binding");
        egreffe_sys_SystemConfigList.add(egreffeSystemConfig1);

        EtxSystemConfig egreffeSystemConfig2 = new EtxSystemConfig();
        egreffeSystemConfig2.setId(11L);
        egreffeSystemConfig2.setSystem(system_EGREFFE_SYS);
        egreffeSystemConfig2.setSystemConfigurationProperty(SystemConfigurationProperty.ETX_BACKEND_SERVICE_FILE_REPOSITORY_URL);
        egreffeSystemConfig2.setPropertyValue("http://localhost:8088/mockFileRepositorySoap11Binding");
        egreffe_sys_SystemConfigList.add(egreffeSystemConfig2);

        EtxSystemConfig egreffeSystemConfig3 = new EtxSystemConfig();
        egreffeSystemConfig3.setId(12L);
        egreffeSystemConfig3.setSystem(system_EGREFFE_SYS);
        egreffeSystemConfig3.setSystemConfigurationProperty(SystemConfigurationProperty.ETX_BACKEND_DOWNLOAD_ATTACHMENTS);
        egreffeSystemConfig3.setPropertyValue("true");
        egreffe_sys_SystemConfigList.add(egreffeSystemConfig3);

        EtxSystemConfig egreffeSystemConfig4 = new EtxSystemConfig();
        egreffeSystemConfig4.setId(13L);
        egreffeSystemConfig4.setSystem(system_EGREFFE_SYS);
        egreffeSystemConfig4.setSystemConfigurationProperty(SystemConfigurationProperty.ETX_BACKEND_FILE_REPOSITORY_LOCATION);
        egreffeSystemConfig4.setPropertyValue(BackendFileRepositoryLocationType.BACKEND_FILE_REPOSITORY_SERVICE.name());
        egreffe_sys_SystemConfigList.add(egreffeSystemConfig4);

        EtxSystemConfig egreffeSystemConfig6 = new EtxSystemConfig();
        egreffeSystemConfig6.setId(65L);
        egreffeSystemConfig6.setSystem(system_EGREFFE_SYS);
        egreffeSystemConfig6.setSystemConfigurationProperty(SystemConfigurationProperty.ETX_BACKEND_NOTIFICATION_TYPE);
        egreffeSystemConfig6.setPropertyValue(BackendNotificationType.WEB_SERVICE.name());
        egreffe_sys_SystemConfigList.add(egreffeSystemConfig6);
        system_EGREFFE_SYS.setSystemConfigList(egreffe_sys_SystemConfigList);
        return system_EGREFFE_SYS;
    }
}

package eu.europa.ec.etrustex.adapter.integration.client.backend.ws.adapter;

import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageReferenceState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.testutils.SamplePartyBuilder;
import eu.europa.ec.etrustex.integration.model.common.v2.EncryptionInformationType;
import eu.europa.ec.etrustex.integration.model.common.v2.ErrorType;
import eu.europa.ec.etrustex.integration.model.common.v2.FileWrapperType;
import eu.europa.ec.etrustex.integration.service.filerepository.v2.*;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.apache.commons.codec.binary.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import javax.activation.DataHandler;
import javax.mail.util.ByteArrayDataSource;
import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.EndpointReference;
import java.io.File;
import java.util.Map;
import java.util.UUID;

/**
 * @author François Gautier
 * @version 1.0
 * @since 16-Jan-18
 */
@RunWith(JMockit.class)
public class FileRepositoryServiceAdapterImplTest {
    public static final String SUBJECT_NAME = "TEST";
    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    private static final String PASSWORD = "PASSWORD";
    private static final String USERNAME = "USERNAME";
    private static final String SERVICE_POINT = "SERVICE_POINT";

    private FileRepositoryServiceAdapterImpl fileRepositoryServiceAdapter;

    @Mocked
    private BackendWebServiceProvider backendWebServiceProvider;

    @Mocked
    private FileRepositoryPortTypeImpl fileRepositoryPortType;

    private EtxMessage messageBundle;
    private EtxAttachment etxAttachment;
    private File file;


    @Before
    public void setUp() {
        fileRepositoryServiceAdapter = new FileRepositoryServiceAdapterImpl(backendWebServiceProvider);
        fileRepositoryServiceAdapter.setPassword(PASSWORD);
        fileRepositoryServiceAdapter.setUserName(USERNAME);
        fileRepositoryServiceAdapter.setServiceEndpoint(SERVICE_POINT);

        messageBundle = getMessage();
        etxAttachment = getAttachment();
        file = new File("");
    }

    @Test
    public void downloadAttachment_happyFlow() {
        new Expectations() {{
            backendWebServiceProvider.buildJaxWsProxy(FileRepositoryPortType.class, true, SERVICE_POINT);
            times = 1;
            result = fileRepositoryPortType;

            fileRepositoryPortType.getFile((GetFileRequestType) any);
            times = 1;
            result = getGetFileResponseType();
        }};
        fileRepositoryServiceAdapter.downloadAttachment(messageBundle, etxAttachment);

        setupConnectionCredentials();
    }

    private GetFileResponseType getGetFileResponseType() {
        GetFileResponseType getFileResponseType = new GetFileResponseType();
        FileWrapperType fileWrapperType = new FileWrapperType();
        EncryptionInformationType encryptionInformationType = new EncryptionInformationType();
        encryptionInformationType.setSessionKey(StringUtils.getBytesUtf8(PASSWORD));
        encryptionInformationType.setX509SubjectName(SUBJECT_NAME);
        fileWrapperType.setEncryptionInformation(encryptionInformationType);
        fileWrapperType.setContent(new DataHandler(new ByteArrayDataSource(StringUtils.getBytesUtf8(SUBJECT_NAME), "txt")));
        getFileResponseType.setFileWrapper(fileWrapperType);
        return getFileResponseType;
    }

    @Test
    public void downloadAttachment_FaultResponse() {
        thrown.expect(ETrustExPluginException.class);

        new Expectations() {{
            backendWebServiceProvider.buildJaxWsProxy(FileRepositoryPortType.class, true, SERVICE_POINT);
            times = 1;
            result = fileRepositoryPortType;

            fileRepositoryPortType.getFile((GetFileRequestType) any);
            times = 1;
            result = new FaultResponse(SUBJECT_NAME, new ErrorType());
        }};

        fileRepositoryServiceAdapter.downloadAttachment(messageBundle, etxAttachment);

        setupConnectionCredentials();
    }

    @Test
    public void downloadAttachment_Exception() {
        thrown.expect(ETrustExPluginException.class);

        new Expectations() {{
            backendWebServiceProvider.buildJaxWsProxy(FileRepositoryPortType.class, true, SERVICE_POINT);
            times = 1;
            result = fileRepositoryPortType;

            fileRepositoryPortType.getFile((GetFileRequestType) any);
            times = 1;
            result = new Exception();
        }};

        fileRepositoryServiceAdapter.downloadAttachment(messageBundle, etxAttachment);

        setupConnectionCredentials();
    }

    @Test
    public void uploadAttachment_happyFlow() {
        new Expectations() {{
            backendWebServiceProvider.buildJaxWsProxy(FileRepositoryPortType.class, true, SERVICE_POINT);
            times = 1;
            result = fileRepositoryPortType;

            fileRepositoryPortType.sendFile((SendFileRequestType) any);
            times = 1;
            result = new SendFileResponseType();
        }};

        fileRepositoryServiceAdapter.uploadAttachment(messageBundle,etxAttachment, file);

        setupConnectionCredentials();
    }

    @Test
    public void uploadAttachment_FaultResponse() {
        thrown.expect(ETrustExPluginException.class);

        new Expectations() {{
            backendWebServiceProvider.buildJaxWsProxy(FileRepositoryPortType.class, true, SERVICE_POINT);
            times = 1;
            result = fileRepositoryPortType;

            fileRepositoryPortType.sendFile((SendFileRequestType) any);
            times = 1;
            result = new FaultResponse("TEST", new ErrorType());
        }};

        fileRepositoryServiceAdapter.uploadAttachment(messageBundle, etxAttachment, file);

        setupConnectionCredentials();
    }

    @Test
    public void uploadAttachment_Exception() {
        thrown.expect(ETrustExPluginException.class);

        new Expectations() {{
            backendWebServiceProvider.buildJaxWsProxy(FileRepositoryPortType.class, true, SERVICE_POINT);
            times = 1;
            result = fileRepositoryPortType;

            fileRepositoryPortType.sendFile((SendFileRequestType) any);
            times = 1;
            result = new Exception();
        }};

        fileRepositoryServiceAdapter.uploadAttachment(messageBundle, etxAttachment, file);

        setupConnectionCredentials();
    }

    private void setupConnectionCredentials() {
        new FullVerifications() {{
            backendWebServiceProvider.setupConnectionCredentials(fileRepositoryPortType, USERNAME, StringUtils.getBytesUtf8(PASSWORD));
            times = 1;
        }};
    }

    private EtxMessage getMessage() {
        EtxMessage etxMessage = new EtxMessage();
        etxMessage.setSender(SamplePartyBuilder.formValid_EGREFFE_APP_PARTY_B4());
        etxMessage.setReceiver(SamplePartyBuilder.formValid_EGREFFE_NP_PARTY_B4());
        etxMessage.setMessageReferenceStateType(MessageReferenceState.AVAILABLE);
        etxMessage.setMessageType(MessageType.MESSAGE_BUNDLE);
        return etxMessage;
    }

    public EtxAttachment getAttachment() {
        EtxAttachment etxAttachment = new EtxAttachment();
        etxAttachment.setAttachmentUuid(UUID.randomUUID().toString());
        etxAttachment.setSessionKey(StringUtils.getBytesUtf8(PASSWORD));
        etxAttachment.setX509SubjectName("TEST");
        return etxAttachment;
    }

    private static class FileRepositoryPortTypeImpl implements FileRepositoryPortType, BindingProvider {

        @Override
        public GetFileResponseType getFile(GetFileRequestType getFileRequest) {
            return null;
        }

        @Override
        public SendFileResponseType sendFile(SendFileRequestType sendFileRequest) {
            return null;
        }

        @Override
        public Map<String, Object> getRequestContext() {
            return null;
        }

        @Override
        public Map<String, Object> getResponseContext() {
            return null;
        }

        @Override
        public Binding getBinding() {
            return null;
        }

        @Override
        public EndpointReference getEndpointReference() {
            return null;
        }

        @Override
        public <T extends EndpointReference> T getEndpointReference(Class<T> clazz) {
            return null;
        }
    }
}
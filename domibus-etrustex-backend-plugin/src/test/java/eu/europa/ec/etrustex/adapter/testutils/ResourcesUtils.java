package eu.europa.ec.etrustex.adapter.testutils;

import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author François Gautier
 * @since 26-Sep-17
 * @version 1.0
 */
public class ResourcesUtils {
    private ResourcesUtils() {
    }

    /**
     *  loads sample xml file from /src/test/resources/data folder
     *
     * @param fileName in UTF8
     * @return as a UTF8 String
     */
    public static String readResource(final String fileName) {
        try {
            return IOUtils.toString(new FileInputStream(ResourcesUtils.class.getClassLoader().getResource(fileName).getPath()), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}

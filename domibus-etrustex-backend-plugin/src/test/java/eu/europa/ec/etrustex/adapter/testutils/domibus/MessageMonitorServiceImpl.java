package eu.europa.ec.etrustex.adapter.testutils.domibus;

import eu.domibus.ext.domain.MessageAttemptDTO;
import eu.domibus.ext.exceptions.AuthenticationExtException;
import eu.domibus.ext.exceptions.MessageMonitorExtException;
import eu.domibus.ext.services.MessageMonitorExtService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author François Gautier
 * @version 1.0
 * @since 25-Jan-18
 */
@Service("etxBackendPluginMessageMonitorService")
public class MessageMonitorServiceImpl implements MessageMonitorExtService {

    @Override
    public List<String> getFailedMessages() throws AuthenticationExtException, MessageMonitorExtException {
        return null;
    }

    @Override
    public List<String> getFailedMessages(String s) throws AuthenticationExtException, MessageMonitorExtException {
        return null;
    }

    @Override
    public Long getFailedMessageInterval(String s) throws AuthenticationExtException, MessageMonitorExtException {
        return null;
    }

    @Override
    public void restoreFailedMessage(String s) throws AuthenticationExtException, MessageMonitorExtException {

    }

    @Override
    public void sendEnqueuedMessage(String s) throws AuthenticationExtException, MessageMonitorExtException {

    }

    @Override
    public List<String> restoreFailedMessagesDuringPeriod(Date date, Date date1) throws AuthenticationExtException, MessageMonitorExtException {
        return null;
    }

    @Override
    public List<String> restoreSendEnqueuedMessagesDuringPeriod(Date date, Date date1) throws AuthenticationExtException, MessageMonitorExtException {
        return null;
    }

    @Override
    public void deleteFailedMessage(String s) throws AuthenticationExtException, MessageMonitorExtException {

    }

    @Override
    public List<MessageAttemptDTO> getAttemptsHistory(String s) throws AuthenticationExtException, MessageMonitorExtException {
        return null;
    }
}

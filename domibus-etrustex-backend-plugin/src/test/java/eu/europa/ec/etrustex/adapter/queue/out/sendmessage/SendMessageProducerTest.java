package eu.europa.ec.etrustex.adapter.queue.out.sendmessage;

import eu.europa.ec.etrustex.adapter.queue.JmsProducer;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import javax.jms.JMSException;
import javax.jms.Queue;

/**
 * JUnit class for <code>SendMessageProducer</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@RunWith(JMockit.class)
public class SendMessageProducerTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Injectable
    private Queue outboxSendMessageBundle;

    @Injectable
    private JmsProducer<SendMessageQueueMessage> jmsProducer;

    @Tested
    private SendMessageProducer sendMessageProducer;


    @Test
    public void testTriggerSendBundle_HappyFlow(final @Mocked SendMessageQueueMessage queueMessage) {
        final Long messageId = 10L;

        //method to test
        sendMessageProducer.triggerSendBundle(messageId);

        verifOk(queueMessage, messageId);
    }

    private void verifOk(@Mocked final SendMessageQueueMessage queueMessage, final Long messageId) {
        new Verifications() {{

            Long messageIdActual;
            queueMessage.setMessageId(messageIdActual = withCapture());
            Assert.assertNotNull(messageIdActual);
            Assert.assertEquals(messageId, messageIdActual);

            SendMessageQueueMessage queueMessageActual;
            jmsProducer.postMessage(outboxSendMessageBundle, queueMessageActual = withCapture());
            Assert.assertNotNull(queueMessageActual);
        }};
    }

    @Test
    public void testTriggerSendBundle_UnhappyFlow(final @Mocked SendMessageQueueMessage queueMessage) {
        final Long messageId = 10L;
        final String exceptionMessage = "jms exception";

        thrown.expect(JMSException.class);
        thrown.expectMessage(exceptionMessage);

        new Expectations() {{
            new SendMessageQueueMessage(SendMessageQueueMessage.Type.BUNDLE);
            result = queueMessage;

            jmsProducer.postMessage(outboxSendMessageBundle, queueMessage);
            result = new JMSException(exceptionMessage);
        }};

        //method to test
        sendMessageProducer.triggerSendBundle(messageId);

    }

    @Test
    public void testTriggerSendStatus_HappyFlow(final @Mocked SendMessageQueueMessage queueMessage) {
        final Long messageId = 10L;

        //method to test
        sendMessageProducer.triggerSendStatus(messageId);

        verifOk(queueMessage, messageId);

    }


    @Test
    public void testTriggerSendStatus_UnhappyFlow(final @Mocked SendMessageQueueMessage queueMessage) {
        final Long messageId = 10L;
        final String exceptionMessage = "jms exception status";

        thrown.expect(JMSException.class);
        thrown.expectMessage(exceptionMessage);

        new Expectations() {{
            new SendMessageQueueMessage(SendMessageQueueMessage.Type.STATUS);
            result = queueMessage;

            jmsProducer.postMessage(outboxSendMessageBundle, queueMessage);
            result = new JMSException(exceptionMessage);
        }};

        //method to test
        sendMessageProducer.triggerSendStatus(messageId);

    }

}
package eu.europa.ec.etrustex.adapter.queue.out.sendmessage;

import eu.domibus.ext.services.DomainContextExtService;
import eu.domibus.ext.services.DomainExtService;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPluginService;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.queue.ETrustExBackendPluginJMSErrorUtils;
import eu.europa.ec.etrustex.common.jms.TextMessageConverter;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * JUnit class for <code>SendMessageConsumer</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@RunWith(JMockit.class)
public class SendMessageConsumerTest {

    @Injectable
    SendMessageHandler handler;

    @Injectable
    TextMessageConverter textMessageConverter;
    @Injectable
    private DomainBackEndPluginService domainBackEndPluginService;
    @Injectable
    @Qualifier("etxBackendPluginProperties")
    private ETrustExBackendPluginProperties etxBackendPluginProperties;
    @Injectable
    private DomainExtService domainExtService;
    @Injectable
    private DomainContextExtService domainContextExtService;

    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Injectable
    private ETrustExBackendPluginJMSErrorUtils eTrustExBackendPluginJMSErrorUtils;
    @Tested
    SendMessageConsumer sendMessageConsumer;

    @Test
    public void testHandleMessage_HappyFlow_Bundle() throws Exception {
        final Long messageId = 10L;
        SendMessageQueueMessage queueMessage = new SendMessageQueueMessage(SendMessageQueueMessage.Type.BUNDLE);
        queueMessage.setMessageId(messageId);

        //method to be tested
        sendMessageConsumer.handleMessage(queueMessage);

        new Verifications() {{
            Long messageIdActual;
            handler.handleSendBundle(messageIdActual = withCapture());
            Assert.assertEquals(messageId, messageIdActual);
        }};
    }

    @Test
    public void testHandleMessage_HappyFlow_Status() throws Exception {
        final Long messageId = 20L;
        SendMessageQueueMessage queueMessage = new SendMessageQueueMessage(SendMessageQueueMessage.Type.STATUS);
        queueMessage.setMessageId(messageId);

        //method to be tested
        sendMessageConsumer.handleMessage(queueMessage);

        new Verifications() {{
            Long messageIdActual;
            handler.handleSendStatus(messageIdActual = withCapture());
            Assert.assertEquals(messageId, messageIdActual);
        }};
    }

    @Test
    public void testValidateMessage_HappyFlow_Bundle() throws Exception {
        final Long messageId = 20L;
        SendMessageQueueMessage queueMessage = new SendMessageQueueMessage(SendMessageQueueMessage.Type.BUNDLE);
        queueMessage.setMessageId(messageId);

        //method to be tested
        sendMessageConsumer.validateMessage(queueMessage);

        new Verifications() {{
            Long messageIdActual;
            handler.validateSendBundle(messageIdActual = withCapture());
            Assert.assertEquals(messageId, messageIdActual);
        }};
    }

    @Test
    public void testValidateMessage_HappyFlow_Status() throws Exception {
        final Long messageId = 20L;
        SendMessageQueueMessage queueMessage = new SendMessageQueueMessage(SendMessageQueueMessage.Type.STATUS);
        queueMessage.setMessageId(messageId);

        //method to be tested
        sendMessageConsumer.validateMessage(queueMessage);

        new Verifications() {{
            Long messageIdActual;
            handler.validateSendStatus(messageIdActual = withCapture());
            Assert.assertEquals(messageId, messageIdActual);
        }};
    }

}
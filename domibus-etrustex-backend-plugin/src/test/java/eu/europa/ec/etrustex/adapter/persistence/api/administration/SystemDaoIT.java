package eu.europa.ec.etrustex.adapter.persistence.api.administration;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxUser;
import eu.europa.ec.etrustex.adapter.persistence.api.AbstractDaoIT;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBase;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 29-Jan-18
 */
public class SystemDaoIT extends AbstractDaoIT<EtxSystem> {

    @Autowired
    private SystemDao systemDao;
    @Autowired
    private UserDao userDao;

    @Test
    public void findSystemByName_notFound() {
        EtxSystem result = systemDao.findSystemByName("NOT_FOUND");
        assertThat(result, is(nullValue()));
    }

    @Test
    public void findSystemByName_ok() {
        EtxSystem result = systemDao.findSystemByName("SYSTEM_1");
        assertThat(result, is(notNullValue()));
    }
    @Test
    public void findSystemByLocalUserName_notFound() {
        EtxSystem result = systemDao.findSystemByLocalUserName("NOT_FOUND");
        assertThat(result, is(nullValue()));
    }

    @Test
    public void findSystemByLocalUserName_ok() {
        EtxSystem result = systemDao.findSystemByLocalUserName("USER_SYS_1");
        assertThat(result, is(notNullValue()));
    }

    @Override
    protected Long getExistingId() {
        return 101L;
    }

    @Override
    protected DaoBase<EtxSystem> getDao() {
        return systemDao;
    }

    @Override
    protected EtxSystem getNewEntity() {
        EtxSystem etxSystem = new EtxSystem();
        etxSystem.setName(UUID.randomUUID().toString());
        etxSystem.setLocalUsername("LOCAL_USERNAME_1");
       // EtxUser byId = userDao.findById(10002L);
        EtxUser backendUser = new EtxUser();
        backendUser.setName(UUID.randomUUID().toString());
        backendUser.setPassword(UUID.randomUUID().toString());
        userDao.save(backendUser);
        etxSystem.setBackendUser(backendUser);
        return etxSystem;
    }
}
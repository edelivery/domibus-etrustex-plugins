package eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter;

import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.services.wsdl.documentwrapper_2.StoreDocumentWrapperRequest;
import eu.domibus.messaging.PModeMismatchException;
import eu.domibus.plugin.exception.TransformationException;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginPropertiesTestConstants;
import eu.europa.ec.etrustex.adapter.integration.handlers.StoreDocumentWrapperResponseFaultHandler;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentDirection;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.ChecksumAlgorithmType;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.testutils.SamplePartyBuilder;
import eu.europa.ec.etrustex.common.exceptions.DomibusSubmissionException;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.entity.attachment.AttachmentType;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.common.util.ConversationIdKey;
import mockit.*;
import org.hibernate.exception.GenericJDBCException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.xml.ws.Holder;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static eu.europa.ec.etrustex.adapter.testutils.ResourcesUtils.readResource;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.getObjectFromXml;
import static org.junit.Assert.*;

/**
 * @author Arun Raj
 * @version 1.0
 * @since 01/09/2017
 */
public class NodeDocumentWrapperServiceAdapterTest extends BaseAdapterUnitTest {

    private static final Path TEST_EUPL_FILE = Paths.get("src/test/resources/data/eupl1.1.-licence-en_0.pdf");

    @Injectable
    private StoreDocumentWrapperResponseFaultHandler storeDocumentWrapperResponseFaultHandler;

    @Capturing(maxInstances = 1)
    @Tested
    private NodeDocumentWrapperServiceAdapter nodeDocumentWrapperServiceAdapter;

    @Before
    public void init() {
        //LargePayloadDTO.setPath(Paths.get(ETrustExBackendPluginPropertiesTestConstants.TEST_PLUGIN_PAYLOADS_DOWNLOAD_LOCATION));
    }

    /**
     * Tests the entire flow of creation of StoreDocumentWrapperRequest - conversion from {@link EtxAttachment} to forming the request parameters of {@link ec.services.wsdl.documentwrapper_2.DocumentWrapperService}:storeDocumentWrapper
     * to forming the {@link ETrustExAdapterDTO} and submission to Domibus for the leg - nodeStoreDocumentWrapperRequest
     */
    @Test
    public void testUploadAttachment_SuccessfulSubmissionToDomibus() {

        final EtxAttachment storeDocumentWrapper_EtxAttachment = formValidEtxAttachmentForStoreDocumentWrapper();
        final String domibusMessageId = "DomibusMessageId1";

        new Expectations(){{
            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            result = domibusMessageId;
            times = 1;
        }};

        //function under test
        nodeDocumentWrapperServiceAdapter.uploadAttachment(storeDocumentWrapper_EtxAttachment, TEST_EUPL_FILE.toFile());

        new FullVerifications() {{

            nodeDocumentWrapperServiceAdapter.fillAuthorisationHeader(anyString, anyString);
            times = 1;

            nodeDocumentWrapperServiceAdapter.populateETrustExAdapterDTOForStoreDocumentWrapper(
                    withAny(new Holder<HeaderType>()),
                    withAny(new StoreDocumentWrapperRequest()),
                    storeDocumentWrapper_EtxAttachment.getMessage().getSender(),
                    null, withAny(storeDocumentWrapper_EtxAttachment),
                    TEST_EUPL_FILE.toFile());
            times = 1;

            ETrustExAdapterDTO eTrustExAdapterDTO;
            backendConnectorSubmissionService.submit(eTrustExAdapterDTO = withCapture());
            times = 1;

            storeDocumentWrapperResponseFaultHandler.process(anyString, (EtxAttachment) any);
            times = 0;

            assertEquals(storeDocumentWrapper_EtxAttachment.getId(), Long.valueOf(eTrustExAdapterDTO.getEtxBackendMessageId()));
            assertEquals(storeDocumentWrapper_EtxAttachment.getMessage().getSender().getPartyUuid(), eTrustExAdapterDTO.getAs4OriginalSender());

            assertEquals(storeDocumentWrapper_EtxAttachment.getMessage().getSender().getPartyUuid(), eTrustExAdapterDTO.getAs4FromPartyId());
            assertEquals(ConversationIdKey.OUTBOX_SEND_ATTACHMENT.getConversationIdKey() + "1", eTrustExAdapterDTO.getAs4ConversationId());
            assertEquals(ETrustExBackendPluginPropertiesTestConstants.TEST_AS4_FROMPARTYIDTYPE, eTrustExAdapterDTO.getAs4FromPartyIdType());
            assertEquals(ETrustExBackendPluginPropertiesTestConstants.TEST_AS4_FROMPARTYROLE, eTrustExAdapterDTO.getAs4FromPartyRole());
            assertEquals(ETrustExBackendPluginPropertiesTestConstants.TEST_AS4_ETRUSTEXNODEPARTYID, eTrustExAdapterDTO.getAs4ToPartyId());
            assertEquals(ETrustExBackendPluginPropertiesTestConstants.TEST_AS4_TOPARTYIDTYPE, eTrustExAdapterDTO.getAs4ToPartyIdType());
            assertEquals(ETrustExBackendPluginPropertiesTestConstants.TEST_AS4_TOPARTYROLE, eTrustExAdapterDTO.getAs4ToPartyRole());
            assertEquals(ETrustExBackendPluginPropertiesTestConstants.TEST_AS4_ETRUSTEXNODEPARTYID, eTrustExAdapterDTO.getAs4FinalRecipient());
            assertEquals(ETrustExBackendPluginPropertiesTestConstants.TEST_AS4_SERVICE_DOCUMENTWRAPPER, eTrustExAdapterDTO.getAs4Service());
            assertEquals(ETrustExBackendPluginPropertiesTestConstants.TEST_AS4_SERVICE_DOCUMENTWRAPPER_SERVICETYPE, eTrustExAdapterDTO.getAs4ServiceType());
            assertEquals(ETrustExBackendPluginPropertiesTestConstants.TEST_AS4_ACTION_STOREDOCUMENTWRAPPER_REQUEST, eTrustExAdapterDTO.getAs4Action());
            assertNotNull("Payload with content id Header should not be null", eTrustExAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_HEADER));
            assertNotNull("Payload with content id - generic should not be null", eTrustExAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_GENERIC));
            assertNotNull("Payload with content id - wrapper should not be null", eTrustExAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_DOCUMENT));
            assertEquals(3, eTrustExAdapterDTO.getAs4Payloads().size());
            assertThat(
                    getObjectFromXml(eTrustExAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_HEADER).getPayloadAsString()),
                    sameBeanAs(getObjectFromXml(readResource("data/HeaderWrapper.xml")))
            );
            assertThat(
                    getObjectFromXml(eTrustExAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_GENERIC).getPayloadAsString()),
                    sameBeanAs(getObjectFromXml(readResource("data/SubmitDocumentWrapperRequest.xml")))
                    .ignoring("documentWrapper.issueDate")
            );
        }};

        assertEquals(domibusMessageId, storeDocumentWrapper_EtxAttachment.getDomibusMessageId());
    }

    public static EtxAttachment formValidEtxAttachmentForStoreDocumentWrapper() {
        EtxMessage etxMessage = new EtxMessage();
        etxMessage.setId(1L);
        etxMessage.setMessageUuid("MsgBundleUUId_201708311600");
        etxMessage.setMessageType(MessageType.MESSAGE_BUNDLE);
        etxMessage.setMessageState(MessageState.MS_CREATED);
        etxMessage.setMessageReferenceUuid("MsgBundle_RefUUId1");
        etxMessage.setSubject("Send Message Bundle & Wrapper Subject");
        etxMessage.setMessageContent("Send Message Bundle & Wrapper Content");
        etxMessage.setSender(SamplePartyBuilder.formValid_EGREFFE_APP_PARTY_B4());
        etxMessage.setReceiver(SamplePartyBuilder.formValid_EGREFFE_NP_PARTY_B4());
        etxMessage.setDirectionType(MessageDirection.OUT);
        etxMessage.setIssueDateTime(Calendar.getInstance());
        etxMessage.setSignature("<?xml version=\"1.0\" encoding=\"UTF-8\"?> <ds:Signature xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" Id=\"xmldsig-89e4db41-2813-4891-9c17-3528b63cb89c\"> <ds:SignedInfo> <ds:CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/> <ds:SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/> <ds:Reference Id=\"xmldsig-89e4db41-2813-4891-9c17-3528b63cb89c-ref0\"> <ds:DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/> <ds:DigestValue>8LsMLbfg0nhZJkW43ovxCHRkGMWY+dxQbZC5L52/C28=</ds:DigestValue> </ds:Reference> <ds:Reference Type=\"http://uri.etsi.org/01903#SignedProperties\" URI=\"#xmldsig-89e4db41-2813-4891-9c17-3528b63cb89c-signedprops\"> <ds:DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/> <ds:DigestValue>kMEV5IwONV6E/uawpA7pgaC+TAFtYSsW4jX+X0PUaZI=</ds:DigestValue> </ds:Reference> </ds:SignedInfo> <ds:SignatureValue Id=\"xmldsig-89e4db41-2813-4891-9c17-3528b63cb89c-sigvalue\"> kMjJ2dHl2fzBt56UM6hqAkxL5aSEg3HoEJvROSlPKlAZtnW5pnxLi3yHrGipcIkTJG1z4Lh04dcg MFUepB0UwUCi/d0gK4BYLtO0nUtLZsMYe0JtE8OlPq3tXL5Zv6YrVyP1yocCn9vCVfxjhqzj7u4U RRUl7XwfUUms4H9Ak5U= </ds:SignatureValue> <ds:KeyInfo> <ds:X509Data> <ds:X509Certificate> MIICuTCCAiKgAwIBAgIEUlfqGTANBgkqhkiG9w0BAQIFADCBoDEdMBsGCSqGSIb3DQEJARYOY2hv bGFrQGlhbi5jb20xCzAJBgNVBAYTAkJFMREwDwYDVQQIDAhCcnVzc2VsczEUMBIGA1UEBwwLQmVs bGlhcmQgMjgxHDAaBgNVBAoME0V1cm9wZWFuIENvbW1pc3Npb24xETAPBgNVBAsMCERJR0lUIEIx MRgwFgYDVQQDDA9Bcm1lbl9TaWduYXR1cmUwHhcNMTMxMDExMTIwOTA1WhcNMTgxMDExMTIwOTA1 WjCBoDEdMBsGCSqGSIb3DQEJARYOY2hvbGFrQGlhbi5jb20xCzAJBgNVBAYTAkJFMREwDwYDVQQI DAhCcnVzc2VsczEUMBIGA1UEBwwLQmVsbGlhcmQgMjgxHDAaBgNVBAoME0V1cm9wZWFuIENvbW1p c3Npb24xETAPBgNVBAsMCERJR0lUIEIxMRgwFgYDVQQDDA9Bcm1lbl9TaWduYXR1cmUwgZ8wDQYJ KoZIhvcNAQEBBQADgY0AMIGJAoGBAJU0MiD7Tn7mUwjdA75r5tPKK8r+DlyhWHBml44gz+Tfq7D4 f9nAZ3B4meNYKftocRxbLcc0u6HTYMhGPfchyUIPfthRj8SE64zjpKc2MX6XIE0LydHplRqBUfSe iW9EiFCflRlBP6Zw2jjw1AZxllLqew/tzs8DwDovv72XzM/3AgMBAAEwDQYJKoZIhvcNAQECBQAD gYEABFDUnv/4k2ABVwVCd3y1zIW5fgpAPdR6oT5CFa7Y4ZXaffrLekaQrEGUhUTzxthf1lZaz2t8 71WSpHVLSJfsIgRTjy6oTLbLRvMn510wVpozu3dFfjaK9edg3z/rhJgkYEQxkBB/3W9gM9gj1WES qkqFat+Eahj89NEYo7G+NVI= </ds:X509Certificate> </ds:X509Data> </ds:KeyInfo> <ds:Object> <xades:QualifyingProperties xmlns:xades=\"http://uri.etsi.org/01903/v1.3.2#\" xmlns:xades141=\"http://uri.etsi.org/01903/v1.4.1#\" Target=\"#xmldsig-89e4db41-2813-4891-9c17-3528b63cb89c\"> <xades:SignedProperties Id=\"xmldsig-89e4db41-2813-4891-9c17-3528b63cb89c-signedprops\"> <xades:SignedSignatureProperties> <xades:SigningTime>2013-10-16T11:11:59.175+02:00</xades:SigningTime> <xades:SigningCertificate> <xades:Cert> <xades:CertDigest> <ds:DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/> <ds:DigestValue>ldvmaLRyOg4X1o04+TTthFtDmqJFoVW06Ugo69jPkqA=</ds:DigestValue> </xades:CertDigest> <xades:IssuerSerial> <ds:X509IssuerName>CN=Armen_Signature,OU=DIGIT B1,O=European Commission,L=Belliard 28,ST=Brussels,C=BE,1.2.840.113549.1.9.1=#160e63686f6c616b4069616e2e636f6d</ds:X509IssuerName> <ds:X509SerialNumber>1381493273</ds:X509SerialNumber> </xades:IssuerSerial> </xades:Cert> </xades:SigningCertificate> </xades:SignedSignatureProperties> </xades:SignedProperties> </xades:QualifyingProperties> </ds:Object> </ds:Signature>");
        etxMessage.setSignedData("<signedBundle:signedBundle xmlns:signedBundle=\"urn:eu:europa:ec:etrustex:signature:v1.0\" xmlns=\"urn:eu:europa:ec:etrustex:signature:v1.0\"> <document> <id>DEV-ETX-ARMEN-PARTY-EUPL001.pdf-958413632273236825</id> <digestMethod>SHA-512</digestMethod> <digestValue>26316A5CAE50C2234CA16717C1F1CBE7B0330F7AA663BB94BEB622F1D4751634B4CA4A8CF4298974214C40353C5E6FAEE954561830871B5EFAC02776E9F925CB</digestValue> </document> </signedBundle:signedBundle>");


        EtxAttachment etxAttachment = new EtxAttachment();
        etxAttachment.setId(1L);
        etxAttachment.setAttachmentUuid("AttachmentUUID_1");
        etxAttachment.setAttachmentType(AttachmentType.BINARY);
        etxAttachment.setStateType(AttachmentState.ATTS_UPLOADED);
        etxAttachment.setDirectionType(AttachmentDirection.OUTGOING);
        etxAttachment.setFileName(TEST_EUPL_FILE.toString());
        etxAttachment.setFilePath("RELATIVE_PATH");
        etxAttachment.setFileSize(41929L);
        etxAttachment.setMimeType("application/pdf");
        etxAttachment.setChecksum("2C53A498EB26BB8AFB00D05B7379464C089A16CE178CCF645AA9902D0491101981330380271C9F79C00E60B54A36BFBDA0A8CA1F3A4B6CCF8F4CA9207D59181C".getBytes());
        etxAttachment.setChecksumAlgorithmType(ChecksumAlgorithmType.SHA_512);
        etxAttachment.setSessionKey("A82E3C2CDF4204E0A17B0F3EE2128EE76E27D7A44460E6DFD221BBE1D9832A3EDD8E037CC094176FC2373372CD67C3F49E8EF5AE39DFC9728CD38130B6B561DEB1566D1AB57684A7B535C2E7E93D1D1D1C4B89AF75A841FFF83EFED527C5550347B04980B3A90E796C34C63142BDCAC54FFCDC978CA0AC4153AE5E187AD358BA".getBytes());
        etxAttachment.setActiveState(true);
        etxAttachment.setMessage(etxMessage);

        etxMessage.addAttachment(etxAttachment);

        return etxAttachment;
    }

    /**
     * Test handling of synchronous errors (MessageProcessingException) thrown on Submission to Domibus.
     * A mandatory AS4 element e.g: AS4 action is missig
     *
     */
    @Test
    public void testUploadAttachment_ValidationFailureOnDomibusSubmission_MissingMandatoryElement() {

        final EtxAttachment storeDocumentWrapper_EtxAttachment = formValidEtxAttachmentForStoreDocumentWrapper();
        final TransformationException mpEx = new TransformationException("Sample Validation Error from Domibus", new IllegalArgumentException("action must not be empty"));
        final DomibusSubmissionException etEx = new DomibusSubmissionException( "Sample Validation Error from Domibus", mpEx);

        new Expectations() {{
            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            times = 1;
            result = etEx;
        }};

        //function under test
        nodeDocumentWrapperServiceAdapter.uploadAttachment(storeDocumentWrapper_EtxAttachment, TEST_EUPL_FILE.toFile());

        new FullVerifications() {{
            storeDocumentWrapperResponseFaultHandler.process(anyString, (EtxAttachment) any);
            times = 1;
        }};
    }

    /**
     * Test handling of synchronous errors (MessageProcessingException) thrown on Submission to Domibus.
     * pMode is incorrectly modified and a required property (e.g: - eTxBackendMsgIdProperty) is missing.
     *
     */
    @Test
    public void testUploadAttachment_ValidationFailureOnDomibusSubmission_PModeMismatch() {

        final EtxAttachment storeDocumentWrapper_EtxAttachment = formValidEtxAttachmentForStoreDocumentWrapper();
        final PModeMismatchException mpEx = new PModeMismatchException("Property profiling for this exchange does not include a property named [eTxBackendMsgId]");
        final DomibusSubmissionException  etEx= new DomibusSubmissionException("Sample Validation Error from Domibus", mpEx);

        new Expectations() {{
            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            times = 1;
            result = etEx;
        }};

        //function under test
        nodeDocumentWrapperServiceAdapter.uploadAttachment(storeDocumentWrapper_EtxAttachment, TEST_EUPL_FILE.toFile());

        new FullVerifications() {{
            storeDocumentWrapperResponseFaultHandler.process(anyString, (EtxAttachment) any);
            times = 1;
        }};
    }

    /**
     * Scenario when other types of exception are encountered in Domibus Submission
     *
     */
    @Test
    public void testUploadAttachment_Exception() {

        final EtxAttachment storeDocumentWrapper_EtxAttachment = formValidEtxAttachmentForStoreDocumentWrapper();

        final GenericJDBCException jbbcException = new GenericJDBCException("Could not open connection", new java.sql.SQLException("Unexpected exception while enlisting XAConnection", new java.sql.SQLException("Transaction rolled back: Transaction timed out after 215 seconds")));

        new Expectations() {{
            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            times = 1;
            result = jbbcException;
        }};

        try {
            //function under test
            nodeDocumentWrapperServiceAdapter.uploadAttachment(storeDocumentWrapper_EtxAttachment, TEST_EUPL_FILE.toFile());
            Assert.fail("Expected eTrustEx exception to be thrown");
        } catch (ETrustExPluginException etxAE) {
            assertEquals(ETrustExError.DEFAULT.getCode(), etxAE.getError().getCode());
        }

        new FullVerifications() {{
            storeDocumentWrapperResponseFaultHandler.process(anyString, (EtxAttachment) any);
            times = 0;
        }};
    }


    @Test
    public void testPopulateETrustExAdapterDTOForStoreDocumentWrapper_NullInputForPayload() {
        try {
            EtxAttachment etxAttachment = formValidEtxAttachmentForStoreDocumentWrapper();
            //function under test
            nodeDocumentWrapperServiceAdapter.populateETrustExAdapterDTOForStoreDocumentWrapper(null, null, etxAttachment.getMessage().getSender(), null, formValidEtxAttachmentForStoreDocumentWrapper(), null);
            Assert.fail("Expected Null pointer exception here");
        } catch (NullPointerException npe) {
            assert true;
        }
    }
}

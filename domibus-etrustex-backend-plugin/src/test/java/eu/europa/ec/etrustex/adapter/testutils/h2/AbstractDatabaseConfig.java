package eu.europa.ec.etrustex.adapter.testutils.h2;

import liquibase.integration.spring.SpringLiquibase;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;
import java.util.Map;
import java.util.Properties;

/**
 * @author François Gautier
 * @version 1.0
 * @since 30/01/2018
 */
@Configuration
@EnableCaching
public abstract class AbstractDatabaseConfig {

    abstract DataSource dataSource();

    abstract Map<Object, String> getProperties();


    @Bean
    public SpringLiquibase springLiquibase() {
        SpringLiquibase springLiquibase = new SpringLiquibase();
        springLiquibase.setChangeLog("classpath:liquibase/master_test.xml");
        springLiquibase.setDataSource(dataSource());
        springLiquibase.setDropFirst(true);
        springLiquibase.setShouldRun(true);
        return springLiquibase;
    }

    @Bean
    @DependsOn("springLiquibase")
    public LocalContainerEntityManagerFactoryBean domibusJTA() {
        LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        localContainerEntityManagerFactoryBean.setPackagesToScan("eu.europa.ec.etrustex.adapter.model.entity");
        localContainerEntityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        localContainerEntityManagerFactoryBean.setPersistenceUnitName("domibusJTA");
        Properties jpaProperties = new Properties();

        jpaProperties.put("hibernate.connection.charSet", "8");
        jpaProperties.put("hibernate.transaction.flush_before_completion", "true");
        jpaProperties.put("hibernate.format_sql", "true");
        jpaProperties.put("hibernate.show_sql", "false");
        jpaProperties.put("hibernate.generate_statistics", "true");
        jpaProperties.put("hibernate.jdbc.batch_size", "30");
        jpaProperties.put("hibernate.connection.release_mode", "auto");
        jpaProperties.put("hibernate.order_updates", "true");
        jpaProperties.put("hibernate.order_inserts", "true");
        jpaProperties.put("hibernate.transaction.auto_close_session", "true");
        jpaProperties.put("hibernate.archive.autodetection", "class");

        jpaProperties.putAll(getProperties());
        localContainerEntityManagerFactoryBean.setJpaProperties(jpaProperties);
        localContainerEntityManagerFactoryBean.setDataSource(dataSource());
        return localContainerEntityManagerFactoryBean;
    }


    @Bean
    public JpaTransactionManager jpaTransactionManager() {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setEntityManagerFactory(domibusJTA().getObject());
        return jpaTransactionManager;
    }

}

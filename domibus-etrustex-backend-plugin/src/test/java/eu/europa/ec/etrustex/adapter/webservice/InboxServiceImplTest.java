package eu.europa.ec.etrustex.adapter.webservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.model.vo.MessageReferenceVO;
import eu.europa.ec.etrustex.adapter.service.security.IdentityManager;
import eu.europa.ec.etrustex.adapter.testutils.JsonUtils;
import eu.europa.ec.etrustex.integration.model.common.v2.EtxMessageReferenceType;
import eu.europa.ec.etrustex.integration.service.inbox.v2.*;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.Collections;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.getObjectFromXml;
import static eu.europa.ec.etrustex.adapter.testutils.ResourcesUtils.readResource;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author François Gautier
 * @version 1.0
 * @since 12-Oct-17
 */
@RunWith(JMockit.class)
@SuppressWarnings("Duplicates")
public class InboxServiceImplTest {

    @Injectable
    private IdentityManager identityManager;

    @Injectable
    private MessageServicePlugin messageService;

    @Tested
    private InboxServiceImpl inboxService;

    @Test
    public void getMessageBundle_noUser() throws Exception {
        final GetMessageBundleRequestType getMessageBundleRequestType =
                getObjectFromXml(readResource("data/getMessageBundleRequest.xml"));
        final String userName = null;
        final String receiverParty = getMessageBundleRequestType.getMessageReference().getReceiver().getId();

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = userName;

            identityManager.hasAccess(receiverParty, userName);
            times = 1;
            result = false;
        }};

        try {
            inboxService.getMessageBundle(getMessageBundleRequestType);
            fail();
        } catch (FaultResponse e) {
            assertThat(e.getMessage(), containsString(ETrustExError.ETX_001.getDescription()));
            assertThat(
                    e.getFaultInfo().getDetail(),
                    containsString("Authenticated user: " + userName + " does not have access to party: " + receiverParty));
        }
    }

    @Test
    public void getMessageBundle_userNotAuthenticated() throws Exception {
        final GetMessageBundleRequestType getMessageBundleRequestType =
                getObjectFromXml(readResource("data/getMessageBundleRequest.xml"));
        final String userName = "NotKnownUser";
        final String receiverParty = getMessageBundleRequestType.getMessageReference().getReceiver().getId();

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = userName;

            identityManager.hasAccess(receiverParty, userName);
            times = 1;
            result = false;
        }};

        try {
            inboxService.getMessageBundle(getMessageBundleRequestType);
            fail();
        } catch (FaultResponse e) {
            assertThat(e.getMessage(), containsString(ETrustExError.ETX_001.getDescription()));
            assertThat(
                    e.getFaultInfo().getDetail(),
                    containsString("Authenticated user: " + userName + " does not have access to party: " + receiverParty));
        }
    }

    @Test
    public void getMessageBundle_MessageNotFound_Exception() throws Exception {
        final GetMessageBundleRequestType getMessageBundleRequestType =
                getObjectFromXml(readResource("data/getMessageBundleRequest.xml"));
        final String userName = "knownUser";
        final EtxMessageReferenceType messageReference = getMessageBundleRequestType.getMessageReference();
        final String receiverParty = messageReference.getReceiver().getId();
        final String senderParty = messageReference.getSender().getId();
        final String messageReferenceId = messageReference.getId();

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = userName;

            identityManager.hasAccess(receiverParty, userName);
            times = 1;
            result = true;

            messageService.readMessage(
                    senderParty,
                    receiverParty,
                    messageReferenceId,
                    singletonList(MessageType.MESSAGE_BUNDLE));
            times = 1;
            result = new IllegalArgumentException();
        }};

        try {
            inboxService.getMessageBundle(getMessageBundleRequestType);
            fail();
        } catch (FaultResponse e) {
            assertThat(e.getMessage(), nullValue());
        }
    }

    @Test
    public void getMessageBundle_MessageNotOK() throws Exception {
        final GetMessageBundleRequestType getMessageBundleRequestType =
                getObjectFromXml(readResource("data/getMessageBundleRequest.xml"));
        final String userName = "knownUser";
        final EtxMessageReferenceType messageReference = getMessageBundleRequestType.getMessageReference();
        final String receiverParty = messageReference.getReceiver().getId();
        final String senderParty = messageReference.getSender().getId();
        final String messageReferenceId = messageReference.getId();

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = userName;

            identityManager.hasAccess(receiverParty, userName);
            times = 1;
            result = true;

            messageService.readMessage(
                    senderParty,
                    receiverParty,
                    messageReferenceId,
                    singletonList(MessageType.MESSAGE_BUNDLE));
            times = 1;
            result = new EtxMessage();
        }};

        try {
            inboxService.getMessageBundle(getMessageBundleRequestType);
            fail();
        } catch (FaultResponse e) {
            assertThat(e.getMessage(), nullValue());
        }
    }

    @Test
    public void getMessageBundle_MessageNotFound_null() throws Exception {
        final GetMessageBundleRequestType getMessageBundleRequestType =
                getObjectFromXml(readResource("data/getMessageBundleRequest.xml"));
        final String userName = "knownUser";
        final EtxMessageReferenceType messageReference = getMessageBundleRequestType.getMessageReference();
        final String receiverParty = messageReference.getReceiver().getId();
        final String senderParty = messageReference.getSender().getId();
        final String messageReferenceId = messageReference.getId();

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = userName;

            identityManager.hasAccess(receiverParty, userName);
            times = 1;
            result = true;

            messageService.readMessage(
                    senderParty,
                    receiverParty,
                    messageReferenceId,
                    singletonList(MessageType.MESSAGE_BUNDLE));
            times = 1;
            result = null;
        }};

        GetMessageBundleResponseType messageBundle = inboxService.getMessageBundle(getMessageBundleRequestType);

        assertThat(messageBundle.getMessageBundle(), nullValue());
    }

    @Test
    public void getMessageBundle_HappyFlow() throws Exception {
        final GetMessageBundleRequestType getMessageBundleRequestType =
                getObjectFromXml(readResource("data/getMessageBundleRequest.xml"));
        final String userName = "knownUser";
        final EtxMessageReferenceType messageReference = getMessageBundleRequestType.getMessageReference();
        final String receiverParty = messageReference.getReceiver().getId();
        final String senderParty = messageReference.getSender().getId();
        final String messageReferenceId = messageReference.getId();

        final EtxMessage etxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageBundle_IN_Valid_OneAttachment.json"),
                EtxMessage.class);

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = userName;

            identityManager.hasAccess(receiverParty, userName);
            times = 1;
            result = true;

            messageService.readMessage(
                    senderParty,
                    receiverParty,
                    messageReferenceId,
                    singletonList(MessageType.MESSAGE_BUNDLE));
            times = 1;
            result = etxMessage;
        }};

        GetMessageBundleResponseType actual = inboxService.getMessageBundle(getMessageBundleRequestType);

        GetMessageBundleResponseType expected = getObjectFromXml(readResource("data/getMessageBundleResponse_OneAttachment.xml"));
        assertThat(actual,sameBeanAs(expected)
                .ignoring("messageBundle.issueDateTime.iChronology")
                .ignoring("messageBundle.issueDateTime.iMillis")
                .ignoring("messageBundle.receivedDateTime.iChronology")
                .ignoring("messageBundle.receivedDateTime.iMillis")
        );
    }

    @Test
    public void getMessageStatus_noUser() throws Exception {
        final GetMessageStatusRequestType getMessageStatusRequestType =
                getObjectFromXml(readResource("data/getMessageStatusRequest.xml"));
        final String userName = null;
        final String receiverParty = getMessageStatusRequestType.getMessageReference().getReceiver().getId();

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = userName;

            identityManager.hasAccess(receiverParty, userName);
            times = 1;
            result = false;
        }};

        try {
            inboxService.getMessageStatus(getMessageStatusRequestType);
            fail();
        } catch (FaultResponse e) {
            assertThat(e.getMessage(), containsString(ETrustExError.ETX_001.getDescription()));
            assertThat(
                    e.getFaultInfo().getDetail(),
                    containsString("Authenticated user: " + userName + " does not have access to party: " + receiverParty));
        }
    }

    @Test
    public void getMessageStatus_userNotAuthenticated() throws Exception {
        final GetMessageStatusRequestType getMessageStatusRequestType =
                getObjectFromXml(readResource("data/getMessageStatusRequest.xml"));
        final String userName = "NotKnownUser";
        final String receiverParty = getMessageStatusRequestType.getMessageReference().getReceiver().getId();

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = userName;

            identityManager.hasAccess(receiverParty, userName);
            times = 1;
            result = false;
        }};

        try {
            inboxService.getMessageStatus(getMessageStatusRequestType);
            fail();
        } catch (FaultResponse e) {
            assertThat(e.getMessage(), containsString(ETrustExError.ETX_001.getDescription()));
            assertThat(
                    e.getFaultInfo().getDetail(),
                    containsString("Authenticated user: " + userName + " does not have access to party: " + receiverParty));
        }
    }

    @Test
    public void getMessageStatus_MessageNotFound_Exception() throws Exception {
        final GetMessageStatusRequestType getMessageStatusRequestType =
                getObjectFromXml(readResource("data/getMessageStatusRequest.xml"));
        final String userName = "knownUser";
        final EtxMessageReferenceType messageReference = getMessageStatusRequestType.getMessageReference();
        final String receiverParty = messageReference.getReceiver().getId();
        final String senderParty = messageReference.getSender().getId();
        final String messageReferenceId = messageReference.getId();

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = userName;

            identityManager.hasAccess(receiverParty, userName);
            times = 1;
            result = true;

            messageService.readMessage(
                    senderParty,
                    receiverParty,
                    messageReferenceId,
                    Arrays.asList(MessageType.MESSAGE_STATUS, MessageType.MESSAGE_ADAPTER_STATUS));
            times = 1;
            result = new IllegalArgumentException();
        }};

        try {
            inboxService.getMessageStatus(getMessageStatusRequestType);
            fail();
        } catch (FaultResponse e) {
            assertThat(e.getMessage(), nullValue());
        }
    }

    @Test
    public void getMessageStatus_MessageNotOK() throws Exception {
        final GetMessageStatusRequestType getMessageStatusRequestType =
                getObjectFromXml(readResource("data/getMessageStatusRequest.xml"));
        final String userName = "knownUser";
        final EtxMessageReferenceType messageReference = getMessageStatusRequestType.getMessageReference();
        final String receiverParty = messageReference.getReceiver().getId();
        final String senderParty = messageReference.getSender().getId();
        final String messageReferenceId = messageReference.getId();

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = userName;

            identityManager.hasAccess(receiverParty, userName);
            times = 1;
            result = true;

            messageService.readMessage(
                    senderParty,
                    receiverParty,
                    messageReferenceId,
                    Arrays.asList(MessageType.MESSAGE_STATUS, MessageType.MESSAGE_ADAPTER_STATUS));
            times = 1;
            result = new EtxMessage();
        }};

        try {
            inboxService.getMessageStatus(getMessageStatusRequestType);
            fail();
        } catch (FaultResponse e) {
            assertThat(e.getMessage(), nullValue());
        }
    }

    @Test
    public void getMessageStatus_MessageNotFound_null() throws Exception {
        final GetMessageStatusRequestType getMessageStatusRequestType =
                getObjectFromXml(readResource("data/getMessageStatusRequest.xml"));
        final String userName = "knownUser";
        final EtxMessageReferenceType messageReference = getMessageStatusRequestType.getMessageReference();
        final String receiverParty = messageReference.getReceiver().getId();
        final String senderParty = messageReference.getSender().getId();
        final String messageReferenceId = messageReference.getId();

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = userName;

            identityManager.hasAccess(receiverParty, userName);
            times = 1;
            result = true;

            messageService.readMessage(
                    senderParty,
                    receiverParty,
                    messageReferenceId,
                    Arrays.asList(MessageType.MESSAGE_STATUS, MessageType.MESSAGE_ADAPTER_STATUS));
            times = 1;
            result = null;
        }};

        GetMessageStatusResponseType messageBundle = inboxService.getMessageStatus(getMessageStatusRequestType);

        assertThat(messageBundle.getMessageStatus(), nullValue());
    }

    @Test
    public void getMessageStatus_HappyFlow() throws Exception {
        final GetMessageStatusRequestType getMessageStatusRequestType =
                getObjectFromXml(readResource("data/getMessageStatusRequest.xml"));
        final String userName = "knownUser";
        final EtxMessageReferenceType messageReference = getMessageStatusRequestType.getMessageReference();
        final String receiverParty = messageReference.getReceiver().getId();
        final String senderParty = messageReference.getSender().getId();
        final String messageReferenceId = messageReference.getId();

        ObjectMapper objectMapper = new ObjectMapper();
        final EtxMessage etxMessage = objectMapper.readValue(
                readResource("data/EtxMessageBundle_IN_Valid_OneAttachment.json"),
                EtxMessage.class);

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = userName;

            identityManager.hasAccess(receiverParty, userName);
            times = 1;
            result = true;

            messageService.readMessage(
                    senderParty,
                    receiverParty,
                    messageReferenceId,
                    Arrays.asList(MessageType.MESSAGE_STATUS, MessageType.MESSAGE_ADAPTER_STATUS));
            times = 1;
            result = etxMessage;
        }};

        GetMessageStatusResponseType messageStatus = inboxService.getMessageStatus(getMessageStatusRequestType);

        assertThat(
                messageStatus,
                sameBeanAs(getObjectFromXml(readResource("data/getMessageStatusResponse_Delivered.xml")
                ))
                        .ignoring("messageStatus.issueDateTime.iChronology")
                        .ignoring("messageStatus.issueDateTime.iMillis")
                        .ignoring("messageStatus.receivedDateTime.iChronology")
                        .ignoring("messageStatus.receivedDateTime.iMillis")
        );
    }

    @Test
    public void getMessageReferenceList_noUser() throws Exception {
        final GetMessageReferenceListRequestType getMessageStatusRequestType =
                getObjectFromXml(readResource("data/getMessageRefListRequest.xml"));
        final String userName = null;
        final String receiverParty = getMessageStatusRequestType.getReceiver().getId();

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = userName;

            identityManager.hasAccess(receiverParty, userName);
            times = 1;
            result = false;
        }};

        try {
            inboxService.getMessageReferenceList(getMessageStatusRequestType);
            fail();
        } catch (FaultResponse e) {
            assertThat(e.getMessage(), containsString(ETrustExError.ETX_001.getDescription()));
            assertThat(
                    e.getFaultInfo().getDetail(),
                    containsString("Authenticated user: " + userName + " does not have access to party: " + receiverParty));
        }
    }

    @Test
    public void getMessageReferenceList_userNotAuthenticated() throws Exception {
        final GetMessageReferenceListRequestType getMessageStatusRequestType =
                getObjectFromXml(readResource("data/getMessageRefListRequest.xml"));
        final String userName = "NotKnownUser";
        final String receiverParty = getMessageStatusRequestType.getReceiver().getId();

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = userName;

            identityManager.hasAccess(receiverParty, userName);
            times = 1;
            result = false;
        }};

        try {
            inboxService.getMessageReferenceList(getMessageStatusRequestType);
            fail();
        } catch (FaultResponse e) {
            assertThat(e.getMessage(), containsString(ETrustExError.ETX_001.getDescription()));
            assertThat(
                    e.getFaultInfo().getDetail(),
                    containsString("Authenticated user: " + userName + " does not have access to party: " + receiverParty));
        }
    }

    @Test
    public void getMessageReferenceList_MessageNotFound_Exception() throws Exception {
        final GetMessageReferenceListRequestType getMessageStatusRequestType =
                getObjectFromXml(readResource("data/getMessageRefListRequest.xml"));
        final String userName = "knownUser";
        final String receiverParty = getMessageStatusRequestType.getReceiver().getId();

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = userName;

            identityManager.hasAccess(receiverParty, userName);
            times = 1;
            result = true;

            messageService.findMessagesForReceiverPartyUuid(receiverParty);
            times = 1;
            result = new IllegalArgumentException();
        }};

        try {
            inboxService.getMessageReferenceList(getMessageStatusRequestType);
            fail();
        } catch (FaultResponse e) {
            assertThat(e.getMessage(), nullValue());
        }
    }

    @Test
    public void getMessageReferenceList_MessageNotOK() throws Exception {
        final GetMessageReferenceListRequestType getMessageStatusRequestType =
                getObjectFromXml(readResource("data/getMessageRefListRequest.xml"));
        final String userName = "knownUser";
        final String receiverParty = getMessageStatusRequestType.getReceiver().getId();

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = userName;

            identityManager.hasAccess(receiverParty, userName);
            times = 1;
            result = true;

            messageService.findMessagesForReceiverPartyUuid(receiverParty);
            times = 1;
            result = Collections.singletonList(new MessageReferenceVO(null, null, null, null));
        }};

        try {
            inboxService.getMessageReferenceList(getMessageStatusRequestType);
            fail();
        } catch (FaultResponse e) {
            assertThat(e.getMessage(), nullValue());
        }
    }

    @Test
    public void getMessageReferenceList_MessageNotFound_null() throws Exception {
        final GetMessageReferenceListRequestType getMessageStatusRequestType =
                getObjectFromXml(readResource("data/getMessageRefListRequest.xml"));
        final String userName = "knownUser";
        final String receiverParty = getMessageStatusRequestType.getReceiver().getId();

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = userName;

            identityManager.hasAccess(receiverParty, userName);
            times = 1;
            result = true;

            messageService.findMessagesForReceiverPartyUuid(receiverParty);
            times = 1;
            result = null;
        }};

        GetMessageReferenceListResponseType messageBundle = inboxService.getMessageReferenceList(getMessageStatusRequestType);

        assertThat(messageBundle.getMessageReferenceList().getMessageReference().size(), is(0));
    }

    @Test
    public void getMessageReferenceList_HappyFlow() throws Exception {
        final GetMessageReferenceListRequestType getMessageStatusRequestType =
                getObjectFromXml(readResource("data/getMessageRefListRequest.xml"));
        final String userName = "knownUser";
        final String receiverParty = getMessageStatusRequestType.getReceiver().getId();

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = userName;

            identityManager.hasAccess(receiverParty, userName);
            times = 1;
            result = true;

            messageService.findMessagesForReceiverPartyUuid(receiverParty);
            times = 1;
            result = Arrays.asList(
                    new MessageReferenceVO("1", MessageType.MESSAGE_STATUS, "1", "2"),
                    new MessageReferenceVO("2", MessageType.MESSAGE_ADAPTER_STATUS, "1", "2"),
                    new MessageReferenceVO("3", MessageType.MESSAGE_BUNDLE, "1", "2"));
        }};

        GetMessageReferenceListResponseType actual = inboxService.getMessageReferenceList(getMessageStatusRequestType);

        GetMessageReferenceListResponseType expected =
                getObjectFromXml(readResource("data/getMessageRefListResponse_3Messages.xml"));
        assertThat(actual, sameBeanAs(expected));
    }
}
package eu.europa.ec.etrustex.adapter.queue.out.downloadattachment;

import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.service.WorkspaceService;
import eu.europa.ec.etrustex.adapter.integration.client.backend.repository.FileRepository;
import eu.europa.ec.etrustex.adapter.integration.client.backend.repository.FileRepositoryProvider;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter.NodeDocumentWrapperServiceAdapterTest;
import eu.europa.ec.etrustex.adapter.model.entity.administration.BackendFileRepositoryLocationType;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystemConfig;
import eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.model.vo.FileAttachmentVO;
import eu.europa.ec.etrustex.adapter.persistence.api.attachment.AttachmentDao;
import eu.europa.ec.etrustex.adapter.queue.out.uploadattachment.OutUploadAttachmentProducer;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import eu.europa.ec.etrustex.adapter.support.Util;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.apache.commons.codec.binary.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static eu.europa.ec.etrustex.adapter.model.entity.administration.BackendFileRepositoryLocationType.BACKEND_FILE_REPOSITORY_SERVICE;

/**
 * JUnit class for <code>OutDownloadAttachmentHandlerImpl</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@RunWith(JMockit.class)
public class OutDownloadAttachmentHandlerImplTest {
    private final static byte[] SESSION_KEY = StringUtils.getBytesUtf8("session key test");
    private final static String X_509_SUBJECT_NAME = "test subject name";

    private final Map<SystemConfigurationProperty, String> senderSystemConfig = new HashMap<>();

    @Injectable
    private AttachmentDao attachmentDao;

    @Tested
    private OutDownloadAttachmentHandlerImpl outDownloadAttachmentHandler;

    @Injectable
    private AttachmentService attachmentService;

    @Injectable
    private FileRepositoryProvider fileRepositoryProvider;

    @Injectable
    private OutUploadAttachmentProducer outUploadAttachmentProducer;

    @Injectable
    private PostProcessProducer postProcessProducer;

    @Injectable
    private WorkspaceService etxBackendPluginWorkspaceService;

    @Mocked
    private FileRepository fileRepository;

    private EtxAttachment attachmentEntity;
    private FileAttachmentVO fileAttachmentVO;

    private Long attachmentId = 10L;
    private Long messageId = 20L;

    @Before
    public void setUp() {

        senderSystemConfig.put(SystemConfigurationProperty.ETX_BACKEND_FILE_REPOSITORY_LOCATION, BackendFileRepositoryLocationType.BACKEND_FILE_REPOSITORY_SERVICE.name());

        //static methods mock-ups
        new MockUp<Util>() {
            @Mock
            Map<SystemConfigurationProperty, String> buildSystemConfig(List<EtxSystemConfig> etxSystemConfigs) {
                return senderSystemConfig;
            }
        };

        attachmentEntity = NodeDocumentWrapperServiceAdapterTest.formValidEtxAttachmentForStoreDocumentWrapper();
        attachmentEntity.getMessage().getSender().getSystem().setName("EGREFFE-SYS");

        fileAttachmentVO = new FileAttachmentVO();
        fileAttachmentVO.setSessionKey(SESSION_KEY);
        fileAttachmentVO.setX509SubjectName(X_509_SUBJECT_NAME);
        fileAttachmentVO.setDataHandler(new DataHandler(new DataSource() {
            @Override
            public InputStream getInputStream() {
                return null;
            }

            @Override
            public OutputStream getOutputStream() {
                return null;
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }
        }));
    }

    @Test
    public void testHandleDownloadAttachment_HappyFlow() throws Exception {

        final Long attachmentId = 10L;

        new Expectations() {{
            attachmentService.findById(attachmentId);
            result = attachmentEntity;
            times = 1;

            attachmentService.checkAttachmentForSystemInState(attachmentEntity, attachmentEntity.getMessage().getSender().getSystem(),
                    Arrays.asList(AttachmentState.ATTS_DOWNLOADED, AttachmentState.ATTS_UPLOADED));
            result = false;
            times = 1;

            fileRepositoryProvider.getFileRepository(BACKEND_FILE_REPOSITORY_SERVICE);
            result = fileRepository;
            times = 1;

            fileRepository.readFile(senderSystemConfig, attachmentEntity);
            result = fileAttachmentVO;
            times = 1;
        }};

        outDownloadAttachmentHandler.handleDownloadAttachment(attachmentId);

        new FullVerifications() {{
            etxBackendPluginWorkspaceService.writeFile(attachmentId, fileAttachmentVO.getDataHandler().getInputStream());
            times = 1;

            fileRepository.releaseResources(fileAttachmentVO);
            times = 1;

            EtxAttachment etxAttachmentActual;
            attachmentService.updateAttachmentState(etxAttachmentActual = withCapture(), AttachmentState.ATTS_CREATED, AttachmentState.ATTS_DOWNLOADED);
            times = 1;
            Assert.assertEquals("attachment entity should be the same", attachmentEntity, etxAttachmentActual);

            Long attachmentIdActual;
            outUploadAttachmentProducer.triggerUpload(attachmentIdActual = withCapture());
            times = 1;
            Assert.assertEquals("attachmentId must match", attachmentId, attachmentIdActual);

        }};

        Assert.assertTrue("session key must match", Arrays.equals(SESSION_KEY, attachmentEntity.getSessionKey()));
        Assert.assertEquals("subject must match", X_509_SUBJECT_NAME, attachmentEntity.getX509SubjectName());
    }

    @Test
    public void testHandleDownloadAttachment_AlreadyDownloaded() {

        new Expectations() {{
            attachmentService.findById(attachmentId);
            result = attachmentEntity;
            times = 1;

            //attachment already downloaded
            attachmentService.checkAttachmentForSystemInState(attachmentEntity,
                    attachmentEntity.getMessage().getSender().getSystem(),
                    Arrays.asList(AttachmentState.ATTS_DOWNLOADED, AttachmentState.ATTS_UPLOADED));
            result = true;
            times = 1;

        }};

        //method to test
        outDownloadAttachmentHandler.handleDownloadAttachment(attachmentId);

        new FullVerifications() {{

            EtxAttachment etxAttachmentActual;
            attachmentService.updateAttachmentState(etxAttachmentActual = withCapture(), AttachmentState.ATTS_CREATED, AttachmentState.ATTS_DOWNLOADED);
            times = 1;
            Assert.assertEquals("attachment entity should be the same", attachmentEntity, etxAttachmentActual);

            Long attachmentIdActual;
            outUploadAttachmentProducer.triggerUpload(attachmentIdActual = withCapture());
            times = 1;
            Assert.assertEquals("attachmentId must match", attachmentId, attachmentIdActual);

        }};
    }

    @Test
    public void testHandleDownloadAttachment_ReadingError() throws Exception {

        new Expectations() {{
            attachmentService.findById(attachmentId);
            result = attachmentEntity;
            times = 1;

            attachmentService.checkAttachmentForSystemInState(attachmentEntity, attachmentEntity.getMessage().getSender().getSystem(),
                    Arrays.asList(AttachmentState.ATTS_DOWNLOADED, AttachmentState.ATTS_UPLOADED));
            result = false;
            times = 1;

            fileRepositoryProvider.getFileRepository(BACKEND_FILE_REPOSITORY_SERVICE);
            result = fileRepository;
            times = 1;

            // error reading attachment
            fileRepository.readFile(senderSystemConfig, attachmentEntity);
            result = null;
            times = 1;

        }};

        try {
            //method to test
            outDownloadAttachmentHandler.handleDownloadAttachment(attachmentId);
            Assert.fail();

        } catch (ETrustExPluginException e) {
            Assert.assertEquals("error code should match", ETrustExError.ETX_007, e.getError());
            Assert.assertTrue("exception message should match", e.getMessage().contains("Reading of attachment file failed: " + attachmentEntity.getId()));

        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void testValidateDownloadAttachment_HappyFlow() {
        attachmentEntity.setStateType(AttachmentState.ATTS_CREATED);
        attachmentEntity.getMessage().setMessageState(MessageState.MS_CREATED);
        new Expectations() {{
            attachmentService.findById(attachmentId);
            result = attachmentEntity;
            times = 1;

        }};

        //method to test
        outDownloadAttachmentHandler.validateDownloadAttachment(attachmentId);
        new FullVerifications() {{
        }};
    }

    @Test
    public void testValidateDownloadAttachment_ExceptionNotFound() {
        final Long attachmentId = 10L;

        new Expectations() {{
            attachmentService.findById(attachmentId);
            result = null;
            times = 1;

        }};

        try {
            //method to test
            outDownloadAttachmentHandler.validateDownloadAttachment(attachmentId);
            Assert.fail();
        } catch (ETrustExPluginException e) {
            Assert.assertEquals("error code should match", ETrustExError.DEFAULT, e.getError());
            Assert.assertTrue("exception message should match", e.getMessage().contains("Attachment [ID: " + attachmentId + "] not found"));

        }

        new FullVerifications() {{
        }};
    }


    @Test
    public void testValidateDownloadAttachment_ExceptionInvalidState() {
        final Long attachmentId = 10L;

        attachmentEntity.setStateType(AttachmentState.ATTS_UPLOADED);

        attachmentEntity.getMessage().setMessageState(MessageState.MS_UPLOADED);
        new Expectations() {{
            attachmentService.findById(attachmentId);
            result = attachmentEntity;
            times = 1;
        }};

        try {
            //method to test
            outDownloadAttachmentHandler.validateDownloadAttachment(attachmentId);
            Assert.fail();

        } catch (ETrustExPluginException e) {

            Assert.assertEquals("error code should match", ETrustExError.ETX_009, e.getError());
            Assert.assertTrue("exception message should match", e.getMessage().contains(
                    "Invalid message bundle and attachment states (expected message bundle state: MS_CREATED, expected attachment state: ATTS_CREATED), " +
                            "skipping download (from backend system) the attachment with ID: " + attachmentId));

        }
        new FullVerifications() {{
        }};

    }

    @Test
    public void testOnError_HappyFlow() {

        attachmentEntity.getMessage().setId(20L);

        new Expectations() {{
            attachmentService.findById(attachmentId);
            result = attachmentEntity;
            times = 1;

        }};

        //method to test
        outDownloadAttachmentHandler.onError(attachmentId);

        new FullVerifications() {{

            Long messageIdActual;
            postProcessProducer.triggerPostProcess(messageIdActual = withCapture(), attachmentId);
            times = 1;
            Assert.assertEquals("message id should match", messageId, messageIdActual);

        }};
        Assert.assertEquals("attachment state should match", AttachmentState.ATTS_FAILED, attachmentEntity.getStateType());

        Assert.assertNotNull("error shouldn't be null", attachmentEntity.getError());
        Assert.assertEquals("error code should match", ETrustExError.DEFAULT.getCode(), attachmentEntity.getError().getResponseCode());
        Assert.assertEquals("error detail should match", "Failed to download attachment " + attachmentEntity.getAttachmentUuid(), attachmentEntity.getError().getDetail());

    }
}
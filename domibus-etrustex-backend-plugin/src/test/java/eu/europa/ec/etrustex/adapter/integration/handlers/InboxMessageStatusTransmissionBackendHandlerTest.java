package eu.europa.ec.etrustex.adapter.integration.handlers;

import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseRequest;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.converters.NodeApplicationResponseTypeConverter;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.service.security.IdentityManager;
import mockit.*;
import mockit.integration.junit4.JMockit;
import oasis.names.specification.ubl.schema.xsd.applicationresponse_2.ApplicationResponseType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * JUnit test for {@link InboxMessageStatusTransmissionBackendHandler}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 08/11/2017
 */
@SuppressWarnings({"ResultOfMethodCallIgnored", "Duplicates"})
@RunWith(JMockit.class)
public class InboxMessageStatusTransmissionBackendHandlerTest {

    @Injectable
    private IdentityManager identityManager;

    @Injectable
    private MessageServicePlugin messageService;

    @Injectable
    private PostProcessProducer postProcessProducer;

    @Tested
    private InboxMessageStatusTransmissionBackendHandler inboxMessageStatusTransmissionBackendHandler;

    @Test
    public void testHandle_HappyFlow(@Mocked final ETrustExAdapterDTO adapterDTO) {
        //input data
        final String domibusMsgId = "TEST_Domibus_Msg_Id";
        final String senderPartyUuid = "senderPartyUuidTest1";
        final EtxParty senderParty = new EtxParty();
        senderParty.setPartyUuid(senderPartyUuid);
        final String receiverPartyUuid = "receiverPartyUuidTest2";
        final EtxParty receiverParty = new EtxParty();
        receiverParty.setPartyUuid(receiverPartyUuid);

        final EtxMessage etxMessage = new EtxMessage();
        final Long messageId = 220L;
        etxMessage.setId(messageId);
        final SubmitApplicationResponseRequest submitApplicationResponseRequest = new SubmitApplicationResponseRequest();
        final ApplicationResponseType applicationResponseType = new ApplicationResponseType();
        submitApplicationResponseRequest.setApplicationResponse(applicationResponseType);

        //static method mockup
        new MockUp<NodeApplicationResponseTypeConverter>() {
            @Mock
            EtxMessage toEtxMessage(ApplicationResponseType applicationResponse) {
                return etxMessage;
            }
        };

        new MockUp<TransformerUtils>() {
            @Mock
            SubmitApplicationResponseRequest extractETrustExXMLPayload(ContentId requestContentId, ETrustExAdapterDTO eTrustExAdapterDTO) {
                return submitApplicationResponseRequest;
            }
        };

        new Expectations(inboxMessageStatusTransmissionBackendHandler) {{
            adapterDTO.getAs4MessageId();
            times = 1;
            result = domibusMsgId;

            adapterDTO.getAs4OriginalSender();
            times = 1;
            result = senderPartyUuid;

            adapterDTO.getAs4FinalRecipient();
            times = 1;
            result = receiverPartyUuid;

            inboxMessageStatusTransmissionBackendHandler.retrieveParty(senderPartyUuid, domibusMsgId);
            times = 1;
            result = senderParty;

            inboxMessageStatusTransmissionBackendHandler.retrieveParty(receiverPartyUuid, domibusMsgId);
            times = 1;
            result = receiverParty;

            inboxMessageStatusTransmissionBackendHandler.getMessage(adapterDTO, senderParty, receiverParty);
            times = 1;
            result = etxMessage;

        }};

        //tested method
        inboxMessageStatusTransmissionBackendHandler.handle(adapterDTO);

        new Verifications() {{

            EtxMessage etxMessageActual;
            messageService.createMessage(etxMessageActual = withCapture());
            times = 1;
            Assert.assertNotNull("etxMessage shouldn't be null", etxMessageActual);
            Assert.assertEquals("etxMessage should match", etxMessage, etxMessageActual);
            Assert.assertEquals("message id should match", messageId, etxMessageActual.getId());

            Long messageIdActual;
            postProcessProducer.triggerPostProcess(messageIdActual = withCapture());
            times = 1;
            Assert.assertNotNull("message id shouldn't be null", messageIdActual);
            Assert.assertEquals("message id should match", messageId, messageIdActual);

        }};
    }

    @Test
    public void testRetrieveParty_HappyFlow() {

        final String partyUuid = "senderPartyTest1";
        final EtxParty etxParty = new EtxParty();
        etxParty.setPartyUuid(partyUuid);
        final String domibusMsgId = "TEST_Domibus_Msg_Id";

        new Expectations() {{
            identityManager.searchPartyByUuid(partyUuid);
            times = 1;
            result = etxParty;

        }};

        //tested method
        inboxMessageStatusTransmissionBackendHandler.retrieveParty(partyUuid, domibusMsgId);

        new Verifications() {{
            String paryUuidActual;
            identityManager.searchPartyByUuid(paryUuidActual = withCapture());
            times = 1;
            Assert.assertEquals("party uuid must match", partyUuid, paryUuidActual);
        }};
    }

    @Test
    public void testRetrieveParty_ThrowIllegalArgumentException() {

        final String partyUuid = null;
        final EtxParty etxParty = new EtxParty();
        etxParty.setPartyUuid(partyUuid);
        final String domibusMsgId = "TEST_Domibus_Msg_Id";

        try {
            //tested method
            inboxMessageStatusTransmissionBackendHandler.retrieveParty(partyUuid, domibusMsgId);
            Assert.fail("Expected exception");
        } catch (Exception e) {
            Assert.assertEquals("expected type is IllegalArgumentException", IllegalArgumentException.class, e.getClass());
        }

        new Verifications() {{
            identityManager.searchPartyByUuid(anyString);
            times = 0;
        }};
    }

    @Test
    public void testRetrieveParty_ThrowETrustExPluginException() {

        final String partyUuid = "senderPartyTest1";
        final String domibusMsgId = "TEST_Domibus_Msg_Id";

        new Expectations() {{
            identityManager.searchPartyByUuid(partyUuid);
            times = 1;
            result = null;

        }};

        try {
            //tested method
            inboxMessageStatusTransmissionBackendHandler.retrieveParty(partyUuid, domibusMsgId);
            Assert.fail("Expected exception");
        } catch (Exception e) {
            Assert.assertEquals("expected type is IllegalArgumentException", ETrustExPluginException.class, e.getClass());
            ETrustExPluginException etxException = (ETrustExPluginException) e;

            Assert.assertEquals("error message must match", "eTrustEx party: " + partyUuid + " communicated in Domibus message ID: "
                    + domibusMsgId + "] is not know at Backend database.", etxException.getMessage());
            Assert.assertEquals("error code must match", ETrustExError.DEFAULT, etxException.getError());
        }

        new Verifications() {{
            String paryUuidActual;
            identityManager.searchPartyByUuid(paryUuidActual = withCapture());
            times = 1;
            Assert.assertEquals("party uuid must match", partyUuid, paryUuidActual);
        }};
    }

    @Test
    public void testGetMessage_HappyFlow(@Mocked final ETrustExAdapterDTO adapterDTO, @Mocked final EtxMessage etxMessage) {

        //input data
        final SubmitApplicationResponseRequest submitApplicationResponseRequest = new SubmitApplicationResponseRequest();
        final ApplicationResponseType applicationResponseType = new ApplicationResponseType();
        submitApplicationResponseRequest.setApplicationResponse(applicationResponseType);
        final String domibusMsgId = "TEST_Domibus_Msg_Id";
        adapterDTO.setAs4MessageId(domibusMsgId);
        final EtxParty senderParty = new EtxParty();
        final String senderPartyUuid = "senderPartyTest1";
        senderParty.setPartyUuid(senderPartyUuid);
        final EtxParty receiverParty = new EtxParty();
        final String receiverPartyUuid = "receiverPartyTest1";
        receiverParty.setPartyUuid(receiverPartyUuid);

        //static method mockup
        new MockUp<NodeApplicationResponseTypeConverter>() {
            @Mock
            EtxMessage toEtxMessage(ApplicationResponseType applicationResponse) {
                return etxMessage;
            }
        };

        new MockUp<TransformerUtils>() {
            @Mock
            SubmitApplicationResponseRequest extractETrustExXMLPayload(ContentId requestContentId, ETrustExAdapterDTO eTrustExAdapterDTO) {
                return submitApplicationResponseRequest;
            }
        };

        new Expectations(inboxMessageStatusTransmissionBackendHandler) {{
            adapterDTO.getAs4MessageId();
            times = 1;
            result = domibusMsgId;

//            adapterDTO.g
        }};

        //tested method
        inboxMessageStatusTransmissionBackendHandler.getMessage(adapterDTO, senderParty, receiverParty);

        new Verifications() {{

            MessageType messageType;
            etxMessage.setMessageType(messageType = withCapture());
            times = 1;
            Assert.assertEquals("message type should match", MessageType.MESSAGE_STATUS, messageType);

            MessageState messageState;
            etxMessage.setMessageState(messageState = withCapture());
            times = 1;
            Assert.assertEquals("message state should match", MessageState.MS_CREATED, messageState);


            MessageDirection messageDirection;
            etxMessage.setDirectionType(messageDirection = withCapture());
            times = 1;
            Assert.assertEquals("message direction should match", MessageDirection.IN, messageDirection);

            EtxParty senderPartyActual;
            etxMessage.setSender(senderPartyActual = withCapture());
            Assert.assertNotNull("senderParty must be not null", senderParty);
            Assert.assertEquals("sender must match", senderParty, senderPartyActual);

            EtxParty receiverPartyActual;
            etxMessage.setReceiver(receiverPartyActual = withCapture());
            Assert.assertNotNull("receiverParty must be not null", receiverParty);
            Assert.assertEquals("receiver must match", receiverParty, receiverPartyActual);

            String domibusMsgIdActual;
            etxMessage.setDomibusMessageId(domibusMsgIdActual = withCapture());
            Assert.assertNotNull("domibus msg id shouldn't be null", domibusMsgIdActual);
            Assert.assertEquals("domibus msg id should match", domibusMsgId, domibusMsgIdActual);

        }};

    }

    @Test
    public void testGetMessage_ThrownException(@Mocked final ETrustExAdapterDTO adapterDTO, @Mocked final SubmitApplicationResponseRequest submitApplicationResponseRequest) {

        //input data
        final EtxParty senderParty = new EtxParty();
        senderParty.setPartyUuid("senderPartyTest1");
        final EtxParty receiverParty = new EtxParty();
        receiverParty.setPartyUuid("receiverPartyTest1");
        final String domibusMsgId = "TEST_Domibus_Msg_Id";

        //static methods mockup
        new MockUp<NodeApplicationResponseTypeConverter>() {
            @Mock
            EtxMessage toEtxMessage(ApplicationResponseType applicationResponse) {
                return null;
            }
        };

        new MockUp<TransformerUtils>() {
            @Mock
            SubmitApplicationResponseRequest extractETrustExXMLPayload(ContentId requestContentId, ETrustExAdapterDTO eTrustExAdapterDTO) {
                return submitApplicationResponseRequest;
            }
        };

        new Expectations() {{
            adapterDTO.getAs4MessageId();
            times = 1;
            result = domibusMsgId;
        }};

        try {
            //tested method
            inboxMessageStatusTransmissionBackendHandler.getMessage(adapterDTO, senderParty, receiverParty);
            Assert.fail("Expected exception");
        } catch (Exception e) {
            Assert.assertEquals("expected type is ETrustExPluginException", ETrustExPluginException.class, e.getClass());
            ETrustExPluginException etxException = (ETrustExPluginException) e;

            Assert.assertEquals("error message must match", "Not able to extract a Message Status from Domibus request having messageId: " + domibusMsgId, etxException.getMessage());
            Assert.assertEquals("error code must match", ETrustExError.ETX_001, etxException.getError());
        }
    }

}
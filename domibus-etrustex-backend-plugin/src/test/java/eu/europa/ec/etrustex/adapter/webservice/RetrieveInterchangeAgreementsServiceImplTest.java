package eu.europa.ec.etrustex.adapter.webservice;

import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.ica.EtxIca;
import eu.europa.ec.etrustex.adapter.service.IcaService;
import eu.europa.ec.etrustex.adapter.service.security.IdentityManager;
import eu.europa.ec.etrustex.integration.model.common.v2.RecipientType;
import eu.europa.ec.etrustex.integration.service.retrieveinterchangeagreements.v2.FaultResponse;
import eu.europa.ec.etrustex.integration.service.retrieveinterchangeagreements.v2.RetrieveInterchangeAgreementsRequestType;
import eu.europa.ec.etrustex.integration.service.retrieveinterchangeagreements.v2.RetrieveInterchangeAgreementsResponseType;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.*;

@RunWith(JMockit.class)
public class RetrieveInterchangeAgreementsServiceImplTest {

    public static final String XML = "<?xml version='1.0' encoding='UTF-8'?><eu.europa.ec.etrustex.integration.model.common.v2.InterchangeAgreementType><confidentialityLevelCode>0</confidentialityLevelCode><integrityLevelCode>0</integrityLevelCode><availabilityLevelCode>0</availabilityLevelCode><receiverPartyCertificate/></eu.europa.ec.etrustex.integration.model.common.v2.InterchangeAgreementType>";
    @Injectable
    private IdentityManager identityManager;

    @Injectable
    private IcaService icaService;

    @Tested
    private RetrieveInterchangeAgreementsServiceImpl retrieveInterchangeAgreementsService;

    @Test
    public void retrieveInterchangeAgreements_noUser() {
        final String id1 = "ID1";
        final String id2 = "ID2";
        RetrieveInterchangeAgreementsRequestType retrieveIcaRequestType =
                new RetrieveInterchangeAgreementsRequestType();

        retrieveIcaRequestType.setSender(getRecipient(id1));
        retrieveIcaRequestType.setReceiver(getRecipient(id2));
        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = new Exception();
        }};

        try {
            retrieveInterchangeAgreementsService.retrieveInterchangeAgreements(retrieveIcaRequestType);
            Assert.fail();
        } catch (FaultResponse faultResponse) {
            Assert.assertThat(faultResponse.getFaultInfo().getCode(), is(ETrustExError.DEFAULT.getCode()));
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void retrieveInterchangeAgreements_noAuth() {
        final String id1 = "ID1";
        final String id2 = "ID2";
        final String user = "user";
        RetrieveInterchangeAgreementsRequestType retrieveIcaRequestType =
                new RetrieveInterchangeAgreementsRequestType();

        retrieveIcaRequestType.setSender(getRecipient(id1));
        retrieveIcaRequestType.setReceiver(getRecipient(id2));

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = user;

            identityManager.hasAccess(id1, user);
            times = 1;
            result = false;
        }};

        try {
            retrieveInterchangeAgreementsService.retrieveInterchangeAgreements(retrieveIcaRequestType);
            Assert.fail();
        } catch (FaultResponse faultResponse) {
            faultResponse.printStackTrace();
            Assert.assertThat(faultResponse.getCause(), is(instanceOf(ETrustExPluginException.class)));
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void retrieveInterchangeAgreements_noIca() {
        final String id1 = "ID1";
        final String id2 = "ID2";
        final String user = "user";
        final EtxParty senderParty = new EtxParty();
        final EtxParty receiverParty = new EtxParty();
        RetrieveInterchangeAgreementsRequestType retrieveIcaRequestType =
                new RetrieveInterchangeAgreementsRequestType();

        retrieveIcaRequestType.setSender(getRecipient(id1));
        retrieveIcaRequestType.setReceiver(getRecipient(id2));

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = user;

            identityManager.hasAccess(id1, user);
            times = 1;
            result = true;

            identityManager.searchPartyByUuid(id1);
            times = 1;
            result = senderParty;

            identityManager.searchPartyByUuid(id2);
            times = 1;
            result = receiverParty;

            icaService.getIca(senderParty, receiverParty);
            times = 1;
            result = null;
        }};

        try {
            retrieveInterchangeAgreementsService.retrieveInterchangeAgreements(retrieveIcaRequestType);
            Assert.fail();
        } catch (FaultResponse faultResponse) {
            faultResponse.printStackTrace();
            Assert.assertThat(faultResponse.getCause(), is(instanceOf(ETrustExPluginException.class)));
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void retrieveInterchangeAgreements_xmlEmpty() {
        final String id1 = "ID1";
        final String id2 = "ID2";
        final String user = "user";
        final EtxParty senderParty = new EtxParty();
        final EtxParty receiverParty = new EtxParty();
        RetrieveInterchangeAgreementsRequestType retrieveIcaRequestType =
                new RetrieveInterchangeAgreementsRequestType();

        retrieveIcaRequestType.setSender(getRecipient(id1));
        retrieveIcaRequestType.setReceiver(getRecipient(id2));

        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = user;

            identityManager.hasAccess(id1, user);
            times = 1;
            result = true;

            identityManager.searchPartyByUuid(id1);
            times = 1;
            result = senderParty;

            identityManager.searchPartyByUuid(id2);
            times = 1;
            result = receiverParty;

            icaService.getIca(senderParty, receiverParty);
            times = 1;
            result = new EtxIca();
        }};

        try {
            retrieveInterchangeAgreementsService.retrieveInterchangeAgreements(retrieveIcaRequestType);
            Assert.fail();
        } catch (FaultResponse faultResponse) {
            Assert.assertThat(faultResponse.getCause().getCause().getMessage(), containsString("Empty xml string"));
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void retrieveInterchangeAgreements_happyFlow() {
        final String id1 = "ID1";
        final String id2 = "ID2";
        final String user = "user";
        final EtxParty senderParty = new EtxParty();
        final EtxParty receiverParty = new EtxParty();
        final EtxIca etxIca = new EtxIca();
        etxIca.setXml(XML);
        etxIca.setSender(senderParty);
        etxIca.setReceiver(receiverParty);

        RetrieveInterchangeAgreementsRequestType retrieveIcaRequestType =
                new RetrieveInterchangeAgreementsRequestType();

        retrieveIcaRequestType.setSender(getRecipient(id1));
        retrieveIcaRequestType.setReceiver(getRecipient(id2));


        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            times = 1;
            result = user;

            identityManager.hasAccess(id1, user);
            times = 1;
            result = true;

            identityManager.searchPartyByUuid(id1);
            times = 1;
            result = senderParty;

            identityManager.searchPartyByUuid(id2);
            times = 1;
            result = receiverParty;

            icaService.getIca(senderParty, receiverParty);
            times = 1;
            result = etxIca;
        }};

        RetrieveInterchangeAgreementsResponseType retrieveInterchangeAgreementsResponseType = null;
        try {
            retrieveInterchangeAgreementsResponseType = retrieveInterchangeAgreementsService.retrieveInterchangeAgreements(retrieveIcaRequestType);
        } catch (FaultResponse faultResponse) {
            Assert.fail();
        }
        Assert.assertThat(retrieveInterchangeAgreementsResponseType, notNullValue());
        new FullVerifications() {{
        }};
    }

    private RecipientType getRecipient(String id) {
        RecipientType recipientType = new RecipientType();
        recipientType.setId(id);
        return recipientType;
    }
}
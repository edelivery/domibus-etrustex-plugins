package eu.europa.ec.etrustex.adapter.model.entity.common;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * JUnit class for <code>ErrorType</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
public class ErrorTypeTest {

    @Test
    public void testFromCode_HappyFlow() throws Exception {
        //test randomly some of the values
        Assert.assertEquals(ErrorType.fromCode("BUSINESS_MULTIPLE_OBJECTS_FOUND"),
                ErrorType.BUSINESS_MULTIPLE_OBJECTS_FOUND);

        Assert.assertEquals(ErrorType.fromCode("BUSINESS_OBJECT_INVALID"),
                ErrorType.BUSINESS_OBJECT_INVALID);

        Assert.assertEquals(ErrorType.fromCode("MSG:1"),
                ErrorType.MSG1);
    }

    @Test
    public void testFromCode_UnhappyFlow() throws Exception {
        //test with some wrong values
        String wrongCode = null;
        Assert.assertNull(ErrorType.fromCode(wrongCode));

        wrongCode = StringUtils.EMPTY;
        Assert.assertNull(ErrorType.fromCode(wrongCode));

        wrongCode = "wrong code";
        Assert.assertNull(ErrorType.fromCode(wrongCode));

        wrongCode = "BUSINESS_OBJECT_INVALID2";
        Assert.assertNull(ErrorType.fromCode(wrongCode));
    }

    @Test
    public void testGetDescription_HappyFlow() throws Exception {

        //test on some values
        Assert.assertEquals(ErrorType.fromCode("TECHNICAL").getDescription(), "Technical error type");
        Assert.assertEquals(ErrorType.fromCode("MSG:1").getDescription(), "Message already exist");
        Assert.assertEquals(ErrorType.fromCode("BUSINESS_OBJECT_ALREADY_EXISTS").getDescription(), "Business object already exists");
    }

}
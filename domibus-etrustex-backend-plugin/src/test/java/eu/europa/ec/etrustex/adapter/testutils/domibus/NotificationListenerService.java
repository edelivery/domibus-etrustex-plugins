
package eu.europa.ec.etrustex.adapter.testutils.domibus;

import eu.domibus.plugin.BackendConnector;
import eu.domibus.plugin.MessageLister;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Christian Koch, Stefan Mueller
 */
public class NotificationListenerService implements MessageListener, JmsListenerConfigurer, MessageLister, NotificationListener {

    private Queue backendNotificationQueue;
    private BackendConnector.Mode mode;
    private BackendConnector backendConnector;

    public NotificationListenerService(final Queue queue, final BackendConnector.Mode mode) {
        backendNotificationQueue = queue;
        this.mode = mode;
    }

    public void setBackendConnector(final BackendConnector backendConnector) {
        this.backendConnector = backendConnector;
    }

    @Transactional
    public void onMessage(final Message message) {
    }




    public Collection<String> listPendingMessages() {
        return new ArrayList<>();
    }



    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void removeFromPending(final String messageId) {

    }


    @Override
    public void configureJmsListeners(final JmsListenerEndpointRegistrar registrar) {
    }

    @Override
    public String getBackendName() {
        return backendConnector.getName();
    }

    @Override
    public Queue getBackendNotificationQueue() {
        return backendNotificationQueue;
    }

    @Override
    public BackendConnector.Mode getMode() {
        return this.mode;
    }
}

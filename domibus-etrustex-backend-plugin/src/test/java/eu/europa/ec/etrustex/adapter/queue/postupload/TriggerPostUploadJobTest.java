package eu.europa.ec.etrustex.adapter.queue.postupload;

import eu.europa.ec.etrustex.adapter.application.AbstractBackEndQuartzJobBeanTest;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.persistence.api.message.MessageDao;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.quartz.JobExecutionContext;

import java.util.ArrayList;
import java.util.List;

import static eu.europa.ec.etrustex.adapter.queue.inbox.uploadattachment.InboxUploadAttachmentHandlerImplTest.createAttachment;
import static eu.europa.ec.etrustex.adapter.queue.inbox.uploadattachment.InboxUploadAttachmentHandlerImplTest.createMessage;

/**
 * @author Arun Raj
 * @version 1.0
 * @since 02/07/2019
 */
@RunWith(JMockit.class)
public class TriggerPostUploadJobTest extends AbstractBackEndQuartzJobBeanTest {

    @Injectable
    private MessageDao messageDao;

    @Injectable
    private PostUploadProducer postUploadProducer;


    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;

    @Mocked
    private JobExecutionContext context;

    @Tested
    private TriggerPostUploadJob triggerPostUploadJob;

    @Test
    public void testProcess_BundlePendingForPostUpload() {
        EtxAttachment etxAttachment = createAttachment(AttachmentState.ATTS_UPLOADED);
        EtxMessage etxMessage = createMessage(1L, MessageState.MS_CREATED);
        etxAttachment.setMessage(etxMessage);
        etxMessage.getAttachmentList().add(etxAttachment);
        List<EtxMessage> etxMessageList = new ArrayList<>();
        etxMessageList.add(etxMessage);

        new Expectations() {{
            messageDao.retrieveMessageBundlesForPostUpload();
            times = 1;
            result = etxMessageList;
        }};

        triggerPostUploadJob.process(context);

        new FullVerifications() {{
            clearEtxLogKeysService.clearKeys();
            times = 1;

            postUploadProducer.triggerBundlePostUpload(etxMessage);
            times = 1;
        }};
    }

    @Test
    public void testProcess_NoBundlePendingForPostUpload()  {
        List<EtxMessage> etxMessageList = new ArrayList<>();

        new Expectations() {{
            messageDao.retrieveMessageBundlesForPostUpload();
            times = 1;
            result = etxMessageList;
        }};

        triggerPostUploadJob.process(context);

        new FullVerifications() {{
            clearEtxLogKeysService.clearKeys();
            times = 1;
        }};
    }
}
package eu.europa.ec.etrustex.adapter.webservice;

import eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter.NodeDocumentWrapperServiceAdapterTest;
import eu.europa.ec.etrustex.adapter.integration.converters.BackendMessageBundleTypeConverter;
import eu.europa.ec.etrustex.adapter.integration.converters.BackendMessageStatusTypeConverter;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentDirection;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.queue.out.downloadattachment.OutDownloadAttachmentProducer;
import eu.europa.ec.etrustex.adapter.queue.out.sendmessage.SendMessageProducer;
import eu.europa.ec.etrustex.adapter.service.MessageBundleService;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.adapter.service.security.IdentityManager;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.integration.model.common.v2.*;
import eu.europa.ec.etrustex.integration.service.outbox.v2.FaultResponse;
import eu.europa.ec.etrustex.integration.service.outbox.v2.ObjectFactory;
import eu.europa.ec.etrustex.integration.service.outbox.v2.SendMessageBundleRequestType;
import eu.europa.ec.etrustex.integration.service.outbox.v2.SendMessageStatusRequestType;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * JUnit class for <code>OutboxServiceImpl</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@RunWith(JMockit.class)
public class OutboxServiceImplTest {

    private static final Long messageId = 10L;
    private static final String senderUUID = "test sender UUID";
    private static final String username = "TEST_EGREFFE_APP_2_B4";

    @Injectable
    private MessageServicePlugin messageService;
    @Injectable
    private IdentityManager identityManager;
    @Injectable
    private SendMessageProducer sendMessageProducer;
    @Injectable
    private OutDownloadAttachmentProducer outDownloadAttachmentProducer;
    @Injectable
    private MessageBundleService etxBackendPluginMessageBundleService;
    @Tested
    private OutboxServiceImpl outboxService;

    private EtxMessage etxMessage;
    private SendMessageBundleRequestType sendMessageBundleRequestType;
    private EtxAttachment etxAttachment;
    private EtxMessageBundleType etxMessageBundleType;
    private SendMessageStatusRequestType sendMessageStatusRequestType;
    private EtxMessageStatusType etxMessageStatusType;

    @Before
    public void setUp() {
        etxMessage = new EtxMessage();
        etxMessage.setId(messageId);
        etxMessage.setAttachmentList(getEtxAttachments());

        //mock static calls here
        new MockUp<BackendMessageBundleTypeConverter>() {
            @Mock
            EtxMessage toEtxMessage(EtxMessageBundleType messageBundleType) {
                return etxMessage;
            }
        };

        new MockUp<BackendMessageStatusTypeConverter>() {
            @Mock
            EtxMessage toEtxMessage(EtxMessageStatusType messageBundleType) {
                return etxMessage;
            }
        };

        sendMessageBundleRequestType = new SendMessageBundleRequestType();
        etxMessageBundleType = getEtxMessageBundleType();
        sendMessageBundleRequestType.setMessageBundle(etxMessageBundleType);
        sendMessageStatusRequestType = new ObjectFactory().createSendMessageStatusRequestType();
        etxMessageStatusType = getEtxMessageStatusType();
        sendMessageStatusRequestType.setMessageStatus(etxMessageStatusType);
    }

    private ArrayList<EtxAttachment> getEtxAttachments() {
        ArrayList<EtxAttachment> attachmentList = new ArrayList<>();
        etxAttachment = NodeDocumentWrapperServiceAdapterTest.formValidEtxAttachmentForStoreDocumentWrapper();
        etxAttachment.setMessage(etxMessage);
        attachmentList.add(etxAttachment);
        return attachmentList;
    }

    private EtxMessageBundleType getEtxMessageBundleType() {
        EtxMessageBundleType etxMessageBundleType = new EtxMessageBundleType();
        RecipientType sender = getRecipientType(senderUUID);
        etxMessageBundleType.setSender(sender);

        RecipientListType receiverList = new RecipientListType();
        receiverList.getRecipient().add(getRecipientType("TEST_EDMA_EXPIRED_ENC_PARTY"));
        etxMessageBundleType.setReceiverList(receiverList);

        return etxMessageBundleType;
    }

    private EtxMessageStatusType getEtxMessageStatusType() {
        EtxMessageStatusType etxMessageStatusType = new EtxMessageStatusType();
        RecipientType sender = getRecipientType(senderUUID);
        etxMessageStatusType.setSender(sender);
        etxMessageStatusType.setMessageReference(getEtxMessageReferenceType(EtxMessageCodeType.MESSAGE_BUNDLE));
        etxMessageStatusType.setReceiver(getRecipientType("TEST_EDMA_EXPIRED_ENC_PARTY"));
        return etxMessageStatusType;
    }

    @Test
    public void testSendMessageBundle_HappyFlow() throws Exception {
        final EtxParty etxParty = new EtxParty();
        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            result = username;
            times = 1;

            identityManager.hasAccess(senderUUID, username);
            result = true;
            times = 1;

            identityManager.searchPartyByUuid(etxMessageBundleType.getSender().getId());
            result = etxParty;
            times = 1;

            identityManager.searchPartyByUuid(etxMessageBundleType.getReceiverList().getRecipient().get(0).getId());
            result = etxParty;
            times = 1;

        }};

        //tested method
        outboxService.sendMessageBundle(sendMessageBundleRequestType);

        new FullVerifications() {{
            messageService.createMessage(etxMessage);
            times = 1;

            EtxMessage actualMsgObj;
            etxBackendPluginMessageBundleService.triggerSendMessageBundle(actualMsgObj = withCapture());
            assertNotNull(actualMsgObj);
            assertEquals(10l, actualMsgObj.getId().longValue());
            assertEquals("MESSAGE_BUNDLE", actualMsgObj.getMessageType().toString());
            assertEquals("MS_CREATED", actualMsgObj.getMessageState().toString());
        }};

    }

    @SuppressWarnings("Duplicates")
    @Test
    public void testSendMessageBundle_EtxAppExceptionThrownUserHasNoAccess() {

        new Expectations(outboxService) {{
            identityManager.retrieveAuthenticatedUserName();
            result = username;
            times = 1;

            identityManager.hasAccess(senderUUID, username);
            result = false;
            times = 1;

        }};

        try {
            //tested method
            outboxService.sendMessageBundle(sendMessageBundleRequestType);
            fail();
        } catch (FaultResponse e) {

            assertNotNull("faultInfo should'n be null", e.getFaultInfo());
            assertEquals("error code should be the same", ETrustExError.ETX_001.getCode(), e.getFaultInfo().getCode());
            assertEquals("error description should be the same", ETrustExError.ETX_001.getDescription(), e.getFaultInfo().getDescription());
            assertTrue("", e.getFaultInfo().getDetail().contains("Authenticated user: " + username + " does not have access to party: " + senderUUID));
        }

        new FullVerifications() {{
        }};

    }

    @Test
    public void testSendMessageBundle_GeneralExceptionThrownUserHasNoAccess() {

        final String exceptionMessage = "Generic Exception";
        final Exception exception = new Exception(exceptionMessage);

        new Expectations(outboxService) {{
            identityManager.retrieveAuthenticatedUserName();
            result = username;
            times = 1;

            identityManager.hasAccess(senderUUID, username);
            result = exception;
            times = 1;

        }};

        try {
            //tested method
            outboxService.sendMessageBundle(sendMessageBundleRequestType);
            fail();
        } catch (FaultResponse e) {
            assertNull("faultInfo should be null", e.getFaultInfo());
            assertEquals("message should be the same", exceptionMessage, e.getMessage());
            assertEquals("cause should be Exception class", exception, e.getCause());
        }
        new FullVerifications() {{

        }};

    }

    @Test
    public void testPersistSendMessageBundleRequest_HappyFlow() {

        final EtxParty senderParty = new EtxParty();

        new Expectations() {{
            identityManager.searchPartyByUuid(etxMessageBundleType.getSender().getId());
            result = senderParty;
            times = 1;
            identityManager.searchPartyByUuid(etxMessageBundleType.getReceiverList().getRecipient().get(0).getId());
            result = senderParty;
            times = 1;
        }};

        //tested method
        outboxService.persistSendMessageBundleRequest(etxMessageBundleType);

        new FullVerifications() {{
            messageService.createMessage(etxMessage);
            times = 1;
        }};

        assertThat(etxMessage.getMessageType(), is(MessageType.MESSAGE_BUNDLE));
        assertThat(etxMessage.getMessageState(), is(MessageState.MS_CREATED));
        assertThat(etxMessage.getDirectionType(), is(MessageDirection.OUT));
        assertThat(etxMessage.getSender(), is(senderParty));

        assertThat(etxAttachment.getStateType(), is(AttachmentState.ATTS_CREATED));
        assertThat(etxAttachment.getDirectionType(), is(AttachmentDirection.OUTGOING));
        assertThat(etxAttachment.isActiveState(), is(true));
    }


    @Test
    public void testSendMessageStatus_HappyFlow() throws Exception {

        final EtxParty etxParty = new EtxParty();
        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            result = username;
            times = 1;

            identityManager.hasAccess(senderUUID, username);
            result = true;
            times = 1;

            identityManager.searchPartyByUuid(etxMessageStatusType.getSender().getId());
            result = etxParty;
            times = 1;
            identityManager.searchPartyByUuid(etxMessageStatusType.getReceiver().getId());
            result = etxParty;
            times = 1;

        }};

        //tested method
        outboxService.sendMessageStatus(sendMessageStatusRequestType);

        new FullVerifications() {{

            Long messageIdActual;
            sendMessageProducer.triggerSendStatus(messageIdActual = withCapture());
            times = 1;
            assertNotNull(messageIdActual);
            assertEquals(messageId, messageIdActual);

            messageService.createMessage(etxMessage);
            times = 1;
        }};
    }

    private RecipientType getRecipientType(String senderUUID) {
        RecipientType recipientType = new RecipientType();
        recipientType.setId(senderUUID);
        return recipientType;
    }

    private EtxMessageReferenceType getEtxMessageReferenceType(EtxMessageCodeType messageBundle) {
        EtxMessageReferenceType messageReferenceType = new EtxMessageReferenceType();
        messageReferenceType.setType(messageBundle);
        return messageReferenceType;
    }

    @Test
    public void testSendMessageStatus_UnsupportedExceptionThrown() {
        etxMessageStatusType.getMessageReference().setType(EtxMessageCodeType.MESSAGE_STATUS);

        try {
            //tested method
            outboxService.sendMessageStatus(sendMessageStatusRequestType);
            fail();
        } catch (Exception e) {
            assertEquals(UnsupportedOperationException.class, e.getCause().getClass());
            assertEquals("Sending status for message with type MESSAGE_STATUS is not supported", e.getMessage());
        }

        new FullVerifications() {{
        }};
    }

    @SuppressWarnings("Duplicates")
    @Test
    public void testSendMessageStatus_EtxAppExceptionThrownUserHasNoAccess() {
        new Expectations() {{
            identityManager.retrieveAuthenticatedUserName();
            result = username;
            times = 1;

            identityManager.hasAccess(senderUUID, username);
            result = false;
            times = 1;
        }};

        try {
            //tested method
            outboxService.sendMessageStatus(sendMessageStatusRequestType);
            fail();
        } catch (FaultResponse e) {

            assertNotNull("faultInfo shouldn't be null", e.getFaultInfo());
            assertEquals(ETrustExError.ETX_001.getCode(), e.getFaultInfo().getCode());
            assertEquals(ETrustExError.ETX_001.getDescription(), e.getFaultInfo().getDescription());
            assertThat(e.getFaultInfo().getDetail(), containsString("Authenticated user: " + username + " does not have access to party: " + senderUUID));
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void testPersistSendMessageStatusRequest_HappyFlow() {
        final EtxParty receiverParty = new EtxParty();
        final EtxParty senderParty = new EtxParty();

        new Expectations() {{
            identityManager.searchPartyByUuid(etxMessageStatusType.getSender().getId());
            result = senderParty;
            times = 1;

            identityManager.searchPartyByUuid(etxMessageStatusType.getReceiver().getId());
            result = receiverParty;
            times = 1;
        }};

        //tested method
        outboxService.persistSendMessageStatusRequest(etxMessageStatusType);

        new FullVerifications() {{
            EtxMessage etxMessageActual;
            messageService.createMessage(etxMessageActual = withCapture());
            times = 1;
            assertThat(etxMessageActual.getMessageType(), is(MessageType.MESSAGE_STATUS));
            assertThat(etxMessageActual.getMessageState(), is(MessageState.MS_CREATED));
            assertThat(etxMessageActual.getDirectionType(), is(MessageDirection.OUT));
        }};


    }


    @Test
    public void testLoadEtxParty_HappyFlow() {
        final String partyUuid = "TEST_EDMA_EXPIRED_ENC_PARTY";
        final EtxParty etxParty = new EtxParty();

        new Expectations() {{
            identityManager.searchPartyByUuid(partyUuid);
            result = etxParty;
            times = 1;

        }};

        //tested method
        outboxService.loadEtxParty(partyUuid);

        new FullVerifications() {{
        }};
    }

    @Test
    public void testLoadEtxParty_EtxAppExceptionThrown() {
        final String partyUuid = "TEST_EDMA_EXPIRED_ENC_PARTY";

        new Expectations() {{
            identityManager.searchPartyByUuid(partyUuid);
            result = null;
            times = 1;
        }};

        try {
            //tested method
            outboxService.loadEtxParty(partyUuid);
            fail();
        } catch (ETrustExPluginException e) {
            assertEquals("error code should match", ETrustExError.ETX_004, e.getError());
            assertTrue("error message should be the same", e.getMessage().contains("Party: " + partyUuid + " could not be located on the DB."));
        }

        new FullVerifications() {{
        }};

    }
}
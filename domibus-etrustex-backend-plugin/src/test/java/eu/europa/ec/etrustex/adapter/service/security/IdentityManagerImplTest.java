package eu.europa.ec.etrustex.adapter.service.security;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.PartyDao;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.SystemDao;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;

/**
 * @author Arun Raj, Catalin Enache, François Gautier
 * @version 1.0
 */
@SuppressWarnings("Duplicates")
@RunWith(JMockit.class)
public class IdentityManagerImplTest {

    @Injectable
    private PartyDao partyDao;

    @Injectable
    private SystemDao systemDao;

    @Tested
    private IdentityManagerImpl identityManager;

    @Test
    public void retrieveAuthenticatedUserName_contextNull(@Mocked final SecurityContextHolder securityContextHolder) {
        new Expectations() {{
            SecurityContextHolder.getContext();
            result = null;
            times = 1;
        }};
        String s = identityManager.retrieveAuthenticatedUserName();
        Assert.assertThat(s, nullValue());

        new FullVerifications() {{
        }};
    }

    @Test
    public void retrieveAuthenticatedUserName_authenticationNull(@Mocked final SecurityContextHolder securityContextHolder,
                                                                 @Mocked final SecurityContext securityContext) {
        new Expectations() {{
            SecurityContextHolder.getContext();
            result = securityContext;
            times = 2;

            securityContext.getAuthentication();
            result = null;
            times = 1;
        }};
        String s = identityManager.retrieveAuthenticatedUserName();
        Assert.assertThat(s, nullValue());

        new FullVerifications() {{
        }};
    }

    @Test
    @WithMockUser
    public void retrieveAuthenticatedUserName_happyFlow(@Mocked final SecurityContextHolder securityContextHolder,
                                                        @Mocked final SecurityContext securityContext,
                                                        @Mocked org.springframework.security.core.Authentication authentication) {
        String principal = "user";
        new Expectations() {{
            SecurityContextHolder.getContext();
            result = securityContext;
            times = 3;

            securityContext.getAuthentication();
            result = authentication;
            times = 2;
            authentication.getName();
            result = principal;
            times = 1;
        }};
        String s = identityManager.retrieveAuthenticatedUserName();
        Assert.assertThat(s, is(principal));

        new FullVerifications() {{
        }};
    }

    @Test
    public void testHasAccess_HappyFlow() {
        final EtxSystem etxSystem = getEtxSystem(16L);
        final EtxParty etxParty = getEtxParty(etxSystem);

        expectationsOk(etxSystem, etxParty);

        Assert.assertTrue(identityManager.hasAccess("TEST_EGREFFE_APP_PARTY-2_B4", "TEST-EGREFFE-2-SYS-USER"));

        fullVerificationOk();
    }

    private EtxParty getEtxParty(EtxSystem etxSystem) {
        final EtxParty etxParty = new EtxParty();
        etxParty.setSystem(etxSystem);
        return etxParty;
    }

    private EtxSystem getEtxSystem(long id) {
        final EtxSystem etxSystem = new EtxSystem();
        etxSystem.setId(id);
        return etxSystem;
    }

    @Test
    public void testHasAccess_differentSystem() {
        final EtxSystem etxSystem1 = getEtxSystem(3L);
        final EtxSystem etxSystem2 = getEtxSystem(16L);
        final EtxParty etxParty = getEtxParty(etxSystem2);
        expectationsOk(etxSystem1, etxParty);

        Assert.assertFalse(identityManager.hasAccess("TEST_EGREFFE_APP_PARTY-2_B4", "TEST-EGREFFE-SYS-USER"));

        fullVerificationOk();
    }

    private void expectationsOk(final EtxSystem etxSystem1, final EtxParty etxParty) {
        new Expectations() {{
            systemDao.findSystemByLocalUserName(anyString);
            times = 1;
            result = etxSystem1;

            partyDao.findByPartyUUID(anyString);
            times = 1;
            result = etxParty;
        }};
    }

    @Test
    public void testHasAccess_UserNameNotFound() {
        final EtxParty etxParty = getEtxParty(getEtxSystem(3L));
        new Expectations() {{
            systemDao.findSystemByLocalUserName(anyString);
            result = null;

            partyDao.findByPartyUUID(anyString);
            result = etxParty;
        }};

        Assert.assertFalse(identityManager.hasAccess("TEST_EGREFFE_APP_PARTY-2_B4", "Dummy-USER"));

        new FullVerifications() {{
            systemDao.findSystemByLocalUserName(anyString);
            times = 1;

            partyDao.findByPartyUUID(anyString);
            times = 1;
        }};
    }

    @Test
    public void testHasAccess_PartyNotFound() {
        final EtxSystem etxSystem1 = getEtxSystem(3L);

        new Expectations() {{
            systemDao.findSystemByLocalUserName(anyString);
            result = etxSystem1;

            partyDao.findByPartyUUID(anyString);
            result = null;
        }};

        Assert.assertFalse(identityManager.hasAccess("Dummy-Party", "TEST-EGREFFE-SYS-USER"));

        fullVerificationOk();
    }

    private void fullVerificationOk() {
        new FullVerifications() {{
            systemDao.findSystemByLocalUserName(anyString);
            times = 1;

            partyDao.findByPartyUUID(anyString);
            times = 1;
        }};
    }
}

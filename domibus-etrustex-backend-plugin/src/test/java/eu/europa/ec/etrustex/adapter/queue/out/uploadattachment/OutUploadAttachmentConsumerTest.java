package eu.europa.ec.etrustex.adapter.queue.out.uploadattachment;

import eu.domibus.ext.services.DomainContextExtService;
import eu.domibus.ext.services.DomainExtService;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPluginService;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.queue.ETrustExBackendPluginJMSErrorUtils;
import eu.europa.ec.etrustex.common.jms.TextMessageConverter;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Qualifier;


/**
 * JUnit class for <code>OutUploadAttachmentConsumer</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@RunWith(JMockit.class)
public class OutUploadAttachmentConsumerTest {

    @Injectable
    OutUploadAttachmentHandler handler;

    @Injectable
    TextMessageConverter textMessageConverter;
    @Injectable
    private DomainBackEndPluginService domainBackEndPluginService;
    @Injectable
    @Qualifier("etxBackendPluginProperties")
    private ETrustExBackendPluginProperties etxBackendPluginProperties;
    @Injectable
    private DomainExtService domainExtService;
    @Injectable
    private DomainContextExtService domainContextExtService;

    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Injectable
    private ETrustExBackendPluginJMSErrorUtils eTrustExBackendPluginJMSErrorUtils;
    @Tested
    OutUploadAttachmentConsumer outUploadAttachmentConsumer;


    @Test
    public void testHandleMessage_HappyFlow(final @Mocked OutUploadAttachmentQueueMessage queueMessage) throws Exception {

        final Long attachmentId = 10L;

        new Expectations() {{
            queueMessage.getAttachmentId();
            result = attachmentId;

        }};

        //method to be tested
        outUploadAttachmentConsumer.handleMessage(queueMessage);

        new Verifications() {{
            Long attachmentIdActual;
            handler.handleUploadAttachment(attachmentIdActual = withCapture());
            times = 1;
            Assert.assertEquals("attachment id must match", attachmentId, attachmentIdActual);
        }};
    }

    @Test
    public void testValidateMessage_HappyFlow(final @Mocked OutUploadAttachmentQueueMessage queueMessage) throws Exception {
        final Long attachmentId = 10L;

        new Expectations() {{
            queueMessage.getAttachmentId();
            result = attachmentId;

        }};

        //method to be tested
        outUploadAttachmentConsumer.validateMessage(queueMessage);

        new Verifications() {{
            Long attachmentIdActual;
            handler.validateUploadAttachment(attachmentIdActual = withCapture());
            times = 1;
            Assert.assertEquals("attachment id must match", attachmentId, attachmentIdActual);
        }};
    }

}
package eu.europa.ec.etrustex.adapter.application.passwordencryption;

import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.domain.PasswordEncryptionResultDTO;
import eu.domibus.ext.services.DomainContextExtService;
import eu.domibus.ext.services.DomibusConfigurationExtService;
import eu.domibus.ext.services.PasswordEncryptionExtService;
import eu.europa.ec.etrustex.adapter.AbstractSpringContextIT;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPluginService;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxUser;
import eu.europa.ec.etrustex.adapter.service.security.BackendUserService;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;

import static java.util.Collections.singletonList;

@RunWith(JMockit.class)
public class ETrustExBackendPluginPasswordEncryptionListenerTest extends AbstractSpringContextIT {

    @Injectable
    private ETrustExBackendPluginProperties etxBackendPluginProperties;
    @Injectable
    private DomainContextExtService domainContextExtService;
    @Injectable
    private PasswordEncryptionExtService pluginPasswordEncryptionService;
    @Injectable
    private DomibusConfigurationExtService domibusConfigurationExtService;
    @Injectable
    private ApplicationContext context;
    @Injectable
    private DomainBackEndPluginService domainBackEndPluginService;

    @Mocked
    private BackendUserService backendUserService;

    @Tested
    private ETrustExBackendPluginPasswordEncryptionListener eTrustExBackendPluginPasswordEncryptionListener;
    private EtxUser etxUser;

    @Before
    public void setUp() throws Exception {
        etxUser = new EtxUser();
        etxUser.setName("NAME");
        etxUser.setId(1L);
        etxUser.setPassword("PWD");
    }

    @Test
    public void noEncryptionByProperties() {

        new Expectations() {{
            domainContextExtService.getCurrentDomainSafely();
            times = 1;
            result = new DomainDTO();

            etxBackendPluginProperties.isPasswordEncryptionActiveDomibus((DomainDTO) any);
            times = 1;
            result = false;
        }};

        eTrustExBackendPluginPasswordEncryptionListener.encryptPasswords();

        new FullVerifications() {{
            domainBackEndPluginService.setBackendPluginDomain();
            times = 1;
        }};
    }

    @Test
    public void noEncryptionByProperties_Failure() {

        new Expectations() {{
            domainContextExtService.getCurrentDomainSafely();
            times = 1;
            result = new DomainDTO();

            etxBackendPluginProperties.isPasswordEncryptionActiveDomibus((DomainDTO) any);
            times = 1;
            result = false;
        }};

        eTrustExBackendPluginPasswordEncryptionListener.encryptPasswords();

        new FullVerifications() {{
            domainBackEndPluginService.setBackendPluginDomain();
            times = 1;
        }};
    }

    @Test
    public void encryptionByProperties_noUserToUpdate() {

        new Expectations() {{
            etxBackendPluginProperties.isPasswordEncryptionActiveDomibus((DomainDTO) any);
            times = 1;
            result = true;

            domibusConfigurationExtService.getConfigLocation();
            times = 1;
            result = "configFile";

            domainContextExtService.getCurrentDomainSafely();
            times = 2;
            result = new DomainDTO();

            context.getBean(BackendUserService.class);
            times = 1;
            result = backendUserService;

            backendUserService.getAllBackEndUsers();
            times = 1;
            result = new ArrayList<>();
        }};

        eTrustExBackendPluginPasswordEncryptionListener.encryptPasswords();

        new FullVerifications() {{
            domainBackEndPluginService.setBackendPluginDomain();
            times = 1;
            pluginPasswordEncryptionService.encryptPasswordsInFile(
                    withAny(new ETrustExBackendPluginPasswordEncryptionContext(
                            null,
                            null,
                            null,
                            null)));
            times = 1;
        }};
    }

    @Test
    public void encryptionByProperties_pwdAlreadyEncrypted() {

        new Expectations() {{
            etxBackendPluginProperties.isPasswordEncryptionActiveDomibus((DomainDTO) any);
            times = 1;
            result = true;

            domibusConfigurationExtService.getConfigLocation();
            times = 1;
            result = "configFile";

            domainContextExtService.getCurrentDomainSafely();
            times = 2;
            result = new DomainDTO();

            context.getBean(BackendUserService.class);
            times = 1;
            result = backendUserService;

            backendUserService.getAllBackEndUsers();
            times = 1;
            result = singletonList(etxUser);

            pluginPasswordEncryptionService.isValueEncrypted(anyString);
            times = 1;
            result = true;
        }};

        eTrustExBackendPluginPasswordEncryptionListener.encryptPasswords();

        new FullVerifications() {{
            domainBackEndPluginService.setBackendPluginDomain();
            times = 1;
            pluginPasswordEncryptionService.encryptPasswordsInFile(
                    withAny(new ETrustExBackendPluginPasswordEncryptionContext(
                            null,
                            null,
                            null,
                            null)));
            times = 1;
        }};
    }

    @Test
    public void encryptionByProperties_pwdToBeEncrypted() {

        new Expectations() {{
            etxBackendPluginProperties.isPasswordEncryptionActiveDomibus((DomainDTO) any);
            times = 1;
            result = true;

            domibusConfigurationExtService.getConfigLocation();
            times = 1;
            result = "configFile";

            domainContextExtService.getCurrentDomainSafely();
            times = 2;
            result = new DomainDTO();

            context.getBean(BackendUserService.class);
            times = 1;
            result = backendUserService;

            backendUserService.getAllBackEndUsers();
            times = 1;
            result = singletonList(etxUser);

            pluginPasswordEncryptionService.isValueEncrypted(anyString);
            times = 1;
            result = false;

            pluginPasswordEncryptionService.encryptProperty((DomainDTO) any, anyString, anyString);
            times = 1;
            result = getResult("ENC(" + etxUser.getPassword() + ")");
        }};

        eTrustExBackendPluginPasswordEncryptionListener.encryptPasswords();

        new FullVerifications() {{
            domainBackEndPluginService.setBackendPluginDomain();
            times = 1;
            pluginPasswordEncryptionService.encryptPasswordsInFile(
                    withAny(new ETrustExBackendPluginPasswordEncryptionContext(
                            null,
                            null,
                            null,
                            null)));
            times = 1;

            EtxUser updatedUser;
            backendUserService.updateUser(updatedUser = withCapture());
            times = 1;
            Assert.assertThat("Password is not udpated", updatedUser.getPassword(), Matchers.containsString("ENC("));
        }};
    }

    private PasswordEncryptionResultDTO getResult(String s) {
        PasswordEncryptionResultDTO passwordEncryptionResultDTO = new PasswordEncryptionResultDTO();
        passwordEncryptionResultDTO.setFormattedBase64EncryptedValue(s);
        return passwordEncryptionResultDTO;
    }
}
package eu.europa.ec.etrustex.adapter.service;

import eu.europa.ec.etrustex.adapter.AbstractSpringContextIT;
import eu.europa.ec.etrustex.adapter.model.entity.AuditEntity;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentDirection;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.ChecksumAlgorithmType;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.PartyDao;
import eu.europa.ec.etrustex.adapter.persistence.api.attachment.AttachmentDao;
import eu.europa.ec.etrustex.adapter.persistence.api.common.ErrorDao;
import eu.europa.ec.etrustex.adapter.persistence.api.message.MessageDao;
import eu.europa.ec.etrustex.common.model.entity.attachment.AttachmentType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

/**
 * @author François Gautier
 */
public class AttachmentServiceBackEndImplIT extends AbstractSpringContextIT {

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private MessageDao messageDao;

    @Autowired
    private PartyDao partyDao;

    @Autowired
    private ErrorDao errorDao;

    @Autowired
    private AttachmentDao attachmentDao;

    private EtxAttachment etxAttachment;

    @Before
    public void setUp() {
        EtxParty sender = partyDao.findById(1001L);
        EtxParty receiver = partyDao.findById(1002L);
        EtxMessage etxMessage = formEtxMessageInboxBundle1Wrapper(sender, receiver, MessageState.MS_CREATED);
        messageDao.save(etxMessage);

        etxAttachment = getEtxAttachment(etxMessage, AttachmentState.ATTS_CREATED);
        etxMessage.addAttachment(etxAttachment);
        attachmentDao.save(etxAttachment);
    }

    @Test
    public void updateStuckAttachments_oneUpdated() {
        attachmentService.updateStuckAttachments(LocalDateTime.now().minusDays(1), LocalDateTime.now().plusDays(1));

        EtxAttachment byId = attachmentDao.findById(etxAttachment.getId());
        Assert.assertEquals(AttachmentState.ATTS_FAILED, byId.getStateType());
        Assert.assertNotNull(byId.getError());
    }

    @Test
    public void updateStuckAttachments_noneUpdated() {
        attachmentService.updateStuckAttachments(LocalDateTime.now().plusDays(100), LocalDateTime.now().plusDays(101));

        EtxAttachment byId = attachmentDao.findById(etxAttachment.getId());
        Assert.assertEquals(AttachmentState.ATTS_CREATED, byId.getStateType());
        Assert.assertNull(byId.getError());
    }

    public static EtxMessage formEtxMessageInboxBundle1Wrapper(EtxParty senderParty, EtxParty receiverParty, MessageState msCreated) {
        EtxMessage etxMessage = new EtxMessage();
        etxMessage.setMessageType(MessageType.MESSAGE_BUNDLE);
        etxMessage.setMessageUuid("NodeToBackend_MessageId_1");
        etxMessage.setMessageState(msCreated);
        etxMessage.setDirectionType(MessageDirection.OUT);
        etxMessage.setMessageReferenceUuid("Msg Ref Id");
        etxMessage.setIssueDateTime(AuditEntity.getGMTCalendar());

        etxMessage.setSender(senderParty);
        etxMessage.setReceiver(receiverParty);

        etxMessage.setDomibusMessageId("DOMIBUS_ID");
        return etxMessage;
    }

    private static EtxAttachment getEtxAttachment(EtxMessage etxMessage, AttachmentState attsCreated) {
        EtxAttachment etxAttachment = new EtxAttachment();
        etxAttachment.setMessage(etxMessage);
        etxAttachment.setAttachmentUuid("NodeToBackend_AttachmentId_1");
        etxAttachment.setAttachmentType(AttachmentType.BINARY);
        etxAttachment.setStateType(attsCreated);
        etxAttachment.setDirectionType(AttachmentDirection.OUTGOING);
        etxAttachment.setFileName("eupl1.1.-licence-en_0.pdf.payload");
        etxAttachment.setFilePath("./src/test/resources");
        etxAttachment.setFileSize(34271L);
        etxAttachment.setMimeType("application/pdf");
        etxAttachment.setChecksum("26316A5CAE50C2234CA16717C1F1CBE7B0330F7AA663BB94BEB622F1D4751634B4CA4A8CF4298974214C40353C5E6FAEE954561830871B5EFAC02776E9F925CB".getBytes());
        etxAttachment.setChecksumAlgorithmType(ChecksumAlgorithmType.SHA_512);
        return etxAttachment;
    }
}
package eu.europa.ec.etrustex.adapter.application;

import eu.europa.ec.etrustex.adapter.domain.DomainInitializerBackEnd;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import mockit.Injectable;

public class AbstractBackEndQuartzJobBeanTest {

    @Injectable
    protected ETrustExBackendPluginProperties etxBackendPluginProperties;

    @Injectable
    private DomainInitializerBackEnd domainInitializerBackEnd;
}

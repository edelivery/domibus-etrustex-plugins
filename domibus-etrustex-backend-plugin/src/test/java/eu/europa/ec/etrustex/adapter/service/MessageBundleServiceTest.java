package eu.europa.ec.etrustex.adapter.service;

import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.persistence.api.attachment.AttachmentDao;
import eu.europa.ec.etrustex.adapter.persistence.api.message.MessageDao;
import eu.europa.ec.etrustex.adapter.queue.out.downloadattachment.OutDownloadAttachmentProducer;
import eu.europa.ec.etrustex.adapter.queue.out.sendmessage.SendMessageProducer;
import eu.europa.ec.etrustex.adapter.service.dto.RetriggerMessageBundleDTO;
import eu.europa.ec.etrustex.adapter.web.retriggeroutgoingbundle.RetriggerBundleRequest;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.Contains;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(JMockit.class)
public class MessageBundleServiceTest {

    public static final String SENDER_PARTY_UUID = "SenderPartyUUID";
    public static final String RECEIVER_PARTY_UUID = "ReceiverPartyUUID";
    @Injectable
    private MessageDao messageDao;

    @Injectable
    private AttachmentDao attachmentDao;

    @Injectable
    private SendMessageProducer sendMessageProducer;

    @Injectable
    private OutDownloadAttachmentProducer outDownloadAttachmentProducer;

    @Injectable
    ETrustExBackendPluginProperties etxBackendPluginProperties;

    @Tested
    MessageBundleService messageBundleService;

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testTriggerSendMessageBundle_HasAttachments() {
        EtxMessage etxMessage = new EtxMessage();
        etxMessage.setId(10L);

        final EtxAttachment etxAttachment = new EtxAttachment();
        final Long attachmentId = 20L;
        etxAttachment.setId(attachmentId);
        etxMessage.addAttachment(etxAttachment);

        messageBundleService.triggerSendMessageBundle(etxMessage);

        new FullVerifications() {{

            List<Long> attachmentIdsActual;
            outDownloadAttachmentProducer.triggerDownload(attachmentIdsActual = withCapture());
            times = 1;
            assertThat(attachmentIdsActual.size(), is(1));
            assertThat(attachmentIdsActual.get(0), is(attachmentId));
        }};
    }

    @Test
    public void testTriggerSendMessageBundle_NoAttachments() {
        EtxMessage etxMessage = new EtxMessage();
        etxMessage.setId(10L);

        messageBundleService.triggerSendMessageBundle(etxMessage);

        new FullVerifications() {{

            Long messageIdActual;
            sendMessageProducer.triggerSendBundle(messageIdActual = withCapture());
            times = 1;
            assertThat(messageIdActual, is(10L));
        }};
    }

    @Test
    public void testReTriggerOutgoingBundleAlreadyProcessed() {
        final String MESSAGE_UUID = "Processed_UUID1";
        EtxMessage msgBundleProcessed = createOutgoingMessageBundleForRetrigger_Processed();
        new Expectations() {{
            messageDao.findEtxMessage("Processed_UUID1", Collections.singletonList(MessageType.MESSAGE_BUNDLE), MessageDirection.OUT, SENDER_PARTY_UUID, RECEIVER_PARTY_UUID);
            result = msgBundleProcessed;
        }};

        RetriggerBundleRequest testInput = new RetriggerBundleRequest(MESSAGE_UUID, MessageType.MESSAGE_BUNDLE.toString(), SENDER_PARTY_UUID, RECEIVER_PARTY_UUID, "MESSAGE_REF_UUID1", MessageState.MS_CREATED.toString(), MessageDirection.OUT.toString());

        RetriggerMessageBundleDTO retriggerMessageBundleDTOResult = messageBundleService.reTriggerOutgoingBundle(Collections.singletonList(testInput));
        List<Exception> exceptions = retriggerMessageBundleDTOResult.getRetriggerBundleExceptionsList().get(MESSAGE_UUID);
        assertNotNull(exceptions);
        Exception expectedException = exceptions.get(0);
        assertThat(expectedException.getMessage(), new Contains("Retriggering transmission will result in business failure from eTrustEx Node due to duplicate bundle"));
    }

    private EtxMessage createOutgoingMessageBundleForRetrigger_Processed() {
        EtxMessage msgBundleProcessed = new EtxMessage();
        msgBundleProcessed.setId(1L);
        msgBundleProcessed.setMessageUuid("Processed_UUID1");
        msgBundleProcessed.setMessageType(MessageType.MESSAGE_BUNDLE);
        msgBundleProcessed.setMessageState(MessageState.MS_PROCESSED);

        EtxParty sender = new EtxParty();
        sender.setPartyUuid(SENDER_PARTY_UUID);
        msgBundleProcessed.setSender(sender);
        EtxParty receiver = new EtxParty();
        sender.setPartyUuid(RECEIVER_PARTY_UUID);
        msgBundleProcessed.setReceiver(receiver);

        return msgBundleProcessed;
    }

}
package eu.europa.ec.etrustex.adapter.service;

import ec.schema.xsd.commonaggregatecomponents_2.InterchangeAgreementType;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.NodeInvocationManager;
import eu.europa.ec.etrustex.adapter.model.dto.IcaDTO;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystemConfig;
import eu.europa.ec.etrustex.adapter.model.entity.ica.EtxIca;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.PartyDao;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.SystemDao;
import eu.europa.ec.etrustex.adapter.persistence.api.ica.IcaDao;
import eu.europa.ec.etrustex.adapter.testutils.ETrustExBackendPluginPropertiesBuilder;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.hamcrest.core.Is;
import org.hamcrest.core.IsCollectionContaining;
import org.hamcrest.core.IsNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

import static eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginPropertiesTestConstants.SYSTEM_DEFAULT;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertThat;

@RunWith(JMockit.class)
public class IcaServiceImplTest {

    @Injectable
    private NodeInvocationManager nodeInvocationManager;

    @Injectable
    private PartyDao partyDao;

    @Injectable
    private SystemDao systemDao;

    @Injectable
    private EtxPartyService etxPartyService;

    @Injectable
    private IcaDao icaDao;

    @Tested
    private IcaServiceImpl icaService;

    @Injectable
    private ETrustExBackendPluginProperties etxBackendPluginProperties;

    @Before
    public void setUp() {
        etxBackendPluginProperties = ETrustExBackendPluginPropertiesBuilder.getInstance();
    }

    @Test
    public void getIcas_Ok() {
        final EtxIca etxIca = getEtxIca(1L);

        new Expectations() {{
            icaDao.findAll();
            times = 1;
            result = singletonList(etxIca);
        }};
        List<EtxIca> icas = icaService.getIcas();

        assertThat(icas, IsCollectionContaining.hasItem(etxIca));
        new FullVerifications() {{
        }};
    }

    @Test
    public void getIca_Ok() {
        final EtxIca etxIca = getEtxIca(1L);

        new Expectations() {{
            icaDao.findIca("sender", "receiver");
            times = 1;
            result = singletonList(etxIca);
        }};
        EtxIca uuid = icaService.getIca(getParty("sender"), getParty("receiver"));

        assertThat(uuid, Is.is(etxIca));
        new FullVerifications() {{
        }};
    }

    @Test
    public void getIca_moreThanOne() {
        final EtxIca etxIca = getEtxIca(1L);

        new Expectations() {{
            icaDao.findIca("sender", "receiver");
            times = 1;
            result = asList(etxIca, getEtxIca(2L));
        }};
        EtxIca uuid = icaService.getIca(getParty("sender"), getParty("receiver"));

        assertThat(uuid, Is.is(etxIca));
        new FullVerifications() {{
        }};
    }

    @Test
    public void getIca_noIcaFound() {
        new Expectations() {{
            icaDao.findIca("sender", "receiver");
            times = 1;
            result = new ArrayList<>();
        }};
        EtxIca uuid = icaService.getIca(getParty("sender"), getParty("receiver"));

        Assert.isNull(uuid);
        new FullVerifications() {{
        }};
    }

    @Test
    public void getIca_senderNull() {
        EtxIca uuid = icaService.getIca(null, getParty("uuid"));

        Assert.isNull(uuid);
        new FullVerifications() {{
        }};
    }

    @Test
    public void getIca_receiverNull() {
        EtxIca uuid = icaService.getIca(getParty("uuid"), null);

        Assert.isNull(uuid);
        new FullVerifications() {{
        }};

    }

    @Test
    public void createIca_noExistingIca_OK() {
        final String senderUuid = "senderUuid";
        final String receiverUuid = "receiverUuid";
        final EtxIca etxIca = getEtxIca(2L);
        final EtxIca etxIca2 = getEtxIca(3L);

        new Expectations() {{
            icaDao.findIca(senderUuid, receiverUuid);
            times = 1;
            result = asList(etxIca, etxIca2);

            partyDao.findByPartyUUID(senderUuid);
            times = 1;
            result = getParty(senderUuid);

            partyDao.findByPartyUUID(receiverUuid);
            times = 1;
            result = getParty(receiverUuid);
        }};

        icaService.createIca(senderUuid, receiverUuid, new InterchangeAgreementType());

        new FullVerifications() {{
            icaDao.remove(etxIca);
            times = 1;

            icaDao.remove(etxIca2);
            times = 1;

            icaDao.save(withAny(new EtxIca()));
            times = 1;
        }};
    }

    @Test
    public void createIca_noExistingIca_ReceiverFound() {
        final String senderUuid = "senderUuid";
        final String receiverUuid = "receiverUuid";

        new Expectations() {{
            icaDao.findIca(senderUuid, receiverUuid);
            times = 1;
            result = new ArrayList<EtxIca>();

            partyDao.findByPartyUUID(senderUuid);
            times = 1;
            result = getParty(senderUuid);

            partyDao.findByPartyUUID(receiverUuid);
            times = 1;
            result = null;

            systemDao.findSystemByName(SYSTEM_DEFAULT);
            times = 1;
            result = new EtxSystem();

            etxPartyService.create(withAny(new EtxParty()));
            times = 1;
            result = new EtxParty();
        }};

        icaService.createIca(senderUuid, receiverUuid, new InterchangeAgreementType());

        new FullVerifications() {{
            icaDao.save(withAny(new EtxIca()));
            times = 1;
        }};
    }

    @Test
    public void createIca_noExistingIca_SenderFound() {
        final String senderUuid = "senderUuid";
        final String receiverUuid = "receiverUuid";

        new Expectations() {{
            icaDao.findIca(senderUuid, receiverUuid);
            times = 1;
            result = new ArrayList<EtxIca>();

            partyDao.findByPartyUUID(senderUuid);
            times = 1;
            result = null;

            partyDao.findByPartyUUID(receiverUuid);
            times = 1;
            result = getParty(receiverUuid);
        }};

        icaService.createIca(senderUuid, receiverUuid, new InterchangeAgreementType());

        new FullVerifications() {{

        }};
    }

    @Test
    public void reloadIcas_noParty() {
        new Expectations() {{
            partyDao.findAll();
            times = 1;
            result = new ArrayList<EtxIca>();
        }};
        icaService.reloadIcas();
        new FullVerifications() {{
        }};
    }

    @Test
    public void reloadIcas_2Parties_error() {
        final EtxParty party1 = getParty("party1", getSystem(asList(new EtxSystemConfig(), new EtxSystemConfig())));
        final EtxParty party2 = getParty("party2", getSystem(asList(new EtxSystemConfig(), new EtxSystemConfig())));
        final EtxParty notEnoughSystemConfig = getParty("notEnoughSystemConfig", getSystem(asList(new EtxSystemConfig())));
        final EtxParty noSystem = getParty("noSystem");
        new Expectations() {{
            partyDao.findAll();
            times = 1;
            result = asList(party1, party2, notEnoughSystemConfig, noSystem);

            nodeInvocationManager.sendInterchangeAgreementRequest(party1);
            times = 1;
            result = "id1";

            nodeInvocationManager.sendInterchangeAgreementRequest(party2);
            times = 1;
            result = new Exception("ERROR");
        }};
        List<IcaDTO> icaDTOS = icaService.reloadIcas();
        new FullVerifications() {{
        }};
        assertThat(icaDTOS.get(0).getErrorDetails(), IsNull.nullValue());
        assertThat(icaDTOS.get(1).getErrorDetails(), Is.is("Error: ERROR"));
    }

    @Test
    public void reloadIcas_2Parties() {
        final EtxParty party1 = getParty("party1", getSystem(asList(new EtxSystemConfig(), new EtxSystemConfig())));
        final EtxParty party2 = getParty("party2", getSystem(asList(new EtxSystemConfig(), new EtxSystemConfig())));
        final EtxParty notEnoughSystemConfig = getParty("notEnoughSystemConfig", getSystem(asList(new EtxSystemConfig())));
        final EtxParty noSystem = getParty("noSystem");
        new Expectations() {{
            partyDao.findAll();
            times = 1;
            result = asList(party1, party2, notEnoughSystemConfig, noSystem);

            nodeInvocationManager.sendInterchangeAgreementRequest(party1);
            times = 1;
            result = "id1";

            nodeInvocationManager.sendInterchangeAgreementRequest(party2);
            times = 1;
            result = "id2";
        }};
        List<IcaDTO> icaDTOS = icaService.reloadIcas();
        new FullVerifications() {{
        }};
        assertThat(icaDTOS.get(0).getErrorDetails(), IsNull.nullValue());
        assertThat(icaDTOS.get(1).getErrorDetails(), IsNull.nullValue());
        assertThat(icaDTOS.get(0).getParty(), Is.is(party1));
        assertThat(icaDTOS.get(1).getParty(), Is.is(party2));
    }

    private EtxIca getEtxIca(long id) {
        return getEtxIca(id, null, null);
    }

    private EtxIca getEtxIca(long id, EtxParty party, EtxParty party1) {
        EtxIca etxIca = new EtxIca();
        etxIca.setId(id);
        etxIca.setSender(party);
        etxIca.setReceiver(party1);
        return etxIca;
    }

    private EtxParty getParty(String uuid) {
        return getParty(uuid, null);
    }

    private EtxParty getParty(String uuid, EtxSystem system) {
        EtxParty etxParty = new EtxParty();
        etxParty.setPartyUuid(uuid);
        etxParty.setSystem(system);
        return etxParty;
    }

    private EtxSystem getSystem(List<EtxSystemConfig> systemConfigList) {
        EtxSystem etxSystem = new EtxSystem();
        etxSystem.setSystemConfigList(systemConfigList);
        return etxSystem;
    }
}
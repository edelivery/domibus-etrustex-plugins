package eu.europa.ec.etrustex.adapter.queue.postprocess;

import eu.europa.ec.etrustex.adapter.integration.client.backend.repository.FileRepository;
import eu.europa.ec.etrustex.adapter.integration.client.backend.repository.FileRepositoryProvider;
import eu.europa.ec.etrustex.adapter.model.entity.administration.BackendFileRepositoryLocationType;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxError;
import eu.europa.ec.etrustex.adapter.model.entity.message.*;
import eu.europa.ec.etrustex.adapter.queue.notify.NotifyProducer;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import mockit.*;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

import static eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter.NodeDocumentWrapperServiceAdapterTest.formValidEtxAttachmentForStoreDocumentWrapper;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;

/**
 * @author François Gautier
 * @version 1.0
 * @since 08/03/2018
 */
@SuppressWarnings("Duplicates")
public class PostProcessHandlerImplTest {
    public static final long ID_NOTIFICATION = 15L;
    public static final long ATTACHMENT_ID = 2L;
    private static final long ID = 1L;

    @Injectable
    private MessageServicePlugin messageService;

    @Injectable
    private FileRepositoryProvider fileRepositoryProvider;

    @Injectable
    private NotifyProducer notifyProducer;

    @Injectable
    private AttachmentService attachmentService;

    @Mocked
    private FileRepository fileRepository;

    @Tested
    private PostProcessHandlerImpl postProcessHandler;


    @Test
    public void validatePostProcess_AttachmentNotFinal() {
        final EtxMessage message = formValidEtxAttachmentForStoreDocumentWrapper().getMessage();
        message.getAttachmentList().get(0).setStateType(AttachmentState.ATTS_CREATED);
        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = message;

            attachmentService.findById(ATTACHMENT_ID);
            times = 1;
            result = message.getAttachmentList().get(0);
        }};

        try {
            postProcessHandler.validatePostProcess(ID, ATTACHMENT_ID);
            Assert.fail();
        } catch (ETrustExPluginException e) {
            Assert.assertThat(e.getMessage(), containsString(String.format("Attachment [ID: %s] is not in final state", ATTACHMENT_ID)));
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void validatePostProcess_AttachmentFinal() {
        final EtxMessage message = formValidEtxAttachmentForStoreDocumentWrapper().getMessage();
        message.getAttachmentList().get(0).setStateType(AttachmentState.ATTS_ALREADY_UPLOADED);
        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = message;

            attachmentService.findById(ATTACHMENT_ID);
            times = 1;
            result = message.getAttachmentList().get(0);
        }};

        postProcessHandler.validatePostProcess(ID, ATTACHMENT_ID);

        new FullVerifications() {{
        }};
    }

    @Test(expected = NullPointerException.class)
    public void validatePostProcess_AttachmentNotFound() {
        final EtxMessage message = formValidEtxAttachmentForStoreDocumentWrapper().getMessage();

        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = message;

            attachmentService.findById(ATTACHMENT_ID);
            times = 1;
            result = null;
        }};

        postProcessHandler.validatePostProcess(ID, ATTACHMENT_ID);

        new FullVerifications() {{
        }};
    }

    @Test
    public void validatePostProcess_NoAttachment_DoNothing() {
        final EtxMessage message = formValidEtxAttachmentForStoreDocumentWrapper().getMessage();

        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = message;
        }};

        postProcessHandler.validatePostProcess(ID, null);

        new FullVerifications() {{
        }};
    }

    @Test
    public void validatePostProcess_notFound() {
        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = null;
        }};

        try {
            postProcessHandler.validatePostProcess(ID, null);
            Assert.fail();
        } catch (ETrustExPluginException e) {
            Assert.assertThat(e.getMessage(), containsString(String.format("Message [ID: %s] not found", ID)));
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void onError_AlreadyHasError() {
        final EtxMessage message = formValidEtxAttachmentForStoreDocumentWrapper().getMessage();

        message.setError(new EtxError("", ""));

        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = message;
        }};

        postProcessHandler.onError(ID);

        new FullVerifications() {{
        }};

        Assert.assertThat(message.getError().getDetail(), not(containsString("Failed to post process message " + message.getMessageUuid())));
        Assert.assertThat(message.getMessageState(), is(MessageState.MS_FAILED));
    }

    @Test
    public void onError_OK() {
        final EtxMessage message = formValidEtxAttachmentForStoreDocumentWrapper().getMessage();

        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = message;
        }};

        postProcessHandler.onError(ID);

        new FullVerifications() {{
        }};

        Assert.assertThat(message.getError().getDetail(), containsString("Failed to post process message " + message.getMessageUuid()));
        Assert.assertThat(message.getMessageState(), is(MessageState.MS_FAILED));
    }

    @Test(expected = NullPointerException.class)
    public void onError_notFound() {
        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = null;
        }};

        postProcessHandler.onError(ID);

        new FullVerifications() {{
        }};
    }

    @Test(expected = NullPointerException.class)
    public void handlePostProcess_messageNotFound() {
        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = null;
        }};
        postProcessHandler.handlePostProcess(ID);

        new FullVerifications() {{
        }};
    }

    @Test
    public void handlePostProcess_MessageStatusFinal_IN() {
        final EtxMessage message = formValidEtxAttachmentForStoreDocumentWrapper().getMessage();
        message.setDirectionType(MessageDirection.IN);
        message.setMessageState(MessageState.MS_PROCESSED);
        message.setMessageType(MessageType.MESSAGE_STATUS);
        message.setError(new EtxError("", ""));

        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = message;
        }};

        postProcessHandler.handlePostProcess(ID);

        new FullVerifications() {{

        }};
        Assert.assertThat(message.getMessageState(), Is.is(MessageState.MS_PROCESSED));
        Assert.assertThat(message.getNotificationState(), Is.is(NotificationState.NF_PENDING));
    }

    @Test
    public void handlePostProcess_MessageStatusNotFinal_withError_NotificationIN(final @Mocked EtxSystem system) {
        final EtxMessage message = formValidEtxAttachmentForStoreDocumentWrapper().getMessage();
        message.setMessageState(MessageState.MS_CREATED);
        message.setMessageType(MessageType.MESSAGE_STATUS);
        message.setError(new EtxError("", ""));

        message.getSender().setSystem(system);

        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = message;

            messageService.createMessageStatusFor(message, MessageReferenceState.FAILED);
            times = 1;
            result = getNotification(ID_NOTIFICATION, MessageDirection.IN, message.getSender());

            system.requiresAutoNotification();
            times = 1;
            result = false;
        }};

        postProcessHandler.handlePostProcess(ID);

        new FullVerifications() {{

        }};
        Assert.assertThat(message.getMessageState(), Is.is(MessageState.MS_FAILED));
    }

    @Test
    public void handlePostProcess_MessageStatusNotFinal_withError_NotificationOUT() {
        final EtxMessage message = formValidEtxAttachmentForStoreDocumentWrapper().getMessage();
        message.setMessageState(MessageState.MS_CREATED);
        message.setMessageType(MessageType.MESSAGE_STATUS);
        message.setError(new EtxError("", ""));

        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = message;

            messageService.createMessageStatusFor(message, MessageReferenceState.FAILED);
            times = 1;
            result = getNotification(ID_NOTIFICATION, MessageDirection.OUT);
        }};

        postProcessHandler.handlePostProcess(ID);

        new FullVerifications() {{
            notifyProducer.triggerNotify(ID_NOTIFICATION);
            times = 1;
        }};
        Assert.assertThat(message.getMessageState(), Is.is(MessageState.MS_FAILED));
    }

    private EtxMessage getNotification(long id, MessageDirection out) {
        return getNotification(id, out, new EtxParty());
    }

    private EtxMessage getNotification(long id, MessageDirection out, EtxParty receiver) {
        EtxMessage etxMessage = new EtxMessage();
        etxMessage.setId(id);
        etxMessage.setMessageState(MessageState.MS_PROCESSED);
        etxMessage.setDirectionType(out);
        etxMessage.setReceiver(receiver);
        return etxMessage;
    }

    @Test
    public void handlePostProcess_MessageStatusNotFinal_noError() {
        final EtxMessage message = formValidEtxAttachmentForStoreDocumentWrapper().getMessage();
        message.setMessageState(MessageState.MS_CREATED);
        message.setMessageType(MessageType.MESSAGE_STATUS);

        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = message;
        }};
        postProcessHandler.handlePostProcess(ID);

        new FullVerifications() {{
        }};
        Assert.assertThat(message.getMessageState(), Is.is(MessageState.MS_PROCESSED));
    }

    @Test
    public void handlePostProcess_MessageBundleNotFinal_AllAttachmentInFinalState_noFailedAtt() {
        final EtxMessage message = formValidEtxAttachmentForStoreDocumentWrapper().getMessage();
        message.setMessageState(MessageState.MS_CREATED);
        message.setMessageType(MessageType.MESSAGE_BUNDLE);

        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = message;

            attachmentService.areAllAttachmentsInFinalState(message);
            times = 1;
            result = true;

            attachmentService.hasFailedAttachment(message);
            times = 1;
            result = false;

            fileRepositoryProvider.getFileRepository((BackendFileRepositoryLocationType) any);
            times = 1;
            result = fileRepository;

            fileRepository.deleteDirectory(message);
            times = 1;
            result = true;
        }};
        postProcessHandler.handlePostProcess(ID);

        new FullVerifications() {{
        }};
        Assert.assertThat(message.getMessageState(), Is.is(MessageState.MS_PROCESSED));
    }

    @Test
    public void handlePostProcess_MessageBundleNotFinal_AllAttachmentInFinalState_FailedAtt() {
        final EtxMessage message = formValidEtxAttachmentForStoreDocumentWrapper().getMessage();
        message.setMessageState(MessageState.MS_CREATED);
        message.setMessageType(MessageType.MESSAGE_BUNDLE);

        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = message;

            attachmentService.areAllAttachmentsInFinalState(message);
            times = 1;
            result = true;

            attachmentService.hasFailedAttachment(message);
            times = 1;
            result = true;

            messageService.createMessageStatusFor(message, MessageReferenceState.FAILED);
            times = 1;
            result = getNotification(ID_NOTIFICATION, MessageDirection.OUT);
        }};
        postProcessHandler.handlePostProcess(ID);

        new FullVerifications() {{
            notifyProducer.triggerNotify(ID_NOTIFICATION);
            times = 1;
        }};
        Assert.assertThat(message.getMessageState(), Is.is(MessageState.MS_FAILED));
    }
}
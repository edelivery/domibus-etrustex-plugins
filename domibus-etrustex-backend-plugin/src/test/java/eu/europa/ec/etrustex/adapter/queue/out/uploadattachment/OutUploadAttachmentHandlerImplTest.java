package eu.europa.ec.etrustex.adapter.queue.out.uploadattachment;

import eu.europa.ec.etrustex.adapter.integration.client.etxnode.NodeInvocationManager;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter.NodeDocumentWrapperServiceAdapterTest;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.queue.postupload.PostUploadProducer;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.service.WorkspaceService;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;

/**
 * JUnit class for <code>OutUploadAttachmentHandlerImpl</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@SuppressWarnings("ResultOfMethodCallIgnored")
@RunWith(JMockit.class)
public class OutUploadAttachmentHandlerImplTest {

    private final Long attachmentId = 10L;
    private final Long messageId = 20L;
    private final static String attachmentUuid = "attachment UUID";
    private final static String senderSystemName = "sender name";
    public static final Path TEST_EUPL_FILE = Paths.get("src/test/resources/data/eupl1.1.-licence-en_0.pdf");

    @Injectable
    private AttachmentService attachmentService;

    @Injectable
    private NodeInvocationManager nodeInvocationManager;

    @Injectable
    private PostProcessProducer postProcessProducer;

    @Injectable
    private PostUploadProducer postUploadProducer;

    @Injectable
    private WorkspaceService etxBackendPluginWorkspaceService;

    @Tested
    private OutUploadAttachmentHandlerImpl outUploadAttachmentHandler;
    private EtxAttachment attachmentEntity;

    @Before
    public void setUp() {
        attachmentEntity = NodeDocumentWrapperServiceAdapterTest.formValidEtxAttachmentForStoreDocumentWrapper();
        attachmentEntity.setId(attachmentId);
        attachmentEntity.setAttachmentUuid(attachmentUuid);
        attachmentEntity.getMessage().setId(messageId);
        attachmentEntity.getMessage().getSender().getSystem().setName(senderSystemName);
    }

    @Test
    public void testHandleUploadAttachment_HappyFlow() {
        attachmentEntity.setStateType(AttachmentState.ATTS_DOWNLOADED);
        new Expectations() {{

            attachmentService.findById(attachmentId);
            result = attachmentEntity;
            times = 1;

            attachmentService.checkAttachmentForSystemInState(attachmentEntity, attachmentEntity.getMessage().getSender().getSystem(), Collections.singletonList(AttachmentState.ATTS_UPLOADED));
            result = false;
            times = 1;

            etxBackendPluginWorkspaceService.getFile(attachmentId);
            result = TEST_EUPL_FILE;
            times = 1;
        }};

        //method to be tested
        outUploadAttachmentHandler.handleUploadAttachment(attachmentId);

        new Verifications() {{

            EtxAttachment attachmentEntityActual;
            File fileActual;
            nodeInvocationManager.uploadAttachment(
                    attachmentEntityActual = withCapture(), fileActual = withCapture());
            times = 1;
            Assert.assertEquals("attachment should match", attachmentEntity, attachmentEntityActual);
            Assert.assertEquals("attachment file should match", TEST_EUPL_FILE.toFile(), fileActual);

        }};
    }

    @Test
    public void testHandleUploadAttachment_AlreadyUploaded() {
        attachmentEntity.setStateType(AttachmentState.ATTS_DOWNLOADED);
        new Expectations() {{
            attachmentService.findById(attachmentId);
            result = attachmentEntity;
            times = 1;

            //already uploaded
            attachmentService.checkAttachmentForSystemInState(attachmentEntity, attachmentEntity.getMessage().getSender().getSystem(), Collections.singletonList(AttachmentState.ATTS_UPLOADED));
            result = true;
            times = 1;
        }};

        //method to be tested
        outUploadAttachmentHandler.handleUploadAttachment(attachmentId);

        new FullVerifications() {{
            postUploadProducer.checkAttachmentsAndTriggerPostUpload(attachmentEntity);
            times = 1;

            EtxAttachment attachmentEntityActual;
            AttachmentState oldStateActual;
            AttachmentState newStateActual;
            attachmentService.updateAttachmentState(attachmentEntityActual = withCapture(), oldStateActual = withCapture(), newStateActual = withCapture());
            times = 1;
            Assert.assertEquals("attachment should match", attachmentEntity, attachmentEntityActual);
            Assert.assertEquals("old state should match", AttachmentState.ATTS_DOWNLOADED, oldStateActual);
            Assert.assertEquals("new state should match", AttachmentState.ATTS_ALREADY_UPLOADED, newStateActual);
        }};
    }


    @Test
    public void testHandleUploadAttachment_NotDownloaded() {
        //not downloaded
        attachmentEntity.setStateType(AttachmentState.ATTS_CREATED);

        new Expectations() {{
            attachmentService.findById(attachmentId);
            result = attachmentEntity;
            times = 1;
        }};

        //method to be tested
        outUploadAttachmentHandler.handleUploadAttachment(attachmentId);

        new FullVerifications() {{
        }};
    }


    @Test
    public void testValidateUploadAttachment_HappyFlow() {
        attachmentEntity.getMessage().setMessageState(MessageState.MS_CREATED);
        attachmentEntity.setStateType(AttachmentState.ATTS_DOWNLOADED);

        new Expectations() {{
            attachmentService.findById(attachmentId);
            result = attachmentEntity;
            times = 1;
        }};

        //method to be tested
        outUploadAttachmentHandler.validateUploadAttachment(attachmentId);
        new FullVerifications() {{
        }};

    }

    @Test
    public void testValidateUploadAttachment_ExceptionNotFound() {

        new Expectations() {{
            attachmentService.findById(attachmentId);
            result = null;
            times = 1;

        }};

        try {
            //method to be tested
            outUploadAttachmentHandler.validateUploadAttachment(attachmentId);
            Assert.fail();
        } catch (ETrustExPluginException e) {
            Assert.assertEquals("EtxApplicationExceptionType should match",
                    ETrustExError.DEFAULT, e.getError());
            Assert.assertTrue("description should match",
                    e.getMessage().contains("Attachment [ID: " + attachmentId + "] not found"));
        }

        new FullVerifications() {{
        }};
    }

    @SuppressWarnings("Duplicates")
    @Test
    public void testValidateUploadAttachment_ExceptionInvalidStateOfMessage() {

        final EtxAttachment attachmentEntity = new EtxAttachment();
        final EtxMessage messageBundleEntity = new EtxMessage();
        attachmentEntity.setMessage(messageBundleEntity);
        attachmentEntity.setStateType(AttachmentState.ATTS_DOWNLOADED);
        //invalid state of message
        messageBundleEntity.setMessageState(MessageState.MS_UPLOADED);

        new Expectations() {{
            attachmentService.findById(attachmentId);
            result = attachmentEntity;
            times = 1;
        }};

        try {
            //method to be tested
            outUploadAttachmentHandler.validateUploadAttachment(attachmentId);
        } catch (ETrustExPluginException e) {
            Assert.assertEquals("EtxApplicationExceptionType should match",
                    ETrustExError.ETX_009, e.getError());
            Assert.assertTrue("description should match",
                    e.getMessage().contains("Invalid message bundle or attachment states (expected " +
                            "message bundle state: MS_CREATED, expected attachment states: ATTS_DOWNLOADED, ATTS_IGNORED, " +
                            "skipping uploading attachment (to Node) ID: " + attachmentId));
        }
        new FullVerifications() {{
        }};
    }

    @SuppressWarnings("Duplicates")
    @Test
    public void testValidateUploadAttachment_ExceptionInvalidStateOfAttachment() {
        final EtxAttachment attachmentEntity = new EtxAttachment();
        final EtxMessage messageBundleEntity = new EtxMessage();
        attachmentEntity.setMessage(messageBundleEntity);
        attachmentEntity.setStateType(AttachmentState.ATTS_ALREADY_UPLOADED);
        messageBundleEntity.setMessageState(MessageState.MS_CREATED);

        new Expectations() {{
            attachmentService.findById(attachmentId);
            result = attachmentEntity;
            times = 1;
        }};

        try {
            //method to be tested
            outUploadAttachmentHandler.validateUploadAttachment(attachmentId);
            Assert.fail();
        } catch (ETrustExPluginException e) {
            Assert.assertEquals("EtxApplicationExceptionType should match",
                    ETrustExError.ETX_009, e.getError());
            Assert.assertTrue("description should match",
                    e.getMessage().contains("Invalid message bundle or attachment states (expected " +
                            "message bundle state: MS_CREATED, expected attachment states: ATTS_DOWNLOADED, ATTS_IGNORED, " +
                            "skipping uploading attachment (to Node) ID: " + attachmentId));
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void testOnError_HappyFlow() {

        new Expectations() {{
            attachmentService.findById(attachmentId);
            result = attachmentEntity;
            times = 1;
        }};

        //method to be tested
        outUploadAttachmentHandler.onError(attachmentId);

        new FullVerifications() {{
            attachmentService.attachmentInError(attachmentId, ETrustExError.DEFAULT.getCode(), "Failed to upload attachment " + attachmentEntity.getAttachmentUuid());
            times = 1;

            Long messageIdActual, attachmentIdActual;
            postProcessProducer.triggerPostProcess(messageIdActual = withCapture(), attachmentIdActual = withCapture());
            times = 1;

            Assert.assertEquals("message id should match", messageId, messageIdActual);
            Assert.assertEquals("attachment id should match", attachmentId, attachmentIdActual);

        }};

        Assert.assertEquals("attachment state should match", AttachmentState.ATTS_UPLOADED, attachmentEntity.getStateType());
    }

}
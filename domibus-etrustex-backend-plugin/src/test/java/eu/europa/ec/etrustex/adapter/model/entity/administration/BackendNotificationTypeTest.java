package eu.europa.ec.etrustex.adapter.model.entity.administration;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * JUnit class for <code>BackendNotificationType</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
public class BackendNotificationTypeTest {

    @Test
    public void testFromCode_HappyFlow() throws Exception {
        //test all the values
        Assert.assertEquals(BackendNotificationType.fromCode("WITNESS_FILE"),
                BackendNotificationType.WITNESS_FILE);

        Assert.assertEquals(BackendNotificationType.fromCode("WEB_SERVICE"),
                BackendNotificationType.WEB_SERVICE);
    }

    @Test
    public void testFromCode_UnhappyFlow() throws Exception {

        //test with some wrong values
        String wrongCode = null;
        Assert.assertNull(BackendNotificationType.fromCode(wrongCode));

        wrongCode = StringUtils.EMPTY;
        Assert.assertNull(BackendNotificationType.fromCode(wrongCode));

        wrongCode = "wrong code";
        Assert.assertNull(BackendNotificationType.fromCode(wrongCode));

        wrongCode = "web_service";
        Assert.assertNull(BackendNotificationType.fromCode(wrongCode));
    }

}
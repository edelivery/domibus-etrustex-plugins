package eu.europa.ec.etrustex.adapter.testutils.h2;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author François Gautier
 * @version 1.0
 * @since 30/01/2018
 */
@EnableTransactionManagement
public class MySqlDataBaseConfig extends AbstractDatabaseConfig{

    @Bean
    public DataSource dataSource() {
        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setUrl("jdbc:mysql://localhost:3306/backend?nullNamePatternMatchesAll=true&allowMultiQueries=true");
        basicDataSource.setPassword("admin");
        basicDataSource.setUsername("root");
        basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
        basicDataSource.setDefaultAutoCommit(true);
        return basicDataSource;
    }

    Map<Object, String> getProperties() {
        HashMap<Object, String> objectStringHashMap = new HashMap<>();
        objectStringHashMap.put("hibernate.show_sql", "false");
        objectStringHashMap.put("hibernate.id.new_generator_mappings", "false");
        return objectStringHashMap;
    }

}

package eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter;

import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleRequest;
import eu.domibus.messaging.PModeMismatchException;
import eu.domibus.plugin.exception.TransformationException;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.converters.NodeDocumentBundleTypeConverter;
import eu.europa.ec.etrustex.adapter.integration.handlers.ResponseFaultHandler;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.testutils.JsonUtils;
import eu.europa.ec.etrustex.common.exceptions.DomibusSubmissionException;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.common.util.ConversationIdKey;
import mockit.*;
import org.hibernate.exception.GenericJDBCException;
import org.junit.Assert;
import org.junit.Test;

import javax.xml.ws.Holder;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginPropertiesTestConstants.*;
import static eu.europa.ec.etrustex.adapter.testutils.ResourcesUtils.readResource;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.getObjectFromXml;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * @author Arun Raj
 * @version 1.0
 * @since 31/08/2017
 */
public class NodeDocumentBundleServiceAdapterTest extends BaseAdapterUnitTest {

    @Injectable
    private ResponseFaultHandler responseFaultHandler;

    @Capturing(maxInstances = 1)
    @Tested
    private NodeDocumentBundleServiceAdapter nodeDocumentBundleServiceAdapter;

    /**
     * Tests the entire conversion from {@link EtxMessage} to forming the request parameters of {@link ec.services.wsdl.documentbundle_2.DocumentBundleService}:submitDocumentBundle
     * to forming the {@link ETrustExAdapterDTO} and Domibus Submission for the leg - nodeSubmitDocumentBundleRequest
     */
    @Test
    public void testSendMessageBundle_SuccessfulSubmissionToDomibus() throws Exception {
        final EtxMessage sendMessageBundleEtxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageBundle_IN_Valid_OneAttachment.json"),
                EtxMessage.class);
        final String domibusMessageId = "DomibusMessageId1";

        new Expectations(){{
            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            result = domibusMessageId;
            times = 1;
        }};

        //function under test
        nodeDocumentBundleServiceAdapter.sendMessageBundle(sendMessageBundleEtxMessage);

        new FullVerifications(){
            {
                nodeDocumentBundleServiceAdapter.fillAuthorisationHeaderWithStatusScope(anyString, anyString);
                times = 1;

                NodeDocumentBundleTypeConverter.convertEtxMessage(sendMessageBundleEtxMessage);
                times = 1;

                nodeDocumentBundleServiceAdapter.populateETrustExAdapterDTOForSubmitDocumentBundle(
                        withAny(new Holder<HeaderType>()),
                        withAny(new SubmitDocumentBundleRequest()),
                        sendMessageBundleEtxMessage);
                times = 1;

                ETrustExAdapterDTO eTrustExAdapterDTO;
                backendConnectorSubmissionService.submit(eTrustExAdapterDTO = withCapture());
                times = 1;

                assertEquals(sendMessageBundleEtxMessage.getId(), Long.valueOf(eTrustExAdapterDTO.getEtxBackendMessageId()));
                assertEquals(sendMessageBundleEtxMessage.getSender().getPartyUuid(), eTrustExAdapterDTO.getAs4OriginalSender());

                assertEquals(sendMessageBundleEtxMessage.getSender().getPartyUuid(), eTrustExAdapterDTO.getAs4FromPartyId());
                assertEquals(ConversationIdKey.OUTBOX_SEND_MESSAGE_BUNDLE.getConversationIdKey() + "1", eTrustExAdapterDTO.getAs4ConversationId());
                assertEquals(TEST_AS4_FROMPARTYIDTYPE, eTrustExAdapterDTO.getAs4FromPartyIdType());
                assertEquals(TEST_AS4_FROMPARTYROLE, eTrustExAdapterDTO.getAs4FromPartyRole());
                assertEquals(TEST_AS4_ETRUSTEXNODEPARTYID, eTrustExAdapterDTO.getAs4ToPartyId());
                assertEquals(TEST_AS4_TOPARTYIDTYPE, eTrustExAdapterDTO.getAs4ToPartyIdType());
                assertEquals(TEST_AS4_TOPARTYROLE, eTrustExAdapterDTO.getAs4ToPartyRole());
                assertEquals(sendMessageBundleEtxMessage.getReceiver().getPartyUuid(), eTrustExAdapterDTO.getAs4FinalRecipient());
                assertEquals(TEST_AS4_SERVICE_DOCUMENTBUNDLE, eTrustExAdapterDTO.getAs4Service());
                assertEquals(TEST_AS4_SERVICE_DOCUMENTBUNDLE_SERVICETYPE, eTrustExAdapterDTO.getAs4ServiceType());
                assertEquals(TEST_AS4_ACTION_SUBMITDOCUMENTBUNDLE_REQUEST, eTrustExAdapterDTO.getAs4Action());
                Assert.assertNotNull("Payload with content id Header should not be null", eTrustExAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_HEADER));
                Assert.assertNotNull("Payload with content id - generic should not be null", eTrustExAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_GENERIC));

                assertThat(
                        getObjectFromXml(eTrustExAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_HEADER).getPayloadAsString()),
                        sameBeanAs(getObjectFromXml(readResource("data/Header.xml")))
                );

                assertThat(
                        getObjectFromXml(eTrustExAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_GENERIC).getPayloadAsString()),
                        sameBeanAs(getObjectFromXml(readResource("data/SubmitDocumentBundleRequest.xml")))
                );
            }
        };

        assertEquals(domibusMessageId, sendMessageBundleEtxMessage.getDomibusMessageId());

    }

    /**
     * Test handling of synchronous errors (MessageProcessingException) thrown on Submission to Domibus.
     * A mandatory AS4 element e.g: AS4 action is missig
     *
     */
    @Test
    public void testSendMessageBundle_ValidationFailureOnDomibusSubmission_MissingMandatoryElement() throws Exception {

        final EtxMessage sendMessageBundle_EtxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageBundle_IN_Valid_OneAttachment.json"),
                EtxMessage.class);

        final TransformationException mpEx = new TransformationException("Sample Validation Error from Domibus", new IllegalArgumentException("action must not be empty"));
        final DomibusSubmissionException etEx = new DomibusSubmissionException( "Sample Validation Error from Domibus", mpEx);

        new Expectations() {{
            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            times = 1;
            result = etEx;
        }};

        //function under test
        nodeDocumentBundleServiceAdapter.sendMessageBundle(sendMessageBundle_EtxMessage);

        new FullVerifications() {{
            responseFaultHandler.process(anyString, (EtxMessage) any);
            times = 1;
        }};
    }

    /**
     * Test handling of synchronous errors (MessageProcessingException) thrown on Submission to Domibus.
     * pMode is incorrectly modified and a required property (e.g: - eTxBackendMsgIdProperty) is missing.
     *
     */
    @Test
    public void testSendMessageBundle_ValidationFailureOnDomibusSubmission_PModeMismatch() throws Exception {

        final EtxMessage sendMessageBundle_EtxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageBundle_IN_Valid_OneAttachment.json"),
                EtxMessage.class);

        final PModeMismatchException mpEx = new PModeMismatchException("Property profiling for this exchange does not include a property named [eTxBackendMsgId]");
        final DomibusSubmissionException  etEx= new DomibusSubmissionException("Sample Validation Error from Domibus", mpEx);

        new Expectations() {{
            backendConnectorSubmissionService.submit(withAny(new ETrustExAdapterDTO()));
            times = 1;
            result = etEx;
        }};

        //function under test
        nodeDocumentBundleServiceAdapter.sendMessageBundle(sendMessageBundle_EtxMessage);

        new FullVerifications() {{
            responseFaultHandler.process(anyString, (EtxMessage) any);
            times = 1;
        }};

    }

    /**
     * Scenario when other types of exception are encountered in Domibus Submission
     *
     */
    @Test
    public void testSendMessageBundle_Exception() throws Exception {

        final EtxMessage sendMessageBundle_EtxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageBundle_IN_Valid_OneAttachment.json"),
                EtxMessage.class);

        final GenericJDBCException jbbcException = new GenericJDBCException("Could not open connection", new java.sql.SQLException("Unexpected exception while enlisting XAConnection", new java.sql.SQLException("Transaction rolled back: Transaction timed out after 215 seconds")));

        new Expectations() {{
            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            times = 1;
            result = jbbcException;
        }};

        try {
            //function under test
            nodeDocumentBundleServiceAdapter.sendMessageBundle(sendMessageBundle_EtxMessage);
            Assert.fail("Expected eTrustEx exception to be thrown");
        } catch (ETrustExPluginException etxAE) {
            assertEquals(ETrustExError.DEFAULT.getCode(), etxAE.getError().getCode());
        }

        new FullVerifications() {{
            responseFaultHandler.process(anyString, (EtxMessage) any);
            times = 0;
        }};
    }


    /**
     * Testing the scenario when message conversion fails.
     */
    @SuppressWarnings("Duplicates")
    @Test
    public void testSendMessageBundle_FailureOnMessageConversion() {

        final EtxMessage etxMessage = getEtxMessage();

        try {
            //function under test
            nodeDocumentBundleServiceAdapter.sendMessageBundle(etxMessage);
            Assert.fail("Expected eTrustEx exception to be thrown");
        } catch (ETrustExPluginException etxAE) {
            assertEquals(ETrustExError.DEFAULT.getCode(), etxAE.getError().getCode());
        }

        new FullVerifications() {{
            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            times = 0;
            responseFaultHandler.process(anyString, (EtxMessage) any);
            times = 0;
        }};
    }

    @Test
    public void testPopulateETrustExAdapterDTOForSubmitDocumentBundle_NullInputForPayload() throws Exception {
        try {
            nodeDocumentBundleServiceAdapter.populateETrustExAdapterDTOForSubmitDocumentBundle(null, null, JsonUtils.getObjectMapper().readValue(
                    readResource("data/EtxMessageBundle_IN_Valid_OneAttachment.json"),
                    EtxMessage.class));
            Assert.fail("Expected Null pointer exception here");
        } catch (NullPointerException npe) {
            assert true;
        }
    }
}

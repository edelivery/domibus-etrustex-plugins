package eu.europa.ec.etrustex.adapter.integration;

import eu.domibus.common.*;
import eu.domibus.ext.domain.MessageAttemptDTO;
import eu.domibus.ext.domain.UserMessageDTO;
import eu.domibus.ext.services.MessageMonitorExtService;
import eu.domibus.ext.services.UserMessageExtService;
import eu.domibus.messaging.MessageNotFoundException;
import eu.europa.ec.etrustex.adapter.integration.handlers.BackendActionHandlerFactory;
import eu.europa.ec.etrustex.adapter.integration.handlers.ResponseFaultHandler;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.handlers.Action;
import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.handlers.Service;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.common.util.ContentId;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 06-Oct-17
 */
@RunWith(JMockit.class)
public class ETrustExDomibusBackendConnectorTest {
    private static final String MSG_ID = UUID.randomUUID().toString();
    private static final String FINAL_RECIPIENT = "FINALRECIPIENT";
    private static final UserMessageDTO USER_MESSAGE = new UserMessageDTO();
    @Mocked
    private ResponseFaultHandler responseFaultHandler;
    @Injectable
    private String name;
    @Injectable
    private MessageMonitorExtService messageMonitorService;
    @Injectable
    private BackendActionHandlerFactory backendActionHandlerFactory;
    @Injectable
    private UserMessageExtService userMessageService;
    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Tested
    @Mocked
    private ETrustExDomibusBackendConnector eTrustExDomibusBackendConnector;

    private List<MessageAttemptDTO> attemptDTOS;

    @Before
    public void setUp() {
        attemptDTOS = new ArrayList<>();
        attemptDTOS.add(getMessageAttemptDTO("ERROR1"));
        attemptDTOS.add(getMessageAttemptDTO("ERROR2"));
    }

    private MessageAttemptDTO getMessageAttemptDTO(String s) {
        MessageAttemptDTO e = new MessageAttemptDTO();
        e.setError(s);
        return e;
    }

    @Test
    public void sendMessageFailed_happyFlow() {
        new Expectations() {{
            userMessageService.getMessage(MSG_ID);
            times = 1;
            result = USER_MESSAGE;

            backendActionHandlerFactory.getOperationHandler((Service) any, (Action) any);
            times = 1;
            result = responseFaultHandler;

            messageMonitorService.getAttemptsHistory(MSG_ID);
            times = 1;
            result = attemptDTOS;
        }};

        eTrustExDomibusBackendConnector.messageSendFailed(new MessageSendFailedEvent(MSG_ID));

        new FullVerifications() {{
            clearEtxLogKeysService.clearKeys();
            times = 1;
            ETrustExAdapterDTO dto;
            responseFaultHandler.handle(dto = withCapture());
            times = 1;
            assertThat("message id must match", getActualFaultPayload(dto), CoreMatchers.containsString("ERROR1"));
        }};
    }

    @Test(expected = IllegalArgumentException.class)
    public void sendMessageFailed_exception() {
        new Expectations() {{
            userMessageService.getMessage(MSG_ID);
            times = 1;
            result = USER_MESSAGE;

            backendActionHandlerFactory.getOperationHandler((Service) any, (Action) any);
            times = 1;
            result = responseFaultHandler;

            messageMonitorService.getAttemptsHistory(MSG_ID);
            times = 1;
            result = attemptDTOS;

            responseFaultHandler.handle((ETrustExAdapterDTO) any);
            times = 1;
            result = new IllegalArgumentException();
        }};

        eTrustExDomibusBackendConnector.messageSendFailed(new MessageSendFailedEvent(MSG_ID));

        new FullVerifications() {{
        }};
    }

    /**
     * You need to extract this method out of the verification or you would found a 'unmockable method"
     *
     * @param dto caught in the capture
     * @return String of CONTENT FAULT of this DTO
     */
    private String getActualFaultPayload(ETrustExAdapterDTO dto) {
        return new String(dto.getPayloadFromCID(ContentId.CONTENT_FAULT).getPayload());
    }

    @Test
    public void messageSendSuccess_happyFlow() {
        eTrustExDomibusBackendConnector.messageSendSuccess(new MessageSendSuccessEvent(MSG_ID));

        new FullVerifications() {{
        }};
    }

    @Test
    public void messageReceiveFailed_happyFlow() {
        MessageReceiveFailureEvent messageReceiveFailureEvent = new MessageReceiveFailureEvent();
        messageReceiveFailureEvent.setErrorResult(new ErrorResultImpl());
        eTrustExDomibusBackendConnector.messageReceiveFailed(messageReceiveFailureEvent);

        new FullVerifications() {{
        }};
    }

    @Test
    public void deliverMessage_happyFlow() throws Exception {
        final ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        eTrustExAdapterDTO.setAs4Service("SERVICE");
        eTrustExAdapterDTO.setAs4Action("ACTION");
        new Expectations() {{
            eTrustExDomibusBackendConnector.downloadMessage(anyString, (ETrustExAdapterDTO) any);
            times = 1;
            result = new ETrustExAdapterDTO();

            backendActionHandlerFactory.getOperationHandler(Service.fromString(eTrustExAdapterDTO.getAs4Service()), Action.fromString(eTrustExAdapterDTO.getAs4Action()));
            times = 1;
            result = (OperationHandler) dto -> false;
        }};
        eTrustExDomibusBackendConnector.deliverMessage(new DeliverMessageEvent(MSG_ID, FINAL_RECIPIENT));

        new FullVerifications() {{
            clearEtxLogKeysService.clearKeys();
            times = 1;
        }};
    }

    @Test(expected = IllegalArgumentException.class)
    public void deliverMessage_exception() throws Exception {
        final ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        eTrustExAdapterDTO.setAs4Service("SERVICE");
        eTrustExAdapterDTO.setAs4Action("ACTION");
        new Expectations() {{
            eTrustExDomibusBackendConnector.downloadMessage(anyString, (ETrustExAdapterDTO) any);
            times = 1;
            result = new ETrustExAdapterDTO();

            backendActionHandlerFactory.getOperationHandler(Service.fromString(eTrustExAdapterDTO.getAs4Service()), Action.fromString(eTrustExAdapterDTO.getAs4Action()));
            times = 1;
            result = new IllegalArgumentException();
        }};
        eTrustExDomibusBackendConnector.deliverMessage(new DeliverMessageEvent(MSG_ID, FINAL_RECIPIENT));

        new FullVerifications() {{
        }};
    }

    @Test(expected = ETrustExPluginException.class)
    public void deliverMessage_MessageNotFoundException() throws Exception {
        new Expectations() {{
            eTrustExDomibusBackendConnector.downloadMessage(anyString, (ETrustExAdapterDTO) any);
            times = 1;
            result = new MessageNotFoundException();
        }};
        eTrustExDomibusBackendConnector.deliverMessage(new DeliverMessageEvent(MSG_ID, FINAL_RECIPIENT));

        new FullVerifications() {{
        }};

    }
}
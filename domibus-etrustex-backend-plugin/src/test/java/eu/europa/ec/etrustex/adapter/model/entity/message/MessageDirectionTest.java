package eu.europa.ec.etrustex.adapter.model.entity.message;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * JUnit class for <code>MessageDirection</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
public class MessageDirectionTest {
    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testValueOf_HappyFlow() throws Exception {
        //test all the values
        Assert.assertEquals(MessageDirection.IN, MessageDirection.valueOf("IN"));
        Assert.assertEquals(MessageDirection.OUT, MessageDirection.valueOf("OUT"));
    }

    @Test
    public void testValueOf_UnhappyFlow() throws Exception {
        final String wrongValue = "test wrong value";

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("No enum constant " + MessageDirection.class.getName() + "." + wrongValue);

        //do the thing, call with this bad value
        MessageDirection.valueOf(wrongValue);
    }

    @Test
    public void testGetOpposite_HappyFlow() throws Exception {
        Assert.assertEquals(MessageDirection.OUT, MessageDirection.IN.getOpposite());
        Assert.assertEquals(MessageDirection.IN, MessageDirection.OUT.getOpposite());
    }

}
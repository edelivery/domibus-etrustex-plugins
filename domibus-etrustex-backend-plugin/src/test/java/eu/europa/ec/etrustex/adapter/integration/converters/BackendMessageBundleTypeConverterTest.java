package eu.europa.ec.etrustex.adapter.integration.converters;

import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.testutils.JsonUtils;
import eu.europa.ec.etrustex.integration.model.common.v2.EtxMessageBundleType;
import org.junit.Test;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static eu.europa.ec.etrustex.adapter.testutils.ResourcesUtils.readResource;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 12-Oct-17
 */
public class BackendMessageBundleTypeConverterTest {

    @Test
    public void toEtxMessage() throws Exception {

        EtxMessageBundleType etxMessageBundleType = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageBundleType_OneAttachment.json"),
                EtxMessageBundleType.class);

        EtxMessage etxMessage = BackendMessageBundleTypeConverter.toEtxMessage(etxMessageBundleType);
        JsonUtils.getObjectMapper().writeValueAsString(etxMessage);

        assertThat(
                etxMessage,
                sameBeanAs(JsonUtils.getObjectMapper().readValue(
                        readResource("data/EtxMessageBundle_CreatedFrom_EtxMessageBundleType.json"),
                        EtxMessage.class))
        );
    }

    @Test
    public void fromEtxMessage() throws Exception {
        EtxMessage etxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageBundle_IN_Valid_OneAttachment.json"),
                EtxMessage.class);
        EtxMessageBundleType etxMessageBundleType = BackendMessageBundleTypeConverter.fromEtxMessage(etxMessage);

        assertThat(
                etxMessageBundleType,
                sameBeanAs(JsonUtils.getObjectMapper().readValue(
                        readResource("data/EtxMessageBundleType_OneAttachment.json"),
                        EtxMessageBundleType.class))
                .ignoring("issueDateTime.iChronology")
                .ignoring("receivedDateTime.iChronology")
        );
    }
}
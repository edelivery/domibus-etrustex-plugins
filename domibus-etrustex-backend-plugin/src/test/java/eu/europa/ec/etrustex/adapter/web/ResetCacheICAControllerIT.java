package eu.europa.ec.etrustex.adapter.web;

import eu.europa.ec.etrustex.adapter.AbstractSpringContextIT;
import eu.europa.ec.etrustex.adapter.service.IcaService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockServletContext;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ResetCacheICAControllerIT extends AbstractSpringContextIT {
    @Autowired
    private WebApplicationContext webAppContext;
    @Autowired
    protected IcaService icaService;

    private MockMvc mvc;

    @Before
    public void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(webAppContext).build();

        MockServletContext sc = new MockServletContext("");
        ServletContextListener listener = new ContextLoaderListener(webAppContext);
        ServletContextEvent event = new ServletContextEvent(sc);
        listener.contextInitialized(event);
    }

    @Test
    @WithMockUser(roles = "ANONYMOUS")
    public void resetIcaCache_ANONYMOUS() throws Exception {

        mvc.perform(get("/resetIcaCache"))
                .andExpect(status().isFound())
                .andExpect(header().string("location", "login?returnUrl=%2FresetIcaCache"))
                .andExpect(content().string("REDIRECT TO LOGIN"));
    }

    @Test
    @WithMockUser(roles = "ANONYMOUS")
    public void checkIca_ANONYMOUS() throws Exception {

        mvc.perform(get("/checkIca"))
                .andExpect(status().isFound())
                .andExpect(header().string("location", "login?returnUrl=%2FcheckIca"))
                .andExpect(content().string("REDIRECT TO LOGIN"));
    }

    @Test
    @WithMockUser(roles={"ADMIN"})
    public void resetIcaCache_admin() throws Exception {

        mvc.perform(get("/resetIcaCache"))
                .andExpect(status().isOk())
                .andExpect(content().string("Request to reload cached ICAs has been sent for the parties:</br> </br> <a href=\"resetIcaCache\"> <button>Reset ICA</button> </a></br> </br> <a href=\"checkIca\">      <button>Check ICA</button> </a></br></br></br><b>SUCCESS:</b></br>PARTY_UUID</br>"));
    }

    @Test
    @WithMockUser(roles={"ADMIN"})
    public void checkIca_admin() throws Exception {

        mvc.perform(get("/checkIca"))
                .andExpect(status().isOk())
                .andExpect(content().string("Request to get ICAs:</br> </br> <a href=\"resetIcaCache\"> <button>Reset ICA</button> </a></br> </br> <a href=\"checkIca\">      <button>Check ICA</button> </a></br> </br>PARTY_UUID<->PARTY_UUID</br>"));
    }

    @Test
    public void resetIcaCache_notAuth() throws Exception {

        mvc.perform(get("/resetIcaCache"))
                .andExpect(status().isFound())
                .andExpect(header().string("location", "login?returnUrl=%2FresetIcaCache"))
                .andExpect(content().string("REDIRECT TO LOGIN"));
    }

}

package eu.europa.ec.etrustex.adapter.testutils.domibus;

import eu.domibus.plugin.BackendConnector;

import javax.jms.Queue;

/**
 * @author Christian Koch, Stefan Mueller
 */
public interface NotificationListener {
    String getBackendName();

    Queue getBackendNotificationQueue();

    BackendConnector.Mode getMode();
}

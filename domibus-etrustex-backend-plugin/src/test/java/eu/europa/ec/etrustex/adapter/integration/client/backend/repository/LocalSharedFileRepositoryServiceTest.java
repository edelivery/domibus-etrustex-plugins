package eu.europa.ec.etrustex.adapter.integration.client.backend.repository;

import eu.domibus.ext.services.DomainContextExtService;
import eu.domibus.ext.services.DomainExtService;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPluginService;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.model.entity.administration.AdapterConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.model.vo.LocalSharedFileAttachmentVO;
import eu.europa.ec.etrustex.adapter.service.AdapterConfigurationService;
import eu.europa.ec.etrustex.adapter.testutils.JsonUtils;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.common.util.WorkSpaceUtils;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.apache.commons.io.IOUtils;
import org.apache.cxf.attachment.LazyDataSource;
import org.apache.cxf.common.util.ReflectionUtil;
import org.hamcrest.CoreMatchers;
import org.hamcrest.core.IsNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.util.ReflectionTestUtils;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URI;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static eu.europa.ec.etrustex.adapter.testutils.ResourcesUtils.readResource;
import static org.apache.commons.codec.binary.StringUtils.getBytesUtf8;
import static org.hamcrest.core.Is.is;

/**
 * @author François Gautier
 * @version 1.0
 * @since 04/06/2018
 */
@RunWith(JMockit.class)
public class LocalSharedFileRepositoryServiceTest {

    private URI uriEmpty;
    @Mocked
    private Path rootDirectory;
    @Mocked
    private FileSystem fileSystem;
    @Mocked
    private DataHandler dataHandler;
    @Mocked
    private DataSource dataSource;
    @Mocked
    private LazyDataSource lazyDataSource;
    @Injectable
    private AdapterConfigurationService adapterConfigurationService;
    @Injectable
    private DomainExtService domainExtService;
    @Injectable
    private DomainContextExtService domainContextExtService;
    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Injectable
    private DomainBackEndPluginService domainBackEndPluginService;
    @Injectable
    @Qualifier("etxBackendPluginProperties")
    private ETrustExBackendPluginProperties etxBackendPluginProperties;
    @Tested
    private LocalSharedFileRepositoryService localSharedFileRepositoryService;

    @Before
    public void setUp() throws Exception {
        uriEmpty = new URI("");
        //stubbing post-construct method
        new MockUp<LocalSharedFileRepositoryService>() {
            @Mock
            void loadConfiguration() {
            }
        };
    }

    @Test
    public void readFile() throws Exception {
        initRootDirectory();

        EtxAttachment etxAttachment = new EtxAttachment();
        etxAttachment.setAttachmentUuid("UUID");
        etxAttachment.setMessage(getEtxMessage());
        LocalSharedFileAttachmentVO localSharedFileAttachmentVO = localSharedFileRepositoryService.readFile(null, etxAttachment);
        Assert.assertThat(localSharedFileAttachmentVO.getDataHandler().getInputStream(), CoreMatchers.notNullValue());
    }

    private void initRootDirectory() throws IllegalAccessException {
        Field rootDirectory = ReflectionUtil.getDeclaredField(localSharedFileRepositoryService.getClass(), "rootDirectory");
        rootDirectory.setAccessible(true);
        rootDirectory.set(localSharedFileRepositoryService, Paths.get("src", "test", "resources"));
    }

    private EtxMessage getEtxMessage() {
        final EtxMessage etxMessage = new EtxMessage();
        etxMessage.setId(1234L);
        etxMessage.setMessageUuid("msgUuid");
        etxMessage.setSender(getParty("sender", "system"));
        etxMessage.setReceiver(getParty("receiver", "system"));
        etxMessage.setDomibusMessageId("domibusId");
        etxMessage.setMessageType(MessageType.MESSAGE_BUNDLE);
        return etxMessage;
    }

    private EtxParty getParty(String uuid, String systemName) {
        EtxParty etxParty = new EtxParty();
        etxParty.setPartyUuid(uuid);
        EtxSystem system = new EtxSystem();
        system.setName(systemName);
        etxParty.setSystem(system);
        return etxParty;
    }

    @Test
    public void getRootDirectory_null() {
        try {
            Assert.assertThat(localSharedFileRepositoryService.getRootDirectory(), IsNull.nullValue());
        } catch (ETrustExPluginException e) {
            Assert.assertThat(e.getMessage(), CoreMatchers.containsString("Repository root path is not configured"));

        }
    }

    @Test
    public void getRootDirectory_ok() {
        ReflectionTestUtils.setField(localSharedFileRepositoryService, "rootDirectory", rootDirectory);
        Assert.assertThat(localSharedFileRepositoryService.getRootDirectory(), IsNull.notNullValue());
    }

    @Test
    public void getRepositoryRootDirectory_empty() {
        new Expectations() {{
            adapterConfigurationService.getAdapterConfigurations();
            times = 1;
            result = new HashMap<>();
        }};
        try {
            ReflectionTestUtils.invokeMethod(localSharedFileRepositoryService, "getRepositoryRootDirectory");
            Assert.fail();
        } catch (ETrustExPluginException e) {
            Assert.assertThat(e.getMessage(), CoreMatchers.containsString("Repository root path is not configured"));
        }
        new FullVerifications() {{
        }};

    }

    @Test
    public void getRepositoryRootDirectory(@Mocked WorkSpaceUtils workSpaceUtils) {
        final Map<AdapterConfigurationProperty, String> config = new HashMap<>();
        config.put(AdapterConfigurationProperty.ETX_ADAPTER_REPOSITORY_ROOT_PATH, "TEST");
        new Expectations() {{
            adapterConfigurationService.getAdapterConfigurations();
            times = 1;
            result = config;

            WorkSpaceUtils.getPath((Path) any);
            times = 1;
            result = rootDirectory;
        }};
        Path getRepositoryRootDirectory = ReflectionTestUtils.invokeMethod(localSharedFileRepositoryService, "getRepositoryRootDirectory");
        new FullVerifications() {{
        }};

        Assert.assertThat(getRepositoryRootDirectory, IsNull.notNullValue());
    }

    @Test
    public void readFile_npe() throws IOException {
        try {
            localSharedFileRepositoryService.readFile(null, null);
            Assert.fail();
        } catch (NullPointerException e) {
            // That's life buddy
        }
    }

    @Test
    public void readFile_noFiles(@Mocked FileSystems fileSystems, @Mocked Files files) throws IOException {
        final EtxMessage etxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageBundle_IN_Valid_OneAttachment.json"),
                EtxMessage.class);

        final EtxAttachment etxAttachment = etxMessage.getAttachmentList().get(0);
        etxAttachment.setMessage(etxMessage);

        ReflectionTestUtils.setField(localSharedFileRepositoryService, "rootDirectory", rootDirectory);

        new Expectations() {{
            rootDirectory.resolve(anyString);
            times = 1;
            result = rootDirectory;

            Files.exists(rootDirectory);
            times = 1;
            result = false;

            rootDirectory.toAbsolutePath();
            times = 1;
            result = rootDirectory;

            rootDirectory.toString();
            times = 1;
            result = "";

        }};

        LocalSharedFileAttachmentVO localSharedFileAttachmentVO = localSharedFileRepositoryService.readFile(null, etxAttachment);

        new FullVerifications() {{
        }};

        Assert.assertThat(localSharedFileAttachmentVO, IsNull.nullValue());

    }

    @Test
    public void readFile_FilesExist_EmptyFolder(@Mocked FileSystems fileSystems, @Mocked Files files) throws IOException {
        final EtxMessage etxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageBundle_IN_Valid_OneAttachment.json"),
                EtxMessage.class);

        final EtxAttachment etxAttachment = etxMessage.getAttachmentList().get(0);
        etxAttachment.setMessage(etxMessage);

        ReflectionTestUtils.setField(localSharedFileRepositoryService, "rootDirectory", rootDirectory);

        new Expectations() {{
            rootDirectory.resolve(anyString);
            times = 1;
            result = rootDirectory;

            Files.exists(rootDirectory);
            times = 1;
            result = true;

            FileSystems.newFileSystem(rootDirectory, null);
            times = 1;
            result = fileSystem;

            fileSystem.getRootDirectories();
            times = 1;
            result = new ArrayList<>();
        }};

        LocalSharedFileAttachmentVO localSharedFileAttachmentVO = localSharedFileRepositoryService.readFile(null, etxAttachment);

        new FullVerifications() {{
        }};

        Assert.assertThat(localSharedFileAttachmentVO, IsNull.notNullValue());
    }

    @Test
    public void delete_nothing(@Mocked Files files) throws IOException {
        ReflectionTestUtils.setField(localSharedFileRepositoryService, "rootDirectory", rootDirectory);

        final EtxMessage etxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageBundle_IN_Valid_OneAttachment.json"),
                EtxMessage.class);
        new Expectations() {{
            rootDirectory.resolve(anyString);
            times = 1;
            result = rootDirectory;

            Files.walkFileTree((Path) any, (SimpleFileVisitor<Path>) any);
            times = 1;
            result = rootDirectory;

            rootDirectory.toString();
            times = 1;
            result = "";
        }};

        boolean result = localSharedFileRepositoryService.deleteDirectory(etxMessage);

        new FullVerifications() {{
        }};
        Assert.assertThat(result, is(true));

    }

    @Test
    public void delete_nothing_exception(@Mocked Files files) throws IOException {
        ReflectionTestUtils.setField(localSharedFileRepositoryService, "rootDirectory", rootDirectory);

        final EtxMessage etxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageBundle_IN_Valid_OneAttachment.json"),
                EtxMessage.class);

        new Expectations() {{
            rootDirectory.resolve(anyString);
            times = 1;
            result = rootDirectory;

            Files.walkFileTree((Path) any, (SimpleFileVisitor<Path>) any);
            times = 1;
            result = new IOException("test");

            rootDirectory.toAbsolutePath();
            result = rootDirectory;

            rootDirectory.toString();
            times = 2;
            result = "";
        }};

        boolean result = localSharedFileRepositoryService.deleteDirectory(etxMessage);

        new FullVerifications() {{
        }};

        Assert.assertThat(result, is(false));
    }

    @Test
    public void writeFile_npe() throws IOException {
        try {
            localSharedFileRepositoryService.writeFile(null, null, null);
            Assert.fail();
        } catch (NullPointerException e) {
            //That's right
        }
    }

    @Test
    public void writeFile(@Mocked FileSystems fileSystems, @Mocked Files files) throws IOException {
        final EtxMessage etxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageBundle_IN_Valid_OneAttachment.json"),
                EtxMessage.class);

        EtxAttachment etxAttachment = etxMessage.getAttachmentList().get(0);
        etxAttachment.setMessage(etxMessage);

        ReflectionTestUtils.setField(localSharedFileRepositoryService, "rootDirectory", rootDirectory);

        new Expectations() {{
            rootDirectory.resolve(anyString);
            times = 2;
            result = rootDirectory;

            rootDirectory.toUri();
            times = 1;
            result = uriEmpty;

            FileSystems.newFileSystem(URI.create("jar:file:"), Collections.singletonMap("create", "true"));
            times = 1;
            result = fileSystem;

            fileSystem.getPath(anyString);
            times = 1;
            result = rootDirectory;

            Files.notExists(rootDirectory);
            times = 1;
            result = false;

            Files.deleteIfExists(rootDirectory);
            times = 1;
            result = true;

            Files.copy((Path) any, (Path) any);
            times = 1;
            result = rootDirectory;


        }};

        localSharedFileRepositoryService.writeFile(null, etxAttachment, null);

        new FullVerifications() {{
            fileSystem.close();
            times = 1;
        }};
    }

    @Test
    public void releaseResources_npe() throws IOException {
        try {
            localSharedFileRepositoryService.releaseResources(null);
            Assert.fail();
        } catch (NullPointerException e) {
            //yup yup yup
        }
    }

    @Test
    public void releaseResources_doNothing() throws IOException {
        LocalSharedFileAttachmentVO faVo = new LocalSharedFileAttachmentVO(null);
        localSharedFileRepositoryService.releaseResources(faVo);
    }
    @Test
    public void releaseResources_normalDataSource(@Mocked IOUtils ioUtils) throws IOException {
        final LocalSharedFileAttachmentVO faVo = new LocalSharedFileAttachmentVO(null);
        faVo.setDataHandler(dataHandler);
        new Expectations(){{
            dataHandler.getDataSource();
            times = 2;
            result = dataSource;

            dataSource.getContentType();
            times = 1;
            result = "ContentType";
        }};

        localSharedFileRepositoryService.releaseResources(faVo);

        new FullVerifications(){{
            IOUtils.closeQuietly(faVo.getZipFileSystem());
            times = 1;
        }};
    }

    @Test
    public void releaseResources_lazyDataSource(@Mocked IOUtils ioUtils) throws IOException {
        final LocalSharedFileAttachmentVO faVo = new LocalSharedFileAttachmentVO(null);
        faVo.setDataHandler(dataHandler);
        new Expectations(){{
            dataHandler.getDataSource();
            times = 2;
            result = lazyDataSource;

            lazyDataSource.getInputStream();
            times = 1;
            result = new ByteArrayInputStream(getBytesUtf8(""));
        }};

        localSharedFileRepositoryService.releaseResources(faVo);

        new FullVerifications(){{
            IOUtils.closeQuietly(faVo.getZipFileSystem());
            times = 1;
        }};
    }
}
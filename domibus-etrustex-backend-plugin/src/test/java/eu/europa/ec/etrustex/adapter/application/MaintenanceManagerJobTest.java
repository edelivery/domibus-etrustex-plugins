package eu.europa.ec.etrustex.adapter.application;

import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.integration.client.backend.repository.LocalSharedFileRepositoryService;
import eu.europa.ec.etrustex.adapter.model.entity.administration.AdapterConfigurationProperty;
import eu.europa.ec.etrustex.adapter.service.AdapterConfigurationService;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 04-Jan-18
 */
@SuppressWarnings({ "ResultOfMethodCallIgnored"})
@RunWith(JMockit.class)
public class MaintenanceManagerJobTest extends AbstractBackEndQuartzJobBeanTest {
    private static final String SYSTEM_DIR = "system";
    private static final Long DAY_IN_MS = 24L * 60L * 60L * 1000L;

    @Tested
    private final MaintenanceManagerJob maintenanceManager = new MaintenanceManagerJob();

    @Injectable
    private AdapterConfigurationService adapterConfigurationService;
    @Injectable
    private LocalSharedFileRepositoryService localSharedFileRepositoryService;

    @Injectable
    @Qualifier("etxBackendPluginProperties")
    private ETrustExBackendPluginProperties etxBackendPluginProperties;

    private Path tempDir;

    @Before
    public void setUp() throws Exception {
        tempDir = Files.createTempDirectory(this.getClass().toString());
    }

    @After
    public void onTearDown() throws Exception {
        Files.walkFileTree(tempDir, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    @Test
    public void test_deleteOldDirectoriesFromLevel_should_deleteDirIfOld_when_remainingLevelIsZero() throws IOException {
        // given
        Path levelOneDir = Paths.get(tempDir.toString(), "levelOne");
        Files.createDirectories(levelOneDir);
        updatePathAccessTimes(levelOneDir, -2L);

        int remainingLevels = 0;
        long ttlInMs = 1L;

        // when
        maintenanceManager.deleteOldDirectoriesFromLevel(levelOneDir, ttlInMs, remainingLevels);

        // then
        assertThat(Files.notExists(levelOneDir), is(equalTo(true)));
    }

    @Test
    public void test_deleteOldDirectoriesFromLevel_should_notTryToDeleteDir_when_remainingLevelIsGreaterThanZero() throws IOException {
        // given
        Path levelOneDir = Paths.get(tempDir.toString(), "levelOne");
        Files.createDirectories(levelOneDir);
        Path oldFile = Paths.get(tempDir.toString(), "oldFileToDelete.txt");
        Files.createFile(oldFile);

        BasicFileAttributeView attributes = Files.getFileAttributeView(oldFile, BasicFileAttributeView.class);
        FileTime newLastModifiedTime = FileTime.fromMillis(attributes.readAttributes().lastModifiedTime().toMillis() - 2L);
        attributes.setTimes(newLastModifiedTime, newLastModifiedTime, newLastModifiedTime);

        int remainingLevels = 2;
        long ttlInMs = 1L;

        // when
        maintenanceManager.deleteOldDirectoriesFromLevel(tempDir, ttlInMs, remainingLevels);

        // then
        assertThat(Files.exists(oldFile), is(equalTo(true)));
        assertThat(Files.exists(levelOneDir), is(equalTo(true)));
    }


    @Test
    public void test_deleteOldBundlesInDirectory_should_attemptDepthLevelBasedDeleting_when_OUTdirectory() throws IOException {
        // given
        createBundleTestDirStructure(LocalSharedFileRepositoryService.ETX_ADAPTER_OUT_DIRECTORY, MaintenanceManagerJob.OUT_BUNDLE_LEVEL);

        long ttlInMs = DAY_IN_MS;

        // when
        maintenanceManager.deleteOldBundlesInDirectory(tempDir, ttlInMs);

        // then
        Path level1_1 = Paths.get(tempDir.toString(), SYSTEM_DIR, LocalSharedFileRepositoryService.ETX_ADAPTER_OUT_DIRECTORY, "level1_1");
        assertThat(Files.notExists(Paths.get(level1_1.toString(), "level2_1")), is(equalTo(true)));
        assertThat(Files.exists(Paths.get(level1_1.toString(), "level2_2")), is(equalTo(true)));
        assertThat(Files.notExists(Paths.get(level1_1.toString(), "level2_3")), is(equalTo(true)));

        Path level1_2 = Paths.get(tempDir.toString(), SYSTEM_DIR, LocalSharedFileRepositoryService.ETX_ADAPTER_OUT_DIRECTORY, "level1_2");
        assertThat(Files.exists(level1_2), is(equalTo(true)));
    }


    @Test
    public void test_deleteOldBundlesInDirectory_should_attemptDepthLevelBasedDeleting_when_INdirectory() throws IOException {
        // given
        createBundleTestDirStructure(LocalSharedFileRepositoryService.ETX_ADAPTER_IN_DIRECTORY, MaintenanceManagerJob.IN_BUNDLE_LEVEL);

        long ttlInMs = DAY_IN_MS;

        // when
        maintenanceManager.deleteOldBundlesInDirectory(tempDir, ttlInMs);

        // then
        Path level1_1 = Paths.get(tempDir.toString(), SYSTEM_DIR, LocalSharedFileRepositoryService.ETX_ADAPTER_IN_DIRECTORY, "level1_1");
        Path level2_1 = Paths.get(level1_1.toString(), "level2_1");

        assertThat(Files.notExists(Paths.get(level2_1.toString(), "level3_1")), is(equalTo(true)));
        assertThat(Files.exists(Paths.get(level2_1.toString(), "level3_2")), is(equalTo(true)));
        assertThat(Files.notExists(Paths.get(level2_1.toString(), "level3_3")), is(equalTo(true)));

        Path level2_2 = Paths.get(level1_1.toString(), "level2_2");
        assertThat(Files.exists(level2_2), is(equalTo(true)));

        Path level1_2 = Paths.get(tempDir.toString(), SYSTEM_DIR, LocalSharedFileRepositoryService.ETX_ADAPTER_IN_DIRECTORY, "level1_2");
        assertThat(Files.exists(level1_2), is(equalTo(true)));
    }

    @Test
    public void test_deleteOldBundlesInDirectory_should_attemptDepthLevelBasedDeleting_WithActualInOutDirStructure() throws IOException {
        Path inBundle1Dir = Paths.get(tempDir.toString(), "system/in/receiver_party/sender_party/bundle1");
        Path inBundle1WithMetaDir = Paths.get(inBundle1Dir.toString(), "meta");
        Files.createDirectories(inBundle1WithMetaDir);
        updatePathAccessTimes(inBundle1WithMetaDir, -2 * DAY_IN_MS);
        updatePathAccessTimes(inBundle1Dir, -2 * DAY_IN_MS);

        Path inBundle2WithOutMetaDir = Paths.get(tempDir.toString(),"system/in/receiver_party/sender_party/bundle2");
        Files.createDirectories(inBundle2WithOutMetaDir);
        updatePathAccessTimes(inBundle2WithOutMetaDir, -3 * DAY_IN_MS);

        Path outBundle1Dir = Paths.get(tempDir.toString(),"system/out/sender_party/bundle1");
        Files.createDirectories(outBundle1Dir);
        updatePathAccessTimes(outBundle1Dir, -3 * DAY_IN_MS);

        maintenanceManager.deleteOldBundlesInDirectory(tempDir, DAY_IN_MS);

        assertThat(Files.exists(inBundle1Dir), is(Boolean.FALSE));
        assertThat(Files.exists(inBundle1WithMetaDir), is(Boolean.FALSE));
        assertThat(Files.exists(inBundle2WithOutMetaDir), is(Boolean.FALSE));
        assertThat(Files.exists(outBundle1Dir), is(Boolean.FALSE));
        assertThat(Files.exists(Paths.get(tempDir.toString(), "system/in/receiver_party/sender_party")), is(Boolean.TRUE));
        assertThat(Files.exists(Paths.get(tempDir.toString(), "system/out/sender_party")), is(Boolean.TRUE));
    }

    @Test
    public void test_getTtlInDays_should_returnTTL_when_configurationPropertyAvailable() {
        Long ttlInDays = 12L;

        Map<AdapterConfigurationProperty, String> adapterConfiguration = new HashMap<>();
        adapterConfiguration.put(AdapterConfigurationProperty.ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS, ttlInDays.toString());

        // DO THE ACTUAL CALL
        Long result = maintenanceManager.getTtlInDays(adapterConfiguration);

        assertThat(result, is(equalTo(ttlInDays)));

        new FullVerifications() {{
        }};
    }

    @Test
    public void test_getTtlInDays_should_returnNull_when_configurationPropertyNotAvailable() {
        Map<AdapterConfigurationProperty, String> adapterConfiguration = new HashMap<>();

        // DO THE ACTUAL CALL
        Long result = maintenanceManager.getTtlInDays(adapterConfiguration);

        assertThat(result, is(nullValue()));

        new FullVerifications() {{
        }};
    }

    @Test
    public void test_getTtlInDays_should_returnNull_when_configurationPropertyFormatIsIncorrect() {
        String ttlInDays = "1K";
        final Map<AdapterConfigurationProperty, String> adapterConfiguration = new HashMap<>();
        adapterConfiguration.put(AdapterConfigurationProperty.ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS, ttlInDays);

        // DO THE ACTUAL CALL
        Long result = maintenanceManager.getTtlInDays(adapterConfiguration);

        assertThat(result, is(nullValue()));

        new FullVerifications() {{
        }};
    }

    @Test
    public void test_clean_should_startDeleting_when_TTLisGreaterThanZero() {
        long ttlInDays = 1L;
        final Map<AdapterConfigurationProperty, String> adapterConfiguration = new HashMap<>();
        adapterConfiguration.put(AdapterConfigurationProperty.ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS, String.valueOf(ttlInDays));

        new Expectations() {{
            adapterConfigurationService.getAdapterConfigurations();
            result = adapterConfiguration;
            times = 1;

            localSharedFileRepositoryService.getRootDirectory();
            times = 1;
            result = tempDir;
        }};

        // DO THE ACTUAL CALL
        maintenanceManager.process(null);

        new FullVerifications() {{
        }};
    }

    @Test
    public void test_clean_should_skipDeleting_when_TtlIsZero() {
        long ttlInDays = 0L;

        final Map<AdapterConfigurationProperty, String> adapterConfiguration = new HashMap<>();
        adapterConfiguration.put(AdapterConfigurationProperty.ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS, String.valueOf(ttlInDays));

        new Expectations() {{
            adapterConfigurationService.getAdapterConfigurations();
            result = adapterConfiguration;
            times = 1;

            localSharedFileRepositoryService.getRootDirectory();
            times = 1;
            result = tempDir;
        }};

        // DO THE ACTUAL CALL
        maintenanceManager.process(null);

        new FullVerifications() {{
        }};
    }

    @Test
    public void test_clean_should_skipDeleting_when_TtlIsNull() {
        final Map<AdapterConfigurationProperty, String> adapterConfiguration = new HashMap<>();

        new Expectations() {{
            adapterConfigurationService.getAdapterConfigurations();
            result = adapterConfiguration;
            times = 1;

            localSharedFileRepositoryService.getRootDirectory();
            times = 1;
            result = tempDir;
        }};

        // DO THE ACTUAL CALL
        maintenanceManager.process(null);

        new FullVerifications() {{
        }};
    }

    private void updatePathAccessTimes(Path path, long timeInMillis) throws IOException {
        final BasicFileAttributeView fileAttributeView = Files.getFileAttributeView(path, BasicFileAttributeView.class);
        final BasicFileAttributes fileAttributes = fileAttributeView.readAttributes();
        FileTime newTime = FileTime.fromMillis(fileAttributes.lastModifiedTime().toMillis() + timeInMillis);
        fileAttributeView.setTimes(newTime, newTime, newTime);
    }

    private void createBundleTestDirStructure(String directionDirectoryName, int level) throws IOException {
        //tempFolder == root
        //  - system
        //    - IN/OUT
        //         - levelX_Y
        // the folders: level<level>_1 and level<level>_3 should be deleted (the lastModifiedTime will be "creation time" -1 day and -2 days)

        Path directionFolder = Paths.get(tempDir.toString(), SYSTEM_DIR, directionDirectoryName);
        Files.createDirectories(directionFolder);

        Path levelPath = directionFolder;
        for (int i = 1; i <= level; i++) {
            if (i == level) {
                Path path = createLevelPath(levelPath, i, 1);
                updatePathAccessTimes(path, -2 * DAY_IN_MS);

                createLevelPath(levelPath, i, 2);
                path = createLevelPath(levelPath, i, 3);
                updatePathAccessTimes(path, -DAY_IN_MS);
            } else {
                Path path = createLevelPath(levelPath, i, 1);
                createLevelPath(levelPath, i, 2);

                levelPath = path;
            }
        }
    }

    private Path createLevelPath(Path currentPath, int level, int nbr) throws IOException {
        Path path = Paths.get(currentPath.toString(), "level" + level + "_" + nbr);
        return Files.createDirectories(path);
    }
}
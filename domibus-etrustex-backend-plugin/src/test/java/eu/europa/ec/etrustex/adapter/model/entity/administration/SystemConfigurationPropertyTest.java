package eu.europa.ec.etrustex.adapter.model.entity.administration;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * JUnit class for <code>SystemConfigurationProperty</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
public class SystemConfigurationPropertyTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testFromCode_HappyFlow() {

        //randomly test some of the values
        Assert.assertEquals(SystemConfigurationProperty.fromCode("etx.backend.download.attachments"),
                SystemConfigurationProperty.ETX_BACKEND_DOWNLOAD_ATTACHMENTS);
        Assert.assertEquals(SystemConfigurationProperty.fromCode("etx.backend.filerepositorylocation"),
                SystemConfigurationProperty.ETX_BACKEND_FILE_REPOSITORY_LOCATION);
        Assert.assertEquals(SystemConfigurationProperty.fromCode("etx.backend.notification.type"),
                SystemConfigurationProperty.ETX_BACKEND_NOTIFICATION_TYPE);
    }

    @Test
    public void testFromCode_UnhappyFlow() {
        final String wrongCode = "etx.backend.download.attachments2";

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("No configuration found for the input code: " + wrongCode);

        //do the thing, call with this bad value
        AdapterConfigurationProperty.fromCode(wrongCode);

    }

    @Test
    public void testFromCode_UnhappyFlow_Null() {
        final String wrongCode = null;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("No configuration found for the input code: " + wrongCode);

        //do the thing, call with this bad value
        AdapterConfigurationProperty.fromCode(wrongCode);

    }

    @Test
    public void testGetCode_HappyFlow() {

        //randomly test some of the values
        Assert.assertEquals(SystemConfigurationProperty.ETX_BACKEND_DOWNLOAD_ATTACHMENTS.getCode(), "etx.backend.download.attachments");
        Assert.assertEquals(SystemConfigurationProperty.ETX_BACKEND_FILE_REPOSITORY_LOCATION.getCode(), "etx.backend.filerepositorylocation");
        Assert.assertEquals(SystemConfigurationProperty.ETX_BACKEND_NOTIFICATION_TYPE.getCode(), "etx.backend.notification.type");
    }

}
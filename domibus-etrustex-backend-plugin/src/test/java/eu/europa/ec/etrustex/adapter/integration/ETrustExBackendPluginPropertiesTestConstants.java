package eu.europa.ec.etrustex.adapter.integration;

/**
 * @author Arun Raj
 * @version 1.0
 * @since 31/08/2017
 */
public class ETrustExBackendPluginPropertiesTestConstants {

    public static final String TEST_USER = "user";
    public static final String TEST_PWD = "pwd";
    public static final String TEST_DOMAIN = "domain";

    public static final String TEST_PLUGIN_PAYLOADS_DOWNLOAD_LOCATION = "src/test/resources/payloads";
    public static final String TEST_AS4_FROMPARTYIDTYPE = "urn:oasis:names:tc:ebcore:partyid-type:unregistered:ETXAS4";
    public static final String TEST_AS4_FROMPARTYROLE = "http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/initiator";
    public static final String TEST_AS4_ETRUSTEXNODEPARTYID = "ETxNodeParty";
    public static final String TEST_AS4_TOPARTYIDTYPE = "urn:oasis:names:tc:ebcore:partyid-type:unregistered:ETXAS4";
    public static final String TEST_AS4_TOPARTYROLE = "http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/responder";

    // ApplicationResponseService specific properties start
    public static final String TEST_AS4_SERVICE_APPLICATIONRESPONSE = "ApplicationResponseService";
    public static final String TEST_AS4_SERVICE_APPLICATIONRESPONSE_SERVICETYPE = "eDelivery";
    public static final String TEST_AS4_ACTION_SUBMITAPPLICATIONRESPONSE_REQUEST = "SubmitApplicationResponseRequest";
    public static final String TEST_AS4_ACTION_SUBMITAPPLICATIONRESPONSE_RESPONSE = "SubmitApplicationResponseResponse";
    //ApplicationResponseService specific properties end

    //DocumentWrapperService specific properties start
    public static final String TEST_AS4_SERVICE_DOCUMENTWRAPPER = "DocumentWrapperService";
    public static final String TEST_AS4_SERVICE_DOCUMENTWRAPPER_SERVICETYPE = "eDelivery";
    public static final String TEST_AS4_ACTION_STOREDOCUMENTWRAPPER_REQUEST = "StoreDocumentWrapperRequest";
    public static final String TEST_AS4_ACTION_STOREDOCUMENTWRAPPER_RESPONSE = "StoreDocumentWrapperResponse";
    //StoreDocumentWrapperService specific properties end
    //DocumentBundleService specific properties start
    public static final String TEST_AS4_SERVICE_DOCUMENTBUNDLE = "DocumentBundleService";
    public static final String TEST_AS4_SERVICE_DOCUMENTBUNDLE_SERVICETYPE = "eDelivery";
    public static final String TEST_AS4_ACTION_SUBMITDOCUMENTBUNDLE_REQUEST = "SubmitDocumentBundleRequest";
    public static final String TEST_AS4_ACTION_SUBMITDOCUMENTBUNDLE_RESPONSE = "SubmitDocumentBundleResponse";
    //DocumentBundleService specific properties end
    //RetrieveICAService specific properties start
    public static final String TEST_AS4_SERVICE_RETRIEVEICA = "RetrieveICAService";
    public static final String TEST_AS4_SERVICE_RETRIEVEICA_SERVICETYPE = "eDelivery";
    public static final String TEST_AS4_ACTION_RETRIEVEICA_REQUEST = "RetrieveICARequest";
    public static final String TEST_AS4_ACTION_RETRIEVEICA_RESPONSE = "RetrieveICAResponse";
    //RetrieveICAService specific properties end
    // InboxBundleTransmission
    public static final String TEST_AS4_SERVICE_INBOXBUNDLETRANSMISSION = "InboxBundleTransmissionService";
    public static final String TEST_AS4_SERVICE_INBOXBUNDLETRANSMISSION_SERVICETYPE = "eDelivery";
    public static final String TEST_AS4_SERVICE_INBOXBUNDLETRANSMISSION_REQUEST = "NotifyInboxBundle";
    public static final String TEST_AS4_SERVICE_INBOXBUNDLETRANSMISSION_ACK = "AckInboxBundleNotification";
    // InboxBundleTransmission
    public static final String connectionTimeout = "30000";
    public static final String receiveTimeout = "120000";

    public static final String SYSTEM_DEFAULT = "FAKE_SYSTEM";
}

package eu.europa.ec.etrustex.adapter.application.passwordencryption;

import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.services.PasswordEncryptionExtService;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class ETrustExBackendPluginPasswordEncryptionContextTest {

    @Mocked
    private ETrustExBackendPluginProperties eTrustExBackendPluginProperties;
    @Mocked
    private PasswordEncryptionExtService pluginPasswordEncryptionService;

    private ETrustExBackendPluginPasswordEncryptionContext config;
    private DomainDTO domain;

    private String property1 = "test1";
    private String property2 = "test2";
    private String property3 = "test3";

    @Before
    public void setUp() {
        domain = new DomainDTO();
        config = new ETrustExBackendPluginPasswordEncryptionContext(eTrustExBackendPluginProperties,
                pluginPasswordEncryptionService,
                "config",
                domain);
    }

    @Test
    public void getPropertiesToEncrypt_Empty_notEncrypted_encrypted() {

        new Expectations(){{
            eTrustExBackendPluginProperties.getEncryptedProperties();
            times = 1;
            result = property1+","+property2+","+property3+",";

            eTrustExBackendPluginProperties.getProperty(property1);
            times = 1;
            result = "";

            eTrustExBackendPluginProperties.getProperty(property2);
            times = 1;
            result = "NotEncoded";

            eTrustExBackendPluginProperties.getProperty(property3);
            times = 1;
            result = "ENC(alreadyEncoded)";

            pluginPasswordEncryptionService.isValueEncrypted("NotEncoded");
            times = 1;
            result = false;

            pluginPasswordEncryptionService.isValueEncrypted( "ENC(alreadyEncoded)");
            times = 1;
            result = true;

        }};

        config.getPropertiesToEncrypt();

        new FullVerifications(){{

        }};
    }
    @Test
    public void getPropertiesToEncrypt_noProperties() {

        new Expectations(){{
            eTrustExBackendPluginProperties.getEncryptedProperties();
            times = 1;
            result = "";

        }};

        config.getPropertiesToEncrypt();

        new FullVerifications(){{

        }};
    }
}
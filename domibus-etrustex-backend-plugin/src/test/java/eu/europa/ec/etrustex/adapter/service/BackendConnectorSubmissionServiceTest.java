package eu.europa.ec.etrustex.adapter.service;

import eu.domibus.ext.services.AuthenticationExtService;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.integration.ETrustExDomibusBackendConnector;
import eu.europa.ec.etrustex.common.exceptions.DomibusSubmissionException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import mockit.*;
import org.junit.Test;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

import static org.junit.Assert.fail;

public class BackendConnectorSubmissionServiceTest {

    @Injectable
    private ETrustExBackendPluginProperties etxBackendPluginProperties;

    @Injectable
    private AuthenticationExtService authenticationExtService;

    @Injectable
    private ETrustExDomibusBackendConnector eTrustExDomibusBackendConnector;

    @Tested
    private BackendConnectorSubmissionService backendConnectorSubmissionService;

    @Test
    public void submit_nullAuth(@Mocked final SecurityContextHolder securityContextHolder) throws Exception {
        ETrustExAdapterDTO dto = new ETrustExAdapterDTO();

        SecurityContextImpl securityContext = new SecurityContextImpl();
        new Expectations() {{
            SecurityContextHolder.getContext();
            result = securityContext;
            times = 1;

            securityContext.getAuthentication();
            result = null;
            times = 1;

            etxBackendPluginProperties.getUser();
            result = "user";
            times = 1;

            etxBackendPluginProperties.getPwd();
            result = "pwd";
            times = 1;

            eTrustExDomibusBackendConnector.submit(dto);
            result = null;
            times = 1;
        }};

        backendConnectorSubmissionService.submit(dto);

        new FullVerifications() {{
            authenticationExtService.basicAuthenticate("user", "pwd");
            times = 1;
        }};
    }

    @Test
    public void submit_notAuth(@Mocked final SecurityContextHolder securityContextHolder, @Mocked SecurityContext securityContext, @Mocked org.springframework.security.core.Authentication authentication) throws Exception {
        ETrustExAdapterDTO dto = new ETrustExAdapterDTO();

        new Expectations() {{
            SecurityContextHolder.getContext();
            result = securityContext;
            times = 2;

            securityContext.getAuthentication();
            result = authentication;
            times = 2;

            authentication.isAuthenticated();
            result = false;
            times = 1;

            etxBackendPluginProperties.getUser();
            result = "user";
            times = 1;

            etxBackendPluginProperties.getPwd();
            result = "pwd";
            times = 1;

            eTrustExDomibusBackendConnector.submit(dto);
            result = null;
            times = 1;
        }};

        backendConnectorSubmissionService.submit(dto);

        new FullVerifications() {{
            authenticationExtService.basicAuthenticate("user", "pwd");
            times = 1;
        }};
    }

    @Test
    public void submit_Auth(@Mocked final SecurityContextHolder securityContextHolder, @Mocked SecurityContext securityContext, @Mocked org.springframework.security.core.Authentication authentication) throws Exception {
        ETrustExAdapterDTO dto = new ETrustExAdapterDTO();

        new Expectations() {{
            SecurityContextHolder.getContext();
            result = securityContext;
            times = 2;

            securityContext.getAuthentication();
            result = authentication;
            times = 2;

            authentication.isAuthenticated();
            result = true;
            times = 1;

            eTrustExDomibusBackendConnector.submit(dto);
            result = null;
            times = 1;
        }};

        backendConnectorSubmissionService.submit(dto);

        new FullVerifications() {{
        }};
    }

    @Test
    public void submit_Error(@Mocked final SecurityContextHolder securityContextHolder, @Mocked SecurityContext securityContext, @Mocked org.springframework.security.core.Authentication authentication) throws Exception {
        ETrustExAdapterDTO dto = new ETrustExAdapterDTO();

        new Expectations() {{
            SecurityContextHolder.getContext();
            result = securityContext;
            times = 2;

            securityContext.getAuthentication();
            result = authentication;
            times = 2;

            authentication.isAuthenticated();
            result = true;
            times = 1;

            eTrustExDomibusBackendConnector.submit(dto);
            result = new IllegalArgumentException("ERROR");
            times = 1;
        }};

        try {
            backendConnectorSubmissionService.submit(dto);
            fail();
        } catch (DomibusSubmissionException e) {
            //OK
        }

        new FullVerifications() {{
        }};
    }
}
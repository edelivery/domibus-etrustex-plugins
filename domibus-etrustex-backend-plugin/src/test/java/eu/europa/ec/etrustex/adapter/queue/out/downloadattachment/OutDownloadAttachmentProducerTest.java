package eu.europa.ec.etrustex.adapter.queue.out.downloadattachment;

import eu.europa.ec.etrustex.adapter.queue.JmsProducer;
import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.jms.Queue;
import java.util.Arrays;
import java.util.List;

/**
 * JUnit class for <code>OutDownloadAttachmentProducer</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@RunWith(JMockit.class)
public class OutDownloadAttachmentProducerTest {

    @Injectable
    Queue outboxDownloadAttachment;

    @Injectable
    JmsProducer<OutDownloadAttachmentQueueMessage> jmsProducer;

    @Tested
    OutDownloadAttachmentProducer outDownloadAttachmentProducer;

    @Test
    public void testTriggerDownload_HappyFlow(final @Mocked List<OutDownloadAttachmentQueueMessage> queueMessages) throws Exception {

        final List<Long> attachmentIds = Arrays.asList(new Long[]{10L, 20L});

        //method to be tested
        outDownloadAttachmentProducer.triggerDownload(attachmentIds);

        new Verifications() {{

            List<OutDownloadAttachmentQueueMessage> queueMessagesActual;
            jmsProducer.postMessageList(outboxDownloadAttachment, queueMessagesActual = withCapture());
            Assert.assertNotNull("captured object shouldn't be null", queueMessagesActual);
            Assert.assertTrue("list size must match", queueMessagesActual.size() == 2);
            Assert.assertEquals("attachmentId must match", new Long(10L), queueMessagesActual.get(0).getAttachmentId());
            Assert.assertEquals("attachmentId must match", new Long(20L), queueMessagesActual.get(1).getAttachmentId());
        }};

    }

}
package eu.europa.ec.etrustex.adapter.persistence.api.administration;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.persistence.api.AbstractDaoIT;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBase;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 29-Jan-18
 */
public class PartyDaoIT extends AbstractDaoIT<EtxParty>{
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(PartyDaoIT.class);
    @Autowired
    private PartyDao partyDao;
    @Autowired
    private SystemDao systemDao;
    @Autowired
    private UserDao userDao;

    @Test
    public void findByPartyUUID_notFound() {
        EtxParty result = partyDao.findByPartyUUID("NOT_FOUND");
        assertThat(result, is(nullValue()));
    }

    @Test
    public void findByPartyUUID_ok() {
        EtxParty result = partyDao.findByPartyUUID("PARTY_1");
        assertThat(result, is(notNullValue()));
    }

    @Test
    public void findAll_ok() {
        List<EtxParty> result = partyDao.findAll();
        for (EtxParty etxParty : result) {
            LOG.info(etxParty.toString());
        }
        assertThat(result, is(notNullValue()));
        assertThat(result.size(), is(2));
    }

    @Override
    protected Long getExistingId() {
        return 1001L;
    }

    @Override
    protected DaoBase<EtxParty> getDao() {
        return partyDao;
    }

    @Override
    protected EtxParty getNewEntity() {
        EtxParty etxParty = new EtxParty();
        etxParty.setPartyUuid(UUID.randomUUID().toString());
        etxParty.setSystem(systemDao.findById(101L));
        return etxParty;
    }
}
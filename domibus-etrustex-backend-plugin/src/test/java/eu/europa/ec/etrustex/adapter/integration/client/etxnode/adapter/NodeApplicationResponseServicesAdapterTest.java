package eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter;

import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseRequest;
import eu.domibus.messaging.PModeMismatchException;
import eu.domibus.plugin.exception.TransformationException;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.converters.NodeDocumentBundleTypeConverter;
import eu.europa.ec.etrustex.adapter.integration.handlers.ResponseFaultHandler;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.testutils.JsonUtils;
import eu.europa.ec.etrustex.common.exceptions.DomibusSubmissionException;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.common.util.ConversationIdKey;
import mockit.*;
import org.hibernate.exception.GenericJDBCException;
import org.junit.Assert;
import org.junit.Test;

import javax.xml.ws.Holder;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginPropertiesTestConstants.*;
import static eu.europa.ec.etrustex.adapter.testutils.ResourcesUtils.readResource;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.getObjectFromXml;
import static org.junit.Assert.assertThat;

/**
 * @author Arun Raj
 * @version 1.0
 * @since 29/08/2017
 */
public class NodeApplicationResponseServicesAdapterTest extends BaseAdapterUnitTest {

    @Injectable
    private ResponseFaultHandler responseFaultHandler;

    @Capturing(maxInstances = 1)
    @Tested
    private NodeApplicationResponseServicesAdapter nodeApplicationResponseServicesAdapter;

    /**
     * Tests the entire conversion from {@link EtxMessage} to forming the request parameters of {@link ec.services.wsdl.applicationresponse_2.ApplicationResponseService}:submitApplicationResponse
     * to forming the {@link ETrustExAdapterDTO} and Domibus Submission for the leg - nodeSubmitApplicationResponseRequest
     */
    @Test
    public void testSendMessageStatus_SuccessfulSubmissionToDomibus() throws Exception {

        final EtxMessage etxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageStatus_NOTIFIED_READ.json"),
                EtxMessage.class);
        final String domibusMessageId = "DomibusMessageId1";

        new Expectations(){{
            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            result = domibusMessageId;
            times = 1;
        }};

        //function under test
        nodeApplicationResponseServicesAdapter.sendMessageStatus(etxMessage);

        new FullVerifications() {{
            nodeApplicationResponseServicesAdapter.fillAuthorisationHeaderWithStatusScope(anyString, anyString);
            times = 1;

            NodeDocumentBundleTypeConverter.convertEtxMessage(etxMessage);
            times = 1;

            nodeApplicationResponseServicesAdapter.populateETrustExAdapterDTOForSubmitApplicationResponse(
                    withAny(new Holder<HeaderType>()),
                    withAny(new SubmitApplicationResponseRequest()),
                    etxMessage);
            times = 1;

            ETrustExAdapterDTO eTrustExAdapterDTO;
            backendConnectorSubmissionService.submit(eTrustExAdapterDTO = withCapture());
            times = 1;

            Assert.assertEquals(etxMessage.getId(), Long.valueOf(eTrustExAdapterDTO.getEtxBackendMessageId()));
            Assert.assertEquals(etxMessage.getSender().getPartyUuid(), eTrustExAdapterDTO.getAs4OriginalSender());

            Assert.assertEquals(etxMessage.getSender().getPartyUuid(), eTrustExAdapterDTO.getAs4FromPartyId());
            Assert.assertEquals(ConversationIdKey.OUTBOX_SEND_MESSAGE_STATUS.getConversationIdKey() + "1", eTrustExAdapterDTO.getAs4ConversationId());
            Assert.assertEquals(TEST_AS4_FROMPARTYIDTYPE, eTrustExAdapterDTO.getAs4FromPartyIdType());
            Assert.assertEquals(TEST_AS4_FROMPARTYROLE, eTrustExAdapterDTO.getAs4FromPartyRole());
            Assert.assertEquals(TEST_AS4_ETRUSTEXNODEPARTYID, eTrustExAdapterDTO.getAs4ToPartyId());
            Assert.assertEquals(TEST_AS4_TOPARTYIDTYPE, eTrustExAdapterDTO.getAs4ToPartyIdType());
            Assert.assertEquals(TEST_AS4_TOPARTYROLE, eTrustExAdapterDTO.getAs4ToPartyRole());
            Assert.assertEquals(etxMessage.getReceiver().getPartyUuid(), eTrustExAdapterDTO.getAs4FinalRecipient());
            Assert.assertEquals(TEST_AS4_SERVICE_APPLICATIONRESPONSE, eTrustExAdapterDTO.getAs4Service());
            Assert.assertEquals(TEST_AS4_SERVICE_APPLICATIONRESPONSE_SERVICETYPE, eTrustExAdapterDTO.getAs4ServiceType());
            Assert.assertEquals(TEST_AS4_ACTION_SUBMITAPPLICATIONRESPONSE_REQUEST, eTrustExAdapterDTO.getAs4Action());
            Assert.assertNotNull("Payload with content id Header should not be null", eTrustExAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_HEADER));
            Assert.assertNotNull("Payload with content id - generic should not be null", eTrustExAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_GENERIC));

            assertThat(
                    getObjectFromXml(eTrustExAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_HEADER).getPayloadAsString()),
                    sameBeanAs(getObjectFromXml(readResource("data/Header.xml")))
            );
            assertThat(
                    getObjectFromXml(eTrustExAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_GENERIC).getPayloadAsString()),
                    sameBeanAs(getObjectFromXml(readResource("data/SubmitApplicationResponseRequest.xml")))
                            .ignoring("applicationResponse.issueDate.value")
            );
        }};

        Assert.assertEquals(domibusMessageId, etxMessage.getDomibusMessageId());
    }

    /**
     * Test handling of synchronous errors (MessageProcessingException) thrown on Submission to Domibus.
     * A mandatory AS4 element e.g: AS4 action is missig
     *
     */
    @Test
    public void testSendMessageStatus_ValidationFailureOnDomibusSubmission_MissingMandatoryElement() throws Exception {

        final EtxMessage sendMessageStatus_EtxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageStatus_NOTIFIED_READ.json"),
                EtxMessage.class);

        final TransformationException mpEx = new TransformationException("Sample Validation Error from Domibus", new IllegalArgumentException("action must not be empty"));
        final DomibusSubmissionException etEx = new DomibusSubmissionException( mpEx.getMessage(), mpEx);

        new Expectations() {{
            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            times = 1;
            result = etEx;
        }};

        //function under test
        nodeApplicationResponseServicesAdapter.sendMessageStatus(sendMessageStatus_EtxMessage);

        new FullVerifications() {{
            responseFaultHandler.process(anyString, (EtxMessage) any);
            times = 1;
        }};
    }

    /**
     * Test handling of synchronous errors (MessageProcessingException) thrown on Submission to Domibus.
     * pMode is incorrectly modified and a required property (e.g: - eTxBackendMsgIdProperty) is missing.
     *
     */
    @Test
    public void testSendMessageStatus_ValidationFailureOnDomibusSubmission_PModeMismatch() throws Exception {

        final EtxMessage sendMessageStatus_EtxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageStatus_NOTIFIED_READ.json"),
                EtxMessage.class);


        final PModeMismatchException mpEx = new PModeMismatchException("Property profiling for this exchange does not include a property named [eTxBackendMsgId]");
        final DomibusSubmissionException etEx = new DomibusSubmissionException( "Sample Validation Error from Domibus", mpEx);


        new Expectations() {{
            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            times = 1;
            result = etEx;
        }};

        //function under test
        nodeApplicationResponseServicesAdapter.sendMessageStatus(sendMessageStatus_EtxMessage);

        new FullVerifications() {{
            responseFaultHandler.process(anyString, (EtxMessage) any);
            times = 1;
        }};
    }

    /**
     * Scenario when other types of exception are encountered in Domibus Submission
     *
     */
    @Test
    public void testSendMessageStatus_Exception() throws Exception {

        final EtxMessage etxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageStatus_NOTIFIED_READ.json"),
                EtxMessage.class);

        final GenericJDBCException jbbcException = new GenericJDBCException("Could not open connection", new java.sql.SQLException("Unexpected exception while enlisting XAConnection", new java.sql.SQLException("Transaction rolled back: Transaction timed out after 215 seconds")));

        new Expectations() {{
            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            times = 1;
            result = jbbcException;
        }};

        //function under test
        try {
            nodeApplicationResponseServicesAdapter.sendMessageStatus(etxMessage);
            Assert.fail("Expected eTrustEx exception to be thrown");
        } catch (ETrustExPluginException etxAE) {
            Assert.assertEquals(ETrustExError.DEFAULT.getCode(), etxAE.getError().getCode());
        }

        new FullVerifications() {{
            responseFaultHandler.process(anyString, (EtxMessage) any);
            times = 0;
        }};
    }

    /**
     * Testing scenario of failure on message conversion due to empty elements in request.
     */
    @SuppressWarnings("Duplicates")
    @Test
    public void testSendMessageStatus_FailureOnMessageConversion() {

        final EtxMessage etxMessage = getEtxMessage();

        try {
            //function under test
            nodeApplicationResponseServicesAdapter.sendMessageStatus(etxMessage);
            Assert.fail("Expected eTrustEx exception to be thrown");
        } catch (ETrustExPluginException etxAE) {
            Assert.assertEquals(ETrustExError.DEFAULT.getCode(), etxAE.getError().getCode());
        }

        new FullVerifications() {{
            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            times = 0;

            responseFaultHandler.process(anyString, (EtxMessage) any);
            times = 0;
        }};
    }

    @Test
    public void testPopulateETrustExAdapterDTOForSubmitApplicationResponse_NullInputForPayload() throws Exception {
        try {
            //function under test
            nodeApplicationResponseServicesAdapter.populateETrustExAdapterDTOForSubmitApplicationResponse(null, null, JsonUtils.getObjectMapper().readValue(
                    readResource("data/EtxMessageStatus_NOTIFIED_READ.json"),
                    EtxMessage.class));
            Assert.fail("Expected Null pointer exception here");
        } catch (NullPointerException npe) {
            assert true;
        }
    }
}

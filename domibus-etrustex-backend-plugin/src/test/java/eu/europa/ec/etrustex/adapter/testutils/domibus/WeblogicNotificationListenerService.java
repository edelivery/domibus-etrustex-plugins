
package eu.europa.ec.etrustex.adapter.testutils.domibus;

import eu.domibus.plugin.BackendConnector;

import javax.jms.Queue;

/**
 * @author Christian Koch, Stefan Mueller
 */

public class WeblogicNotificationListenerService extends NotificationListenerService  {

    public WeblogicNotificationListenerService(final Queue queue, final BackendConnector.Mode mode) {
        super(queue, mode);
    }
}

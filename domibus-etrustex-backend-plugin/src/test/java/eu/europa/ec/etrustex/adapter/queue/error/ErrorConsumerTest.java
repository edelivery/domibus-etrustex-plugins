package eu.europa.ec.etrustex.adapter.queue.error;

import eu.domibus.ext.services.DomainContextExtService;
import eu.domibus.ext.services.DomainExtService;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPluginService;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.queue.ETrustExBackendPluginJMSErrorUtils;
import eu.europa.ec.etrustex.adapter.queue.deadletter.DeadLetterProducer;
import eu.europa.ec.etrustex.adapter.queue.inbox.uploadattachment.InboxUploadAttachmentHandler;
import eu.europa.ec.etrustex.adapter.queue.inbox.uploadattachment.InboxUploadAttachmentQueueMessage;
import eu.europa.ec.etrustex.adapter.queue.notify.NotifyHandler;
import eu.europa.ec.etrustex.adapter.queue.notify.NotifyQueueMessage;
import eu.europa.ec.etrustex.adapter.queue.out.downloadattachment.OutDownloadAttachmentHandler;
import eu.europa.ec.etrustex.adapter.queue.out.downloadattachment.OutDownloadAttachmentQueueMessage;
import eu.europa.ec.etrustex.adapter.queue.out.sendmessage.SendMessageHandler;
import eu.europa.ec.etrustex.adapter.queue.out.sendmessage.SendMessageQueueMessage;
import eu.europa.ec.etrustex.adapter.queue.out.uploadattachment.OutUploadAttachmentHandler;
import eu.europa.ec.etrustex.adapter.queue.out.uploadattachment.OutUploadAttachmentQueueMessage;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessHandler;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessQueueMessage;
import eu.europa.ec.etrustex.adapter.queue.postupload.PostUploadHandler;
import eu.europa.ec.etrustex.adapter.queue.postupload.PostUploadQueueMessage;
import eu.europa.ec.etrustex.common.jms.ErrorQueueMessage;
import eu.europa.ec.etrustex.common.jms.QueueMessage;
import eu.europa.ec.etrustex.common.jms.TextMessageConverter;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static eu.domibus.logging.DomibusLogger.MDC_MESSAGE_ID;

/**
 * @author François Gautier
 * @version 1.0
 * @since 06/06/2018
 */
@RunWith(Parameterized.class)
public class ErrorConsumerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Parameterized.Parameter
    public QueueMessage queueMessage;
    @Parameterized.Parameter(1)
    public Class<?> clazz;
    @Injectable
    private DeadLetterProducer deadLetterProducer;
    @Injectable
    private InboxUploadAttachmentHandler inboxUploadAttachmentHandler;
    @Injectable
    private NotifyHandler notifyHandler;
    @Injectable
    private OutDownloadAttachmentHandler outDownloadAttachmentHandler;
    @Injectable
    private OutUploadAttachmentHandler outUploadAttachmentHandler;
    @Injectable
    private SendMessageHandler sendMessageHandler;
    @Injectable
    private PostUploadHandler postUploadHandler;
    @Injectable
    private ErrorHandler errorHandler;
    @Injectable
    private PostProcessHandler postProcessHandler;
    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Injectable
    private DomainBackEndPluginService domainBackEndPluginService;
    @Tested
    private ErrorConsumer errorConsumer;
    private Map<Class<?>, VerificationConsumer> map;
    @Injectable
    private TextMessageConverter textMessageConverter;
    @Injectable
    @Qualifier("etxBackendPluginProperties")
    private ETrustExBackendPluginProperties etxBackendPluginProperties;
    @Injectable
    private DomainExtService domainExtService;
    @Injectable
    private DomainContextExtService domainContextExtService;
    @Injectable
    private ETrustExBackendPluginJMSErrorUtils eTrustExBackendPluginJMSErrorUtils;

    @Parameterized.Parameters(name = "{index}: QueueMessage({0}) handle by {1}")
    public static Collection combinations() {
        return Arrays.asList(new Object[][]{
                // Happy flow scenario
                {new InboxUploadAttachmentQueueMessage(), InboxUploadAttachmentHandler.class},
                {new NotifyQueueMessage(), NotifyHandler.class},
                {new OutDownloadAttachmentQueueMessage(), OutDownloadAttachmentHandler.class},
                {new OutUploadAttachmentQueueMessage(), OutUploadAttachmentHandler.class},
                {new SendMessageQueueMessage(null), SendMessageHandler.class},
                {new PostUploadQueueMessage(null), PostUploadHandler.class},
                {new PostProcessQueueMessage(null), PostProcessHandler.class},
                {new ErrorQueueMessage(null), ErrorHandler.class},

                // returns null
                {null, DeadLetterProducer.class},
        });
    }

    @Before
    public void setUp() {
        map = new HashMap<>();
        map.put(DeadLetterProducer.class, new VerificationConsumer() {
            @Override
            public void verif() {
                new FullVerifications() {{
                    domainBackEndPluginService.setBackendPluginDomain();
                    times = 1;
                    clearEtxLogKeysService.clearKeys(MDC_MESSAGE_ID);
                    times = 1;
                    deadLetterProducer.triggerDeadLetter((QueueMessage) any);
                    times = 1;
                }};
            }
        });
        map.put(InboxUploadAttachmentHandler.class, new VerificationConsumer() {
            @Override
            public void verif() {
                new FullVerifications() {{
                    domainBackEndPluginService.setBackendPluginDomain();
                    times = 1;
                    clearEtxLogKeysService.clearKeys(MDC_MESSAGE_ID);
                    times = 1;
                    inboxUploadAttachmentHandler.onError(anyLong);
                    times = 1;
                }};
            }
        });
        map.put(NotifyHandler.class, new VerificationConsumer() {
            @Override
            public void verif() {
                new FullVerifications() {{
                    domainBackEndPluginService.setBackendPluginDomain();
                    times = 1;
                    clearEtxLogKeysService.clearKeys(MDC_MESSAGE_ID);
                    times = 1;
                    notifyHandler.onError(anyLong);
                    times = 1;
                }};
            }
        });
        map.put(OutDownloadAttachmentHandler.class, new VerificationConsumer() {
            @Override
            public void verif() {
                new FullVerifications() {{
                    domainBackEndPluginService.setBackendPluginDomain();
                    times = 1;
                    clearEtxLogKeysService.clearKeys(MDC_MESSAGE_ID);
                    times = 1;
                    outDownloadAttachmentHandler.onError(anyLong);
                    times = 1;
                }};
            }
        });
        map.put(OutUploadAttachmentHandler.class, new VerificationConsumer() {
            @Override
            public void verif() {
                new FullVerifications() {{
                    domainBackEndPluginService.setBackendPluginDomain();
                    times = 1;
                    clearEtxLogKeysService.clearKeys(MDC_MESSAGE_ID);
                    times = 1;
                    outUploadAttachmentHandler.onError(anyLong);
                    times = 1;
                }};
            }
        });
        map.put(SendMessageHandler.class, new VerificationConsumer() {
            @Override
            public void verif() {
                new FullVerifications() {{
                    domainBackEndPluginService.setBackendPluginDomain();
                    times = 1;
                    clearEtxLogKeysService.clearKeys(MDC_MESSAGE_ID);
                    times = 1;
                    sendMessageHandler.onError(anyLong);
                    times = 1;
                }};
            }
        });
        map.put(PostProcessHandler.class, new VerificationConsumer() {
            @Override
            public void verif() {
                new FullVerifications() {{
                    domainBackEndPluginService.setBackendPluginDomain();
                    clearEtxLogKeysService.clearKeys(MDC_MESSAGE_ID);
                    postProcessHandler.onError(anyLong);
                    times = 1;
                }};
            }
        });
        map.put(PostUploadHandler.class, new VerificationConsumer() {
            @Override
            public void verif() {
                new FullVerifications() {{
                    domainBackEndPluginService.setBackendPluginDomain();
                    clearEtxLogKeysService.clearKeys(MDC_MESSAGE_ID);
                    postUploadHandler.onError(anyLong);
                    times = 1;
                }};
            }
        });
        map.put(ErrorHandler.class, new VerificationConsumer() {
            @Override
            public void verif() {
                new FullVerifications() {{
                    domainBackEndPluginService.setBackendPluginDomain();
                    clearEtxLogKeysService.clearKeys(MDC_MESSAGE_ID);

                    errorHandler.onError(anyString);
                    times = 1;
                }};
            }
        });
    }

    @Test
    public void getFollowUp() {

        errorConsumer.handleMessage(queueMessage);
        errorConsumer.validateMessage(queueMessage);

        map.get(clazz).verif();
    }

    private abstract class VerificationConsumer {
        public abstract void verif();
    }
}
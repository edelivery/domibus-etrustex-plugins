package eu.europa.ec.etrustex.adapter.integration.client.backend.repository;

import eu.europa.ec.etrustex.adapter.model.entity.administration.BackendFileRepositoryLocationType;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class FileRepositoryProviderTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Injectable
    LocalSharedFileRepositoryService localSharedFileRepositoryService;

    @Injectable
    WebServiceFileRepositoryService webServiceFileRepositoryService;

    @Tested
    FileRepositoryProvider fileRepositoryProvider;

    @Test
    public void testGetFileRepository_HappyFlow() throws Exception {

        final BackendFileRepositoryLocationType backendFileRepositoryLocationType =
                BackendFileRepositoryLocationType.LOCAL_SHARED_FILE_SYSTEM;

        FileRepository fileRepository = fileRepositoryProvider.getFileRepository(backendFileRepositoryLocationType);

        Assert.assertNotNull(fileRepository);
        Assert.assertEquals(localSharedFileRepositoryService, fileRepository);

    }

    @Test
    public void testGetFileRepository_Backend() throws Exception {

        final BackendFileRepositoryLocationType backendFileRepositoryLocationType =
                BackendFileRepositoryLocationType.BACKEND_FILE_REPOSITORY_SERVICE;

        FileRepository fileRepository = fileRepositoryProvider.getFileRepository(backendFileRepositoryLocationType);
        Assert.assertNotNull(fileRepository);
        Assert.assertEquals(webServiceFileRepositoryService, fileRepositoryProvider.getFileRepository(backendFileRepositoryLocationType));

    }

    @Test
    public void testGetFileRepository_UnhappyFlow() throws Exception {

        //exception is thrown but the enum itself as not possible to add dynamically a new value to the enum - see default case
        thrown.expect(IllegalArgumentException.class);

        final BackendFileRepositoryLocationType backendFileRepositoryLocationType =
                BackendFileRepositoryLocationType.valueOf("test");

        FileRepository fileRepository = fileRepositoryProvider.getFileRepository(backendFileRepositoryLocationType);

        Assert.assertNotNull(fileRepository);
        Assert.assertEquals(webServiceFileRepositoryService, fileRepositoryProvider.getFileRepository(backendFileRepositoryLocationType));

    }

}
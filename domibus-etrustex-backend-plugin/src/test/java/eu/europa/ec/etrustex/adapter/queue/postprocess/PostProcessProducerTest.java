package eu.europa.ec.etrustex.adapter.queue.postprocess;

import eu.europa.ec.etrustex.adapter.queue.JmsProducer;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.jms.Queue;

/**
 * JUnit class for <code>PostProcessProducer</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@RunWith(JMockit.class)
public class PostProcessProducerTest {
    private final Long messageId = 10L;
    private final Long attachmentId = 20L;

    @Injectable
    private Queue postProcessing;

    @Injectable
    private JmsProducer<PostProcessQueueMessage> jmsProducer;

    @Tested
    private PostProcessProducer postProcessProducer;

    @Test
    public void testTriggerPostProcess_HappyFlow() {

        final PostProcessQueueMessage queueMessage = new PostProcessQueueMessage(messageId);

        //method to test
        postProcessProducer.triggerPostProcess(messageId);

        new Verifications() {{
            PostProcessQueueMessage queueMessageActual;
            jmsProducer.postMessage(postProcessing, queueMessageActual = withCapture());
            times = 1;
            Assert.assertEquals("Queue Message must match", queueMessage, queueMessageActual);
            Assert.assertEquals("message id must match", messageId, queueMessageActual.getMessageId());

        }};
    }

    @Test
    public void testTriggerPostProcess_ForMessageAndAttachmentId() {

        final PostProcessQueueMessage queueMessage = new PostProcessQueueMessage(messageId, attachmentId);

        //method to test
        postProcessProducer.triggerPostProcess(messageId, attachmentId);

        new FullVerifications() {{
            PostProcessQueueMessage queueMessageActual;
            jmsProducer.postMessage(postProcessing, queueMessageActual = withCapture());
            times = 1;
            Assert.assertEquals("Queue Message must match", queueMessage, queueMessageActual);
            Assert.assertEquals("message id must match", messageId, queueMessageActual.getMessageId());
            Assert.assertEquals("message id must match", attachmentId, queueMessageActual.getAttachmentId());
        }};
    }

}
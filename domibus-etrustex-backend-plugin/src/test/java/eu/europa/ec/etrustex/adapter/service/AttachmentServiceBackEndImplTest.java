package eu.europa.ec.etrustex.adapter.service;

import eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter.NodeDocumentWrapperServiceAdapterTest;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.persistence.api.attachment.AttachmentDao;
import eu.europa.ec.etrustex.adapter.persistence.api.common.ErrorDao;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.service.WorkspaceService;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.*;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * JUnit class for <code>AttachmentServiceImpl</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@SuppressWarnings({"ResultOfMethodCallIgnored", "Duplicates"})
@RunWith(JMockit.class)
public class AttachmentServiceBackEndImplTest {

    public static final String CODE = "code";
    public static final String DESCRIPTION = "description";
    private static final long RESULT = 3L;
    @Injectable
    private AttachmentDao attachmentDao;
    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Injectable
    private WorkspaceService etxBackendPluginWorkspaceService;
    @Injectable
    private ErrorDao errorDao;

    @Tested
    private AttachmentServiceBackEndImpl attachmentService;

    @Test
    public void attachmentInError() {
        final EtxAttachment etxAttachment = new EtxAttachment();

        new Expectations() {{
            attachmentDao.findById(RESULT);
            times = 1;
            result = etxAttachment;
        }};
        attachmentService.attachmentInError(RESULT, CODE, DESCRIPTION);

        new FullVerifications() {{
            attachmentDao.update(etxAttachment);
            times = 1;
        }};
        assertThat(etxAttachment.getStateType(), is(AttachmentState.ATTS_FAILED));
        assertThat(etxAttachment.getError().getResponseCode(), is(CODE));
        assertThat(etxAttachment.getError().getDetail(), is(DESCRIPTION));
    }

    @Test
    public void findAttachmentByDomibusMessageId_ok() {
        final String domibusMessageID = UUID.randomUUID().toString();
        final EtxAttachment etxAttachment = new EtxAttachment();

        new Expectations() {{
            attachmentDao.findAttachmentByDomibusMessageId(domibusMessageID);
            times = 1;
            result = etxAttachment;
        }};
        assertThat(attachmentService.findAttachmentByDomibusMessageId(domibusMessageID), is(etxAttachment));
        new FullVerifications() {};
    }

    @Test
    public void findAttachmentByDomibusMessageId_domibusEmpty_Exception() {
        final String domibusMessageID = "  ";

        try {
            attachmentService.findAttachmentByDomibusMessageId(domibusMessageID);
            Assert.fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getError(), is(ETrustExError.DEFAULT));
        }

        new FullVerifications() {};
    }

    @Test
    public void findAttachmentByDomibusMessageId_attachmentNotFound_Exception() {
        final String domibusMessageID = UUID.randomUUID().toString();

        new Expectations() {{
            attachmentDao.findAttachmentByDomibusMessageId(domibusMessageID);
            times = 1;
            result = null;
        }};

        try {
            attachmentService.findAttachmentByDomibusMessageId(domibusMessageID);
            Assert.fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getError(), is(ETrustExError.ETX_005));
        }

        new FullVerifications() {};
    }

    @Test
    public void testCount_ok() {
        final Date date = new Date();
        new Expectations() {{
            attachmentDao.countNextAttachmentsToExpire(date);
            times = 1;
            result = RESULT;
        }};
        assertThat(attachmentService.countNextAttachmentsToExpire(date), is(RESULT));
        new FullVerifications() {};
    }

    @Test
    public void testCount_null_0L() {
        final Date date = new Date();
        new Expectations() {{
            attachmentDao.countNextAttachmentsToExpire(date);
            times = 1;
            result = null;
        }};
        assertThat(attachmentService.countNextAttachmentsToExpire(date), is(0L));
        new FullVerifications() {};
    }

    @Test
    public void testAreAllAttachmentsUploaded_HappyFlow() {

        final Long idExpected = 100L;
        final Long countExpected = 2L;

        final EtxMessage etxMessage = getEtxMessage(idExpected);

        new Expectations() {{
            attachmentDao.countMessageAttachmentsNotInStateList(idExpected,
                    Arrays.asList(AttachmentState.ATTS_UPLOADED, AttachmentState.ATTS_ALREADY_UPLOADED));
            result = countExpected;
            times = 1;
        }};

        Assert.assertFalse(attachmentService.areAllAttachmentsUploaded(etxMessage));

        new FullVerifications() {};

    }

    @Test
    public void testHasFailedAttachment_HappyFollow() {

        final Long idExpected = 200L;
        final Long countExpected = 10L;

        final EtxMessage etxMessage = getEtxMessage(idExpected);
        new Expectations() {{
            attachmentDao.countMessageAttachmentsInStateList(idExpected,
                    Collections.singletonList(AttachmentState.ATTS_FAILED));
            result = countExpected;
            times = 1;

        }};

        Assert.assertTrue(attachmentService.hasFailedAttachment(etxMessage));
        new FullVerifications() {};
    }

    @Test
    public void testAreAllAttachmentsInFinalState_HappyFlow() {
        final Long idExpected = 150L;
        final Long countExpected = 10L;

        final EtxMessage etxMessage = getEtxMessage(idExpected);

        new Expectations() {{
            attachmentDao.countMessageAttachmentsNotInStateList(idExpected,
                    Arrays.asList(AttachmentState.ATTS_FAILED, AttachmentState.ATTS_UPLOADED, AttachmentState.ATTS_ALREADY_UPLOADED, AttachmentState.ATTS_IGNORED));
            result = countExpected;
            times = 1;

        }};

        Assert.assertFalse(attachmentService.areAllAttachmentsInFinalState(etxMessage));

        new FullVerifications() {};
    }

    private EtxMessage getEtxMessage(Long idExpected) {
        final EtxMessage etxMessage = new EtxMessage();
        etxMessage.setId(idExpected);
        return etxMessage;
    }

    @Test
    public void testUpdateAttachmentState_HappyFlow() {
        final AttachmentState oldState = AttachmentState.ATTS_CREATED;
        final AttachmentState newState = AttachmentState.ATTS_DOWNLOADED;

        final EtxAttachment etxAttachment = getEtxAttachment();
        etxAttachment.setStateType(oldState);

        attachmentService.updateAttachmentState(etxAttachment, oldState, newState);

        new FullVerifications() {{
            attachmentDao.update(etxAttachment);
            times = 1;
        }};

        Assert.assertEquals(etxAttachment.getStateType(), newState);

    }

    @Test
    public void testUpdateAttachmentState_actualStateDifferentFromOldState_doNothing() {
        final AttachmentState oldState = AttachmentState.ATTS_CREATED;
        final AttachmentState newState = AttachmentState.ATTS_DOWNLOADED;

        final EtxAttachment etxAttachment = getEtxAttachment();
        etxAttachment.setStateType(AttachmentState.ATTS_ALREADY_UPLOADED);

        attachmentService.updateAttachmentState(etxAttachment, oldState, newState);
        new FullVerifications() {};
    }

    @Test
    public void testCheckAttachmentForSystemInState_HappyFlow() {

        final EtxAttachment etxAttachment = new EtxAttachment();
        final List<AttachmentState> attachmentStateList = Collections.singletonList(AttachmentState.ATTS_CREATED);

        final Long systemId = 10L;
        final Long countExpected = 200L;
        final EtxSystem senderSystem = new EtxSystem();
        senderSystem.setId(systemId);

        new Expectations() {{
            attachmentDao.countAttachmentsBySystemAndUUIDAndState(etxAttachment.getAttachmentUuid(), attachmentStateList, systemId);
            result = countExpected;
            times = 1;

        }};

        Assert.assertEquals(countExpected > 0, attachmentService.checkAttachmentForSystemInState(etxAttachment, senderSystem, attachmentStateList));

        new FullVerifications() {};
    }

    @Test
    public void testExpireAttachments_HappyFlow() {

        final EtxAttachment etxAttachment = getEtxAttachment();
        final List<EtxAttachment> etxAttachmentList = Collections.singletonList(etxAttachment);
        final Date instance = new Date();

        new Expectations() {{
            attachmentDao.getNextAttachmentsToExpire(instance, 0, 0);
            times = 1;
            result = etxAttachmentList;
        }};

        //method to test it
        attachmentService.expireAttachments(instance, 0, 0);

        new FullVerifications() {{
            clearEtxLogKeysService.clearKeys();
            times = 1;

            attachmentDao.update(etxAttachment);
            times = 1;

            etxBackendPluginWorkspaceService.deleteFile(etxAttachment.getId());
            times = 1;
        }};

        Assert.assertFalse(etxAttachment.isActiveState());
    }

    private EtxAttachment getEtxAttachment() {
        final EtxAttachment etxAttachment = NodeDocumentWrapperServiceAdapterTest.formValidEtxAttachmentForStoreDocumentWrapper();
        etxAttachment.setActiveState(true);
        etxAttachment.setId(20L);
        return etxAttachment;
    }

}
package eu.europa.ec.etrustex.adapter.integration.client.backend.ws.adapter;

import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageReferenceState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.testutils.SamplePartyBuilder;
import eu.europa.ec.etrustex.integration.model.common.v2.ErrorType;
import eu.europa.ec.etrustex.integration.service.notification.v2.*;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.apache.commons.codec.binary.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.EndpointReference;
import java.util.Map;

import static org.hamcrest.core.Is.is;

/**
 * @author François Gautier
 * @version 1.0
 * @since 16-Jan-18
 */
@RunWith(JMockit.class)
public class InboxNotificationServiceAdapterImplTest {
    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    private static final String PASSWORD = "PASSWORD";
    private static final String USERNAME = "USERNAME";
    private static final String SERVICE_POINT = "SERVICE_POINT";
    private InboxNotificationServiceAdapterImpl inboxNotificationServiceAdapter;

    @Mocked
    private BackendWebServiceProvider backendWebServiceProvider;

    @Mocked
    private InboxNotificationPortTypeImpl inboxNotificationPortType;

    private EtxMessage messageBundle;


    @Before
    public void setUp() {
        inboxNotificationServiceAdapter = new InboxNotificationServiceAdapterImpl(backendWebServiceProvider);
        inboxNotificationServiceAdapter.setPassword(PASSWORD);
        inboxNotificationServiceAdapter.setUserName(USERNAME);
        inboxNotificationServiceAdapter.setServiceEndpoint(SERVICE_POINT);

        messageBundle = getMessage();
    }

    @Test
    public void onMessageBundle_happyFlow() {
        new Expectations() {{
            backendWebServiceProvider.buildJaxWsProxy(InboxNotificationPortType.class, false, SERVICE_POINT);
            times = 1;
            result = inboxNotificationPortType;
        }};
        inboxNotificationServiceAdapter.notifyMessageBundle(messageBundle);

        onMessageBundle();
    }

    @Test
    public void onMessageBundle_FaultResponse() {
        thrown.expect(ETrustExPluginException.class);

        new Expectations() {{
            backendWebServiceProvider.buildJaxWsProxy(InboxNotificationPortType.class, false, SERVICE_POINT);
            times = 1;
            result = inboxNotificationPortType;

            inboxNotificationPortType.onMessageBundle((OnMessageBundleRequestType) any);
            times = 1;
            result = new FaultResponse("TEST", new ErrorType());
        }};

        inboxNotificationServiceAdapter.notifyMessageBundle(messageBundle);

        onMessageBundle();
    }

    @Test
    public void onMessageBundle_Exception() {
        thrown.expect(ETrustExPluginException.class);

        new Expectations() {{
            backendWebServiceProvider.buildJaxWsProxy(InboxNotificationPortType.class, false, SERVICE_POINT);
            times = 1;
            result = inboxNotificationPortType;

            inboxNotificationPortType.onMessageBundle((OnMessageBundleRequestType) any);
            times = 1;
            result = new Exception();
        }};

        inboxNotificationServiceAdapter.notifyMessageBundle(messageBundle);

        onMessageBundle();
    }

    private void onMessageBundle() {
        new FullVerifications() {{
            backendWebServiceProvider.setupConnectionCredentials(inboxNotificationPortType, USERNAME, StringUtils.getBytesUtf8(PASSWORD));
            times = 1;

            OnMessageBundleRequestType object;
            inboxNotificationPortType.onMessageBundle(object = withCapture());
            times = 1;
            Assert.assertThat(object.getMessageBundle().getSender().getId(), is(messageBundle.getSender().getPartyUuid()));
        }};
    }
    @Test
    public void onMessageStatus_happyFlow() {
        new Expectations() {{
            backendWebServiceProvider.buildJaxWsProxy(InboxNotificationPortType.class, false, SERVICE_POINT);
            times = 1;
            result = inboxNotificationPortType;
        }};
        inboxNotificationServiceAdapter.notifyMessageStatus(messageBundle, MessageType.MESSAGE_ADAPTER_STATUS);

        onMessageStatus();
    }

    @Test
    public void onMessageStatus_FaultResponse() {
        thrown.expect(ETrustExPluginException.class);

        new Expectations() {{
            backendWebServiceProvider.buildJaxWsProxy(InboxNotificationPortType.class, false, SERVICE_POINT);
            times = 1;
            result = inboxNotificationPortType;

            inboxNotificationPortType.onMessageStatus((OnMessageStatusRequestType) any);
            times = 1;
            result = new FaultResponse("TEST", new ErrorType());
        }};

        inboxNotificationServiceAdapter.notifyMessageStatus(messageBundle, MessageType.MESSAGE_ADAPTER_STATUS);

        onMessageStatus();
    }

    @Test
    public void onMessageStatus_Exception() {
        thrown.expect(ETrustExPluginException.class);

        new Expectations() {{
            backendWebServiceProvider.buildJaxWsProxy(InboxNotificationPortType.class, false, SERVICE_POINT);
            times = 1;
            result = inboxNotificationPortType;

            inboxNotificationPortType.onMessageStatus((OnMessageStatusRequestType) any);
            times = 1;
            result = new Exception();
        }};

        inboxNotificationServiceAdapter.notifyMessageStatus(messageBundle, MessageType.MESSAGE_ADAPTER_STATUS);

        onMessageStatus();
    }

    private void onMessageStatus() {
        new FullVerifications() {{
            backendWebServiceProvider.setupConnectionCredentials(inboxNotificationPortType, USERNAME, StringUtils.getBytesUtf8(PASSWORD));
            times = 1;

            OnMessageStatusRequestType object;
            inboxNotificationPortType.onMessageStatus(object = withCapture());
            times = 1;
            Assert.assertThat(object.getMessageStatus().getSender().getId(), is(messageBundle.getSender().getPartyUuid()));
        }};
    }

    private EtxMessage getMessage() {
        EtxMessage etxMessage = new EtxMessage();
        etxMessage.setSender(SamplePartyBuilder.formValid_EGREFFE_APP_PARTY_B4());
        etxMessage.setReceiver(SamplePartyBuilder.formValid_EGREFFE_NP_PARTY_B4());
        etxMessage.setMessageReferenceStateType(MessageReferenceState.AVAILABLE);
        return etxMessage;
    }

    private static class InboxNotificationPortTypeImpl implements InboxNotificationPortType, BindingProvider {
        @Override
        public OnMessageBundleResponseType onMessageBundle(OnMessageBundleRequestType onMessageBundleRequest) {
            return null;
        }

        @Override
        public OnMessageStatusResponseType onMessageStatus(OnMessageStatusRequestType onMessageStatusRequest) {
            return null;
        }

        @Override
        public Map<String, Object> getRequestContext() {
            return null;
        }

        @Override
        public Map<String, Object> getResponseContext() {
            return null;
        }

        @Override
        public Binding getBinding() {
            return null;
        }

        @Override
        public EndpointReference getEndpointReference() {
            return null;
        }

        @Override
        public <T extends EndpointReference> T getEndpointReference(Class<T> clazz) {
            return null;
        }
    }
}
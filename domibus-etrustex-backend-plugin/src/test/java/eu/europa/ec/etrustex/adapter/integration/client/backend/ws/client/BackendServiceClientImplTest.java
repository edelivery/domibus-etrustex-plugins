package eu.europa.ec.etrustex.adapter.integration.client.backend.ws.client;

import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.services.DomainContextExtService;
import eu.domibus.ext.services.PasswordEncryptionExtService;
import eu.europa.ec.etrustex.adapter.integration.client.backend.ws.adapter.BackendWebServiceProvider;
import eu.europa.ec.etrustex.adapter.integration.client.backend.ws.adapter.FileRepositoryServiceAdapterImpl;
import eu.europa.ec.etrustex.adapter.integration.client.backend.ws.adapter.InboxNotificationServiceAdapterImpl;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxUser;
import eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.model.vo.FileAttachmentVO;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * JUnit class for <code>BackendServiceClientImpl</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@SuppressWarnings("ResultOfMethodCallIgnored")
@RunWith(JMockit.class)
public class BackendServiceClientImplTest {

    private static final String USER_NAME = "name_test", password = "password_test";
    private static final String END_POINT = "http://test_id";
    private final EtxUser backendUser = new EtxUser();
    private final EtxAttachment etxAttachment = new EtxAttachment();

    @Injectable
    private BackendWebServiceProvider backendWebServiceProvider;
    @Injectable
    private DomainContextExtService domainContextExtService;
    @Injectable
    private PasswordEncryptionExtService passwordEncryptionExtService;

    @Tested
    private BackendServiceClientImpl backendServiceClient;

    private Map<SystemConfigurationProperty, String> systemConfig;
    private DomainDTO domain;

    @Before
    public void setUp() {
        //setup input parameters
        systemConfig = new HashMap<>();
        systemConfig.put(SystemConfigurationProperty.ETX_BACKEND_SERVICE_FILE_REPOSITORY_URL, END_POINT);
        systemConfig.put(SystemConfigurationProperty.ETX_BACKEND_SERVICE_INBOX_NOTIFICATION_URL, END_POINT);

        backendUser.setName(USER_NAME);
        backendUser.setPassword(password);

        domain = new DomainDTO();
    }

    /**
     * To test file download using the web service of the backend - FileRepositoryService similar to EGREFFE flow.
     */
    @Test
    public void testDownloadAttachment_UsingFileRepositoryWebService_HappyFlow(final @Mocked EtxMessage etxMessage,
                                                                               final @Mocked EtxParty etxParty,
                                                                               final @Mocked FileRepositoryServiceAdapterImpl fileRepositoryService) {

        final FileAttachmentVO fileAttachmentVOExpected = new FileAttachmentVO();
        new Expectations() {{
            new FileRepositoryServiceAdapterImpl(backendWebServiceProvider);
            result = fileRepositoryService;
            times = 1;

            etxMessage.getSender();
            result = etxParty;
            times = 1;

            etxParty.getSystem().getBackendUser();
            result = backendUser;
            times = 1;

            fileRepositoryService.downloadAttachment(etxMessage, etxAttachment);
            result = fileAttachmentVOExpected;
            times = 1;

            domainContextExtService.getCurrentDomain();
            times = 1;
            result = domain;

            passwordEncryptionExtService.decryptProperty((DomainDTO) any, anyString, anyString);
            times = 1;
            result = password;
        }};

        //tested method
        FileAttachmentVO fileAttachmentVO = backendServiceClient.downloadAttachment(systemConfig, etxMessage, etxAttachment);

        Assert.assertNotNull(fileAttachmentVO);
        Assert.assertEquals(fileAttachmentVOExpected, fileAttachmentVO);

        new FullVerifications() {{
            String s;
            fileRepositoryService.setUserName(s = withCapture());
            times = 1;
            Assert.assertEquals(USER_NAME, s);

            fileRepositoryService.setPassword(s = withCapture());
            times = 1;
            Assert.assertEquals(password, s);

            fileRepositoryService.setServiceEndpoint(END_POINT);
            times = 1;
        }};

    }

    @Test
    public void testUploadAttachment_HappyFlow(final @Mocked EtxMessage etxMessage,
                                               final @Mocked EtxParty etxParty,
                                               final @Mocked FileRepositoryServiceAdapterImpl fileRepositoryService) {

        final File file = new File("/tmp/test");

        new Expectations() {{
            new FileRepositoryServiceAdapterImpl(backendWebServiceProvider);

            etxMessage.getReceiver();
            result = etxParty;

            etxParty.getSystem().getBackendUser();
            result = backendUser;

            domainContextExtService.getCurrentDomain();
            times = 1;
            result = domain;

            passwordEncryptionExtService.decryptProperty((DomainDTO) any, anyString, anyString);
            times = 1;
            result = password;
        }};

        //tested method
        backendServiceClient.uploadAttachment(systemConfig, etxMessage, etxAttachment, file);

        new FullVerifications() {{
            String s;
            fileRepositoryService.setUserName(s = withCapture());
            Assert.assertEquals(USER_NAME, s);

            fileRepositoryService.setPassword(s = withCapture());
            Assert.assertEquals(password, s);

            EtxMessage etxMessageActual;
            EtxAttachment etxAttachmentActual;
            File fileActual;
            fileRepositoryService.uploadAttachment(etxMessageActual = withCapture(), etxAttachmentActual = withCapture(),
                    fileActual = withCapture());
            Assert.assertEquals(etxMessage, etxMessageActual);
            Assert.assertEquals(etxAttachment, etxAttachmentActual);
            Assert.assertEquals(file, fileActual);

            fileRepositoryService.setServiceEndpoint(END_POINT);
            times = 1;
        }};
    }

    @Test
    public void testNotifyMessageBundle_HappyFlow(final @Mocked EtxMessage etxMessage,
                                                  final @Mocked EtxParty etxParty,
                                                  final @Mocked InboxNotificationServiceAdapterImpl inboxNotificationService) {

        new Expectations() {{
            new InboxNotificationServiceAdapterImpl(backendWebServiceProvider);

            etxMessage.getReceiver();
            result = etxParty;

            etxParty.getSystem().getBackendUser();
            result = backendUser;

            domainContextExtService.getCurrentDomain();
            times = 1;
            result = domain;

            passwordEncryptionExtService.decryptProperty((DomainDTO) any, anyString, anyString);
            times = 1;
            result = password;
        }};

        //tested method
        backendServiceClient.notifyMessageBundle(systemConfig, etxMessage);

        new FullVerifications() {{
            String s;
            inboxNotificationService.setUserName(s = withCapture());
            Assert.assertEquals(USER_NAME, s);

            inboxNotificationService.setPassword(s = withCapture());
            Assert.assertEquals(password, s);

            inboxNotificationService.setServiceEndpoint(END_POINT);
            times = 1;

            EtxMessage etxMessageActual;
            inboxNotificationService.notifyMessageBundle(etxMessageActual = withCapture());
            Assert.assertEquals(etxMessage, etxMessageActual);
        }};
    }

    @Test
    public void testNotifyMessageStatus_HappyFlow(final @Mocked EtxMessage etxMessage,
                                                  final @Mocked EtxParty etxParty,
                                                  final @Mocked InboxNotificationServiceAdapterImpl inboxNotificationService) {

        final MessageType messageType = MessageType.MESSAGE_STATUS;

        new Expectations() {{
            new InboxNotificationServiceAdapterImpl(backendWebServiceProvider);

            etxMessage.getReceiver();
            result = etxParty;

            etxParty.getSystem().getBackendUser();
            result = backendUser;

            domainContextExtService.getCurrentDomain();
            times = 1;
            result = domain;

            passwordEncryptionExtService.decryptProperty((DomainDTO) any, anyString, anyString);
            times = 1;
            result = password;
        }};

        //tested method
        backendServiceClient.notifyMessageStatus(systemConfig, etxMessage, messageType);

        new FullVerifications() {{
            String s;
            inboxNotificationService.setUserName(s = withCapture());
            Assert.assertEquals(USER_NAME, s);

            inboxNotificationService.setPassword(s = withCapture());
            Assert.assertEquals(password, s);

            inboxNotificationService.setServiceEndpoint(END_POINT);
            times = 1;

            EtxMessage etxMessageActual;
            MessageType messageTypeActual;
            inboxNotificationService.notifyMessageStatus(etxMessageActual = withCapture(), messageTypeActual = withCapture());
            Assert.assertEquals(etxMessage, etxMessageActual);
            Assert.assertEquals(messageType, messageTypeActual);
        }};
    }

}
package eu.europa.ec.etrustex.adapter.queue.out.downloadattachment;

import eu.domibus.ext.services.DomainContextExtService;
import eu.domibus.ext.services.DomainExtService;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPluginService;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.queue.ETrustExBackendPluginJMSErrorUtils;
import eu.europa.ec.etrustex.common.jms.TextMessageConverter;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * JUnit class for <code>OutDownloadAttachmentConsumer</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@RunWith(JMockit.class)
public class OutDownloadAttachmentConsumerTest {

    @Injectable
    OutDownloadAttachmentHandler handler;

    @Injectable
    TextMessageConverter textMessageConverter;
    @Injectable
    @Qualifier("etxBackendPluginProperties")
    private ETrustExBackendPluginProperties etxBackendPluginProperties;
    @Injectable
    private DomainExtService domainExtService;
    @Injectable
    private DomainBackEndPluginService domainBackEndPluginService;
    @Injectable
    private DomainContextExtService domainContextExtService;
    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Injectable
    private ETrustExBackendPluginJMSErrorUtils eTrustExBackendPluginJMSErrorUtils;
    @Tested
    OutDownloadAttachmentConsumer outDownloadAttachmentConsumer;

    @Test
    public void testHandleMessage_HappyFlow(final @Mocked OutDownloadAttachmentQueueMessage outDownloadAttachmentQueueMessage) throws Exception {

        final Long attachmentId = 10L;
        new Expectations() {{
            outDownloadAttachmentQueueMessage.getAttachmentId();
            result = attachmentId;
        }};

        //method to test
        outDownloadAttachmentConsumer.handleMessage(outDownloadAttachmentQueueMessage);

        new Verifications() {{
            Long attachmentIdActual;
            handler.handleDownloadAttachment(attachmentIdActual = withCapture());
            Assert.assertEquals("attachmentId must match", attachmentId, attachmentIdActual);
        }};
    }

    @Test
    public void testValidateMessage_HappyFlow(final @Mocked OutDownloadAttachmentQueueMessage outDownloadAttachmentQueueMessage) throws Exception {

        final Long attachmentId = 10L;
        new Expectations() {{
            outDownloadAttachmentQueueMessage.getAttachmentId();
            result = attachmentId;
        }};

        //method to test
        outDownloadAttachmentConsumer.validateMessage(outDownloadAttachmentQueueMessage);

        new Verifications() {{
            Long attachmentIdActual;
            handler.validateDownloadAttachment(attachmentIdActual = withCapture());
            Assert.assertEquals("attachmentId must match", attachmentId, attachmentIdActual);
        }};
    }

}
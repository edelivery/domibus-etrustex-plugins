package eu.europa.ec.etrustex.adapter;

import ec.schema.xsd.commonaggregatecomponents_2.InterchangeAgreementType;
import eu.domibus.api.multitenancy.Domain;
import eu.domibus.api.multitenancy.DomainTaskExecutor;
import eu.domibus.common.JMSConstants;
import eu.domibus.ext.delegate.converter.DomainExtConverter;
import eu.domibus.ext.delegate.services.multitenancy.DomainTaskExecutorExtDelegate;
import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.domain.PasswordEncryptionResultDTO;
import eu.domibus.ext.services.*;
import eu.domibus.plugin.handler.MessagePuller;
import eu.domibus.plugin.handler.MessageRetriever;
import eu.domibus.plugin.handler.MessageSubmitter;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPluginService;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.integration.ETrustExDomibusBackendConnector;
import eu.europa.ec.etrustex.adapter.integration.client.backend.repository.LocalSharedFileRepositoryService;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.NodeInvocationManager;
import eu.europa.ec.etrustex.adapter.integration.handlers.BackendActionHandlerFactory;
import eu.europa.ec.etrustex.adapter.model.dto.IcaDTO;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.ica.EtxIca;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.AdapterConfigDao;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.AdapterConfigDaoImpl;
import eu.europa.ec.etrustex.adapter.persistence.api.attachment.AttachmentDao;
import eu.europa.ec.etrustex.adapter.persistence.api.message.MessageDao;
import eu.europa.ec.etrustex.adapter.queue.JmsProducer;
import eu.europa.ec.etrustex.adapter.queue.out.downloadattachment.OutDownloadAttachmentProducer;
import eu.europa.ec.etrustex.adapter.queue.out.downloadattachment.OutDownloadAttachmentQueueMessage;
import eu.europa.ec.etrustex.adapter.queue.out.sendmessage.SendMessageProducer;
import eu.europa.ec.etrustex.adapter.queue.out.sendmessage.SendMessageQueueMessage;
import eu.europa.ec.etrustex.adapter.service.*;
import eu.europa.ec.etrustex.adapter.testutils.h2.InMemoryDataBaseConfig;
import eu.europa.ec.etrustex.adapter.testutils.h2.StatementExecutor;
import eu.europa.ec.etrustex.common.service.CacheService;
import eu.europa.ec.etrustex.common.service.CacheServiceImpl;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.jcache.JCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.cache.Caching;
import javax.cache.spi.CachingProvider;
import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static eu.europa.ec.etrustex.adapter.queue.out.downloadattachment.OutDownloadAttachmentProducer.ETX_BACKEND_PLUGIN_OUTBOX_DOWNLOAD_ATTACHMENT_QUEUE;
import static eu.europa.ec.etrustex.adapter.queue.out.sendmessage.SendMessageProducer.ETX_BACKEND_PLUGIN_OUTBOX_SEND_MESSAGE_BUNDLE_QUEUE;

/**
 * @author François Gautier
 * @version 1.0
 * @since 05/02/2018
 * <p>
 * Extends this class to access to an InMemory Database and Spring Beans (to be defined here)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        InMemoryDataBaseConfig.class/*MySqlDataBaseConfig.class*/,
        AbstractSpringContextIT.ConfigPlugin.class,
        AbstractSpringContextIT.ConfigDomibus.class})
@WebAppConfiguration(value = "src/test/resources")
@EnableWebMvc
@EnableCaching
public abstract class AbstractSpringContextIT {

    @Configuration
    @ComponentScan(basePackages = {
            "eu.europa.ec.etrustex.adapter.service",
            "eu.europa.ec.etrustex.adapter.model",
            "eu.europa.ec.etrustex.adapter.persistence.api.administration",
            "eu.europa.ec.etrustex.adapter.persistence.api.message",
            "eu.europa.ec.etrustex.adapter.persistence.api.ica",
            "eu.europa.ec.etrustex.adapter.web",
            "eu.europa.ec.etrustex.adapter.persistence",
            "eu.europa.ec.etrustex.common.util"
    })
    static class ConfigPlugin {
        @Bean
        public StatementExecutor helper() {
            return new StatementExecutor();
        }

        @Bean
        public Properties eTrustExPluginCommonAS4Properties() {
            return new Properties();
        }

        @Bean
        public ETrustExBackendPluginProperties etxBackendPluginProperties() {
            return new ETrustExBackendPluginProperties();
        }

        @Bean
        public DomainBackEndPluginService domainBackEndPluginAspect() {
            return new DomainBackEndPluginService();
        }

        @Bean
        public Executor taskExecutor() {
            return Runnable::run;
        }

        @Bean
        public Properties buildProperties() {
            return new Properties();
        }

        @Bean
        public AdapterConfigurationService adapterConfigurationService() {
            return new AdapterConfigurationServiceImpl();
        }

        @Bean
        public ETrustExDomibusBackendConnector eTrustExDomibusBackendConnector() {
            return new ETrustExDomibusBackendConnector("TEST");
        }

        @Bean
        public UserMessageExtService userMessageExtService() {
            return Mockito.mock(UserMessageExtService.class);
        }

        @Bean
        public MessageExtService messageExtService() {
            return Mockito.mock(MessageExtService.class);
        }

        @Bean
        public BackendConnectorSubmissionService etxBackendPluginBackendConnectorSubmissionService() {
            return new BackendConnectorSubmissionService(null, null, null);
        }

        @Bean
        public MessageMonitorExtService messageMonitorExtService() {
            return Mockito.mock(MessageMonitorExtService.class);
        }

        @Bean
        public BackendActionHandlerFactory backendActionHandlerFactory() {
            return Mockito.mock(BackendActionHandlerFactory.class);
        }

        @Bean
        public NodeInvocationManager etxBackendPluginNodeInvocationManager() {
            return Mockito.mock(NodeInvocationManager.class);
        }

        @Bean
        public ClearEtxLogKeysService clearEtxLogKeysService() {
            return Mockito.mock(ClearEtxLogKeysService.class);
        }

        @Bean
        public MessagePuller messagePuller() {
            return Mockito.mock(MessagePuller.class);
        }

        @Bean
        public MessageSubmitter messageSubmitter() {
            return Mockito.mock(MessageSubmitter.class);
        }

        @Bean
        public MessageRetriever messageRetriever() {
            return Mockito.mock(MessageRetriever.class);
        }

        @Bean
        public AuthenticationExtService authenticationExtService() {
            return Mockito.mock(AuthenticationExtService.class);
        }

        @Bean
        public AdapterConfigDao adapterConfigDao() {
            return new AdapterConfigDaoImpl();
        }

        @Bean
        public BackendRuntimeManagerImpl runtimeManager() {
            return new BackendRuntimeManagerImpl();
        }

        @Bean
        public IcaService icaService() {
            return new IcaService() {
                @Override
                public List<IcaDTO> reloadIcas() {
                    EtxParty etxParty = new EtxParty();
                    etxParty.setPartyUuid("PARTY_UUID");
                    return Arrays.asList(new IcaDTO(etxParty));
                }

                @Override
                public List<EtxIca> getIcas() {
                    EtxIca etxIca = new EtxIca();
                    EtxParty etxParty = new EtxParty();
                    etxParty.setPartyUuid("PARTY_UUID");
                    etxIca.setReceiver(etxParty);
                    etxIca.setSender(etxParty);
                    return Arrays.asList(etxIca);
                }

                @Override
                public void createIca(String senderUuid, String receiverUuid, InterchangeAgreementType interchangeAgreementType) {

                }

                @Override
                public EtxIca getIca(EtxParty senderParty, EtxParty receiverParty) {
                    return new EtxIca();
                }
            };
        }

        @Bean
        public CacheService cacheService(CacheManager cacheManager) {
            return new CacheServiceImpl(cacheManager);
        }

//        @Bean
//        public EtxPartyService etxBackendPluginPartyService() {
//            return new EtxPartyServiceImpl();
//        }

        @Bean
        public DomibusConfigurationExtService domibusConfigurationExtService() {
            return new DomibusConfigurationExtService() {
                @Override
                public boolean isMultiTenantAware() {
                    return false;
                }

                @Override
                public boolean isSecuredLoginRequired() {
                    return false;
                }

                @Override
                public String getConfigLocation() {
                    return null;
                }

                @Override
                public boolean isClusterDeployment() {
                    return false;
                }
            };
        }

        @Bean
        public PasswordEncryptionExtService passwordEncryptionExtService() {
            return new PasswordEncryptionExtService() {
                @Override
                public void encryptPasswordsInFile(PluginPasswordEncryptionContext pluginPasswordEncryptionContext) {

                }

                @Override
                public boolean isValueEncrypted(String s) {
                    return false;
                }

                @Override
                public String decryptProperty(DomainDTO domainDTO, String s, String s1) {
                    return s1;
                }

                @Override
                public PasswordEncryptionResultDTO encryptProperty(DomainDTO domainDTO, String s, String s1) {
                    return null;
                }
            };
        }

        @Bean
        public WorkspaceServiceBackEndImpl etxBackendPluginWorkspaceService() {
            return new WorkspaceServiceBackEndImpl();
        }

        @Bean
        public DomainTaskExecutorExtDelegate executor() {
            return new DomainTaskExecutorExtDelegate();
        }

        @Bean
        public LocalSharedFileRepositoryService localSharedFileRepositoryService() {
            return new LocalSharedFileRepositoryService();
        }

        @Bean(JMSConstants.DOMIBUS_JMS_XACONNECTION_FACTORY)
        public ConnectionFactory domibusJMS_XAConnectionFactory() {
            return Mockito.mock(ConnectionFactory.class);
        }

        @Bean(eu.europa.ec.etrustex.adapter.service.MessageBundleService.MESSAGEBUNDLESERVICEBEANNAME)
        public MessageBundleService messageBundleService(@Qualifier("etxBackendPluginProperties") ETrustExBackendPluginProperties properties,
                                                         MessageDao messageDao,
                                                         AttachmentDao attachmentDao,
                                                         SendMessageProducer sendMessageProducer,
                                                         OutDownloadAttachmentProducer outDownloadAttachmentProducer) {
            return new MessageBundleService(properties,
                                            messageDao,
                                            attachmentDao,
                                            sendMessageProducer,
                                            outDownloadAttachmentProducer);
        }

        @Bean(ETX_BACKEND_PLUGIN_OUTBOX_DOWNLOAD_ATTACHMENT_QUEUE)
        public Queue outboxDownloadAttachment() {
            return Mockito.mock(Queue.class);
        }

        @Bean
        public JmsProducer<OutDownloadAttachmentQueueMessage> jmsProducerOutDownloadAttachmentQueueMessage(){
            return Mockito.mock(JmsProducer.class);
        }

        @Bean("etxBackendPluginOutDownloadAttachmentProducer")
        public OutDownloadAttachmentProducer outDownloadAttachmentProducer() {
            return new OutDownloadAttachmentProducer(outboxDownloadAttachment(), jmsProducerOutDownloadAttachmentQueueMessage());
        }
        @Bean(ETX_BACKEND_PLUGIN_OUTBOX_SEND_MESSAGE_BUNDLE_QUEUE)
        public Queue outboxSendMessageBundle() {
            return Mockito.mock(Queue.class);
        }

        @Bean
        public JmsProducer<SendMessageQueueMessage> jmsProducerSendMessageQueueMessage(){
            return Mockito.mock(JmsProducer.class);
        }
        @Bean("etxBackendPluginSendMessageProducer")
        public SendMessageProducer sendMessageProducer() {
            return new SendMessageProducer(outboxSendMessageBundle(), jmsProducerSendMessageQueueMessage());
        }

    }


    @Configuration
    static class ConfigDomibus {

        @Bean("cacheManager")
        public org.springframework.cache.CacheManager ehCacheManager() throws Exception {
            CachingProvider provider = Caching.getCachingProvider();
            ClassLoader classLoader = getClass().getClassLoader();

            //default cache
            final ClassPathResource classPathResource = new ClassPathResource("ehcache-test.xml");

            javax.cache.CacheManager cacheManager = provider.getCacheManager(
                    classPathResource.getURL().toURI(),
                    classLoader);

            return new JCacheCacheManager(cacheManager);
        }

        @Bean
        public DomibusPropertyExtService domibusPropertyExtService() {
            return new DomibusPropertyExtService() {
                @Override
                public String getProperty(String s) {
                    return null;
                }

                @Override
                public String getDomainProperty(DomainDTO domainDTO, String s) {
                    return null;
                }

                @Override
                public void setDomainProperty(DomainDTO domainDTO, String s, String s1) {

                }

                @Override
                public boolean containsDomainPropertyKey(DomainDTO domainDTO, String s) {
                    return false;
                }

                @Override
                public boolean containsPropertyKey(String s) {
                    return false;
                }

                @Override
                public String getDomainProperty(DomainDTO domainDTO, String s, String s1) {
                    return null;
                }

                @Override
                public String getDomainResolvedProperty(DomainDTO domainDTO, String s) {
                    return null;
                }

                @Override
                public String getResolvedProperty(String s) {
                    return null;
                }
            };
        }

        @Bean
        public DomainTaskExecutor domainTaskExecutor() {
            return new DomainTaskExecutor() {
                @Override
                public <T> T submit(Callable<T> callable) {
                    return null;
                }

                @Override
                public void submit(Runnable runnable) {

                }

                @Override
                public Future<?> submit(Runnable runnable, boolean b) {
                    return null;
                }

                @Override
                public void submit(Runnable runnable, Runnable runnable1, File file, boolean b, Long aLong, TimeUnit timeUnit) {

                }

                @Override
                public void submit(Runnable runnable, Runnable runnable1, File file) {

                }

                @Override
                public void submit(Runnable runnable, Domain domain) {

                }

                @Override
                public void submit(Runnable runnable, Domain domain, boolean b, Long aLong, TimeUnit timeUnit) {

                }

                @Override
                public void submitLongRunningTask(Runnable runnable, Runnable runnable1, Domain domain) {

                }

                @Override
                public void submitLongRunningTask(Runnable runnable, Domain domain) {

                }
            };
        }

        @Bean
        public DomainExtConverter domainExtConverter() {
            return new DomainExtConverter() {
                @Override
                public <T, U> T convert(U u, Class<T> aClass) {
                    return null;
                }

                @Override
                public <T, U> List<T> convert(List<U> list, Class<T> aClass) {
                    return null;
                }

                @Override
                public <T, U> Map<String, T> convert(Map<String, U> map, Class<T> aClass) {
                    return null;
                }
            };
        }

        @Bean
        public DomainExtService domainExtService() {
            return new DomainExtService() {
                @Override
                public DomainDTO getDomainForScheduler(String s) {
                    return new DomainDTO(s, s);
                }

                @Override
                public DomainDTO getDomain(String s) {
                    return new DomainDTO(s, s);
                }
            };
        }

        @Bean
        public DomainContextExtService domainContextExtService() {
            return new DomainContextExtService() {
                @Override
                public DomainDTO getCurrentDomain() {
                    return null;
                }

                @Override
                public DomainDTO getCurrentDomainSafely() {
                    return null;
                }

                @Override
                public void setCurrentDomain(DomainDTO domainDTO) {

                }

                @Override
                public void clearCurrentDomain() {

                }
            };
        }
    }
}

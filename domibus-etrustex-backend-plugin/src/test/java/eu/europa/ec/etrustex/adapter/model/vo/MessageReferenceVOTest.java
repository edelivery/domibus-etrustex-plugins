package eu.europa.ec.etrustex.adapter.model.vo;

import static org.junit.Assert.*;

/**
 * @author François Gautier
 * @version 1.0
 * @since 06/06/2018
 */
public class MessageReferenceVOTest extends AbstractDtoTest<MessageReferenceVO>{

    @Override
    protected MessageReferenceVO getInstance() {
        return new MessageReferenceVO("", null, "","");
    }
}
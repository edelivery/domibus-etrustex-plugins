package eu.europa.ec.etrustex.adapter.testutils.domibus;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;

/**
 * @author François Gautier
 * @version 1.0
 * @since 25-Jan-18
 */
public class ServletContextFactory implements FactoryBean<ServletContext>,
        ServletContextAware {
    @Override
    public ServletContext getObject() throws Exception {
        return null;
    }

    @Override
    public Class<?> getObjectType() {
        return null;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }

    @Override
    public void setServletContext(ServletContext servletContext) {

    }
}

package eu.europa.ec.etrustex.adapter.queue.out.sendmessage;

import eu.europa.ec.etrustex.adapter.integration.client.etxnode.NodeInvocationManager;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxError;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

/**
 * JUnit class for <code>SendMessageHandlerImpl</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@SuppressWarnings("ResultOfMethodCallIgnored")
@RunWith(JMockit.class)
public class SendMessageHandlerImplTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Injectable
    private MessageServicePlugin messageServicePlugin;

    @Tested
    private SendMessageHandlerImpl sendMessageHandler;

    @Injectable
    private NodeInvocationManager nodeInvocationManager;

    @Injectable
    private PostProcessProducer postProcessProducer;

    @Test
    public void testHandleSendBundle_HappyFlow() {
        final Long messageId = 10L;
        final EtxMessage etxMessageBundle = new EtxMessage();
        etxMessageBundle.setId(messageId);

        new Expectations() {{
            messageServicePlugin.findById(messageId);
            result = etxMessageBundle;

        }};

        //method to test
        sendMessageHandler.handleSendBundle(messageId);

        new FullVerifications() {{
            EtxMessage etxMessageActual;
            nodeInvocationManager.sendMessageBundle(etxMessageActual = withCapture());

            Assert.assertNotNull("return object should'n be null", etxMessageActual);
            Assert.assertEquals(etxMessageBundle, etxMessageActual);
        }};
    }

    @Test
    public void testHandleSendBundle_ExceptionFromNodeManagerNoFaultResponse() {
        final Long messageId = 10L;
        final EtxMessage etxMessageBundle = new EtxMessage();
        etxMessageBundle.setId(messageId);
        final ETrustExPluginException etxApplicationException =
                new ETrustExPluginException(ETrustExError.ETX_002, "test exception", new Exception());

        thrown.expect(ETrustExPluginException.class);
        thrown.expectMessage("test exception");

        new Expectations() {{

            messageServicePlugin.findById(messageId);
            result = etxMessageBundle;

            nodeInvocationManager.sendMessageBundle(etxMessageBundle);
            result = etxApplicationException;
        }};

        //method to test
        sendMessageHandler.handleSendBundle(messageId);

        new FullVerifications() {{

        }};
    }

    @Test
    public void testHandleSendStatus_HappyFlow() {
        final Long messageId = 20L;
        final EtxMessage messageStatusEntity = new EtxMessage();
        messageStatusEntity.setId(messageId);

        new Expectations() {{
            messageServicePlugin.findById(messageId);
            result = messageStatusEntity;

        }};

        //method to test
        sendMessageHandler.handleSendStatus(messageId);

        new FullVerifications() {{
            EtxMessage messageStatusActual;
            nodeInvocationManager.sendMessageStatus(messageStatusActual = withCapture());
            Assert.assertEquals(messageStatusEntity, messageStatusActual);
        }};
    }

    @Test
    public void testHandleSendStatus_UnhappyFlow() {
        final Long messageId = 20L;
        final EtxMessage messageStatusEntity = new EtxMessage();
        messageStatusEntity.setId(messageId);
        final String exceptionDescription = "test exception 2";

        thrown.expect(ETrustExPluginException.class);
        thrown.expectMessage(exceptionDescription);

        new Expectations() {{

            messageServicePlugin.findById(messageId);
            result = messageStatusEntity;

            nodeInvocationManager.sendMessageStatus(messageStatusEntity);
            result = new ETrustExPluginException(ETrustExError.ETX_002, exceptionDescription, new Exception());

        }};

        //method to test
        sendMessageHandler.handleSendStatus(messageId);

        new FullVerifications() {{
            EtxMessage messageStatusActual;
            nodeInvocationManager.sendMessageStatus(messageStatusActual = withCapture());
            Assert.assertEquals(messageStatusEntity, messageStatusActual);
        }};
    }

    @Test
    public void testValidateSendBundle_HappyFlow(final @Mocked EtxMessage etxMessageBundle) {
        final Long messageId = 20L;
        etxMessageBundle.setId(messageId);

        new Expectations() {{
            messageServicePlugin.findById(messageId);
            result = etxMessageBundle;

            etxMessageBundle.getMessageState();
            result = MessageState.MS_CREATED;

        }};

        //method to test
        sendMessageHandler.validateSendBundle(messageId);

        new FullVerifications() {{
        }};
    }

    @Test
    public void testValidateSendBundle_MessageUploadedSuccessfully(final @Mocked EtxMessage etxMessageBundle) {
        final Long messageId = 20L;
        etxMessageBundle.setId(messageId);

        new Expectations() {{
            messageServicePlugin.findById(messageId);
            result = etxMessageBundle;

            etxMessageBundle.getMessageState();
            result = MessageState.MS_UPLOADED;

        }};

        //method to test
        sendMessageHandler.validateSendBundle(messageId);
    }

    @Test
    public void testValidateSendBundle_ExceptionThrownMessageDaoResultIsNull() {
        final Long messageId = 20L;

        thrown.expect(ETrustExPluginException.class);
        thrown.expectMessage("Message bundle [ID: " + messageId + "] not found");

        new Expectations() {{
            messageServicePlugin.findById(messageId);
            result = null;

        }};

        //method to test
        sendMessageHandler.validateSendBundle(messageId);

        new FullVerifications() {{
        }};
    }

    @Test
    public void testValidateSendBundle_ExceptionThrownMessageStatusIsFailed(final @Mocked EtxMessage etxMessageBundle) {
        final Long messageId = 20L;
        etxMessageBundle.setId(messageId);

        thrown.expect(ETrustExPluginException.class);
        thrown.expectMessage("Invalid message bundle state (expected message bundle state: MS_CREATED, " +
                "skipping send message bundle (to Node) with ID: " + messageId);

        new Expectations() {{
            messageServicePlugin.findById(messageId);
            result = etxMessageBundle;

            etxMessageBundle.getMessageState();
            result = MessageState.MS_FAILED;

        }};

        //method to test
        sendMessageHandler.validateSendBundle(messageId);

        new FullVerifications() {{
        }};
    }

    @Test
    public void testValidateSendStatus_HappyFlow(final @Mocked EtxMessage etxMessageStatus) {
        final Long messageId = 20L;
        etxMessageStatus.setId(messageId);

        new Expectations() {{
            messageServicePlugin.findById(messageId);
            result = etxMessageStatus;

            etxMessageStatus.getMessageType();
            result = MessageType.MESSAGE_STATUS;

            etxMessageStatus.getMessageState();
            result = MessageState.MS_CREATED;

        }};

        //method to test
        sendMessageHandler.validateSendStatus(messageId);

        new FullVerifications() {{
        }};
    }

    @Test
    public void testValidateSendStatus_MessageProcessedSuccessfully(final @Mocked EtxMessage etxMessageStatus) {
        final Long messageId = 20L;
        etxMessageStatus.setId(messageId);

        new Expectations() {{
            messageServicePlugin.findById(messageId);
            result = etxMessageStatus;

            etxMessageStatus.getMessageType();
            times = 2;
            result = MessageType.MESSAGE_ADAPTER_STATUS;

            etxMessageStatus.getMessageState();
            result = MessageState.MS_PROCESSED;

        }};

        //method to test
        sendMessageHandler.validateSendStatus(messageId);

        new FullVerifications() {{
        }};
    }

    @Test
    public void testValidateSendStatus_ExceptionThrownMessageDaoResultIsNull() {
        final Long messageId = 20L;

        thrown.expect(ETrustExPluginException.class);
        thrown.expectMessage("Message status [ID: " + messageId + "] not found");

        new Expectations() {{
            messageServicePlugin.findById(messageId);
            result = null;

        }};

        //method to test
        sendMessageHandler.validateSendStatus(messageId);

        new FullVerifications() {{
        }};
    }

    @Test
    public void testValidateSendStatus_ExceptionThrownMessageStatusIsFailed(final @Mocked EtxMessage etxMessageStatus) {
        final Long messageId = 20L;

        thrown.expect(ETrustExPluginException.class);
        thrown.expectMessage("Invalid message status state, skipping send message status (to Node) with ID: " + messageId);

        new Expectations() {{
            messageServicePlugin.findById(messageId);
            result = etxMessageStatus;

            etxMessageStatus.getMessageType();
            result = MessageType.MESSAGE_ADAPTER_STATUS;

            etxMessageStatus.getMessageState();
            result = MessageState.MS_FAILED;

        }};

        //method to test
        sendMessageHandler.validateSendStatus(messageId);

        new FullVerifications() {{
        }};
    }

    @Test
    public void testOnError_HappyFlow(final @Mocked EtxMessage etxMessage) {
        final Long messageId = 20L;
        etxMessage.setId(messageId);
        etxMessage.setMessageUuid("message_UUID");

        new Expectations() {{
            messageServicePlugin.findById(messageId);
            result = etxMessage;
        }};

        //method to test
        sendMessageHandler.onError(messageId);

        new FullVerifications() {{
            EtxError etxErrorActual;

            etxMessage.setError(etxErrorActual = withCapture());
            Assert.assertNotNull(etxErrorActual);
            Assert.assertEquals(ETrustExError.DEFAULT.getCode(), etxErrorActual.getResponseCode());
            Assert.assertEquals("Failed to send message " + etxMessage.getMessageUuid(), etxErrorActual.getDetail());

            postProcessProducer.triggerPostProcess(messageId);
        }};
    }

}
package eu.europa.ec.etrustex.adapter.model.entity.administration;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * JUnit class for <code>AdapterConfigurationProperty</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
public class AdapterConfigurationPropertyTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testFromCode_HappyFlow() throws Exception {

        //randomly test some of the values
        Assert.assertEquals(AdapterConfigurationProperty.fromCode("etx.adapter.notification.expiration.days"),
                AdapterConfigurationProperty.ETX_ADAPTER_NOTIFICATION_EXPIRATION_DAYS);

        Assert.assertEquals(AdapterConfigurationProperty.fromCode("etx.adapter.workspace.root.path"),
                AdapterConfigurationProperty.ETX_ADAPTER_WORKSPACE_ROOT_PATH);

        Assert.assertEquals(AdapterConfigurationProperty.fromCode("etx.node.attachment.retentionPolicy.weeks"),
                AdapterConfigurationProperty.ETX_NODE_RETENTION_POLICY_WEEKS);
    }

    @Test
    public void testFromCode_UnhappyFlow() throws Exception {
        final String wrongCode = "etx.adapter.notification.expiration.days2";

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("No configuration found for the input code: " + wrongCode);

        //do the thing, call with this bad value
        AdapterConfigurationProperty.fromCode(wrongCode);

    }

    @Test
    public void testFromCode_UnhappyFlow_Null() throws Exception {
        final String wrongCode = null;

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("No configuration found for the input code: " + wrongCode);

        //do the thing, call with this bad value
        AdapterConfigurationProperty.fromCode(wrongCode);

    }

    @Test
    public void testGetCode_HappyFlow() throws Exception {

        //randomly test some of the values
        Assert.assertEquals(AdapterConfigurationProperty.ETX_NODE_RETENTION_POLICY_WEEKS.getCode(), "etx.node.attachment.retentionPolicy.weeks");
        Assert.assertEquals(AdapterConfigurationProperty.ETX_ADAPTER_WORKSPACE_ROOT_PATH.getCode(), "etx.adapter.workspace.root.path");
        Assert.assertEquals(AdapterConfigurationProperty.ETX_NODE_RETENTION_POLICY_WEEKS.getCode(), "etx.node.attachment.retentionPolicy.weeks");
    }

}
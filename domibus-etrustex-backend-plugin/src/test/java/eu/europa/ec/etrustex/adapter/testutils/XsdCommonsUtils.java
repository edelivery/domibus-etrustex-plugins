package eu.europa.ec.etrustex.adapter.testutils;

import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.DocumentReferenceType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.DocumentResponseType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.ResponseType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IssueDateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseCodeType;
import org.joda.time.DateTime;

import java.util.Collection;

/**
 * @author François Gautier
 * @version 1.0
 * @since 16/02/2018
 */
public class XsdCommonsUtils {

    private XsdCommonsUtils() {
    }

    public static IDType getIdType(String value) {
        final IDType idType = new IDType();
        idType.setValue(value);
        return idType;
    }

    public static DocumentResponseType getDocumentResponseType(String documentReferenceType, String responseType, Collection<? extends DescriptionType> descriptions) {
        final DocumentResponseType documentResponseType = new DocumentResponseType();
        documentResponseType.setDocumentReference(getDocumentReferenceType(documentReferenceType));
        documentResponseType.setResponse(getResponseType(responseType));
        documentResponseType.getResponse().getDescription().addAll(descriptions);
        return documentResponseType;
    }

    private static ResponseType getResponseType(String value) {
        ResponseType responseType = new ResponseType();
        responseType.setResponseCode(getResponseCodeType(value));
        return responseType;
    }

    private static ResponseCodeType getResponseCodeType(String value) {
        ResponseCodeType responseCode = new ResponseCodeType();
        responseCode.setValue(value);
        return responseCode;
    }

    private static DocumentReferenceType getDocumentReferenceType(String value) {
        DocumentReferenceType documentReferenceType = new DocumentReferenceType();
        documentReferenceType.setID(getIdType(value));
        return documentReferenceType;
    }
    public static IssueDateType getIssueDateType(DateTime date) {
        IssueDateType value = new IssueDateType();
        value.setValue(date);
        return value;
    }
}

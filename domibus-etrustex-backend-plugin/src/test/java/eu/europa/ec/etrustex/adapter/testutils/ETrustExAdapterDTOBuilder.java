package eu.europa.ec.etrustex.adapter.testutils;

import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * {@link ETrustExAdapterDTO} builder for JUnit tests
 *
 * @author François Gautier, Catalin Enache
 * @since 25-Sep-17
 * @version 1.0
 */
public class ETrustExAdapterDTOBuilder {

    /**
     * private constructor, all methods are static
     */
    private ETrustExAdapterDTOBuilder() {
    }


    public static ETrustExAdapterDTO buildETxAdapterDTOResponse(ContentId contentIdGeneric, String documentAsString) {
        return buildETxAdapterDTOResponseWithActionService(contentIdGeneric, documentAsString, "SubmitDocumentBundleResponse", "DocumentBundleService");
    }

    /**
     * builds a Response ETrustExAdapterDTO object
     *
     * @param contentIdGeneric content id
     * @param documentAsString payload to be transmitted
     * @param action           PMode action
     * @param service          PMode service
     * @return dto
     */
    public static ETrustExAdapterDTO buildETxAdapterDTOResponseWithActionService(ContentId contentIdGeneric, String documentAsString, String action, String service) {
        ETrustExAdapterDTO eTrustExAdapterResponseDTO = new ETrustExAdapterDTO();

        eTrustExAdapterResponseDTO.setAs4MessageId("domibus message id");
        eTrustExAdapterResponseDTO.setAs4RefToMessageId("domibus ref message id");
        eTrustExAdapterResponseDTO.setAs4ConversationId("domibus conversation id");

        eTrustExAdapterResponseDTO.setAs4FromPartyId("ETxNodeParty");
        eTrustExAdapterResponseDTO.setAs4FromPartyIdType("urn:oasis:names:tc:ebcore:partyid-type:unregistered:ETXAS4");
        eTrustExAdapterResponseDTO.setAs4FromPartyRole("http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/initiator");

        eTrustExAdapterResponseDTO.setAs4ToPartyId("EtxBackendItaly_EGREFFE-SYS");
        eTrustExAdapterResponseDTO.setAs4ToPartyIdType("urn:oasis:names:tc:ebcore:partyid-type:unregistered:ETXAS4");
        eTrustExAdapterResponseDTO.setAs4ToPartyRole("http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/responder");

        eTrustExAdapterResponseDTO.setAs4AgreementRef("OAE");
        eTrustExAdapterResponseDTO.setAs4Service(service);
        eTrustExAdapterResponseDTO.setAs4ServiceType("eDelivery");
        eTrustExAdapterResponseDTO.setAs4Action(action);

        eTrustExAdapterResponseDTO.setAs4OriginalSender("ETxNodeParty");
        eTrustExAdapterResponseDTO.setAs4FinalRecipient("TEST_EGREFFE_APP_PARTY_B4");
        eTrustExAdapterResponseDTO.setEtxBackendMessageId("200");

        if(StringUtils.isNotBlank(documentAsString)) {
            PayloadDTO payloadDTO = TransformerUtils.getGenericPayloadDTO(documentAsString);
            payloadDTO.setContentId(contentIdGeneric.getValue());
            eTrustExAdapterResponseDTO.addAs4Payload(payloadDTO);
        }
        return eTrustExAdapterResponseDTO;
    }


}

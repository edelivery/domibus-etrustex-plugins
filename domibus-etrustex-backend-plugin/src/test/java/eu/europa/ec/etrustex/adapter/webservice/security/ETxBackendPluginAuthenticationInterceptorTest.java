package eu.europa.ec.etrustex.adapter.webservice.security;

import eu.domibus.ext.exceptions.AuthenticationExtException;
import eu.domibus.ext.exceptions.DomibusErrorCode;
import eu.domibus.ext.services.AuthenticationExtService;
import eu.europa.ec.etrustex.adapter.service.security.IdentityManager;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Arun Raj
 * @version 1.0
 */
@RunWith(JMockit.class)
public class ETxBackendPluginAuthenticationInterceptorTest {

    @Injectable
    AuthenticationExtService authenticationExtService;
    @Injectable
    IdentityManager identityManager;

    @Injectable
    Message message;

    @Injectable
    HttpServletRequest httpServletRequest;

    @Tested
    ETxBackendPluginAuthenticationInterceptor eTxBackendPluginAuthenticationInterceptor;

    @Injectable
    HttpServletResponse httpServletResponse;

    @Test
    public void testHandleMessage_HappyFlow() {

        new Expectations() {{
            message.get("HTTP.REQUEST");
            result = httpServletRequest;

            httpServletRequest.getPathInfo();
            result = "/e-trustex/adapter/integration/services/outbox/OutboxService/v2.0";

        }};

        eTxBackendPluginAuthenticationInterceptor.handleMessage(message);

        new FullVerifications() {{
            authenticationExtService.authenticate((HttpServletRequest) any);
            times = 1;
        }};
    }

    @Test
    public void testHandleMessage_AuthenticationFailureFlow() throws IOException {
        new Expectations() {{
            message.get("HTTP.REQUEST");
            result = httpServletRequest;

            message.get("HTTP.RESPONSE");
            result = httpServletResponse;

            httpServletRequest.getPathInfo();
            result = "/e-trustex/adapter/integration/services/outbox/OutboxService/v2.0";

            authenticationExtService.authenticate((HttpServletRequest) any);
            result = new AuthenticationExtException(DomibusErrorCode.DOM_001, "Mismatch between UserName and Password");

        }};

        try {
            eTxBackendPluginAuthenticationInterceptor.handleMessage(message);
            Assert.fail("Expected CXF Fault in this test case");
        } catch (Fault cxffault) {
            Assert.assertEquals(HttpServletResponse.SC_UNAUTHORIZED, cxffault.getStatusCode());
            AuthenticationExtException eTrustExPluginException = (AuthenticationExtException) cxffault.getCause();
            Assert.assertEquals(DomibusErrorCode.DOM_001.getErrorCode(), eTrustExPluginException.getErrorCode().getErrorCode());
        }

        new FullVerifications() {{
            httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            times = 1;
        }};

    }
}

package eu.europa.ec.etrustex.adapter.testutils.h2;

import org.h2.tools.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType.H2;

/**
 * @author François Gautier
 * @version 1.0
 * @since 30/01/2018
 */
@EnableTransactionManagement
public class InMemoryDataBaseConfig extends AbstractDatabaseConfig {

    /**
     *
     * @return {@link DataSource} with autoCommit = false
     */
    @Bean
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(H2)
                .build();
    }

    /**
     * Stop the process of the unit test with a break point on the thread to access the webserver
     */
    @Bean(initMethod = "start", destroyMethod = "stop")
    public Server h2WebServer() throws SQLException {
        return Server.createWebServer("-web", "-webAllowOthers", "-webPort", "8182");
    }

    @Bean(initMethod = "start", destroyMethod = "stop")
    public Server h2TcpServer() throws SQLException {
        return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-webPort", "9182");
    }

    Map<Object, String> getProperties() {
        HashMap<Object, String> objectStringHashMap = new HashMap<>();
        objectStringHashMap.put("hibernate.show_sql", "false");
        objectStringHashMap.put("hibernate.id.new_generator_mappings", "true");
        return objectStringHashMap;
    }
}

package eu.europa.ec.etrustex.adapter.testutils.domibus;

import eu.domibus.ext.domain.UserMessageDTO;
import eu.domibus.ext.exceptions.UserMessageExtException;
import eu.domibus.ext.services.UserMessageExtService;
import org.springframework.stereotype.Service;

/**
 * @author François Gautier
 * @version 1.0
 * @since 25-Jan-18
 */
@Service("etxBackendPluginUserMessageService")
public class UserMessageServiceImpl implements UserMessageExtService {
    @Override
    public UserMessageDTO getMessage(String messageId) throws UserMessageExtException {
        return new UserMessageDTO();
    }
}

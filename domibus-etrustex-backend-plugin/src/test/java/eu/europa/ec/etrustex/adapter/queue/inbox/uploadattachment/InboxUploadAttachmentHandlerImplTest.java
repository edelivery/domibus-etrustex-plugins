package eu.europa.ec.etrustex.adapter.queue.inbox.uploadattachment;

import eu.europa.ec.etrustex.adapter.integration.client.backend.repository.FileRepository;
import eu.europa.ec.etrustex.adapter.integration.client.backend.repository.FileRepositoryProvider;
import eu.europa.ec.etrustex.adapter.model.entity.administration.*;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.queue.postupload.PostUploadProducer;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.entity.attachment.AttachmentType;
import eu.europa.ec.etrustex.common.service.WorkspaceService;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.hamcrest.CoreMatchers;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.jms.JMSException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author Francois GAUTIER
 * @version 1.0
 * @since Oct 2017
 */
@RunWith(JMockit.class)
public class InboxUploadAttachmentHandlerImplTest {

    @Injectable
    private AttachmentService attachmentService;

    @Injectable
    private FileRepositoryProvider fileRepositoryProvider;

    @Injectable
    private PostProcessProducer postProcessProducer;

    @Injectable
    private PostUploadProducer postUploadProducer;

    @Injectable
    private WorkspaceService etxBackendPluginWorkspaceService;

    @Injectable
    private FileRepository fileRepository;

    @Injectable
    private Path filePath;

    @Tested
    private final InboxUploadAttachmentHandlerImpl handler = new InboxUploadAttachmentHandlerImpl();

    @Test
    public void testHandleUploadAttachmentSuccessfulInboxUploadShouldTriggerPostUpload() throws IOException {
        final Long attachmentId = 1L;
        final EtxAttachment attachmentEntity = createAttachment(AttachmentState.ATTS_FAILED);
        attachmentEntity.setMessage(createMessage(null, MessageState.MS_CREATED));
        attachmentEntity.getMessage().setReceiver(createReceiverParty());

        new Expectations() {{
            attachmentService.findById(attachmentId);
            times = 1;
            result = attachmentEntity;

            etxBackendPluginWorkspaceService.getFile(attachmentId);
            times = 1;
            result = filePath;

            fileRepositoryProvider.getFileRepository(withAny(BackendFileRepositoryLocationType.values()[0]));
            times = 1;
            result = fileRepository;
        }};

        handler.handleUploadAttachment(attachmentId);

        new FullVerifications() {{
            fileRepository.writeFile((Map<SystemConfigurationProperty, String>) any, attachmentEntity, filePath);
            attachmentService.updateAttachmentState(attachmentEntity, AttachmentState.ATTS_DOWNLOADED, AttachmentState.ATTS_UPLOADED);
            postUploadProducer.checkAttachmentsAndTriggerPostUpload(attachmentEntity);
        }};
    }

    @Test
    public void testValidateUploadAttachmentShouldThrowExceptionWhenRecordNotFound() {
        final Long attachmentId = 1L;

        new Expectations() {{
            attachmentService.findById(attachmentId);
            times = 1;
            result = null;
        }};

        try {
            handler.validateUploadAttachment(attachmentId);
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getError(), CoreMatchers.is(ETrustExError.DEFAULT));
            assertThat(e.getMessage(), CoreMatchers.containsString("Attachment [ID: " + attachmentId + "] not found"));
        }
    }

    @Test
    public void testValidateUploadAttachmentShouldThrowExceptionWhenAttachmentStateIsInvalid() {
        final Long attachmentId = 1L;
        final EtxAttachment attachmentEntity = createAttachment(AttachmentState.ATTS_FAILED);
        attachmentEntity.setMessage(createMessage(null, MessageState.MS_CREATED));

        new Expectations() {{
            attachmentService.findById(attachmentId);
            times = 1;
            result = attachmentEntity;
        }};

        try {
            handler.validateUploadAttachment(attachmentId);
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getError(), CoreMatchers.is(ETrustExError.ETX_009));
            assertThat(e.getMessage(), CoreMatchers.containsString("Invalid message and attachment state"));
        }
    }

    @Test
    public void testValidateUploadAttachmentShouldThrowExceptionWhenMessageStateIsInvalid() {
        final Long attachmentId = 1L;
        final EtxAttachment attachmentEntity = createAttachment(AttachmentState.ATTS_FAILED);
        attachmentEntity.setMessage(createMessage(null, MessageState.MS_FAILED));

        new Expectations() {{
            attachmentService.findById(attachmentId);
            times = 1;
            result = attachmentEntity;
        }};
        try {
            handler.validateUploadAttachment(attachmentId);
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getError(), CoreMatchers.is(ETrustExError.ETX_009));
            assertThat(e.getMessage(), CoreMatchers.containsString("Invalid message and attachment state"));
        }
    }

    @Test
    public void testValidateUploadAttachmentShouldThrowNoExceptionWhenMessageIsValid() {
        final Long attachmentId = 1L;
        final EtxAttachment attachmentEntity = createAttachment(AttachmentState.ATTS_DOWNLOADED);
        attachmentEntity.setMessage(createMessage(null, MessageState.MS_CREATED));

        new Expectations() {{
            attachmentService.findById(attachmentId);
            times = 1;
            result = attachmentEntity;
        }};

        handler.validateUploadAttachment(attachmentId);

        new FullVerifications() {{
        }};
    }

    @Test
    public void testOnErrorShouldSetAttStateToFailed() throws JMSException {
        final Long attachmentId = 4L;
        final Long messageId = 34L;
        final EtxMessage messageEntity = createMessage(messageId, MessageState.MS_CREATED);
        final EtxAttachment attachmentEntity = createAttachment(AttachmentState.ATTS_CREATED);
        attachmentEntity.setMessage(messageEntity);

        new Expectations() {{
            attachmentService.findById(attachmentId);
            times = 1;
            result = attachmentEntity;
        }};

        handler.onError(attachmentId);

        new FullVerifications() {{
            postProcessProducer.triggerPostProcess(messageId, attachmentId);
            times = 1;
        }};

        Assert.assertThat(attachmentEntity.getStateType(), Is.is(CoreMatchers.equalTo(AttachmentState.ATTS_FAILED)));
    }

    @Test
    public void testOnErrorShouldTriggerPostProcess() throws JMSException {
        final Long attachmentId = 4L;
        final Long messageId = 34L;
        final EtxMessage messageEntity = createMessage(messageId, MessageState.MS_CREATED);
        final EtxAttachment attachmentEntity = createAttachment(AttachmentState.ATTS_CREATED);
        attachmentEntity.setMessage(messageEntity);

        new Expectations() {{
            attachmentService.findById(attachmentId);
            times = 1;
            result = attachmentEntity;
        }};

        handler.onError(attachmentId);

        new FullVerifications() {{
            postProcessProducer.triggerPostProcess(messageId, attachmentId);
            times = 1;
        }};
    }

    public static EtxAttachment createAttachment(AttachmentState state) {
        EtxAttachment att = new EtxAttachment();
        att.setId(1L);
        att.setAttachmentUuid("ATT_UUID");
        att.setAttachmentType(AttachmentType.BINARY);
        att.setStateType(state);
        att.setDirectionType(null);
        return att;
    }

    public static EtxMessage createMessage(Long id, MessageState state) {
        EtxMessage m = new EtxMessage();
        m.setId(id);
        m.setMessageType(null);
        m.setMessageUuid(null);
        m.setMessageState(state);
        m.setDirectionType(null);
        m.setReceiver(createReceiverParty());
        return m;
    }

    public static EtxParty createReceiverParty() {
        EtxParty receiverParty = new EtxParty();
        receiverParty.setId(2L);
        receiverParty.setPartyUuid("RECEIVER_PARTY");
        receiverParty.setSystem(createSystem());
        return receiverParty;

    }

    public static EtxSystem createSystem() {
        EtxSystem etxSystem = new EtxSystem();
        etxSystem.setId(1L);
        etxSystem.setName("SYSTEM_NAME");
        etxSystem.setLocalUsername("SYS_USER_NAME");
        etxSystem.setBackendUser(createBackendUser());
        etxSystem.setSystemConfigList(create_WS_SystemConfigList(etxSystem));
        return etxSystem;
    }

    public static EtxUser createBackendUser() {
        EtxUser etxUser = new EtxUser();
        etxUser.setId(1L);
        etxUser.setName("USERNAME");
        etxUser.setPassword("PASSWORD");
        return etxUser;
    }

    public static List<EtxSystemConfig> create_WS_SystemConfigList(EtxSystem etxSystem) {
        EtxSystemConfig etxSystemConfig1 = new EtxSystemConfig();
        etxSystemConfig1.setId(1L);
        etxSystemConfig1.setSystemConfigurationProperty(SystemConfigurationProperty.ETX_BACKEND_FILE_REPOSITORY_LOCATION);
        etxSystemConfig1.setPropertyValue(BackendFileRepositoryLocationType.BACKEND_FILE_REPOSITORY_SERVICE.name());

        EtxSystemConfig etxSystemConfig2 = new EtxSystemConfig();
        etxSystemConfig2.setId(2L);
        etxSystemConfig2.setSystemConfigurationProperty(SystemConfigurationProperty.ETX_BACKEND_NOTIFICATION_TYPE);
        etxSystemConfig2.setPropertyValue(BackendNotificationType.WEB_SERVICE.name());

        EtxSystemConfig etxSystemConfig3 = new EtxSystemConfig();
        etxSystemConfig3.setId(3L);
        etxSystemConfig3.setSystemConfigurationProperty(SystemConfigurationProperty.ETX_BACKEND_SERVICE_FILE_REPOSITORY_URL);
        etxSystemConfig3.setPropertyValue("http://FRS_URL");

        EtxSystemConfig etxSystemConfig4 = new EtxSystemConfig();
        etxSystemConfig4.setId(4L);
        etxSystemConfig4.setSystemConfigurationProperty(SystemConfigurationProperty.ETX_BACKEND_SERVICE_INBOX_NOTIFICATION_URL);
        etxSystemConfig4.setPropertyValue("http://NOTIFY_URL");

        List<EtxSystemConfig> etxSystemConfigList = new ArrayList<>();
        etxSystemConfigList.add(etxSystemConfig1);
        etxSystemConfigList.add(etxSystemConfig2);
        etxSystemConfigList.add(etxSystemConfig3);
        etxSystemConfigList.add(etxSystemConfig4);
        return etxSystemConfigList;
    }
}

package eu.europa.ec.etrustex.adapter.queue.notify;

import eu.europa.ec.etrustex.adapter.integration.client.backend.BackendServiceClient;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.NodeInvocationManager;
import eu.europa.ec.etrustex.adapter.model.entity.administration.*;
import eu.europa.ec.etrustex.adapter.model.entity.message.*;
import eu.europa.ec.etrustex.adapter.service.AdapterConfigurationService;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.apache.commons.lang3.RandomUtils;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

/**
 * @author François Gautier
 * @version 1.0
 * @since 08-Jan-18
 */
@SuppressWarnings("Duplicates")
@RunWith(JMockit.class)
public class NotifyHandlerImplTest {

    @Injectable
    private MessageServicePlugin messageService;

    @Injectable
    private AdapterConfigurationService adapterConfigurationService;

    @Injectable
    private NodeInvocationManager nodeServiceClient;

    @Injectable
    private BackendServiceClient etxBackendPluginServiceWithWebService;

    @Injectable
    private BackendServiceClient etxBackendPluginWitnessFileServiceClient;
    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Tested
    private NotifyHandlerImpl notifyHandler;

    private long id;
    private EtxMessage etxMessage;
    private Map<AdapterConfigurationProperty, String> config;
    private ZonedDateTime eightDaysFromNow;
    private ZonedDateTime twoDaysFromNow;

    @Before
    public void recordExpectationsForPostConstruct() {
        new MockUp<NotifyHandlerImpl>() {
            @Mock
            void init() {
            }
        };
        id = RandomUtils.nextLong(0, 99999);
        etxMessage = new EtxMessage();
        etxMessage.setNotificationState(NotificationState.NF_AUTO_PENDING);
        etxMessage.setId(id);
        eightDaysFromNow = ZonedDateTime.now().minusDays(8);
        twoDaysFromNow = ZonedDateTime.now().minusDays(2);

        config = new HashMap<>();
    }

    @Test
    public void onError_defaultTTL7days_expired() {
        etxMessage.setCreationDate(GregorianCalendar.from(eightDaysFromNow));
        new Expectations() {{
            messageService.findById(id);
            result = etxMessage;
            times = 1;

            adapterConfigurationService.getAdapterConfigurations();
            result = config;
            times = 1;
        }};
        notifyHandler.onError(id);
        new FullVerifications() {{
            EtxMessage messageEntity;
            messageService.updateMessage(messageEntity = withCapture());
            times = 1;

            Assert.assertThat(messageEntity.getNotificationState(), Is.is(NotificationState.NF_EXPIRED));
        }};
    }

    @Test
    public void onError_defaultTTL7days_NotExpired() {
        etxMessage.setCreationDate(GregorianCalendar.from(twoDaysFromNow));

        new Expectations() {{
            messageService.findById(id);
            result = etxMessage;
            times = 1;

            adapterConfigurationService.getAdapterConfigurations();
            result = config;
            times = 1;
        }};

        notifyHandler.onError(id);

        new FullVerifications() {{
        }};
    }

    @Test
    public void onError_TTL3days_expired() {
        etxMessage.setCreationDate(GregorianCalendar.from(eightDaysFromNow));
        config.put(AdapterConfigurationProperty.ETX_ADAPTER_NOTIFICATION_EXPIRATION_DAYS, "3");
        new Expectations() {{
            messageService.findById(id);
            result = etxMessage;
            times = 1;

            adapterConfigurationService.getAdapterConfigurations();
            result = config;
            times = 1;
        }};
        notifyHandler.onError(id);
        new FullVerifications() {{
            EtxMessage messageEntity;
            messageService.updateMessage(messageEntity = withCapture());
            times = 1;

            Assert.assertThat(messageEntity.getNotificationState(), Is.is(NotificationState.NF_EXPIRED));
        }};
    }

    @Test
    public void onError_TTL3days_NotExpired() {
        etxMessage.setCreationDate(GregorianCalendar.from(twoDaysFromNow));
        config.put(AdapterConfigurationProperty.ETX_ADAPTER_NOTIFICATION_EXPIRATION_DAYS, "3");

        new Expectations() {{
            messageService.findById(id);
            result = etxMessage;
            times = 1;

            adapterConfigurationService.getAdapterConfigurations();
            result = config;
            times = 1;
        }};

        notifyHandler.onError(id);

        new FullVerifications() {{
        }};
    }

    @Test
    public void handleNotify_NotNotified() {
        etxMessage.setNotificationState(NotificationState.NF_NOTIFIED);

        new Expectations() {{
            messageService.findById(id);
            result = etxMessage;
            times = 1;

        }};

        notifyHandler.handleNotify(id);

        new FullVerifications() {{


        }};
    }

    @Test
    public void handleNotify_NotifiedOUT() {
        etxMessage.setNotificationState(NotificationState.NF_AUTO_PENDING);
        etxMessage.setDirectionType(MessageDirection.OUT);

        new Expectations() {{
            messageService.findById(id);
            result = etxMessage;
            times = 1;

        }};
        notifyHandler.handleNotify(id);

        new FullVerifications() {{
            nodeServiceClient.sendMessageStatus(etxMessage);
            times = 1;
        }};
    }

    private EtxParty getParty(String notificationType) {
        EtxParty etxParty = new EtxParty();
        EtxSystem system = new EtxSystem();
        EtxSystemConfig etxSystemConfig = new EtxSystemConfig();
        etxSystemConfig.setPropertyValue(notificationType);
        etxSystemConfig.setSystemConfigurationProperty(SystemConfigurationProperty.ETX_BACKEND_NOTIFICATION_TYPE);
        system.setSystemConfigList(Collections.singletonList(etxSystemConfig));
        etxParty.setSystem(system);
        return etxParty;
    }

    @Test
    public void handleNotify_NotifiedIN_WebService_noMessageType_nothing() {
        etxMessage.setNotificationState(NotificationState.NF_AUTO_PENDING);
        etxMessage.setDirectionType(MessageDirection.IN);

        etxMessage.setReceiver(getParty(BackendNotificationType.WEB_SERVICE.name()));

        new Expectations() {{
            messageService.findById(id);
            result = etxMessage;
            times = 1;

        }};
        notifyHandler.handleNotify(id);

        new FullVerifications() {{

        }};
    }

    @Test
    public void handleNotify_NotifiedIN_Error() {
        config.put(AdapterConfigurationProperty.ETX_ADAPTER_NOTIFICATION_EXPIRATION_DAYS, "3");

        etxMessage.setNotificationState(NotificationState.NF_AUTO_PENDING);
        etxMessage.setDirectionType(MessageDirection.IN);
        etxMessage.setMessageType(MessageType.MESSAGE_BUNDLE);
        etxMessage.setReceiver(getParty(BackendNotificationType.WEB_SERVICE.name()));
        etxMessage.setCreationDate(GregorianCalendar.from(eightDaysFromNow));

        final Map<AdapterConfigurationProperty, String> adapterConfiguration = new HashMap<>();
        adapterConfiguration.put(AdapterConfigurationProperty.ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS, String.valueOf(1));

        new Expectations() {{
            messageService.findById(id);
            result = etxMessage;
            times = 1;

            etxBackendPluginServiceWithWebService.notifyMessageBundle(withAny(new HashMap<>()), etxMessage);
            result = new Exception("ERROR");
            times = 1;

            adapterConfigurationService.getAdapterConfigurations();
            result = config;
            times = 1;
        }};

        notifyHandler.handleNotify(id);

        new FullVerifications() {{
            EtxMessage messageEntity;
            messageService.updateMessage(messageEntity = withCapture());
            times = 1;

            Assert.assertThat(messageEntity.getNotificationState(), Is.is(NotificationState.NF_EXPIRED));
        }};
    }

    @Test
    public void handleNotify_NotifiedIN_WebService_messageBundle() {
        etxMessage.setNotificationState(NotificationState.NF_AUTO_PENDING);
        etxMessage.setDirectionType(MessageDirection.IN);
        etxMessage.setMessageType(MessageType.MESSAGE_BUNDLE);
        etxMessage.setReceiver(getParty(BackendNotificationType.WEB_SERVICE.name()));

        new Expectations() {{
            messageService.findById(id);
            result = etxMessage;
            times = 1;

        }};
        notifyHandler.handleNotify(id);

        new FullVerifications() {{
            etxBackendPluginServiceWithWebService.notifyMessageBundle(withAny(new HashMap<>()), etxMessage);
            times = 1;
        }};
    }

    @Test
    public void handleNotify_NotifiedIN_witnessFile_messageBundle() {
        etxMessage.setNotificationState(NotificationState.NF_AUTO_PENDING);
        etxMessage.setDirectionType(MessageDirection.IN);
        etxMessage.setMessageType(MessageType.MESSAGE_BUNDLE);
        etxMessage.setReceiver(getParty(BackendNotificationType.WITNESS_FILE.name()));

        new Expectations() {{
            messageService.findById(id);
            result = etxMessage;
            times = 1;

        }};
        notifyHandler.handleNotify(id);

        new FullVerifications() {{
            etxBackendPluginWitnessFileServiceClient.notifyMessageBundle(withAny(new HashMap<>()), etxMessage);
            times = 1;
        }};
    }

    @Test
    public void handleNotify_NotifiedIN_WebService_messageStatus() {
        etxMessage.setNotificationState(NotificationState.NF_AUTO_PENDING);
        etxMessage.setDirectionType(MessageDirection.IN);
        etxMessage.setMessageType(MessageType.MESSAGE_STATUS);
        etxMessage.setReceiver(getParty(BackendNotificationType.WEB_SERVICE.name()));

        new Expectations() {{
            messageService.findById(id);
            result = etxMessage;
            times = 1;

            messageService.referenceMessageTypeFor(etxMessage);
            result = MessageType.MESSAGE_STATUS;
            times = 1;
        }};
        notifyHandler.handleNotify(id);

        new FullVerifications() {{
            etxBackendPluginServiceWithWebService.notifyMessageStatus(withAny(new HashMap<>()), etxMessage, MessageType.MESSAGE_STATUS);
            times = 1;
        }};
    }
    @Test
    public void handleNotify_NotifiedIN_WebService_messageAdapter() {
        etxMessage.setNotificationState(NotificationState.NF_AUTO_PENDING);
        etxMessage.setDirectionType(MessageDirection.IN);
        etxMessage.setMessageType(MessageType.MESSAGE_ADAPTER_STATUS);
        etxMessage.setReceiver(getParty(BackendNotificationType.WEB_SERVICE.name()));

        new Expectations() {{
            messageService.findById(id);
            result = etxMessage;
            times = 1;

            messageService.referenceMessageTypeFor(etxMessage);
            result = MessageType.MESSAGE_ADAPTER_STATUS;
            times = 1;
        }};
        notifyHandler.handleNotify(id);

        new FullVerifications() {{
            etxBackendPluginServiceWithWebService.notifyMessageStatus(withAny(new HashMap<>()), etxMessage, MessageType.MESSAGE_ADAPTER_STATUS);
            times = 1;
        }};
    }

    @Test
    public void validateNotify_noMessage_exception() {
        final String exceptionDescription = "Message [ID: " + id + "] not found";

        thrown.expect(ETrustExPluginException.class);
        thrown.expectMessage(exceptionDescription);

        new Expectations() {{
            messageService.findById(id);
            result = null;
            times = 1;

        }};
        notifyHandler.validateNotify(id);
        new FullVerifications() {{
        }};
    }

    @Test
    public void validateNotify_MessageAlreadyProcessed_doNothing() {
        etxMessage.setMessageState(MessageState.MS_PROCESSED);
        new Expectations() {{
            messageService.findById(id);
            result = etxMessage;
            times = 1;
        }};

        notifyHandler.validateNotify(id);

        new FullVerifications() {{
        }};
    }

    @Test
    public void validateNotify_notProcessed_exception() {
        final String exceptionDescription = "Invalid message state (expected: MS_PROCESSED [2]), skipping processing of message ID: " + id + ", state: " + etxMessage.getMessageState();

        thrown.expect(ETrustExPluginException.class);
        thrown.expectMessage(exceptionDescription);

        new Expectations() {{
            messageService.findById(id);
            result = etxMessage;
            times = 1;

        }};
        notifyHandler.validateNotify(id);
        new FullVerifications() {{
        }};
    }
}
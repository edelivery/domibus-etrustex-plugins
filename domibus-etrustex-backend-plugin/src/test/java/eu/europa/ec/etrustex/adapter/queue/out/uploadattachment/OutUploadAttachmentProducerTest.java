package eu.europa.ec.etrustex.adapter.queue.out.uploadattachment;

import eu.europa.ec.etrustex.adapter.queue.JmsProducer;
import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.jms.Queue;

/**
 * JUnit class for <code>OutUploadAttachmentProducer</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@RunWith(JMockit.class)
public class OutUploadAttachmentProducerTest {

    @Injectable
    Queue outboxUploadAttachment;

    @Injectable
    JmsProducer<OutUploadAttachmentQueueMessage> jmsProducer;

    @Tested
    OutUploadAttachmentProducer outUploadAttachmentProducer;

    @Test
    public void testTriggerUpload_HappyFlow(final @Mocked OutUploadAttachmentQueueMessage queueMessage) throws Exception {

        final Long attachmentId = 10L;

        //method to be tested
        outUploadAttachmentProducer.triggerUpload(attachmentId);

        new Verifications() {{
            Long attachmentIdActual;
            queueMessage.setAttachmentId(attachmentIdActual = withCapture());
            times = 1;
            Assert.assertEquals("attachment id should be de the same", attachmentId, attachmentIdActual);

            OutUploadAttachmentQueueMessage queueMessageActual;

            jmsProducer.postMessage(outboxUploadAttachment, queueMessageActual = withCapture());
            times = 1;
            Assert.assertNotNull("captured object shouldn't be null", queueMessageActual);
        }};
    }

}
package eu.europa.ec.etrustex.adapter.persistence.api.administration;

import eu.europa.ec.etrustex.adapter.model.entity.administration.AdapterConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxAdapterConfig;
import eu.europa.ec.etrustex.adapter.persistence.api.AbstractDaoIT;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBase;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 29-Jan-18
 */
public class AdapterConfigIT extends AbstractDaoIT<EtxAdapterConfig>{

    @Autowired
    private AdapterConfigDao adapterConfigDao;

    @Test
    public void findByPartyUUID_ok() {
        List<EtxAdapterConfig> adapterConfigs = adapterConfigDao.findAdapterConfigs();
        assertThat(adapterConfigs.size(), is(5));
    }

    @Override
    protected Long getExistingId() {
        return 1L;
    }

    @Override
    protected DaoBase<EtxAdapterConfig> getDao() {
        return adapterConfigDao;
    }

    @Override
    protected EtxAdapterConfig getNewEntity() {
        EtxAdapterConfig etxAdapterConfig = new EtxAdapterConfig();
        etxAdapterConfig.setPropertyName(AdapterConfigurationProperty.ETX_ADAPTER_REPOSITORY_TTL_IN_DAYS);
        etxAdapterConfig.setPropertyValue(UUID.randomUUID().toString());
        return etxAdapterConfig;
    }
}
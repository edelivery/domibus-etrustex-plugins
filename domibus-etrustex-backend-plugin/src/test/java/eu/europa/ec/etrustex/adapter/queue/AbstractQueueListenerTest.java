package eu.europa.ec.etrustex.adapter.queue;

import eu.domibus.ext.services.DomainContextExtService;
import eu.domibus.ext.services.DomainExtService;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPluginService;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.queue.notify.NotifyQueueMessage;
import eu.europa.ec.etrustex.common.jms.TextMessageConverter;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.jms.Message;
import javax.jms.TextMessage;


/**
 * @author François Gautier
 * @project ETX
 * @Version 1.0
 */
@RunWith(JMockit.class)
public class AbstractQueueListenerTest {

    @Injectable
    private TextMessageConverter textMessageConverter;

    @Injectable
    @Qualifier("etxBackendPluginProperties")
    private ETrustExBackendPluginProperties etxBackendPluginProperties;

    @Injectable
    private DomainExtService domainExtService;

    @Injectable
    private DomainBackEndPluginService domainBackEndPluginService;

    @Injectable
    private DomainContextExtService domainContextExtService;

    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;

    @Injectable
    private ETrustExBackendPluginJMSErrorUtils eTrustExBackendPluginJMSErrorUtils;

    @Tested
    private AbstractQueueListener<NotifyQueueMessage> queueListenerTest;

    @Test
    public void notTextMessage(@Mocked Message message) throws Exception {
        try {
            queueListenerTest.onMessage(message);
        } catch (Exception e) {
            if (!(e.getCause() instanceof IllegalArgumentException)) {
                Assert.fail("new IllegalArgumentException(Unexpected message type for {}) is expected");
            }
        }
    }

    @Test
    public void happyFlow(@Mocked final TextMessage message) throws Exception {
        new Expectations() {{
            textMessageConverter.fromMessage(message);
            times = 1;
            result = new NotifyQueueMessage();
        }};

        queueListenerTest.onMessage(message);

        new Verifications(){{
            queueListenerTest.handleMessage(((NotifyQueueMessage) any));
            times = 1;
            queueListenerTest.validateMessage(((NotifyQueueMessage) any));
            times = 1;
        }};
    }
}
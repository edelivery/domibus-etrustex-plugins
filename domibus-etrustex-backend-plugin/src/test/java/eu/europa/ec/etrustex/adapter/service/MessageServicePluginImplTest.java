package eu.europa.ec.etrustex.adapter.service;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageReferenceState;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.model.vo.MessageReferenceVO;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.PartyDao;
import eu.europa.ec.etrustex.adapter.persistence.api.message.MessageDao;
import eu.europa.ec.etrustex.adapter.testutils.JsonUtils;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.*;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static eu.europa.ec.etrustex.adapter.model.entity.message.NotificationState.NF_NOTIFIED;
import static eu.europa.ec.etrustex.adapter.testutils.ResourcesUtils.readResource;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.RandomUtils.nextLong;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author François Gautier
 * @version 1.0
 * @since 12-Oct-17
 */
@RunWith(JMockit.class)
public class MessageServicePluginImplTest {

    @Injectable
    private MessageDao messageDao;
    @Injectable
    private PartyDao partyDao;

    @Tested
    private MessageServicePluginImpl messageService;

    @Test
    public void createMessage() {
        final EtxMessage messageEntity = new EtxMessage();
        messageService.createMessage(messageEntity);

        new FullVerifications() {{
            messageDao.save(messageEntity);
            times = 1;
        }};
    }

    @Test
    public void updateMessage() {
        final EtxMessage messageEntity = new EtxMessage();
        messageService.updateMessage(messageEntity);

        new FullVerifications() {{
            messageDao.update(messageEntity);
            times = 1;
        }};
    }

    @Test
    public void referenceMessageTypeFor_happyFlow() {
        final EtxMessage etx = new EtxMessage();
        etx.setSender(getParty(nextLong(0, 99999)));
        etx.setReceiver(getParty(nextLong(0, 99999)));
        etx.setMessageReferenceUuid(UUID.randomUUID().toString());
        final MessageType messageStatus = MessageType.MESSAGE_STATUS;

        new Expectations() {{
            messageDao.findMessagesByUuidAndSenderAndReceiver(
                    etx.getMessageReferenceUuid(),
                    etx.getReceiver().getId(),
                    etx.getSender().getId()
            );
            times = 1;
            result = singletonList(new MessageReferenceVO(
                    UUID.randomUUID().toString(),
                    messageStatus,
                    UUID.randomUUID().toString(),
                    UUID.randomUUID().toString()));
        }};

        MessageType result = messageService.referenceMessageTypeFor(etx);
        assertThat(result, is(messageStatus));
        new FullVerifications() {{
        }};
    }

    @Test
    public void referenceMessageTypeFor_twoMessages_exception() {
        final EtxMessage etx = new EtxMessage();
        etx.setSender(getParty(nextLong(0, 99999)));
        etx.setReceiver(getParty(nextLong(0, 99999)));
        etx.setMessageReferenceUuid(UUID.randomUUID().toString());
        final MessageType messageStatus = MessageType.MESSAGE_STATUS;
        final List<MessageReferenceVO> resultList = Arrays.asList(
                new MessageReferenceVO(UUID.randomUUID().toString(), messageStatus, UUID.randomUUID().toString(), UUID.randomUUID().toString()),
                new MessageReferenceVO(UUID.randomUUID().toString(), messageStatus, UUID.randomUUID().toString(), UUID.randomUUID().toString())
        );

        new Expectations() {{
            messageDao.findMessagesByUuidAndSenderAndReceiver(
                    etx.getMessageReferenceUuid(),
                    etx.getReceiver().getId(),
                    etx.getSender().getId()
                    );
            times = 1;
            result = resultList;
        }};

        try {
            messageService.referenceMessageTypeFor(etx);
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getMessage(), is(
                    "message uuid: " + etx.getMessageReferenceUuid() +
                            " receiverPartyUuid:" + etx.getSender().getId() +
                            " senderPartyUuid: " + etx.getReceiver().getId() +
                            ", records expected: 1, records found: " + resultList.size()));
            assertThat(e.getError(), is(ETrustExError.ETX_009));
        }
        new FullVerifications() {{
        }};
    }

    private EtxParty getParty(long id) {
        EtxParty etxParty = new EtxParty();
        etxParty.setId(id);
        return etxParty;
    }

    @Test
    public void findMessageByDomibusMessageId() {
        final String partyUuid = UUID.randomUUID().toString();
        final EtxMessage etx = new EtxMessage();

        new Expectations() {{
            messageDao.findMessageByDomibusMessageId(partyUuid);
            times = 1;
            result = etx;
        }};

        EtxMessage result = messageService.findMessageByDomibusMessageId(partyUuid);
        assertThat(result, is(etx));

        new FullVerifications() {{
        }};
    }

    @Test
    public void findMessageByDomibusMessageId_noMessageFound() {
        final String partyUuid = UUID.randomUUID().toString();

        new Expectations() {{
            messageDao.findMessageByDomibusMessageId(partyUuid);
            times = 1;
            result = null;
        }};

        try {
            messageService.findMessageByDomibusMessageId(partyUuid);
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getMessage(), is("Missing entry in eTrustEx Backend Message DB for Domibus Message Id: " + partyUuid));
            assertThat(e.getError(), is(ETrustExError.ETX_005));
        }
        new FullVerifications() {{
        }};

    }

    @Test
    public void findMessageByDomibusMessageId_noUuid() {
        try {
            messageService.findMessageByDomibusMessageId(null);
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getMessage(), is("The Domibus Message ID required to retrieve the eTrustEx Backend Message has not been sent!"));
            assertThat(e.getError(), is(ETrustExError.DEFAULT));
        }
        new FullVerifications() {{
        }};

    }

    @Test
    public void findMessagesForReceiverPartyUuid() {
        final String partyUuid = UUID.randomUUID().toString();
        new Expectations() {{
            messageDao.findUnreadMessagesByReceiverPartyUuid(partyUuid);
            times = 1;
            result = new ArrayList<MessageReferenceVO>();
        }};

        List<MessageReferenceVO> messagesForReceiverPartyUuid = messageService.findMessagesForReceiverPartyUuid(partyUuid);

        assertThat(messagesForReceiverPartyUuid, is(Collections.<MessageReferenceVO>emptyList()));
        new FullVerifications() {{
        }};
    }

    @Test
    public void createMessageStatusFor_happyFlow_AVAILABLE() throws Exception {
        new Expectations() {{
            messageDao.save((EtxMessage) any);
            times = 1;
        }};

        EtxMessage etxMessage = messageService.createMessageStatusFor(getMessageBundle("data/EtxMessageBundle_IN_Valid_OneAttachment.json"), MessageReferenceState.AVAILABLE);

        assertThat(
                etxMessage,
                sameBeanAs(getMessageBundle("data/EtxMessageStatusFor_OUT_CreatedFrom_EtxMessage.json"))
                        .ignoring("messageUuid")
                        .ignoring("issueDateTime")
                        .with("issueDateTime", notNullValue())
                        .with("messageUuid", notNullValue())
        );
        new FullVerifications() {{
        }};
    }

    @Test
    public void createMessageStatusFor_happyFlow_FAILED() throws Exception {
        new Expectations() {{
            messageDao.save((EtxMessage) any);
            times = 1;
        }};

        EtxMessage etxMessage = messageService.createMessageStatusFor(
                getMessageBundle("data/EtxMessageBundle_FAILED_PENDING.json"),
                MessageReferenceState.FAILED);

        assertThat(
                etxMessage,
                sameBeanAs(getMessageBundle("data/EtxMessageStatusFor_FAILED.json"))
                        .ignoring("messageUuid")
                        .ignoring("issueDateTime")
                        .with("issueDateTime", notNullValue())
                        .with("messageUuid", notNullValue())
        );
        new FullVerifications() {{
        }};
    }

    private EtxMessage getMessageBundle(String fileName) throws java.io.IOException {
        return JsonUtils.getObjectMapper().readValue(
                readResource(fileName),
                EtxMessage.class);
    }

    @Test
    public void read_happyFlow_AlreadyRead() {
        final String messageUuid = "MESSAGE_UUID";
        final String senderPartyUuid = "SENDER_UUID";
        final String receiverPartyUuid = "RECEIVER_UUID";
        final List<MessageType> messageTypes = singletonList(MessageType.MESSAGE_BUNDLE);
        new Expectations() {{
            messageDao.findEtxMessage(messageUuid, messageTypes,
                    MessageDirection.IN, senderPartyUuid, receiverPartyUuid);
            EtxMessage result = new EtxMessage();
            result.setNotificationState(NF_NOTIFIED);
            this.result = result;

        }};
        EtxMessage etxMessage = messageService.readMessage(senderPartyUuid, receiverPartyUuid, messageUuid, messageTypes);
        assertThat(etxMessage, is(notNullValue()));
        assertThat(etxMessage.getNotificationState(), is(NF_NOTIFIED));
        new FullVerifications() {{
        }};
    }

    @Test
    public void read_happyFlow() {

        final String messageUuid = "MESSAGE_UUID";
        final String senderPartyUuid = "SENDER_UUID";
        final String receiverPartyUuid = "RECEIVER_UUID";
        final List<MessageType> messageTypes = singletonList(MessageType.MESSAGE_BUNDLE);
        new Expectations() {{
            messageDao.findEtxMessage(messageUuid, messageTypes,
                    MessageDirection.IN, senderPartyUuid, receiverPartyUuid);
            result = new EtxMessage();

        }};
        EtxMessage etxMessage = messageService.readMessage(senderPartyUuid, receiverPartyUuid, messageUuid, messageTypes);
        assertThat(etxMessage, is(notNullValue()));
        assertThat(etxMessage.getNotificationState(), is(NF_NOTIFIED));
        new FullVerifications() {{
        }};
    }

    @Test
    public void read_null() {

        final String messageUuid = "MESSAGE_UUID";
        final String senderPartyUuid = "SENDER_UUID";
        final String receiverPartyUuid = "RECEIVER_UUID";
        final List<MessageType> messageTypes = singletonList(MessageType.MESSAGE_BUNDLE);
        new Expectations() {{
            messageDao.findEtxMessage(messageUuid, messageTypes,
                    MessageDirection.IN, senderPartyUuid, receiverPartyUuid);
            times = 1;
            result = null;
        }};
        EtxMessage etxMessage = messageService.readMessage(senderPartyUuid, receiverPartyUuid, messageUuid, messageTypes);

        assertThat(etxMessage, is(nullValue()));
        new FullVerifications() {{
        }};
    }
}
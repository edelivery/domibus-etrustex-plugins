package eu.europa.ec.etrustex.adapter.queue.postupload;

import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageDirection;
import eu.europa.ec.etrustex.adapter.queue.out.sendmessage.SendMessageProducer;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.adapter.service.WorkspaceServiceBackEndImpl;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import eu.europa.ec.etrustex.common.util.Validate;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.hamcrest.CoreMatchers;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import static eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter.NodeDocumentWrapperServiceAdapterTest.formValidEtxAttachmentForStoreDocumentWrapper;
import static eu.europa.ec.etrustex.adapter.model.entity.message.MessageState.*;
import static eu.europa.ec.etrustex.common.util.LoggingUtils.MDC_ETX_ATT_UUID;

/**
 * @author François Gautier
 * @version 1.0
 * @since 07/03/2018
 */
@SuppressWarnings("Duplicates")
@RunWith(JMockit.class)
public class PostUploadHandlerImplTest {

    private static final long ID = 1L;

    @Injectable
    private MessageServicePlugin messageService;

    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;

    @Injectable
    private SendMessageProducer sendMessageProducer;

    @Injectable
    private PostProcessProducer postProcessProducer;

    @Injectable
    private WorkspaceServiceBackEndImpl etxBackendPluginWorkspaceService;

    @Injectable
    private AttachmentService attachmentService;

    @Tested
    private PostUploadHandlerImpl postUploadHandler;

    @Test(expected = ETrustExPluginException.class)
    public void validatePostUpload_msgNotFound() {
        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = null;
        }};

        postUploadHandler.validatePostUpload(ID);

        new FullVerifications() {{
            Validate.notNull(any, anyString);
        }};
    }

    @Test
    public void validatePostUpload_isFinal() {
        final EtxAttachment etxAttachment = formValidEtxAttachmentForStoreDocumentWrapper();
        etxAttachment.setStateType(AttachmentState.ATTS_ALREADY_UPLOADED);
        final EtxMessage etxMessage = etxAttachment.getMessage();

        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = etxMessage;
        }};

        postUploadHandler.validatePostUpload(ID);
        new FullVerifications() {{
            clearEtxLogKeysService.clearSpecificKeys(MDC_ETX_ATT_UUID);
            times = 1;
        }};
    }

    @Test
    public void validatePostUpload_isNotFinal() {
        final EtxAttachment etxAttachment = formValidEtxAttachmentForStoreDocumentWrapper();
        etxAttachment.setStateType(AttachmentState.ATTS_CREATED);
        final EtxMessage etxMessage = etxAttachment.getMessage();

        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = etxMessage;
        }};

        try {
            postUploadHandler.validatePostUpload(ID);
            Assert.fail();
        } catch (ETrustExPluginException e) {
            Assert.assertThat(e.getMessage(), CoreMatchers.containsString(String.format("For Message[ID: %s] Non-final attachment state [ID: %s; state: %s]", ID, etxAttachment.getId(), AttachmentState.ATTS_CREATED)));
        }
        new FullVerifications() {{
        }};
    }

    @Test(expected = ETrustExPluginException.class)
    public void onError_msgNotFound() {
        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = null;
        }};
        postUploadHandler.onError(ID);
        new FullVerifications() {{
            Validate.notNull(any, anyString);
        }};
    }

    @Test
    public void onError_allAttachmentInFinalState() {
        final EtxAttachment etxAttachment = formValidEtxAttachmentForStoreDocumentWrapper();
        final EtxMessage etxMessage = etxAttachment.getMessage();
        etxMessage.setDirectionType(MessageDirection.IN);

        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = etxMessage;

            attachmentService.areAllAttachmentsInFinalState(etxMessage);
            times = 1;
            result = true;
        }};

        postUploadHandler.onError(ID);

        new FullVerifications() {{
            postProcessProducer.triggerPostProcess(etxMessage.getId());
            times = 1;
        }};
    }

    @Test
    public void onError_NOTallAttachmentInFinalState() {
        final EtxAttachment etxAttachment = formValidEtxAttachmentForStoreDocumentWrapper();
        final EtxMessage etxMessage = etxAttachment.getMessage();
        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = etxMessage;

            attachmentService.areAllAttachmentsInFinalState(etxMessage);
            times = 1;
            result = false;
        }};
        postUploadHandler.onError(ID);
        new FullVerifications() {{

        }};
    }

    @Test(expected = ETrustExPluginException.class)
    public void handlePostUpload_attNotFound() {

        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = null;
        }};

        postUploadHandler.handlePostUpload(ID);

        new FullVerifications() {{
            Validate.notNull(any, anyString);
        }};
    }

    @Test
    public void handlePostUpload_messageFinal() {
        final EtxAttachment etxAttachment = formValidEtxAttachmentForStoreDocumentWrapper();
        etxAttachment.getMessage().setMessageState(MS_PROCESSED);
        final EtxMessage etxMessage = etxAttachment.getMessage();

        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = etxMessage;
        }};

        postUploadHandler.handlePostUpload(ID);

        new FullVerifications() {{
            etxBackendPluginWorkspaceService.cleanWorkspaceFilesForBundleAsynchronously(etxMessage);
            times = 1;
        }};
    }

    @Test
    public void handlePostUpload_messageNotFinal_IN() {
        final EtxAttachment etxAttachment = formValidEtxAttachmentForStoreDocumentWrapper();
        final EtxMessage message = etxAttachment.getMessage();
        message.setMessageState(MS_CREATED);
        message.setDirectionType(MessageDirection.IN);

        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = message;

            attachmentService.areAllAttachmentsInFinalState(message);
            times = 1;
            result = true;
        }};

        postUploadHandler.handlePostUpload(ID);

        new FullVerifications() {{
            etxBackendPluginWorkspaceService.cleanWorkspaceFilesForBundleAsynchronously(message);
            times = 1;

            postProcessProducer.triggerPostProcess(message.getId());
            times = 1;
        }};
    }

    @Test
    public void handlePostUpload_messageCreated_OUT_allAttUploaded() {
        final EtxAttachment etxAttachment = formValidEtxAttachmentForStoreDocumentWrapper();
        final EtxMessage message = etxAttachment.getMessage();
        message.setMessageState(MS_CREATED);
        message.setDirectionType(MessageDirection.OUT);

        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = message;

            attachmentService.areAllAttachmentsInFinalState(message);
            times = 1;
            result = true;

            attachmentService.areAllAttachmentsUploaded(message);
            times = 1;
            result = true;
        }};

        postUploadHandler.handlePostUpload(ID);

        new FullVerifications() {{
            etxBackendPluginWorkspaceService.cleanWorkspaceFilesForBundleAsynchronously(message);
            times = 1;

            messageService.updateMessage(message);
            times = 1;

            sendMessageProducer.triggerSendBundle(message.getId());
            times = 1;
        }};

        Assert.assertThat(message.getMessageState(), Is.is(MS_UPLOADED));
    }

    @Test
    public void handlePostUpload_messageCreated_OUT_notAllAttUploaded() {
        final EtxAttachment etxAttachment = formValidEtxAttachmentForStoreDocumentWrapper();
        final EtxMessage message = etxAttachment.getMessage();
        message.setMessageState(MS_CREATED);
        message.setDirectionType(MessageDirection.OUT);

        new Expectations() {{
            messageService.findById(ID);
            times = 1;
            result = message;

            attachmentService.areAllAttachmentsInFinalState(message);
            times = 1;
            result = true;

            attachmentService.areAllAttachmentsUploaded(message);
            times = 1;
            result = false;
        }};

        postUploadHandler.handlePostUpload(ID);

        new FullVerifications() {{
            etxBackendPluginWorkspaceService.cleanWorkspaceFilesForBundleAsynchronously(message);
            times = 1;

            postProcessProducer.triggerPostProcess(message.getId());
            times = 1;
        }};
    }
}
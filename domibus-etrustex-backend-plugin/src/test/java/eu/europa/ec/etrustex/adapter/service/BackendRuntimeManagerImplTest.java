package eu.europa.ec.etrustex.adapter.service;

import eu.europa.ec.etrustex.common.service.CacheService;
import eu.europa.ec.etrustex.common.service.WorkspaceService;
import eu.europa.ec.etrustex.adapter.integration.client.backend.repository.LocalSharedFileRepositoryService;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * JUnit test class for <code>RuntimeManagerImpl</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@RunWith(JMockit.class)
public class BackendRuntimeManagerImplTest {

    @Tested
    BackendRuntimeManagerImpl runtimeManager;

    @Injectable
    private CacheService cacheService;

    @Injectable
    private WorkspaceService etxBackendPluginWorkspaceService;

    @Injectable
    private LocalSharedFileRepositoryService fileRepositoryService;

    @Test
    public void testResetCache_HappyFlow() throws Exception {

        runtimeManager.resetCache();

        new Verifications() {{
            cacheService.clearCache();
            times = 1;

            etxBackendPluginWorkspaceService.loadConfiguration();
            times = 1;

            fileRepositoryService.loadConfiguration();
            times = 1;

        }};
    }

}
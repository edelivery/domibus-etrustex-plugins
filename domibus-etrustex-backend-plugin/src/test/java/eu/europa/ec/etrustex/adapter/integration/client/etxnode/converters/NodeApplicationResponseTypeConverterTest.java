package eu.europa.ec.etrustex.adapter.integration.client.etxnode.converters;

import eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter.NodeDocumentWrapperServiceAdapterTest;
import eu.europa.ec.etrustex.adapter.model.entity.common.StatusResponseCode;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageReferenceState;
import eu.europa.ec.etrustex.adapter.testutils.XsdCommonsUtils;
import mockit.integration.junit4.JMockit;
import oasis.names.specification.ubl.schema.xsd.applicationresponse_2.ApplicationResponseType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import static eu.europa.ec.etrustex.common.util.CalendarHelper.getJodaDateFromCalendar;
import static eu.europa.ec.etrustex.adapter.integration.client.etxnode.converters.NodeApplicationResponseTypeConverter.MESSAGE_BUNDLE;
import static eu.europa.ec.etrustex.adapter.integration.client.etxnode.converters.NodeApplicationResponseTypeConverter.convertEtxMessage;
import static eu.europa.ec.etrustex.adapter.testutils.XsdCommonsUtils.getDocumentResponseType;
import static eu.europa.ec.etrustex.adapter.testutils.XsdCommonsUtils.getIdType;

/**
 * JUnit class for {@link NodeApplicationResponseTypeConverter}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 10/11/2017
 */
@RunWith(JMockit.class)
public class NodeApplicationResponseTypeConverterTest {

    private final static String documentReferenceUuid = "document_ref_id";
    private final static String messageUuid = "TEST_MSG_uuid_12345";
    private final static String description = "description_test_123";

    private final DescriptionType descriptionType = new DescriptionType();
    private final List<DescriptionType> descList = Collections.singletonList(descriptionType);
    private final Calendar issueDate = Calendar.getInstance();
    private final DateTime jodaIssueDate = new DateTime(issueDate);

    @Before
    public void setUp() {
        descriptionType.setValue(description);
    }

    /**
     * Happy flow - status for message = {@link MessageReferenceState#READ}
     */
    @Test
    public void testConvertEtxMessage_HappyFlow() throws Exception {
        EtxMessage etxMessage = NodeDocumentWrapperServiceAdapterTest.formValidEtxAttachmentForStoreDocumentWrapper().getMessage();
        etxMessage.setMessageUuid(messageUuid);
        etxMessage.setIssueDateTime(issueDate);
        etxMessage.setMessageReferenceUuid(documentReferenceUuid);
        //status is read
        etxMessage.setMessageReferenceStateType(MessageReferenceState.READ);

        //tested method
        ApplicationResponseType response = convertEtxMessage(etxMessage);

        Assert.assertNotNull("resulting object shouldn't be null", response);
        Assert.assertEquals("message Uuid didn't match", messageUuid, response.getID().getValue());
        Assert.assertEquals("issue date didn't match", getJodaDateFromCalendar(issueDate), response.getIssueDate().getValue());
        Assert.assertEquals("message ref id didn't match", documentReferenceUuid, response.getDocumentResponse().getResponse().getReferenceID().getValue());
        Assert.assertEquals("message ref id didn't match", documentReferenceUuid, response.getDocumentResponse().getDocumentReference().getID().getValue());
        Assert.assertEquals("document type code didn't match",
                MESSAGE_BUNDLE,
                response.getDocumentResponse().getDocumentReference().getDocumentTypeCode().getValue());

        Assert.assertEquals("status reason code id didn't match", StatusResponseCode.BDL7.getCode(),
                response.getDocumentResponse().getResponse().getResponseCode().getValue());
    }


    /**
     * Happy flow - status for message = {@link MessageReferenceState#AVAILABLE}
     */
    @Test
    public void testToEtxMessage_HappyFlow() {
        final String responseCode = "BDL:1";
        ApplicationResponseType applicationResponse = new ApplicationResponseType();
        applicationResponse.setID(getIdType(messageUuid));
        applicationResponse.setDocumentResponse(getDocumentResponseType(documentReferenceUuid, responseCode, descList));
        applicationResponse.setIssueDate(XsdCommonsUtils.getIssueDateType(jodaIssueDate));

        //tested method
        EtxMessage etxMessage = NodeApplicationResponseTypeConverter.toEtxMessage(applicationResponse);

        verifyEtxMessage(responseCode, etxMessage, MessageReferenceState.AVAILABLE);

    }

    /**
     * Happy flow - status for message = {@link MessageReferenceState#READ}
     */
    @Test
    public void testToEtxMessage_StatusRead() {

        final String responseCode = "BDL:7";
        ApplicationResponseType applicationResponse = new ApplicationResponseType();
        applicationResponse.setID(getIdType(messageUuid));
        applicationResponse.setDocumentResponse(getDocumentResponseType(documentReferenceUuid, responseCode, descList));
        applicationResponse.setIssueDate(XsdCommonsUtils.getIssueDateType(jodaIssueDate));

        //tested method
        EtxMessage etxMessage = NodeApplicationResponseTypeConverter.toEtxMessage(applicationResponse);

        verifyEtxMessage(responseCode, etxMessage, MessageReferenceState.READ);

    }

    /**
     * Happy flow - status for message = {@link MessageReferenceState#FAILED}
     */
    @Test
    public void testToEtxMessage_StatusFailed() {

        final String responseCode = "301:4";
        ApplicationResponseType applicationResponse = new ApplicationResponseType();
        applicationResponse.setID(getIdType(messageUuid));
        applicationResponse.setDocumentResponse(getDocumentResponseType(documentReferenceUuid, responseCode, descList));
        applicationResponse.setIssueDate(XsdCommonsUtils.getIssueDateType(jodaIssueDate));

        //tested method
        EtxMessage etxMessage = NodeApplicationResponseTypeConverter.toEtxMessage(applicationResponse);

        verifyEtxMessage(responseCode, etxMessage, MessageReferenceState.FAILED);

    }

    /**
     * Negative scenario - input {@link ApplicationResponseType}
     */
    @Test
    public void testToEtxMessage_ApplicationResponseNull() {
        final ApplicationResponseType applicationResponse = null;

        //tested method
        EtxMessage etxMessage = NodeApplicationResponseTypeConverter.toEtxMessage(applicationResponse);
        Assert.assertNull("resulting object should be null", etxMessage);
    }

    /**
     * verify {@link EtxMessage} obtained from converter
     *
     * @param responseCode   response code
     * @param etxMessage     {@link EtxMessage}
     * @param referenceState reference state
     */
    private void verifyEtxMessage(String responseCode, EtxMessage etxMessage, MessageReferenceState referenceState) {
        Assert.assertNotNull("resulting object shouldn't be null", etxMessage);
        Assert.assertEquals("message Uuid didn't match", messageUuid, etxMessage.getMessageUuid());
        Assert.assertEquals("message ref id didn't match", documentReferenceUuid, etxMessage.getMessageReferenceUuid());
        Assert.assertEquals("status reason code id didn't match", responseCode, etxMessage.getStatusReasonCode());
        Assert.assertEquals("reference state type didn't match", referenceState, etxMessage.getMessageReferenceStateType());
        Assert.assertEquals("message description didn't match", description, etxMessage.getMessageContent());
        Assert.assertEquals("issue date didn't match", jodaIssueDate.toGregorianCalendar(), etxMessage.getIssueDateTime());
    }
}
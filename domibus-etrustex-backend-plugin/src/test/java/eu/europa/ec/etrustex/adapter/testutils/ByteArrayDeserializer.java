package eu.europa.ec.etrustex.adapter.testutils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.math.BigInteger;

/**
 * @author François Gautier
 * @version 1.0
 * @since 06-Nov-17
 */
public class ByteArrayDeserializer extends StdDeserializer<byte[]> {
        public ByteArrayDeserializer() {
            this(null);
        }

        public ByteArrayDeserializer(Class<?> vc) {
            super(vc);
        }

        @Override
        public byte[] deserialize(JsonParser jp, DeserializationContext ctxt)
                throws IOException {
            JsonNode node = jp.getCodec().readTree(jp);
            String binaryString = node.asText();
            return new BigInteger(binaryString,16).toByteArray();
        }
}

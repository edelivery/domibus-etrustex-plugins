package eu.europa.ec.etrustex.adapter.application;

import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.quartz.JobExecutionContext;

@RunWith(JMockit.class)
public class BackEndQuartzJobBeanTest extends AbstractBackEndQuartzJobBeanTest {


    @Tested(fullyInitialized = true)
    private BackEndQuartzJobBeanTest.BackEndQuartzJobBeanTestImpl backEndQuartzJobBeanTest;

    @Test
    public void isAllowed() {
        new Expectations(){{
            etxBackendPluginProperties.getDomain();
            times = 2;
            result = "default";
        }};
        Assert.assertThat(backEndQuartzJobBeanTest.isAllowedToRun("default"), Is.is(true));
        new FullVerifications(){{}};
    }

    @Test
    public void isNotAllowed() {
        new Expectations(){{
            etxBackendPluginProperties.getDomain();
            times = 2;
            result = "test";
        }};
        Assert.assertThat(backEndQuartzJobBeanTest.isAllowedToRun("NOPE"), Is.is(false));
        new FullVerifications(){{}};
    }

    @Test
    public void isAllowed_empty() {
        Assert.assertThat(backEndQuartzJobBeanTest.isAllowedToRun("test"), Is.is(true));
    }

    static class BackEndQuartzJobBeanTestImpl extends BackEndQuartzJobBean {

        @Override
        public void process(JobExecutionContext context) {

        }
    }
}
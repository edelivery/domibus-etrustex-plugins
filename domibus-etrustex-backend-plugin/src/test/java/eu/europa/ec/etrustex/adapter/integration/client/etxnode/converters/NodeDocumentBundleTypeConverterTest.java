package eu.europa.ec.etrustex.adapter.integration.client.etxnode.converters;

import com.shazam.shazamcrest.matcher.Matchers;
import ec.schema.xsd.documentbundle_1.DocumentBundleType;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.testutils.JsonUtils;
import org.junit.Test;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

import static eu.europa.ec.etrustex.adapter.testutils.ResourcesUtils.readResource;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 05/06/2018
 */
public class NodeDocumentBundleTypeConverterTest {

    @Test
    public void convertEtxMessage() throws IOException, ParserConfigurationException {
        final EtxMessage etxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageBundle_IN_Valid_OneAttachment.json"),
                EtxMessage.class);

        DocumentBundleType documentBundleType = NodeDocumentBundleTypeConverter.convertEtxMessage(etxMessage);

        assertThat(documentBundleType,

                Matchers.sameBeanAs(
                        JsonUtils
                                .getObjectMapper()
                                .readValue(readResource("data/documentBundleType_from_EtxMessageBundle_IN_Valid_OneAttachment.json"),
                                        DocumentBundleType.class))
                        .ignoring("issueDate")
                        .ignoring("ublExtensions")
        );
    }

    @Test
    public void toEtxMessage() throws IOException {
        final EtxMessage etxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageBundle_IN_Valid_OneAttachment.json"),
                EtxMessage.class);
        DocumentBundleType documentBundleType1 = JsonUtils
                .getObjectMapper()
                .readValue(readResource("data/documentBundleType_from_EtxMessageBundle_IN_Valid_OneAttachment.json"),
                        DocumentBundleType.class);

        EtxMessage result = NodeDocumentBundleTypeConverter.toEtxMessage(documentBundleType1);

        assertThat(result, Matchers.sameBeanAs(etxMessage)
        .ignoring("mimeType")
        .ignoring("creationDate")
        .ignoring("directionType")
        .ignoring("id")
        .ignoring("messageReferenceStateType")
        .ignoring("messageState")
        .ignoring("messageType")
        .ignoring("modificationDate")
        .ignoring("receiver")
        .ignoring("sender")
        .ignoring("signature")
        .ignoring("signedData")
        .ignoring("attachmentList")
        );
    }
}
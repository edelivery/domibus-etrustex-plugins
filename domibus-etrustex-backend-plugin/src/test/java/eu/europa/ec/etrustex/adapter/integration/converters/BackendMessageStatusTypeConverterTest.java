package eu.europa.ec.etrustex.adapter.integration.converters;

import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageType;
import eu.europa.ec.etrustex.adapter.testutils.JsonUtils;
import eu.europa.ec.etrustex.integration.model.common.v2.EtxMessageStatusType;
import org.junit.Test;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static eu.europa.ec.etrustex.adapter.testutils.ResourcesUtils.readResource;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 06-Nov-17
 */
public class BackendMessageStatusTypeConverterTest {

    @Test
    public void toEtxMessage_adapter() throws Exception {

        EtxMessageStatusType etxMessageBundleType = JsonUtils.getObjectMapper().readValue(
                readResource("data/ExpectedResult_StatusMessageConverter_fromEtxMessage.json"),
                EtxMessageStatusType.class);

        EtxMessage etxMessage = BackendMessageStatusTypeConverter.toEtxMessage(etxMessageBundleType);

        assertThat(
                etxMessage,
                sameBeanAs(JsonUtils.getObjectMapper().readValue(
                        readResource("data/ExpectedResult_BackendMessageStatusTypeConverterTest_toEtxMessage_adapter.json"),
                        EtxMessage.class))
        );
    }

    @Test
    public void fromEtxMessage() throws Exception {
        EtxMessage etxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageBundle_IN_Valid_OneAttachment.json"),
                EtxMessage.class);
        EtxMessageStatusType etxMessageStatusType = BackendMessageStatusTypeConverter.convertEtxMessage(etxMessage, MessageType.MESSAGE_ADAPTER_STATUS);

        assertThat(
                etxMessageStatusType,
                sameBeanAs(JsonUtils.getObjectMapper().readValue(
                        readResource("data/ExpectedResult_StatusMessageConverter_fromEtxMessage.json"),
                        EtxMessageStatusType.class))
                        .ignoring("issueDateTime.iChronology")
                        .ignoring("receivedDateTime.iChronology")

        );
    }

    @Test
    public void fromEtxMessage_failed() throws Exception {
        EtxMessage etxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageBundle_FAILED_PENDING.json"),
                EtxMessage.class);
        EtxMessageStatusType etxMessageStatusType = BackendMessageStatusTypeConverter.convertEtxMessage(etxMessage, MessageType.MESSAGE_ADAPTER_STATUS);

        assertThat(
                etxMessageStatusType,
                sameBeanAs(JsonUtils.getObjectMapper().readValue(
                        readResource("data/ExpectedResult_BackendMessageStatusTypeConverter_fromEtxMessage_failed.json"),
                        EtxMessageStatusType.class))
                        .ignoring("issueDateTime.iChronology")
                        .ignoring("receivedDateTime.iChronology")
        );
    }
}
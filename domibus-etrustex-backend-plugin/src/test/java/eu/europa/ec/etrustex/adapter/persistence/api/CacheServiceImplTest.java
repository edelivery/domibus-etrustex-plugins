package eu.europa.ec.etrustex.adapter.persistence.api;

import eu.europa.ec.etrustex.adapter.AbstractSpringContextIT;
import eu.europa.ec.etrustex.common.service.CacheService;
import eu.europa.ec.etrustex.common.service.CacheServiceImpl;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import mockit.FullVerifications;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

import java.io.File;

/**
 * @author Catalin Enache
 * @version 1.0
 */
public class CacheServiceImplTest extends AbstractSpringContextIT {
    /**
     * name of the cache manager for etxBackend plugin
     */
    private final static String ETX_ADAPTER_CACHE_MANAGER = "etxAdapter";

    /**
     * name of the cache to check for etxBackend plugin
     */
    private final static String ETX_ADAPTER_CACHE_NAME_1 = "config";
    @Autowired
    private CacheService cacheService;

    @Autowired
    private CacheManager cacheManager;

    @Test
    public void testClearCache_HappyFlow() {
        Assert.assertNotNull("cache manager should be present", cacheManager);

        Cache cache = cacheManager.getCache(ETX_ADAPTER_CACHE_NAME_1);
        Assert.assertNotNull("cache named " + ETX_ADAPTER_CACHE_NAME_1 + " should be present", cache);

        //put a new element
        cache.put("key1", new EtxSystem());

        Assert.assertNotNull("cache should have size > 0", cache.get("key1"));

        //do the thing
        cacheService.clearCache();

        Assert.assertNull("cache should have size > 0", cache.get("key1"));
    }

}
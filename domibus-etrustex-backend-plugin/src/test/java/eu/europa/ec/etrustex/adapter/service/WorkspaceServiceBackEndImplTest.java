package eu.europa.ec.etrustex.adapter.service;

import eu.domibus.ext.delegate.services.multitenancy.DomainTaskExecutorExtDelegate;
import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.services.DomainContextExtService;
import eu.domibus.ext.services.DomainExtService;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.model.entity.administration.AdapterConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.queue.inbox.uploadattachment.InboxUploadAttachmentHandlerImplTest;
import eu.europa.ec.etrustex.common.service.AbstractWorkspaceService;
import eu.europa.ec.etrustex.common.util.ETrustExPluginAS4Properties;
import eu.europa.ec.etrustex.common.util.WorkSpaceUtils;
import mockit.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.hamcrest.core.Is.is;

/**
 * @author François Gautier
 * @version 1.0
 * @since 22-Dec-17
 */
@SuppressWarnings("unused")
public class WorkspaceServiceBackEndImplTest {

    private static final String STRING = "TEST";

    @Injectable
    private ETrustExBackendPluginProperties etxBackendPluginProperties;
    @Injectable
    private AdapterConfigurationService adapterConfigurationService;
    @Injectable
    protected ETrustExPluginAS4Properties etxPluginAS4Properties;
    @Injectable
    private DomainContextExtService domainContextExtService;
    @Injectable
    private DomainTaskExecutorExtDelegate taskExecutor;
    @Injectable
    private DomainExtService domainExtService;
    @Injectable
    protected DomainTaskExecutorExtDelegate executor;

    @Tested
    private WorkspaceServiceBackEndImpl workspaceServiceNode = new WorkspaceServiceBackEndImpl();

    private HashMap<AdapterConfigurationProperty, String> config;

    @Before
    public void setUp() {
        //stubbing post-construct method
        new MockUp<AbstractWorkspaceService>() {
            @Mock
            void loadConfiguration() {
            }
        };
        config = new HashMap<>();
        executor = new DomainTaskExecutorExtDelegate(){
            public void submitLongRunningTask(Runnable task, DomainDTO domainDTO) {
                task.run();
            }
        };
    }

    @Test
    public void testCleanWorkspaceFilesForBundleAsynchronously_RunWithoutException(@Mocked EtxMessage etxMessage) {
        EtxAttachment etxAttachment1 = InboxUploadAttachmentHandlerImplTest.createAttachment(AttachmentState.ATTS_UPLOADED);
        EtxAttachment etxAttachment2 = InboxUploadAttachmentHandlerImplTest.createAttachment(AttachmentState.ATTS_ALREADY_UPLOADED);
        EtxAttachment etxAttachment3 = InboxUploadAttachmentHandlerImplTest.createAttachment(AttachmentState.ATTS_IGNORED);
        final List<EtxAttachment> etxAttachmentList = new ArrayList<>();
        etxAttachmentList.add(etxAttachment1);
        etxAttachmentList.add(etxAttachment2);
        etxAttachmentList.add(etxAttachment3);

        new Expectations() {{
            etxMessage.getId();
            result = 1L;

            etxMessage.getMessageUuid();
            result = "BUNDLE_UUID";

            etxMessage.getAttachmentList();
            times = 1;
            result = etxAttachmentList;

        }};
        workspaceServiceNode.cleanWorkspaceFilesForBundleAsynchronously(etxMessage);

        new FullVerifications() {{
        }};
    }

    @Test(expected = NullPointerException.class)
    public void getRoot_PathNotFound() {

        new Expectations() {{
            adapterConfigurationService.getAdapterConfigurations();
            times = 1;
            result = config;
        }};

        workspaceServiceNode.getRoot();

        new FullVerifications() {{
        }};

    }

    @Test
    public void getRoot(
            @Mocked final Paths paths,
            @Mocked final WorkSpaceUtils workSpaceUtils) {

        final Path path = getPath();
        config.put(AdapterConfigurationProperty.ETX_ADAPTER_WORKSPACE_ROOT_PATH, STRING);
        new Expectations() {{
            adapterConfigurationService.getAdapterConfigurations();
            times = 1;
            result = config;

            Paths.get(STRING);
            times = 1;
            result = path;

            WorkSpaceUtils.getPath((Path) any);
            times = 1;
        }};

        workspaceServiceNode.getRoot();

        new FullVerifications() {{
            Path resultPath;
            WorkSpaceUtils.getPath(resultPath = withCapture());
            Assert.assertThat(resultPath, is(path));
        }};

    }

    @Test
    public void getRoot_multitenancyOK(
            @Mocked final Paths paths,
            @Mocked final WorkSpaceUtils workSpaceUtils) {
        DomainDTO domainDTO = new DomainDTO();
        final Path path = getPath();
        config.put(AdapterConfigurationProperty.ETX_ADAPTER_WORKSPACE_ROOT_PATH, STRING);
        new Expectations() {{
            adapterConfigurationService.getAdapterConfigurations();
            times = 1;
            result = config;

            Paths.get(STRING);
            times = 1;
            result = path;

            WorkSpaceUtils.getPath((Path) any);
            times = 1;
        }};

        workspaceServiceNode.getRoot();

        new FullVerifications() {{
            Path resultPath;
            WorkSpaceUtils.getPath(resultPath = withCapture());
            Assert.assertThat(resultPath, is(path));
            times = 1;
        }};

    }

    private Path getPath() {
        return new File("").toPath();
    }
}
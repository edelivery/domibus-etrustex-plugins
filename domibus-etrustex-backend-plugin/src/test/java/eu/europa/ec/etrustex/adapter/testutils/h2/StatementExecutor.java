package eu.europa.ec.etrustex.adapter.testutils.h2;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author François Gautier
 * @version 1.0
 * @since 02/02/2018
 */
@Service("etxBackendPluginStatementExecutor")
public class StatementExecutor {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(StatementExecutor.class);

    @Autowired
    private DataSource dataSource;

    public void updateAdapterConfig(final String propertyName, String propertyValue) throws SQLException {
        LOG.info("+++++ Update " + propertyName + " with value " + propertyValue);
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            statement = connection.prepareStatement(
                    "UPDATE ETX_ADT_CONFIG " +
                            "SET ACG_PROPERTY_VALUE = ? " +
                            "WHERE ACG_PROPERTY_NAME = ?");
            statement.setString(1, propertyValue);
            statement.setString(2, propertyName);
            statement.executeUpdate();
            connection.commit();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }
}

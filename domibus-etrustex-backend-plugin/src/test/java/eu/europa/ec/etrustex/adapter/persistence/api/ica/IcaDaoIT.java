package eu.europa.ec.etrustex.adapter.persistence.api.ica;

import eu.europa.ec.etrustex.adapter.model.entity.ica.EtxIca;
import eu.europa.ec.etrustex.adapter.persistence.api.AbstractDaoIT;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBase;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.PartyDao;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class IcaDaoIT extends AbstractDaoIT<EtxIca> {

    @Autowired
    private IcaDao icaDao;

    @Autowired
    private PartyDao partyDao;

    @Override
    protected Long getExistingId() {
        return 10L;
    }

    @Override
    protected DaoBase<EtxIca> getDao() {
        return icaDao;
    }

    @Override
    protected EtxIca getNewEntity() {
        EtxIca etxIca = new EtxIca();
        etxIca.setId(null);
        etxIca.setSender(partyDao.findById(1001L));
        etxIca.setReceiver(partyDao.findById(1002L));
        return etxIca;
    }

    @Test
    public void findIca_ok() {
        List<EtxIca> results = icaDao.findIca(partyDao.findById(1001L).getPartyUuid(), partyDao.findById(1002L).getPartyUuid());
        assertThat(results.size(), is(1));
    }

    @Test
    public void findIca_notFound() {
        List<EtxIca> results = icaDao.findIca("", "");
        assertThat(results.size(), is(0));
    }
}
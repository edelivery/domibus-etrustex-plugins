package eu.europa.ec.etrustex.adapter.queue.error;

import eu.domibus.ext.services.AuthenticationExtService;
import eu.domibus.messaging.MessageNotFoundException;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.integration.ETrustExDomibusBackendConnector;
import eu.europa.ec.etrustex.adapter.integration.handlers.BackendActionHandlerFactory;
import eu.europa.ec.etrustex.adapter.testutils.ETrustExBackendPluginPropertiesBuilder;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import static eu.europa.ec.etrustex.common.handlers.Action.FAULT_ACTION;
import static eu.europa.ec.etrustex.common.handlers.Service.fromString;

@RunWith(JMockit.class)
public class ErrorHandlerImplTest {

    @Injectable
    private ETrustExDomibusBackendConnector eTrustExDomibusBackendConnector;

    @Injectable
    private AuthenticationExtService authenticationExtService;

    @Injectable
    private ETrustExBackendPluginProperties eTrustExBackendPluginProperties;

    @Injectable
    private BackendActionHandlerFactory backendActionHandlerFactory;

    @Tested
    private ErrorHandlerImpl errorHandler;

    @Before
    public void setUp() {
        eTrustExBackendPluginProperties = ETrustExBackendPluginPropertiesBuilder.getInstance();
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    @Test
    public void errorHandler_MessageNotFound() throws MessageNotFoundException {
        final String asMsgId = "";
        new Expectations() {{
            eTrustExDomibusBackendConnector.downloadMessage(asMsgId, (ETrustExAdapterDTO) any);
            times = 1;
            result = new MessageNotFoundException("", new Throwable());
        }};
        try {
            errorHandler.onError(asMsgId);
            Assert.fail();
        } catch (ETrustExPluginException e) {
            //OK
        }

        new FullVerifications() {{
            authenticationExtService.basicAuthenticate(eTrustExBackendPluginProperties.getUser(), eTrustExBackendPluginProperties.getPwd());
            times = 1;
        }};
    }

    @Test
    public void errorHandler_ok_Auth() throws MessageNotFoundException {
        ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        final String service = "DocumentWrapperService";
        eTrustExAdapterDTO.setAs4Service(service);

        final String asMsgId = "";
        new Expectations() {{
            eTrustExDomibusBackendConnector.downloadMessage(asMsgId, (ETrustExAdapterDTO) any);
            times = 1;
            result = eTrustExAdapterDTO;

            backendActionHandlerFactory.getOperationHandler(fromString(service), FAULT_ACTION);
            result = (OperationHandler) dto -> true;
            times = 1;


        }};

        errorHandler.onError(asMsgId);

        new FullVerifications() {{
            authenticationExtService.basicAuthenticate(eTrustExBackendPluginProperties.getUser(), eTrustExBackendPluginProperties.getPwd());
            times = 1;
        }};
    }

    @Test
    public void errorHandler_ok_notAuth() throws MessageNotFoundException {
        ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        final String service = "DocumentWrapperService";
        eTrustExAdapterDTO.setAs4Service(service);
        final String asMsgId = "";
        SecurityContextHolder.getContext().setAuthentication(getAuthToken());
        new Expectations() {{
            eTrustExDomibusBackendConnector.downloadMessage(asMsgId, (ETrustExAdapterDTO) any);
            times = 1;
            result = eTrustExAdapterDTO;

            backendActionHandlerFactory.getOperationHandler(fromString(service), FAULT_ACTION);
            result = (OperationHandler) dto -> true;
            times = 1;
        }};

        errorHandler.onError(asMsgId);

        new FullVerifications() {{
        }};
    }

    private TestingAuthenticationToken getAuthToken() {
        TestingAuthenticationToken authentication = new TestingAuthenticationToken(null, null);
        authentication.setAuthenticated(true);
        return authentication;
    }
}
package eu.europa.ec.etrustex.adapter.testutils;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxUser;

/**
 * Class to build sample parties for Tests
 *
 * @author Arun Raj
 * @version 1.0
 * @since 29/08/2017
 */
public class SamplePartyBuilder {

    public static EtxParty formValid_EGREFFE_APP_PARTY_B4() {
        EtxParty party_EGREFFE_APP_PARTY_B4 = new EtxParty();
        party_EGREFFE_APP_PARTY_B4.setId(20L);
        party_EGREFFE_APP_PARTY_B4.setPartyUuid("TEST_EGREFFE_APP_PARTY_B4");

        EtxUser nodeUser_EGREFFE_APP_PARTY_B4 = new EtxUser();
        nodeUser_EGREFFE_APP_PARTY_B4.setId(26L);
        nodeUser_EGREFFE_APP_PARTY_B4.setName("TEST_EGREFFE_APP_B4");
        nodeUser_EGREFFE_APP_PARTY_B4.setPassword("TestAdapter4");

        party_EGREFFE_APP_PARTY_B4.setSystem(SampleSystemBuilder.formValid_EGREFFE_SYS());

        return party_EGREFFE_APP_PARTY_B4;
    }


    public static EtxParty formValid_EGREFFE_NP_PARTY_B4() {
        EtxParty party_EGREFFE_NP_PARTY_B4 = new EtxParty();
        party_EGREFFE_NP_PARTY_B4.setId(21L);
        party_EGREFFE_NP_PARTY_B4.setPartyUuid("TEST_EGREFFE_NP_PARTY_B4");

        EtxSystem system_WEB_SYS = new EtxSystem();
        system_WEB_SYS.setId(2L);
        system_WEB_SYS.setName("ETX-WEB-SYS");
        party_EGREFFE_NP_PARTY_B4.setSystem(system_WEB_SYS);

        return party_EGREFFE_NP_PARTY_B4;
    }
}

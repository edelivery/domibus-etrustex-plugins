package eu.europa.ec.etrustex.adapter.model.entity.common;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * JUnit class for <code>StatusResponseCode</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
public class StatusResponseCodeTest {

    @Test
    public void testFromCode_HappyFlow() throws Exception {
        //test all the values
        Assert.assertEquals(StatusResponseCode.fromCode("BDL:1"),
                StatusResponseCode.BDL1);
        Assert.assertEquals(StatusResponseCode.fromCode("301:4"),
                StatusResponseCode.STS4);
    }

    @Test
    public void testFromCode_UnhappyFlow() throws Exception {

        //test with some wrong values
        String code = StringUtils.EMPTY;
        Assert.assertNull(StatusResponseCode.fromCode(code));

        code = "wrong code";
        Assert.assertNull(StatusResponseCode.fromCode(code));

        code = "bdl:1";
        Assert.assertNull(StatusResponseCode.fromCode(code));
    }

    @Test
    public void testGetCode_HappyFlow() throws Exception {
        //randomly test some of the values
        Assert.assertEquals(StatusResponseCode.BDL1.getCode(), "BDL:1");
        Assert.assertEquals(StatusResponseCode.STS4.getCode(), "301:4");
        Assert.assertEquals(StatusResponseCode.STS6.getCode(), "301:6");
    }

    @Test
    public void testGetDescription_HappyFlow() throws Exception {
        //test on some values
        Assert.assertEquals(StatusResponseCode.fromCode("BDL:1").getDescription(), "The bundle is available in the receiver's inbox");
        Assert.assertEquals(StatusResponseCode.fromCode("301:4").getDescription(), "Hard business rule violated");
        Assert.assertEquals(StatusResponseCode.fromCode("301:6").getDescription(), "Parent document is not in the expected state");
    }

}
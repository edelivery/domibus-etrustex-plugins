package eu.europa.ec.etrustex.adapter.testutils.domibus;

import eu.domibus.common.ErrorResult;
import eu.domibus.common.MessageStatus;
import eu.domibus.ext.exceptions.MessageExtException;
import eu.domibus.ext.services.MessageExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.logging.MDCKey;
import eu.domibus.messaging.MessagingProcessingException;
import eu.domibus.plugin.Submission;
import eu.domibus.plugin.handler.MessagePuller;
import eu.domibus.plugin.handler.MessageRetriever;
import eu.domibus.plugin.handler.MessageSubmitter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service("etxBackendPluginDatabaseMessageHandler")
public class DatabaseMessageHandler implements MessageSubmitter, MessageRetriever, MessagePuller, MessageExtService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(DatabaseMessageHandler.class);

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public Submission downloadMessage(final String messageId) {
        return new Submission();
    }

    @Override
    public Submission browseMessage(String messageId) {
        return new Submission();
    }


    @Override
    public MessageStatus getStatus(final String messageId) {

        return eu.domibus.common.MessageStatus.RECEIVED;
    }


    @Override
    public List<? extends ErrorResult> getErrorsForMessage(final String messageId) {
        return new ArrayList<>();
    }


    @Override
    @Transactional
    @MDCKey(DomibusLogger.MDC_MESSAGE_ID)
    public String submit(final Submission messageData, final String backendName) throws MessagingProcessingException {
        return UUID.randomUUID().toString();
    }

    @Override
    public void initiatePull(String s) {

    }

    @Override
    public String cleanMessageIdentifier(String s) throws MessageExtException {
        return null;
    }
}

package eu.europa.ec.etrustex.adapter.integration.handlers;

import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter.NodeDocumentWrapperServiceAdapterTest;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.apache.commons.lang3.RandomUtils;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.UUID;

import static org.hamcrest.core.Is.is;

/**
 * @author François Gautier
 * @version 1.0
 * @since 17-Jan-18
 */
@RunWith(JMockit.class)
public class InboxBundleTransmissionFaultHandlerTest {


    @Injectable
    private PostProcessProducer postProcessProducer;

    @Injectable
    private MessageServicePlugin messageService;

    @Tested
    private InboxBundleTransmissionFaultHandler inboxBundleTransmissionFaultHandler;

    private ETrustExAdapterDTO eTrustExAdapterDTO;
    private EtxAttachment etxAttachment;
    private EtxMessage etxMessage;
    private String payload;

    @Before
    public void setUp() {
        eTrustExAdapterDTO = new ETrustExAdapterDTO();
        eTrustExAdapterDTO.setAs4RefToMessageId(UUID.randomUUID().toString());
        etxMessage = new EtxMessage();
        etxMessage.setId(RandomUtils.nextLong(0, 99999));
        etxMessage.setMessageUuid(UUID.randomUUID().toString());
        etxAttachment = NodeDocumentWrapperServiceAdapterTest.formValidEtxAttachmentForStoreDocumentWrapper();
        etxMessage.addAttachment(etxAttachment);
        payload = UUID.randomUUID().toString();
    }

    @Test
    public void process() {
        new Expectations() {{
            messageService.findOrCreateMessageByDomibusMessageId(eTrustExAdapterDTO);
            times = 1;
            result = etxMessage;
        }};

        inboxBundleTransmissionFaultHandler.process(payload, eTrustExAdapterDTO);

        new FullVerifications() {{
            messageService.updateMessage(etxMessage);
            times = 1;

            postProcessProducer.triggerPostProcess(etxMessage.getId());
            times = 1;
        }};

        Assert.assertThat(etxMessage.getMessageState(), is(MessageState.MS_FAILED));
        Assert.assertThat(etxMessage.getError().getResponseCode(), is(ETrustExError.DEFAULT.getCode()));
        Assert.assertThat(etxMessage.getError().getDetail(), CoreMatchers.containsString(payload));

        Assert.assertThat(etxAttachment.getStateType(), is(AttachmentState.ATTS_FAILED));
        Assert.assertThat(etxAttachment.getError().getResponseCode(), is(ETrustExError.DEFAULT.getCode()));
        Assert.assertThat(etxAttachment.getError().getDetail(), CoreMatchers.containsString(etxMessage.getMessageUuid()));

    }

    @Test
    public void process_noEtxMessage() {
        new Expectations() {{
            messageService.findOrCreateMessageByDomibusMessageId(eTrustExAdapterDTO);
            times = 1;
            result = null;
        }};

        inboxBundleTransmissionFaultHandler.process(payload, eTrustExAdapterDTO);

        new FullVerifications() {{
        }};

    }
}
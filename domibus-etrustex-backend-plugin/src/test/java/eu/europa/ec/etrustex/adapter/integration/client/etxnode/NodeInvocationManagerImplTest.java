package eu.europa.ec.etrustex.adapter.integration.client.etxnode;

import eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter.NodeApplicationResponseServicesAdapter;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter.NodeDocumentBundleServiceAdapter;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter.NodeDocumentWrapperServiceAdapter;
import eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter.NodeRetrieveInterchangeAgreementsServiceAdapter;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;

/**
 * JUnit class for <code>NodeInvocationManagerImpl</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@RunWith(JMockit.class)
public class NodeInvocationManagerImplTest {

    @Tested
    private NodeInvocationManagerImpl nodeInvocationManager;

    @Injectable
    private NodeDocumentBundleServiceAdapter nodeDocumentBundleServiceAdapter;

    @Injectable
    private NodeApplicationResponseServicesAdapter nodeApplicationResponseServiceAdapter;

    @Injectable
    private NodeDocumentWrapperServiceAdapter nodeDocumentWrapperServiceAdapter;

    @Injectable
    private NodeRetrieveInterchangeAgreementsServiceAdapter nodeRetrieveInterchangeAgreementsServiceAdapter;

    @Test
    public void testSendMessageBundle_HappyFlow() {

        final EtxMessage etxMessage = new EtxMessage();
        nodeInvocationManager.sendMessageBundle(etxMessage);

        new Verifications() {{
            EtxMessage extEtxMessageCaptured;

            nodeDocumentBundleServiceAdapter.sendMessageBundle(extEtxMessageCaptured = withCapture());
            times = 1;
            Assert.assertEquals(etxMessage, extEtxMessageCaptured);
        }};
    }

    @Test
    public void testSendMessageStatus_HappyFlow() {

        final EtxMessage etxMessage = new EtxMessage();
        nodeInvocationManager.sendMessageStatus(etxMessage);

        new Verifications() {{
            EtxMessage extEtxMessageCaptured;

            nodeApplicationResponseServiceAdapter.sendMessageStatus(extEtxMessageCaptured = withCapture());
            times = 1;
            Assert.assertEquals(etxMessage, extEtxMessageCaptured);
        }};
    }

    @Test
    public void testUploadAttachment_SuccessfulInvocationOfDocumentWrapperAdapter() {

        final EtxAttachment etxAttachment = new EtxAttachment();
        final File file = new File("/tmp/test");

        nodeInvocationManager.uploadAttachment(etxAttachment, file);

        new Verifications() {{
            EtxAttachment etxAttachmentCaptured;
            File fileCaptured;

            nodeDocumentWrapperServiceAdapter.uploadAttachment(etxAttachmentCaptured = withCapture(),
                    fileCaptured = withCapture());

            times = 1;
            Assert.assertEquals(etxAttachment, etxAttachmentCaptured);
            Assert.assertEquals(file, fileCaptured);
        }};
    }

    @Test
    public void testSendInterchangeAgreementRequest_HappyFlow() {
        final EtxParty senderParty = new EtxParty();
        final String receiverPartyUuid = "RECEIVER_TEST_UUID";
        final String etxBackendMessageId = "TEST_ID";

        new Expectations() {{
            nodeRetrieveInterchangeAgreementsServiceAdapter.sendInterchangeAgreementRequest(senderParty);
            result = etxBackendMessageId;

        }};

        String result = nodeInvocationManager.sendInterchangeAgreementRequest(senderParty);
        Assert.assertEquals(etxBackendMessageId, result);

    }
}
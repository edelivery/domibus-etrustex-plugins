package eu.europa.ec.etrustex.adapter.persistence.api.attachment;

import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxError;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxErrorBuilder;
import eu.europa.ec.etrustex.adapter.persistence.api.AbstractDaoIT;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBase;
import eu.europa.ec.etrustex.adapter.persistence.api.common.ErrorDao;
import eu.europa.ec.etrustex.adapter.persistence.api.message.MessageDao;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentDirection.OUTGOING;
import static eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState.*;
import static eu.europa.ec.etrustex.common.model.entity.attachment.AttachmentType.BINARY;
import static java.util.Arrays.asList;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 29-Jan-18
 */
public class AttachmentDaoIT extends AbstractDaoIT<EtxAttachment> {

    private static final Date DATE_2018_01_04 = Date.from(LocalDate.parse("2018-01-04")
            .atStartOfDay()
            .atZone(ZoneId.systemDefault())
            .toInstant());
    private static final String ATTACHMENT_UUID_ACTIVE = "FGA_f795f693-3ee2-4153-8c1a-55a6edb37930_01";
    private static final String ATTACHMENT_UUID_INACTIVE = "FGA_f795f693-3ee2-4153-8c1a-55a6edb37930_07";

    @Autowired
    private AttachmentDao attachmentDao;

    @Autowired
    private ErrorDao errorDao;

    @PersistenceContext(unitName = "domibusJTA")
    private EntityManager entityManager;

    @Autowired
    private MessageDao messageDao;

    @Test
    public void getNextAttachmentsToExpire_ok() {
        List<EtxAttachment> index0 = attachmentDao.getNextAttachmentsToExpire(DATE_2018_01_04, 2, 0);
        assertThat(index0.size(), is(2));
        List<EtxAttachment> index1 = attachmentDao.getNextAttachmentsToExpire(DATE_2018_01_04, 2, 1);
        assertThat(index1.size(), is(1));
        List<EtxAttachment> index2 = attachmentDao.getNextAttachmentsToExpire(DATE_2018_01_04, 2, 2);
        assertThat(index2.size(), is(0));
    }

    @Test
    @Ignore("EDELIVERY-10357")
    public void updateStuckAttachments() {
        LocalDate parse = LocalDate.parse("2000-01-04");

        EtxError error = EtxErrorBuilder.getInstance().withResponseCode(ETrustExError.ETX_010.getCode())
                .withDetail("TEST")
                .build();
        errorDao.save(error);
        entityManager.flush();

        int i = attachmentDao.updateStuckAttachments(parse.atStartOfDay(), LocalDateTime.now().plusDays(1), error);

        assertEquals(6, i);
    }

    @Test
    public void countNextAttachmentsToExpire_ok() {
        Long result = attachmentDao.countNextAttachmentsToExpire(DATE_2018_01_04);
        assertThat(result, is(3L));
    }

    @Test
    public void countNextAttachmentsToExpire_nothing() {
        Long result = attachmentDao.countNextAttachmentsToExpire(Date.from(LocalDate.parse("2017-01-04").atStartOfDay(ZoneId.systemDefault()).toInstant()));
        assertThat(result, is(0L));
    }

    @Test
    public void findMessageByDomibusMessageId_ok() {
        EtxAttachment result = attachmentDao.findAttachmentByDomibusMessageId("b3f36a7e-7077-40ae-8eed-d2af13f22f26@domibus.eu");
        assertThat(result, is(notNullValue()));
    }
    @Test
    public void findMessageByDomibusMessageId_notFound() {
        EtxAttachment result = attachmentDao.findAttachmentByDomibusMessageId("NOT_FOUND");
        assertThat(result, is(nullValue()));
    }

    @Test
    public void countMessageAttachmentsNotInStateList() {
        Long count = attachmentDao.countMessageAttachmentsNotInStateList(10L, asList(ATTS_CREATED, ATTS_FAILED));
        assertThat(count, is(4L));
    }

    @Test
    public void countMessageAttachmentsInStateList() {
        Long count = attachmentDao.countMessageAttachmentsInStateList(10L, asList(ATTS_CREATED, ATTS_FAILED));
        assertThat(count, is(2L));
    }

    @Test
    public void countAttachmentsBySystemAndUUIDAndState_found() {
        Long count = attachmentDao.countAttachmentsBySystemAndUUIDAndState(
                "FGA_f795f693-3ee2-4153-8c1a-55a6edb37930_01",
                asList(ATTS_CREATED, ATTS_FAILED),
                101L);
        assertThat(count, is(1L));
    }

    @Test
    public void countAttachmentsBySystemAndUUIDAndState_notFound_notActive() {
        Long count = attachmentDao.countAttachmentsBySystemAndUUIDAndState(
                ATTACHMENT_UUID_INACTIVE,
                asList(ATTS_CREATED, ATTS_FAILED),
                101L);
        assertThat(count, is(0L));
    }

    @Test
    public void countAttachmentsBySystemAndUUIDAndState_notFound_wrongSender() {
        Long count = attachmentDao.countAttachmentsBySystemAndUUIDAndState(
                ATTACHMENT_UUID_ACTIVE,
                asList(ATTS_CREATED, ATTS_FAILED),
                102L);
        assertThat(count, is(0L));
    }

    @Test
    public void countAttachmentsBySystemAndUUIDAndState_notFound_wrongState() {
        Long count = attachmentDao.countAttachmentsBySystemAndUUIDAndState(
                ATTACHMENT_UUID_ACTIVE,
                asList(ATTS_CREATED, ATTS_DOWNLOADED),
                101L);
        assertThat(count, is(0L));
    }

    @Test
    public void update() {
        EtxAttachment etxMessage = getDao().findById(getExistingId());
        etxMessage.setAttachmentUuid("TEST");
        getDao().update(etxMessage);
    }

    @Override
    protected Long getExistingId() {
        return 10L;
    }

    @Override
    protected DaoBase<EtxAttachment> getDao() {
        return attachmentDao;
    }

    @Override
    protected EtxAttachment getNewEntity() {
        EtxAttachment etxAttachment = new EtxAttachment();
        etxAttachment.setAttachmentUuid(UUID.randomUUID().toString());
        etxAttachment.setStateType(ATTS_CREATED);
        etxAttachment.setAttachmentType(BINARY);
        etxAttachment.setDirectionType(OUTGOING);
        etxAttachment.setMessage(messageDao.findById(15L));
        etxAttachment.setActiveState(true);
        return etxAttachment;
    }
}
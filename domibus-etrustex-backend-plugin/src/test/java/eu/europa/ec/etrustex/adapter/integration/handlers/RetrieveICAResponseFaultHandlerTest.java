package eu.europa.ec.etrustex.adapter.integration.handlers;

import eu.europa.ec.etrustex.adapter.testutils.ETrustExAdapterDTOBuilder;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import mockit.FullVerifications;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_FAULT;
import static eu.europa.ec.etrustex.adapter.testutils.ResourcesUtils.readResource;

/**
 * @Author François Gautier
 * @Since 26-Sep-17
 * @Version 1.0
 */
@RunWith(JMockit.class)
public class RetrieveICAResponseFaultHandlerTest {

    @Tested
    private RetrieveICAResponseFaultHandler retrieveICAResponseFaultHandler;

    @Test
    public void testHandle_HappyFlow() throws Exception {

        //object is build by helper method
        final ETrustExAdapterDTO eTrustExAdapterResponseDTO = ETrustExAdapterDTOBuilder.buildETxAdapterDTOResponse(CONTENT_FAULT, readResource("data/SubmitDocumentBundleResponse.xml"));

        //method to test
        retrieveICAResponseFaultHandler.handle(eTrustExAdapterResponseDTO);

        new FullVerifications() {{

        }};
    }
    public static String getXml() {
        try {
            return readResource("data/jmsCall.xml");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
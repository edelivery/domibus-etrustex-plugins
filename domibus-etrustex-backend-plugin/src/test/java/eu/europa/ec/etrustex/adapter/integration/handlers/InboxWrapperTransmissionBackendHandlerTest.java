package eu.europa.ec.etrustex.adapter.integration.handlers;

import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.handlers.Action;
import eu.europa.ec.etrustex.common.handlers.Service;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.LargePayloadDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.service.WorkspaceService;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystemConfig;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.AttachmentState;
import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.queue.inbox.uploadattachment.InboxUploadAttachmentProducer;
import eu.europa.ec.etrustex.adapter.testutils.JsonUtils;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.activation.DataHandler;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.UUID;

import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_ID_DOCUMENT;
import static eu.europa.ec.etrustex.adapter.model.entity.administration.SystemConfigurationProperty.ETX_BACKEND_DOWNLOAD_ATTACHMENTS;
import static eu.europa.ec.etrustex.adapter.testutils.ETrustExAdapterDTOBuilder.buildETxAdapterDTOResponseWithActionService;
import static eu.europa.ec.etrustex.adapter.testutils.ResourcesUtils.readResource;
import static java.util.Collections.singletonList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author François Gautier
 * @version 1.0
 * @since 07-Dec-17
 */
@RunWith(JMockit.class)
public class InboxWrapperTransmissionBackendHandlerTest {

    private static final String MESSAGE_DOMIBUS_ID = UUID.randomUUID().toString();
    private static final String MIME_TYPE = "mime";
    @Injectable
    private InboxUploadAttachmentProducer inboxUploadAttachmentProducer;
    @Injectable
    private WorkspaceService etxBackendPluginWorkspaceService;
    @Injectable
    private MessageServicePlugin messageService;

    @Tested
    private InboxWrapperTransmissionBackendHandler inboxWrapperTransmissionBackendHandler;
    private ETrustExAdapterDTO dataTransferObject;
    private EtxMessage etxMessage;

    @Before
    public void setUp() throws Exception {
        dataTransferObject = buildETxAdapterDTOResponseWithActionService(CONTENT_ID_DOCUMENT, "", Action.TRANSMIT_WRAPPER_TO_BACKEND.getCode(), Service.WRAPPER_TRANSMISSION_SERVICE.getCode());
        dataTransferObject.setAs4RefToMessageId(MESSAGE_DOMIBUS_ID);

        etxMessage = JsonUtils.getObjectMapper().readValue(
                readResource("data/EtxMessageBundle_IN_Valid_OneAttachment.json"),
                EtxMessage.class);
    }

    @Test
    public void handle_ok() throws Exception {
        final EtxAttachment etxAttachment = etxMessage.getAttachmentList().get(0);
        dataTransferObject.setAttachmentId(etxAttachment.getAttachmentUuid());
        final PayloadDTO largePayloadDTO = new LargePayloadDTO(CONTENT_ID_DOCUMENT.getValue(), MIME_TYPE, new DataHandler(new ByteArrayDataSource("payload", "txt")), "description", Locale.getDefault(), "filename");
        dataTransferObject.addAs4Payload(largePayloadDTO);
        etxMessage.getReceiver().getSystem().setSystemConfigList(singletonList(getEtxSystemConfig()));
        new Expectations() {{
            messageService.findMessageByDomibusMessageId(MESSAGE_DOMIBUS_ID);
            times = 1;
            result = etxMessage;
        }};

        inboxWrapperTransmissionBackendHandler.handle(dataTransferObject);

        new FullVerifications() {{
            etxBackendPluginWorkspaceService.writeFile(etxAttachment.getId(), (InputStream) any);
            times = 1;

            inboxUploadAttachmentProducer.triggerUpload(etxAttachment.getId());
            times = 1;
        }};

        assertThat(etxAttachment.getDomibusMessageId(), is(dataTransferObject.getAs4MessageId()));
        assertThat(etxAttachment.getStateType(), is(AttachmentState.ATTS_DOWNLOADED));
        assertThat(etxAttachment.getMimeType(), is(MIME_TYPE));
    }

    @Test
    public void handle_writeFileIoException() throws Exception {
        final EtxAttachment etxAttachment = etxMessage.getAttachmentList().get(0);
        dataTransferObject.setAttachmentId(etxAttachment.getAttachmentUuid());
        final PayloadDTO largePayloadDTO = new LargePayloadDTO(CONTENT_ID_DOCUMENT.getValue(), MIME_TYPE, new DataHandler(new ByteArrayDataSource("payload", "txt")), "description", Locale.getDefault(), "filename");
        dataTransferObject.addAs4Payload(largePayloadDTO);
        etxMessage.getReceiver().getSystem().setSystemConfigList(singletonList(getEtxSystemConfig()));
        new Expectations() {{
            messageService.findMessageByDomibusMessageId(MESSAGE_DOMIBUS_ID);
            times = 1;
            result = etxMessage;

            etxBackendPluginWorkspaceService.writeFile(etxAttachment.getId(), (InputStream) any);
            times = 1;
            result = new IOException();
        }};

        try {
            inboxWrapperTransmissionBackendHandler.handle(dataTransferObject);
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getError(), is(ETrustExError.DEFAULT));
        }
        new FullVerifications() {{
        }};

    }

    @Test
    public void handle_NoPayload() {
        dataTransferObject.setAttachmentId(etxMessage.getAttachmentList().get(0).getAttachmentUuid());
        etxMessage.getReceiver().getSystem().setSystemConfigList(singletonList(getEtxSystemConfig()));
        new Expectations() {{
            messageService.findMessageByDomibusMessageId(MESSAGE_DOMIBUS_ID);
            times = 1;
            result = etxMessage;
        }};

        try {
            inboxWrapperTransmissionBackendHandler.handle(dataTransferObject);
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getError(), is(ETrustExError.DEFAULT));
        }

        new FullVerifications() {{
        }};

    }

    @Test
    public void handle_attachmentNotFound() {
        dataTransferObject.setAttachmentId(UUID.randomUUID().toString());
        etxMessage.getReceiver().getSystem().setSystemConfigList(singletonList(getEtxSystemConfig()));
        new Expectations() {{
            messageService.findMessageByDomibusMessageId(MESSAGE_DOMIBUS_ID);
            times = 1;
            result = etxMessage;
        }};

        try {
            inboxWrapperTransmissionBackendHandler.handle(dataTransferObject);
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getError(), is(ETrustExError.DEFAULT));
        }

        new FullVerifications() {{
        }};

    }

    @Test
    public void handle_noAttachmentId() {
        etxMessage.getReceiver().getSystem().setSystemConfigList(singletonList(getEtxSystemConfig()));
        new Expectations() {{
            messageService.findMessageByDomibusMessageId(MESSAGE_DOMIBUS_ID);
            times = 1;
            result = etxMessage;
        }};
        try {
            inboxWrapperTransmissionBackendHandler.handle(dataTransferObject);
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getError(), is(ETrustExError.DEFAULT));
        }
        new FullVerifications() {{

        }};

    }

    private EtxSystemConfig getEtxSystemConfig() {
        EtxSystemConfig etxSystemConfig = new EtxSystemConfig();
        etxSystemConfig.setSystemConfigurationProperty(ETX_BACKEND_DOWNLOAD_ATTACHMENTS);
        etxSystemConfig.setPropertyValue("true");
        return etxSystemConfig;
    }

    @Test
    public void handle_handleIgnoreAttachments() {
        new Expectations() {{
            messageService.findMessageByDomibusMessageId(MESSAGE_DOMIBUS_ID);
            times = 1;
            result = etxMessage;
        }};

        inboxWrapperTransmissionBackendHandler.handle(dataTransferObject);

        new FullVerifications() {{
        }};

    }

    @Test
    public void handle_noEtxMessageFound() {
        new Expectations() {{
            messageService.findMessageByDomibusMessageId(MESSAGE_DOMIBUS_ID);
            times = 1;
            result = null;
        }};
        try {
            inboxWrapperTransmissionBackendHandler.handle(dataTransferObject);
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getError(), is(ETrustExError.DEFAULT));
        }

        new FullVerifications() {{
        }};
    }
}
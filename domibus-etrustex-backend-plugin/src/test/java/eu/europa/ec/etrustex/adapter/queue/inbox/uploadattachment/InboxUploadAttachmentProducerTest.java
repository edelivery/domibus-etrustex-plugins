package eu.europa.ec.etrustex.adapter.queue.inbox.uploadattachment;

import eu.europa.ec.etrustex.adapter.queue.JmsProducer;
import eu.europa.ec.etrustex.common.jms.TextMessageConverter;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
//import weblogic.jndi.WLContext;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Hashtable;

import static org.hamcrest.core.Is.is;

/**
 * @author Francois GAUTIER
 * @version 1.0
 * @since Oct 2017
 */
@RunWith(JMockit.class)
public class InboxUploadAttachmentProducerTest {

    @Tested
    private InboxUploadAttachmentProducer producer;

    @Injectable
    private Queue inboxUploadAttachment;

    @Injectable
    private JmsProducer<InboxUploadAttachmentQueueMessage> jmsProducer;

    @Before
    public void setUp() {
        producer = new InboxUploadAttachmentProducer(inboxUploadAttachment, jmsProducer);
    }

    @Test
    public void testTriggerUploadPostCorrectMessageWithValidAttachmentId() {
        final Long attachmentId = 7L;

        producer.triggerUpload(attachmentId);

        final InboxUploadAttachmentQueueMessage expectedQM = new InboxUploadAttachmentQueueMessage();
        expectedQM.setAttachmentId(attachmentId);

        new FullVerifications(){{
            InboxUploadAttachmentQueueMessage queueMessage;
            jmsProducer.postMessage(inboxUploadAttachment, queueMessage = withCapture());
            times = 1;

            Assert.assertThat(queueMessage.getAttachmentId(), is(attachmentId));
        }};
    }


    public final static String JNDI_FACTORY = "weblogic.jndi.WLInitialContextFactory";
    public final static String JMS_FACTORY = "jms/ConnectionFactory";
    public final static String QUEUE = "jms/eTrustExBackendPlugin/in/inUploadAttachmentJMSQueue";
    //For Manual testing queue message
    @Ignore
    @Test
    public void testProduceSampleMessageInRealQueue() throws NamingException, JMSException, InterruptedException {
        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_FACTORY);
        env.put(Context.PROVIDER_URL, "t3://localhost:10001,localhost:10002");
        //env.put(WLContext.REPLICATE_BINDINGS, "false");
        InitialContext ic = new InitialContext(env);

        TextMessageConverter converter = new TextMessageConverter();
        converter.setDomain("default");
        long[] attachmentIds = {60900252, 60900253, 60900254, 60900255, 60900256, 60900257, 60900258, 60900259, 60900260, 60900261, 60900262, 60900263, 60900264, 60900265, 60900266, 60900267, 60900268, 60900269, 60900270, 60900271, 60900272, 60900273, 60900274, 60900275, 60900276, 60900277, 60900278, 60900279, 60900280, 60900281, 60900282, 60900283, 60900284, 60900285, 60900286, 60900287, 60900288, 60900289, 60900290, 60900291, 60900292, 60900293, 60900294, 60900295, 60900296, 60900297, 60900298, 60900299, 60900300, 60900301, 60900302, 60900303, 60900304, 60900305, 60900306, 60900307, 60900308, 60900309, 60900310, 60900311, 60900312, 60900313, 60900314, 60900315, 60900316, 60900317, 60900318, 60900319, 60900320, 60900321, 60900322, 60900323, 60900324, 60900325, 60900326, 60900327, 60900328, 60900329, 60900330, 60900331, 60900332, 60900333, 60900334, 60900335, 60900336, 60900337, 60900338, 60900339, 60900340, 60900341, 60900342, 60900343, 60900344, 60900345, 60900346, 60900347, 60900348};

        for(int i=0; i<97; i++){
            Thread.sleep(10);
            QueueConnectionFactory qcf = (QueueConnectionFactory) ic.lookup(JMS_FACTORY);
            Queue inUploadAttachmentJMSQueue = (Queue) ic.lookup(QUEUE);
            QueueConnection queueConnection = qcf.createQueueConnection();
            queueConnection.start();
            Session queueSession = queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = queueSession.createProducer(inUploadAttachmentJMSQueue);

            InboxUploadAttachmentQueueMessage inboxUploadAttachmentQueueMessage = new InboxUploadAttachmentQueueMessage();
            inboxUploadAttachmentQueueMessage.setAttachmentId(attachmentIds[i]);

            Message message = converter.toMessage(inboxUploadAttachmentQueueMessage, queueSession);
            System.out.println("Triggering Queue Message no:" + i + " :" + message);

            messageProducer.send(message);
            //System.out.println("TextMessage sent to queue:" + inUploadAttachmentJMSQueue.getQueueName());
            messageProducer.close();
            queueSession.close();
            queueConnection.stop();
            queueConnection.close();
        }
        ic.close();

    }
}

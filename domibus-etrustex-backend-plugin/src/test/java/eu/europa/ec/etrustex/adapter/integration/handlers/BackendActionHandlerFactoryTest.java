package eu.europa.ec.etrustex.adapter.integration.handlers;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.handlers.Action;
import eu.europa.ec.etrustex.common.handlers.OperationHandler;
import eu.europa.ec.etrustex.common.handlers.Service;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.context.ApplicationContext;

import java.util.Arrays;
import java.util.Collection;

import static eu.europa.ec.etrustex.common.handlers.Action.UNKNOWN;
import static eu.europa.ec.etrustex.common.handlers.Action.*;
import static eu.europa.ec.etrustex.common.handlers.Service.*;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.runners.Parameterized.Parameter;

/**
 * @Author François Gautier
 * @Since 26-Sep-17
 * @Version
 */
@SuppressWarnings("Duplicates")
@RunWith(Parameterized.class)
public class BackendActionHandlerFactoryTest {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(BackendActionHandlerFactoryTest.class);

    @Injectable
    private ApplicationContext context;
    @Tested
    private BackendActionHandlerFactory backendActionHandlerFactory;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Parameter
    public Service service;
    @Parameter(1)
    public Action action;
    @Parameter(2)
    public Class<? extends OperationHandler> clazz;
    @Parameter(3)
    public Class<? extends Exception> expectedException;

    @Parameters( name = "{index}: {0} | {1}" )
    public static Collection combinations() {
        return Arrays.asList(new Object[][]{
                // Happy flow scenario
                {APPLICATION_RESPONSE_SERVICE, SUBMIT_APPLICATION_RESPONSE_RESPONSE, SubmitApplicationResponseResponseHandler.class, null},
                {APPLICATION_RESPONSE_SERVICE, FAULT_ACTION, ResponseFaultHandler.class, null},
                {DOCUMENT_WRAPPER_SERVICE, STORE_DOCUMENT_WRAPPER_RESPONSE, StoreDocumentWrapperResponseHandler.class, null},
                {DOCUMENT_WRAPPER_SERVICE, FAULT_ACTION, StoreDocumentWrapperResponseFaultHandler.class, null},
                {DOCUMENT_BUNDLE_SERVICE, SUBMIT_DOCUMENT_BUNDLE_RESPONSE, SubmitDocumentBundleResponseHandler.class, null},
                {DOCUMENT_BUNDLE_SERVICE, FAULT_ACTION, ResponseFaultHandler.class, null},
                {RETRIEVE_ICA_SERVICE, RETRIEVE_ICA_RESPONSE, RetrieveICAResponseHandler.class, null},
                {RETRIEVE_ICA_SERVICE, FAULT_ACTION, RetrieveICAResponseFaultHandler.class, null},
                {WRAPPER_TRANSMISSION_SERVICE, TRANSMIT_WRAPPER_TO_BACKEND, InboxWrapperTransmissionBackendHandler.class, null},
                {BUNDLE_TRANSMISSION_SERVICE, NOTIFY_BUNDLE_TO_BACKEND_REQUEST, InboxBundleTransmissionBackendHandler.class, null},
                {BUNDLE_TRANSMISSION_SERVICE, FAULT_ACTION, InboxBundleTransmissionFaultHandler.class, null},
                {STATUS_MESSAGE_TRANSMISSION_SERVICE, STATUS_MESSAGE_TRANSMISSION, InboxMessageStatusTransmissionBackendHandler.class, null},
                // exception
                {APPLICATION_RESPONSE_SERVICE, UNKNOWN, OperationHandler.class, UnsupportedOperationException.class},
                {DOCUMENT_WRAPPER_SERVICE, UNKNOWN, OperationHandler.class, UnsupportedOperationException.class},
                {DOCUMENT_BUNDLE_SERVICE, UNKNOWN, OperationHandler.class, UnsupportedOperationException.class},
                {RETRIEVE_ICA_SERVICE, UNKNOWN, OperationHandler.class, UnsupportedOperationException.class},
                {WRAPPER_TRANSMISSION_SERVICE, UNKNOWN, OperationHandler.class, UnsupportedOperationException.class},
                {BUNDLE_TRANSMISSION_SERVICE, UNKNOWN, OperationHandler.class, UnsupportedOperationException.class},
                {STATUS_MESSAGE_TRANSMISSION_SERVICE, UNKNOWN, OperationHandler.class, UnsupportedOperationException.class},

                {Service.UNKNOWN, UNKNOWN, OperationHandler.class, UnsupportedOperationException.class},
                //exception
                {null, UNKNOWN, OperationHandler.class, NullPointerException.class},
                {Service.UNKNOWN, null, OperationHandler.class, UnsupportedOperationException.class},
        });
    }

    @Test
    public void getOperator() throws Exception {
        LOG.info("Parametrized Number is : " + service + " " + action + " " + clazz);

        if (clazz != null && clazz != OperationHandler.class) {
            new Expectations() {{
                context.getBean(clazz);
                times = 1;
                result = clazz.newInstance();
            }};
        }
        if (expectedException != null) {
            thrown.expect(expectedException);
            if(expectedException == UnsupportedOperationException.class) {
                thrown.expectMessage(CoreMatchers.containsString("Operation Unsupported for Service:[" + service + "] Action:[" + action + "]"));
            }
        }

        OperationHandler operationHandler = backendActionHandlerFactory.getOperationHandler(service, action);

        new FullVerifications() {{
        }};

        if (clazz != null) {
            assertThat(operationHandler, notNullValue());
        }
    }
}
package eu.europa.ec.etrustex.adapter.testutils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.joda.JodaModule;

/**
 * @author François Gautier
 * @version 1.0
 * @since 06-Nov-17
 */
public class JsonUtils {

    private JsonUtils() {
    }

    public static ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JodaModule());
        // We need to add a custom deserializer so the json file holds the hexadecimal string
        SimpleModule module = new SimpleModule();
        module.addDeserializer(byte[].class, new ByteArrayDeserializer());
        objectMapper.registerModule(module);
        return objectMapper;
    }

}

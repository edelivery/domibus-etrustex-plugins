package eu.europa.ec.etrustex.adapter.persistence.api;

import eu.europa.ec.etrustex.adapter.AbstractSpringContextIT;
import eu.europa.ec.etrustex.adapter.model.entity.AbstractEntity;
import org.hamcrest.core.IsCollectionContaining;
import org.junit.After;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by François Gautier on 02/02/2016.
 * <p>
 * Extend this class to test a DAO with CRUD methods.
 */
@Transactional
public abstract class AbstractDaoIT<T extends AbstractEntity<Long>> extends AbstractSpringContextIT {
    @PersistenceContext(unitName = "domibusJTA", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    @After
    public void tearDown() {
        entityManager.flush();
    }

    @Test
    public void save() {
        T newEntity = getNewEntity();
        getDao().save(newEntity);
        assertThat(newEntity.getId(), is(notNullValue()));

        getDao().remove(newEntity);
    }

    @Test
    public void findById() {
        T byId = getDao().findById(getExistingId());
        assertThat(byId, is(notNullValue()));
    }

    @Test
    public void remove() {
        T newEntity = getNewEntity();
        getDao().save(newEntity);
        entityManager.flush();
        assertThat(newEntity.getId(), is(notNullValue()));

        getDao().remove(newEntity);
        entityManager.flush();
        T byId = getDao().findById(newEntity.getId());
        assertThat(byId, is(nullValue()));
    }

    @Test
    public void findAll() {
        List<T> byId = getDao().findAll();
        assertThat(byId, is(notNullValue()));
        assertThat(byId, IsCollectionContaining.hasItem(getDao().findById(getExistingId())));
    }

    @Test
    @Transactional
    public void update() {
        T newEntity = getNewEntity();
        newEntity.setId(getExistingId());
        T updated = getDao().update(newEntity);
        assertThat(updated, is(notNullValue()));
    }

    /**
     * To implement this method, find a valid entity in the file
     * test/resources/data/database/base_test_data.sql
     *
     * @return id of a valid entity
     */
    protected abstract Long getExistingId();

    protected abstract DaoBase<T> getDao();

    protected abstract T getNewEntity();
}

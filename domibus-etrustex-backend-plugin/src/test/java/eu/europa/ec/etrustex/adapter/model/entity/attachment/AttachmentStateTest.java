package eu.europa.ec.etrustex.adapter.model.entity.attachment;

import org.junit.Assert;
import org.junit.Test;

/**
 * JUnit class for <code>AttachmentState</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
public class AttachmentStateTest {

    @Test
    public void testIsFinal_HappyFlow() {

        Assert.assertTrue(AttachmentState.ATTS_FAILED.isFinal());
        Assert.assertFalse(AttachmentState.ATTS_CREATED.isFinal());
        Assert.assertFalse(AttachmentState.ATTS_DOWNLOADED.isFinal());
        Assert.assertTrue(AttachmentState.ATTS_UPLOADED.isFinal());
        Assert.assertTrue(AttachmentState.ATTS_ALREADY_UPLOADED.isFinal());
        Assert.assertTrue(AttachmentState.ATTS_IGNORED.isFinal());

    }

}
package eu.europa.ec.etrustex.adapter.integration.handlers;

import ec.schema.xsd.ack_2.AcknowledgmentType;
import ec.schema.xsd.commonbasiccomponents_1.AckIndicatorType;
import ec.services.wsdl.documentbundle_2.FaultResponse;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleResponse;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import eu.europa.ec.etrustex.adapter.model.entity.common.EtxError;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.queue.postprocess.PostProcessProducer;
import eu.europa.ec.etrustex.adapter.service.MessageServicePlugin;
import mockit.*;
import mockit.integration.junit4.JMockit;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseCodeType;
import oasis.names.specification.ubl.schema.xsd.fault_1.FaultType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_ID_GENERIC;
import static eu.europa.ec.etrustex.adapter.testutils.ETrustExAdapterDTOBuilder.buildETxAdapterDTOResponse;
import static eu.europa.ec.etrustex.adapter.testutils.ResourcesUtils.readResource;
import static org.junit.Assert.assertEquals;

/**
 * JUnit class for <code>SubmitDocumentBundleResponseHandler</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@RunWith(JMockit.class)
public class SubmitDocumentBundleResponseHandlerTest {

    @Injectable
    private MessageServicePlugin messageService;

    @Injectable
    private PostProcessProducer postProcessProducer;

    @Tested
    private SubmitDocumentBundleResponseHandler submitDocumentBundleResponseHandler;

    /**
     * Test for Successful handling of ETrustExAdapterDTO response
     */
    @Test
    public void testHandle_HappyFlow() {
        //object is build by helper method
        final ETrustExAdapterDTO eTrustExAdapterResponseDTO =
                buildETxAdapterDTOResponse(
                        CONTENT_ID_GENERIC,
                        readResource("data/SubmitDocumentBundleResponse.xml"));

        final Long messageId = 10L;
        final String messageUuid = "message test UUID";
        final EtxMessage etxMessage = new EtxMessage();
        etxMessage.setId(messageId);
        etxMessage.setMessageUuid(messageUuid);

        new Expectations() {{
            messageService.findMessageByDomibusMessageId(eTrustExAdapterResponseDTO.getAs4RefToMessageId());
            times = 1;
            result = etxMessage;

        }};

        //method to test
        submitDocumentBundleResponseHandler.handle(eTrustExAdapterResponseDTO);

        new FullVerifications() {{
            Long messageIdActual;
            postProcessProducer.triggerPostProcess(messageIdActual = withCapture());
            times = 1;
            assertEquals("message id must match", messageId, messageIdActual);
        }};
    }

    /**
     * Test scenario for Exception thrown when SubmitDocumentBundleResponse and
     * FaultResponse objects are both null
     */
    @Test
    public void testHandle_ExceptionSubmitDocBundleFaultResponseMissing() {

        //extract method will return null
        new MockUp<TransformerUtils>() {
            @Mock
            <T> T extractETrustExXMLPayload(ContentId requestContentId, ETrustExAdapterDTO eTrustExAdapterDTO) {
                return null;
            }
        };

        //object is build by helper method
        final ETrustExAdapterDTO eTrustExAdapterResponseDTO = new ETrustExAdapterDTO();

        final Long messageId = 10L;
        final String messageUuid = "message test UUID";
        final EtxMessage etxMessage = new EtxMessage();
        etxMessage.setId(messageId);
        etxMessage.setMessageUuid(messageUuid);

        try {
            //method to test
            submitDocumentBundleResponseHandler.handle(eTrustExAdapterResponseDTO);
            Assert.fail();
        } catch (ETrustExPluginException e) {
            assertEquals("exception error code must match", ETrustExError.ETX_002, e.getError());
            assertEquals("exception message must match", "Either SubmitDocumentBundleResponse or FaultResponse should be provided!", e.getMessage());
        }

        new FullVerifications() {{
        }};
    }

    /**
     * Test scenario for successful processing of node fault
     */
    @Test
    public void testProcessNodeFault_HappyFlow() {

        final String errorDescription = "error description test";
        FaultType faultInfo = new FaultType();
        ResponseCodeType responseCode = new ResponseCodeType();
        final String faultResponseCode = "fault response code test";
        responseCode.setValue(faultResponseCode);
        faultInfo.setResponseCode(responseCode);
        final FaultResponse faultResponse = new FaultResponse(errorDescription, faultInfo);

        final EtxMessage etxMessage = new EtxMessage();

        new Expectations(submitDocumentBundleResponseHandler) {{

            submitDocumentBundleResponseHandler.getNodeFaultResponseCode(faultResponse.getFaultInfo());
            times = 1;
            result = faultResponseCode;
        }};

        //method to be tested
        submitDocumentBundleResponseHandler.processNodeFault(faultResponse, etxMessage);

        new FullVerifications() {{
            EtxMessage etxMessageActual;
            messageService.updateMessage(etxMessageActual = withCapture());
            times = 1;
            Assert.assertNotNull("EtxMessage object shouldn't be null", etxMessageActual);
            assertEquals("EtxMessage object should match", etxMessage, etxMessageActual);

        }};
        EtxError etxErrorActual = etxMessage.getError();
        Assert.assertNotNull("EtxError object shouldn't be null", etxErrorActual);
        assertEquals("fault response code should match", faultResponseCode, etxErrorActual.getResponseCode());
        assertEquals("error detail should match", String.format("Sending of Bundle for message with ID %s  returns the fault response code: %s; error details: %s",
                etxMessage.getId(), faultResponseCode, errorDescription), etxErrorActual.getDetail());
    }

    /**
     * Test scenario for successful process node success operation
     */
    @Test
    public void testProcessNodeSuccess_HappyFlow() {

        final EtxMessage etxMessage = new EtxMessage();
        final SubmitDocumentBundleResponse submitDocumentBundleResponse = getSubmitDocumentBundleResponseWithAck();

        //method to be tested
        submitDocumentBundleResponseHandler.processNodeSuccess(submitDocumentBundleResponse, etxMessage);

        new FullVerifications() {{
        }};
    }

    private SubmitDocumentBundleResponse getSubmitDocumentBundleResponseWithAck() {
        final SubmitDocumentBundleResponse submitDocumentBundleResponse = new SubmitDocumentBundleResponse();
        AcknowledgmentType acknowledgmentType = new AcknowledgmentType();
        AckIndicatorType ackIndicatorType = new AckIndicatorType();
        ackIndicatorType.setValue(true);
        acknowledgmentType.setAckIndicator(ackIndicatorType);
        submitDocumentBundleResponse.setAck(acknowledgmentType);
        return submitDocumentBundleResponse;
    }
}
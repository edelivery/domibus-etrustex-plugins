package eu.europa.ec.etrustex.adapter.model.vo;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertSame;
import static org.apache.commons.codec.binary.StringUtils.getBytesUtf8;

/**
 * A utility class which allows for testing entity and transfer object classes. This is mainly for code coverage since
 * these types of objects are normally nothing more than getters and setters. If any logic exists in the method, then
 * the get method name should be sent in as an ignored field and a custom test function should be written.
 *
 * @param <T> The object type to test.
 */
public abstract class AbstractDtoTest<T> {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(AbstractDtoTest.class);

    /**
     * A map of default mappers for common objects.
     */
    private static final Map<Class<?>, Create<?>> DEFAULT_MAPPERS;

    static {
        final Map<Class<?>, Create<?>> mapperBuilder = new HashMap<>();

        /* Primitives */
        mapperBuilder.put(int.class, new Create<Integer>() {
            @Override
            public Integer get() {
                return 0;
            }
        });
        mapperBuilder.put(double.class, new Create<Double>() {
            @Override
            public Double get() {
                return 0.0d;
            }
        });
        mapperBuilder.put(float.class, new Create<Float>() {
            @Override
            public Float get() {
                return 0.0f;
            }
        });
        mapperBuilder.put(long.class, new Create<Long>() {
            @Override
            public Long get() {
                return 0L;
            }
        });
        mapperBuilder.put(boolean.class, new Create<Boolean>() {
            @Override
            public Boolean get() {
                return true;
            }
        });
        mapperBuilder.put(short.class, new Create<Short>() {
            @Override
            public Short get() {
                return (short) 0;
            }
        });
        mapperBuilder.put(byte[].class, new Create<byte[]>() {
            @Override
            public byte[] get() {
                return getBytesUtf8("");
            }
        });
        mapperBuilder.put(byte.class, new Create<Byte>() {
            @Override
            public Byte get() {
                return (byte) 0;
            }
        });
        mapperBuilder.put(char.class, new Create<Character>() {
            @Override
            public Character get() {
                return (char) 0;
            }
        });
        mapperBuilder.put(Integer.class, new Create<Integer>() {
            @Override
            public Integer get() {
                return 0;
            }
        });
        mapperBuilder.put(Double.class, new Create<Double>() {
            @Override
            public Double get() {
                return 0.0;
            }
        });
        mapperBuilder.put(Float.class, new Create<Float>() {
            @Override
            public Float get() {
                return 0.0f;
            }
        });
        mapperBuilder.put(Long.class, new Create<Long>() {
            @Override
            public Long get() {
                return 0L;
            }
        });
        mapperBuilder.put(Boolean.class, new Create<Boolean>() {
            @Override
            public Boolean get() {
                return Boolean.TRUE;
            }
        });
        mapperBuilder.put(Short.class, new Create<Short>() {
            @Override
            public Short get() {
                return (short) 0;
            }
        });
        mapperBuilder.put(Byte.class, new Create<Byte>() {
            @Override
            public Byte get() {
                return (byte) 0;
            }
        });
        mapperBuilder.put(Character.class, new Create<Character>() {
            @Override
            public Character get() {
                return (char) 0;
            }
        });
        mapperBuilder.put(BigDecimal.class, new Create<BigDecimal>() {
            @Override
            public BigDecimal get() {
                return BigDecimal.ONE;
            }
        });
        mapperBuilder.put(Date.class, new Create<Object>() {
            @Override
            public Object get() {
                return new Date();
            }
        });
        mapperBuilder.put(Set.class, new Create<Set>() {
            @Override
            public Set get() {
                return Collections.emptySet();
            }
        });
        mapperBuilder.put(SortedSet.class, new Create<SortedSet>() {
            @Override
            public SortedSet get() {
                return new TreeSet();
            }
        });
        mapperBuilder.put(List.class, new Create<List>() {
            @Override
            public List get() {
                return Collections.emptyList();
            }
        });
        mapperBuilder.put(Collection.class, new Create<Collection>() {
            @Override
            public Collection get() {
                return Collections.emptyList();
            }
        });
        mapperBuilder.put(Map.class, new Create<Map>() {
            @Override
            public Map get() {
                return Collections.emptyMap();
            }
        });
        mapperBuilder.put(SortedMap.class, new Create<SortedMap>() {
            @Override
            public SortedMap get() {
                return new TreeMap();
            }
        });
        mapperBuilder.put(Locale.class, new Create<Locale>() {
            @Override
            public Locale get() {
                return Locale.getDefault();
            }
        });

        DEFAULT_MAPPERS = mapperBuilder;
    }

    /**
     * The get fields to ignore and not try to test.
     */
    private Set<String> ignoredGetFields = new HashSet<>();

    /**
     * A custom mapper. Normally used when the test class has abstract objects.
     */
    private Map<Class<?>, Create<?>> mappers = DEFAULT_MAPPERS;

    AbstractDtoTest() {
        this(null, null);
    }

    /**
     * Creates an instance of {@link AbstractDtoTest} with the default ignore fields.
     *
     * @param customMappers Any custom mappers for a given class type.
     * @param ignoreFields  The getters which should be ignored (e.g., "getId" or "isActive").
     */
    private AbstractDtoTest(Map<Class<?>, Create<?>> customMappers, Set<String> ignoreFields) {
        this.ignoredGetFields = new HashSet<>();
        if (ignoreFields != null) {
            this.ignoredGetFields.addAll(ignoreFields);
        }
        this.ignoredGetFields.add("getClass");
        this.ignoredGetFields.add("getPayloadAsString");
        this.ignoredGetFields.add("getPayloadDataHandler");

        if (customMappers == null) {
            this.mappers = DEFAULT_MAPPERS;
        } else {
            final Map<Class<?>, Create<?>> builder = new HashMap<>();
            builder.putAll(customMappers);
            builder.putAll(DEFAULT_MAPPERS);
            this.mappers = builder;
        }
    }


    /**
     * Returns an instance to use to test the get and set methods.
     *
     * @return An instance to use to test the get and set methods.
     */
    protected abstract T getInstance();

    /**
     * Creates an object for the given {@link Class}.
     *
     * @param fieldName The name of the field.
     * @param clazz     The {@link Class} type to create.
     * @return A new instance for the given {@link Class}.
     */
    private Object createObject(String fieldName, Class<?> clazz) {

        final Create<?> supplier = this.mappers.get(clazz);
        if (supplier != null) {
            return supplier.get();
        }

        if (clazz.isEnum()) {
            return clazz.getEnumConstants()[0];
        }

        try {
            return clazz.newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
            throw new RuntimeException("Unable to create objects for field '" + fieldName + "'.", e);
        }
    }

    /**
     * Tests all the getters and setters. Verifies that when a set method is called, that the get method returns the
     * same thing. This will also use reflection to set the field if no setter exists (mainly used for user immutable
     * entities but Hibernate normally populates).
     *
     * @throws Exception If an unexpected error occurs.
     */
    @Test
    public void testGettersAndSetters() throws Exception {
        /* Sort items for consistent test runs. */
        final SortedMap<String, GetterSetterPair> getterSetterMapping = new TreeMap<>();

        final T instance = getInstance();

        for (final Method method : instance.getClass().getMethods()) {
            final String methodName = method.getName();

            if (this.ignoredGetFields.contains(methodName)) {
                continue;
            }

            String objectName;
            if (methodName.startsWith("get") && method.getGenericParameterTypes().length == 0) {
                /* Found the get method. */
                objectName = methodName.substring("get".length());

                GetterSetterPair getterSettingPair = getterSetterMapping.get(objectName);
                if (getterSettingPair == null) {
                    getterSettingPair = new GetterSetterPair();
                    getterSetterMapping.put(objectName, getterSettingPair);
                }
                getterSettingPair.setGetter(method);
            } else if (methodName.startsWith("set") && method.getGenericParameterTypes().length == 1) {
                /* Found the set method. */
                objectName = methodName.substring("set".length());

                GetterSetterPair getterSettingPair = getterSetterMapping.get(objectName);
                if (getterSettingPair == null) {
                    getterSettingPair = new GetterSetterPair();
                    getterSetterMapping.put(objectName, getterSettingPair);
                }
                getterSettingPair.setSetter(method);
            } else if (methodName.startsWith("is") && method.getGenericParameterTypes().length == 0) {
                /* Found the is method, which really is a get method. */
                objectName = methodName.substring("is".length());

                GetterSetterPair getterSettingPair = getterSetterMapping.get(objectName);
                if (getterSettingPair == null) {
                    getterSettingPair = new GetterSetterPair();
                    getterSetterMapping.put(objectName, getterSettingPair);
                }
                getterSettingPair.setGetter(method);
            }
        }

        /*
         * Found all our mappings. Now call the getter and setter or set the field via reflection and call the getter
         * it doesn't have a setter.
         */
        for (final Map.Entry<String, GetterSetterPair> entry : getterSetterMapping.entrySet()) {
            final GetterSetterPair pair = entry.getValue();

            final String objectName = entry.getKey();
            final String fieldName = objectName.substring(0, 1).toLowerCase() + objectName.substring(1);

            if (pair.hasGetterAndSetter()) {
                /* Create an object. */
                final Class<?> parameterType = pair.getSetter().getParameterTypes()[0];
                final Object newObject = createObject(fieldName, parameterType);

                pair.getSetter().invoke(instance, newObject);

                callGetter(fieldName, pair.getGetter(), instance, newObject);
            } else if (pair.getGetter() != null) {
                /*
                 * Object is immutable (no setter but Hibernate or something else sets it via reflection). Use
                 * reflection to set object and verify that same object is returned when calling the getter.
                 */
                final Object newObject = createObject(fieldName, pair.getGetter().getReturnType());
                final Field field = instance.getClass().getDeclaredField(fieldName);
                field.setAccessible(true);
                field.set(instance, newObject);

                callGetter(fieldName, pair.getGetter(), instance, newObject);
            }
        }
        LOG.debug("toString: " + instance.toString());
        LOG.debug("equals null: " + instance.equals(new Object()));
        LOG.debug("hashcode: " + instance.hashCode());
    }

    /**
     * Calls a getter and verifies the result is what is expected.
     *
     * @param fieldName The field name (used for error messages).
     * @param getter    The get {@link Method}.
     * @param instance  The test instance.
     *                  //     * @param fieldType The type of the return type.
     * @param expected  The expected result.
     */
    private void callGetter(String fieldName, Method getter, T instance, Object expected)
            throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {

        final Object getResult = getter.invoke(instance);

        if (getter.getReturnType().isPrimitive()) {
            /* Calling assetEquals() here due to autoboxing of primitive to object type. */
            assertEquals(fieldName + " is different", expected, getResult);
        } else {
            /* This is a normal object. The object passed in should be the exactly same object we get back. */
            assertSame(fieldName + " is different", expected, getResult);
        }
    }

    abstract static class Create<T> {
        public abstract T get();
    }
}
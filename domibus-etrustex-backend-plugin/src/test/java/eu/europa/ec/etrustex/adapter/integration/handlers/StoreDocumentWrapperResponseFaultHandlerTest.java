package eu.europa.ec.etrustex.adapter.integration.handlers;

import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.queue.postupload.PostUploadProducer;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import eu.europa.ec.etrustex.adapter.testutils.ETrustExAdapterDTOBuilder;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import static eu.europa.ec.etrustex.adapter.testutils.ResourcesUtils.readResource;
import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_FAULT;


/**
 * @Author François Gautier
 * @Since 26-Sep-17
 * @Version
 */
@RunWith(JMockit.class)
public class StoreDocumentWrapperResponseFaultHandlerTest {

    @Injectable
    private AttachmentService attachmentService;

    @Injectable
    private PostUploadProducer postUploadProducer;

    @Tested
    private StoreDocumentWrapperResponseFaultHandler storeDocumentWrapperResponseFaultHandler;

    @Test
    public void testHandle_HappyFlow() {

        //object is build by helper method
        final ETrustExAdapterDTO eTrustExAdapterResponseDTO = ETrustExAdapterDTOBuilder.buildETxAdapterDTOResponse(CONTENT_FAULT, readResource("data/SubmitDocumentBundleResponse.xml"));

        final Long messageId = 10L;
        final String attachementUuid = "attachement test UUID";
        final EtxAttachment etxAttachment = getEtxAttachment(messageId, attachementUuid);

        new Expectations() {{
            attachmentService.findAttachmentByDomibusMessageId(eTrustExAdapterResponseDTO.getAs4RefToMessageId());
            times = 1;
            result = etxAttachment;
        }};

        //method to test
        storeDocumentWrapperResponseFaultHandler.handle(eTrustExAdapterResponseDTO);

        new FullVerifications() {{
            attachmentService.attachmentInError(etxAttachment.getId(), ETrustExError.DEFAULT.getCode(), anyString);
            times = 1;

            EtxAttachment etxAttachmentActual;
            postUploadProducer.checkAttachmentsAndTriggerPostUpload(etxAttachmentActual = withCapture());
            times = 1;
            Assert.assertEquals("Attachment must match", attachementUuid, etxAttachmentActual.getAttachmentUuid());
        }};
    }

    private EtxAttachment getEtxAttachment(Long messageId, String attachementUuid) {
        final EtxAttachment etxMessage = new EtxAttachment();
        etxMessage.setId(messageId);
        etxMessage.setAttachmentUuid(attachementUuid);
        return etxMessage;
    }
}
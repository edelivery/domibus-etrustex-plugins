package eu.europa.ec.etrustex.adapter.persistence.api.administration;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxUser;
import eu.europa.ec.etrustex.adapter.persistence.api.AbstractDaoIT;
import eu.europa.ec.etrustex.adapter.persistence.api.DaoBase;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 29-Jan-18
 */
public class UserDaoIT extends AbstractDaoIT<EtxUser> {

    @Autowired
    private UserDao userDao;

    @Test
    public void findUserByName_notFound() {
        assertThat(userDao.findUserByName("NOT_FOUND"), nullValue());
    }

    @Test
    public void findUserByName_ok() {
        EtxUser result = userDao.findUserByName("USER_3");
        assertThat(result, is(notNullValue()));
    }

    @Override
    protected Long getExistingId() {
        return 10003L;
    }

    @Override
    protected DaoBase<EtxUser> getDao() {
        return userDao;
    }

    @Override
    protected EtxUser getNewEntity() {
        EtxUser etxUser = new EtxUser();
        etxUser.setName(UUID.randomUUID().toString());
        etxUser.setPassword(UUID.randomUUID().toString());
        return etxUser;
    }
}
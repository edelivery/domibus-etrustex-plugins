package eu.europa.ec.etrustex.adapter.service;

import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPluginService;
import eu.europa.ec.etrustex.adapter.model.entity.administration.AdapterConfigurationProperty;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxAdapterConfig;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.AdapterConfigDao;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * JUnit test class for <code>AdapterConfigurationServiceImpl</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@RunWith(JMockit.class)
public class AdapterConfigurationServiceImplTest {

    @Injectable
    AdapterConfigDao adapterConfigDao;
    @Injectable
    ClearEtxLogKeysService clearEtxLogKeysService;

    @Injectable
    private DomainBackEndPluginService domainBackEndPluginService;

    @Tested
    AdapterConfigurationServiceImpl adapterConfigurationService;

    @Test
    public void testGetAdapterConfigurations_HappyFlow() throws Exception {

        EtxAdapterConfig etxAdapterConfigExpected = new EtxAdapterConfig();
        final AdapterConfigurationProperty property = AdapterConfigurationProperty.ETX_ADAPTER_REPOSITORY_ROOT_PATH;
        final String value = "test_root";
        etxAdapterConfigExpected.setPropertyName(property);
        etxAdapterConfigExpected.setPropertyValue(value);
        final List<EtxAdapterConfig> etxAdapterConfigList =
                Collections.singletonList(etxAdapterConfigExpected);

        new Expectations() {{
            adapterConfigDao.findAdapterConfigs();
            result = etxAdapterConfigList;

        }};

        final Map<AdapterConfigurationProperty, String> adapterConfigurationsActual = adapterConfigurationService.getAdapterConfigurations();

        Assert.assertNotNull("adapterConfigurations object shouldn't be null", adapterConfigurationsActual);
        Assert.assertEquals(1, adapterConfigurationsActual.size());
        Assert.assertEquals(value, adapterConfigurationsActual.get(property));
    }

}
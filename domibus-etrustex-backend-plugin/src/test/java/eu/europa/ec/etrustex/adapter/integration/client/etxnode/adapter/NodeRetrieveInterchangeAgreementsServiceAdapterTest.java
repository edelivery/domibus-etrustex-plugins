package eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter;

import com.google.common.base.Charsets;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxParty;
import eu.europa.ec.etrustex.adapter.testutils.ETrustExBackendPluginPropertiesBuilder;
import eu.europa.ec.etrustex.common.exceptions.DomibusSubmissionException;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.util.ContentId;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginPropertiesTestConstants.*;
import static eu.europa.ec.etrustex.adapter.testutils.ResourcesUtils.readResource;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.deserializeXMLToObj;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.getObjectFromXml;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @since 27-Sep-17
 * @version 1.0
 */
@RunWith(JMockit.class)
public class NodeRetrieveInterchangeAgreementsServiceAdapterTest extends BaseAdapterUnitTest {

    private static final String SENDER_UUID = "SENDER_UUID";
    private static final String XML = readResource("data/SubmitRetrieveInterchangeAgreementsRequestRequest.xml");

    @Tested
    private NodeRetrieveInterchangeAgreementsServiceAdapter nodeRetrieveInterchangeAgreementsServiceAdapter;

    private EtxParty senderParty;

    @Before
    public void setUp() {
        super.setUp();
        senderParty = new EtxParty();
        senderParty.setPartyUuid(SENDER_UUID);
    }

    @Test
    public void sendMessage_exception() {
        new Expectations() {{
            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            times = 1;
            result = new IllegalArgumentException();
        }};

        try {
            //function under test
            nodeRetrieveInterchangeAgreementsServiceAdapter.sendInterchangeAgreementRequest(senderParty);
            Assert.fail("Expected exception to be thrown");
        } catch (ETrustExPluginException etxAE) {
            assertEquals(ETrustExError.DEFAULT.getCode(), etxAE.getError().getCode());
        }
    }

    @Test
    public void sendMessage_DomibusSubmissionException() {
        new Expectations() {{
            backendConnectorSubmissionService.submit((ETrustExAdapterDTO) any);
            times = 1;
            result = new DomibusSubmissionException("", new Throwable());
        }};

        try {
            //function under test
            nodeRetrieveInterchangeAgreementsServiceAdapter.sendInterchangeAgreementRequest(senderParty);
            Assert.fail("Expected exception to be thrown");
        } catch (DomibusSubmissionException etxAE) {
            // ok
        }
    }

    @Test
    public void sendInterchangeAgreementRequest_happyFlow() {

        nodeRetrieveInterchangeAgreementsServiceAdapter.sendInterchangeAgreementRequest(senderParty);
        new FullVerifications() {{
            ETrustExAdapterDTO eTrustExAdapterDTO;
            backendConnectorSubmissionService.submit(eTrustExAdapterDTO = withCapture());
            times = 1;

            assertThat(eTrustExAdapterDTO.getEtxBackendMessageId(), CoreMatchers.containsString("RICA"));
            assertEquals(senderParty.getPartyUuid(), eTrustExAdapterDTO.getAs4OriginalSender());
            assertEquals(senderParty.getPartyUuid(), eTrustExAdapterDTO.getAs4FromPartyId());
            assertEquals(TEST_AS4_FROMPARTYIDTYPE, eTrustExAdapterDTO.getAs4FromPartyIdType());
            assertEquals(TEST_AS4_FROMPARTYROLE, eTrustExAdapterDTO.getAs4FromPartyRole());
            assertEquals(TEST_AS4_ETRUSTEXNODEPARTYID, eTrustExAdapterDTO.getAs4ToPartyId());
            assertEquals(TEST_AS4_TOPARTYIDTYPE, eTrustExAdapterDTO.getAs4ToPartyIdType());
            assertEquals(TEST_AS4_TOPARTYROLE, eTrustExAdapterDTO.getAs4ToPartyRole());
            assertEquals(TEST_AS4_ETRUSTEXNODEPARTYID, eTrustExAdapterDTO.getAs4FinalRecipient());
            Assert.assertNotNull("Payload with content id Header should not be null", eTrustExAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_HEADER));
            Assert.assertNotNull("Payload with content id - generic should not be null", eTrustExAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_GENERIC));
            assertThat(
                    deserializeXMLToObj(eTrustExAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_HEADER).getPayloadAsString()),
                    sameBeanAs(getObjectFromXml(readResource("data/HeaderICA.xml")))
            );
            String payloadAsString = eTrustExAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_GENERIC).getPayloadAsString();
            assertThat(
                    deserializeXMLToObj(payloadAsString),
                    sameBeanAs(getObjectFromXml(XML)));
        }};
    }
}
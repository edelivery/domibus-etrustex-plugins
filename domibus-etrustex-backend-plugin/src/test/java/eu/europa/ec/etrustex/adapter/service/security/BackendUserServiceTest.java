package eu.europa.ec.etrustex.adapter.service.security;

import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxSystem;
import eu.europa.ec.etrustex.adapter.model.entity.administration.EtxUser;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.SystemDao;
import eu.europa.ec.etrustex.adapter.persistence.api.administration.UserDao;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.hamcrest.Matchers;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

@RunWith(JMockit.class)
public class BackendUserServiceTest {
    @Injectable
    private SystemDao systemDao;
    @Injectable
    private UserDao userDao;

    @Tested
    private BackendUserService backendUserService;
    private EtxUser etxUser;
    private List<Object> systems;

    @Before
    public void setUp() {
        etxUser = new EtxUser();
        systems = new ArrayList<>();
        systems.add(getSystem(etxUser));
        systems.add(getSystem(null));
    }

    private EtxSystem getSystem(EtxUser egreffeSystemLocalUser) {
        EtxSystem sys = new EtxSystem();

        sys.setBackendUser(egreffeSystemLocalUser);
        return sys;
    }

    @Test
    public void update() {
        new Expectations() {{
            userDao.update(etxUser);
            times = 1;
            result = etxUser;
        }};
        backendUserService.updateUser(etxUser);
        new FullVerifications() {{
        }};
    }

    @Test
    public void getAllBackEndUsers() {
        new Expectations() {{
            systemDao.findAll();
            times = 1;
            result = systems;
        }};

        List<EtxUser> allBackEndUsers = backendUserService.getAllBackEndUsers();
        Assert.assertThat(allBackEndUsers.size(), Is.is(1));
        Assert.assertThat(allBackEndUsers, Matchers.contains(etxUser));

        new FullVerifications() {{
        }};
    }
}
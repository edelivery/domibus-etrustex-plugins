package eu.europa.ec.etrustex.adapter.model.entity.attachment;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * JUnit class for <code>ChecksumAlgorithmType</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
public class ChecksumAlgorithmTypeTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testFromValue_HappyFlow() throws Exception {
        //test all the values
        Assert.assertEquals(ChecksumAlgorithmType.fromValue("SHA-512"),
                ChecksumAlgorithmType.SHA_512);
    }

    @Test
    public void testFromValue_UnhappyFlow() throws Exception {
        final String wrongValue = "test wrong value";

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("No matching enum found for value: " + wrongValue);

        //do the thing, call with this bad value
        ChecksumAlgorithmType.fromValue(wrongValue);
    }

    @Test
    public void testGetValue_HappyFlow() throws Exception {
        Assert.assertEquals(ChecksumAlgorithmType.SHA_512.getValue(), "SHA-512");
    }

}
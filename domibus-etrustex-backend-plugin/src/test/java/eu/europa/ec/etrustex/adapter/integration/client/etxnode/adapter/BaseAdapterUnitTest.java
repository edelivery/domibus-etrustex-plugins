package eu.europa.ec.etrustex.adapter.integration.client.etxnode.adapter;

import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.service.BackendConnectorSubmissionService;
import eu.europa.ec.etrustex.adapter.testutils.ETrustExBackendPluginPropertiesBuilder;
import mockit.Injectable;
import mockit.integration.junit4.JMockit;
import org.junit.Before;
import org.junit.runner.RunWith;

/**
 * @author François Gautier
 * @since 03-Oct-17
 * @version 1.0
 */
@RunWith(JMockit.class)
abstract class BaseAdapterUnitTest {

    @Injectable
    protected ETrustExBackendPluginProperties etxBackendPluginProperties;

    @Injectable
    protected BackendConnectorSubmissionService backendConnectorSubmissionService;

    @Before
    public void setUp() {
        etxBackendPluginProperties = ETrustExBackendPluginPropertiesBuilder.getInstance();
    }

    protected EtxMessage getEtxMessage() {
        EtxMessage etxMessage = new EtxMessage();
        etxMessage.setMessageUuid("UUID");
        etxMessage.setId(1L);
        return etxMessage;
    }
}

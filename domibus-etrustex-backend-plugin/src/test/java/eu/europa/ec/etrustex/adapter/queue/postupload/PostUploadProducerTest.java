package eu.europa.ec.etrustex.adapter.queue.postupload;

import eu.europa.ec.etrustex.adapter.model.entity.attachment.EtxAttachment;
import eu.europa.ec.etrustex.adapter.model.entity.message.EtxMessage;
import eu.europa.ec.etrustex.adapter.model.entity.message.MessageState;
import eu.europa.ec.etrustex.adapter.persistence.api.message.MessageDao;
import eu.europa.ec.etrustex.adapter.queue.JmsProducer;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.jms.Queue;

/**
 * @author Arun Raj
 * @version 1.0
 * @since 02/07/2019
 */
@RunWith(JMockit.class)
public class PostUploadProducerTest {

    @Injectable
    private Queue postUpload;

    @Injectable
    private JmsProducer<PostUploadQueueMessage> jmsProducer;

    @Injectable
    private MessageDao messageDao;

    @Injectable
    private AttachmentService attachmentService;

    @Tested
    private PostUploadProducer postUploadProducer;

    @Test
    public void testTriggerBundlePostUpload(@Mocked EtxMessage etxMessage, @Mocked PostUploadQueueMessage postUploadQueueMessage) {

        new Expectations() {{
            etxMessage.setMessageState(MessageState.MS_UPLOAD_REQUESTED);

            etxMessage.getId();
            result = 1L;

            new PostUploadQueueMessage(etxMessage.getId());
            result = postUploadQueueMessage;
        }};

        postUploadProducer.triggerBundlePostUpload(etxMessage);

        new FullVerifications() {{
            messageDao.update(etxMessage);
            jmsProducer.postMessage(postUpload, withAny(postUploadQueueMessage));
        }};
    }

    @Test
    public void testCheckAttachmentsAndTriggerPostUpload_AllAttsInFinalState(@Mocked EtxMessage etxMessage, @Mocked EtxAttachment etxAttachment) {

        new Expectations() {{
            etxAttachment.getId();
            times = 1;
            result = 1L;

            etxAttachment.getMessage();
            times = 1;
            result = etxMessage;

            attachmentService.areAllAttachmentsInFinalState(etxAttachment.getMessage());
            result = true;
        }};

        postUploadProducer.checkAttachmentsAndTriggerPostUpload(etxAttachment);

        new FullVerifications() {{
            postUploadProducer.triggerBundlePostUpload(etxMessage);
        }};
    }

    @Test
    public void testCheckAttachmentsAndTriggerPostUpload_AllAttsNotInFinalState(@Mocked EtxMessage etxMessage, @Mocked EtxAttachment etxAttachment) {

        new Expectations() {{
            etxAttachment.getId();
            times = 1;
            result = 1L;

            etxAttachment.getMessage();
            times = 1;
            result = etxMessage;

            attachmentService.areAllAttachmentsInFinalState(etxAttachment.getMessage());
            result = false;
        }};

        postUploadProducer.checkAttachmentsAndTriggerPostUpload(etxAttachment);

        new FullVerifications() {{
        }};
    }
}
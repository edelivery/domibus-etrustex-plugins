package eu.europa.ec.etrustex.adapter.model.entity.message;

import org.junit.Assert;
import org.junit.Test;

/**
 * JUnit class for <code>MessageState</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
public class MessageStateTest {

    @Test
    public void testIsFinal_HappyFlow() throws Exception {

        Assert.assertEquals(false, MessageState.MS_CREATED.isFinal());
        Assert.assertEquals(false, MessageState.MS_UPLOADED.isFinal());
        Assert.assertEquals(true, MessageState.MS_PROCESSED.isFinal());
        Assert.assertEquals(true, MessageState.MS_FAILED.isFinal());
    }

}
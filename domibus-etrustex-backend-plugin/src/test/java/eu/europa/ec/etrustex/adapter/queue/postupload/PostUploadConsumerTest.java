package eu.europa.ec.etrustex.adapter.queue.postupload;

import eu.domibus.ext.services.DomainContextExtService;
import eu.domibus.ext.services.DomainExtService;
import eu.europa.ec.etrustex.adapter.domain.DomainBackEndPluginService;
import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.queue.ETrustExBackendPluginJMSErrorUtils;
import eu.europa.ec.etrustex.common.jms.TextMessageConverter;
import eu.europa.ec.etrustex.common.util.ClearEtxLogKeysService;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * JUnit class for <code>PostUploadConsumer</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@RunWith(JMockit.class)
public class PostUploadConsumerTest {

    @Injectable
    private PostUploadHandler handler;

    @Injectable
    private TextMessageConverter textMessageConverter;
    @Injectable
    private DomainBackEndPluginService domainBackEndPluginService;
    @Injectable
    @Qualifier("etxBackendPluginProperties")
    private ETrustExBackendPluginProperties etxBackendPluginProperties;
    @Injectable
    private DomainExtService domainExtService;
    @Injectable
    private DomainContextExtService domainContextExtService;

    @Injectable
    private ClearEtxLogKeysService clearEtxLogKeysService;
    @Injectable
    private ETrustExBackendPluginJMSErrorUtils eTrustExBackendPluginJMSErrorUtils;
    @Tested
    private PostUploadConsumer postUploadConsumer;

    @Test
    public void testHandleMessage_HappyFlow() throws Exception {
        final Long messageId = 20L;
        PostUploadQueueMessage queueMessage = new PostUploadQueueMessage(messageId);

        //method to be tested
        postUploadConsumer.handleMessage(queueMessage);

        new FullVerifications() {{
            Long messageIdActual;
            handler.handlePostUpload(messageIdActual = withCapture());
            Assert.assertNotNull("message id shouldn't be null", messageIdActual);
            Assert.assertEquals("message id should match", messageId, messageIdActual);
        }};
    }

    @Test
    public void testValidateMessage_HappyFlow() throws Exception {
        final Long messageId = 20L;

        PostUploadQueueMessage queueMessage = new PostUploadQueueMessage(messageId);

        //method to be tested
        postUploadConsumer.validateMessage(queueMessage);

        new FullVerifications() {{
            Long messageIdActual;
            handler.validatePostUpload(messageIdActual = withCapture());
            Assert.assertNotNull("message id shouldn't be null", messageIdActual);
            Assert.assertEquals("message id should match", messageId, messageIdActual);
        }};

    }

}
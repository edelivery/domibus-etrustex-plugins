package eu.europa.ec.etrustex.adapter.application;

import eu.europa.ec.etrustex.adapter.integration.ETrustExBackendPluginProperties;
import eu.europa.ec.etrustex.adapter.service.AdapterConfigurationService;
import eu.europa.ec.etrustex.adapter.service.AttachmentService;
import eu.europa.ec.etrustex.common.service.WorkspaceFileService;
import eu.europa.ec.etrustex.common.util.FileUtils;
import mockit.*;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static eu.europa.ec.etrustex.adapter.application.HandleExpiredMessageJob.DEFAULT_RETENTION_WEEKS;
import static eu.europa.ec.etrustex.adapter.application.HandleExpiredMessageJob.PAGE_SIZE;
import static eu.europa.ec.etrustex.adapter.model.entity.administration.AdapterConfigurationProperty.ETX_NODE_RETENTION_POLICY_WEEKS;

/**
 * @author François Gautier
 * @version 1.0
 * @since 22-Jan-18
 */
public class HandleExpiredMessageJobTest extends AbstractBackEndQuartzJobBeanTest {

    @Injectable
    private AttachmentService attachmentService;

    @Injectable
    private AdapterConfigurationService adapterConfigurationService;

    @Injectable
    private WorkspaceFileService etxBackendPluginWorkspaceService;

    @Injectable
    private ETrustExBackendPluginProperties etxBackendPluginProperties;

    @Tested
    private HandleExpiredMessageJob handleExpiredMessageJob;
    private HashMap<Object, Object> config;
    private Path path;

    @Before
    public void setUp() {
        new File("cleanup").mkdir();
        config = new HashMap<>();
        path = new File("cleanup").toPath();
    }

    @After
    public void tearDown() throws Exception {
        org.apache.commons.io.FileUtils.deleteDirectory(new File("cleanup"));
    }

    @Test
    public void executeInternal_wrongRetentionPolicy_useDefault_0page() {
        config.put(ETX_NODE_RETENTION_POLICY_WEEKS, "");
        final List<Date> dates = new ArrayList<>();

        new Expectations() {{
            adapterConfigurationService.getAdapterConfigurations();
            result = config;
            times = 1;

            attachmentService.countNextAttachmentsToExpire(withCapture(dates));
            result = 0L;
            times = 1;

            etxBackendPluginWorkspaceService.getRoot();
            result = path;
            times = 1;
        }};

        handleExpiredMessageJob.process(null);

        new FullVerifications() {{
        }};

        Assert.assertEquals(DateTime.now()
                .withTimeAtStartOfDay()
                .minusWeeks(DEFAULT_RETENTION_WEEKS)
                .toDate(), dates.get(0));
    }

    @SuppressWarnings("unused")
    @Test
    public void executeInternal_20Attachment_2pages(@Mocked final FileUtils fileUtils) {
        config.put(ETX_NODE_RETENTION_POLICY_WEEKS, null);
        final List<Date> dates = new ArrayList<>();

        new Expectations() {{
            adapterConfigurationService.getAdapterConfigurations();
            result = config;
            times = 1;

            attachmentService.countNextAttachmentsToExpire(withCapture(dates));
            result = 20L;
            times = 1;

            etxBackendPluginWorkspaceService.getRoot();
            result = path;
            times = 1;

        }};

        handleExpiredMessageJob.process(null);

        new FullVerifications() {{
            attachmentService.expireAttachments((Date) any, PAGE_SIZE, 0);
            times = 1;

            attachmentService.expireAttachments((Date) any, PAGE_SIZE, 1);
            times = 1;

            FileUtils.deleteOldFilesInDirectory(path, TimeUnit.MILLISECONDS.convert(DEFAULT_RETENTION_WEEKS * 7 + 1, TimeUnit.DAYS));
            times = 1;
        }};

        Assert.assertEquals(DateTime.now()
                .withTimeAtStartOfDay()
                .minusWeeks(DEFAULT_RETENTION_WEEKS)
                .toDate(), dates.get(0));
    }
}
import eu.domibus.plugin.Submission;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import javax.activation.DataHandler;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Arun Raj
 */
public class DataHandlerOperationTest {

    @Test
    public void testPayloadExtraction() throws IOException {
        String xmlInput = "<ns4:SubmitApplicationResponseRequest xmlns:ns5=\"ec:schema:xsd:CommonAggregateComponents-2\" xmlns:ns3=\"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2\" xmlns:ns2=\"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2\" xmlns=\"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2\" xmlns:ns4=\"ec:services:wsdl:ApplicationResponse-2\"><ns4:ApplicationResponse><ID>1234</ID><IssueDate>2017-03-08</IssueDate><ns3:DocumentResponse><ns3:Response><ReferenceID>BDL15FEB2016-26744705251210_1OF1</ReferenceID><ResponseCode>BDL:7</ResponseCode></ns3:Response><ns3:DocumentReference><ID>BDL15FEB2016-26744705251210_1OF1</ID><DocumentTypeCode>BDL</DocumentTypeCode></ns3:DocumentReference></ns3:DocumentResponse></ns4:ApplicationResponse></ns4:SubmitApplicationResponseRequest>";

        final Collection<Submission.TypedProperty> etxAS4HeaderPayloadProperties = new ArrayList<>();
        Submission.TypedProperty typedProperty1 = new Submission.TypedProperty("MimeType", "text/xml", "cid:generic");
        etxAS4HeaderPayloadProperties.add(typedProperty1);

        DataHandler dataHandlerETXHeaderHolder = new DataHandler(new ByteArrayDataSource(xmlInput.getBytes(), "text/xml"));
        Submission.Payload payload = new Submission.Payload("cid:generic", dataHandlerETXHeaderHolder, etxAS4HeaderPayloadProperties, false, null, null);

        System.out.println(payload);

        String s = new String(IOUtils.toByteArray(payload.getPayloadDatahandler().getDataSource().getInputStream()), StandardCharsets.UTF_8);
        System.out.println(payload.getContentId());
//        System.out.println(payload.getDescription().getValue());
        for (Submission.TypedProperty payloadProperty : payload.getPayloadProperties()) {
            System.out.println("KEY:" + payloadProperty.getKey());
            System.out.println("Value:" + payloadProperty.getValue());
            System.out.println("Type:" + payloadProperty.getType());
        }

        System.out.println(s);
    }
}

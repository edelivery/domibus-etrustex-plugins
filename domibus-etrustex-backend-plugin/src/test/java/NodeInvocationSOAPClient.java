import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.services.wsdl.applicationresponse_2.*;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import oasis.names.specification.ubl.schema.xsd.fault_1.FaultType;

import javax.xml.bind.JAXBException;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import java.net.MalformedURLException;

/**
 * Created by Arun Raj on 11/04/2017.
 */
public class NodeInvocationSOAPClient {

    public static void main(String[] a) throws MalformedURLException, JAXBException {
//        URL wsdlURL = new URL("E:\\workspace\\domibus-plugins\\domibus-etrustex-plugin-common\\src\\main\\resources\\node\\wsdl\\ApplicationResponse-2.0.wsdl");
//        URL wsdlURL = new URL("https://webgate.test.ec.europa.eu/e-trustexnode/services/ApplicationResponse-2.0");
        ApplicationResponseService applicationResponseService = new ApplicationResponseService();
        ApplicationResponsePortType applicationResponsePortType = applicationResponseService.getApplicationResponsePort();

        String headerXML = "<ns11:Header xmlns:ns8=\"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2\" xmlns:ns7=\"ec:schema:xsd:CommonBasicComponents-1\" xmlns:ns6=\"urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2\" xmlns:ns5=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:ns4=\"urn:oasis:names:specification:ubl:schema:xsd:SignatureBasicComponents-2\" xmlns:ns3=\"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2\" xmlns:ns2=\"ec:schema:xsd:CommonAggregateComponents-2\" xmlns:ns10=\"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2\" xmlns=\"http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader\" xmlns:ns11=\"ec:services:wsdl:ApplicationResponse-2\"><ns2:BusinessHeader><Sender><Identifier>TEST_EGREFFE_APP_PARTY_B4</Identifier></Sender><Receiver><Identifier>TEST_EGREFFE_NP_PARTY_B4</Identifier></Receiver><BusinessScope><Scope><Type>CREATE_STATUS_AVAILABLE</Type></Scope></BusinessScope></ns2:BusinessHeader><ns2:TechnicalHeader><ns6:SignatureInformation/></ns2:TechnicalHeader></ns11:Header>";
        HeaderType headerType = TransformerUtils.getObjectFromXml(headerXML);
        Holder<HeaderType> headerHolder = new Holder<>(headerType);
        String submitApplicationResponseRequestXML = "<ns4:SubmitApplicationResponseRequest xmlns:ns5=\"ec:schema:xsd:CommonAggregateComponents-2\" xmlns:ns3=\"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2\" xmlns:ns2=\"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2\" xmlns=\"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2\" xmlns:ns4=\"ec:services:wsdl:ApplicationResponse-2\"><ns4:ApplicationResponse><ID>123457</ID><IssueDate>2017-03-29</IssueDate><ns3:DocumentResponse><ns3:Response><ReferenceID>BDL15FEB2016-26744705251210_1OF2</ReferenceID><ResponseCode>BDL:6</ResponseCode></ns3:Response><ns3:DocumentReference><ID>BDL15FEB2016-26744705251210_1OF2</ID><DocumentTypeCode>BDL</DocumentTypeCode></ns3:DocumentReference></ns3:DocumentResponse></ns4:ApplicationResponse></ns4:SubmitApplicationResponseRequest>";
        SubmitApplicationResponseRequest submitApplicationResponseRequest = TransformerUtils.getObjectFromXml(submitApplicationResponseRequestXML);

        BindingProvider prov = (BindingProvider) applicationResponsePortType;
        prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "TEST_EGREFFE_APP_B4");
        prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "TestAdapter4");
        prov.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "https://webgate.test.ec.europa.eu/e-trustexnode/services/ApplicationResponse-2.0");

        try {
            applicationResponsePortType.submitApplicationResponse(submitApplicationResponseRequest, headerHolder);
        } catch (FaultResponse faultResponse) {
            //faultResponse.printStackTrace();
            System.out.println(faultResponse.getLocalizedMessage());

            FaultType faultType = faultResponse.getFaultInfo();
            String faultMsg = faultResponse.getMessage();

            String faultTypeXML = TransformerUtils.serializeObjToXML(faultType);
            System.out.println(faultTypeXML);

            FaultType faultTypeAtBackend = TransformerUtils.getObjectFromXml(faultTypeXML);
            String faultDescription = ETrustExError.DEFAULT.getCode();
            for (DescriptionType descriptionType : faultTypeAtBackend.getDescription()) {
                faultDescription = descriptionType.getValue();
            }

            String faultResponseCode = faultTypeAtBackend.getResponseCode().getValue();
            FaultResponse faultResponseAtBackend = new FaultResponse(faultDescription, faultTypeAtBackend);
            System.out.println(faultResponseAtBackend.getLocalizedMessage());
        }
    }

}

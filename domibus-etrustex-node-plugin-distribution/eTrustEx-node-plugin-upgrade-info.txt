eTrustEx-Node-Plugin 1.3.3 (from version 1.3.2):
        - Archive the configuration files of eTrustEx Node plugin from the following locations:
            - ${domibus.config.location}/plugins/lib/domibus-etrustex-node-plugin-*.jar
            - ${domibus.config.location}/plugins/config/etrustex-node-plugin.properties
            - ${domibus.config.location}/plugins/config/etrustex-node-plugin*.xml
        - Upgrade to the latest compatible version of Domibus 4.2.x .(refer Node Plugin admin guide for details)
        - Update the ${domibus.config.location}/plugins/lib - replace the domibus-etrustex-node-plugin-*.jar with the latest version in the eTrustEx Node Plugin release package plugins/lib folder
        - Replace the configuration files in ${domibus.config.location}/plugins/config/etrustex-node-plugin*.xml and ${domibus.config.location}/plugins/config/etrustex-node-plugin.properties from archive
        - From the Domibus Admin console - Message Filter page, ensure that 'DomibusETrustExNodeConnector' is the top row in the domain for the Node Plugin.

eTrustEx-Node-Plugin 1.3.2 (from version 1.2):
        - Upgrade to the latest compatible version of Domibus 4.2.x .(refer Node Plugin admin guide for details)
        - Update the ${domibus.config.location}/plugins/lib - replace the domibus-etrustex-node-plugin-*.jar with the latest version in the eTrustEx Node Plugin release package plugins/lib folder
        - Update the ${domibus.config.location}/plugins/config - replace the following files with the latest files in the eTrustEx Node Plugin release package plugins/config folder and ensure local config changes are reimplemented.
             - etrustex-node-plugin.xml
             - etrustex-node-plugin-connector.xml
             - etrustex-node-plugin-jmsContext.xml
        - From Domibus Admin console - 'Message Filter' page verify that 'DomibusETrustExNodeConnector' is the top message filter in the domain of Node Plugin.
        - Update the domain name of Node plugin Access Point in the pMode to lower case (ec_etx_nodeplugin).
        - [Domibus Multi-tenant environment only]:
            -  in ${domibus.config.location}/plugins/config/etrustex-node-plugin.properties update 'etx.node.domain' to lowercase.


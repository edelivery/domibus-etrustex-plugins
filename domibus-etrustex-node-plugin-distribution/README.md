# README

## E-TrustEx Node Plugin Distribution

### Prerequisites for the release
1. In module **domibus-etrustex-plugin-common** ensure the following in the pom file:
    1. ````project.parent.version```` - should point to correct Domibus release version
    2. ````project.version```` - should be set to correct backend plugin release version

2. In module  **Domibus-etrustex-plugin** ensure the following in the pom file:
    1. ````project.parent.version```` - should point to correct Domibus release version
    2. ````project.version```` - should be set to correct backend plugin release version
    3. under profile **liquibase** modify the execution to generate files for the latest node plugin final release version (without snapshot)

3. In module **domibus-etrustex-node-plugin-distribution** ensure the following in the pom file:
    1. ````project.parent.version```` - should point to correct Domibus release version
    2. ````project.version```` - should be set to correct backend plugin release version

### Execution Sequence for distribution
1. In module - **domibus-etrustex-plugin-common** execute:  ````mvn clean install````

2. In module **Domibus-etrustex-plugin** execute: ````mvn clean install -Pliquibase````
This will generate the necessary Jars and config properties in the project build directory. The SQLs will be generated in the *Domibus-etrustex-plugin* src/main/conf/etxNodePlugin/oracle  folder.

3. In module **domibus-etrustex-node-plugin-distribution** execute: 
    1. to release the main artefact: ````mvn clean deploy````
    

### Release artefact structure

This main release artefact is a zip file containing the eTrustEx Node Plugin artefacts organized under directories that can be easily mapped with Domibus.
 
    - plugins
        - config
            domibus-etrustex-backend-plugin.properties
        - lib
            domibus-etrustex-backend-plugin-1.2-SNAPSHOT.jar
    - scripts
        - weblogic
            etrustex-backend-plugin-WeblogicCluster.properties
            etrustex-backend-plugin-WeblogicSingleServer.properties
    - sql-scripts
        **DDLs for eTrustEx Backend Plugin installation from scratch and upgrade - Oracle only
    - nonMultiTenant-DomibusProperties - with several properties customized for EUSEND non multi-tenant environments
        domibus.properties
        logback.xml
    - multiTenant-DomibusProperties - with several properties customized for tenant domain - EC_ETX_NODEPLUGIN in EUSEND multi-tenant environment.
        EC_ETX_NODEPLUGIN-domibus.properties
        EC_ETX_NODEPLUGIN-logback.xml
WireMock - Weblogic 12.1.3.0.0 installation
======================================================

[![Build Status](https://travis-ci.org/tomakehurst/wiremock.svg?branch=master)](https://travis-ci.org/tomakehurst/wiremock)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.github.tomakehurst/wiremock/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.github.tomakehurst/wiremock)



Building WireMock for weblogic locally
--------------------------------------
To build both war and explodedWar
```bash
./gradlew clean war explodedWar
```

The built files will be placed under ``build/libs`` (for the war) and ``build/wiremock`` (for the exploded war)


Test
----

GET ```{hostname:port}```/wiremock/api/test
should return:
```
{
    "working": "YES" 
}
```

For more information: http://wiremock.org/docs/

Files information
-----------------

BasicTest.txt

```
<urn1:file>
     <urn1:id>basicTest.txt_XXXXXXXXXXXXXXXX</urn1:id>
     <urn1:fileType>BINARY</urn1:fileType>
     <urn1:relativePath>RELATIVE PATH</urn1:relativePath>
     <urn1:name>basicTest.txt</urn1:name>
     <urn1:mimeType>application/txt</urn1:mimeType>
     <urn1:size>2241</urn1:size>
     <urn1:checksum algorithm="SHA-512">990AEECDD4E2FF084B9594A936D856A7676D89DF42C39A15CE4F60617EB194322D76486836F246C431BF5C2F18E723B2977F4C550F92901EA70CEC5A6BA83E89</urn1:checksum>
</urn1:file>
```
               
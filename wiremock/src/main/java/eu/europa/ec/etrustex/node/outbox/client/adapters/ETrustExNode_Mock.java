package eu.europa.ec.etrustex.node.outbox.client.adapters;

/*import ec.schema.xsd.ack_2.AcknowledgmentType;
import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.schema.xsd.commonbasiccomponents_1.AckIndicatorType;
import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseRequest;
import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseResponse;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleRequest;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleResponse;
import ec.services.wsdl.documentwrapper_2.FaultResponse;
import ec.services.wsdl.documentwrapper_2.StoreDocumentWrapperRequest;
import ec.services.wsdl.documentwrapper_2.StoreDocumentWrapperResponse;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.w3c.dom.Element;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.*;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.Holder;
import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Iterator;
*/
/**
 * Methods to mock responses from eTrustExNode for <b>LOCAL testing</b>.<br/>
 * For use:<br/>
 * <ol>
 *     <li>Uncomment all lines in the imports and all lines of the method - invokeInboxNotifyApplicationResponse_BDLOK()</li>
 *     <li>Copy this class to the source directory of node plugin under package - eu.europa.ec.etrustex.node.outbox.client.adapters so that class is available in jar for deployment.</li>
 *     <li>In eu.europa.ec.etrustex.node.outbox.client.adapters.SubmitDocumentBundleAdapter#performOperation below the invocation to port.submitDocumentBundle add ETrustExNode_Mock.invokeInboxNotifyApplicationResponse_BDLOK() </li>
 *     <li>In etrustex-node-plugin.properties change:
 *     <ol>
 *         <li>etx.node.services.DocumentBundleService.endpoint to URL of Node Plugin - InboxService DocumentBundle</li>
 *         <li>etx.node.services.ApplicationResponseService.endpoint to URL of Node Plugin - InboxService ApplicationService</li>
 *         <li>etx.node.services.DocumentWrapperService.endpoint to Wiremock endpoint</li>
 *     </ol>
 *     </li>
 * </ol><br/>
 *
 * Only invokeInboxNotifyApplicationResponse_BDLOK operation is needed. Others are only experimental for information purposes & not needed.
 *TODO: change to a rest service that can be deployed as a web application in wiremock server. The service should have an API that accepts DocumentBundle from NodePlugin to Node invocation, routes it to NodePlugin Inbox DocumentBundle and addtionally sends _BDL_OK to NodePlugin Inbox ApplicationResponse.
 */
public class ETrustExNode_Mock {

    //private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ETrustExNode_Mock.class);

    /**
     * Mocks the _BDL_OK notification from eTrustEx Node.
     * Modify source code of Node Plugin {@link SubmitDocumentBundleAdapter} performOperation to invoke this method after bundle invocation.
     * But do not commit this mock invocation.
     *
     * @param outboxSubmitDocumentBundleRequest
     * @throws JAXBException
     * @throws SOAPException
     * @throws IOException
     * @throws TransformerException
     */
    /*public static void invokeInboxNotifyApplicationResponse_BDLOK(SubmitDocumentBundleRequest outboxSubmitDocumentBundleRequest) throws JAXBException, SOAPException, IOException, TransformerException {

        JAXBContext jaxbContext = JAXBContext.newInstance(SubmitDocumentBundleRequest.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(outboxSubmitDocumentBundleRequest, sw);
        String submitApplicationResponseRequestSoapEnvXml = new StringBuilder()
                .append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ec=\"ec:services:wsdl:ApplicationResponse-2\" xmlns:ec1=\"ec:schema:xsd:CommonAggregateComponents-2\" xmlns:stan=\"http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader\" xmlns:urn=\"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2\" xmlns:urn1=\"urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2\" xmlns:urn2=\"urn:oasis:names:specification:ubl:schema:xsd:SignatureBasicComponents-2\" xmlns:xd=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:urn3=\"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2\" xmlns:urn4=\"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2\">\n")
                .append("   <soapenv:Header>\n")
                .append("      <ec:Header>\n")
                .append("         <ec1:BusinessHeader>\n")
                .append("            <stan:Sender>\n")
                .append("               <stan:Identifier>").append(outboxSubmitDocumentBundleRequest.getDocumentBundle().getReceiverParty().get(0).getEndpointID().getValue()).append("</stan:Identifier>\n")
                .append("            </stan:Sender>\n")
                .append("            <stan:Receiver>\n")
                .append("               <stan:Identifier>").append(outboxSubmitDocumentBundleRequest.getDocumentBundle().getSenderParty().getEndpointID().getValue()).append("</stan:Identifier>\n")
                .append("            </stan:Receiver>\n")
                .append("         </ec1:BusinessHeader>\n")
                .append("      </ec:Header>\n")
                .append("   </soapenv:Header>\n")
                .append("   <soapenv:Body>\n")
                .append("      <ec:SubmitApplicationResponseRequest>\n")
                .append("         <ec:ApplicationResponse>\n")
                .append("            <urn:ID>").append(outboxSubmitDocumentBundleRequest.getDocumentBundle().getID().getValue()).append("_BDL_OK</urn:ID>\n")
                .append("            <urn:IssueDate>").append(outboxSubmitDocumentBundleRequest.getDocumentBundle().getIssueDate().getValue()).append("</urn:IssueDate>\n")
                .append("            <urn4:DocumentResponse>\n")
                .append("               <urn4:Response>\n")
                .append("                  <urn:ReferenceID/>\n")
                .append("                  <urn:ResponseCode>BDL:1</urn:ResponseCode>\n")
                .append("                  <urn:Description>Available</urn:Description>\n")
                .append("               </urn4:Response>\n")
                .append("               <urn4:DocumentReference>\n")
                .append("                  <urn:ID>").append(outboxSubmitDocumentBundleRequest.getDocumentBundle().getID().getValue()).append("</urn:ID>\n")
                .append("                  <urn:DocumentTypeCode>BDL</urn:DocumentTypeCode>\n")
                .append("               </urn4:DocumentReference>\n")
                .append("            </urn4:DocumentResponse>\n")
                .append("         </ec:ApplicationResponse>\n")
                .append("      </ec:SubmitApplicationResponseRequest>\n")
                .append("   </soapenv:Body>\n")
                .append("</soapenv:Envelope>").toString();

        MessageFactory messageFactory = MessageFactory.newInstance();
        InputStream is = new ByteArrayInputStream(submitApplicationResponseRequestSoapEnvXml.getBytes(Charset.forName("UTF-8")));

        MimeHeaders mimeHeaders = new MimeHeaders();
        String basicAuthentication = "Basic "+ Base64.getEncoder().encodeToString(("etx_node_ws_routing_user"+":"+"Test@Plugins#987").getBytes());
        mimeHeaders.addHeader("SOAPAction", "");
        mimeHeaders.addHeader("Authorization", basicAuthentication);

        SOAPMessage soapRequest = messageFactory.createMessage(mimeHeaders, is);
        LOG.info("##########STUB INBOX SUBMIT APPLICATION RESPONSE REQUEST SoapHeader############");
        Iterator<Node> lstNodesSoapHeader = soapRequest.getSOAPPart().getEnvelope().getHeader().getChildElements();
        Node nodeSoapHeader;
        while (lstNodesSoapHeader.hasNext()) {
            nodeSoapHeader = lstNodesSoapHeader.next();
            if (nodeSoapHeader instanceof Element) {
                QName  operationName = new QName(nodeSoapHeader.getNamespaceURI(), nodeSoapHeader.getLocalName());
                LOG.info(operationName.toString());
                StringWriter sw1 = new StringWriter();
                TransformerFactory.newInstance().newTransformer().transform(
                        new DOMSource(nodeSoapHeader),
                        new StreamResult(sw1));
                LOG.info(sw1.toString());
            }
        }
        LOG.info("##########STUB INBOX SUBMIT APPLICATION RESPONSE REQUEST SoapHeader############");
        LOG.info("##########STUB INBOX SUBMIT APPLICATION RESPONSE REQUEST SoapBody############");
        Iterator<Node> lstNodesSoapBody = soapRequest.getSOAPPart().getEnvelope().getBody().getChildElements();
        Node nodeSoapBody = null;
        while (lstNodesSoapBody.hasNext()) {
            nodeSoapBody = lstNodesSoapBody.next();
            if (nodeSoapBody instanceof Element) {
                QName  operationName = new QName(nodeSoapBody.getNamespaceURI(), nodeSoapBody.getLocalName());
                LOG.info(operationName.toString());
                StringWriter sw2 = new StringWriter();
                TransformerFactory.newInstance().newTransformer().transform(
                        new DOMSource(nodeSoapBody),
                        new StreamResult(sw2));
                LOG.info(sw2.toString());
            }
        }
        LOG.info("##########STUB INBOX SUBMIT APPLICATION RESPONSE REQUEST SoapBody############");
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();
        LOG.info("##########STUB INVOKING INBOX SUBMIT APPLICATION RESPONSE############");
        SOAPMessage soapResponse = soapConnection.call(soapRequest, new URL("http://localhost:10005/domibus/services/e-trustex/integration/services/inbox/ApplicationResponseService/v2.0"));
        LOG.info("\n##########STUB INVOKING INBOX SUBMIT APPLICATION RESPONSE############");
        LOG.info("##########STUB INBOX SUBMIT APPLICATION RESPONSE RESPONSE SoapHeader############");
        Iterator<Node> lstNodesResponseSoapHeader = soapResponse.getSOAPPart().getEnvelope().getHeader().getChildElements();
        Node nodeResponseSoapHeader;
        while (lstNodesResponseSoapHeader.hasNext()) {
            nodeResponseSoapHeader = lstNodesResponseSoapHeader.next();
            if (nodeResponseSoapHeader instanceof Element) {
                QName  operationName = new QName(nodeResponseSoapHeader.getNamespaceURI(), nodeResponseSoapHeader.getLocalName());
                LOG.info(operationName.toString());
                StringWriter sw1 = new StringWriter();
                TransformerFactory.newInstance().newTransformer().transform(
                        new DOMSource(nodeResponseSoapHeader),
                        new StreamResult(sw1));
                LOG.info(sw1.toString());
            }
        }
        LOG.info("##########STUB INBOX SUBMIT APPLICATION RESPONSE RESPONSE SoapHeader############");
        LOG.info("##########STUB INBOX SUBMIT APPLICATION RESPONSE RESPONSE SoapBody############");
        Iterator<Node> lstNodesResponseSoapBody = soapResponse.getSOAPPart().getEnvelope().getBody().getChildElements();
        Node nodeResponseSoapBody = null;
        while (lstNodesResponseSoapBody.hasNext()) {
            nodeResponseSoapBody = lstNodesResponseSoapBody.next();
            if (nodeResponseSoapBody instanceof Element) {
                QName  operationName = new QName(nodeResponseSoapBody.getNamespaceURI(), nodeResponseSoapBody.getLocalName());
                LOG.info(operationName.toString());
                StringWriter sw2 = new StringWriter();
                TransformerFactory.newInstance().newTransformer().transform(
                        new DOMSource(nodeResponseSoapBody),
                        new StreamResult(sw2));
                LOG.info(sw2.toString());
            }
        }
        LOG.info("##########STUB INBOX SUBMIT APPLICATION RESPONSE RESPONSE SoapBody############");

    }*/

    /**
     * Mock the basic success response for StoreDocumentWrapper request.
     * For EndToEnd or SanityTest suites it is better to redirect the URL of Outbox flow SubmitApplicationResponse to the NodePlugin Inbox NotifyApplicationResponse URL.
     * @param authorizationHeaderHolder
     * @param request
     * @return
     * @throws FaultResponse
     */
    /*public static StoreDocumentWrapperResponse buildStoreDocumentWrapperSuccessResponse_FromRequest(Holder<HeaderType> authorizationHeaderHolder, StoreDocumentWrapperRequest request) throws FaultResponse {
        StoreDocumentWrapperResponse response = new StoreDocumentWrapperResponse();
        AckIndicatorType ackIndicatorType = new AckIndicatorType();
        ackIndicatorType.setValue(true);
        AcknowledgmentType acknowledgmentType = new AcknowledgmentType();
        acknowledgmentType.setAckIndicator(ackIndicatorType);
        response.setAck(acknowledgmentType);
        return response;
    }*/


    /**
     * NOT NEEDED: instead for Outbox just redirect the DocumentBundle URL of eTrustEx Node to the Inbox DocumentBundleURL of NodePlugin
     * @param outboxSubmitDocumentBundleRequest
     * @return
     * @throws SOAPException
     * @throws JAXBException
     * @throws IOException
     * @throws TransformerException
     */
    /*@Deprecated
    public static SubmitDocumentBundleResponse invokeInboxNotifyDocumentBundle(SubmitDocumentBundleRequest outboxSubmitDocumentBundleRequest) throws SOAPException, JAXBException, IOException, TransformerException {

        JAXBContext jaxbContext = JAXBContext.newInstance(SubmitDocumentBundleRequest.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(outboxSubmitDocumentBundleRequest, sw);
        String submitDocumentBundleRequestSoapEnvelopeXml = new StringBuilder()
                .append("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n")
                .append("   <soap:Header>\n")
                .append("      <ns7:Header xmlns:ns16=\"http://uri.etsi.org/01903/v1.4.1#\" xmlns:ns15=\"urn:oasis:names:specification:ubl:schema:xsd:Fault-1\" xmlns:ns14=\"ec:schema:xsd:DocumentBundle-1\" xmlns:ns13=\"ec:schema:xsd:Ack-2\" xmlns:ns12=\"http://uri.etsi.org/01903/v1.3.2#\" xmlns:ns11=\"urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2\" xmlns:ns10=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:ns9=\"urn:oasis:names:specification:ubl:schema:xsd:SignatureBasicComponents-2\" xmlns:ns8=\"http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader\" xmlns:ns7=\"ec:services:wsdl:DocumentBundle-2\" xmlns:ns6=\"ec:schema:xsd:CommonBasicComponents-1\" xmlns:xmime=\"http://www.w3.org/2005/05/xmlmime\" xmlns:ns4=\"ec:schema:xsd:CommonAggregateComponents-2\" xmlns:ns3=\"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2\" xmlns:ns2=\"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2\" xmlns=\"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2\">\n")
                .append("         <ns4:BusinessHeader>\n")
                .append("            <ns8:Sender>\n")
                .append("               <ns8:Identifier>").append(outboxSubmitDocumentBundleRequest.getDocumentBundle().getSenderParty().getEndpointID().getValue()).append("</ns8:Identifier>\n")
                .append("            </ns8:Sender>\n")
                .append("            <ns8:Receiver>\n")
                .append("               <ns8:Identifier>").append(outboxSubmitDocumentBundleRequest.getDocumentBundle().getReceiverParty().get(0).getEndpointID().getValue()).append("</ns8:Identifier>\n")
                .append("            </ns8:Receiver>\n")
                .append("            <ns8:BusinessScope>\n")
                .append("               <ns8:Scope>\n")
                .append("                  <ns8:Type>CREATE_STATUS_AVAILABLE</ns8:Type><ns8:InstanceIdentifier/>\n")
                .append("               </ns8:Scope>\n")
                .append("            </ns8:BusinessScope>\n")
                .append("         </ns4:BusinessHeader>\n")
                .append("      </ns7:Header>\n")
                .append("   </soap:Header>\n")
                .append("   <soap:Body>")
                .append(sw.toString())
                .append("\n")
                .append("   </soap:Body>\n")
                .append("</soap:Envelope>").toString();

        LOG.info("#########submitDocumentBundleRequestSoapEnvelopeXml:"+submitDocumentBundleRequestSoapEnvelopeXml);

        MessageFactory messageFactory = MessageFactory.newInstance();
        InputStream is = new ByteArrayInputStream(submitDocumentBundleRequestSoapEnvelopeXml.getBytes(Charset.forName("UTF-8")));

        MimeHeaders mimeHeaders = new MimeHeaders();
        String basicAuthentication = "Basic "+ Base64.getEncoder().encodeToString(("etx_node_ws_routing_user"+":"+"Test@Plugins#987").getBytes());
        mimeHeaders.addHeader("SOAPAction", "");
        mimeHeaders.addHeader("Authorization", basicAuthentication);

        SOAPMessage soapRequest = messageFactory.createMessage(mimeHeaders, is);
        LOG.info("##########STUB INBOX SUBMIT DOCUMENT BUNDLE REQUEST SoapHeader############");
        Iterator<Node> lstNodesSoapHeader = soapRequest.getSOAPPart().getEnvelope().getHeader().getChildElements();
        Node nodeSoapHeader;
        while (lstNodesSoapHeader.hasNext()) {
            nodeSoapHeader = lstNodesSoapHeader.next();
            if (nodeSoapHeader instanceof Element) {
                QName operationName = new QName(nodeSoapHeader.getNamespaceURI(), nodeSoapHeader.getLocalName());
                LOG.info(operationName.toString());
                StringWriter sw1 = new StringWriter();
                TransformerFactory.newInstance().newTransformer().transform(
                        new DOMSource(nodeSoapHeader),
                        new StreamResult(sw1));
                LOG.info(sw1.toString());
            }
        }
        LOG.info("##########STUB INBOX SUBMIT DOCUMENT BUNDLE REQUEST SoapHeader############");
        LOG.info("##########STUB INBOX SUBMIT DOCUMENT BUNDLE REQUEST SoapBody############");
        Iterator<Node> lstNodesSoapBody = soapRequest.getSOAPPart().getEnvelope().getBody().getChildElements();
        Node nodeSoapBody = null;
        while (lstNodesSoapBody.hasNext()) {
            nodeSoapBody = lstNodesSoapBody.next();
            if (nodeSoapBody instanceof Element) {
                QName  operationName = new QName(nodeSoapBody.getNamespaceURI(), nodeSoapBody.getLocalName());
                LOG.info(operationName.toString());
                StringWriter sw2 = new StringWriter();
                TransformerFactory.newInstance().newTransformer().transform(
                        new DOMSource(nodeSoapBody),
                        new StreamResult(sw2));
                LOG.info(sw2.toString());
            }
        }
        LOG.info("##########STUB INBOX SUBMIT DOCUMENT BUNDLE REQUEST SoapBody############");
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();
        LOG.info("##########STUB INVOKING INBOX SUBMIT DOCUMENT BUNDLE############");
        SOAPMessage soapResponse = soapConnection.call(soapRequest, new URL("http://localhost:10005/domibus/services/e-trustex/integration/services/inbox/DocumentBundleService/v2.0"));
        LOG.info("\n##########STUB INVOKING INBOX SUBMIT DOCUMENT BUNDLE############");

        LOG.info("##########STUB INBOX SUBMIT DOCUMENT BUNDLE RESPONSE SoapHeader############");
        Iterator<Node> lstNodesResponseSoapHeader = soapResponse.getSOAPPart().getEnvelope().getHeader().getChildElements();
        Node nodeResponseSoapHeader;
        while (lstNodesResponseSoapHeader.hasNext()) {
            nodeResponseSoapHeader = lstNodesResponseSoapHeader.next();
            if (nodeResponseSoapHeader instanceof Element) {
                QName  operationName = new QName(nodeResponseSoapHeader.getNamespaceURI(), nodeResponseSoapHeader.getLocalName());
                LOG.info(operationName.toString());
                StringWriter sw1 = new StringWriter();
                TransformerFactory.newInstance().newTransformer().transform(
                        new DOMSource(nodeResponseSoapHeader),
                        new StreamResult(sw1));
                LOG.info(sw1.toString());
            }
        }
        LOG.info("##########STUB INBOX SUBMIT DOCUMENT BUNDLE RESPONSE SoapHeader############");
        LOG.info("##########STUB INBOX SUBMIT DOCUMENT BUNDLE RESPONSE SoapBody############");
        String submitDocumentBundleResponseXML = null;
        Iterator<Node> lstNodesResponseSoapBody = soapResponse.getSOAPPart().getEnvelope().getBody().getChildElements();
        Node nodeResponseSoapBody = null;
        while (lstNodesResponseSoapBody.hasNext()) {
            nodeResponseSoapBody = lstNodesResponseSoapBody.next();
            if (nodeResponseSoapBody instanceof Element) {
                QName  operationName = new QName(nodeResponseSoapBody.getNamespaceURI(), nodeResponseSoapBody.getLocalName());
                LOG.info(operationName.toString());
                StringWriter sw2 = new StringWriter();
                TransformerFactory.newInstance().newTransformer().transform(
                        new DOMSource(nodeResponseSoapBody),
                        new StreamResult(sw2));
                submitDocumentBundleResponseXML = sw2.toString();
                LOG.info(submitDocumentBundleResponseXML);
            }
        }
        LOG.info("##########STUB INBOX SUBMIT DOCUMENT BUNDLE RESPONSE SoapBody############");

        JAXBContext jaxbContextResponse = JAXBContext.newInstance(SubmitDocumentBundleResponse.class);
        Unmarshaller jaxbUnmarshaller = jaxbContextResponse.createUnmarshaller();
        SubmitDocumentBundleResponse response = (SubmitDocumentBundleResponse) jaxbUnmarshaller.unmarshal(new StringReader(submitDocumentBundleResponseXML));
        return  response;
    }*/


    /**
     * NOT NEEDED: instead for Outbox just redirect the ApplicationResponse URL of eTrustEx Node to the Inbox ApplicationResponse of NodePlugin
     * @param authorizationHeaderHolder
     * @param outboxSubmitApplicationResponseRequest
     * @return
     * @throws JAXBException
     * @throws SOAPException
     * @throws IOException
     * @throws TransformerException
     * @throws ec.services.wsdl.applicationresponse_2.FaultResponse
     */
    /*@Deprecated
    public static SubmitApplicationResponseResponse invokeInboxNotifyApplicationResponse(Holder<HeaderType> authorizationHeaderHolder, SubmitApplicationResponseRequest outboxSubmitApplicationResponseRequest) throws JAXBException, SOAPException, IOException, TransformerException, ec.services.wsdl.applicationresponse_2.FaultResponse {

        JAXBContext jaxbContext = JAXBContext.newInstance(SubmitApplicationResponseRequest.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        jaxbMarshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(outboxSubmitApplicationResponseRequest, sw);
        String submitApplicationResponseRequestSoapEnvXml = new StringBuilder()
                .append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ec=\"ec:services:wsdl:ApplicationResponse-2\" xmlns:ec1=\"ec:schema:xsd:CommonAggregateComponents-2\" xmlns:stan=\"http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader\" xmlns:urn=\"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2\" xmlns:urn1=\"urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2\" xmlns:urn2=\"urn:oasis:names:specification:ubl:schema:xsd:SignatureBasicComponents-2\" xmlns:xd=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:urn3=\"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2\" xmlns:urn4=\"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2\">\n")
                .append("   <soapenv:Header>\n")
                .append("      <ec:Header>\n")
                .append("         <ec1:BusinessHeader>\n")
                .append("            <stan:Sender>\n")
                .append("               <stan:Identifier>").append(authorizationHeaderHolder.value.getBusinessHeader().getSender().get(0).getIdentifier().getValue()).append("</stan:Identifier>\n")
                .append("            </stan:Sender>\n")
                .append("            <stan:Receiver>\n")
                .append("               <stan:Identifier>").append(authorizationHeaderHolder.value.getBusinessHeader().getReceiver().get(0).getIdentifier().getValue()).append("</stan:Identifier>\n")
                .append("            </stan:Receiver>\n")
                .append("         </ec1:BusinessHeader>\n")
                .append("      </ec:Header>\n")
                .append("   </soapenv:Header>\n")
                .append("   <soapenv:Body>\n")
                .append(sw.toString())
                .append("\n")
                .append("   </soapenv:Body>\n")
                .append("</soapenv:Envelope>").toString();

        MessageFactory messageFactory = MessageFactory.newInstance();
        InputStream is = new ByteArrayInputStream(submitApplicationResponseRequestSoapEnvXml.getBytes(Charset.forName("UTF-8")));

        MimeHeaders mimeHeaders = new MimeHeaders();
        String basicAuthentication = "Basic "+ Base64.getEncoder().encodeToString(("etx_node_ws_routing_user"+":"+"Test@Plugins#987").getBytes());
        mimeHeaders.addHeader("SOAPAction", "");
        mimeHeaders.addHeader("Authorization", basicAuthentication);

        SOAPMessage soapRequest = messageFactory.createMessage(mimeHeaders, is);
        LOG.info("##########STUB INBOX SUBMIT APPLICATION RESPONSE REQUEST SoapHeader############");
        Iterator<Node> lstNodesSoapHeader = soapRequest.getSOAPPart().getEnvelope().getHeader().getChildElements();
        Node nodeSoapHeader;
        while (lstNodesSoapHeader.hasNext()) {
            nodeSoapHeader = lstNodesSoapHeader.next();
            if (nodeSoapHeader instanceof Element) {
                QName operationName = new QName(nodeSoapHeader.getNamespaceURI(), nodeSoapHeader.getLocalName());
                LOG.info(operationName.toString());
                StringWriter sw1 = new StringWriter();
                TransformerFactory.newInstance().newTransformer().transform(
                        new DOMSource(nodeSoapHeader),
                        new StreamResult(sw1));
                LOG.info(sw1.toString());
            }
        }
        LOG.info("##########STUB INBOX SUBMIT APPLICATION RESPONSE REQUEST SoapHeader############");
        LOG.info("##########STUB INBOX SUBMIT APPLICATION RESPONSE REQUEST SoapBody############");
        Iterator<Node> lstNodesSoapBody = soapRequest.getSOAPPart().getEnvelope().getBody().getChildElements();
        Node nodeSoapBody = null;
        while (lstNodesSoapBody.hasNext()) {
            nodeSoapBody = lstNodesSoapBody.next();
            if (nodeSoapBody instanceof Element) {
                QName  operationName = new QName(nodeSoapBody.getNamespaceURI(), nodeSoapBody.getLocalName());
                LOG.info(operationName.toString());
                StringWriter sw2 = new StringWriter();
                TransformerFactory.newInstance().newTransformer().transform(
                        new DOMSource(nodeSoapBody),
                        new StreamResult(sw2));
                LOG.info(sw2.toString());
            }
        }
        LOG.info("##########STUB INBOX SUBMIT APPLICATION RESPONSE REQUEST SoapBody############");
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();
        LOG.info("##########STUB INVOKING INBOX SUBMIT APPLICATION RESPONSE############");
        SOAPMessage soapResponse = soapConnection.call(soapRequest, new URL("http://localhost:10005/domibus/services/e-trustex/integration/services/inbox/ApplicationResponseService/v2.0"));
        LOG.info("\n##########STUB INVOKING INBOX SUBMIT APPLICATION RESPONSE############");
        LOG.info("##########STUB INBOX SUBMIT APPLICATION RESPONSE RESPONSE SoapHeader############");
        Iterator<Node> lstNodesResponseSoapHeader = soapResponse.getSOAPPart().getEnvelope().getHeader().getChildElements();
        Node nodeResponseSoapHeader;
        while (lstNodesResponseSoapHeader.hasNext()) {
            nodeResponseSoapHeader = lstNodesResponseSoapHeader.next();
            if (nodeResponseSoapHeader instanceof Element) {
                QName  operationName = new QName(nodeResponseSoapHeader.getNamespaceURI(), nodeResponseSoapHeader.getLocalName());
                LOG.info(operationName.toString());
                StringWriter sw1 = new StringWriter();
                TransformerFactory.newInstance().newTransformer().transform(
                        new DOMSource(nodeResponseSoapHeader),
                        new StreamResult(sw1));
                LOG.info(sw1.toString());
            }
        }
        LOG.info("##########STUB INBOX SUBMIT APPLICATION RESPONSE RESPONSE SoapHeader############");
        LOG.info("##########STUB INBOX SUBMIT APPLICATION RESPONSE RESPONSE SoapBody############");
        String submitApplicationResponseResponseXML = null;
        Iterator<Node> lstNodesResponseSoapBody = soapResponse.getSOAPPart().getEnvelope().getBody().getChildElements();
        Node nodeResponseSoapBody = null;
        while (lstNodesResponseSoapBody.hasNext()) {
            nodeResponseSoapBody = lstNodesResponseSoapBody.next();
            if (nodeResponseSoapBody instanceof Element) {
                QName  operationName = new QName(nodeResponseSoapBody.getNamespaceURI(), nodeResponseSoapBody.getLocalName());
                LOG.info(operationName.toString());
                StringWriter sw2 = new StringWriter();
                TransformerFactory.newInstance().newTransformer().transform(
                        new DOMSource(nodeResponseSoapBody),
                        new StreamResult(sw2));
                submitApplicationResponseResponseXML = sw2.toString();
                LOG.info(submitApplicationResponseResponseXML);
            }
        }
        LOG.info("##########STUB INBOX SUBMIT APPLICATION RESPONSE RESPONSE SoapBody############");

        JAXBContext jaxbContextResponse = JAXBContext.newInstance(SubmitApplicationResponseResponse.class);
        Unmarshaller jaxbUnmarshaller = jaxbContextResponse.createUnmarshaller();
        SubmitApplicationResponseResponse response = (SubmitApplicationResponseResponse) jaxbUnmarshaller.unmarshal(new StringReader(submitApplicationResponseResponseXML));
        return  response;
    }*/
}

package eu.europa.ec.etrustex;

import javax.xml.soap.*;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;
import java.util.Date;

/**
 * @author Arun Raj
 * @version 1.0
 * @since 05/02/2020
 */
public class TestLocalFileSystem_SendMessageBundle {


    private static final String APPLICATION_FILES_ROOT = "E:/workspace/Servers/wildfly12_singleserver/CommonStorage/_application_files";
    private static final String SENDER_SYSTEM_NAME = "DEV_ARUN_FSSYS1";
    private static final String C2_BACKENDPLUGIN_OUTBOXSERVICE_URL = "http://localhost:12003/domibus/services/e-trustex/adapter/integration/services/outbox/OutboxService/v2.0";

    private static final int NO_OF_FILES_IN_BUNDLE = 3;
    private static final String SIZE_PER_FILE = "10";

    private static final String BUNDLE_ID = "BUNDLE_ID_" + java.util.UUID.randomUUID();
    private static final String REFERENCE_ID = "EUPL V1.2";
    private static final String now = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date());
    private static final String SUBJECT = "Während Adam lacht, jagen zwölf Boxkämpfer Eva quer über den großen Sylter Deich - für satte 12.345.667,89 €uro";
    private static final String CONTENT = "Während Adam lacht, jagen zwölf Boxkämpfer Eva quer über den großen Sylter Deich - für satte 12.345.667,89 €uro";
    private static final String SENDER = "DEV_ARUN_PULL_SENDER2";
    private static final String SENDER_PLUGIN_USER = "Dev_Arun_FSSys1_PluginUser";
    private static final String SENDER_PLUGIN_PASSWORD = "pwd";
    private static final String RECEIVER = "DEV_ARUN_PULL_RECEIVER2";
    private static final String RECEIVER_PLUGIN_USER = "Dev_Arun_FSSys1_PluginUser";
    private static final String RECEIVER_PLUGIN_PASSWORD = "pwd";


    public static void main(String[] args) throws IOException, SOAPException {

        long starttime = System.currentTimeMillis();
        Path sendMessageBundleDirectory = createSendMessageBundleDirectory();

        StringBuilder sendMessageBundleRequest = new StringBuilder().append(createMessageBundleInitialPart());


        testWithBinaryFiles(sendMessageBundleRequest, sendMessageBundleDirectory);
        //testWithGermanSpecialChars(sendMessageBundleRequest, sendMessageBundleDirectory);

        sendMessageBundleRequest.append(createMessageBundleFinalPart());
        long endtime = System.currentTimeMillis();
        System.out.println("Bundle generation took:" + ((endtime - starttime) / 1000) + "seconds");
        System.out.println("sendMessageBundleRequest:" + sendMessageBundleRequest.toString());

        invokeSendMessageBundleRequest(sendMessageBundleRequest.toString());

    }

    private static void testWithGermanSpecialChars(StringBuilder sendMessageBundleRequest, Path sendMessageBundleDirectory) throws IOException {
        for (int i = 0; i < NO_OF_FILES_IN_BUNDLE; i++) {
            WrapperFile wrapperFile = new WrapperFile(BUNDLE_ID + "_file" + i + ".txt", WrapperFileType.BINARY, "BundesratFile" + i + ".txt", "application/txt");
            wrapperFile.createGermanSpecialCharsFile(sendMessageBundleDirectory, BUNDLE_ID);
            sendMessageBundleRequest.append(wrapperFile.toXML());
        }
    }

    private static void testWithBinaryFiles(StringBuilder sendMessageBundleRequest, Path sendMessageBundleDirectory) throws IOException {
        for (int i = 0; i < NO_OF_FILES_IN_BUNDLE; i++) {
            WrapperFile wrapperFile = new WrapperFile("basicTest.txt"+BUNDLE_ID + "_file" + i + ".bin", WrapperFileType.BINARY, "BinaryFile" + i + ".bin", "application/octet-stream");
            wrapperFile.createRandomBinaryFile(sendMessageBundleDirectory, BUNDLE_ID, SIZE_PER_FILE);
            sendMessageBundleRequest.append(wrapperFile.toXML());
        }
    }

    private static void invokeSendMessageBundleRequest(String sendMessageBundleRequest) throws SOAPException, IOException {
        MessageFactory messageFactory = MessageFactory.newInstance();
        InputStream is = new ByteArrayInputStream(sendMessageBundleRequest.getBytes(Charset.forName("UTF-8")));
        MimeHeaders mimeHeaders = new MimeHeaders();
        String basicAuthentication = "Basic " + Base64.getEncoder().encodeToString((SENDER_PLUGIN_USER + ":" + SENDER_PLUGIN_PASSWORD).getBytes());
        mimeHeaders.addHeader("SOAPAction", "http://ec.europa.eu/e-trustex/integration/services/outbox/OutboxService/v2.0/sendMessageBundle");
        mimeHeaders.addHeader("Authorization", basicAuthentication);


        SOAPMessage soapRequest = messageFactory.createMessage(mimeHeaders, is);
        System.out.println("##########SOAP request object############");
        soapRequest.writeTo(System.out);
        System.out.println("\n##########SOAP request object############");

        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        SOAPConnection soapConnection = soapConnectionFactory.createConnection();
        SOAPMessage soapResponse = soapConnection.call(soapRequest, new URL(C2_BACKENDPLUGIN_OUTBOXSERVICE_URL));
        System.out.println("##########SOAP response object############");
        soapResponse.writeTo(System.out);
        System.out.println("\n##########SOAP response object############");
    }

    private static String createMessageBundleFinalPart() {
        return "</common:fileMetadataList></outbox:messageBundle></outbox:sendMessageBundleRequest></soapenv:Body></soapenv:Envelope>";
    }

    private static String createMessageBundleInitialPart() {
        StringBuilder sendMessageBundleRequest = new StringBuilder()
                .append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" >")
                .append("<soapenv:Header/>")
                .append("<soapenv:Body wsu:Id=\"id-1\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">")
                .append("<outbox:sendMessageBundleRequest xmlns:outbox=\"urn:eu:europa:ec:etrustex:integration:service:outbox:v2.0\" xmlns:common=\"urn:eu:europa:ec:etrustex:integration:model:common:v2.0\" >")
                .append("<outbox:messageBundle>")
                .append("<common:id>").append(BUNDLE_ID).append("</common:id>")
                .append("<common:referenceId>").append(REFERENCE_ID).append("</common:referenceId>")
                .append("<common:issueDateTime>").append(now).append("</common:issueDateTime>")
                .append("<common:receivedDateTime>").append(now).append("</common:receivedDateTime>")
                .append("<common:subject>").append(SUBJECT).append("</common:subject>")
                .append("<common:content>").append(CONTENT).append("</common:content>")
                .append("<common:sender>").append("<common:id>").append(SENDER).append("</common:id>").append("</common:sender>")
                .append("<common:receiverList><common:recipient><common:id>").append(RECEIVER).append("</common:id></common:recipient></common:receiverList>")
                .append("<common:fileMetadataList>");
        return sendMessageBundleRequest.toString();
    }


    private static Path createSendMessageBundleDirectory() throws IOException {
        File sendBundleDirectory = new File(APPLICATION_FILES_ROOT + File.separator + SENDER_SYSTEM_NAME + File.separator + "out" + File.separator + SENDER + File.separator + BUNDLE_ID);
        if (!sendBundleDirectory.exists()) {
            Path sendBundleDirectoryPath = Files.createDirectories(sendBundleDirectory.toPath());
            System.out.println("Created outgoing MESSAGE_BUNDLE Directory structure:" + sendBundleDirectoryPath.toString());
            return sendBundleDirectoryPath;
        }
        return null;
    }
}

package eu.europa.ec.etrustex;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;

/**
 * To update the last modified date time of all files in a directory including subdirectories by X number of days.
 */
public class UpdatLastModifiedDateTime {
    private static final Long DAY_IN_MS = 24L * 60L * 60L * 1000L;

    public static void main(String[] ags) throws IOException {
        Path dirPath = Paths.get("E:/workspace/Servers/wildfly12_singleserver/CommonStorage/");
        Files.walkFileTree(dirPath, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                System.out.println("Inside directory:{" + dirPath + "}, initiating updatePathAccessTimes of file:{" + file + "}");
                updatePathAccessTimes(file, -84 * DAY_IN_MS);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                System.out.println("Initiating updatePathAccessTimes of directory:{" + dir + "}");
                updatePathAccessTimes(dir, -13 * DAY_IN_MS);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    private static void updatePathAccessTimes(Path path, long timeInMillis) throws IOException {
        final BasicFileAttributeView fileAttributeView = Files.getFileAttributeView(path, BasicFileAttributeView.class);
        final BasicFileAttributes fileAttributes = fileAttributeView.readAttributes();
        FileTime newTime = FileTime.fromMillis(fileAttributes.lastModifiedTime().toMillis() + timeInMillis);
        fileAttributeView.setTimes(newTime, newTime, newTime);
    }
}

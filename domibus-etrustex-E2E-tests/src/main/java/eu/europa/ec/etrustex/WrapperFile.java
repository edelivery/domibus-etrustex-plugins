package eu.europa.ec.etrustex;

import com.google.common.hash.Hashing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author Arun Raj
 * @version 1.0
 * @since 05/02/2020
 */
public class WrapperFile {
    private static final SecureRandom secureRandom = new SecureRandom();

    private String id;
    private WrapperFileType wrapperFileType;
    private String relativePath = "RELATIVE PATH";
    private String name;
    private String mimeType;
    private long size;
    private String checksum;
    private WrapperFileChecksumAlgorithm checksumAlgorithm;

    public WrapperFile(String id, WrapperFileType wrapperFileType, String name, String mimeType) {
        this.id = id;
        this.wrapperFileType = wrapperFileType;
        this.name = name;
        this.mimeType = mimeType;
    }

    public void createRandomBinaryFile(Path bundleDirectory, String BUNDLE_ID, String fileSize) throws IOException {

        File wrapperUnzipped = Paths.get(bundleDirectory.toString(), id).toFile();
        System.out.println("Going to create non compressible binary file:" + wrapperUnzipped.getAbsolutePath());
        wrapperUnzipped.createNewFile();

        Properties blockSizeIterationsProps = computeArrayBlockSizeAndIterations(fileSize);

        try (FileOutputStream wrapperUnzippedOutputStream = new FileOutputStream(wrapperUnzipped)) {
            for (int i = 0; i < Integer.parseInt(blockSizeIterationsProps.getProperty("ITERATIONS_COUNT")); i++) {
                byte[] randomByteArray = new byte[Integer.parseInt(blockSizeIterationsProps.getProperty("BLOCK_SIZE"))];
                secureRandom.nextBytes(randomByteArray);
                wrapperUnzippedOutputStream.write(randomByteArray);
            }
        }
        System.out.println("Created File: " + wrapperUnzipped.getAbsolutePath() + " of size:" + wrapperUnzipped.length() + " bytes.");
        size = wrapperUnzipped.length();

        String wrapperUnzippedSHA512Checksum = com.google.common.io.Files.asByteSource(wrapperUnzipped).hash(Hashing.sha512()).toString().toUpperCase();
        System.out.println("SHA-512 checksum of wrapper created:" + wrapperUnzippedSHA512Checksum);

        checksumAlgorithm = WrapperFileChecksumAlgorithm.SHA_512;
        checksum = wrapperUnzippedSHA512Checksum;

        //Zip the wrapper file
        String wrapperZippedFileName = wrapperUnzipped.getCanonicalPath() + ".zip";
        try (ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(new File(wrapperZippedFileName)));
             FileInputStream sourceFIS = new FileInputStream(wrapperUnzipped);
        ) {
            ZipEntry wrapperZippedFile = new ZipEntry(wrapperUnzipped.getName());
            zipOutputStream.putNextEntry(wrapperZippedFile);
            byte[] bytes = new byte[Integer.parseInt(blockSizeIterationsProps.getProperty("BLOCK_SIZE"))];
            int length;
            while((length = sourceFIS.read(bytes)) >= 0){
                zipOutputStream.write(bytes, 0, length);
            }
            zipOutputStream.closeEntry();
        }

        //delete the unzipped file
        wrapperUnzipped.delete();
    }

    private static Properties computeArrayBlockSizeAndIterations(String fileSize) {
        int arrayBlockSize = 0;
        int iterationsToBuildFile = 0;

        if (fileSize == null) {
            throw new NullPointerException("FileSize cannot be empty or 0");
        }
        try {
            int intFileSize = Integer.parseInt(fileSize); //scenario when filesize is specified in bytes without K, M, G
            if (intFileSize <= 0) {
                throw new NullPointerException("FileSize cannot be empty or 0");
            } else if (intFileSize > 0 && intFileSize < 1024) {
                arrayBlockSize = intFileSize;
                iterationsToBuildFile = 1;
            } else if (intFileSize > 1024) {
                arrayBlockSize = 1024;
                iterationsToBuildFile = intFileSize % 1024;
            }
        } catch (NumberFormatException nfe) {

            if (fileSize.matches(".*[kKmMgG]")) { //scenario when filesize contains K, M, G

                int numValueInFileSize = Integer.parseInt(fileSize.substring(0, fileSize.length() - 1).trim());
                if (fileSize.endsWith("K") || fileSize.endsWith("k")) {
                    arrayBlockSize = 1024;
                    iterationsToBuildFile = numValueInFileSize;
                }
                if (fileSize.endsWith("M") || fileSize.endsWith("m")) {
                    arrayBlockSize = 1049000;
                    iterationsToBuildFile = numValueInFileSize;
                }
                if (fileSize.endsWith("G") || fileSize.endsWith("g")) {
                    arrayBlockSize = 1074000000;
                    iterationsToBuildFile = numValueInFileSize;
                }
            } else {
                throw new UnsupportedOperationException("Only K, M, G supported! ");
            }
        }



        Properties blockSizeIterationsProps = new Properties();
        blockSizeIterationsProps.put("BLOCK_SIZE", Integer.toString(arrayBlockSize));
        blockSizeIterationsProps.put("ITERATIONS_COUNT", Integer.toString(iterationsToBuildFile));

        return blockSizeIterationsProps;
    }


    public String toXML() {
        StringBuilder xml = new StringBuilder("<commonFile:file xmlns:commonFile=\"urn:eu:europa:ec:etrustex:integration:model:common:v2.0\">")
                .append("<commonFile:id>").append(id).append("</commonFile:id>")
                .append("<commonFile:fileType>").append(wrapperFileType).append("</commonFile:fileType>")
                .append("<commonFile:relativePath>").append(relativePath).append("</commonFile:relativePath>")
                .append("<commonFile:name>").append(name).append("</commonFile:name>")
                .append("<commonFile:mimeType>").append(mimeType).append("</commonFile:mimeType>")
                .append("<commonFile:size>").append(Long.toString(size)).append("</commonFile:size>")
                .append("<commonFile:checksum algorithm=\"").append(checksumAlgorithm.getAlgoName()).append("\">").append(checksum).append("</commonFile:checksum>")
                .append("</commonFile:file>");
        return xml.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public WrapperFileType getWrapperFileType() {
        return wrapperFileType;
    }

    public void setWrapperFileType(WrapperFileType wrapperFileType) {
        this.wrapperFileType = wrapperFileType;
    }

    public String getRelativePath() {
        return relativePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public long getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public WrapperFileChecksumAlgorithm getChecksumAlgorithm() {
        return checksumAlgorithm;
    }

    public void setChecksumAlgorithm(WrapperFileChecksumAlgorithm checksumAlgorithm) {
        this.checksumAlgorithm = checksumAlgorithm;
    }

    public void createGermanSpecialCharsFile(Path sendMessageBundleDirectory, String bundleId) throws IOException {
        File wrapperUnzipped = Paths.get(sendMessageBundleDirectory.toString(), id).toFile();
        System.out.println("Going to create non compressible German Special chars file:" + wrapperUnzipped.getAbsolutePath());
        wrapperUnzipped.createNewFile();

        try (FileOutputStream wrapperUnzippedOutputStream = new FileOutputStream(wrapperUnzipped)) {
            String testData = "Während Adam lacht, jagen zwölf Boxkämpfer Eva quer über den großen Sylter Deich - für satte 12.345.667,89 €uro\n" +
                    "¡ ¢ £ € ¥ Š § š © ª « ¬ SHY ® ° ± ² ³ Ž µ ¶ · ž ¹ º » Œ œ Ÿ ¿   À Á Â Ã Ä Å Æ Ç È É Ê Ë Ì Í Î Ï Ð Ñ Ò Ó Ô Õ Ö × Ø Ù Ú Û Ü Ý Þ ß à á â ã ä å æ ç è é ê ë ì í î ï  ð ñ ò ó ô õ ö ÷ ø ù ú û ü ý þ ÿ ";
            wrapperUnzippedOutputStream.write(testData.getBytes());
        }
        System.out.println("Created File: " + wrapperUnzipped.getAbsolutePath() + " of size:" + wrapperUnzipped.length() + " bytes.");
        size = wrapperUnzipped.length();

        String wrapperUnzippedSHA512Checksum = com.google.common.io.Files.asByteSource(wrapperUnzipped).hash(Hashing.sha512()).toString().toUpperCase();
        System.out.println("SHA-512 checksum of wrapper created:" + wrapperUnzippedSHA512Checksum);

        checksumAlgorithm = WrapperFileChecksumAlgorithm.SHA_512;
        checksum = wrapperUnzippedSHA512Checksum;

        //Zip the wrapper file
        String wrapperZippedFileName = wrapperUnzipped.getCanonicalPath() + ".zip";
        try (ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(new File(wrapperZippedFileName)));
             FileInputStream sourceFIS = new FileInputStream(wrapperUnzipped);
        ) {
            ZipEntry wrapperZippedFile = new ZipEntry(wrapperUnzipped.getName());
            zipOutputStream.putNextEntry(wrapperZippedFile);
            byte[] bytes = new byte[1024];
            int length;
            while((length = sourceFIS.read(bytes)) >= 0){
                zipOutputStream.write(bytes, 0, length);
            }
            zipOutputStream.closeEntry();
        }

        //delete the unzipped file
        //wrapperUnzipped.delete();
    }
}

enum WrapperFileType {
    BINARY,
    METADATA
}

enum WrapperFileChecksumAlgorithm {

    SHA_512("SHA-512");

    private String algoName;

    WrapperFileChecksumAlgorithm(String algoName) {
        this.algoName = algoName;
    }

    public String getAlgoName() {
        return algoName;
    }
}
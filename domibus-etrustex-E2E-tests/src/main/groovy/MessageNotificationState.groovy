enum MessageNotificationState {
    NF_PENDING,
    NF_AUTO_PENDING,
    NF_NOTIFIED,
    NF_EXPIRED
}

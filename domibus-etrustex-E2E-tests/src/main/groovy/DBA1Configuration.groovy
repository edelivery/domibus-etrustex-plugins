import org.hamcrest.CoreMatchers
import report.Report
import service.EnvUtils
import service.EtrustExSqlService
import service.MockFileRepo

import static report.Assert.assertThat

class DBA1Configuration {
    public static final String FILE_REPO_URL = 'etx.backend.services.FileRepositoryService.url'
    EtrustExSqlService sql
    MockFileRepo mock
    Report report
    def sysId
    def log

    DBA1Configuration(testRunner, sql, log) {
        this.sql = sql
        this.log = log
        mock = new MockFileRepo(testRunner)
        report = new Report(this.sql.getUrl(), null)
    }

    /**
     * context.expand('${Properties#A1_USERNAME}').toString()
     * @return
     */
    Report checkConfig(username) {
        def rows = sql.getSystemConfigBySysId(getSysId(username))
        rows.each { row ->
            if (row.get("SCG_PROPERTY_NAME").equals(FILE_REPO_URL)) {
                report.add(assertThat(FILE_REPO_URL + " in DB should have correct value for sender " + username, row.get("SCG_PROPERTY_VALUE").toString(), CoreMatchers.is(getUrl())))
            }
        }
        return report
    }

    private String getUrl() {
        return 'http://' + EnvUtils.getComputerName() + ':' + mock.getPort() + mock.getPath()
    }

    def getSysId(username) {
        if (!this.sysId) {
            def user = sql.getUserByName(username)
            def sys = sql.getSystemByUserId(user.getAt("USR_ID").get(0))
            this.sysId = sys.getAt("SYS_ID").get(0)
        }
        return this.sysId
    }

    void updateFileRepoUrl(username) {
        log.info 'username: '+username+ ' | getSysId(username): '+getSysId(username)+'| getUrl(): '+getUrl()
        boolean updated = sql.updateSysConfig(getSysId(username), getUrl(), log)
        log.info 'updated?'+ updated
    }
}

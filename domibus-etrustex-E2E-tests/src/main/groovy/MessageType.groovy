/**
 * @author Arun Raj
 * @version 1.0
 * @since 04/04/2018
 */
enum MessageType {
    MESSAGE_BUNDLE,
    MESSAGE_STATUS,
    MESSAGE_ADAPTER_STATUS
}
import dto.Request
import report.Report
import service.ConnectionData
import service.DomibusSqlService
import service.EtrustExNodeSqlService
import service.EtrustExSqlService

class EndToEndAssertions {

    enum State{

    }
    def log
    def context
    def testRunner
    private EtrustExSqlService etrustExSqlService
    private EtrustExSqlService etrustExAdapterSqlService
    private DomibusSqlService a1DomibusSqlService
    private DomibusSqlService anDomibusSqlService
    private EtrustExNodeSqlService etrustExNodeSqlService
    private Report report
    private boolean isA1Plugin
    private boolean isA2Plugin

    EndToEndAssertions(log, context, testRunner) {
        this.testRunner = testRunner
        this.context = context
        this.log = log
        this.report = new Report("End2End Test")
        this.isA1Plugin = context.expand('${Properties#GLOBAL_ADAPTOR_AS_A1}') != 'true' // value passed back from context is not exactly a boolean. Hence, this hack.
        this.isA2Plugin = context.expand('${Properties#GLOBAL_ADAPTOR_AS_A2}') != 'true'
    }

    // Ensure db connections are open and ready
    void init(){
        log.info 'initialisation'
        initA1DBEtrustex()
        log.info "value of GLOBAL_ADAPTOR_AS_A1 is: " + isA1Plugin
        log.info "value of GLOBAL_ADAPTOR_AS_A2 is: " + isA2Plugin
        if(isA1Plugin) {
            initA1DBDomibus()
            initANDBDomibus()
        }
        initEtrustexNodeDB()
        initA2DBEtrustex()
        initEtrustexAdapterDB()
    }

    // generate reports
    Report generateReport(){
        Request request = new SendMessageBundle().parseMessageSent(testRunner.testCase.testSteps['SendMessageBundleR1'])
        Request request2 = new SendMessageBundle().parseMessageSent(testRunner.testCase.testSteps['SendMessageBundleR2'])

        A1EtrustExAssertions a1EtrustexAssertion
        A1EtrustExAssertions a1EtrustexAssertion2

        // for consistency - kept the method signature the same. Repetitive and verbose.
        // otherwise, the top level assertions will take in different parameters.
        /***** To be looked into later. *****/

        if(isA1Plugin) {
            a1EtrustexAssertion = new A1EtrustExAssertions(etrustExSqlService, request, log)
            a1EtrustexAssertion2 = new A1EtrustExAssertions(etrustExSqlService, request2, log)
        }else { //isA1Adapter
            a1EtrustexAssertion = new A1EtrustExAssertions(etrustExAdapterSqlService, request, log)
            a1EtrustexAssertion2 = new A1EtrustExAssertions(etrustExAdapterSqlService, request2, log)
        }


        log('A1-EtrustEx')
        this.report.add(a1EtrustexAssertion.checkMessageBundle())
        this.report.add(a1EtrustexAssertion2.checkMessageBundle())
        this.report.add(a1EtrustexAssertion.checkMessageStatus())
        this.report.add(a1EtrustexAssertion2.checkMessageStatus())

        if(isA1Plugin) {
            log('A1-Domibus')
            this.report.add(new A1DomibusAssertions(a1DomibusSqlService, log).checkMessageBundle(a1EtrustexAssertion.getDomibusMessageIdOfBundle()))
            this.report.add(new A1DomibusAssertions(a1DomibusSqlService, log).checkMessageBundle(a1EtrustexAssertion2.getDomibusMessageIdOfBundle()))

            this.report.add(new A1DomibusAssertions(a1DomibusSqlService, log).checkMessageAttachement(a1EtrustexAssertion.getDomibusMessageIdOfAttachments()))
            this.report.add(new A1DomibusAssertions(a1DomibusSqlService, log).checkMessageAttachement(a1EtrustexAssertion2.getDomibusMessageIdOfAttachments()))

            log('AN-Domibus')
            this.report.add(new A1DomibusAssertions(anDomibusSqlService, log).checkMessageBundle(a1EtrustexAssertion.getDomibusMessageIdOfBundle()))
            this.report.add(new A1DomibusAssertions(anDomibusSqlService, log).checkMessageBundle(a1EtrustexAssertion2.getDomibusMessageIdOfBundle()))

            this.report.add(new A1DomibusAssertions(anDomibusSqlService, log).checkMessageAttachement(a1EtrustexAssertion.getDomibusMessageIdOfAttachments()))
            this.report.add(new A1DomibusAssertions(anDomibusSqlService, log).checkMessageAttachement(a1EtrustexAssertion2.getDomibusMessageIdOfAttachments()))
        }
        log('Node')
        this.report.add(new NodeAssertions(etrustExNodeSqlService, request).checkMessage())

        A2EtrustExAssertions a2EtrustexAssertion
        A2EtrustExAssertions a2EtrustexAssertion2

        if(isA2Plugin) {
            a2EtrustexAssertion = new A2EtrustExAssertions(etrustExSqlService, request, log)
            a2EtrustexAssertion2 = new A2EtrustExAssertions(etrustExSqlService, request2, log)
        }else { //isA2Adapter
            a2EtrustexAssertion = new A2EtrustExAssertions(etrustExAdapterSqlService, request, log)
            a2EtrustexAssertion2 = new A2EtrustExAssertions(etrustExAdapterSqlService, request2, log)
        }

        log('A2-EtrustEx')
        this.report.add(a2EtrustexAssertion.checkMessageBundle())
        this.report.add(a2EtrustexAssertion2.checkMessageStatus())

        return this.report
    }


    private void log(String env) {
        log.info '--------------------------------'
        log.info '---------- Check ' + env + ' ----------'
        log.info '--------------------------------'
    }

    // Ensure that connections can be successfully obtained to all DBs
    void initA1DBEtrustex(){
        def connection = ConnectionData.A1_ETRUSTEX.with(context)
        log.debug 'Init A1 DB EtrustEx: ' + connection.toString()
        etrustExSqlService = new EtrustExSqlService(connection)
        log.info 'A1 EtrustEx OK'
    }

    void initA2DBEtrustex(){
        def connection = ConnectionData.A2_ETRUSTEX.with(context)
        log.debug 'Init A2 DB EtrustEx: ' + connection.toString()
        etrustExAdapterSqlService = new EtrustExSqlService(connection)
        log.info 'A2 EtrustEx OK'
    }

    void initA1DBDomibus(){
        def connection = ConnectionData.A1_DOMIBUS.with(context)
        log.debug 'Init A1DBDomibus Node DB: ' : connection.toString()
        a1DomibusSqlService = new DomibusSqlService(connection)
        log.info 'A1 Domibus OK'
    }
    void initANDBDomibus(){
        def connection = ConnectionData.AN_DOMIBUS.with(context)
        log.debug 'Init ANDBDomibus Node DB: ' : connection.toString()
        anDomibusSqlService = new DomibusSqlService(connection)
        log.info 'AN Domibus OK'
    }
    void initEtrustexNodeDB(){
        def connection = ConnectionData.N_NODE.with(context)
        log.debug 'Init EtrustEx Node DB: ' : connection.toString()
        etrustExNodeSqlService = new EtrustExNodeSqlService(connection)
        log.info 'EtrustEx Node OK'
    }

    void initEtrustexAdapterDB(){
        def connection = ConnectionData.ETRUSTEX_ADAPTER.with(context)
        log.debug 'Init DB EtrustEx Adapter: ' + connection.toString()
        etrustExAdapterSqlService = new EtrustExSqlService(connection)
        log.info 'EtrustEx Adapter OK'
    }
}

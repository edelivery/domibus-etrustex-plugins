import dto.Request

interface SendMessage {

    Request parseMessageSent(requestNode)

}
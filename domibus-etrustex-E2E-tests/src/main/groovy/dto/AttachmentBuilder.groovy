package dto

class AttachmentBuilder {
    String id
    String size
    String mimeType
    String name
    String checksum

    static AttachmentBuilder getInstance(){
        return new AttachmentBuilder()
    }

    Attachment build() {
        return new Attachment(id, size, mimeType, name, checksum)
    }

    AttachmentBuilder withId(String id) {
        this.id = id
        return this
    }

    AttachmentBuilder withSize(String size) {
        this.size = size
        return this
    }

    AttachmentBuilder withMimeType(String mimeType) {
        this.mimeType = mimeType
        return this
    }

    AttachmentBuilder withName(String name) {
        this.name = name
        return this
    }

    AttachmentBuilder withChecksum(String checksum) {
        this.checksum = checksum
        return this
    }
}

package dto

class Attachment {
    String id
    String size
    String mimeType
    String name
    String checksum

    Attachment(String id, String size, String mimeType, String name, String checksum) {
        this.id = id
        this.size = size
        this.mimeType = mimeType
        this.name = name
        this.checksum = checksum
    }


    @Override
    public String toString() {
        return "Attachment{" +
                "id='" + id + '\'' +
                ", size='" + size + '\'' +
                ", mimeType='" + mimeType + '\'' +
                ", name='" + name + '\'' +
                ", checksum='" + checksum + '\'' +
                '}';
    }
}

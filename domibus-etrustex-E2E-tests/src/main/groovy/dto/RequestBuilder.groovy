package dto

class RequestBuilder {
    String referenceId
    String sender
    List<String> receivers
    String id
    String issueDateTime
    String receivedDateTime
    String subject
    String content
    List<Attachment> fileMetadataList
    String signedData
    String signature
    
    static RequestBuilder getInstance(){
        return new RequestBuilder()
    }
    Request build() {
        return new Request(referenceId, sender, receivers, id, issueDateTime, receivedDateTime, subject, content, fileMetadataList, signedData, signature)
    }

    RequestBuilder withReferenceId(String referenceId) {
        this.referenceId = referenceId
        return this
    }

    RequestBuilder withSender(String sender) {
        this.sender = sender
        return this
    }

    RequestBuilder withReceivers(List<String> receivers) {
        this.receivers = receivers
        return this
    }

    RequestBuilder withId(String id) {
        this.id = id
        return this
    }

    RequestBuilder withIssueDateTime(String issueDateTime) {
        this.issueDateTime = issueDateTime
        return this
    }

    RequestBuilder withReceivedDateTime(String receivedDateTime) {
        this.receivedDateTime = receivedDateTime
        return this
    }

    RequestBuilder withSubject(String subject) {
        this.subject = subject
        return this
    }

    RequestBuilder withContent(String content) {
        this.content = content
        return this
    }

    RequestBuilder withFileMetadataList(List<Attachment> fileMetadataList) {
        this.fileMetadataList = fileMetadataList
        return this
    }

    RequestBuilder withSignedData(String signedData) {
        this.signedData = signedData
        return this
    }

    RequestBuilder withSignature(String signature) {
        this.signature = signature
        return this
    }
}

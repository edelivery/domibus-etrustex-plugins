package dto

class Request {
    String referenceId
    String sender
    List<String> receivers
    String id
    String issueDateTime
    String receivedDateTime
    String subject
    String content
    List<Attachment> fileMetadataList
    String signedData
    String signature


    Request(String referenceId, String sender, List<String> receivers, String id, String issueDateTime, String receivedDateTime, String subject, String content, List<Attachment> fileMetadataList, String signedData, String signature) {
        this.referenceId = referenceId
        this.sender = sender
        this.receivers = receivers
        this.id = id
        this.issueDateTime = issueDateTime
        this.receivedDateTime = receivedDateTime
        this.subject = subject
        this.content = content
        this.fileMetadataList = fileMetadataList
        this.signedData = signedData
        this.signature = signature


    }

    @Override
    public String toString() {
        return "Request{" +
                "referenceId='" + referenceId + '\'' +
                ", sender='" + sender + '\'' +
                ", receivers='" + receivers + '\'' +
                ", id='" + id + '\'' +
                ", issueDateTime='" + issueDateTime + '\'' +
                ", receivedDateTime='" + receivedDateTime + '\'' +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                ", fileMetadataList=" + fileMetadataList +
                ", signedData='" + signedData + '\'' +
                ", signature='" + signature + '\'' +
                '}';
    }
}

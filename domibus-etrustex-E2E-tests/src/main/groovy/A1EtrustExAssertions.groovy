import dto.Attachment
import dto.Request
import groovy.sql.GroovyRowResult
import org.hamcrest.CoreMatchers
import report.Report
import service.EtrustExSqlService

import static report.Assert.assertThat

class A1EtrustExAssertions {
    private EtrustExSqlService sqlService
    private Request request
    private Report report
    def log
    private List<GroovyRowResult> bundleMessages
    private List<GroovyRowResult> statusMessages
    private List<GroovyRowResult> statusMessages_read

    // Constructor
    A1EtrustExAssertions(EtrustExSqlService etrustEx, Request request, log) {
        this.sqlService = etrustEx
        this.request = request
        this.report = new Report("A1EtrustExAssertions")
        this.log = log
    }

    /* Fetch the Domibus Message Bundle ID */
    String getDomibusMessageIdOfBundle() {
        /* Unnecessary to execute the query below because message object is visible throughout the class and is populated in the calling class through the checkmessage() method */
        if (!bundleMessages) {
            // messages = sqlService.collectMsgId(request.getId(), "OUT", request.getSender(), request.getReceivers())
            log.info "-------------- Message is NULL ------------- getDomibusMessageIdOfBundle ---------------- "
        }
        /* Query results are always intended to return a single row. Hence, negating the need for iterating through multiple entities in a list */
        def domibusMessageId = bundleMessages.get(0).get("DOMIBUS_MESSAGEID").toString()

        // assert not null
        if(domibusMessageId != null && !domibusMessageId.isEmpty()){
            log.info "------- The value of domibusMessageId is not NULL ------"
        }
        report.add(assertThat("Domibus Message ID is null", domibusMessageId, CoreMatchers.notNullValue()))

        return domibusMessageId
    }

    /* Fetch the Domibus Message Attachment ID */
    List<String> getDomibusMessageIdOfAttachments() {
        if (!bundleMessages) {
            log.info "-------------- Message is NULL -------------"
            // messages = sqlService.collectMsgId(request.getId(), "OUT", request.getSender(), request.getReceivers())
        }
        def msgId = bundleMessages.get(0).get("MSG_ID").toString()

        // assert not null
        if(msgId != null && !msgId.isEmpty()){log.info "------- The value of message ID (generic) is not NULL ------"}
        report.add(assertThat("Domibus Attachment ID is null", msgId, CoreMatchers.notNullValue()))

        // get all attachments related to a specific MSG_ID and DOMIBUS_MESSAGEID
        return sqlService.collectAttachments(msgId).collect {
            attachment -> attachment.get('DOMIBUS_MESSAGEID').toString()
        }
    }

    /*  Retrieve message bundles/attachments and verify content is accurate - expected and actual values match (based on
        expected element names in request message) and add assertions to report. */
    Report checkMessageBundle() {

        /* common tasks to be wrapped in a private method */
        def msg_uuid = request.getId()
        def senderParty = request.getSender() // TESTEDEL_EGREFFE_PLUGIN_SENDER1 - configured with Auto-notification
        List<String> receivers = request.getReceivers() // Always returns a single entity as each sendMessageBundle() instance always sends to a single receiver
        String receiverParty = receivers.get(0) // TESTEDEL_EGREFFE_PLUGIN_RECEIVER1/TESTEDEL_EGREFFE_PLUGIN_RECEIVER2

        /*  MessageBundle - extract bundle messages (execute sql query to extract bundle messages - not message statuses)
            based on a specific sender-receiver scenario - resultset should always contain 1 row   */
        bundleMessages = sqlService.collectMsg(msg_uuid, 'OUT', 'MESSAGE_BUNDLE', senderParty, receiverParty)
//        log.info "**senderParty**: " + senderParty + " receiverParty: " + receiverParty
//        log.info "msg_uuid: " + msg_uuid
//        log.info "bundleMessages size is: " + bundleMessages.size()
//        for(int i=0; i<bundleMessages.size(); i++){log.info "||||||||||| bundle contents are ||||||||||: " + bundleMessages.getAt(i)}

        if(!bundleMessages){log.info "-------------- BundleMessages is NULL ------------- checkMessageBundle "}
        report.add(assertThat("Number of messages is: 1", this.bundleMessages.size(), CoreMatchers.is(1)))

        if (this.bundleMessages.size() > 0) {
            GroovyRowResult bundleMessage = this.bundleMessages.get(0)

            def senderParId = bundleMessage.get("MSG_SENDER_PAR_ID").toString() // "41"
            List<GroovyRowResult> senderParties = sqlService.collectParties(senderParId)
            GroovyRowResult senderPartyRow = senderParties.get(0)
            def receiverParId = bundleMessage.get("MSG_RECEIVER_PAR_ID").toString() // "42" or "43"
            List<GroovyRowResult> receiverParties = sqlService.collectParties(receiverParId)
            GroovyRowResult receiverPartyRow = receiverParties.get(0)

//            log.info " -- senderParId (checkMessageBundle) -- " + senderParId
//            log.info " -- receiverParId (checkMessageBundle) -- " + receiverParId

            log.info "--------A1  EtrustEx Assertions - BUNDLE MESSAGES --------"
            report.add(assertThat("MessageBundle State is Incorrect", bundleMessage.get("MSG_STATE").toString(), CoreMatchers.is(MessageState.MS_PROCESSED.name())))
            /* Add assertion for MSG_PARENT_UUID with specific conditions - confirm details */
            report.add(assertThat("MessageBundle REF ID is Incorrect", bundleMessage.get("MSG_REF_UUID").toString(), CoreMatchers.is(request.getReferenceId())))
            report.add(assertThat("MessageBundle MSG_REF_STATE is Incorrect - should be NULL VALUE", bundleMessage.get("MSG_REF_STATE"), CoreMatchers.nullValue()))
            report.add(assertThat("MessageBundle Subject is Incorrect", bundleMessage.get("MSG_SUBJECT").toString(), CoreMatchers.is(request.getSubject())))
            report.add(assertThat("MessageBundle Content is Incorrect", bundleMessage.get("MSG_CONTENT").toString(), CoreMatchers.is(request.getContent())))
            report.add(assertThat("SenderParty/ID is Incorrect", senderPartyRow.get("PAR_UUID").toString(), CoreMatchers.is(senderParty)))
            report.add(assertThat("ReceiverParty/ID is Incorrect", receiverPartyRow.get("PAR_UUID").toString(), CoreMatchers.is(receiverParty)))
            report.add(assertThat("MessageBundle MSG_NOTIFICATION_STATE is Incorrect - should be NULL VALUE", bundleMessage.get("MSG_NOTIFICATION_STATE"), CoreMatchers.nullValue()))
            report.add(assertThat("MessageBundle Message Issue Date is Incorrect", bundleMessage.get("MSG_ISSUE_DT").toString().substring(0, 10), CoreMatchers.is(request.getIssueDateTime().toString().substring(0, 10))))
            report.add(assertThat("MessageBundle Message Issue Time is Incorrect", bundleMessage.get("MSG_ISSUE_DT").toString().substring(12, 19), CoreMatchers.is(request.getIssueDateTime().toString().substring(12, 19))))
            report.add(assertThat("MessageBundle ERR_ID testerchecker is Incorrect - should be NULL VALUE", bundleMessage.get("ERR_ID"), CoreMatchers.nullValue()))
//            log.info "=============**************================= ERR_ID value =============**************=================" + bundleMessage.get("ERR_ID").toString()
//            log.info "=============**************================= ERR_ID value =============**************=================" + bundleMessage.get("ERR_ID")
            /* Add assertion for MSG_SIGNATURE with specific conditions i.e. if EDMA/DECIDE/EGREFFE - confirm details */
            /* Add assertion for MSG_SIGNED_DATA with specific conditions i.e. if EDMA/DECIDE/EGREFFE - confirm details */
            report.add(assertThat("MessageBundle Domibus Message ID is null", bundleMessage.get("DOMIBUS_MESSAGEID").toString(), CoreMatchers.notNullValue()))
            report.add(assertThat("Number of senders found is: 1", senderParties.size(), CoreMatchers.is(1)))
            report.add(assertThat("Number of receivers is not: 1", receiverParties.size(), CoreMatchers.is(1)))

//            log.info "---------Message UUID: ---------" + bundleMessage.get("MSG_UUID")
//            log.info "message value of date: " + bundleMessage.get("MSG_ISSUE_DT").toString()
//            log.info "   " + request.getReceivers() + request.getSender()
//            log.info "request value of date: " + request.getIssueDateTime().toString()

            /* Message Attachments */
            def etxAdtMsgId = bundleMessage.get("MSG_ID").toString()
            List<Attachment> messageAttachments = request.getFileMetadataList()
            Attachment attachments = messageAttachments.get(0)
            List<GroovyRowResult> allAttachments = sqlService.collectAttachments(etxAdtMsgId)
            GroovyRowResult allAttachmentRow = allAttachments.get(0)
            List<GroovyRowResult> allCheckSum = sqlService.collectCheckSum(etxAdtMsgId)
            GroovyRowResult allCheckSumRow = allCheckSum.get(0)

            log.info "--------A1  EtrustEx Assertions - ATTACHMENTS --------"
//
//            log.info "=============**************================= messageAttachments (DB) value =============**************=================" + (messageAttachments.get(0)).getId()
//            log.info "=============**************================= allAttachments (request) value =============**************=================" + (allAttachments.get(0)).get("ATT_UUID")
//            log.info "=============**************================= messageAttachments (DB) value =============**************=================" + (messageAttachments.get(1)).getId()
//            log.info "=============**************================= allAttachments (request) value =============**************=================" + (allAttachments.get(1)).get("ATT_UUID")
//            log.info "=============**************================= messageAttachments (DB) value =============**************=================" + (messageAttachments.get(2)).getId()
//            log.info "=============**************================= allAttachments (request) value =============**************=================" + (allAttachments.get(2)).get("ATT_UUID")
//            log.info "=============**************================= messageAttachments (DB) value =============**************=================" + (messageAttachments.get(3)).getId()
//            log.info "=============**************================= allAttachments (request) value =============**************=================" + (allAttachments.get(3)).get("ATT_UUID")
//            log.info "=============**************================= messageAttachments (DB) value =============**************=================" + (messageAttachments.get(4)).getId()
//            log.info "=============**************================= allAttachments (request) value =============**************=================" + (allAttachments.get(4)).get("ATT_UUID")

            for(int i=0; i<messageAttachments.size(); i++)
                for(int j=0; j<allAttachments.size(); j++) {
                    if( (messageAttachments.get(i)).getId() == allAttachments.get(j).get("ATT_UUID")) {
                        report.add(assertThat("Attachment ID is null", allAttachments.get(j).get("ATT_ID").toString(), CoreMatchers.notNullValue()))  // redundant as conditional check already affirms its non-null state
                        if (receiverParty == "TESTEDEL_EGREFFE_PLUGIN_RECEIVER1" || receiverParty == "TESTEDEL_EGREFFE_ADAPTER_RECEIVER1") {
                            report.add(assertThat("Attachment State is Incorrect", allAttachments.get(j).get("ATT_STATE").toString(), CoreMatchers.is(AttachmentState.ATTS_UPLOADED.name())))
                        } else (report.add(assertThat("Attachment State is Incorrect", allAttachments.get(j).get("ATT_STATE").toString(), CoreMatchers.is(AttachmentState.ATTS_ALREADY_UPLOADED.name()))))
                        report.add(assertThat("Attachment Name is Incorrect", allAttachments.get(j).get("ATT_FILE_NAME").toString(), CoreMatchers.is(messageAttachments.get(j).name)))
                        report.add(assertThat("File Size is Incorrect", allAttachments.get(j).get("ATT_FILE_SIZE").toString(), CoreMatchers.is(messageAttachments.get(j).getSize())))
                        report.add(assertThat("Mime Type is Incorrect", allAttachments.get(j).get("ATT_MIME_TYPE").toString(), CoreMatchers.is(messageAttachments.get(j).getMimeType())))
                        report.add(assertThat("CheckSum is Incorrect", allCheckSumRow.get("RAWTOHEX(ATT_CHECKSUM)"), CoreMatchers.is(messageAttachments.get(j).getChecksum())))
                        report.add(assertThat("ERR_ID is Incorrect - should be NULL VALUE", allAttachments.get(j).get("ERR_ID"), CoreMatchers.nullValue()))
                        report.add(assertThat("MessageBundle Domibus Message ID is null", allAttachments.get(j).get("DOMIBUS_MESSAGEID").toString(), CoreMatchers.notNullValue()))
                        report.add(assertThat("Number of attachments is : 5", allAttachments.size(), CoreMatchers.is(5)))
                    }
                }
        }
        return report
    }

    /*  Retrieve message statuses and verify content is accurate - expected and actual values match (based on
        expected element names in request message) and add assertions to report */
    Report checkMessageStatus() {

        /* common tasks to be wrapped in a private method */
        def msg_ref_uuid = request.getId()
        def bundle_read = msg_ref_uuid + '_READ%'
        def bundle_ok = msg_ref_uuid + '_BDL_OK%'

        def senderParty = request.getSender() // TESTEDEL_EGREFFE_PLUGIN_SENDER1 - configured with Auto-notification
        List<String> receivers = request.getReceivers() // Always returns a single entity as each sendMessageBundle() instance always sends to a single receiver
        String receiverParty = receivers.get(0) // TESTEDEL_EGREFFE_PLUGIN_SENDER1

        /*  MessageStatus - extract status messages (execute sql query to extract status messages - not message bundles)
            based on a specific sender-receiver scenario - resultset should always contain 1 row  */
        statusMessages = sqlService.collectMsgStatus(bundle_ok, 'IN', 'MESSAGE_STATUS', senderParty, receiverParty)
        statusMessages_read = sqlService.collectMsgStatus(bundle_read, 'IN', 'MESSAGE_STATUS', senderParty, receiverParty)

        if(!statusMessages || !statusMessages_read){ log.info "-------------- statusMessages equals NULL ------------- checkMessageStatus " }
//        log.info "senderParty: " + senderParty + " receiverParty: " + receiverParty
//        log.info "msg_ref_uuid: " + msg_ref_uuid
//        log.info "bundle_read: " + bundle_read
//        log.info "bundle_ok: " + bundle_ok
//        log.info "statusMessages (_BDL_OK) size is : " + statusMessages.size()
//        log.info "statusMessages (_READ) size is : " + statusMessages_read.size()
//        for(int i=0; i<statusMessages.size(); i++){log.info "||||||||||| statusMessages contents are ||||||||||: " + statusMessages.getAt(i)}
//        for(int i=0; i<statusMessages_read.size(); i++){log.info "||||||||||| statusMessages_read contents are ||||||||||: " + statusMessages_read.getAt(i)}

        /* Assertions */
        report.add(assertThat("Number of statusMessages (_BDL_OK) is: 1", this.statusMessages.size(), CoreMatchers.is(1)))
        report.add(assertThat("Number of statusMessages_read (_READ) is: 1", this.statusMessages_read.size(), CoreMatchers.is(1)))

        if (this.statusMessages.size() > 0 && this.statusMessages_read.size() > 0) {
            GroovyRowResult statusMessage = this.statusMessages.get(0)
            GroovyRowResult statusMessage_read = this.statusMessages_read.get(0)

            def senderParId = statusMessage.get("MSG_SENDER_PAR_ID").toString() // "41" or "46"
            List<GroovyRowResult> senderParties = sqlService.collectParties(senderParId)
            GroovyRowResult senderPartyRow = senderParties.get(0)
            def receiverParId = statusMessage.get("MSG_RECEIVER_PAR_ID").toString() // "42" or "43" || "44" or "45"
            List<GroovyRowResult> receiverParties = sqlService.collectParties(receiverParId)
            GroovyRowResult receiverPartyRow = receiverParties.get(0)

//            log.info " -- senderParId (checkMessageStatus) -- " + senderParId
//            log.info " -- receiverParId (checkMessageStatus) -- " + receiverParId

            log.info "--------A1  EtrustEx Assertions - STATUS MESSAGES --------"

            /* Status Message _BDL_OK */
            report.add(assertThat("MSG_STATE is Incorrect (STATUS _BDL_OK)", statusMessage.get("MSG_STATE").toString(), CoreMatchers.is(MessageState.MS_PROCESSED.name())))
            /* Add assertion for MSG_PARENT_UUID with specific conditions - confirm details */
            report.add(assertThat("MSG_REF_UUID is Incorrect (STATUS _BDL_OK)", statusMessage.get("MSG_REF_UUID").toString(), CoreMatchers.is(request.getId())))
            report.add(assertThat("MSG_REF_STATE is Incorrect (STATUS _BDL_OK)", statusMessage.get("MSG_REF_STATE").toString(), CoreMatchers.is("AVAILABLE")))
            report.add(assertThat("MSG_SUBJECT is Incorrect (STATUS _BDL_OK)", statusMessage.get("MSG_SUBJECT"), CoreMatchers.nullValue()))
            report.add(assertThat("MSG_CONTENT is Incorrect (STATUS _BDL_OK)", statusMessage.get("MSG_CONTENT").toString(), CoreMatchers.is("Available")))
            report.add(assertThat("SenderParty/ID is Incorrect", senderPartyRow.get("PAR_UUID").toString(), CoreMatchers.is(receiverParty))) //sender becomes the receiver in this scenario and vice-versa
            report.add(assertThat("ReceiverParty/ID is Incorrect", receiverPartyRow.get("PAR_UUID").toString(), CoreMatchers.is(senderParty))) //receiver becomes the sender in this scenario and vice-versa
            report.add(assertThat("MSG_NOTIFICATION_STATE is Incorrect (STATUS _BDL_OK)", statusMessage.get("MSG_NOTIFICATION_STATE").toString(), CoreMatchers.is("NF_NOTIFIED")))
            //report.add(assertThat("MSG_ISSUE_DT is Incorrect (STATUS _BDL_OK)", statusMessage_read.get("MSG_ISSUE_DT").toString().substring(0, 10), CoreMatchers.is(request.getIssueDateTime().toString().substring(0, 10))))
            // report.add(assertThat("MSG_ISSUE_DT is Incorrect (STATUS _BDL_OK)", statusMessage_read.get("MSG_ISSUE_DT").toString().substring(12, 19), CoreMatchers.is(request.getIssueDateTime().toString().substring(12, 19))))
            report.add(assertThat("ERR_ID is Incorrect - should be NULL VALUE", statusMessage.get("ERR_ID"), CoreMatchers.nullValue()))
            /* Add assertion for MSG_SIGNATURE with specific conditions i.e. if EDMA/DECIDE/EGREFFE - confirm details */
            /* Add assertion for MSG_SIGNED_DATA with specific conditions i.e. if EDMA/DECIDE/EGREFFE - confirm details */
            report.add(assertThat("DOMIBUS_MESSAGEID is null (STATUS _BDL_OK)", statusMessage.get("DOMIBUS_MESSAGEID").toString(), CoreMatchers.notNullValue()))

            /* Status Message _READ */
            report.add(assertThat("Message State is Incorrect (STATUS _READ)", statusMessage_read.get("MSG_STATE").toString(), CoreMatchers.is(MessageState.MS_PROCESSED.name())))
            /* Add assertion for MSG_PARENT_UUID with specific conditions - confirm details */
            report.add(assertThat("MSG_REF_UUID is Incorrect (STATUS _BDL_OK)", statusMessage_read.get("MSG_REF_UUID").toString(), CoreMatchers.is(request.getId())))
            report.add(assertThat("Message Ref State is Incorrect (STATUS _READ)", statusMessage_read.get("MSG_REF_STATE").toString(), CoreMatchers.is("READ")))
            report.add(assertThat("MSG_SUBJECT is Incorrect (STATUS _BDL_OK)", statusMessage_read.get("MSG_SUBJECT"), CoreMatchers.nullValue()))
            report.add(assertThat("MSG_CONTENT is Incorrect (STATUS _BDL_OK)", statusMessage_read.get("MSG_CONTENT"), CoreMatchers.nullValue()))
            report.add(assertThat("SenderParty/ID is Incorrect", senderPartyRow.get("PAR_UUID").toString(), CoreMatchers.is(receiverParty)))
            report.add(assertThat("ReceiverParty/ID is Incorrect", receiverPartyRow.get("PAR_UUID").toString(), CoreMatchers.is(senderParty)))
            report.add(assertThat("Message Notification state is Incorrect (STATUS _READ)", statusMessage_read.get("MSG_NOTIFICATION_STATE").toString(), CoreMatchers.is("NF_NOTIFIED")))
            // report.add(assertThat("MessageBundle Message Issue Date is Incorrect (STATUS _READ)", statusMessage.get("MSG_ISSUE_DT").toString().substring(0, 10), CoreMatchers.is(request.getIssueDateTime().toString().substring(0, 10))))
            // report.add(assertThat("MessageBundle Message Issue Time is Incorrect (STATUS _READ)", statusMessage.get("MSG_ISSUE_DT").toString().substring(12, 19), CoreMatchers.is(request.getIssueDateTime().toString().substring(12, 19))))
            report.add(assertThat("ERR_ID is Incorrect - should be NULL VALUE (STATUS _READ)", statusMessage_read.get("ERR_ID"), CoreMatchers.nullValue()))
            /* Add assertion for MSG_SIGNATURE with specific conditions i.e. if EDMA/DECIDE/EGREFFE - confirm details */
            /* Add assertion for MSG_SIGNED_DATA with specific conditions i.e. if EDMA/DECIDE/EGREFFE - confirm details */
            report.add(assertThat("Domibus Message ID is null (STATUS _READ)", statusMessage_read.get("DOMIBUS_MESSAGEID").toString(), CoreMatchers.notNullValue()))

//            log.info "message value of date: " + statusMessage.get("MSG_ISSUE_DT").toString()
//            log.info "   "
//            log.info "request value of date: " + request.getIssueDateTime().toString()
        }
        return report
    }
}
/**
 * @author Arun Raj
 * @version 1.0
 * @since 04/04/2018
 */
enum AttachmentDirection {
    INCOMING, OUTGOING
}
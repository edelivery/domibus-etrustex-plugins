package service

import groovy.sql.Sql
import org.apache.commons.lang.StringUtils
import org.apache.commons.lang.Validate

class SqlService {

    Sql sql
    String url

    SqlService(ConnectionData connection) {
        this(connection.getHostname(),
                connection.getPort(),
                getConn(connection),
                connection.getUser(),
                connection.getPwd())
    }

    private static String getConn(ConnectionData connection) {
        def inter = ""
        if (StringUtils.isNotBlank(connection.getServiceName())) {
            inter = "/" + connection.getServiceName()
        } else if (StringUtils.isNotBlank(connection.getSid())) {
            inter = ":" + connection.getSid()
        }
        inter
    }

    SqlService(String hostname, String port, String serviceName, String user, String pwd) {
        Validate.notEmpty(hostname, port, serviceName, user, pwd)
        this.url = "jdbc:oracle:thin:@" + hostname + ":" + port + serviceName
        //this.url = "jdbc:mysql://" + hostname + ":" + port + serviceName
        this.sql = Sql.newInstance(url, user, pwd)
    }

    void close() {
        if (sql != null) {
            sql.close()
        }
    }

}

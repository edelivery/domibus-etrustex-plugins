package service

import groovy.sql.GroovyRowResult

class EtrustExSqlService extends SqlService {

    def log

    EtrustExSqlService(ConnectionData connection) {
        super(connection)
    }

    /**
     * Get Messages from table ETX_ADT_MESSAGE
     * @param bundleId = MSG_UUID
     * @param direction = OUT or IN
     * @return SELECT * FROM ETX_ADT_MESSAGE WHERE MSG_UUID = bundleId AND MSG_DIRECTION = direction
     * updated to included sender and receiver parties
     */
    List<GroovyRowResult> collectMsgId(bundleId, direction, senderParty, receiverParty) {
        return sql.rows(" Select *  From ETX_ADT_MESSAGE Where MSG_UUID like '" + bundleId + "' AND MSG_DIRECTION = '" + direction + "' AND MSG_SENDER_PAR_ID = '" + senderParty + "' AND MSG_RECEIVER_PAR_ID = '" + receiverParty + "' ")
    }

    /**
     * Get Messages from table ETX_ADT_MESSAGE
     * @param bundleId = MSG_UUID
     * @param direction = OUT or IN
     * @return SELECT * FROM ETX_ADT_MESSAGE WHERE MSG_UUID = bundleId AND MSG_DIRECTION = direction
     * updated to include sender and receiver parties
     */
    List<GroovyRowResult> collectMsgBundleId(msg_uuid, direction, senderParty, receiverParty) {
        return sql.rows(" Select *  From ETX_ADT_MESSAGE Where MSG_UUID = '" + msg_uuid + "' AND MSG_DIRECTION = '" + direction + "' AND MSG_SENDER_PAR_ID = '" + senderParty + "' AND MSG_RECEIVER_PAR_ID = '" + receiverParty + "' ")
    }

    /**
     * Get Messages from table ETX_ADT_MESSAGE
     * @param bundleId = MSG_UUID
     * @param direction = OUT or IN
     * @return SELECT * FROM ETX_ADT_MESSAGE WHERE MSG_UUID = bundleId AND MSG_DIRECTION = direction
     * updated to include sender and receiver parties
     */
//    List<GroovyRowResult> collectMsgStatus(msg_uuid, direction, msg_status_type, senderParty, receiverParty) {
//        return sql.rows(" Select *  From ETX_ADT_MESSAGE Where MSG_UUID like '" + msg_uuid + "' AND MSG_DIRECTION = '" + direction + "' AND MSG_TYPE = '" + msg_status_type + "' AND MSG_SENDER_PAR_ID = '" + senderParty + "' AND MSG_RECEIVER_PAR_ID = '" + receiverParty + "' ")
//    }

    @Deprecated
    /**
     * TO BE REMOVED AS THIS IS DUPLICATE OF collectMsg(msg_uuid, direction, msg_type, senderParty, receiverParty)
     * Get Messages from table ETX_ADT_MESSAGE
     * @param msg_ref_uuid = MSG_REF_UUID
     * @param direction = OUT or IN
     * @return Select eam.* from ETX_ADT_MESSAGE eam, ETX_ADT_PARTY eap_sender, ETX_ADT_PARTY eap_receiver where eam.MSG_TYPE='MESSAGE_STATUS' and eam.MSG_DIRECTION='IN' and eam.MSG_REF_UUID = 'BUNDLE_ID_f89276a8-ad8d-4eca-a9ee-9e6615983c00' and eam.MSG_SENDER_PAR_ID = eap_sender.PAR_ID and eap_sender.PAR_UUID= 'TESTEDEL_EGREFFE_PLUGIN_RECEIVER1' and eam.MSG_RECEIVER_PAR_ID = eap_receiver.PAR_ID and eap_receiver.PAR_UUID= 'TESTEDEL_EGREFFE_PLUGIN_SENDER1';
     */
    List<GroovyRowResult> collectMsgStatus(msg_uuid, direction, msg_type, senderParty, receiverParty) {
        return sql.rows(" Select eam.* from ETX_ADT_MESSAGE eam, ETX_ADT_PARTY eap_sender, ETX_ADT_PARTY eap_receiver where eam.MSG_UUID like '" + msg_uuid + "' AND eam.MSG_DIRECTION = '" + direction + "' AND eam.MSG_TYPE = '" + msg_type + "' AND eam.MSG_SENDER_PAR_ID = eap_sender.PAR_ID AND eam.MSG_RECEIVER_PAR_ID = eap_receiver.PAR_ID AND eap_receiver.PAR_UUID = '" + senderParty + "' AND eap_sender.PAR_UUID = '" + receiverParty + "' ")
    }

    /**
     * Get Messages from table ETX_ADT_MESSAGE
     * @param bundleId = MSG_UUID
     * @param direction = OUT or IN
     * @return Select eam.* from ETX_ADT_MESSAGE eam, ETX_ADT_PARTY eap_sender, ETX_ADT_PARTY eap_receiver where eam.MSG_TYPE='MESSAGE_BUNDLE' and eam.MSG_DIRECTION='OUT' and eam.MSG_UUID= 'BUNDLE_ID_84a647cf-e0f4-4897-af33-f87ea0c875cf' and eam.MSG_SENDER_PAR_ID = eap_sender.PAR_ID and eap_sender.PAR_UUID= 'TESTEDEL_EGREFFE_PLUGIN_SENDER1' and eam.MSG_RECEIVER_PAR_ID = eap_receiver.PAR_ID and eap_receiver.PAR_UUID= 'TESTEDEL_EGREFFE_PLUGIN_RECEIVER1';
     */
    List<GroovyRowResult> collectMsg(msg_uuid, direction, msg_type, senderParty, receiverParty) {
        return sql.rows(" Select eam.* from ETX_ADT_MESSAGE eam, ETX_ADT_PARTY eap_sender, ETX_ADT_PARTY eap_receiver where eam.MSG_UUID like '" + msg_uuid + "' AND eam.MSG_DIRECTION = '" + direction + "' AND eam.MSG_TYPE = '" + msg_type + "' AND eam.MSG_SENDER_PAR_ID = eap_sender.PAR_ID AND eam.MSG_RECEIVER_PAR_ID = eap_receiver.PAR_ID AND eap_receiver.PAR_UUID = '" + receiverParty + "' AND eap_sender.PAR_UUID = '" + senderParty + "' ")
    }

    List<GroovyRowResult> collectMsgWithMessageReferenceId(msg_ref_id, direction, msg_type, senderParty, receiverParty) {
        return sql.rows(" select eam.* from ETX_ADT_MESSAGE eam, ETX_ADT_PARTY sender, ETX_ADT_PARTY receiver where eam.MSG_REF_UUID = '" + msg_ref_id + "' and eam.MSG_DIRECTION = '" + direction + "' and eam.MSG_TYPE = '" + msg_type + "' and eam.MSG_SENDER_PAR_ID = sender.PAR_ID and sender.PAR_UUID = '" + senderParty + "' and eam.MSG_RECEIVER_PAR_ID = receiver.PAR_ID and receiver.PAR_UUID = '" + receiverParty + "' ")
    }

    /**
     * Get Parties from table ETX_ADT_PARTY
     * @param partyId = PAR_ID
     * @return SELECT * FROM ETX_ADT_PARTY WHERE PAR_ID = partyId
     */
    List<GroovyRowResult> collectParties(partyId) {
        return sql.rows("Select * From ETX_ADT_PARTY Where PAR_ID = '" + partyId + "' ")
    }

    /**
     * Get Parties from table ETX_ADT_PARTY using party names
     * @param partyName = PAR_UUID
     * @return SELECT * FROM ETX_ADT_PARTY WHERE PAR_UUID = partyName
     */
    List<GroovyRowResult> collectPartyNames(partyName) {
        return sql.rows("Select * From ETX_ADT_PARTY Where PAR_UUID = '" + partyName + "' ")
    }

    /**
     * Get System config from table ETX_ADT_SYSTEM_CONFIG
     * @param sysId = SYS_ID
     * @return SELECT * FROM ETX_ADT_SYSTEM_CONFIG WHERE SYS_ID = sysId
     */
    List<GroovyRowResult> getSystemConfigBySysId(sysId) {
        return sql.rows("Select * From ETX_ADT_SYSTEM_CONFIG Where SYS_ID = '" + sysId + "' ")
    }
    /**
     * Get System config from table ETX_ADT_SYSTEM_CONFIG
     * @param sysId = SYS_ID
     * @return SELECT * FROM ETX_ADT_SYSTEM_CONFIG WHERE SYS_ID = sysId
     */
    boolean updateSysConfig(sysId, value, log) {
        def sqlStatement = "UPDATE ETX_ADT_SYSTEM_CONFIG SET SCG_PROPERTY_VALUE = '" + value + "', SCG_UPDATED_ON = CURRENT_TIMESTAMP Where SYS_ID = '" + sysId + "' AND SCG_PROPERTY_NAME ='etx.backend.services.FileRepositoryService.url'"
        log.info 'STATEMENT: ' + sqlStatement
        def execute = sql.executeUpdate(sqlStatement)
        if (execute) {
            log.info 'to be committed'
            sql.commit()
            log.info 'committed'
            return true
        }
        log.info 'nothing updated'
        return false
    }

    /**
     * Get User config from table ETX_ADT_USER
     * @param userName = USR_NAME
     * @return SELECT * FROM ETX_ADT_USER WHERE USR_NAME = userName
     */
    List<GroovyRowResult> getUserByName(userName) {
        return sql.rows("Select * From ETX_ADT_USER Where USR_NAME = '" + userName + "' ")
    }

    /**
     * Get System from table ETX_ADT_SYSTEM
     * @param userId = USR_ID
     * @return SELECT * FROM ETX_ADT_SYSTEM WHERE USR_ID = userId
     */
    List<GroovyRowResult> getSystemByUserId(userId) {
        return sql.rows("Select * From ETX_ADT_SYSTEM Where USR_ID = '" + userId + "' ")
    }


    List<GroovyRowResult> collectAttachments(attachmentId) {
        // collect attachment ID for the persisted message bundle
        return sql.rows("Select * " +
                "From ETX_ADT_ATTACHMENT " +
                "Where MSG_ID = '" + attachmentId + "' ")
    }

    List<GroovyRowResult> collectCheckSum(attachmentId) {
        // collect attachment ID for the persisted message bundle
        return sql.rows("Select RAWTOHEX (ATT_CHECKSUM)  " +
                "From ETX_ADT_ATTACHMENT " +
                "Where MSG_ID = '" + attachmentId + "' ")
    }

}
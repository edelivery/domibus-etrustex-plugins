package service

enum ConnectionData {
    A1_ETRUSTEX(
            '${Properties#A1_DB_ETRUSTEX_SERVICE_NAME}',
            '${Properties#A1_DB_ETRUSTEX_SID}',
            '${Properties#A1_DB_ETRUSTEX_HOST}',
            '${Properties#A1_DB_ETRUSTEX_PORT}',
            '${Properties#A1_DB_ETRUSTEX_USERNAME}',
            '${Properties#A1_DB_ETRUSTEX_PASSWORD}'),
    A1_DOMIBUS(
            '${Properties#A1_DB_DOMIBUS_SERVICE_NAME}',
            '${Properties#A1_DB_DOMIBUS_SID}',
            '${Properties#A1_DB_DOMIBUS_HOST}',
            '${Properties#A1_DB_DOMIBUS_PORT}',
            '${Properties#A1_DB_DOMIBUS_USERNAME}',
            '${Properties#A1_DB_DOMIBUS_PASSWORD}'),
    AN_DOMIBUS(
            '${Properties#AN_DB_DOMIBUS_SERVICE_NAME}',
            '${Properties#AN_DB_DOMIBUS_HOST}',
            '${Properties#AN_DB_DOMIBUS_PORT}',
            '${Properties#AN_DB_DOMIBUS_USERNAME}',
            '${Properties#AN_DB_DOMIBUS_PASSWORD}'),
    AN_ETRUSTEX(
            '${Properties#AN_DB_ETRUSTEX_SERVICE_NAME}',
            '${Properties#AN_DB_ETRUSTEX_HOST}',
            '${Properties#AN_DB_ETRUSTEX_PORT}',
            '${Properties#AN_DB_ETRUSTEX_USERNAME}',
            '${Properties#AN_DB_ETRUSTEX_PASSWORD}'),
    N_NODE('${Properties#ETRUSTEX_NODE_DB_SERVICE_NAME}',
            '${Properties#ETRUSTEX_NODE_DB_HOST}',
            '${Properties#ETRUSTEX_NODE_DB_PORT}',
            '${Properties#ETRUSTEX_NODE_DB_USERNAME}',
            '${Properties#ETRUSTEX_NODE_DB_PASSWORD}' ),
    A2_DOMIBUS(
            '${Properties#A2_DB_DOMIBUS_SERVICE_NAME}',
            '${Properties#A2_DB_DOMIBUS_HOST}',
            '${Properties#A2_DB_DOMIBUS_PORT}',
            '${Properties#A2_DB_DOMIBUS_USERNAME}',
            '${Properties#A2_DB_DOMIBUS_PASSWORD}'),
    A2_ETRUSTEX(
            '${Properties#A2_DB_ETRUSTEX_SERVICE_NAME}',
            '${Properties#A2_DB_ETRUSTEX_HOST}',
            '${Properties#A2_DB_ETRUSTEX_PORT}',
            '${Properties#A2_DB_ETRUSTEX_USERNAME}',
            '${Properties#A2_DB_ETRUSTEX_PASSWORD}'),


    ETRUSTEX_ADAPTER( //DB for old Etrustex stand-alone Adapter.
            '${Properties#ETRUSTEX_ADAPTER_DB_SERVICE_NAME}',
            '${Properties#ETRUSTEX_ADAPTER_DB_HOST}',
            '${Properties#ETRUSTEX_ADAPTER_DB_PORT}',
            '${Properties#ETRUSTEX_ADAPTER_DB_USERNAME}',
            '${Properties#ETRUSTEX_ADAPTER_DB_PASSWORD}'),

    private String hostname
    private String port
    private String serviceName
    private String user
    private String sid
    private String pwd
    def context

    ConnectionData(String serviceName, String hostname, String port, String user, String pwd) {
        this.pwd = pwd
        this.user = user
        this.serviceName = serviceName
        this.port = port
        this.hostname = hostname
    }
    ConnectionData(String serviceName, String sid, String hostname, String port, String user, String pwd) {
        this.pwd = pwd
        this.user = user
        this.serviceName = serviceName
        this.port = port
        this.sid = sid
        this.hostname = hostname
    }

    ConnectionData with(context){
        this.context= context
        return this
    }

    String getHostname() {
        return context.expand(hostname)
    }

    String getPort() {
        return context.expand(port)
    }

    String getServiceName() {
        return context.expand(serviceName)
    }
    String getSid() {
        return context.expand(sid)
    }

    String getUser() {
        return context.expand(user)
    }

    String getPwd() {
        return context.expand(pwd)
    }

    @Override
    public String toString() {
        return "Connection{" +
                "hostname='" + getHostname() + '\'' +
                ", port='" + getPort() + '\'' +
                ", serviceName='" + getServiceName() + '\'' +
                ", sid='" + getSid() + '\'' +
                ", user='" + getUser() + '\'' +
                ", pwd='" + getPwd() + '\'' +
                '}';
    }

}
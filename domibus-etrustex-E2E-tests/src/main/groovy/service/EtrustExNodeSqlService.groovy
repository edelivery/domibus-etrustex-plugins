package service

import groovy.sql.GroovyRowResult

class EtrustExNodeSqlService extends SqlService {
    EtrustExNodeSqlService(ConnectionData connection) {
        super(connection)
    }

    List<GroovyRowResult> collectMsgId(bundleId, sender, receiver) {
        // return sql.rows("Select *  From ETR_TB_MESSAGE Where MSG_DOCUMENT_ID = '" + bundleId + "' ")
        return sql.rows(" Select * from EPROCUREMENT.ETR_TB_MESSAGE message, EPROCUREMENT.ETR_TB_PARTY_ID senderParty, EPROCUREMENT.ETR_TB_PARTY_ID receiverParty where message.MSG_DOCUMENT_ID like '" + bundleId + "' AND senderParty.PID_PTY_ID = message.MSG_ISSUER_ID AND receiverParty.PID_PTY_ID = message.MSG_RECEIVER_ID AND senderParty.PID_IDENTIFIER_VALUE = '" + sender + "' AND receiverParty.PID_IDENTIFIER_VALUE = '" + receiver + "' ")
    }

}

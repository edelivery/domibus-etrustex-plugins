package service

import com.eviware.soapui.impl.wsdl.WsdlProject
import com.eviware.soapui.impl.wsdl.mock.WsdlMockService
import com.eviware.soapui.impl.wsdl.panels.support.MockTestRunner
import com.eviware.soapui.model.project.Project

class MockFileRepo {

    public static final String MOCK_REPO = "FileRepositorySoap11Binding MockService"
    WsdlMockService mock

    MockFileRepo(test) {
        WsdlProject project = test.getTestCase().getTestSuite().getProject()
        mock = project.getMockServiceByName(MOCK_REPO)
    }

    String getPort(){
        return mock.getPort()
    }
    String getPath(){
        return mock.getPath()
    }

    void start() {
        mock.start()
    }

    void stop() {
        mock.getMockRunner().stop()
    }
}
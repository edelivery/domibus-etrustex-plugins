package service

import groovy.sql.GroovyRowResult

class DomibusSqlService extends SqlService{

    DomibusSqlService(ConnectionData connection) {
        super(connection)
    }

    /**
     * Get Messages from table TB_MESSAGE_LOG
     * @param messageId = MESSAGE_ID
     * @return SELECT * FROM TB_MESSAGE_LOG WHERE MESSAGE_ID = messageId
     */
    List<GroovyRowResult> collectMsgLogByMsgId(messageId) {
        return sql.rows("Select *  From TB_MESSAGE_LOG Where MESSAGE_ID = '" + messageId + "' ")
    }
}

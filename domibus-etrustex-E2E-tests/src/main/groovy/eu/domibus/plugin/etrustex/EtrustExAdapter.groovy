import groovy.sql.Sql
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import java.text.SimpleDateFormat;
import com.eviware.soapui.model.mock.MockService
import com.eviware.soapui.model.mock.MockRunner
import static groovyx.net.http.Method.GET
	// METHODS	
	public class EtrustExAdapter {
        public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		def sql;
		def log;
		def context = null;
		def testRunner = null;
		String statusId = null;
		String senderPartyName = null;
		String receiverPartyName = null;
		String bundleId = null;
		String issueDateTime = null;
		String receivedDateTime = null;
		MockService mockService;
		MockService mockService2;
		MockService mockService3;
		MockRunner mockRunner;	
		MockRunner mockRunner2;
		MockRunner mockRunner3;		
		
		//=======================================================================================================================
		public EtrustExAdapter(log, context, testRunner) {
			this.log = log
			this.context = context
			this.testRunner = testRunner
		}
		//=======================================================================================================================
		def mockServiceStartUp(){	
			def project = testRunner.testCase.getTestSuite().getProject();
			def testsuiteName = testRunner.testCase.testSuite.name;
			if(testsuiteName == "OutboxTest_sendMessageStatus"){	
				mockService = project.getMockServiceByName( "ApplicationResponseBinding MockService" );	
				mockRunner=mockService.start();			
			} else if(testsuiteName == "OutboxTest_sendMessagebundle"){	
				// setup and launch 3 mock services
				 mockService = project.getMockServiceByName( "documentWrapperBinding MockService" )
				 mockService2 = project.getMockServiceByName( "documentBundleBinding MockService" )
				 mockService3 = project.getMockServiceByName( "EGREFFE_FileRepositorySoap11Binding MockService" )
				 mockRunner = mockService.start()
				 mockRunner2 = mockService2.start()
				 mockRunner3 = mockService3.start()
			}			 
		}
		//=======================================================================================================================
		 def mockServiceStop(){
		 def testSuiteName = testRunner.testCase.testSuite.name;
			 mockRunner.stop();
			 if(testSuiteName == "OutboxTest_sendMessagebundle"){	
				mockRunner2.stop();
				mockRunner3.stop();
			 }
		}
		//=======================================================================================================================
		def sendMessageStatus(testCaseName,testStepName){												
			def testStep = testRunner.testCase.testSuite.testCases[testCaseName].testSteps[testStepName]			
			def reqStep = testStep.getProperty("request");				
			def requestNode = new groovy.util.XmlSlurper().parseText(reqStep.value);			
						
			if(testStepName == "sendMessageStatus1"){	
				//waitforMockSerivice(testRunner,testStepName);	 				
				//setAndAssertApplicationResponseProperties(testRunner,requestNode);				
				assertMessageStatusList(testRunner,requestNode,testStepName);				
			} else if((testStepName == "sendMessageStatus_status_id_validation") || (testStepName == "sendMessageStatus_sender_party_validation")
				|| (testStepName == "sendMessageStatus_receiver_party_validation") || (testStepName="sendMessageStatus_status_code_validation") ||
				(testStepName == "sendMessageStatus_issue_date_time_validation") || (testStepName=="sendMessageStatus_msg_reference_id_validation")){
				assertMessageStatusList(testRunner,requestNode,testStepName);	
			}
		
		}
		//=======================================================================================================================
		def sendMessageBundle(testStepName){	
			def testCaseName = testRunner.testCase.name;		
			def testStep = testRunner.testCase.testSuite.testCases[testCaseName].testSteps[testStepName]			
			def reqStep = testStep.getProperty("request");				
			def requestNode = new groovy.util.XmlSlurper().parseText(reqStep.value);			
						
			if(testStepName == "sendMessageBundle_one_attachment"|| testStepName == "sendMessageBundle_header_not_well_formed"
			|| testStepName == "sendMessageBundle_empty_reference_id"){	
				waitforMockSerivice(testStepName);	 				
				setAndAssertBundleProperties(requestNode);				
				assertMessageBundleList(requestNode,testStepName);				
			} 
			else if((testStepName == "sendMessageBundle_empty_bundle_id") 
				|| (testStepName == "sendMessageBundle_missing_bundle_id_element")
				|| (testStepName == "sendMessageBundle_missing_reference_id_element")
				|| (testStepName == "sendMessageBundle_empty_issue_date_time") 
				|| (testStepName == "sendMessageBundle_missing_issue_date_time_element")
				|| (testStepName == "sendMessageBundle_empty_receiver_date_time")
				|| (testStepName == "sendMessageBundle_missing_receiver_date_time_element")
				|| (testStepName="sendMessageBundle_empty_senderParty") 
				|| (testStepName="sendMessageBundle_missing_senderParty_element")
				|| (testStepName == "sendMessageBundle_invalid_senderParty")
				||(testStepName == "sendMessageBundle_empty_receiverParty") 
				||(testStepName == "sendMessageBundle_missing_receiverParty_element") 
				|| (testStepName == "sendMessageBundle_invalid_receiverParty")
				|| (testStepName=="sendMessageBundle_empty_attachmentId")
				|| (testStepName=="sendMessageBundle_missing_attachmentId_element")
				|| (testStepName == "sendMessageBundle_empty_attachmentName")
				|| (testStepName == "sendMessageBundle_missing_attachmentName_element")
				|| (testStepName == "sendMessageBundle_empty_attachmentType")
				|| (testStepName == "sendMessageBundle_missing_attachmentType_element")
				|| (testStepName == "sendMessageBundle_invalid_attachmentType")
				|| (testStepName == "sendMessageBundle_empty_mimeType")
				|| (testStepName == "sendMessageBundle_missing_mimeType_element")
				|| (testStepName == "sendMessageBundle_empty_checksum")
				|| (testStepName == "DB_Configuration_missing_checksum")
				|| (testStepName == "sendMessageBundle_invalid_checksum")
				|| (testStepName == "sendMessageBundle_invalid_algorithm")
				|| (testStepName == "sendMessageBundle_empty_algorithm")
				|| (testStepName == "sendMessageBundle_missing_algorithm")
				|| (testStepName == "sendMessageBundle_empty_bundle_element")
				|| (testStepName == "sendMessageBundle_missing_bundle_element")
				|| (testStepName == "sendMessageBundle_missing_sender_element")
				|| (testStepName == "sendMessageBundle_missing_receiver_element")
				|| (testStepName == "sendMessageBundle_with_fileSize_not_number")
				|| (testStepName == "sendMessageBundle_expired_issue_date_time")
				|| (testStepName == "sendMessageBundle_future_issue_date_time_element")
				|| (testStepName == "sendMessageBundle_referenceId_nonAscii_chars")
				|| (testStepName == "sendMessageBundle_referenceId_greater_250chars")
				){
				assertMessageBundleList(requestNode,testStepName);	
			}		
		}
		//=======================================================================================================================
		def assertMessageStatusList(testRunner,requestNode,testStepName){
			Map localMap  = localvariablesForSendMessageStatus();
			Map reqParamMap = getRequestParametersForSendMessageStatus(testRunner,requestNode);// open DB connection
			openDbConnection();
			// collect message status data from the DB
			def msgStatusList = collectMsgId(reqParamMap['statusId'])
			if(testStepName == "sendMessageStatus1"){	
				if(msgStatusList != null) {
					// convert DB date
					Date dbIssueDateTime = msgStatusList[0].MSG_ISSUE_DT

					// convert input date
					Date dateIssueDateTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(reqParamMap['issueDateTime'])

					// check message status on the DB to verify that correct values are persisted
					assert msgStatusList[0].MSG_TYPE == "MESSAGE_STATUS"
					assert msgStatusList[0].MSG_STATE == "MS_PROCESSED"
					assert msgStatusList[0].MSG_REF_UUID == reqParamMap['bundleId']
					assert msgStatusList[0].MSG_REF_STATE == "READ"	
					assert msgStatusList[0].MSG_SUBJECT == null
					assert msgStatusList[0].MSG_CONTENT == null	
					assert msgStatusList[0].MSG_SENDER_PAR_ID == localMap['senderPartyPartyId']
					assert msgStatusList[0].MSG_RECEIVER_PAR_ID == localMap['receiverPartyPartyId']
					assert msgStatusList[0].MSG_NOTIFICATION_STATE == null
					assert msgStatusList[0].MSG_DIRECTION == "OUT"		
					assert dbIssueDateTime == dateIssueDateTime	
					assert msgStatusList[0].ERR_ID == null
					assert msgStatusList[0].MSG_SIGNATURE == null
					assert msgStatusList[0].MSG_SIGNED_DATA == null
					assert msgStatusList[0].MSG_STATUS_REASON_CODE == null
					}
				} else if(testStepName == "sendMessageStatus_failed"){	
				if(msgStatusList != null) {
					// convert DB date
					Date dbIssueDateTime = msgStatusList[0].MSG_ISSUE_DT

					// convert input date
					Date dateIssueDateTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(reqParamMap['issueDateTime'])

					// check message status on the DB to verify that correct values are persisted
					assert msgStatusList[0].MSG_TYPE == "MESSAGE_STATUS"
					assert msgStatusList[0].MSG_STATE == "MS_PROCESSED"
					assert msgStatusList[0].MSG_REF_UUID == reqParamMap['bundleId']
					assert msgStatusList[0].MSG_REF_STATE == "FAILED"	
					assert msgStatusList[0].MSG_SUBJECT == null
					assert msgStatusList[0].MSG_CONTENT == null	
					assert msgStatusList[0].MSG_SENDER_PAR_ID == localMap['senderPartyPartyId']
					assert msgStatusList[0].MSG_RECEIVER_PAR_ID == localMap['receiverPartyPartyId']
					assert msgStatusList[0].MSG_NOTIFICATION_STATE == null
					assert msgStatusList[0].MSG_DIRECTION == "OUT"		
					assert dbIssueDateTime == dateIssueDateTime	
					assert msgStatusList[0].ERR_ID == null
					assert msgStatusList[0].MSG_SIGNATURE == null
					assert msgStatusList[0].MSG_SIGNED_DATA == null
					assert msgStatusList[0].MSG_STATUS_REASON_CODE == null
					}
				} 
				else if((testStepName == "sendMessageStatus_status_id_validation") || (testStepName == "sendMessageStatus_sender_party_validation")
				|| (testStepName == "sendMessageStatus_receiver_party_validation") || (testStepName="sendMessageStatus_status_code_validation") ||
				(testStepName == "sendMessageStatus_issue_date_time_validation") || (testStepName=="sendMessageStatus_msg_reference_id_validation")){
				// check no message status was generated on the DB
				assert msgStatusList[0] == null
			}
			// close DB connection
				closeDbConnection()
		}
		//=======================================================================================================================
		def assertMessageBundleList(requestNode,testStepName){
			Map localMap  = localvariablesForSendMessageBundle();
			Map reqParamMap = getRequestParametersForSendMessageBundle(requestNode);// open DB connection
			def signatureFromDB;
			def signedDataFromDB;
			def msgId;
			openDbConnection();
		// Check DB values
		//////////////////

		// collect the data from the new message persisted on the DB
		def msgBundleList = collectMsgId(reqParamMap['bundleId'])
		log.info "reqParamMap['bundleId']" + reqParamMap['bundleId'];
		if(testStepName == "sendMessageBundle_one_attachment" || testStepName == "sendMessageBundle_header_not_well_formed"){
		if(msgBundleList[0] != null){
			// extract msg_id from DB for the bundle
			 msgId = msgBundleList[0].MSG_ID
		}
		// convert input date
		Date dateIssueDateTime = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss").parse(reqParamMap['issueDateTime'])

		// convert output date
		Date dbIssueDateTime = msgBundleList[0].MSG_ISSUE_DT
		if(msgBundleList[0].MSG_SIGNATURE != null){
			// convert signature to a character stream
			signatureFromDB = msgBundleList[0].MSG_SIGNATURE.getCharacterStream().getText()
		}
		if(msgBundleList[0].MSG_SIGNED_DATA != null){
			// convert signed data to a character stream
			signedDataFromDB = msgBundleList[0].MSG_SIGNED_DATA.getCharacterStream().getText()
		}

		// check db values
		assert msgBundleList[0].MSG_ID != null
		assert msgBundleList[0].MSG_TYPE == "MESSAGE_BUNDLE"
		assert msgBundleList[0].MSG_UUID == reqParamMap['bundleId']
		assert msgBundleList[0].MSG_STATE == "MS_PROCESSED"
		assert msgBundleList[0].MSG_PARENT_UUID == null	
		//assert msgBundleList[0].MSG_REF_UUID == reqParamMap['referenceId']
		assert msgBundleList[0].MSG_REF_STATE == null
		//assert msgBundleList[0].MSG_SUBJECT == reqParamMap['subject']
		//assert msgBundleList[0].MSG_CONTENT == reqParamMap['content']
		assert msgBundleList[0].MSG_SENDER_PAR_ID ==	localMap['senderPartyPartyId']
		assert msgBundleList[0].MSG_RECEIVER_PAR_ID == localMap['receiverPartyPartyId']
		//assert msgBundleList[0].MSG_NOTIFICATION_STATE ==	"NF_PENDING"
		//assert msgBundleList[0].MSG_DIRECTION == "IN"
		//assert dbIssueDateTime == dateIssueDateTime
		assert msgBundleList[0].ERR_ID == null
		//assert signatureFromDB == reqParamMap['signature']	
		//assert signedDataFromDB == reqParamMap['signedData']	
		assert msgBundleList[0].MSG_CREATED_ON != null							
		assert msgBundleList[0].MSG_UPDATED_ON != null
		assert msgBundleList[0].MSG_STATUS_REASON_CODE == null		
			
		// collect the data from the new attachment persisted on the DB
		def attachmentList = collectAttId(reqParamMap['attachmentId'])
		if(attachmentList[0] != null) {
		// convert checksum
		def inputChecksum = attachmentList[0].ATT_CHECKSUM
		hexAttachmentChecksum = byteToHexChecksum(inputChecksum)

		// check db values
		assert attachmentList[0].ATT_ID != null
		assert attachmentList[0].MSG_ID == msgBundleList[0].MSG_ID
		assert attachmentList[0].ATT_UUID == reqParamMap['attachmentId']
		assert attachmentList[0].ATT_TYPE == "BINARY"
		assert attachmentList[0].ATT_STATE == "ATTS_UPLOADED"	
		assert attachmentList[0].ATT_DIRECTION == "OUTGOING"
		assert attachmentList[0].ATT_FILE_NAME == reqParamMap['attachmentFileName']
		assert attachmentList[0].ATT_FILE_PATH == "RELATIVE PATH"
		assert attachmentList[0].ATT_FILE_SIZE == reqParamMap['attachmentFileSize']
		assert attachmentList[0].ATT_MIME_TYPE == reqParamMap['attachmentMimeType']
		assert hexAttachmentChecksum == reqParamMap['attachmentChecksum']
		assert attachmentList[0].ATT_CHECKSUM_ALGORITHM == "SHA_512"
		assert attachmentList[0].ATT_LANGUAGE_CODE == null
		assert attachmentList[0].ATT_SESSION_KEY == null
		assert attachmentList[0].ATT_X509_SUBJECT_NAME == null
		assert attachmentList[0].ATT_CREATED_ON != null	
		assert attachmentList[0].ATT_UPDATED_ON != null	
		assert attachmentList[0].ERR_ID == null							
		assert attachmentList[0].ATT_ACTIVE_STATE == 1
		}
		}else if((testStepName == "sendMessageBundle_empty_bundle_id") 
				|| (testStepName == "sendMessageBundle_missing_bundle_id_element")
				|| (testStepName == "sendMessageBundle_missing_reference_id_element")
				|| (testStepName == "sendMessageBundle_empty_issue_date_time") 
				|| (testStepName == "sendMessageBundle_missing_issue_date_time_element")
				|| (testStepName == "sendMessageBundle_empty_receiver_date_time")
				|| (testStepName == "sendMessageBundle_missing_receiver_date_time_element")
				|| (testStepName="sendMessageBundle_empty_senderParty") 
				|| (testStepName="sendMessageBundle_missing_senderParty_element")
				|| (testStepName == "sendMessageBundle_invalid_senderParty")
				||(testStepName == "sendMessageBundle_empty_receiverParty") 
				||(testStepName == "sendMessageBundle_missing_receiverParty_element") 
				|| (testStepName == "sendMessageBundle_invalid_receiverParty")
				|| (testStepName=="sendMessageBundle_empty_attachmentId")
				|| (testStepName=="sendMessageBundle_missing_attachmentId_element")
				|| (testStepName == "sendMessageBundle_empty_attachmentName")
				|| (testStepName == "sendMessageBundle_missing_attachmentName_element")
				|| (testStepName == "sendMessageBundle_empty_attachmentType")
				|| (testStepName == "sendMessageBundle_missing_attachmentType_element")
				|| (testStepName == "sendMessageBundle_invalid_attachmentType")
				|| (testStepName == "sendMessageBundle_empty_mimeType")
				|| (testStepName == "sendMessageBundle_missing_mimeType_element")
				|| (testStepName == "sendMessageBundle_empty_checksum")
				|| (testStepName == "DB_Configuration_missing_checksum")
				|| (testStepName == "sendMessageBundle_invalid_checksum")
				|| (testStepName == "sendMessageBundle_invalid_algorithm")
				|| (testStepName == "sendMessageBundle_empty_algorithm")
				|| (testStepName == "sendMessageBundle_missing_algorithm")
				|| (testStepName == "sendMessageBundle_empty_bundle_element")
				|| (testStepName == "sendMessageBundle_missing_bundle_element")
				|| (testStepName == "sendMessageBundle_missing_sender_element")
				|| (testStepName == "sendMessageBundle_missing_receiver_element")
				|| (testStepName == "sendMessageBundle_with_fileSize_not_number")
				|| (testStepName == "sendMessageBundle_expired_issue_date_time")
				|| (testStepName == "sendMessageBundle_future_issue_date_time_element")
				|| (testStepName == "sendMessageBundle_referenceId_nonAscii_chars")
				|| (testStepName == "sendMessageBundle_referenceId_greater_250chars")
				){
				// check no message status was generated on the DB
				assert msgBundleList[0] == null
			}
			// close DB connection
				closeDbConnection()
		}
		//=======================================================================================================================
		// collect the input data from the request to the mock service for sendMessagebundle
		def setAndAssertBundleProperties(requestNode){
			Map reqParamMap = getRequestParametersForSendMessageBundle(requestNode);			
			////////////////////////////////
		// Check file repository request
		////////////////////////////////

		// collect the input data from the request to the mock service
		String wsEgreffeRequestBundleId = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_EGREFFE_FILE_REPOSITORY_BUNDLE_ID")
		String wsEgreffeRequestSenderPartyId = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_EGREFFE_FILE_REPOSITORY_SENDER_PARTY_ID")
		String wsEgreffeRequestSenderPartyName = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_EGREFFE_FILE_REPOSITORY_SENDER_PARTY_NAME")
		String wsEgreffeRequestReceiverPartyId = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_EGREFFE_FILE_REPOSITORY_RECEIVER_PARTY_ID")
		String wsEgreffeRequestReceiverPartyName = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_EGREFFE_FILE_REPOSITORY_RECEIVER_PARTY_NAME")
		String wsEgreffeRequestType = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_EGREFFE_FILE_REPOSITORY_TYPE")
		String wsEgreffeRequestAttachmentId = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_EGREFFE_FILE_REPOSITORY_ATTACHMENT_ID")
log.error "5555"
		// check data requested from egreffe file repository is correct
		assert wsEgreffeRequestBundleId == reqParamMap['bundleId']
		assert wsEgreffeRequestSenderPartyId == reqParamMap['senderPartyName']
		assert wsEgreffeRequestSenderPartyName == reqParamMap['senderPartyName']
		assert wsEgreffeRequestReceiverPartyId == reqParamMap['receiverPartyName']
		assert wsEgreffeRequestReceiverPartyName == reqParamMap['receiverPartyName']
		assert wsEgreffeRequestType == "MESSAGE_BUNDLE"
		assert wsEgreffeRequestAttachmentId == reqParamMap['attachmentId']

		/////////////////////////////////
		// Check document wrapper request
		/////////////////////////////////
log.error "66666"
		// collect the input data from the request to the mock service
		String wsDocWrapperRequestBusinessHeader = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_DOC_WRAPPER_BUSINESS_HEADER_SENDER")
		String wsDocWrapperRequestAttachmentId = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_DOC_WRAPPER_ATTACHMENT_ID")
		String wsDocWrapperRequestIssueDate = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_DOC_WRAPPER_ISSUE_DATE")
		String wsDocWrapperRequestDocumentTypeCode = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_DOC_WRAPPER_DOCUMENT_TYPE_CODE")
		String wsDocWrapperRequestSenderParty = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_DOC_WRAPPER_SENDER_PARTY")
		String wsDocWrapperRequestFileSize = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_DOC_WRAPPER_FILESIZE")
		String wsDocWrapperRequestMimeType = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_DOC_WRAPPER_MIME_TYPE")

		// check data sent to node document wrapper is correct
		assert wsDocWrapperRequestBusinessHeader == reqParamMap['senderPartyName']
		assert wsDocWrapperRequestAttachmentId == reqParamMap['attachmentId']
		assert wsDocWrapperRequestIssueDate == "2015-07-01"
		assert wsDocWrapperRequestDocumentTypeCode == "BINARY"
		assert wsDocWrapperRequestSenderParty == reqParamMap['senderPartyName']
		assert wsDocWrapperRequestFileSize == reqParamMap['attachmentFileSize']
		assert wsDocWrapperRequestMimeType == reqParamMap['attachmentMimeType']

		//////////////////////////////////
		// Check document document request
		//////////////////////////////////
log.error "77777"
		// collect the input data from the request to the mock service
		String wsSubmitBundleRequestBusinessHeaderSender = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_BUSINESS_HEADER_SENDER")
		String wsSubmitBundleRequestBusinessHeaderReceiver = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_BUSINESS_HEADER_RECEIVER")
		String wsSubmitBundleRequestBusinessHeaderScope = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_BUSINESS_HEADER_SCOPE")
		String wsSubmitBundleRequestMessageSubject = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_MESSAGE_SUBJECT")
		String wsSubmitBundleRequestBundleId = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_BUNDLE_ID")
		String wsSubmitBundleRequestReferenceId = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_REFERENCE_ID")
		String wsSubmitBundleRequestIssueDate = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_ISSUE_DATE")
		String wsSubmitBundleRequestIssueTime = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_ISSUE_TIME")
		String wsSubmitBundleRequestMessageContent = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_MESSAGE_CONTENT")
		String wsSubmitBundleRequestSenderParty = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_SENDER_PARTY")
		String wsSubmitBundleRequestReceiverParty = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_RECEIVER_PARTY")
		String wsSubmitBundleRequestAttachmentId = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_ATTACHMENT_ID")
		String wsSubmitBundleRequestAttDocTypeCode = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_ATTACHMENT_DOCUMENT_TYPE_CODE")
		String wsSubmitBundleRequestAttFileName = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_ATTACHMENT_FILENAME")
		String wsSubmitBundleRequestFileSize = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_ATTACHMENT_FILESIZE")
		String wsSubmitBundleRequestHashType = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_ATTACHMENT_HASHTYPE")
		String wsSubmitBundleRequestChecksum = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_ATTACHMENT_CHECKSUM")
	
		// check data sent to node submit bundle is correct
		assert wsSubmitBundleRequestBusinessHeaderSender == reqParamMap['senderPartyName']
		assert wsSubmitBundleRequestBusinessHeaderReceiver == reqParamMap['receiverPartyName']
		assert wsSubmitBundleRequestBusinessHeaderScope == "CREATE_STATUS_AVAILABLE"
		assert wsSubmitBundleRequestMessageSubject == reqParamMap['subject']
		assert wsSubmitBundleRequestBundleId == reqParamMap['bundleId']
		assert wsSubmitBundleRequestReferenceId == reqParamMap['referenceId']
		assert wsSubmitBundleRequestIssueDate == "2015-07-01"
		assert wsSubmitBundleRequestIssueTime == "14:44:39.000"
		assert wsSubmitBundleRequestMessageContent == reqParamMap['content']
		assert wsSubmitBundleRequestSenderParty == reqParamMap['senderPartyName']
		assert wsSubmitBundleRequestReceiverParty == reqParamMap['receiverPartyName']
		assert wsSubmitBundleRequestAttachmentId == reqParamMap['attachmentId']
		assert wsSubmitBundleRequestAttDocTypeCode == "BINARY"
		assert wsSubmitBundleRequestAttFileName == reqParamMap['attachmentFileName']
		assert wsSubmitBundleRequestFileSize == reqParamMap['attachmentFileSize']
		assert wsSubmitBundleRequestHashType == "SHA-512"	
		assert wsSubmitBundleRequestChecksum == reqParamMap['attachmentChecksum']
			
		}
		//=======================================================================================================================
		def waitforMockSerivice(testStepName){
			// define waiter that is used to pause the script until the application response has been successfully delivered to //the mock service
			def pauser = true

			def oneMinutesInMillies = 1 * 120 * 1000
			def startWaitingTime = System.currentTimeMillis()
			if(testStepName == "sendMessageStatus1"){
				// wait until the request is made to the file repository service
				while(pauser && ((System.currentTimeMillis() - startWaitingTime) < oneMinutesInMillies)) {
					pauser = (testRunner.testCase.testSuite.project.getPropertyValue("APP_RESPONSE_WAIT").equals("0"))
					sleep 1000
				}	
				// reset waiter 
				testRunner.testCase.testSuite.project.setPropertyValue("APP_RESPONSE_WAIT", "0")
			}else if(testStepName == "sendMessageBundle_one_attachment" || testStepName == "sendMessageBundle_header_not_well_formed" || testStepName == "sendMessageBundle_empty_reference_id"){
				
				// wait until the request is made to the file repository service
				while(pauser && ((System.currentTimeMillis() - startWaitingTime) < oneMinutesInMillies)) {
					pauser = (testRunner.testCase.testSuite.project.getPropertyValue("EGREFFE_WAIT").equals("0"))
					sleep 1000
				}
					log.error "22222"
				// reset waiter 
				testRunner.testCase.testSuite.project.setPropertyValue("EGREFFE_WAIT", "0") 

				pauser = true

				// wait until the request is made to the file repository service
				while(pauser && ((System.currentTimeMillis() - startWaitingTime) < oneMinutesInMillies)) {
					pauser = (testRunner.testCase.testSuite.project.getPropertyValue("WRAPPER_WAIT").equals("0"))
					sleep 1000
				}
					log.error "3333"
				// reset waiter 
				testRunner.testCase.testSuite.project.setPropertyValue("WRAPPER_WAIT", "0") 

				// define waiter that is used to pause the script until the payload has been successfully delivered to the mock service
				def waiter = true

				startWaitingTime = System.currentTimeMillis()

				// wait until the payload arrives at the mock service
				while(waiter && ((System.currentTimeMillis() - startWaitingTime) < oneMinutesInMillies)) {
					waiter = (testRunner.testCase.testSuite.project.getPropertyValue("BUNDLE_WAIT").equals("0"))
					sleep 1000
				}
					log.error "444444"
				// reset waiter 
				testRunner.testCase.testSuite.project.setPropertyValue("BUNDLE_WAIT", "0") 
			}
		}
		//=======================================================================================================================
		def Map localvariablesForSendMessageStatus(){
			
			def localVarMap = [senderPartyLocalUserId: 10001, senderPartyNodeUserId:10002, senderPartySystemId: 1000001, senderPartyPartyId:1000001, receiverPartyLocalUserId:10003 ,
				receiverPartyBackendUserId:10004 , receiverPartySystemId:1000002 ,receiverPartyPartyId:1000002 ,msgId:1000001 ,attachmentUUID:'ATTACHMENT_0001',
				attachmentChecksum:'D5CE4474F5DE9AE959EC444BE6013E52DD8F314718EA9A06149E22139C4E01A170AC9887925C209E2C85F64FB5218FC55C716FF5481D3D53499B6BBBC0D7C516',
				metadataUUID:'PAYLOAD_0001',
				metadataChecksum:'BF121E052559469A23FCD8E6E047CD65A608F97479CA0E973FB8BFF644885AF68170C48B6E5ACB76775D69730C411AE28CFF60017D51582020FD5A8A9B8DB142'];
				return localVarMap;
		}
		//=======================================================================================================================
		def Map localvariablesForSendMessageBundle(){
			
			def localBundleMap = [senderPartyLocalUserId:10001,senderPartyBackendUserId:10002,senderPartyNodeUserId :10003,senderPartySystemId :1000001,senderPartyPartyId :1000002,receiverPartyLocalUserId : 10004,receiverPartyBackendUserId : 10005,receiverPartySystemId : 1000002,receiverPartyPartyId : 1000001,msgId : 1000001,msgStatusId : 1000002];
				return localBundleMap;
		}
		//=======================================================================================================================
		def Map getRequestParametersForSendMessageStatus(requestNode){		 
			def reqParamMap = [
			statusId : requestNode.Body.sendMessageStatusRequest.messageStatus.id.text(),
			senderPartyName : requestNode.Body.sendMessageStatusRequest.messageStatus.sender.id.text(),
			receiverPartyName : requestNode.Body.sendMessageStatusRequest.messageStatus.receiver.id.text(),
			bundleId : requestNode.Body.sendMessageStatusRequest.messageStatus.messageReference.id.text(),
			issueDateTime : requestNode.Body.sendMessageStatusRequest.messageStatus.issueDateTime.text(),
			receivedDateTime : requestNode.Body.sendMessageStatusRequest.messageStatus.receivedDateTime.text()];			
			
			return reqParamMap;
		}
		//=======================================================================================================================
		def Map getRequestParametersForSendMessageBundle(requestNode){		 
			def reqParamMap = [
			referenceId : requestNode.Body.sendMessageBundleRequest.messageBundle.referenceId.text(),
			senderPartyName : requestNode.Body.sendMessageBundleRequest.messageBundle.sender.id.text(),
			receiverPartyName : requestNode.Body.sendMessageBundleRequest.messageBundle.receiverList.recipient.id.text(),
			bundleId : requestNode.Body.sendMessageBundleRequest.messageBundle.id.text(),
			issueDateTime : requestNode.Body.sendMessageBundleRequest.messageBundle.issueDateTime.text(),
			receivedDateTime : requestNode.Body.sendMessageBundleRequest.messageBundle.receivedDateTime.text(),			
			subject : requestNode.Body.sendMessageBundleRequest.messageBundle.subject.text(),
			content : requestNode.Body.sendMessageBundleRequest.messageBundle.content.text(),
			attachmentId : requestNode.Body.sendMessageBundleRequest.messageBundle.fileMetadataList.file.id.text(),
			attachmentFileSize : requestNode.Body.sendMessageBundleRequest.messageBundle.fileMetadataList.file.size.text(),
			attachmentMimeType : requestNode.Body.sendMessageBundleRequest.messageBundle.fileMetadataList.file.mimeType.text(),
			attachmentFileName : requestNode.Body.sendMessageBundleRequest.messageBundle.fileMetadataList.file.name.text(),
			attachmentChecksum : requestNode.Body.sendMessageBundleRequest.messageBundle.fileMetadataList.file.checksum.text(),
			signedData : requestNode.Body.sendMessageBundleRequest.messageBundle.signedData.text(),
			signature : requestNode.Body.sendMessageBundleRequest.messageBundle.signature.text()];
			return reqParamMap;
		}
		//=======================================================================================================================
		def dbUpdatesforSendMessageStatusAndBundle(testStepName){
			def testCaseName = testRunner.testCase.name;
			def testsuiteName = testRunner.testCase.testSuite.name;
			def testStep = testRunner.testCase.testSuite.testCases[testCaseName].testSteps[testStepName]			
			def reqStep = testStep.getProperty("request");				
			def requestNode = new groovy.util.XmlSlurper().parseText(reqStep.value);
			Map localStatusMap  = localvariablesForSendMessageStatus();
			Map localBundleMap  = localvariablesForSendMessageBundle();
			Map reqParamMap;			
			if(testsuiteName == "OutboxTest_sendMessageStatus"){
				 reqParamMap = getRequestParametersForSendMessageStatus(requestNode);
			} else if(testsuiteName == "OutboxTest_sendMessagebundle") {
				 reqParamMap = getRequestParametersForSendMessageBundle(requestNode);
			}
			// open DB connection
			openDbConnection();		
			// create sender party users on user table
			createUser(localStatusMap['senderPartyLocalUserId'], "ETX-ADAPTER-AUTOMATION-USER", "3>#8g~<;CZpB2\$T")
			log.info "Sender party user ETX-ADAPTER-AUTOMATION-USER has been created"
			if(testsuiteName == "OutboxTest_sendMessagebundle"){
				createUser(localBundleMap['senderPartyBackendUserId'], "WSUSERNAME", "WSPASSWORD")	
			}
			createUser(localStatusMap['senderPartyNodeUserId'], "ROBOT", "ROBOT")
			log.info "Sender party user ROBOT has been created"
			

			// create sender party system on system table
			createSystem(localStatusMap['senderPartySystemId'], "ROBOT-SYS", localStatusMap['senderPartyLocalUserId'], null)
			log.info "sender party system ROBOT-SYS has been created"
			
			// create sender party system config on system config table
			if(testsuiteName == "OutboxTest_sendMessagebundle"){			
			createSystemConfig(100001, localBundleMap['senderPartySystemId'], "etx.backend.download.attachments", "true")
			createSystemConfig(100002, localBundleMap['senderPartySystemId'], "etx.backend.filerepositorylocation", "BACKEND_FILE_REPOSITORY_SERVICE")
			createSystemConfig(100003, localBundleMap['senderPartySystemId'], "etx.backend.services.FileRepositoryService.url", "http://D02DI1505644DIT.net1.cec.eu.int:8877/mockFileRepositorySoap11Binding")
			}
			if(testStepName == "sendMessageBundle_empty_senderParty" || testStepName == "sendMessageBundle_missing_senderParty_element" || testStepName == "sendMessageBundle_invalid_senderParty" ||
			testStepName == "sendMessageBundle_missing_sender_element"){
				// create sender party party on party table
				createParty(localStatusMap['senderPartyPartyId'], "Dummy_Sender_Party", localStatusMap['senderPartySystemId'], localStatusMap['senderPartyNodeUserId'])
				//log.info "Sender Party" + senderPartyName +  has been created"
			}else{
				// create sender party party on party table
				createParty(localStatusMap['senderPartyPartyId'], reqParamMap['senderPartyName'], localStatusMap['senderPartySystemId'], localStatusMap['senderPartyNodeUserId'])
				//log.info "Sender Party" + senderPartyName +  has been created"
			}
			// create receiver party users on user table
			createUser(localStatusMap['receiverPartyLocalUserId'], "ROBOT-WEB-SYS-USER", "generic_password")
			log.info "Receiver Party ROBOT-WEB-SYS-USER has been created"
			createUser(localStatusMap['receiverPartyBackendUserId'], "ETX-ADAPTER-SYS-USER", "generic_password")	
			log.info "Receiver Party ETX-ADAPTER-SYS-USER has been created"
			
			// create receiver party system on system table
			createSystem(localStatusMap['receiverPartySystemId'], "ROBOT-WEB-SYS", localStatusMap['receiverPartyLocalUserId'], null)
			log.info "Receiver party system ROBOT-WEB-SYS has been created"
			if(testStepName == "sendMessageBundle_empty_receiverParty" || testStepName == "sendMessageBundle_missing_receiverParty_element" || testStepName == "sendMessageBundle_invalid_receiverParty"
			|| testStepName == "sendMessageBundle_missing_receiver_element"){
				// create receiver party party on party table8345
				createParty(localStatusMap['receiverPartyPartyId'], "DUMMY_RECEIVER_PARTY", localStatusMap['receiverPartySystemId'], localStatusMap['receiverPartyBackendUserId'])	
				//log.info "receiver Party" + receiverPartyName +  has been created"
			} else{
				// create receiver party party on party table8345
				createParty(localStatusMap['receiverPartyPartyId'], reqParamMap['receiverPartyName'], localStatusMap['receiverPartySystemId'], localStatusMap['receiverPartyBackendUserId'])	
				//log.info "receiver Party" + receiverPartyName +  has been created"
			}
			// define a time to use in the script
			Calendar cal = Calendar.getInstance()
			cal.set(2015, 2, 23, 22, 15, 35)
			def dateAsString = sdf.format(cal.getTime())
			if(testsuiteName == "OutboxTest_sendMessageStatus"){
				// create message on message table
				createMessageBundle(localStatusMap['msgId'], reqParamMap['bundleId'], "MSG_REF_0001", "MSG_SUBJECT_0001", "MSG_CONTENT_0001", localStatusMap['receiverPartyPartyId'], localStatusMap['senderPartyPartyId'], "IN", dateAsString)
				log.info "Message Bundle  has been created"

				// create attachments on attachment table
				createAttachment(1000001, localStatusMap['msgId'], localStatusMap['attachmentUUID'], "BINARY", "ATTACHMENT_FILENAME0001", "ATTACHMENT_FILEPATH", 23425, localStatusMap['attachmentChecksum'])
				log.info "Attachment has been added to the attachment table"
				createAttachment(1000002, localStatusMap['msgId'], localStatusMap['metadataUUID'], "METADATA", "METADATA_FILENAME0001", "METADATA_FILEPATH", 3912, localStatusMap['metadataChecksum'])
				log.info "Attachment has been added to the attachment table"
			}
			 //inject mock service URL into the application response DB property
			def newEndpoint = 'http://' + InetAddress.getLocalHost().getCanonicalHostName() + ':' + mockService.getPort() + mockService.getPath()

			// update application response URL to point to the mock service
			updateApplicationResponseURL(newEndpoint)
			
		}
		//=======================================================================================================================
		// collect the input data from the request to the mock service for sendMessageStatus
		def setAndAssertApplicationResponseProperties(requestNode){
			Map reqParamMap = getRequestParametersForSendMessageStatus(requestNode);			
			String wsApplicationResponseBusinessHeaderSender = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_APPLICATION_RESPONSE_BUSINESS_HEADER_SENDER")
			String wsApplicationResponseBusinessHeaderReceiver = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_APPLICATION_RESPONSE_BUSINESS_HEADER_RECEIVER")
			String wsApplicationResponseBusinessHeaderScope = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_APPLICATION_RESPONSE_BUSINESS_HEADER_SCOPE")
			String wsApplicationResponseMessageStatusId = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_APPLICATION_RESPONSE_MESSAGE_STATUS_ID")
			String wsApplicationResponseIssueDate = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_APPLICATION_RESPONSE_ISSUE_DATE")
			String wsApplicationResponseResponseBundleId = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_APPLICATION_RESPONSE_DOCUMENT_RESPONSE_BUNDLE_ID")
			String wsApplicationResponseResponseCode = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_APPLICATION_RESPONSE_DOCUMENT_RESPONSE_CODE")
			String wsApplicationResponseReferenceBundleId = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_APPLICATION_RESPONSE_DOCUMENT_REFERENCE_BUNDLE_ID")
			String wsApplicationResponseReferenceTypeCode = testRunner.testCase.testSuite.project.getPropertyValue("MOCK_NODE_APPLICATION_RESPONSE_DOCUMENT_REFERENCE_TYPE_CODE")
			log.info "wsApplicationResponseBusinessHeaderSender "  + wsApplicationResponseBusinessHeaderSender;
			log.info "reqParamMap['senderPartyName'] "  + reqParamMap['senderPartyName'];
			// verify that the data sent to the node matches the expected results
			assert wsApplicationResponseBusinessHeaderSender == reqParamMap['senderPartyName']

			assert wsApplicationResponseBusinessHeaderReceiver == 	reqParamMap['receiverPartyName'] 
			assert wsApplicationResponseBusinessHeaderScope == "CREATE_STATUS_AVAILABLE"
			assert wsApplicationResponseMessageStatusId == reqParamMap['statusId']
			assert wsApplicationResponseIssueDate == "2017-02-02"
			assert wsApplicationResponseResponseBundleId == reqParamMap['bundleId']
			assert wsApplicationResponseResponseCode == "BDL:7"
			assert wsApplicationResponseReferenceBundleId == reqParamMap['bundleId']
			assert wsApplicationResponseReferenceTypeCode == "BDL"
			
		}
		
		
		//=======================================================================================================================
		// Clear database tables
		def cleanDatabase(testStepName){			
		// open DB connection
			openDbConnection();
		def testsuiteName = testRunner.testCase.testSuite.name;
		def testCaseName = testRunner.testCase.name;
		if(testsuiteName == "OutboxTest_sendMessageStatus"){
			clearDataFromGlobalprojectProperties();
		} else if(testsuiteName == "OutboxTest_sendMessagebundle"){
			clearDataFromGlobalprojectPropertiesForSendMessageBundle();
		}
		
		// revert application response URL back to the node value
		revertApplicationResponseURL()	
		dbChangesForSendMessageStatusAndBundleClearDB(testStepName);			
		// close DB connection
		closeDbConnection()
		// reset cache
		resetCacheAdapter()
		}
		//=======================================================================================================================
		// clear data from the global properties
		def clearDataFromGlobalprojectProperties(){		
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_APPLICATION_RESPONSE_BUSINESS_HEADER_SENDER", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_APPLICATION_RESPONSE_BUSINESS_HEADER_RECEIVER", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_APPLICATION_RESPONSE_BUSINESS_HEADER_SCOPE", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_APPLICATION_RESPONSE_MESSAGE_STATUS_ID", "")		
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_APPLICATION_RESPONSE_ISSUE_DATE", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_APPLICATION_RESPONSE_DOCUMENT_RESPONSE_BUNDLE_ID", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_APPLICATION_RESPONSE_DOCUMENT_RESPONSE_CODE", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_APPLICATION_RESPONSE_DOCUMENT_REFERENCE_BUNDLE_ID", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_APPLICATION_RESPONSE_DOCUMENT_REFERENCE_TYPE_CODE", "")
		}
		//=======================================================================================================================
		// clear data from the global properties
		def clearDataFromGlobalprojectPropertiesForSendMessageBundle(){		
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_EGREFFE_FILE_REPOSITORY_BUNDLE_ID", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_EGREFFE_FILE_REPOSITORY_SENDER_PARTY_ID", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_EGREFFE_FILE_REPOSITORY_SENDER_PARTY_NAME", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_EGREFFE_FILE_REPOSITORY_RECEIVER_PARTY_ID", "")		
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_EGREFFE_FILE_REPOSITORY_RECEIVER_PARTY_NAME", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_EGREFFE_FILE_REPOSITORY_TYPE", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_EGREFFE_FILE_REPOSITORY_ATTACHMENT_ID", "")
		
		
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_DOC_WRAPPER_BUSINESS_HEADER_SENDER", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_DOC_WRAPPER_ATTACHMENT_ID", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_DOC_WRAPPER_ISSUE_DATE", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_DOC_WRAPPER_DOCUMENT_TYPE_CODE", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_DOC_WRAPPER_SENDER_PARTY", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_DOC_WRAPPER_FILESIZE", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_DOC_WRAPPER_MIME_TYPE", "")
		
		
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_BUSINESS_HEADER_SENDER", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_BUSINESS_HEADER_RECEIVER", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_BUSINESS_HEADER_SCOPE", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_MESSAGE_SUBJECT", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_BUNDLE_ID", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_REFERENCE_ID", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_ISSUE_DATE", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_ISSUE_TIME", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_MESSAGE_CONTENT", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_SENDER_PARTY", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_RECEIVER_PARTY", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_ATTACHMENT_ID", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_ATTACHMENT_DOCUMENT_TYPE_CODE", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_ATTACHMENT_FILENAME", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_ATTACHMENT_FILESIZE", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_ATTACHMENT_HASHTYPE", "")
		testRunner.testCase.testSuite.project.setPropertyValue("MOCK_NODE_SUBMIT_BUNDLE_ATTACHMENT_CHECKSUM", "")
		}
		//=======================================================================================================================
		def dbChangesForSendMessageStatusAndBundleClearDB(testStepName){
		def testCaseName = testRunner.testCase.name;
		def testsuiteName = testRunner.testCase.testSuite.name;
		def reqStep = testRunner.testCase.testSuite.testCases[testCaseName].testSteps[testStepName].testStep.getProperty("request");		
		def requestNode = new groovy.util.XmlSlurper().parseText(reqStep.value);
		Map localVarMap;
		Map reqParamMap;
		
			if(testsuiteName == "OutboxTest_sendMessageStatus"){
				localVarMap = localvariablesForSendMessageStatus();
				reqParamMap = getRequestParametersForSendMessageStatus(testRunner,requestNode);
			} else if(testsuiteName == "OutboxTest_sendMessagebundle"){
				localVarMap = localvariablesForSendMessageBundle();
				reqParamMap = getRequestParametersForSendMessageBundle(requestNode);
			}
			// open DB connection
			openDbConnection();
			// delete attachment from attachments table
			deleteAttachment(localVarMap['msgId'])
			// collect the data from the new message status persisted on the DB
			def msgStatusList = collectMsgId(reqParamMap['statusId'])
			log.info "msgStatusList : " + msgStatusList.size();
			// extract msg_id from DB for the bundle
			if(msgStatusList[0] != null) {
				String msgStatusId = msgStatusList[0].MSG_ID			

				if(msgStatusId != null) {
					
					// delete status from message table
					deleteStatus(msgStatusId)
						
						
				}
			}
			// delete message from message table
			deleteMessage(localVarMap['msgId'],reqParamMap['bundleId'])
			log.info "reqParamMap['senderPartyName'] " + reqParamMap['senderPartyName'];
			if(testStepName == "sendMessageBundle_empty_senderParty" || testStepName == "sendMessageBundle_missing_senderParty_element" || testStepName == "sendMessageBundle_invalid_senderParty"
			|| testStepName == "sendMessageBundle_missing_sender_element"){
				// delete parties from party table
				deleteParty('Dummy_Sender_Party')
			}else{
				// delete parties from party table
				deleteParty(reqParamMap['senderPartyName'])
			}
			if(testStepName == "sendMessageBundle_empty_receiverParty" || testStepName == "sendMessageBundle_missing_receiverParty_element" || testStepName == "sendMessageBundle_invalid_receiverParty"
			|| testStepName == "sendMessageBundle_missing_receiver_element"){
				deleteParty('DUMMY_RECEIVER_PARTY')
			}else{
				deleteParty(reqParamMap['receiverPartyName'])
			}
			deleteSystemConfig(localVarMap['senderPartySystemId']);
			// delete systems from system table
			deleteSystem(localVarMap['senderPartySystemId'])
			deleteSystem(localVarMap['receiverPartySystemId'])

			// delete users from user table
			deleteUser(localVarMap['senderPartyLocalUserId'])
			deleteUser(localVarMap['senderPartyNodeUserId'])
			deleteUser(localVarMap['senderPartyBackendUserId'])
			deleteUser(localVarMap['receiverPartyLocalUserId'])
			deleteUser(localVarMap['receiverPartyBackendUserId'])
			// close DB connection
			closeDbConnection()
		}
		//=======================================================================================================================
		
		def openDbConnection(){	
		//log.info "${#Project#ADAPTER_DATABASE_CONNECTION}";
			sql = Sql.newInstance(context.expand('${#Project#ADAPTER_DATABASE_CONNECTION}'))
		}
	
		//=======================================================================================================================
		def closeDbConnection(){
			sql.close()
		}

		//=======================================================================================================================
		def createUser(userId, userName, userPassword) { 
			//Delete the user if already exists  '"+msgId+"'
			sql.execute("delete from ETX_ADT_USER where USR_ID = '"+userId+"' ")
			// INSERT USER INTO ETX_ADT_USER TABLE
			sql.execute("Insert into ETX_ADT_USER (USR_ID,USR_NAME,USR_PASSWORD,USR_CREATED_ON,USR_UPDATED_ON)" + 
			" values ("+userId+",'"+userName+"','"+userPassword+"',sysdate,sysdate)") 
		}

		//=======================================================================================================================
		def createSystem(systemId, systemName, userId, sysBackendUsrId) { 
			//Delete the system if already exists
			sql.execute("delete from ETX_ADT_SYSTEM where SYS_ID = '"+systemId+"' ")
			// INSERT SYSTEM INTO ETX_ADT_SYSTEM TABLE
			sql.execute("Insert into ETX_ADT_SYSTEM (SYS_ID,SYS_NAME,USR_ID,SYS_BACKEND_USR_ID,SYS_CREATED_ON,SYS_UPDATED_ON)" + 
			" values ("+systemId+",'"+systemName+"',"+userId+","+sysBackendUsrId+",sysdate,sysdate)") 
		}

		//=======================================================================================================================
		def createSystemConfig(scgId, systemId, SCGPropertyName, SCGPropertyValue) { 
			// INSERT SYSTEM INTO ETX_ADT_SYSTEM TABLE
			sql.execute("Insert into ETX_ADT_SYSTEM_CONFIG (SCG_ID,SYS_ID,SCG_PROPERTY_NAME,SCG_PROPERTY_VALUE,SCG_CREATED_ON,SCG_UPDATED_ON)" + 
			" values ("+scgId+","+systemId+",'"+SCGPropertyName+"','"+SCGPropertyValue+"',sysdate,null)") 
		}

		//=======================================================================================================================
		def createParty(partyId, partyUUID, systemId, userId) { 
			//Delete the party if already exists
			sql.execute("delete from ETX_ADT_PARTY where PAR_ID = '"+partyId+"' ")
			// INSERT PARTY INTO ETX_ADT_PARTY TABLE
			sql.execute("Insert into ETX_ADT_PARTY (PAR_ID,PAR_UUID,SYS_ID,PAR_CREATED_ON,PAR_UPDATED_ON,USR_ID,PAR_IS_RECEIVING_MESSAGES)" + 
			" values ("+partyId+",'"+partyUUID+"',"+systemId+",sysdate,sysdate,"+userId+",0)") 
		}

		//=======================================================================================================================
		def createMessageBundle(msgId, msgUUID, msgRefUUID, msgSubject, msgContent, msgSenderParId, msgReceiverParId, msgDirection, msgIssueDateStr) { 
			//Delete the messagebundle if already exists
			sql.execute("delete from ETX_ADT_MESSAGE where MSG_ID = '"+msgId+"' ")
			// INSERT MESSAGE BUNDLE INTO ETX_ADT_MESSAGE TABLE
			sql.execute("Insert into ETX_ADT_MESSAGE (MSG_ID,MSG_TYPE,MSG_UUID,MSG_STATE,MSG_PARENT_UUID,MSG_REF_UUID,MSG_REF_STATE,MSG_SUBJECT,MSG_CONTENT,MSG_SENDER_PAR_ID,MSG_RECEIVER_PAR_ID,MSG_NOTIFICATION_STATE,MSG_DIRECTION,MSG_ISSUE_DT,ERR_ID,MSG_CREATED_ON,MSG_UPDATED_ON,MSG_STATUS_REASON_CODE)" + 
			" values ("+msgId+",'MESSAGE_BUNDLE','"+msgUUID+"','MS_PROCESSED',null,'"+msgRefUUID+"',null,'"+msgSubject+"','"+msgContent+"',"+msgSenderParId+","+msgReceiverParId+",'NF_PENDING','"+msgDirection+"',TO_DATE('"+msgIssueDateStr+"','YYYY-MM-DD HH24:MI:SS'),null,sysdate,sysdate,null)") 
		}

		//=======================================================================================================================
		def createMessageStatus(msgStatusId, msgStatusUUID, msgStatusRefUUID, msgStatusRefState, msgContent, msgSenderParId, msgReceiverParId, msgDirection, msgIssueDateStr) { 
			// INSERT MESSAGE STATUS INTO ETX_ADT_MESSAGE TABLE
			sql.execute("Insert into ETX_ADT_MESSAGE (MSG_ID,MSG_TYPE,MSG_UUID,MSG_STATE,MSG_PARENT_UUID,MSG_REF_UUID,MSG_REF_STATE,MSG_SUBJECT,MSG_CONTENT,MSG_SENDER_PAR_ID,MSG_RECEIVER_PAR_ID,MSG_NOTIFICATION_STATE,MSG_DIRECTION,MSG_ISSUE_DT,ERR_ID,MSG_CREATED_ON,MSG_UPDATED_ON,MSG_STATUS_REASON_CODE)" + 
			" values ("+msgStatusId+",'MESSAGE_STATUS','"+msgStatusUUID+"','MS_PROCESSED',null,'"+msgStatusRefUUID+"','"+msgStatusRefState+"',null,'"+msgContent+"',"+msgSenderParId+","+msgReceiverParId+",'NF_PENDING','"+msgDirection+"',TO_DATE('"+msgIssueDateStr+"','YYYY-MM-DD HH24:MI:SS'),null,sysdate,sysdate,null)") 
		}

		//=======================================================================================================================
		def createAttachment(attachmentId, msgId, attachmentUUID, attachmentType, attachmentFilename, attachmentFilepath, attachmentFilesize, attachmentChecksum) { 
			// INSERT PARTY INTO ETX_ADT_PARTY TABLE
			sql.execute("Insert into ETX_ADT_ATTACHMENT (ATT_ID,MSG_ID,ATT_UUID,ATT_TYPE,ATT_STATE,ATT_DIRECTION,ATT_FILE_NAME,ATT_FILE_PATH,ATT_FILE_SIZE,ATT_MIME_TYPE,ATT_CHECKSUM,ATT_CHECKSUM_ALGORITHM,ATT_LANGUAGE_CODE,ATT_SESSION_KEY,ATT_X509_SUBJECT_NAME,ATT_CREATED_ON,ATT_UPDATED_ON,ERR_ID,ATT_ACTIVE_STATE)" + 
			" values ("+attachmentId+","+msgId+",'"+attachmentUUID+"','"+attachmentType+"','ATTS_UPLOADED','INCOMING','"+attachmentFilename+"','"+attachmentFilepath+"',"+attachmentFilesize+",'application/octet-stream','"+attachmentChecksum+"','SHA_512',null,null,null,sysdate,sysdate,null,1)") 
		}

		//=======================================================================================================================
		def updateInterchangeAgreementURL(newUrl) { 

			// update URL of interchange agreement URL to use mock service
			sql.execute("Update ETX_ADT_CONFIG set ACG_PROPERTY_VALUE = '" + newUrl + "' where ACG_PROPERTY_NAME = 'etx.node.services.RetrieveInterchangeAgreementService.url' ") 
		}

		//=======================================================================================================================
		def revertInterchangeAgreementURL() { 
			// revert URL of interchange agreement URL to use node URL
			sql.execute("Update ETX_ADT_CONFIG set ACG_PROPERTY_VALUE = 'https://webgate.test.ec.europa.eu/e-trustexnode/services/RetrieveInterchangeAgreementsRequest-2.0' where ACG_PROPERTY_NAME = 'etx.node.services.RetrieveInterchangeAgreementService.url' ") 
		}

		//=======================================================================================================================
		def updateApplicationResponseURL(newUrl) { 

			// update URL of interchange agreement URL to use mock service
			sql.execute("Update ETX_ADT_CONFIG set ACG_PROPERTY_VALUE = '" + newUrl + "' where ACG_PROPERTY_NAME = 'etx.node.services.ApplicationResponseService.url' ") 
		}

		//=======================================================================================================================
		def revertApplicationResponseURL() { 
			// revert URL of interchange agreement URL to use node URL
			sql.execute("Update ETX_ADT_CONFIG set ACG_PROPERTY_VALUE = 'https://webgate.test.ec.europa.eu/e-trustexnode/services/ApplicationResponse-2.0' where ACG_PROPERTY_NAME = 'etx.node.services.ApplicationResponseService.url' ") 
		}
		
		//=======================================================================================================================
		def updateDocumentWrapperURL(newUrl) { 

			// update URL of documemt wrapper to use mock service
			sql.execute("Update ETX_ADT_CONFIG set ACG_PROPERTY_VALUE = '" + newUrl + "' where ACG_PROPERTY_NAME = 'etx.node.services.DocumentWrapperService.url' ") 
		}

		//=======================================================================================================================
		def revertDocumentWrapperURL() { 
			// revert URL of documemt wrapper to use node URL
			sql.execute("Update ETX_ADT_CONFIG set ACG_PROPERTY_VALUE = 'https://webgate.test.ec.europa.eu/e-trustexnode/services/DocumentWrapper-2.0' where ACG_PROPERTY_NAME = 'etx.node.services.DocumentWrapperService.url' ") 
		}

		//=======================================================================================================================
		def updateDocumentBundleURL(newUrl) { 

			// update URL of document bundle to use mock service
			sql.execute("Update ETX_ADT_CONFIG set ACG_PROPERTY_VALUE = '" + newUrl + "' where ACG_PROPERTY_NAME = 'etx.node.services.DocumentBundleService.url' ") 
		}

		//=======================================================================================================================
		def revertDocumentBundleURL() { 
			// revert URL of document bundle to use node URL
			sql.execute("Update ETX_ADT_CONFIG set ACG_PROPERTY_VALUE = 'https://webgate.test.ec.europa.eu/e-trustexnode/services/DocumentBundle-2.0' where ACG_PROPERTY_NAME = 'etx.node.services.DocumentBundleService.url' ") 
		}

		//=======================================================================================================================
	    def collectMsgId(bundleId) {
			// collect message ID for the persisted message bundle
			return sql.rows("Select * " + 
					"From ETX_ADT_MESSAGE " +
					"Where MSG_UUID = '"+bundleId+"' ")
		}

		//=======================================================================================================================
	    def collectAttId(attachmentId) {
			// collect attachment ID for the persisted attachment
			return sql.rows("Select * " + 
					"From ETX_ADT_ATTACHMENT " +
					"Where ATT_UUID = '"+attachmentId+"' ")
		}

		//=======================================================================================================================
	    def byteToHexChecksum(inputChecksum) {
			def outputChecksum = ""
			inputChecksum.each {
				outputChecksum = outputChecksum + String.format("%02x", it);
			}
			return outputChecksum.toUpperCase()	
		}

		//=======================================================================================================================
	    def deleteAttachment(msgId) {
			// DELETE ATTACHMENT FROM ETX_ADT_ATTACHMENT TABLE
			sql.execute("delete from ETX_ADT_ATTACHMENT where MSG_ID = '"+msgId+"' ")
		}

		//=======================================================================================================================
	    def deleteMessage(msgId,msgUuid) {
			// DELETE MESSAGE FROM ETX_ADT_MESSAGE TABLE
			sql.execute("delete from ETX_ADT_MESSAGE where MSG_ID = '"+msgId+"' OR MSG_UUID = '"+msgUuid+"' ")
		}

		//=======================================================================================================================
	    def deleteStatus(msgStatusId) {
			// DELETE MESSAGE FROM ETX_ADT_MESSAGE TABLE
			sql.execute("delete from ETX_ADT_MESSAGE where MSG_ID = '"+msgStatusId+"' ")
		}

		//=======================================================================================================================
	    def deleteParty(partyName) {
			// DELETE PARTY FROM ETX_ADT_PARTY TABLE
			sql.execute("delete from ETX_ADT_PARTY where PAR_UUID = '"+partyName+"' ")
		}

		//=======================================================================================================================
	    def deleteSystem(sysId) {
			// DELETE SYSTEM FROM ETX_ADT_SYSTEM TABLE
			sql.execute("delete from ETX_ADT_SYSTEM where SYS_ID = "+sysId+" ")
		}

		//=======================================================================================================================
	    def deleteSystemConfig(sysId) {
			// DELETE SYSTEM FROM ETX_ADT_SYSTEM CONFIG TABLE
			sql.execute("delete from ETX_ADT_SYSTEM_CONFIG where SYS_ID = "+sysId+" ")
		}

		//=======================================================================================================================
	    def deleteUser(userId) {
			// DELETE USER FROM ETX_ADT_USER TABLE
			sql.execute("delete from ETX_ADT_USER where USR_ID = "+userId+" ")
		}

		//=======================================================================================================================
	    def resetCacheAdapter() {
		def cache_user = context.expand('${#Project#RESET_CACHE_USER}');
		def cache_pwd = context.expand('${#Project#RESET_CACHE_PASSWORD}');
		def http = new HTTPBuilder(context.expand('${#Project#RESET_CACHE_URL}'));
		http.request(GET, ContentType.ANY) { req ->
			headers.'Authorization' = 'Basic ' + (cache_user + ':' + cache_pwd).bytes.encodeBase64().toString()
			response.success = { resp  ->
			//    log.debug 'Cache reset successful: ' + (resp.entity.content.text == 'Cache reset.' ? 'true' : 'false');
			}
			response.failure = { resp  ->
			//   log.debug 'Cache reset FAILED'
			    }
			}
		}					
		//===================================================================================================================
	// END CLASS
	}
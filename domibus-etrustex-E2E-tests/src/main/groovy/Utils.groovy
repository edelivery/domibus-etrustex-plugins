import groovy.sql.GroovyRowResult
import service.ConnectionData
import service.EtrustExSqlService

/**
 * @author Arun Raj
 * @version 1.0
 * @since 30/03/2018
 */
class Utils {

    public final int MESSAGE_WAIT_MILLISECONDS = 10000
    public final int MESSAGE_WAIT_ITERATIONS = 120   //wait for 20 minutes total.
    def log
    def context
    def testRunner
    private boolean isA1Plugin
    private boolean isA2Plugin

    Utils(log, context, testRunner) {
        this.testRunner = testRunner
        this.context = context
        this.log = log
        this.isA1Plugin = context.expand('${Properties#GLOBAL_ADAPTOR_AS_A1}') != 'true' // value passed back from context is not exactly a boolean. Hence, this hack.
        this.isA2Plugin = context.expand('${Properties#GLOBAL_ADAPTOR_AS_A2}') != 'true'
    }

    /**
     * To wait until a message becomes in MS_PROCESSED state
     * Based on the transmissionNodeIdentifier (A1_ETRUSTEX or A2_ETRUSTEX) this method will open the connection to the corresponding database of backend plugin.
     * It will query the message with given UUID, type, direction, sender party and receiver party and check if the MSG_STATE = MS_PROCESSED.
     * If message is not in MS_PROCESSED, wait and retry will be done for the number of times specified in MESSAGE_WAIT_ITERATIONS.
     * After the iterations, in case the message is not found or the MSG_STATE is not in MS_PROCESSED, AssertionError will be raised.
     * MESSAGE_BUNDLE or MESSAGE_STATUS or MESSAGE_ADAPTER_STATUS can be used.
     *
     * @param transmissionNodeIdentifier - instance of TransmissionNodeIdentifier#A1_ETRUSTEX or A2_ETRUSTEX
     * @param messageType - instance of MessageType
     * @param direction - instance of MessageDirection
     * @param msgUUID - the UUID of the message
     * @param sender - UUID of the sender party
     * @param receiver - UUID of the receiver party
     */
    void waitForMessageToBeProcessed(def transmissionNodeIdentifier, def messageType, def direction,
                                                  String msgUUID, String sender, String receiver) {
        transmissionNodeIdentifier = transmissionNodeIdentifier as TransmissionNodeIdentifier
        messageType = messageType as MessageType
        direction = direction as MessageDirection
        log.info 'Waiting for Message of type:[' + messageType + '] in direction:[' + direction + '] with Message UUID:[' + msgUUID + '] from sender:[' + sender + '] to receiver:[' + receiver + '] to be in state MS_PROCESSED at:[' + transmissionNodeIdentifier + ']'

        EtrustExSqlService eTrustExSqlService = getDBConn(transmissionNodeIdentifier)
        String msgState = "UNKNOWN"
        int loopCount = 0
        try {
            while (loopCount < MESSAGE_WAIT_ITERATIONS && !"MS_PROCESSED".equalsIgnoreCase(msgState) && !"MS_FAILED".equalsIgnoreCase(msgState)) {
                List<GroovyRowResult> messages = eTrustExSqlService.collectMsg(msgUUID, direction, messageType, sender, receiver)
                if (messages.size() > 0) {
                    msgState = messages.get(0).get("MSG_STATE").toString()
                    log.info "MSG_STATE retrieved from DB:" + msgState
                }
                loopCount++
				if(!"MS_PROCESSED".equalsIgnoreCase(msgState)){
                    log.info loopCount + "/"+MESSAGE_WAIT_ITERATIONS
					sleep(MESSAGE_WAIT_MILLISECONDS)
				}
            }
            assert "MS_PROCESSED" == msgState: "MSG_STATE should be in MS_PROCESSED"
        }
        finally {
            eTrustExSqlService.close()
        }
    }

    /**
     * Wait for the Message Notification status to be in state - NF_NOTIFIED.
     * Intended to check messages for auto notification status.
     *
     * @param transmissionNodeIdentifier
     * @param messageType
     * @param direction
     * @param msgUUID
     * @param sender
     * @param receiver
     */
    void waitForMessageToBeNotified(def transmissionNodeIdentifier, def messageType, def direction,
                                    String msgUUID, String sender, String receiver) {
        transmissionNodeIdentifier = transmissionNodeIdentifier as TransmissionNodeIdentifier
        messageType = messageType as MessageType
        direction = direction as MessageDirection
        log.info 'Waiting for Message of type:[' + messageType + '] in direction:[' + direction + '] with Message UUID:[' + msgUUID + '] from sender:[' + sender + '] to receiver:[' + receiver + '] to have MSG_NOTIFICATION_STATE = NF_NOTIFIED at:[' + transmissionNodeIdentifier + ']'

        EtrustExSqlService eTrustExSqlService = getDBConn(transmissionNodeIdentifier)
        String msgNotificationState = "UNKNOWN"
        int loopCount = 0
        try {
            while (loopCount < MESSAGE_WAIT_ITERATIONS && !"NF_NOTIFIED".equalsIgnoreCase(msgNotificationState)) {
                List<GroovyRowResult> messages = eTrustExSqlService.collectMsg(msgUUID, direction, messageType, sender, receiver)
                if (messages.size() > 0) {
                    msgNotificationState = messages.get(0).get("MSG_NOTIFICATION_STATE").toString()
                    log.info "MSG_NOTIFICATION_STATE retrieved from DB:" + msgNotificationState
                }
                loopCount++
				if(!"NF_NOTIFIED".equalsIgnoreCase(msgNotificationState)){
                    log.info loopCount + "/"+MESSAGE_WAIT_ITERATIONS
					sleep(MESSAGE_WAIT_MILLISECONDS)
				}
            }
            assert "NF_NOTIFIED" == msgNotificationState: "MSG_NOTIFICATION_STATE should be in NF_NOTIFIED"
        }
        finally {
            eTrustExSqlService.close()
        }
    }

    /**
     * To wait until a message referencing a given messageUUID becomes in MS_PROCESSED state.
     * Based on the transmissionNodeIdentifier (A1_ETRUSTEX or A2_ETRUSTEX) this method will open the connection to the corresponding database of backend plugin.
     * It will query the message having MSG_REF_UUID as the given msgRefUUID, messageType, direction, sender party and receiver party and check if the MSG_STATE = MS_PROCESSED.
     * If message is not in MS_PROCESSED, wait and retry will be done for the number of times specified in MESSAGE_WAIT_ITERATIONS.
     * After the iterations, in case the message is not found or the MSG_STATE is not in MS_PROCESSED, AssertionError will be raised.
     *
     * e.g: Scenario 1 - when a bundle is uploaded to ETX Node, ETX Node will send an AVAILABLE status message to A1 which will refer the original bundle.
     * This method can be used to wait and check when the AVAILABLE status message will be in MS_PROCESSED. In this case chkFor_BDL_OK_msg should be true.
     *
     * e.g: Scenario 2 - when a message upload to B2 fails, a new MESSAGE_ADAPTER_STATUS will be created to handle the notification.
     * This method can be used to wait and check when the notification MESSAGE_ADAPTER_STATUS will be in MS_PROCESSED. In this case chkFor_BDL_OK_msg should be false.
     *
     * @param transmissionNodeIdentifier - instance of TransmissionNodeIdentifier#A1_ETRUSTEX or A2_ETRUSTEX
     * @param messageType - instance of MessageType
     * @param direction - instance of MessageDirection
     * @param msgRefUUID - UUID of the message which the current message is referencing
     * @param sender - UUID of the sender party
     * @param receiver - UUID of the receiver party
     * @param chkFor_BDL_OK_msg - boolean flag to specify if check is to be done for BDL_OK status messages
     */
    void waitForMessageStatusReferencingMessageToBeProcessed(
            def transmissionNodeIdentifier, def messageType,
            def direction, String msgRefUUID, String sender, String receiver, boolean chkFor_BDL_OK_msg) {
        transmissionNodeIdentifier = transmissionNodeIdentifier as TransmissionNodeIdentifier
        messageType = messageType as MessageType
        direction = direction as MessageDirection
        log.info 'Waiting for Message of type:[' + messageType + '] in direction:[' + direction + '] from sender:[' + sender + '] to receiver:[' + receiver + '] referencing message with UUID:[' + msgRefUUID + '] to be in state MS_PROCESSED at:[' + transmissionNodeIdentifier + ']'

        EtrustExSqlService eTrustExSqlService = getDBConn(transmissionNodeIdentifier)
        String msgState = "UNKNOWN"
        int loopCount = 0
        try {
            while (loopCount < MESSAGE_WAIT_ITERATIONS && !"MS_PROCESSED".equalsIgnoreCase(msgState) && !"MS_FAILED".equalsIgnoreCase(msgState)) {
                List<GroovyRowResult> messages = eTrustExSqlService.collectMsgWithMessageReferenceId(msgRefUUID, direction, messageType, sender, receiver)
                if (messages.size() > 0) {
                    for (GroovyRowResult msg : messages) {
                        String msgUUID = messages.get(0).get("MSG_UUID").toString()
                        if (chkFor_BDL_OK_msg && msgUUID.contains("_BDL_OK")) {
                            msgState = messages.get(0).get("MSG_STATE").toString()
                            log.info "Retrieved from DB, MSG_UUID:[" + msgUUID + "] and MSG_STATE:[" + msgState + "]"
                            break
                        }
                        if (!chkFor_BDL_OK_msg && !msgUUID.contains("_BDL_OK")) {
                            msgState = messages.get(0).get("MSG_STATE").toString()
                            log.info "Retrieved from DB, MSG_UUID:[" + msgUUID + "] and MSG_STATE:[" + msgState + "]"
                            break
                        }
                    }
                }
                loopCount++
				if(!"MS_PROCESSED".equalsIgnoreCase(msgState)){
                    log.info loopCount + "/"+MESSAGE_WAIT_ITERATIONS
					sleep(MESSAGE_WAIT_MILLISECONDS)
				}
            }
            assert "MS_PROCESSED" == msgState: "MSG_STATE should be in MS_PROCESSED"
        }
        finally {
            eTrustExSqlService.close()
        }
    }



    /**
     * To wait until a message referencing a given messageUUID becomes in MSG_NOTIFICATION_STATE = NF_NOTIFIED .
     * Based on the transmissionNodeIdentifier (A1_ETRUSTEX or A2_ETRUSTEX) this method will open the connection to the corresponding database of backend plugin.
     * It will query the message having MSG_REF_UUID as the given msgRefUUID, messageType, direction, sender party and receiver party and check if the MSG_NOTIFICATION_STATE = NF_NOTIFIED .
     *
     * @param transmissionNodeIdentifier - instance of TransmissionNodeIdentifier#A1_ETRUSTEX or A2_ETRUSTEX
     * @param messageType - instance of MessageType
     * @param direction - instance of MessageDirection
     * @param msgRefUUID - UUID of the message which the current message is referencing
     * @param sender - UUID of the sender party
     * @param receiver - UUID of the receiver party
     * @param chkFor_BDL_OK_msg - boolean flag to specify if check is to be done for BDL_OK status messages
     */
    void waitForMessageStatusReferencingMessageToBeNotified(
            def transmissionNodeIdentifier, def messageType,
            def direction, String msgRefUUID, String sender, String receiver, boolean chkFor_BDL_OK_msg) {
        transmissionNodeIdentifier = transmissionNodeIdentifier as TransmissionNodeIdentifier
        messageType = messageType as MessageType
        direction = direction as MessageDirection
        log.info 'Waiting for Message of type:[' + messageType + '] in direction:[' + direction + '] from sender:[' + sender + '] to receiver:[' + receiver + '] referencing message with UUID:[' + msgRefUUID + '] to be in MSG_NOTIFICATION_STATE = NF_NOTIFIED at:[' + transmissionNodeIdentifier + ']'

        EtrustExSqlService eTrustExSqlService = getDBConn(transmissionNodeIdentifier)
        String msgNotificationState = "UNKNOWN"
        int loopCount = 0
        try {
            while (loopCount < MESSAGE_WAIT_ITERATIONS && !"NF_NOTIFIED".equalsIgnoreCase(msgNotificationState) ) {
                List<GroovyRowResult> messages = eTrustExSqlService.collectMsgWithMessageReferenceId(msgRefUUID, direction, messageType, sender, receiver)
                if (messages.size() > 0) {
                    for (GroovyRowResult msg : messages) {
                        String msgUUID = messages.get(0).get("MSG_UUID").toString()
                        if (chkFor_BDL_OK_msg && msgUUID.contains("_BDL_OK")) {
                            msgNotificationState = messages.get(0).get("MSG_NOTIFICATION_STATE").toString()
                            log.info "Retrieved from DB, MSG_UUID:[" + msgUUID + "] and MSG_NOTIFICATION_STATE:[" + msgNotificationState + "]"
                            break
                        }
                        if (!chkFor_BDL_OK_msg && !msgUUID.contains("_BDL_OK")) {
                            msgNotificationState = messages.get(0).get("MSG_NOTIFICATION_STATE").toString()
                            log.info "Retrieved from DB, MSG_UUID:[" + msgUUID + "] and MSG_NOTIFICATION_STATE:[" + msgNotificationState + "]"
                            break
                        }
                    }
                }
                loopCount++
                if(!"NF_NOTIFIED".equalsIgnoreCase(msgNotificationState)){
                    log.info loopCount + "/"+MESSAGE_WAIT_ITERATIONS
                    sleep(MESSAGE_WAIT_MILLISECONDS)
                }
            }
            assert "NF_NOTIFIED" == msgNotificationState: "MSG_NOTIFICATION_STATE should be in NF_NOTIFIED"
        }
        finally {
            eTrustExSqlService.close()
        }
    }


    EtrustExSqlService getDBConn(TransmissionNodeIdentifier transmissionNodeIdentifier) {
        switch (transmissionNodeIdentifier) {
            case TransmissionNodeIdentifier.A1_ETRUSTEX: return initA1DBEtrustex()
                break
            case TransmissionNodeIdentifier.A2_ETRUSTEX: return initA2DBEtrustex()
                break
        }
    }

    // Ensure that connections can be successfully obtained to all DBs
    EtrustExSqlService initA1DBEtrustex() {
        def connection = ConnectionData.A1_ETRUSTEX.with(context)
        log.info 'Init A1 DB EtrustEx: ' + connection.toString()
        EtrustExSqlService etrustExSqlService = new EtrustExSqlService(connection);
        log.info 'A1 EtrustEx OK'
        return etrustExSqlService
    }

    EtrustExSqlService initA2DBEtrustex() {
        def connection = ConnectionData.A2_ETRUSTEX.with(context)
        log.info 'Init A2 DB EtrustEx: ' + connection.toString()
        EtrustExSqlService etrustExSqlService = new EtrustExSqlService(connection);
        log.info 'A2 EtrustEx OK'
        return etrustExSqlService
    }

    void clearProperties_TestCase_TestSuite(String propertiesTestStepName){
        log.info('Clearing custom properties for TestCase:['+testRunner.testCase.name+'] in TestSuite:['+testRunner.testCase.testSuite.name+']');

        if(propertiesTestStepName != null && propertiesTestStepName != ''){
            testRunner.testCase.testSteps[propertiesTestStepName].clearPropertyValues()
        }

        testRunner.testCase.properties.each {
            testRunner.testCase.properties[it.key].value = ''
        }

        testRunner.testCase.testSuite.properties.each {
            testRunner.testCase.testSuite.properties[it.key].value = ''
        }
    }
}

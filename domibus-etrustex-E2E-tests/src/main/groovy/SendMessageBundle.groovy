import dto.Attachment
import dto.AttachmentBuilder
import dto.Request
import dto.RequestBuilder

class SendMessageBundle implements SendMessage {

    Request parseMessageSent(testStep) {
        def reqStep = testStep.getProperty("rawRequest")
        def requestNode = new XmlSlurper().parseText(reqStep.value)
        def request = requestNode.Body.sendMessageBundleRequest

        List<Attachment> attachmentList = request.messageBundle.fileMetadataList.file.collect {
            att ->
                AttachmentBuilder
                        .getInstance()
                        .withId(att.id.text())
                        .withSize(att.size.text())
                        .withMimeType(att.mimeType.text())
                        .withName(att.name.text())
                        .withChecksum(att.checksum.text())
                        .build()
        }

        Request result = RequestBuilder
                .getInstance()
                .withReferenceId(request.messageBundle.referenceId.text())
                .withSender(request.messageBundle.sender.id.text())
                .withReceivers(request.messageBundle.receiverList.collect{it.recipient.id})
                .withId(request.messageBundle.id.text())
                .withIssueDateTime(request.messageBundle.issueDateTime.text())
                .withReceivedDateTime(request.messageBundle.receivedDateTime.text())
                .withSubject(request.messageBundle.subject.text())
                .withContent(request.messageBundle.content.text())
                .withSignedData(request.messageBundle.signedData.text())
                .withSignature(request.messageBundle.signature.text())
                .withFileMetadataList(attachmentList)
                .build()

        return result
    }
}

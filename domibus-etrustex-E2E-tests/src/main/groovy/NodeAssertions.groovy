import groovy.sql.GroovyRowResult
import org.hamcrest.CoreMatchers
import report.Report
import service.EtrustExNodeSqlService
import dto.Request

import static report.Assert.assertThat

class NodeAssertions {
    private EtrustExNodeSqlService sqlService
    private Request request
    private Report report

    NodeAssertions(EtrustExNodeSqlService etrustEx, Request request) {
        this.sqlService = etrustEx
        this.request = request
        this.report = new Report("EtrustEx Node")
    }

    Report checkMessage() {
        def bundleId = request.getId()
        def sender = request.getSender()
        List<String> receivers = request.getReceivers()
        String receiver = receivers.get(0)
        List<GroovyRowResult> messages = sqlService.collectMsgId(bundleId, sender, receiver)
        report.add(assertThat("Number of messages is: 1", messages.size(), CoreMatchers.is(1)))
        // log.info "--------End of Node  Assertions--------"
        return report
    }
}

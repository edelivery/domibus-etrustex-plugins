/**
 * @author Arun Raj
 * @version 1.0
 * @since 04/04/2018
 */
enum TransmissionNodeIdentifier {
    A1_ETRUSTEX,
    A1_DOMIBUS,
    A2_ETRUSTEX,
    A2_DOMIBUS,
    AN_ETRUSTEX,
    AN_DOMIBUS
}
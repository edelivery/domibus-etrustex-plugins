import dto.Request

class SendMessageStatus implements SendMessage{

    Request parseMessageSent(Object requestNode) {
        def reqParamMap = [
                statusId         : requestNode.Body.sendMessageStatusRequest.messageStatus.id.text(),
                senderPartyName  : requestNode.Body.sendMessageStatusRequest.messageStatus.sender.id.text(),
                receiverPartyName: requestNode.Body.sendMessageStatusRequest.messageStatus.receiver.id.text(),
                bundleId         : requestNode.Body.sendMessageStatusRequest.messageStatus.messageReference.id.text(),
                issueDateTime    : requestNode.Body.sendMessageStatusRequest.messageStatus.issueDateTime.text(),
                receivedDateTime : requestNode.Body.sendMessageStatusRequest.messageStatus.receivedDateTime.text()];

        return reqParamMap
    }
}

import dto.Attachment
import dto.Request
import groovy.sql.GroovyRowResult
import org.hamcrest.CoreMatchers
import report.Report
import service.EtrustExSqlService

import static report.Assert.assertThat

class A2EtrustExAssertions {
    private EtrustExSqlService sqlService
    private Request request
    private Report report
    def log
    private List<GroovyRowResult> bundleMessages
    private List<GroovyRowResult> statusMessages_read

    // Constructor
    A2EtrustExAssertions(EtrustExSqlService etrustEx, Request request, log) {
        this.sqlService = etrustEx
        this.request = request
        this.report = new Report("A2EtrustExAssertions")
        this.log = log
    }

    /*  Retrieve message bundles/attachments and verify content is accurate - expected and actual values match (based on
        expected element names in request message) and add assertions to report. */
    Report checkMessageBundle() {

        /* common tasks to be wrapped in a private method */
        def msg_uuid = request.getId()
        def senderParty = request.getSender() // TESTEDEL_EGREFFE_PLUGIN_SENDER1 - configured with Auto-notification
        List<String> receivers = request.getReceivers() // Always returns a single entity as each sendMessageBundle() instance always sends to a single receiver
        String receiverParty = receivers.get(0) // TESTEDEL_EGREFFE_PLUGIN_SENDER1

        /*  MessageBundle - extract bundle messages (execute sql query to extract bundle messages - not message statuses)
            based on a specific sender-receiver scenario - resultset should always contain 1 row   */
        bundleMessages = sqlService.collectMsg(msg_uuid, 'IN', 'MESSAGE_BUNDLE', senderParty, receiverParty)
//        log.info "senderParty: " + senderParty + " receiverParty: " + receiverParty
//        log.info "msg_uuid: " + msg_uuid
//        log.info "bundleMessages size is: " + bundleMessages.size()
//        for(int i=0; i<bundleMessages.size(); i++){log.info "||||||A2||||| bundle contents are ||||||||||: " + bundleMessages.getAt(i)}

        if(!bundleMessages){log.info "-------------- BundleMessages is NULL ------------- checkMessageBundle "}
        report.add(assertThat("Number of messages is: 1", this.bundleMessages.size(), CoreMatchers.is(1)))

        if (this.bundleMessages.size() > 0) {
            GroovyRowResult bundleMessage = this.bundleMessages.get(0)

            def senderParId = bundleMessage.get("MSG_SENDER_PAR_ID").toString() // "41"
            List<GroovyRowResult> senderParties = sqlService.collectParties(senderParId)
            GroovyRowResult senderPartyRow = senderParties.get(0)
            def receiverParId = bundleMessage.get("MSG_RECEIVER_PAR_ID").toString() // "42" or "43"
            List<GroovyRowResult> receiverParties = sqlService.collectParties(receiverParId)
            GroovyRowResult receiverPartyRow = receiverParties.get(0)

//            log.info " -- senderParId (checkMessageBundle - A2) -- " + senderParId
//            log.info " -- receiverParId (checkMessageBundle - A2) -- " + receiverParId

            log.info "--------A2  EtrustEx Assertions - BUNDLE MESSAGES --------"
            report.add(assertThat("MessageBundle State is Incorrect (BUNDLE MESSAGES)", bundleMessage.get("MSG_STATE").toString(), CoreMatchers.is(MessageState.MS_PROCESSED.name())))
            /* Add assertion for MSG_PARENT_UUID with specific conditions - confirm details */
            report.add(assertThat("MessageBundle REF ID is Incorrect (BUNDLE MESSAGES)", bundleMessage.get("MSG_REF_UUID").toString(), CoreMatchers.is(request.getReferenceId())))
            report.add(assertThat("MSG_REF_STATE is Incorrect (BUNDLE MESSAGES)", bundleMessage.get("MSG_REF_STATE"), CoreMatchers.nullValue()))
            report.add(assertThat("MessageBundle Subject is Incorrect (BUNDLE MESSAGES)", bundleMessage.get("MSG_SUBJECT").toString(), CoreMatchers.is(request.getSubject())))
            report.add(assertThat("MessageBundle Content is Incorrect (BUNDLE MESSAGES)", bundleMessage.get("MSG_CONTENT").toString(), CoreMatchers.is(request.getContent())))
            report.add(assertThat("MessageBundle Sender ID is Incorrect (BUNDLE MESSAGES)", senderPartyRow.get("PAR_UUID").toString(), CoreMatchers.is(senderParty)))
            report.add(assertThat("MessageBundle Receiver Party is Incorrect (BUNDLE MESSAGES)", receiverPartyRow.get("PAR_UUID").toString(), CoreMatchers.is(receiverParty)))
            report.add(assertThat("MSG_NOTIFICATION_STATE is Incorrect (STATUS _BDL_OK)", bundleMessage.get("MSG_NOTIFICATION_STATE").toString(), CoreMatchers.is("NF_NOTIFIED")))
            report.add(assertThat("ERR_ID is Incorrect - should be NULL VALUE", bundleMessage.get("ERR_ID"), CoreMatchers.nullValue()))
            /* Add assertion for MSG_SIGNATURE with specific conditions i.e. if EDMA/DECIDE/EGREFFE - confirm details */
            /* Add assertion for MSG_SIGNED_DATA with specific conditions i.e. if EDMA/DECIDE/EGREFFE - confirm details */
            report.add(assertThat("MessageBundle Message Issue Date is Incorrect (BUNDLE MESSAGES)", bundleMessage.get("MSG_ISSUE_DT").toString().substring(0, 10), CoreMatchers.is(request.getIssueDateTime().toString().substring(0, 10))))
            report.add(assertThat("MessageBundle Message Issue Time is Incorrect (BUNDLE MESSAGES)", bundleMessage.get("MSG_ISSUE_DT").toString().substring(12, 19), CoreMatchers.is(request.getIssueDateTime().toString().substring(12, 19))))
            report.add(assertThat("MessageBundle Domibus Message ID is null (BUNDLE MESSAGES)", bundleMessage.get("DOMIBUS_MESSAGEID").toString(), CoreMatchers.notNullValue()))
            report.add(assertThat("MessageBundle Number of senders found is: 1 (BUNDLE MESSAGES)", senderParties.size(), CoreMatchers.is(1)))
            report.add(assertThat("MessageBundle Number of receivers is not: 1 (BUNDLE MESSAGES)", receiverParties.size(), CoreMatchers.is(1)))

//            log.info "A2---------Message UUID: ---------" + bundleMessage.get("MSG_UUID")
//            log.info "message value of date: " + bundleMessage.get("MSG_ISSUE_DT").toString()
//            log.info " A2  " + request.getReceivers() + request.getSender()
//            log.info "request value of date: " + request.getIssueDateTime().toString()

            /* Message Attachments */
            def attachmentId = bundleMessage.get("MSG_ID").toString()
            List<Attachment> messageAttachments = request.getFileMetadataList()
            Attachment attachments = messageAttachments.get(0)
            List<GroovyRowResult> allAttachments = sqlService.collectAttachments(attachmentId)
            GroovyRowResult allAttachmentRow = allAttachments.get(0)
            List<GroovyRowResult> allCheckSum = sqlService.collectCheckSum(attachmentId)
            GroovyRowResult allCheckSumRow = allCheckSum.get(0)

            log.info "--------A2  EtrustEx Assertions - ATTACHMENTS --------"

//            log.info "=============**************================= messageAttachments (DB) value =============**************=================" + (messageAttachments.get(0)).getId()
//            log.info "=============**************================= allAttachments (request) value =============**************=================" + (allAttachments.get(0)).get("ATT_UUID")
//            log.info "=============**************================= messageAttachments (DB) value =============**************=================" + (messageAttachments.get(1)).getId()
//            log.info "=============**************================= allAttachments (request) value =============**************=================" + (allAttachments.get(1)).get("ATT_UUID")
//            log.info "=============**************================= messageAttachments (DB) value =============**************=================" + (messageAttachments.get(2)).getId()
//            log.info "=============**************================= allAttachments (request) value =============**************=================" + (allAttachments.get(2)).get("ATT_UUID")
//            log.info "=============**************================= messageAttachments (DB) value =============**************=================" + (messageAttachments.get(3)).getId()
//            log.info "=============**************================= allAttachments (request) value =============**************=================" + (allAttachments.get(3)).get("ATT_UUID")
//            log.info "=============**************================= messageAttachments (DB) value =============**************=================" + (messageAttachments.get(4)).getId()
//            log.info "=============**************================= allAttachments (request) value =============**************=================" + (allAttachments.get(4)).get("ATT_UUID")

            for(int i=0; i<messageAttachments.size(); i++)
                for(int j=0; j<allAttachments.size(); j++) {
                    if( (messageAttachments.get(i)).getId() == allAttachments.get(j).get("ATT_UUID")) {
                        report.add(assertThat("Attachment ID is null", allAttachments.get(j).get("ATT_ID").toString(), CoreMatchers.notNullValue()))  // redundant as conditional check already affirms its non-null state
                        if (receiverParty == "TESTEDEL_EGREFFE_PLUGIN_RECEIVER1" || receiverParty == "TESTEDEL_EGREFFE_ADAPTER_RECEIVER1") {
                            report.add(assertThat("Attachment State is Incorrect", allAttachments.get(j).get("ATT_STATE").toString(), CoreMatchers.is(AttachmentState.ATTS_UPLOADED.name())))
                        } else (report.add(assertThat("Attachment State is Incorrect", allAttachments.get(j).get("ATT_STATE").toString(), CoreMatchers.is(AttachmentState.ATTS_ALREADY_UPLOADED.name()))))
                        report.add(assertThat("Attachment Name is Incorrect", allAttachments.get(j).get("ATT_FILE_NAME").toString(), CoreMatchers.is(messageAttachments.get(j).name)))
                        report.add(assertThat("File Size is Incorrect", allAttachments.get(j).get("ATT_FILE_SIZE").toString(), CoreMatchers.is(messageAttachments.get(j).getSize())))
                        report.add(assertThat("Mime Type is Incorrect", allAttachments.get(j).get("ATT_MIME_TYPE").toString(), CoreMatchers.is(messageAttachments.get(j).getMimeType())))
                        report.add(assertThat("CheckSum is Incorrect", allCheckSumRow.get("RAWTOHEX(ATT_CHECKSUM)"), CoreMatchers.is(messageAttachments.get(j).getChecksum())))
                        report.add(assertThat("ERR_ID is Incorrect - should be NULL VALUE", allAttachments.get(j).get("ERR_ID"), CoreMatchers.nullValue()))
                        report.add(assertThat("MessageBundle Domibus Message ID is null", allAttachments.get(j).get("DOMIBUS_MESSAGEID").toString(), CoreMatchers.notNullValue()))
                        report.add(assertThat("Number of attachments is : 5", allAttachments.size(), CoreMatchers.is(5)))
                    }
                }
        }
        return report
    }

    /*  Retrieve message statuses and verify content is accurate - expected and actual values match (based on
        expected element names in request message) and add assertions to report */
    Report checkMessageStatus() {

        /* common tasks to be wrapped in a private method */
        def msg_ref_uuid = request.getId()
        def bundle_read = msg_ref_uuid + '_READ%'

        def senderParty = request.getSender() // TESTEDEL_EGREFFE_PLUGIN_SENDER1 - configured with Auto-notification
        List<String> receivers = request.getReceivers() // Always returns a single entity as each sendMessageBundle() instance always sends to a single receiver
        String receiverParty = receivers.get(0) // TESTEDEL_EGREFFE_PLUGIN_SENDER1

        /*  MessageStatus - extract status messages (execute sql query to extract status messages - not message bundles)
            based on a specific sender-receiver scenario - resultset should always contain 1 row  */
        statusMessages_read = sqlService.collectMsgStatus(bundle_read, 'OUT', 'MESSAGE_STATUS', senderParty, receiverParty)

        if(!statusMessages_read){ log.info "-----A2--------- statusMessages_read equals NULL ------------- checkMessageStatus " }
        log.info "-----A2-----"
//        log.info "senderParty: " + senderParty + " receiverParty: " + receiverParty
//        log.info "msg_ref_uuid: " + msg_ref_uuid
//        log.info "bundle_read: " + bundle_read
//        log.info "statusMessages (_READ) size is : " + statusMessages_read.size()
//        for(int i=0; i<statusMessages_read.size(); i++){log.info "||||||A2||||| statusMessages_read contents are ||||||||||: " + statusMessages_read.getAt(i)}

        /* Assertions */
        report.add(assertThat("Number of statusMessages_read (STATUS _READ) is: 1", this.statusMessages_read.size(), CoreMatchers.is(1)))

        if (this.statusMessages_read.size() > 0) {
            GroovyRowResult statusMessage_read = this.statusMessages_read.get(0)

            def senderParId = statusMessage_read.get("MSG_SENDER_PAR_ID").toString() // "41"
            List<GroovyRowResult> senderParties = sqlService.collectParties(senderParId)
            GroovyRowResult senderPartyRow = senderParties.get(0)
            def receiverParId = statusMessage_read.get("MSG_RECEIVER_PAR_ID").toString() // "42" or "43"
            List<GroovyRowResult> receiverParties = sqlService.collectParties(receiverParId)
            GroovyRowResult receiverPartyRow = receiverParties.get(0)

//            log.info " -- senderParId (checkMessageStatus - A2) -- " + senderParId
//            log.info " -- receiverParId (checkMessageStatus - A2) -- " + receiverParId

            log.info "--------A2  EtrustEx Assertions - STATUS MESSAGES --------"

            /* Status Message _READ */
            report.add(assertThat("Message State is Incorrect (STATUS _READ)", statusMessage_read.get("MSG_STATE").toString(), CoreMatchers.is(MessageState.MS_PROCESSED.name())))
            /* Add assertion for MSG_PARENT_UUID with specific conditions - confirm details */
            report.add(assertThat("Message MSG REF UUID is Incorrect (STATUS _READ)", statusMessage_read.get("MSG_REF_UUID").toString(), CoreMatchers.is(request.getId())))
            report.add(assertThat("Message Ref State is Incorrect (STATUS _READ)", statusMessage_read.get("MSG_REF_STATE").toString(), CoreMatchers.is("READ")))
            report.add(assertThat("MSG_SUBJECT is Incorrect (STATUS _READ)", statusMessage_read.get("MSG_SUBJECT"), CoreMatchers.nullValue()))
            report.add(assertThat("MSG_CONTENT is Incorrect (STATUS _READ)", statusMessage_read.get("MSG_CONTENT"), CoreMatchers.nullValue()))
            report.add(assertThat("SenderParty/ID is Incorrect", senderPartyRow.get("PAR_UUID").toString(), CoreMatchers.is(receiverParty)))
            report.add(assertThat("ReceiverParty/ID is Incorrect", receiverPartyRow.get("PAR_UUID").toString(), CoreMatchers.is(senderParty)))
            report.add(assertThat("Message Notification state is Incorrect (STATUS _READ)", statusMessage_read.get("MSG_NOTIFICATION_STATE"), CoreMatchers.is(CoreMatchers.nullValue())))
            report.add(assertThat("MessageStatus Issue Date is Incorrect (STATUS _READ)", statusMessage_read.get("MSG_ISSUE_DT").toString().substring(0, 10), CoreMatchers.is(request.getIssueDateTime().toString().substring(0, 10))))
            report.add(assertThat("MessageStatus Issue Time is Incorrect (STATUS _READ)", statusMessage_read.get("MSG_ISSUE_DT").toString().substring(12, 19), CoreMatchers.is(request.getIssueDateTime().toString().substring(12, 19))))
            report.add(assertThat("ERR_ID is Incorrect - should be NULL VALUE (STATUS _READ)", statusMessage_read.get("ERR_ID"), CoreMatchers.nullValue()))
            /* Add assertion for MSG_SIGNATURE with specific conditions i.e. if EDMA/DECIDE/EGREFFE - confirm details */
            /* Add assertion for MSG_SIGNED_DATA with specific conditions i.e. if EDMA/DECIDE/EGREFFE - confirm details */
            report.add(assertThat("Domibus Message ID is null (STATUS _READ)", statusMessage_read.get("DOMIBUS_MESSAGEID").toString(), CoreMatchers.notNullValue()))

//            log.info "message value of date: " + statusMessage_read.get("MSG_ISSUE_DT").toString()
//            log.info "   "
//            log.info "request value of date: " + request.getIssueDateTime().toString()
        }
        return report
    }
}
import MessageState
import dto.Request
import groovy.sql.GroovyRowResult
import org.hamcrest.CoreMatchers
import report.Report
import service.DomibusSqlService
import service.EtrustExSqlService

import static report.Assert.assertThat

class A1DomibusAssertions {
    private DomibusSqlService sqlService
    private Request request
    private Report report
    def log

    A1DomibusAssertions(DomibusSqlService sql, log) {
        this.log = log
        this.sqlService = sql
        this.report = new Report("A1 Domibus Assertions")
    }

    Report checkMessageBundle(String domibusMessageId) {
        log.info 'MessageBundle'

        List<GroovyRowResult> messages = sqlService.collectMsgLogByMsgId(domibusMessageId)
        log.info messages
        report.add(assertThat("Number of messages is: 1", messages.size(), CoreMatchers.is(1)))
        if(messages.size() != 0) {
            GroovyRowResult message = messages.get(0)
            log.info 'State of message: ' + message.get("MESSAGE_STATUS").toString()
            log.info 'State of notification: ' + message.get("NOTIFICATION_STATUS").toString()
        }
        return report
    }
    Report checkMessageAttachement(List<String> domibusAttachmentId) {
        log.info 'Attachments'
        report.add(assertThat("Number of attachment is: 5", domibusAttachmentId.size(), CoreMatchers.is(5)))
        List<GroovyRowResult> messages = sqlService.collectMsgLogByMsgId(domibusAttachmentId.get(0))
        log.info messages
        if(messages.size() != 0) {
            report.add(assertThat("Number of messages is: 1", messages.size(), CoreMatchers.is(1)))
            GroovyRowResult message = messages.get(0)
            log.info 'State of message: ' + message.get("MESSAGE_STATUS").toString()
            log.info 'State of notification: ' + message.get("NOTIFICATION_STATUS").toString()
        }
        return report
    }

}


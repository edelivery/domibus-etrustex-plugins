import groovy.sql.Sql

class InsertMsgDataRow {

    def log
    def context
    def testRunner
    def DB
    def sql

    InsertMsgDataRow(log, context, testRunner) {
        this.testRunner = testRunner
        this.context = context
        this.log = log
        this.DB = "jdbc:oracle:thin:ETX_BACKEND_PLUGIN/fw3u=16j@olrdev3.cc.cec.eu.int:1597/EX1UDIGT_TAF.cc.cec.eu.int"
        this.sql = Sql.newInstance(DB)
    }

    def bundleid = UUID.randomUUID().toString()
    def bundleid_ok = bundleid + "_BDL_OK"

    // prepare SQL
    def insertMsgSql = "INSERT INTO ETX_ADT_MESSAGE VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
    def insertAttSql = "INSERT INTO ETX_ADT_ATTACHMENT VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

    def insert() {
        log.info ' ' + DB
        log.info ' ' + bundleid
        Class.forName("oracle.jdbc.driver.OracleDriver")
        sql.execute(insertMsgSql, ['9999999','MESSAGE_STATUS',bundleid_ok,'MS_PROCESSED','',bundleid,'AVAILABLE','','Available','42','41','NF_NOTIFIED','IN','05-JAN-18','','','','05-JAN-18','05-JAN-18','BDL:1','8583aaf6-fd58-41df-91f8-e6fb8f4895ee@domibus.eu'])
        sql.execute(insertMsgSql, ['9999998','MESSAGE_BUNDLE',bundleid,'MS_PROCESSED','','EUPL V1.2','','EUPL TEST SUBJECT','EUPL TEST MSG CONTENT','41','42','','OUT','05-JAN-18','','','','05-JAN-18','05-JAN-18','','1504032e-42d0-4d96-b691-d492ebc6558c@domibus.eu'])
        sql.execute(insertAttSql, ['9999997','9999998','eupl1.1.-licence-en_0.pdfBUNDLE_ID_d12e5177-e371-49a5-b856-e7558de2d070_1','BINARY','ATTS_UPLOADED','OUTGOING','eupl1.1.-licence-en_0.pdf','RELATIVE PATH','34271','application/pdf','26316A5CAE50C2234CA16717C1F1CBE7B0330F7AA663BB94BEB622F1D4751634B4CA4A8CF4298974214C40353C5E6FAEE954561830871B5EFAC02776E9F925CB','SHA_512','','','','05-JAN-18','05-JAN-18','','1','1504032e-42d0-4d96-b691-d492ebc6558c@domibus.eu'])

        sql.close()

        return bundleid
    }
}

enum MessageState {
    MS_CREATED,
    MS_UPLOADED,
    MS_PROCESSED,
    MS_FAILED
}

package report

import org.hamcrest.Matcher

class Assert {


    /**
     * To replace the previous assert method
     *
     * @param reason
     * @param actual
     * @param matcher
     * @return [error:{true/false}, message: "..."]
     */
    static <T> Object assertThat(String reason, T actual, Matcher<? super T> matcher){
        try {
            org.junit.Assert.assertThat(reason, actual, matcher)
        } catch (AssertionError e) {
            return [error:true, message: e.getMessage()]
        }
        return [error:false, message: reason]
    }
}

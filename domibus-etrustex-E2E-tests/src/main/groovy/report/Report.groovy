package report

import dto.Request

class Report {
    String sqlUrl
    Request request

    List<String> errors = new ArrayList<>()
    List<String> passedAssertions = new ArrayList<>()
    String context

    Report(String context) {
        this.context = context
    }

    void add(assertion) {
        if (assertion.error) {
            errors.add(context + " - " + assertion.message)
        } else {
            passedAssertions.add(context + " - " + assertion.message +" OK")
        }
    }

    void addAllErrors(List<String> errors){
        this.errors.addAll(errors)
    }
    void addAllPassedAssertions(List<String> passedAssertions){
        this.passedAssertions.addAll(passedAssertions)
    }

    void add(Report report){
        if(report){
            this.addAllErrors(report.getErrors())
            this.addAllPassedAssertions(report.getPassedAssertions())
        }
    }
}


eTrustEx Plugins Arun Test data setup: ORACLE
=======================================

Notes:
Sender1 & Receiver1 are in webservice configuration for interaction with backend. This is System1 configuration.
Sender2 & Receiver2 are in file system configuration for interaction with backend. This is System2 configuration.
---------------------------------------------------------------------

------ORACLE Data Insert scripts---------
set define on;
def genericpassword = "'pwd'";
---WS_SYS1 properties
def wssys1_name = "'DEV_ARUN_WSSYS1'";
def wssys1_pluginuser = "'Dev_Arun_WSSys1_PluginUser'";
def wssys1_backenduser = "'Dev_Arun_WSSys1_backenduser'";
def wssys1_backenduserPassword = "&genericpassword";
def wssys1_filerepositoryservice_url="'http://localhost:10006/wiremock/mockFileRepository'";
def wssys1_inboxnotificationservice_url="'http://localhost:10006/wiremock/mockInboxNotification'";
---FS_SYS1 properties
def fssys1_name = "'DEV_ARUN_FSSYS1'";
def fssys1_pluginuser = "'Dev_Arun_FSSys1_PluginUser'";
--push parties
def push_sender1 = "'DEV_ARUN_PUSH_SENDER1'";
def push_sender2 = "'DEV_ARUN_PUSH_SENDER2'";
def push_receiver1 = "'DEV_ARUN_PUSH_RECEIVER1'";
def push_receiver2 = "'DEV_ARUN_PUSH_RECEIVER2'";
--pull parties
def pull_sender1 = "'DEV_ARUN_PULL_SENDER1'";
def pull_sender2 = "'DEV_ARUN_PULL_SENDER2'";
def pull_receiver1 = "'DEV_ARUN_PULL_RECEIVER1'";
def pull_receiver2 = "'DEV_ARUN_PULL_RECEIVER2'";

------eTrustEx Node plugin---------

insert into ETX_USER (USR_ID, USR_NAME, USR_PASSWORD, USR_NODE_SYS) values ( (select nvl(max(USR_ID)+1,1) from ETX_USER), &push_sender1  , &genericpassword, 0);
insert into ETX_USER (USR_ID, USR_NAME, USR_PASSWORD, USR_NODE_SYS) values ( (select nvl(max(USR_ID)+1,1) from ETX_USER), &push_sender2  , &genericpassword, 0);
insert into ETX_USER (USR_ID, USR_NAME, USR_PASSWORD, USR_NODE_SYS) values ( (select nvl(max(USR_ID)+1,1) from ETX_USER), &push_receiver1, &genericpassword, 0);
insert into ETX_USER (USR_ID, USR_NAME, USR_PASSWORD, USR_NODE_SYS) values ( (select nvl(max(USR_ID)+1,1) from ETX_USER), &push_receiver2, &genericpassword, 0);
insert into ETX_USER (USR_ID, USR_NAME, USR_PASSWORD, USR_NODE_SYS) values ( (select nvl(max(USR_ID)+1,1) from ETX_USER), &pull_sender1  , &genericpassword, 0);
insert into ETX_USER (USR_ID, USR_NAME, USR_PASSWORD, USR_NODE_SYS) values ( (select nvl(max(USR_ID)+1,1) from ETX_USER), &pull_sender2  , &genericpassword, 0);
insert into ETX_USER (USR_ID, USR_NAME, USR_PASSWORD, USR_NODE_SYS) values ( (select nvl(max(USR_ID)+1,1) from ETX_USER), &pull_receiver1, &genericpassword, 0);
insert into ETX_USER (USR_ID, USR_NAME, USR_PASSWORD, USR_NODE_SYS) values ( (select nvl(max(USR_ID)+1,1) from ETX_USER), &pull_receiver2, &genericpassword, 0);

insert into ETX_PARTY (PAR_ID, PAR_UUID, USR_ID, PAR_REQUIRES_WRAPPERS) values ((select nvl(max(PAR_ID)+1,1) from ETX_PARTY), &push_sender1  , (select USR_ID from ETX_USER where USR_NAME=&push_sender1  ), 1);
insert into ETX_PARTY (PAR_ID, PAR_UUID, USR_ID, PAR_REQUIRES_WRAPPERS) values ((select nvl(max(PAR_ID)+1,1) from ETX_PARTY), &push_sender2  , (select USR_ID from ETX_USER where USR_NAME=&push_sender2  ), 1);
insert into ETX_PARTY (PAR_ID, PAR_UUID, USR_ID, PAR_REQUIRES_WRAPPERS) values ((select nvl(max(PAR_ID)+1,1) from ETX_PARTY), &push_receiver1, (select USR_ID from ETX_USER where USR_NAME=&push_receiver1), 1);
insert into ETX_PARTY (PAR_ID, PAR_UUID, USR_ID, PAR_REQUIRES_WRAPPERS) values ((select nvl(max(PAR_ID)+1,1) from ETX_PARTY), &push_receiver2, (select USR_ID from ETX_USER where USR_NAME=&push_receiver2), 1);
insert into ETX_PARTY (PAR_ID, PAR_UUID, USR_ID, PAR_REQUIRES_WRAPPERS) values ((select nvl(max(PAR_ID)+1,1) from ETX_PARTY), &pull_sender1  , (select USR_ID from ETX_USER where USR_NAME=&pull_sender1  ), 1);
insert into ETX_PARTY (PAR_ID, PAR_UUID, USR_ID, PAR_REQUIRES_WRAPPERS) values ((select nvl(max(PAR_ID)+1,1) from ETX_PARTY), &pull_sender2  , (select USR_ID from ETX_USER where USR_NAME=&pull_sender2  ), 1);
insert into ETX_PARTY (PAR_ID, PAR_UUID, USR_ID, PAR_REQUIRES_WRAPPERS) values ((select nvl(max(PAR_ID)+1,1) from ETX_PARTY), &pull_receiver1, (select USR_ID from ETX_USER where USR_NAME=&pull_receiver1), 1);
insert into ETX_PARTY (PAR_ID, PAR_UUID, USR_ID, PAR_REQUIRES_WRAPPERS) values ((select nvl(max(PAR_ID)+1,1) from ETX_PARTY), &pull_receiver2, (select USR_ID from ETX_USER where USR_NAME=&pull_receiver2), 1);
------eTrustEx Node plugin---------
------eTrustEx Backend plugin---------

---WS_SYS1
insert into ETX_ADT_USER (USR_ID, USR_NAME, USR_PASSWORD) values ( (select nvl(max(USR_ID)+1,1) from ETX_ADT_USER), &wssys1_backenduser, &wssys1_backenduserPassword);
insert into ETX_ADT_SYSTEM (SYS_ID, SYS_NAME, SYS_BACKEND_USR_ID, USERNAME) values ( (select nvl(max(SYS_ID)+1,1) from ETX_ADT_SYSTEM), &wssys1_name, (select min(USR_ID) from ETX_ADT_USER where USR_NAME = &wssys1_backenduser), &wssys1_pluginuser);
insert into ETX_ADT_SYSTEM_CONFIG (SCG_ID, SYS_ID, SCG_PROPERTY_NAME, SCG_PROPERTY_VALUE) values ((select nvl(max(SCG_ID)+1,1) from ETX_ADT_SYSTEM_CONFIG), (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = &wssys1_name), 'etx.backend.filerepositorylocation', 'BACKEND_FILE_REPOSITORY_SERVICE');
insert into ETX_ADT_SYSTEM_CONFIG (SCG_ID, SYS_ID, SCG_PROPERTY_NAME, SCG_PROPERTY_VALUE) values ((select nvl(max(SCG_ID)+1,1) from ETX_ADT_SYSTEM_CONFIG), (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = &wssys1_name), 'etx.backend.services.FileRepositoryService.url', &wssys1_filerepositoryservice_url);
insert into ETX_ADT_SYSTEM_CONFIG (SCG_ID, SYS_ID, SCG_PROPERTY_NAME, SCG_PROPERTY_VALUE) values ((select nvl(max(SCG_ID)+1,1) from ETX_ADT_SYSTEM_CONFIG), (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = &wssys1_name), 'etx.backend.notification.type', 'WEB_SERVICE');
insert into ETX_ADT_SYSTEM_CONFIG (SCG_ID, SYS_ID, SCG_PROPERTY_NAME, SCG_PROPERTY_VALUE) values ((select nvl(max(SCG_ID)+1,1) from ETX_ADT_SYSTEM_CONFIG), (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = &wssys1_name), 'etx.backend.services.InboxNotificationService.url', &wssys1_inboxnotificationservice_url);
insert into ETX_ADT_SYSTEM_CONFIG (SCG_ID, SYS_ID, SCG_PROPERTY_NAME, SCG_PROPERTY_VALUE) values ((select nvl(max(SCG_ID)+1,1) from ETX_ADT_SYSTEM_CONFIG), (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = &wssys1_name), 'etx.backend.download.attachments', 'true');
--FS_SYS1
insert into ETX_ADT_SYSTEM (SYS_ID, SYS_NAME, SYS_BACKEND_USR_ID, USERNAME) values ( (select nvl(max(SYS_ID)+1,1) from ETX_ADT_SYSTEM), &fssys1_name, null, &fssys1_pluginuser);
insert into ETX_ADT_SYSTEM_CONFIG (SCG_ID, SYS_ID, SCG_PROPERTY_NAME, SCG_PROPERTY_VALUE) values ((select nvl(max(SCG_ID)+1,1) from ETX_ADT_SYSTEM_CONFIG), (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = &fssys1_name), 'etx.backend.filerepositorylocation', 'LOCAL_SHARED_FILE_SYSTEM');
insert into ETX_ADT_SYSTEM_CONFIG (SCG_ID, SYS_ID, SCG_PROPERTY_NAME, SCG_PROPERTY_VALUE) values ((select nvl(max(SCG_ID)+1,1) from ETX_ADT_SYSTEM_CONFIG), (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = &fssys1_name), 'etx.backend.notification.type', 'WITNESS_FILE');
insert into ETX_ADT_SYSTEM_CONFIG (SCG_ID, SYS_ID, SCG_PROPERTY_NAME, SCG_PROPERTY_VALUE) values ((select nvl(max(SCG_ID)+1,1) from ETX_ADT_SYSTEM_CONFIG), (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = &fssys1_name), 'etx.backend.download.attachments', 'true');
--push parties
insert into ETX_ADT_PARTY (PAR_ID, PAR_UUID, SYS_ID) values ((select nvl(max(PAR_ID)+1,1) from ETX_ADT_PARTY), &push_sender1, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = &wssys1_name));
insert into ETX_ADT_PARTY (PAR_ID, PAR_UUID, SYS_ID) values ((select nvl(max(PAR_ID)+1,1) from ETX_ADT_PARTY), &push_sender2, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = &fssys1_name));
insert into ETX_ADT_PARTY (PAR_ID, PAR_UUID, SYS_ID) values ((select nvl(max(PAR_ID)+1,1) from ETX_ADT_PARTY), &push_receiver1, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = &wssys1_name));
insert into ETX_ADT_PARTY (PAR_ID, PAR_UUID, SYS_ID) values ((select nvl(max(PAR_ID)+1,1) from ETX_ADT_PARTY), &push_receiver2, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = &fssys1_name));
--pull parties
insert into ETX_ADT_PARTY (PAR_ID, PAR_UUID, SYS_ID) values ((select nvl(max(PAR_ID)+1,1) from ETX_ADT_PARTY), &pull_sender1, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = &wssys1_name));
insert into ETX_ADT_PARTY (PAR_ID, PAR_UUID, SYS_ID) values ((select nvl(max(PAR_ID)+1,1) from ETX_ADT_PARTY), &pull_sender2, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = &fssys1_name));
insert into ETX_ADT_PARTY (PAR_ID, PAR_UUID, SYS_ID) values ((select nvl(max(PAR_ID)+1,1) from ETX_ADT_PARTY), &pull_receiver1, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = &wssys1_name));
insert into ETX_ADT_PARTY (PAR_ID, PAR_UUID, SYS_ID) values ((select nvl(max(PAR_ID)+1,1) from ETX_ADT_PARTY), &pull_receiver2, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = &fssys1_name));

------eTrustEx Backend plugin---------
------ORACLE Data Insert scripts---------


============================================================================================================================================================================================================================================================================================================================================================================

eTrustEx Plugins Arun Test data setup: MYSQL
=======================================

Notes:
Sender1 & Receiver1 are in webservice configuration for interaction with backend. This is System1 configuration.
Sender2 & Receiver2 are in file system configuration for interaction with backend. This is System2 configuration.
---------------------------------------------------------------------

######MYSQL Data Insert scripts######

set @genericpassword = 'Test@Plugins#987';
###WS_SYS1 properties
set @wssys1_name = 'DEV_ARUN_WSSYS1';
set @wssys1_pluginuser = 'Dev_Arun_WSSys1_PluginUser';
set @wssys1_backenduser = 'Dev_Arun_WSSys1_backenduser';
set @wssys1_backenduserPassword = @genericpassword;
set @wssys1_filerepositoryservice_url='http://localhost:10006/wiremock/mockFileRepository';
set @wssys1_inboxnotificationservice_url='http://localhost:10006/wiremock/mockInboxNotification';
###FS_SYS1 properties
set @fssys1_name = 'DEV_ARUN_FSSYS1';
set @fssys1_pluginuser = 'Dev_Arun_FSSys1_PluginUser';
###push parties
set @push_sender1 = 'DEV_ARUN_PUSH_SENDER1';
set @push_sender2 = 'DEV_ARUN_PUSH_SENDER2';
set @push_receiver1 = 'DEV_ARUN_PUSH_RECEIVER1';
set @push_receiver2 = 'DEV_ARUN_PUSH_RECEIVER2';
###pull parties
set @pull_sender1 = 'DEV_ARUN_PULL_SENDER1';
set @pull_sender2 = 'DEV_ARUN_PULL_SENDER2';
set @pull_receiver1 = 'DEV_ARUN_PULL_RECEIVER1';
set @pull_receiver2 = 'DEV_ARUN_PULL_RECEIVER2';


######eTrustEx Backend plugin######
###WS_SYS1
set @id=(select ifnull(max(USR_ID)+1,1) from ETX_ADT_USER);
insert into ETX_ADT_USER(USR_ID, USR_NAME, USR_PASSWORD) values ( @id, @wssys1_backenduser, @wssys1_backenduserPassword );
set @id=(select ifnull(max(sys_id)+1,1) from ETX_ADT_SYSTEM);
insert into ETX_ADT_SYSTEM(SYS_ID, SYS_NAME, SYS_BACKEND_USR_ID, USERNAME) values ( @id, @wssys1_name, (select USR_ID from ETX_ADT_USER where USR_NAME=@wssys1_backenduser), @wssys1_pluginuser);
set @id=(select ifnull(max(SCG_ID+1),1) from ETX_ADT_SYSTEM_CONFIG);
insert into ETX_ADT_SYSTEM_CONFIG(SCG_ID, SYS_ID, SCG_PROPERTY_NAME, SCG_PROPERTY_VALUE) values ( @id, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = @wssys1_name), 'etx.backend.filerepositorylocation', 'BACKEND_FILE_REPOSITORY_SERVICE' );
set @id=(select ifnull(max(SCG_ID+1),1) from ETX_ADT_SYSTEM_CONFIG);
insert into ETX_ADT_SYSTEM_CONFIG(SCG_ID, SYS_ID, SCG_PROPERTY_NAME, SCG_PROPERTY_VALUE) values ( @id, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = @wssys1_name), 'etx.backend.notification.type', 'WEB_SERVICE' );
set @id=(select ifnull(max(SCG_ID+1),1) from ETX_ADT_SYSTEM_CONFIG);
insert into ETX_ADT_SYSTEM_CONFIG(SCG_ID, SYS_ID, SCG_PROPERTY_NAME, SCG_PROPERTY_VALUE) values ( @id, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = @wssys1_name), 'etx.backend.services.FileRepositoryService.url', @wssys1_filerepositoryservice_url );
set @id=(select ifnull(max(SCG_ID+1),1) from ETX_ADT_SYSTEM_CONFIG);
insert into ETX_ADT_SYSTEM_CONFIG(SCG_ID, SYS_ID, SCG_PROPERTY_NAME, SCG_PROPERTY_VALUE) values ( @id, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = @wssys1_name), 'etx.backend.services.InboxNotificationService.url', @wssys1_inboxnotificationservice_url );
set @id=(select ifnull(max(SCG_ID+1),1) from ETX_ADT_SYSTEM_CONFIG);
insert into ETX_ADT_SYSTEM_CONFIG(SCG_ID, SYS_ID, SCG_PROPERTY_NAME, SCG_PROPERTY_VALUE) values ( @id, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = @wssys1_name), 'etx.backend.download.attachments', 'true' );
###FS_SYS1
set @id=(select ifnull(max(sys_id)+1,1) from ETX_ADT_SYSTEM);
insert into ETX_ADT_SYSTEM(SYS_ID, SYS_NAME, SYS_BACKEND_USR_ID, USERNAME) values ( @id, @fssys1_name, null, @fssys1_pluginuser);
set @id=(select ifnull(max(SCG_ID+1),1) from ETX_ADT_SYSTEM_CONFIG);
insert into ETX_ADT_SYSTEM_CONFIG(SCG_ID, SYS_ID, SCG_PROPERTY_NAME, SCG_PROPERTY_VALUE) values ( @id, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = @fssys1_name), 'etx.backend.filerepositorylocation', 'LOCAL_SHARED_FILE_SYSTEM' );
set @id=(select ifnull(max(SCG_ID+1),1) from ETX_ADT_SYSTEM_CONFIG);
insert into ETX_ADT_SYSTEM_CONFIG(SCG_ID, SYS_ID, SCG_PROPERTY_NAME, SCG_PROPERTY_VALUE) values ( @id, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = @fssys1_name), 'etx.backend.notification.type', 'WITNESS_FILE' );
set @id=(select ifnull(max(SCG_ID+1),1) from ETX_ADT_SYSTEM_CONFIG);
insert into ETX_ADT_SYSTEM_CONFIG(SCG_ID, SYS_ID, SCG_PROPERTY_NAME, SCG_PROPERTY_VALUE) values ( @id, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = @fssys1_name), 'etx.backend.download.attachments', 'true' );
###push parties
set @id=(select ifnull(max(PAR_ID+1),1) from ETX_ADT_PARTY);
insert into ETX_ADT_PARTY (PAR_ID, PAR_UUID, SYS_ID) values (@id, @push_sender1, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = @wssys1_name));
set @id=(select ifnull(max(PAR_ID+1),1) from ETX_ADT_PARTY);
insert into ETX_ADT_PARTY (PAR_ID, PAR_UUID, SYS_ID) values (@id, @push_sender2, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = @fssys1_name));
set @id=(select ifnull(max(PAR_ID+1),1) from ETX_ADT_PARTY);
insert into ETX_ADT_PARTY (PAR_ID, PAR_UUID, SYS_ID) values (@id, @push_receiver1, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = @wssys1_name));
set @id=(select ifnull(max(PAR_ID+1),1) from ETX_ADT_PARTY);
insert into ETX_ADT_PARTY (PAR_ID, PAR_UUID, SYS_ID) values (@id, @push_receiver2, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = @fssys1_name));
###pull parties
set @id=(select ifnull(max(PAR_ID+1),1) from ETX_ADT_PARTY);
insert into ETX_ADT_PARTY (PAR_ID, PAR_UUID, SYS_ID) values (@id, @pull_sender1, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = @wssys1_name));
set @id=(select ifnull(max(PAR_ID+1),1) from ETX_ADT_PARTY);
insert into ETX_ADT_PARTY (PAR_ID, PAR_UUID, SYS_ID) values (@id, @pull_sender2, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = @fssys1_name));
set @id=(select ifnull(max(PAR_ID+1),1) from ETX_ADT_PARTY);
insert into ETX_ADT_PARTY (PAR_ID, PAR_UUID, SYS_ID) values (@id, @pull_receiver1, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = @wssys1_name));
set @id=(select ifnull(max(PAR_ID+1),1) from ETX_ADT_PARTY);
insert into ETX_ADT_PARTY (PAR_ID, PAR_UUID, SYS_ID) values (@id, @pull_receiver2, (select SYS_ID from ETX_ADT_SYSTEM where SYS_NAME = @fssys1_name));

######eTrustEx Backend plugin######
######MYSQL Data Insert scripts######
------load test clear tables - Node plugin
alter table "EC_ETX_NODEPLUGIN_ADMIN"."ETX_ATTACHMENT" disable constraint C_ETX_ATTACHMENT_ERR_ID_FK;
alter table "EC_ETX_NODEPLUGIN_ADMIN"."ETX_ATTACHMENT" disable constraint C_ETX_ATT_MSG_ID_FK;
alter table "EC_ETX_NODEPLUGIN_ADMIN"."ETX_MESSAGE" disable constraint C_ETX_MESSAGE_ERR_ID_FK;
truncate TABLE "EC_ETX_NODEPLUGIN_ADMIN"."ETX_ATTACHMENT"  drop storage;
truncate TABLE "EC_ETX_NODEPLUGIN_ADMIN"."ETX_MESSAGE"  drop storage;
truncate TABLE "EC_ETX_NODEPLUGIN_ADMIN"."ETX_ERROR"  drop storage;
alter table "EC_ETX_NODEPLUGIN_ADMIN"."ETX_ATTACHMENT" enable constraint C_ETX_ATTACHMENT_ERR_ID_FK;
alter table "EC_ETX_NODEPLUGIN_ADMIN"."ETX_ATTACHMENT" enable constraint C_ETX_ATT_MSG_ID_FK;
alter table "EC_ETX_NODEPLUGIN_ADMIN"."ETX_MESSAGE" enable constraint C_ETX_MESSAGE_ERR_ID_FK;

alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_SIGNAL_MESSAGE" disable constraint FK_C08LJJWI4P9DX1RJTCDBE1SFU;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_SIGNAL_MESSAGE" disable constraint FK_M6UU2Y6G9BUET3O3K1N4QXWEC;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_USER_MESSAGE" disable constraint FK_BE4XC1069QYW2KLHVM3XMG26S;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_RECEIPT_DATA" disable constraint FK_8NCCAE214MVS1KH5SGCJ3Y5OY;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_RAWENVELOPE_LOG" disable constraint FK_SIGNALMSG_ID_FK_RAWENV_ID;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_RAWENVELOPE_LOG" disable constraint FK_USERMSG_ID_FK_RAWENV_ID;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_PROPERTY" disable constraint FK_2H31P0A2Y6WEG2GRH70C9CYVA;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_PROPERTY" disable constraint FK_CWYLVG6ERNWBA61MO3YAGMKG8;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_PART_INFO" disable constraint FK_TQ6LBN3MP0VSFC6QQU7WXY54G;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_MESSAGING" disable constraint FK_433RGM5A446T59Q6JKB885L3X;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_MESSAGING" disable constraint FK_53GP9SMTEMQCUIO8O9T10NARM;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_ERROR" disable constraint FK_PRRCMI4GRM4TXHFSFNFHGQ1K1;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_PARTY_ID" disable constraint FK_FKFTD5ORW2ETIU4GHMN3MHX9I;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_PARTY_ID" disable constraint FK_YLUB8LPTGVSCH02MJ71EUUIL;
truncate table "EC_ETX_NODEPLUGIN_ADMIN"."TB_ERROR_LOG" drop storage;
truncate TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_MESSAGE_LOG" drop storage;
truncate TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_MESSAGING" drop storage;
truncate TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_MESSAGE_INFO" drop storage;
truncate TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_PART_INFO" drop storage;
truncate TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_PROPERTY" drop storage;
truncate TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_RAWENVELOPE_LOG" drop storage;
truncate TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_RECEIPT" drop storage;
truncate TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_RECEIPT_DATA" drop storage;
truncate TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_SEND_ATTEMPT" drop storage;
truncate TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_SIGNAL_MESSAGE" drop storage;
truncate TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_USER_MESSAGE" drop storage;
truncate TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_PARTY_ID" drop storage;
truncate TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_MESSAGE_UI" drop storage;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_SIGNAL_MESSAGE" enable constraint FK_C08LJJWI4P9DX1RJTCDBE1SFU;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_SIGNAL_MESSAGE" enable constraint FK_M6UU2Y6G9BUET3O3K1N4QXWEC;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_USER_MESSAGE" enable constraint FK_BE4XC1069QYW2KLHVM3XMG26S;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_RECEIPT_DATA" enable constraint FK_8NCCAE214MVS1KH5SGCJ3Y5OY;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_RAWENVELOPE_LOG" enable constraint FK_SIGNALMSG_ID_FK_RAWENV_ID;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_RAWENVELOPE_LOG" enable constraint FK_USERMSG_ID_FK_RAWENV_ID;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_PROPERTY" enable constraint FK_2H31P0A2Y6WEG2GRH70C9CYVA;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_PROPERTY" enable constraint FK_CWYLVG6ERNWBA61MO3YAGMKG8;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_PART_INFO" enable constraint FK_TQ6LBN3MP0VSFC6QQU7WXY54G;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_MESSAGING" enable constraint FK_433RGM5A446T59Q6JKB885L3X;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_MESSAGING" enable constraint FK_53GP9SMTEMQCUIO8O9T10NARM;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_ERROR" enable constraint FK_PRRCMI4GRM4TXHFSFNFHGQ1K1;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_PARTY_ID" enable constraint FK_FKFTD5ORW2ETIU4GHMN3MHX9I;
alter TABLE "EC_ETX_NODEPLUGIN_ADMIN"."TB_PARTY_ID" enable constraint FK_YLUB8LPTGVSCH02MJ71EUUIL;

---queries for verification - Node Plugin
select MSG_TYPE, MSG_DIRECTION, MSG_STATE, count(1) from "ETX_NODE_PLUGIN".etx_message /*where MSG_ID > 3616*/ group by MSG_TYPE, MSG_DIRECTION, msg_state;
SELECT ATT_DIRECTION, ATT_STATE, count(1) FROM "ETX_NODE_PLUGIN".ETX_ATTACHMENT /*where ATT_ID > 6345*/ group by ATT_DIRECTION, ATT_STATE;



---Load test clear tables - Backend plugin - ORACLE-----
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."ETX_ADT_ATTACHMENT" disable constraint C_ETX_ADT_ATT_ERR_ID_FK;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."ETX_ADT_ATTACHMENT" disable constraint C_ETX_ADT_ATT_MSG_ID_FK;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."ETX_ADT_MESSAGE" disable constraint C_ETX_ADT_MSG_ERR_ID_FK;
truncate TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."ETX_ADT_ATTACHMENT"  drop storage;
truncate TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."ETX_ADT_MESSAGE"  drop storage;
truncate TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."ETX_ADT_ERROR"  drop storage;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."ETX_ADT_ATTACHMENT" enable constraint C_ETX_ADT_ATT_ERR_ID_FK;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."ETX_ADT_ATTACHMENT" enable constraint C_ETX_ADT_ATT_MSG_ID_FK;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."ETX_ADT_MESSAGE" enable constraint C_ETX_ADT_MSG_ERR_ID_FK;

alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_SIGNAL_MESSAGE" disable constraint FK_C08LJJWI4P9DX1RJTCDBE1SFU;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_SIGNAL_MESSAGE" disable constraint FK_M6UU2Y6G9BUET3O3K1N4QXWEC;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_USER_MESSAGE" disable constraint FK_BE4XC1069QYW2KLHVM3XMG26S;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_RECEIPT_DATA" disable constraint FK_8NCCAE214MVS1KH5SGCJ3Y5OY;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_RAWENVELOPE_LOG" disable constraint FK_SIGNALMSG_ID_FK_RAWENV_ID;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_RAWENVELOPE_LOG" disable constraint FK_USERMSG_ID_FK_RAWENV_ID;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_PROPERTY" disable constraint FK_2H31P0A2Y6WEG2GRH70C9CYVA;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_PROPERTY" disable constraint FK_CWYLVG6ERNWBA61MO3YAGMKG8;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_PART_INFO" disable constraint FK_TQ6LBN3MP0VSFC6QQU7WXY54G;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_MESSAGING" disable constraint FK_433RGM5A446T59Q6JKB885L3X;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_MESSAGING" disable constraint FK_53GP9SMTEMQCUIO8O9T10NARM;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_ERROR" disable constraint FK_PRRCMI4GRM4TXHFSFNFHGQ1K1;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_PARTY_ID" disable constraint FK_FKFTD5ORW2ETIU4GHMN3MHX9I;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_PARTY_ID" disable constraint FK_YLUB8LPTGVSCH02MJ71EUUIL;
truncate table "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_ERROR_LOG" drop storage;
truncate TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_MESSAGE_LOG" drop storage;
truncate TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_MESSAGING" drop storage;
truncate TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_MESSAGE_INFO" drop storage;
truncate TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_PART_INFO" drop storage;
truncate TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_PROPERTY" drop storage;
truncate TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_RAWENVELOPE_LOG" drop storage;
truncate TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_RECEIPT" drop storage;
truncate TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_RECEIPT_DATA" drop storage;
truncate TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_SEND_ATTEMPT" drop storage;
truncate TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_SIGNAL_MESSAGE" drop storage;
truncate TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_USER_MESSAGE" drop storage;
truncate TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_PARTY_ID" drop storage;
truncate table "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_MESSAGE_UI" drop storage;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_SIGNAL_MESSAGE" enable constraint FK_C08LJJWI4P9DX1RJTCDBE1SFU;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_SIGNAL_MESSAGE" enable constraint FK_M6UU2Y6G9BUET3O3K1N4QXWEC;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_USER_MESSAGE" enable constraint FK_BE4XC1069QYW2KLHVM3XMG26S;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_RECEIPT_DATA" enable constraint FK_8NCCAE214MVS1KH5SGCJ3Y5OY;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_RAWENVELOPE_LOG" enable constraint FK_SIGNALMSG_ID_FK_RAWENV_ID;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_RAWENVELOPE_LOG" enable constraint FK_USERMSG_ID_FK_RAWENV_ID;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_PROPERTY" enable constraint FK_2H31P0A2Y6WEG2GRH70C9CYVA;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_PROPERTY" enable constraint FK_CWYLVG6ERNWBA61MO3YAGMKG8;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_PART_INFO" enable constraint FK_TQ6LBN3MP0VSFC6QQU7WXY54G;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_MESSAGING" enable constraint FK_433RGM5A446T59Q6JKB885L3X;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_MESSAGING" enable constraint FK_53GP9SMTEMQCUIO8O9T10NARM;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_ERROR" enable constraint FK_PRRCMI4GRM4TXHFSFNFHGQ1K1;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_PARTY_ID" enable constraint FK_FKFTD5ORW2ETIU4GHMN3MHX9I;
alter TABLE "ARUN_PUSH_ETX_BEPLUGIN_ADMIN"."TB_PARTY_ID" enable constraint FK_YLUB8LPTGVSCH02MJ71EUUIL;


---Load test clear tables - Backend plugin - MySQL-----
SET FOREIGN_KEY_CHECKS=0;
truncate TABLE ETX_ADT_ATTACHMENT;
truncate TABLE ETX_ADT_MESSAGE;
truncate TABLE ETX_ADT_ERROR;

truncate table TB_ERROR_LOG;
truncate TABLE TB_MESSAGE_LOG;
truncate TABLE TB_MESSAGING;
truncate TABLE TB_MESSAGE_INFO;
truncate TABLE TB_PART_INFO;
truncate TABLE TB_PROPERTY;
truncate TABLE TB_RAWENVELOPE_LOG;
truncate TABLE TB_RECEIPT;
truncate TABLE TB_RECEIPT_DATA;
truncate TABLE TB_SEND_ATTEMPT;
truncate TABLE TB_SIGNAL_MESSAGE;
truncate TABLE TB_USER_MESSAGE;
truncate TABLE TB_PARTY_ID;
truncate table TB_MESSAGE_UI;
truncate table tb_messaging_lock;
SET FOREIGN_KEY_CHECKS=1;
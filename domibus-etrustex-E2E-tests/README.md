# README

## Tests End To End

### A - Configuration

#### 1 - Logging

##### a - PROD
    <property name="encoderPattern" value=%d{ISO8601} [%marker] [%X{d_user}] [%X{d_domain}] [%X{d_messageId}] [%X{d_etxConversationId}] [%X{d_etxMessageUuid}] [%X{d_etxAttUuid}] [%thread] %5p %c{1}:%L - %m%n" scope="global"/>

##### b - DEV
	<property name="encoderPatternColor" value="%d{HH:mm:ss,SSS} %boldGreen([%7.-7X{d_domain}]) %boldCyan([%5.5X{d_user}] [%8.-8X{d_messageId}]) %boldWhite([%15.-15X{d_etxConversationId}] [%15.-15X{d_etxMessageUuid}] [%8.8X{d_etxAttUuid}]) %highlight(%5p) %50.-50(.\(%F:%L\\)) - %m%n" scope="global"/>

#### Passwords

In order to protect the passwords used in the tests, a file called 'passwords.txt' contains them and is NOT to be shared
or committed in the repository.

Content of the file without the values:
    password.edma=
    password.egreffe.adaptor.sysuser=
    password.egreffe.plugin.sysuser=
    password.egreffe.adapter.sys.1.user=
    password.egreffe.adapter.sys.2.user=
    password.egreffe.adapter.sys.3.user=
    password.egreffe.plugin.sys.1.user=
    password.egreffe.plugin.sys.2.user=
    password.egreffe.plugin.sys.3.user=
    password.backend.plugin.jdbc=
    password.a1.domibus.admin2=
    password.an.domibus.admin=
    password.node.plugin.jdbc=
    password.etrustex.node.etrustexread=
    password.adapter.jdbc.etrustexadapter=
    password.ACC_EDELAS4_DECIDE_SYS1_USER=
    
    
    
###Executing specific executions of maven-soapui-pro-plugin
In order to execute only specific executions of the maven soap-ui-pro plugin, the following command structure can be used.
mvn pluginId:mojoName@executionId
e.g: mvn soapui-pro:test@SanityTestSuite


###Jars needed for local testing
For Oracle - ojdbc6.jar
For MySQL - mysql-connector-java-8.xxx.jar
Note: Automation with maven-soapui-pro-plugin is done only for Oracle database
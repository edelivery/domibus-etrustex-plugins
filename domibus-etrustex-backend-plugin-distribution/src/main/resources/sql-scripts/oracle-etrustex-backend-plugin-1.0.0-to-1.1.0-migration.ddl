-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: src/main/resources/config/liquibase/changelog/changelog-1.1.0-delta.xml
-- Ran at: 24/01/20 18:29
-- Against: null@offline:oracle?version=11.2.0&changeLogFile=E:\workspace\domibus-plugins\domibus-etrustex-backend-plugin\target/liquibase/changelog-1.1.0-delta.oracle
-- Liquibase version: 3.5.4
-- *********************************************************************

SET DEFINE OFF;

-- Changeset src/main/resources/config/liquibase/changelog/changelog-1.1.0-delta.xml::EDELIVERY-2009_create_hibernate_sequence::gautifr
set serveroutput on;
DECLARE
    l_new_seq INTEGER;
    rcount INTEGER := 0;
    new_seq_no INTEGER;
    sql_stmt  VARCHAR2(3000);
BEGIN
    select max(maxnum) INTO l_new_seq from(
            select (max(last_number) + 1) maxnum  from user_sequences
            WHERE sequence_name IN (
                'ETX_ADT_ATT_SQ',
                'ETX_ADT_CONFIG_SQ',
                'ETX_ADT_ERR_SQ',
                'ETX_ADT_MSG_SQ',
                'ETX_ADT_PAR_SQ',
                'ETX_ADT_SCG_SQ',
                'ETX_ADT_SYS_SQ',
                'ETX_ADT_USR_SQ'
            )
            union
            select (max(msg_id) + 1) maxnum from ETX_ADT_MESSAGE
            union
            select (max(att_id) + 1) maxnum from ETX_ADT_ATTACHMENT
            union
            select (max(err_id) +1) maxnum from ETX_ADT_ERROR
    );

    select count(1) into rcount from user_sequences where upper(sequence_name) = 'HIBERNATE_SEQUENCE';
    if rcount > 0 then
        sql_stmt := 'alter sequence HIBERNATE_SEQUENCE increment by ' || l_new_seq;
        dbms_output.put_line(sql_stmt);
        execute immediate sql_stmt;

        sql_stmt := 'select HIBERNATE_SEQUENCE.nextval from dual';
        dbms_output.put_line(sql_stmt);
        execute immediate sql_stmt into new_seq_no;
        dbms_output.put_line('new_seq_no:'||new_seq_no);

        sql_stmt := 'alter sequence HIBERNATE_SEQUENCE increment by 1';
        dbms_output.put_line(sql_stmt);
        execute immediate sql_stmt;
    else
        sql_stmt := 'Create sequence HIBERNATE_SEQUENCE start with ' || l_new_seq || ' increment by 1 MINVALUE 1 MAXVALUE 9999999999999999999999999999 CACHE 20 NOORDER';
        dbms_output.put_line(sql_stmt);
        execute immediate sql_stmt;
    end if;
END;
/

-- Changeset src/main/resources/config/liquibase/changelog/changelog-1.1.0-delta.xml::EDELIVERY-2009_delete_sequences::gautifr
set serveroutput on;
declare
    sql_stmt  VARCHAR2(3000);
begin
    --------------------Remove unnecessary sequences start-----------------------
    for cur_rec in (select sequence_name from user_sequences where upper(sequence_name) in ('ETX_ADT_ATT_SQ', 'ETX_ADT_CONFIG_SQ', 'ETX_ADT_ERR_SQ', 'ETX_ADT_KSE_SQ', 'ETX_ADT_MSG_SQ', 'ETX_ADT_MSJ_SQ', 'ETX_ADT_PAR_SQ', 'ETX_ADT_SCG_SQ', 'ETX_ADT_SKS_SQ', 'ETX_ADT_SYS_SQ', 'ETX_ADT_USR_SQ')) loop
        sql_stmt := 'drop sequence ' || ' "' || cur_rec.sequence_name || '" ';
        dbms_output.put_line(sql_stmt);
        execute immediate  sql_stmt;
    end loop;
--------------------Remove unnecessary sequences end-----------------------
end;
/

-- Changeset src/main/resources/config/liquibase/changelog/changelog-1.1.0-delta.xml::EDELIVERY-3557_remove_redundant_data::gautifr
DELETE FROM ETX_ADT_CONFIG WHERE ACG_PROPERTY_NAME in (
                'etx.node.services.DocumentBundleService.url',
                'etx.node.services.DocumentWrapperService.url',
                'etx.node.services.InboxRequestService.url',
                'etx.node.services.RetrieveRequestService.url',
                'etx.node.services.ApplicationResponseService.url',
                'etx.node.services.RetrieveInterchangeAgreementService.url',
                'etx.node.security.p2p.enabled'
                );

DELETE FROM ETX_ADT_SYSTEM_CONFIG WHERE SCG_PROPERTY_NAME = 'etx.backend.soap.message.logging';

ALTER TABLE ETX_ADT_PARTY DROP CONSTRAINT C_ETX_ADT_USER_USR_ID_FK;

ALTER TABLE ETX_ADT_PARTY DROP COLUMN USR_ID;

-- Changeset src/main/resources/config/liquibase/changelog/changelog-1.1.0-delta.xml::EDELIVERY-3557_create_ica_table::gautifr
CREATE TABLE ETX_ADT_ICA (ICA_ID NUMBER(20, 0) NOT NULL, ICA_SENDER_PAR_ID NUMBER(20, 0) NOT NULL, ICA_RECEIVER_PAR_ID NUMBER(20, 0) NOT NULL, ICA_XML VARCHAR2(4000 CHAR), ICA_CREATED_ON date DEFAULT SYSDATE NOT NULL, ICA_UPDATED_ON date, CONSTRAINT PK_ETX_ADT_ICA PRIMARY KEY (ICA_ID));

ALTER TABLE ETX_ADT_ICA ADD CONSTRAINT C_ETX_ADT_ICA_SENDER_PAR_ID_FK FOREIGN KEY (ICA_SENDER_PAR_ID) REFERENCES ETX_ADT_PARTY (PAR_ID);

ALTER TABLE ETX_ADT_ICA ADD CONSTRAINT C_ETX_ADT_ICA_RECEVR_PAR_ID_FK FOREIGN KEY (ICA_RECEIVER_PAR_ID) REFERENCES ETX_ADT_PARTY (PAR_ID);

-- Changeset src/main/resources/config/liquibase/changelog/changelog-1.1.0-delta.xml::EDELIVERY-4140_default_system_for_third_parties::venugar
set serveroutput on;
declare
    rcount number := 0;
    sql_stmt  VARCHAR2(3000) := 'INSERT INTO ETX_ADT_SYSTEM (SYS_ID, SYS_NAME) VALUES ( (SELECT CASE WHEN nvl(min(sys_id),1) > 0 THEN 0 ELSE max(sys_id)+1 END FROM ETX_ADT_SYSTEM), ''THIRD-PARTY-SYS'')';
begin
    select count(1) into rcount from ETX_ADT_SYSTEM where sys_name = 'THIRD-PARTY-SYS';
    if rcount = 0 then
        dbms_output.put_line(sql_stmt);
        execute immediate sql_stmt;
    end if;
end;
/

-- Changeset src/main/resources/config/liquibase/changelog/changelog-1.1.0-delta.xml::EDELIVERY-4272_SYSTEM_with_same_USER::gautifr
ALTER TABLE ETX_ADT_SYSTEM ADD USERNAME VARCHAR2(255 CHAR);

update ETX_ADT_SYSTEM eas set eas.USERNAME = ( select distinct eau.USR_NAME from ETX_ADT_USER eau where eau.usr_id = eas.usr_id );

ALTER TABLE ETX_ADT_SYSTEM DROP CONSTRAINT C_ETX_ADT_SYS_LOCAL_USR_FK;

DROP INDEX I_ETX_ADT_SYS_LOCAL_USR_ID_IX;

ALTER TABLE ETX_ADT_SYSTEM DROP COLUMN USR_ID;


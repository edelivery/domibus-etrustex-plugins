
set serveroutput on;
declare
  rcount number := 0;
  sql_stmt  VARCHAR2(3000);
begin
    --------------------Remove unnecessary tables start-----------------------
    for cur_rec in (select object_name, object_type from user_objects where upper(object_type) = 'TABLE' and upper(object_name) in ('ETX_ADT_MESSAGE_JOB', 'ETX_ADT_KEY_STORE_ENTRY', 'ETX_ADT_SYSTEM_KEY_STORE')) loop
        sql_stmt := 'drop table ' || ' "' || cur_rec.object_name || '" cascade constraints purge';
        dbms_output.put_line(sql_stmt);
        execute immediate  sql_stmt;
    end loop;

    select count(1) into rcount from user_objects where upper(object_type) = 'PROCEDURE' and upper(object_name) = 'ADDPARTY';
    if rcount >= 1 then
        sql_stmt := 'drop procedure ADDPARTY';
        dbms_output.put_line(sql_stmt);
        execute immediate sql_stmt ;
    end if;
    select count(1) into rcount from user_tab_columns where upper(table_name) = 'ETX_ADT_PARTY' and upper(column_name) = 'PAR_IS_RECEIVING_MESSAGES';
    if rcount >= 1 then
        sql_stmt := 'alter table ETX_ADT_PARTY drop column PAR_IS_RECEIVING_MESSAGES cascade constraints';
        dbms_output.put_line(sql_stmt);
        execute immediate sql_stmt ;
    end if;
    --------------------Remove unnecessary tables end-----------------------
    --------------------Remove Constraints start-----------------------
    for cur_rec in (select owner, constraint_name, table_name from user_constraints where upper(constraint_type) in ('R', 'U') ) loop
        sql_stmt := 'alter table "' || cur_rec.table_name || '" drop constraint "' || cur_rec.constraint_name || '"' ;
        dbms_output.put_line(sql_stmt);
        execute immediate  sql_stmt;
    end loop;

    for cur_rec in (select owner, constraint_name, table_name from user_constraints where upper(constraint_type) in ('P') ) loop
        sql_stmt := 'alter table "' || cur_rec.table_name || '" drop constraint "' || cur_rec.constraint_name || '"' ;
        dbms_output.put_line(sql_stmt);
        execute immediate  sql_stmt;
    end loop;
    --------------------Remove Constraints end-------------------------
    --------------------Remove Indexes start---------------------------
    for cur_rec in (select object_name, object_type from user_objects where upper(object_type) = 'INDEX' and object_name like 'ETX_ADT_%') loop
        sql_stmt := 'drop index ' || ' "' || cur_rec.object_name || '" ';
        dbms_output.put_line(sql_stmt);
        execute immediate  sql_stmt;
    end loop;
    --------------------Remove Indexes end---------------------------
end;
/
--------------------------------------------------------------
--  DDL To add primary keys, constraints and indexes
--------------------------------------------------------------
alter table ETX_ADT_CONFIG add constraint PK_ETX_ADT_CONFIG PRIMARY KEY (ACG_ID);
alter table ETX_ADT_USER add constraint PK_ETX_ADT_USER PRIMARY KEY (USR_ID);
alter table ETX_ADT_SYSTEM add constraint PK_ETX_ADT_SYSTEM PRIMARY KEY (SYS_ID);
alter table ETX_ADT_SYSTEM_CONFIG add constraint PK_ETX_ADT_SYSTEM_CONFIG PRIMARY KEY (SCG_ID);
alter table ETX_ADT_PARTY add constraint PK_ETX_ADT_PARTY PRIMARY KEY (PAR_ID);
alter table ETX_ADT_ERROR add constraint PK_ETX_ADT_ERROR PRIMARY KEY (ERR_ID);
alter table ETX_ADT_MESSAGE add constraint PK_ETX_ADT_MESSAGE PRIMARY KEY (MSG_ID);
alter table ETX_ADT_ATTACHMENT add CONSTRAINT PK_ETX_ADT_ATTACHMENT PRIMARY KEY (ATT_ID);

CREATE UNIQUE INDEX I_ETX_ADT_CONFIG_NAME_VALUE ON ETX_ADT_CONFIG(ACG_PROPERTY_NAME, ACG_PROPERTY_VALUE);
ALTER TABLE ETX_ADT_CONFIG ADD CONSTRAINT C_ETX_ADT_CONFIG_NAME_VALUE UNIQUE (ACG_PROPERTY_NAME, ACG_PROPERTY_VALUE);

CREATE INDEX I_ETX_ADT_SYS_BACK_USR_ID_IX ON ETX_ADT_SYSTEM(SYS_BACKEND_USR_ID);
CREATE INDEX I_ETX_ADT_SYS_LOCAL_USR_ID_IX ON ETX_ADT_SYSTEM(USR_ID);
ALTER TABLE ETX_ADT_SYSTEM ADD CONSTRAINT C_ETX_ADT_SYS_BACKEND_USR_FK FOREIGN KEY (SYS_BACKEND_USR_ID) REFERENCES ETX_ADT_USER (USR_ID);
ALTER TABLE ETX_ADT_SYSTEM ADD CONSTRAINT C_ETX_ADT_SYS_LOCAL_USR_FK FOREIGN KEY (USR_ID) REFERENCES ETX_ADT_USER (USR_ID);

CREATE UNIQUE INDEX I_ETX_ADT_SCG_SYS_ID_PROPERTY ON ETX_ADT_SYSTEM_CONFIG(SYS_ID, SCG_PROPERTY_NAME);
ALTER TABLE ETX_ADT_SYSTEM_CONFIG ADD CONSTRAINT C_ETX_ADT_SCG_SYS_ID_PROPERTY UNIQUE (SYS_ID, SCG_PROPERTY_NAME);
ALTER TABLE ETX_ADT_SYSTEM_CONFIG ADD CONSTRAINT C_ETX_ADT_SCG_SYS_ID_FK FOREIGN KEY (SYS_ID) REFERENCES ETX_ADT_SYSTEM (SYS_ID);

CREATE UNIQUE INDEX I_ETX_ADT_PARTY_PAR_UUID_UK ON ETX_ADT_PARTY(PAR_UUID);
ALTER TABLE ETX_ADT_PARTY ADD CONSTRAINT C_ETX_ADT_PARTY_PAR_UUID_UK UNIQUE (PAR_UUID);
CREATE INDEX I_ETX_ADT_PARTY_SYS_ID_IX ON ETX_ADT_PARTY(SYS_ID);
ALTER TABLE ETX_ADT_PARTY ADD CONSTRAINT C_ETX_ADT_PARTY_SYS_ID_FK FOREIGN KEY (SYS_ID) REFERENCES ETX_ADT_SYSTEM (SYS_ID);
ALTER TABLE ETX_ADT_PARTY ADD CONSTRAINT C_ETX_ADT_USER_USR_ID_FK FOREIGN KEY (USR_ID) REFERENCES ETX_ADT_USER (USR_ID);

CREATE INDEX I_ETX_ADT_MSG_ERR_ID_IX ON ETX_ADT_MESSAGE(ERR_ID);
CREATE INDEX I_ETX_ADT_MSG_NOTST_IX ON ETX_ADT_MESSAGE(MSG_NOTIFICATION_STATE);
CREATE INDEX I_ETX_ADT_MSG_REC_ID_IX ON ETX_ADT_MESSAGE(MSG_RECEIVER_PAR_ID);
CREATE INDEX I_ETX_ADT_MSG_SEN_ID_IX ON ETX_ADT_MESSAGE(MSG_SENDER_PAR_ID);
CREATE INDEX I_ETX_ADT_MSG_UUID_IX ON ETX_ADT_MESSAGE(MSG_UUID);
ALTER TABLE ETX_ADT_MESSAGE ADD CONSTRAINT C_ETX_ADT_MSG_ERR_ID_FK FOREIGN KEY (ERR_ID) REFERENCES ETX_ADT_ERROR (ERR_ID);
ALTER TABLE ETX_ADT_MESSAGE ADD CONSTRAINT C_ETX_ADT_MSG_REC_ID_FK FOREIGN KEY (MSG_RECEIVER_PAR_ID) REFERENCES ETX_ADT_PARTY (PAR_ID);
ALTER TABLE ETX_ADT_MESSAGE ADD CONSTRAINT C_ETX_ADT_MSG_SEN_ID_FK FOREIGN KEY (MSG_SENDER_PAR_ID) REFERENCES ETX_ADT_PARTY (PAR_ID);

CREATE INDEX I_ETX_ADT_ATT_ATTUUID_IX ON ETX_ADT_ATTACHMENT(ATT_UUID);
CREATE INDEX I_ETX_ADT_ATT_MSG_ID_IX ON ETX_ADT_ATTACHMENT(MSG_ID);
ALTER TABLE ETX_ADT_ATTACHMENT ADD CONSTRAINT C_ETX_ADT_ATT_ERR_ID_FK FOREIGN KEY (ERR_ID) REFERENCES ETX_ADT_ERROR (ERR_ID);
ALTER TABLE ETX_ADT_ATTACHMENT ADD CONSTRAINT C_ETX_ADT_ATT_MSG_ID_FK FOREIGN KEY (MSG_ID) REFERENCES ETX_ADT_MESSAGE (MSG_ID);


ALTER TABLE ETX_ADT_MESSAGE ADD DOMIBUS_MESSAGEID VARCHAR2(255 BYTE);
ALTER TABLE ETX_ADT_ATTACHMENT ADD DOMIBUS_MESSAGEID VARCHAR2(255 BYTE);
CREATE INDEX ETX_ADT_MSG_DOMIBUSMSGID_IX ON ETX_ADT_MESSAGE (DOMIBUS_MESSAGEID);
CREATE INDEX ETX_ADT_ATTCH_DOMIBUSMSGID_IX ON ETX_ADT_ATTACHMENT (DOMIBUS_MESSAGEID);
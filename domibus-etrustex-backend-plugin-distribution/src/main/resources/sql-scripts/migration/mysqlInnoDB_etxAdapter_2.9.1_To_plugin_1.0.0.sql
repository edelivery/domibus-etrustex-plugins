set FOREIGN_KEY_CHECKS=0;
#--------------------------------------------------------------
#--  DDL To drop unnecessary tables
#--------------------------------------------------------------
drop table if exists etx_adt_message_job;
drop table if exists ETX_ADT_KEY_STORE_ENTRY;
drop table if exists ETX_ADT_SYSTEM_KEY_STORE;
drop procedure if exists ADDPARTY;

#--------------------------------------------------------------
#--  DDL To correct the character-set and collation of all migrated tables
#--------------------------------------------------------------
alter table ETX_ADT_CONFIG convert to character set UTF8 collate UTF8_BIN;
alter table ETX_ADT_USER convert to character set UTF8 collate UTF8_BIN;
alter table ETX_ADT_SYSTEM convert to character set UTF8 collate UTF8_BIN;
alter table ETX_ADT_SYSTEM_CONFIG convert to character set UTF8 collate UTF8_BIN;
alter table ETX_ADT_PARTY convert to character set UTF8 collate UTF8_BIN;
alter table ETX_ADT_ERROR convert to character set UTF8 collate UTF8_BIN;
alter table ETX_ADT_MESSAGE convert to character set UTF8 collate UTF8_BIN;
alter table ETX_ADT_ATTACHMENT convert to character set UTF8 collate UTF8_BIN;

#--------------------------------------------------------------
#--  DDL To drop foreign keys
#--------------------------------------------------------------
set @qry=if((SELECT true FROM information_schema.TABLE_CONSTRAINTS WHERE
            CONSTRAINT_SCHEMA = DATABASE() AND
            TABLE_NAME        = 'ETX_ADT_ATTACHMENT' AND
            CONSTRAINT_NAME   = 'ETX_ADT_ATT_ERR_ID_FK' AND
            CONSTRAINT_TYPE   = 'FOREIGN KEY') = true,'alter table ETX_ADT_ATTACHMENT drop foreign key ETX_ADT_ATT_ERR_ID_FK; ','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;

set @qry=if((SELECT true FROM information_schema.TABLE_CONSTRAINTS WHERE
            CONSTRAINT_SCHEMA = DATABASE() AND
            TABLE_NAME        = 'ETX_ADT_ATTACHMENT' AND
            CONSTRAINT_NAME   = 'ETX_ADT_ATT_MSG_ID_FK' AND
            CONSTRAINT_TYPE   = 'FOREIGN KEY') = true,'alter table ETX_ADT_ATTACHMENT drop foreign key ETX_ADT_ATT_MSG_ID_FK; ','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;

set @qry=if((SELECT true FROM information_schema.TABLE_CONSTRAINTS WHERE
            CONSTRAINT_SCHEMA = DATABASE() AND
            TABLE_NAME        = 'ETX_ADT_MESSAGE' AND
            CONSTRAINT_NAME   = 'ETX_ADT_MSG_ERR_ID_FK' AND
            CONSTRAINT_TYPE   = 'FOREIGN KEY') = true,'alter table ETX_ADT_MESSAGE drop foreign key ETX_ADT_MSG_ERR_ID_FK; ','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;

set @qry=if((SELECT true FROM information_schema.TABLE_CONSTRAINTS WHERE
            CONSTRAINT_SCHEMA = DATABASE() AND
            TABLE_NAME        = 'ETX_ADT_MESSAGE' AND
            CONSTRAINT_NAME   = 'ETX_ADT_MSG_REC_ID_FK' AND
            CONSTRAINT_TYPE   = 'FOREIGN KEY') = true,'alter table ETX_ADT_MESSAGE drop foreign key ETX_ADT_MSG_REC_ID_FK; ','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;

set @qry=if((SELECT true FROM information_schema.TABLE_CONSTRAINTS WHERE
            CONSTRAINT_SCHEMA = DATABASE() AND
            TABLE_NAME        = 'ETX_ADT_MESSAGE' AND
            CONSTRAINT_NAME   = 'ETX_ADT_MSG_SEN_ID_FK' AND
            CONSTRAINT_TYPE   = 'FOREIGN KEY') = true,'alter table ETX_ADT_MESSAGE drop foreign key ETX_ADT_MSG_SEN_ID_FK; ','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;

set @qry=if((SELECT true FROM information_schema.TABLE_CONSTRAINTS WHERE
            CONSTRAINT_SCHEMA = DATABASE() AND
            TABLE_NAME        = 'ETX_ADT_PARTY' AND
            CONSTRAINT_NAME   = 'ETX_ADT_PARTY_SYS_ID_FK' AND
            CONSTRAINT_TYPE   = 'FOREIGN KEY') = true,'alter table ETX_ADT_PARTY drop foreign key ETX_ADT_PARTY_SYS_ID_FK; ','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;

set @qry=if((SELECT true FROM information_schema.TABLE_CONSTRAINTS WHERE
            CONSTRAINT_SCHEMA = DATABASE() AND
            TABLE_NAME        = 'ETX_ADT_PARTY' AND
            CONSTRAINT_NAME   = 'ETX_ADT_USER_USR_ID_FK' AND
            CONSTRAINT_TYPE   = 'FOREIGN KEY') = true,'alter table ETX_ADT_PARTY drop foreign key ETX_ADT_USER_USR_ID_FK; ','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;

set @qry=if((SELECT true FROM information_schema.TABLE_CONSTRAINTS WHERE
            CONSTRAINT_SCHEMA = DATABASE() AND
            TABLE_NAME        = 'ETX_ADT_SYSTEM' AND
            CONSTRAINT_NAME   = 'ETX_ADT_SYSTEM_BACKEND_USR_FK' AND
            CONSTRAINT_TYPE   = 'FOREIGN KEY') = true,'alter table ETX_ADT_SYSTEM drop foreign key ETX_ADT_SYSTEM_BACKEND_USR_FK; ','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;

set @qry=if((SELECT true FROM information_schema.TABLE_CONSTRAINTS WHERE
            CONSTRAINT_SCHEMA = DATABASE() AND
            TABLE_NAME        = 'ETX_ADT_SYSTEM' AND
            CONSTRAINT_NAME   = 'ETX_ADT_SYSTEM_LOCAL_USR_FK' AND
            CONSTRAINT_TYPE   = 'FOREIGN KEY') = true,'alter table ETX_ADT_SYSTEM drop foreign key ETX_ADT_SYSTEM_LOCAL_USR_FK; ','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;

set @qry=if((SELECT true FROM information_schema.TABLE_CONSTRAINTS WHERE
            CONSTRAINT_SCHEMA = DATABASE() AND
            TABLE_NAME        = 'ETX_ADT_SYSTEM_CONFIG' AND
            CONSTRAINT_NAME   = 'ETX_ADT_SCG_SYS_ID_FK' AND
            CONSTRAINT_TYPE   = 'FOREIGN KEY') = true,'alter table ETX_ADT_SYSTEM_CONFIG drop foreign key ETX_ADT_SCG_SYS_ID_FK; ','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;


#--------------------------------------------------------------
#--  DDL to drop index
#--------------------------------------------------------------
set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_ATTACHMENT' and
            INDEX_NAME   = 'ETX_ADT_ATT_PK') = true,'alter table ETX_ADT_ATTACHMENT drop index ETX_ADT_ATT_PK;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;

set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_ATTACHMENT' and
            INDEX_NAME   = 'ETX_ADT_ATT_ATTUUID_IX') = true,'alter table ETX_ADT_ATTACHMENT drop index ETX_ADT_ATT_ATTUUID_IX;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;

set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_ATTACHMENT' and
            INDEX_NAME   = 'ETX_ADT_ATT_MSG_ID_IX') = true,'alter table ETX_ADT_ATTACHMENT drop index ETX_ADT_ATT_MSG_ID_IX;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;

set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_MESSAGE' and
            INDEX_NAME   = 'ETX_ADT_MSG_PK') = true,'alter table ETX_ADT_MESSAGE drop index ETX_ADT_MSG_PK;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;

set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_MESSAGE' and
            INDEX_NAME   = 'ETX_ADT_MSG_UUID_IX') = true,'alter table ETX_ADT_MESSAGE drop index ETX_ADT_MSG_UUID_IX;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;

set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_MESSAGE' and
            INDEX_NAME   = 'ETX_ADT_MSG_REC_ID_IX') = true,'alter table ETX_ADT_MESSAGE drop index ETX_ADT_MSG_REC_ID_IX;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;

set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_MESSAGE' and
            INDEX_NAME   = 'ETX_ADT_MSG_SEN_ID_IX') = true,'alter table ETX_ADT_MESSAGE drop index ETX_ADT_MSG_SEN_ID_IX;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;

set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_MESSAGE' and
            INDEX_NAME   = 'ETX_ADT_MSG_ERR_ID_IX') = true,'alter table ETX_ADT_MESSAGE drop index ETX_ADT_MSG_ERR_ID_IX;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;


set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_MESSAGE' and
            INDEX_NAME   = 'ETX_ADT_MSG_NOTST_IX') = true,'alter table ETX_ADT_MESSAGE drop index ETX_ADT_MSG_NOTST_IX;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;

set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_ERROR' and
            INDEX_NAME   = 'ETX_ADT_ERR_PK') = true,'alter table ETX_ADT_ERROR drop index ETX_ADT_ERR_PK;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;


set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_PARTY' and
            INDEX_NAME   = 'ETX_ADT_PARTY_PK') = true,'alter table ETX_ADT_PARTY drop index ETX_ADT_PARTY_PK;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;


set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_PARTY' and
            INDEX_NAME   = 'ETX_ADT_PARTY_PAR_UUID_UK') = true,'alter table ETX_ADT_PARTY drop index ETX_ADT_PARTY_PAR_UUID_UK;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;


set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_PARTY' and
            INDEX_NAME   = 'ETX_ADT_PARTY_SYS_ID_IX') = true,'alter table ETX_ADT_PARTY drop index ETX_ADT_PARTY_SYS_ID_IX;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;


set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_PARTY' and
            INDEX_NAME   = 'ETX_ADT_USER_USR_ID_FK') = true,'alter table ETX_ADT_PARTY drop index ETX_ADT_USER_USR_ID_FK;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;


set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_SYSTEM_CONFIG' and
            INDEX_NAME   = 'ETX_ADT_SCG_PK') = true,'alter table ETX_ADT_SYSTEM_CONFIG drop index ETX_ADT_SCG_PK;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;


set @qry=if((select distinct true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_SYSTEM_CONFIG' and
            INDEX_NAME   = 'ETX_ADT_SCG_SYS_ID_PROPERTY') = true,'alter table ETX_ADT_SYSTEM_CONFIG drop index ETX_ADT_SCG_SYS_ID_PROPERTY;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;


set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_SYSTEM' and
            INDEX_NAME   = 'ETX_ADT_SYS_PK') = true,'alter table ETX_ADT_SYSTEM drop index ETX_ADT_SYS_PK;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;


set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_SYSTEM' and
            INDEX_NAME   = 'ETX_ADT_SYSTEM_LOCAL_USR_ID_IX') = true,'alter table ETX_ADT_SYSTEM drop index ETX_ADT_SYSTEM_LOCAL_USR_ID_IX;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;


set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_SYSTEM' and
            INDEX_NAME   = 'ETX_ADT_SYSTEM_BACK_USR_ID_IX') = true,'alter table ETX_ADT_SYSTEM drop index ETX_ADT_SYSTEM_BACK_USR_ID_IX;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;


set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_USER' and
            INDEX_NAME   = 'ETX_ADT_USR_PK') = true,'alter table ETX_ADT_USER drop index ETX_ADT_USR_PK;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;


set @qry=if((select true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_CONFIG' and
            INDEX_NAME   = 'ETX_ADT_CONFIG_PK') = true,'alter table ETX_ADT_CONFIG drop index ETX_ADT_CONFIG_PK;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;


set @qry=if((select distinct true from INFORMATION_SCHEMA.STATISTICS where
            TABLE_SCHEMA = DATABASE() and
            INDEX_NAME <> 'PRIMARY' and
            TABLE_NAME = 'ETX_ADT_CONFIG' and
            INDEX_NAME   = 'ETX_ADT_CONFIG_NAME_VALUE') = true,'alter table ETX_ADT_CONFIG drop index ETX_ADT_CONFIG_NAME_VALUE;','select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;


set @qry=if((select true from INFORMATION_SCHEMA.COLUMNS
              where TABLE_SCHEMA = DATABASE() and
              TABLE_NAME = 'ETX_ADT_PARTY' and
              column_name = 'PAR_IS_RECEIVING_MESSAGES') = true, 'alter table ETX_ADT_PARTY drop column PAR_IS_RECEIVING_MESSAGES;', 'select 1');
select @qry;
prepare stmt from @qry;
execute stmt;
deallocate prepare stmt;


alter table ETX_ADT_ATTACHMENT drop primary key, add constraint PK_ETX_ADT_ATTACHMENT primary key (ATT_ID) ;
alter table ETX_ADT_MESSAGE drop primary key, add constraint PK_ETX_ADT_MESSAGE primary key (MSG_ID);
alter table ETX_ADT_ERROR drop primary key, add constraint PK_ETX_ADT_ERROR primary key (ERR_ID);
alter table ETX_ADT_PARTY drop primary key, add constraint PK_ETX_ADT_PARTY primary key (PAR_ID);
alter table ETX_ADT_SYSTEM_CONFIG drop primary key, add constraint PK_ETX_ADT_SYSTEM_CONFIG primary key (SCG_ID);
alter table ETX_ADT_SYSTEM drop primary key, add constraint PK_ETX_ADT_SYSTEM primary key (SYS_ID);
alter table ETX_ADT_USER drop primary key, add constraint PK_ETX_ADT_USER primary key (USR_ID);
alter table ETX_ADT_CONFIG drop primary key, add constraint PK_ETX_ADT_CONFIG primary key (ACG_ID);



#--------------------------------------------------------------
#--  DDL to recreate constraints with correct name
#--------------------------------------------------------------
alter table ETX_ADT_ATTACHMENT add constraint C_ETX_ADT_ATT_ERR_ID_FK foreign key (ERR_ID) references ETX_ADT_ERROR (ERR_ID) on update restrict on delete restrict;
alter table ETX_ADT_ATTACHMENT add constraint C_ETX_ADT_ATT_MSG_ID_FK foreign key (MSG_ID) references ETX_ADT_MESSAGE (MSG_ID) on update restrict on delete restrict;
alter table ETX_ADT_MESSAGE add constraint C_ETX_ADT_MSG_ERR_ID_FK foreign key (ERR_ID) references ETX_ADT_ERROR (ERR_ID) on update restrict on delete restrict;
alter table ETX_ADT_MESSAGE add constraint C_ETX_ADT_MSG_REC_ID_FK foreign key (MSG_RECEIVER_PAR_ID) references ETX_ADT_PARTY (PAR_ID) on update restrict on delete restrict;
alter table ETX_ADT_MESSAGE add constraint C_ETX_ADT_MSG_SEN_ID_FK foreign key (MSG_SENDER_PAR_ID) references ETX_ADT_PARTY (PAR_ID) on update restrict on delete restrict;
alter table ETX_ADT_PARTY add constraint C_ETX_ADT_PARTY_SYS_ID_FK foreign key (SYS_ID) references ETX_ADT_SYSTEM (SYS_ID) on update restrict on delete restrict;
alter table ETX_ADT_PARTY add constraint C_ETX_ADT_USER_USR_ID_FK foreign key (USR_ID) references ETX_ADT_USER (USR_ID) on update restrict on delete restrict;
alter table ETX_ADT_SYSTEM add constraint C_ETX_ADT_SYS_BACKEND_USR_FK foreign key (SYS_BACKEND_USR_ID) references ETX_ADT_USER (USR_ID) on update restrict on delete restrict;
alter table ETX_ADT_SYSTEM add constraint C_ETX_ADT_SYS_LOCAL_USR_FK foreign key (USR_ID) references ETX_ADT_USER (USR_ID) on update restrict on delete restrict;
alter table ETX_ADT_SYSTEM_CONFIG add constraint C_ETX_ADT_SCG_SYS_ID_FK foreign key (SYS_ID) references ETX_ADT_SYSTEM (SYS_ID) on update restrict on delete restrict;

#--------------------------------------------------------------
#--  DDL to recreate index with correct names
#--------------------------------------------------------------
create index I_ETX_ADT_ATT_ATTUUID_IX on ETX_ADT_ATTACHMENT(ATT_UUID);
create index I_ETX_ADT_ATT_MSG_ID_IX on ETX_ADT_ATTACHMENT(MSG_ID);
create unique index I_ETX_ADT_CONFIG_NAME_VALUE on ETX_ADT_CONFIG(ACG_PROPERTY_NAME, ACG_PROPERTY_VALUE);
create index I_ETX_ADT_MSG_UUID_IX on ETX_ADT_MESSAGE(MSG_UUID);
create index I_ETX_ADT_MSG_REC_ID_IX on ETX_ADT_MESSAGE(MSG_RECEIVER_PAR_ID);
create index I_ETX_ADT_MSG_SEN_ID_IX on ETX_ADT_MESSAGE(MSG_SENDER_PAR_ID);
create index I_ETX_ADT_MSG_ERR_ID_IX on ETX_ADT_MESSAGE(ERR_ID);
create index I_ETX_ADT_MSG_NOTST_IX on ETX_ADT_MESSAGE(MSG_NOTIFICATION_STATE);
create unique index I_ETX_ADT_PARTY_PAR_UUID_UK on ETX_ADT_PARTY(PAR_UUID);
create index I_ETX_ADT_PARTY_SYS_ID_IX on ETX_ADT_PARTY(SYS_ID);
create index I_ETX_ADT_SYS_LOCAL_USR_ID_IX on ETX_ADT_SYSTEM(USR_ID);
create index I_ETX_ADT_SYS_BACK_USR_ID_IX on ETX_ADT_SYSTEM(SYS_BACKEND_USR_ID);
create unique index I_ETX_ADT_SCG_SYS_ID_PROPERTY on ETX_ADT_SYSTEM_CONFIG(SYS_ID, SCG_PROPERTY_NAME);


#--------------------------------------------------------------
#--  DDL To Extend Tables ETX_ADT_MESSAGE & ETX_ADT_ATTACHMENT
#--------------------------------------------------------------

ALTER TABLE ETX_ADT_MESSAGE ADD DOMIBUS_MESSAGEID VARCHAR(255);
ALTER TABLE ETX_ADT_ATTACHMENT ADD DOMIBUS_MESSAGEID VARCHAR(255);
CREATE INDEX ETX_ADT_MSG_DOMIBUSMSGID_IX ON ETX_ADT_MESSAGE (DOMIBUS_MESSAGEID);
CREATE INDEX ETX_ADT_ATTCH_DOMIBUSMSGID_IX ON ETX_ADT_ATTACHMENT (DOMIBUS_MESSAGEID);

set FOREIGN_KEY_CHECKS=1;

--  *********************************************************************
--  Update Database Script
--  *********************************************************************
--  Change Log: src/main/resources/config/liquibase/changelog/changelog-1.1.0-delta.xml
--  Ran at: 24/01/20 18:29
--  Against: null@offline:mysql?changeLogFile=E:\workspace\domibus-plugins\domibus-etrustex-backend-plugin\target/liquibase/changelog-1.1.0-delta.mysql
--  Liquibase version: 3.5.4
--  *********************************************************************

--  Changeset src/main/resources/config/liquibase/changelog/changelog-1.1.0-delta.xml::EDELIVERY-3557_remove_redundant_data::gautifr
SET SQL_SAFE_UPDATES = 0;

DELETE FROM ETX_ADT_CONFIG WHERE ACG_PROPERTY_NAME in (
                'etx.node.services.DocumentBundleService.url',
                'etx.node.services.DocumentWrapperService.url',
                'etx.node.services.InboxRequestService.url',
                'etx.node.services.RetrieveRequestService.url',
                'etx.node.services.ApplicationResponseService.url',
                'etx.node.services.RetrieveInterchangeAgreementService.url',
                'etx.node.security.p2p.enabled'
                );

DELETE FROM ETX_ADT_SYSTEM_CONFIG WHERE SCG_PROPERTY_NAME = 'etx.backend.soap.message.logging';

SET SQL_SAFE_UPDATES = 1;

ALTER TABLE ETX_ADT_PARTY DROP FOREIGN KEY C_ETX_ADT_USER_USR_ID_FK;

ALTER TABLE ETX_ADT_PARTY DROP COLUMN USR_ID;

--  Changeset src/main/resources/config/liquibase/changelog/changelog-1.1.0-delta.xml::EDELIVERY-3557_create_ica_table::gautifr
CREATE TABLE ETX_ADT_ICA (ICA_ID BIGINT AUTO_INCREMENT NOT NULL, ICA_SENDER_PAR_ID BIGINT NOT NULL, ICA_RECEIVER_PAR_ID BIGINT NOT NULL, ICA_XML VARCHAR(4000) NULL, ICA_CREATED_ON datetime DEFAULT NOW() NOT NULL, ICA_UPDATED_ON datetime NULL, CONSTRAINT PK_ETX_ADT_ICA PRIMARY KEY (ICA_ID));

ALTER TABLE ETX_ADT_ICA ADD CONSTRAINT C_ETX_ADT_ICA_SENDER_PAR_ID_FK FOREIGN KEY (ICA_SENDER_PAR_ID) REFERENCES ETX_ADT_PARTY (PAR_ID) ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE ETX_ADT_ICA ADD CONSTRAINT C_ETX_ADT_ICA_RECEVR_PAR_ID_FK FOREIGN KEY (ICA_RECEIVER_PAR_ID) REFERENCES ETX_ADT_PARTY (PAR_ID) ON UPDATE RESTRICT ON DELETE RESTRICT;

--  Changeset src/main/resources/config/liquibase/changelog/changelog-1.1.0-delta.xml::EDELIVERY-4140_default_system_for_third_parties::venugar
set @sysIdToEnter = (select if(ifnull(min(sys_id),1) > 0, 0 , max(sys_id)+1) from ETX_ADT_SYSTEM);

set @qry=if ((select count(1) from ETX_ADT_SYSTEM where sys_name = 'THIRD-PARTY-SYS')=0,
            'INSERT INTO ETX_ADT_SYSTEM (SYS_ID, SYS_NAME, USR_ID, SYS_BACKEND_USR_ID) VALUES (@sysIdToEnter, ''THIRD-PARTY-SYS'', NULL, NULL)',
            'select 1');

prepare stmt from @qry;

execute stmt;

deallocate prepare stmt;

--  Changeset src/main/resources/config/liquibase/changelog/changelog-1.1.0-delta.xml::EDELIVERY-4272_SYSTEM_with_same_USER::gautifr
ALTER TABLE ETX_ADT_SYSTEM ADD USERNAME VARCHAR(255) NULL;

SET SQL_SAFE_UPDATES = 0;

update ETX_ADT_SYSTEM eas set eas.USERNAME = ( select distinct eau.USR_NAME from ETX_ADT_USER eau where eau.usr_id = eas.usr_id );

SET SQL_SAFE_UPDATES = 1;

ALTER TABLE ETX_ADT_SYSTEM DROP FOREIGN KEY C_ETX_ADT_SYS_LOCAL_USR_FK;

DROP INDEX I_ETX_ADT_SYS_LOCAL_USR_ID_IX ON ETX_ADT_SYSTEM;

ALTER TABLE ETX_ADT_SYSTEM DROP COLUMN USR_ID;


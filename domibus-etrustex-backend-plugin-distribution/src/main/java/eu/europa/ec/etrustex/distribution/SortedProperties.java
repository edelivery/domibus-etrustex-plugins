package eu.europa.ec.etrustex.distribution;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.*;
import java.util.function.Predicate;

import static eu.europa.ec.etrustex.distribution.PropertiesUtils.getRealName;
import static java.math.BigDecimal.valueOf;
import static java.math.RoundingMode.CEILING;
import static org.apache.commons.lang3.StringUtils.*;
import static org.apache.commons.lang3.math.NumberUtils.isParsable;

/**
 * @author François Gautier
 * @version 1.0
 * @since 16/05/2018
 */
public class SortedProperties extends Properties {

    static final String SEPARATOR_POINT = ".";
    static final String NAME = "name";

    private static final String CHARSET_NAME = "8859_1";
    private static final String SEP =
            "#############################################" +
                    "##############################################";
    private static final String SEP_SEMI_COL = ": ";
    private static final String SPACE = " ";

    private Set<String> tops = new HashSet<>();


    /**
     * Overrides, called by the store method.
     */
    public synchronized Enumeration keys() {
        Vector<String> keyTops = get(super.keys(), this::isInTops);
        Collections.sort(keyTops);

        Vector<String> keyRest = get(super.keys(), this::isNotInTops);
        Collections.sort(keyRest);
        keyTops.addAll(keyRest);

        return keyTops.elements();
    }

    private Vector<String> get(Enumeration keysEnum, Predicate<String> predicate) {
        Vector<String> keyRest = new Vector<>();
        while (keysEnum.hasMoreElements()) {
            String key = String.valueOf(keysEnum.nextElement());

            if(predicate.test(key)) {
                keyRest.add(key);
            }
        }
        return keyRest;
    }

    Set<String> getKey(String value){
        Set<String> result = new HashSet<>();
        for (Map.Entry<Object, Object> entry: entrySet()) {
            if(StringUtils.equalsIgnoreCase(String.valueOf(entry.getValue()), value)){
                result.add(String.valueOf(entry.getKey()));
            }
        }
        return result;
    }

    boolean isNotInTops(String key){
        return tops.stream().noneMatch(key::contains);
    }
    boolean isInTops(String key){
        return tops.stream().anyMatch(key::contains);
    }

    public void store(OutputStream out, String comments) throws IOException {
        storeCustom(new BufferedWriter(new OutputStreamWriter(out, CHARSET_NAME)), comments);
    }

    /**
     * We define the root of the key as the two first element of the key plus the third one if it is a number.
     * Ex:
     * domain.workmanager.1.targets -> domain.workmanager.1
     * domain.workmanager.items     -> domain.workmanager
     *
     * @param key with "." as separator
     * @return the root of the key
     */
    static String getRoot(String key) {
        String result = StringUtils.EMPTY;
        String[] split = StringUtils.split(key, SEPARATOR_POINT);
        int indexNumber = 2;
        for (int i = 0; i < split.length; i++) {
            if (isParsable(split[i])) {
                indexNumber = i;
            }
        }

        for (int i = 0; i < indexNumber; i++) {
            if (isNotBlank(result)) {
                result += SEPARATOR_POINT;
            }
            result += split[i];
        }

        if (split.length > 2 && isParsable(split[indexNumber])) {
            result += SEPARATOR_POINT + split[indexNumber];
        }
        return result;
    }

    private void storeCustom(BufferedWriter bw, String commentsTitle) throws IOException {
        addSeparation(bw);
        writeComment(bw, commentsTitle);
        writeComment(bw, new Date().toString());
        addSeparation(bw);
        bw.newLine();
        synchronized (this) {
            String previousRoot = StringUtils.EMPTY;
            for (Enumeration<?> e = keys(); e.hasMoreElements(); ) {
                String key = (String) e.nextElement();
                String val = (String) get(key);
                key = saveConvert(key, true);
                /* No need to escape embedded and trailing spaces for value, hence
                 * pass false to flag.
                 */
                val = saveConvert(val, false);
                String newRoot = getRoot(key);
                if (!previousRoot.equals(newRoot) && isNotInTops(newRoot)) {
                    separateSection(bw, newRoot);
                }
                if(isNotInTops(newRoot) && isInTops(previousRoot)){
                   addSeparation(bw);
                }
                previousRoot = newRoot;
                addStringWithReturn(bw, key + "=" + val);
            }
        }
        bw.flush();
    }

    private void separateSection(BufferedWriter bw, String newRoot) throws IOException {
        if (isParsable(substringAfterLast(newRoot, SEPARATOR_POINT))) {
            bw.newLine();
            addSeparation(bw);
            String property = getRealName(this, newRoot + SEPARATOR_POINT + NAME);
            if (isNotBlank(property)) {
                writeComment(bw, getFormattedRoot(substringBeforeLast(newRoot, SEPARATOR_POINT)) + SEP_SEMI_COL + property);
                addSeparation(bw);
            }
        } else {
            addStringWithReturn(bw, "");
        }
    }

    private String getFormattedRoot(String root) {
        StringBuilder result = new StringBuilder(StringUtils.EMPTY);
        String[] split = StringUtils.split(root, SEPARATOR_POINT);
        for (String s : split) {
            result.append(StringUtils.capitalize(s)).append(SPACE);
        }
        return result.toString();
    }

    private void writeComment(BufferedWriter bw, String s) throws IOException {
        int i = SEP.length() - (s.length() + 2);
        int divide = valueOf(i).divide(valueOf(2), CEILING).intValue();
        int remainder = valueOf(i).remainder(valueOf(2)).intValue();
        addStringWithReturn(bw, "#" + getFiller(divide-remainder) + s + getFiller(divide) + "#");
    }

    private String getFiller(int divide) {
        StringBuilder filler = new StringBuilder(StringUtils.EMPTY);
        for (int j = 0; j < divide; j++) {
            filler.append(SPACE);
        }
        return filler.toString();
    }

    private void addSeparation(BufferedWriter bw) throws IOException {
        addStringWithReturn(bw, SEP);
    }
    private void addStringWithReturn(BufferedWriter bw, String sep) throws IOException {
        bw.write(sep);
        bw.newLine();
    }

    /*
     * Converts unicodes to encoded &#92;uxxxx and escapes
     * special characters with a preceding slash
     */
    private String saveConvert(String theString,
                               boolean escapeSpace) {
        int len = theString.length();
        int bufLen = len * 2;
        if (bufLen < 0) {
            bufLen = Integer.MAX_VALUE;
        }
        StringBuffer outBuffer = new StringBuffer(bufLen);

        for (int x = 0; x < len; x++) {
            char aChar = theString.charAt(x);
            // Handle common case first, selecting largest block that
            // avoids the specials below
            if ((aChar > 61) && (aChar < 127)) {
                if (aChar == '\\') {
                    outBuffer.append('\\');
                    outBuffer.append('\\');
                    continue;
                }
                outBuffer.append(aChar);
                continue;
            }
            switch (aChar) {
                case ' ':
                    if (x == 0 || escapeSpace)
                        outBuffer.append('\\');
                    outBuffer.append(' ');
                    break;
                case '\t':
                    outBuffer.append('\\');
                    outBuffer.append('t');
                    break;
                case '\n':
                    outBuffer.append('\\');
                    outBuffer.append('n');
                    break;
                case '\r':
                    outBuffer.append('\\');
                    outBuffer.append('r');
                    break;
                case '\f':
                    outBuffer.append('\\');
                    outBuffer.append('f');
                    break;
                case '=': // Fall through
                case ':': // Fall through
                case '#': // Fall through
                case '!':
                    outBuffer.append('\\');
                    outBuffer.append(aChar);
                    break;
                default:
                    if (((aChar < 0x0020) || (aChar > 0x007e)) & true) {
                        outBuffer.append('\\');
                        outBuffer.append('u');
                        outBuffer.append(toHex((aChar >> 12) & 0xF));
                        outBuffer.append(toHex((aChar >> 8) & 0xF));
                        outBuffer.append(toHex((aChar >> 4) & 0xF));
                        outBuffer.append(toHex(aChar & 0xF));
                    } else {
                        outBuffer.append(aChar);
                    }
            }
        }
        return outBuffer.toString();
    }

    /**
     * A table of hex digits
     */
    private static final char[] hexDigit = {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
    };

    /**
     * Convert a nibble to a hex character
     *
     * @param nibble the nibble to convert.
     */
    private static char toHex(int nibble) {
        return hexDigit[(nibble & 0xF)];
    }

    public void addTopRoot(String top) {
        this.tops.add(top);
    }
}

package eu.europa.ec.etrustex.distribution;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author François Gautier
 * @version 1.0
 * @since 14/05/2018
 */
public class MergeWlst {

    private static final String NEWLINE = "\n";

    public static void main(String[] a) throws IOException {
        System.out.println("Domibus  properties file: " + a[0]);
        System.out.println("eTrustEx properties file: " + a[1]);
        System.out.println("Output   properties file: " + a[2]);


        if(Files.notExists(Paths.get(a[2]))){
            File mergedFile = new File(a[2]);
            mergedFile.getParentFile().mkdirs();
            mergedFile.createNewFile();
            System.out.println("Created File:"+mergedFile.getAbsolutePath());
        }
        if (a[0].contains("WeblogicCluster") && a[1].contains("WeblogicCluster")) {
            mergeWeblogicClusterProperties(a[0], a[1], a[2]);
        }

        if (a[0].contains("WeblogicSingleServer") && a[1].contains("WeblogicSingleServer")) {
            mergeWeblogicSingleServerProperties(a[0], a[1], a[2]);
        }
    }

    private static void mergeWeblogicClusterProperties(String domibusWeblogicClusterFile, String etxBePluginWeblogicClusterFile, String mergedWeblogicClusterFile) throws IOException {
        try (BufferedReader domibusPropsFileInput = new BufferedReader(new FileReader(domibusWeblogicClusterFile));
             BufferedReader etxBePluginPropsFileInput = new BufferedReader(new FileReader(etxBePluginWeblogicClusterFile));
             BufferedWriter mergedOutput = new BufferedWriter(new FileWriter(mergedWeblogicClusterFile))
        ) {
            String domibusprops;
            int queueCount = 0;
            while ((domibusprops = domibusPropsFileInput.readLine()) != null) {
                //if JMS queues are found, increment the count
                if (domibusprops.matches("^jms\\.uniform\\.distributed\\.queue\\.\\d*\\.name=.*$")) {
                    queueCount++;
                    System.out.println("Queue found, queueCount:" + queueCount);
                }

                //Continue until JMS queue index is found.
                if (!domibusprops.startsWith("## JMS queue index")) {
                    writeToFileWithNewLine(domibusprops, mergedOutput);
                }


                if (domibusprops.contains("JMS queue index")) {
                    //reached jms queue index of original domibus properties file
                    //skip lines till we see jms.uniform.distributed.queue.items
                    while (!(domibusprops = domibusPropsFileInput.readLine()).contains("jms.uniform.distributed.queue.items")) {
                        System.out.println("Skipping");
                    }

                    //reached after jms.uniform.distributed.queue.items
                    //read etx be plugin wlst properties file and write from the point of ##############ETrustExBackendPlugin Queue Configurations start#########################
                    writeEtxBePluginWeblogicClusterJMSProps(domibusPropsFileInput, etxBePluginPropsFileInput, mergedOutput, queueCount);
                }
            }
        }
    }

    private static void writeEtxBePluginWeblogicClusterJMSProps(BufferedReader domibusPropsFileInput, BufferedReader etxBePluginPropsFileInput, BufferedWriter mergedOutput, int queueCount) throws IOException {
        String etxBePluginProps;

        //Skip until JMS configurations start in BE Plugin properteis
        while ((etxBePluginProps = etxBePluginPropsFileInput.readLine()) != null) {
            if (!etxBePluginProps.contains("ETrustExBackendPlugin Queue Configurations start")) {
                continue;
            } else {
                break;
            }
        }

        writeToFileWithNewLine(etxBePluginProps, mergedOutput);
        //Write ETX BE Plugin queue configurations
        while ((etxBePluginProps = etxBePluginPropsFileInput.readLine()) != null) {
            if (etxBePluginProps.startsWith("#")) {
                writeToFileWithNewLine(etxBePluginProps, mergedOutput);
            }
            if (etxBePluginProps.startsWith("jms.uniform.distributed.queue")) {
                if (etxBePluginProps.matches("^jms\\.uniform\\.distributed\\.queue\\.\\d*\\.name=.*$")) {
                    queueCount++;
                    System.out.println("Queue found, queueCount:" + queueCount);
                }
                String[] jmsQueueConfigArray = etxBePluginProps.split("\\.\\d\\.");
                if (jmsQueueConfigArray != null && jmsQueueConfigArray.length > 1) {
                    //Queue properties found. Write them.
                    String etxQueueConfig = jmsQueueConfigArray[0] + "." + (queueCount - 1) + "." + jmsQueueConfigArray[1];
                    writeToFileWithNewLine(etxQueueConfig, mergedOutput);
                }
            }

            if (etxBePluginProps.contains("jms.uniform.distributed.queue.items=")) {
                String strQueueCount = "jms.uniform.distributed.queue.items=" + queueCount;
                writeToFileWithNewLine(strQueueCount, mergedOutput);
                break;
            }
        }
    }

    private static void mergeWeblogicSingleServerProperties(String domibusWeblogicSingleServerFile, String etxBePluginWeblogicSingleServerFile, String mergedWeblogicSingleServerFile) throws IOException {
        try (BufferedReader domibusPropsFileInput = new BufferedReader(new FileReader(domibusWeblogicSingleServerFile));
             BufferedReader etxBePluginPropsFileInput = new BufferedReader(new FileReader(etxBePluginWeblogicSingleServerFile));
             BufferedWriter mergedOutput = new BufferedWriter(new FileWriter(mergedWeblogicSingleServerFile))
        ) {
            //mergeJTATimeout(domibusPropsFileInput, etxBePluginPropsFileInput, mergedOutput);
            mergeWeblogicSingleServerJMSQueueProperties(domibusPropsFileInput, etxBePluginPropsFileInput, mergedOutput);
        }
    }

    private static void mergeWeblogicSingleServerJMSQueueProperties(BufferedReader domibusPropsFileInput, BufferedReader etxBePluginPropsFileInput, BufferedWriter mergedOutput) throws IOException {
        String domibusprops;
        int queueCount = 0;
        while ((domibusprops = domibusPropsFileInput.readLine()) != null) {
            if (domibusprops.matches("^jms\\.queue\\.\\d*\\.name=.*$")) {
                queueCount++;
                System.out.println("Queue found, queueCount:" + queueCount);
            }

            if (!domibusprops.startsWith("## Queue index")) {
                writeToFileWithNewLine(domibusprops, mergedOutput);
            }

            if (domibusprops.contains("Queue index")) {
                //reached jms queue index of original domibus properties file
                //skip lines till we see jms.queue.items
                while (!(domibusprops = domibusPropsFileInput.readLine()).contains("jms.queue.items")) {
                    System.out.println("Skipping");
                }

                //reached after jms.uniform.distributed.queue.items
                //read etx be plugin wlst properties file and write from the point of ##############ETrustExBackendPlugin Queue Configurations start#########################
                writeEtxBePluginWeblogicSingleServerJMSProps(domibusPropsFileInput, etxBePluginPropsFileInput, mergedOutput, queueCount);
            }
        }
    }

    private static void writeEtxBePluginWeblogicSingleServerJMSProps(BufferedReader domibusPropsFileInput, BufferedReader etxBePluginPropsFileInput, BufferedWriter mergedOutput, int queueCount) throws IOException {
        String etxBePluginProps;

        //Skip until JMS configurations start in BE Plugin properteis
        while ((etxBePluginProps = etxBePluginPropsFileInput.readLine()) != null) {
            if (!etxBePluginProps.contains("ETrustExBackendPlugin Queue Configurations start")) {
                continue;
            } else {
                break;
            }
        }

        writeToFileWithNewLine(etxBePluginProps, mergedOutput);
        while ((etxBePluginProps = etxBePluginPropsFileInput.readLine()) != null) {
            if (etxBePluginProps.startsWith("#")) {
                writeToFileWithNewLine(etxBePluginProps, mergedOutput);
            }
            if (etxBePluginProps.startsWith("jms.queue")) {
                if (etxBePluginProps.matches("^jms\\.queue\\.\\d*\\.name=.*$")) {
                    queueCount++;
                    System.out.println("Queue found, queueCount:" + queueCount);
                }
                String[] jmsQueueConfigArray = etxBePluginProps.split("\\.\\d\\.");
                if (jmsQueueConfigArray != null && jmsQueueConfigArray.length > 1) {
                    //Queue properties found. Write them.
                    String etxQueueConfig = jmsQueueConfigArray[0] + "." + (queueCount - 1) + "." + jmsQueueConfigArray[1];
                    writeToFileWithNewLine(etxQueueConfig, mergedOutput);
                }
            }

            if (etxBePluginProps.startsWith("jms.queue.items=")) {
                String strQueueCount = "jms.queue.items=" + queueCount;
                writeToFileWithNewLine(strQueueCount, mergedOutput);
                break;
            }
        }

    }

    private static void writeToFileWithNewLine(String text, BufferedWriter bufferedWriter) throws IOException {
        bufferedWriter.write(text);
        bufferedWriter.write(NEWLINE);
        bufferedWriter.flush();
        System.out.println(text);
    }

}

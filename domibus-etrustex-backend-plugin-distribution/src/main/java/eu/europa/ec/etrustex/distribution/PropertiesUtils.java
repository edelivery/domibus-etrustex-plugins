package eu.europa.ec.etrustex.distribution;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

public class PropertiesUtils {

    static SortedProperties getProperties(String pathname, String errorMessage) throws IOException {
        File propertiesFile = new File(pathname);
        SortedProperties props = new SortedProperties();
        FileInputStream in = new FileInputStream(propertiesFile);
        props.load(in);
        in.close();
        PropertiesUtils.validatePropertiesFileNotEmpty(props, errorMessage);
        System.out.println("Properties size: " + props.size());
        return props;
    }

    static void saveProperties(Properties properties, Path output) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(output.toFile());
        properties.store(fileOutputStream, "eDelivery with eTrustEx Back-End Plugin");
        fileOutputStream.close();
    }


    static void validatePropertiesFileNotEmpty(Properties etrustExProperties, String s) {
        if (etrustExProperties.size() == 0) {
            throw new IllegalArgumentException(s);
        }
    }

    static Set<String> findByValue(Properties properties, String value) {
        return properties.entrySet().stream()
                .filter(objectObjectEntry -> StringUtils.equalsIgnoreCase(String.valueOf(objectObjectEntry.getValue()), value))
                .map(objectObjectEntry -> String.valueOf(objectObjectEntry.getKey()))
                .collect(Collectors.toSet());
    }

    public static String getRealName(SortedProperties domibusProperties, String property) {
        String result = domibusProperties.getProperty(property);
        if (result != null && result.contains("$")) {
            System.out.println("Search: " + getKey(result));
            result = domibusProperties.getProperty(getKey(result));
            System.out.println("Found: " + result);
        }
        return result;
    }

    private static String getKey(String property) {
        return property.substring(property.indexOf("{") + 1, property.indexOf("}"));
    }
}

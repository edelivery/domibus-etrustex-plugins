package eu.europa.ec.etrustex.distribution;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author François Gautier
 * @version 1.0
 * @since 14/05/2018
 */
@Data
@Builder
public class Entity {
    private boolean leaf;
    private List<Entity> children;
    private Entity parent;
    private String value;
    private String key;
}

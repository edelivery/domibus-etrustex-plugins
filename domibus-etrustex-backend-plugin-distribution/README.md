# README

## E-TrustEx BackEnd Plugin Distribution

There are 2 profiles associated with this distribution:
 1. **etx_backend_plugin_only_distribution** - providing the main release artefact - the package of the eTrustEx Backend plugin

 2. **etx_backend_plugin_domibus_distributions** - providing packages with Domibus artefacts included. This package is intended for experts - to reduce the number of deployment steps. The backend plugin artefacts are inbuilt into the package.

### Prerequisites for the release
1. In module **domibus-etrustex-plugin-common** ensure the following in the pom file:
    1. ````project.parent.version```` - should point to correct Domibus release version
    2. ````project.version```` - should be set to correct backend plugin release version

2. In module  **domibus-etrustex-backend-plugin** ensure the following in the pom file:
    1. ````project.parent.version```` - should point to correct Domibus release version
    2. ````project.version```` - should be set to correct backend plugin release version
    3. under profile **liquibase** modify the execution to generate files for the latest backend plugin final release version (without snapshot)

3. In module **domibus-etrustex-backend-plugin-distribution** ensure the following in the pom file:
    1. ````project.parent.version```` - should point to correct Domibus release version
    2. ````project.version```` - should be set to correct backend plugin release version
    3. ````project.properties.plugin.finalrelease.version```` - should be set to the latest backend plugin final release version (without snapshot)

### Execution Sequence for distribution
1. In module - **domibus-etrustex-plugin-common** execute:  ````mvn clean install````

2. In module **domibus-etrustex-backend-plugin** execute: ````mvn clean install -Pliquibase````
This will generate the necessary Jars in the project build directory. The SQLs will be generated in the *domibus-etrustex-backend-plugin-distribution* project.

3. In module **domibus-etrustex-backend-plugin-distribution** execute: 
    1. to release the main artefact: ````mvn clean deploy -Petx_backend_plugin_only_distribution````
    2. to release artefacts combined with Domibus(only for experts): ````mvn clean deploy -Petx_backend_plugin_domibus_distributions````
    

### Release artefact structure

This main release artefact is a zip file containing the eTrustEx Backend Plugin artefacts organized under directories that can be easily mapped with Domibus.
 
    - plugins
        - config
            domibus-etrustex-backend-plugin.properties
        - lib
            domibus-etrustex-backend-plugin-1.2-SNAPSHOT.jar
    - internal
        ehcache.xml
    - scripts
        - weblogic
            etrustex-backend-plugin-WeblogicCluster.properties
            etrustex-backend-plugin-WeblogicSingleServer.properties
    - sql-scripts
        **DDLs for eTrustEx Backend Plugin installation from scratch and upgrade (Oracle and MySQL)
        - migration
            ** SQLs for migration from eTrustEx Adapter 2.9.1 to eTrustEx Backend Plugin
    logback.xml
    clientauthentication.xml
package eu.europa.ec.edelivery.etrustexplugin.mock.etrustexnode.application;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author Arun Raj
 * @since 1.0
 */
@Service
public class ETXNodeMockProperties {

    @Value("${etx.nodeplugin.services.DocumentBundleService.endpoint}")
    private String etxNodePluginDocumentBundleServiceEndpoint;

    @Value("${etx.nodeplugin.services.ApplicationResponseService.endpoint}")
    private String etxNodePluginApplicationResponseServiceEndpoint;

    @Value("${etx.nodeplugin.user}")
    private String etxNodePluginUser;

    @Value("${etx.nodeplugin.password}")
    private String etxNodePluginPassword;

    public String getEtxNodePluginDocumentBundleServiceEndpoint() {
        return etxNodePluginDocumentBundleServiceEndpoint;
    }

    public void setEtxNodePluginDocumentBundleServiceEndpoint(String etxNodePluginDocumentBundleServiceEndpoint) {
        this.etxNodePluginDocumentBundleServiceEndpoint = etxNodePluginDocumentBundleServiceEndpoint;
    }

    public String getEtxNodePluginApplicationResponseServiceEndpoint() {
        return etxNodePluginApplicationResponseServiceEndpoint;
    }

    public void setEtxNodePluginApplicationResponseServiceEndpoint(String etxNodePluginApplicationResponseServiceEndpoint) {
        this.etxNodePluginApplicationResponseServiceEndpoint = etxNodePluginApplicationResponseServiceEndpoint;
    }

    public String getEtxNodePluginUser() {
        return etxNodePluginUser;
    }

    public void setEtxNodePluginUser(String etxNodePluginUser) {
        this.etxNodePluginUser = etxNodePluginUser;
    }

    public String getEtxNodePluginPassword() {
        return etxNodePluginPassword;
    }

    public void setEtxNodePluginPassword(String etxNodePluginPassword) {
        this.etxNodePluginPassword = etxNodePluginPassword;
    }
}

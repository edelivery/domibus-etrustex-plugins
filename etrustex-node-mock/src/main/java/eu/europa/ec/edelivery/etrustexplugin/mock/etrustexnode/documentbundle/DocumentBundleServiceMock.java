package eu.europa.ec.edelivery.etrustexplugin.mock.etrustexnode.documentbundle;

import ec.schema.xsd.ack_2.AcknowledgmentType;
import ec.schema.xsd.commonaggregatecomponents_2.AcknowledgedDocumentReferenceType;
import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.schema.xsd.commonbasiccomponents_1.AckIndicatorType;
import ec.services.wsdl.documentbundle_2.*;
import eu.europa.ec.edelivery.etrustexplugin.mock.etrustexnode.application.ETXNodeMockProperties;
import eu.europa.ec.edelivery.etrustexplugin.mock.etrustexnode.applicationresponse.ApplicationResponseServiceMock;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.DocumentReferenceType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PartyType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DocumentTypeCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.server.endpoint.annotation.SoapHeader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;

@Endpoint
public class DocumentBundleServiceMock {
    private static final Logger LOG = LoggerFactory.getLogger(DocumentBundleServiceMock.class);

    @Autowired
    ETXNodeMockProperties etxNodeMockProperties;

    @Autowired
    ApplicationResponseServiceMock applicationResponseServiceMock;

    /**
     * @param submitDocumentBundleRequest
     * @param soapHeaderElement
     * @param messageContext
     * @return
     * @throws FaultResponse
     * @throws JAXBException
     */
    @PayloadRoot(namespace = "ec:services:wsdl:DocumentBundle-2", localPart = "SubmitDocumentBundleRequest")
    @ResponsePayload
    public SubmitDocumentBundleResponse submitDocumentBundle(@RequestPayload SubmitDocumentBundleRequest submitDocumentBundleRequest
            , @SoapHeader(value = "{ec:services:wsdl:DocumentBundle-2}Header") SoapHeaderElement soapHeaderElement
            , MessageContext messageContext) throws FaultResponse, JAXBException, IOException {

        JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        JAXBElement<HeaderType> headers = (JAXBElement<HeaderType>) unmarshaller.unmarshal(soapHeaderElement.getSource());
        HeaderType documentBundleHeaderType = headers.getValue();

        String documentBundleId = submitDocumentBundleRequest.getDocumentBundle().getID().getValue();
        String senderParty = documentBundleHeaderType.getBusinessHeader().getSender().get(0).getIdentifier().getValue();
        String receiverParty = documentBundleHeaderType.getBusinessHeader().getReceiver().get(0).getIdentifier().getValue();
        LOG.info("Reached DocumentBundleServiceMock:submitDocumentBundle with BundleId: [{}], Sender:[{}], Receiver:[{}]", documentBundleId, senderParty, receiverParty);

        SubmitDocumentBundleResponseBuilder submitDocumentBundleResponseBuilder = new SubmitDocumentBundleResponseBuilder();
        submitDocumentBundleResponseBuilder.withAckIndicator(true)
                .withDocumentReferenceId(documentBundleId)
                .withDocumentTypeCode("BDL")
                .withSenderParty(submitDocumentBundleRequest.getDocumentBundle().getSenderParty())
                .withReceiverParties(submitDocumentBundleRequest.getDocumentBundle().getReceiverParty())
                .getSubmitDocumentBundleResponse();

        invokeDocumentBundleInNodePlugin(submitDocumentBundleRequest, documentBundleHeaderType);

        try {
            applicationResponseServiceMock.triggerApplicationResponseBDLOK(submitDocumentBundleRequest);
        } catch (Exception e) {
            LOG.error("Faced exception while triggerring BDL OK.", e);
        }

        return submitDocumentBundleResponseBuilder.getSubmitDocumentBundleResponse();
    }

    private void invokeDocumentBundleInNodePlugin(SubmitDocumentBundleRequest submitDocumentBundleRequest, HeaderType documentBundleHeaderType) throws FaultResponse, IOException {

        Holder<HeaderType> header = new Holder<>();
        header.value = documentBundleHeaderType;

        Resource documentBundleServiceWSDLResource = new ClassPathResource("/node/wsdl/DocumentBundle-2.0.wsdl", this.getClass());
        URL documentBundleServiceWSDLLocURL = documentBundleServiceWSDLResource.getURL();
        LOG.trace("DocumentBundleServiceWSDLResource getURL:[{}]", documentBundleServiceWSDLLocURL);

        QName documentBundleServiceQName = new QName("ec:services:wsdl:DocumentBundle-2", "documentBundleService");
        DocumentBundleService documentBundleService = new DocumentBundleService(documentBundleServiceWSDLLocURL, documentBundleServiceQName);
        javax.xml.ws.Service service = javax.xml.ws.Service.create(documentBundleServiceWSDLLocURL, documentBundleService.getServiceName());
        DocumentBundlePortType documentBundlePortType = service.getPort(DocumentBundlePortType.class);

        LOG.info("Forwarding DocumentBundle to NodePlugin DocumentBundle endpoint:[{}], with user:[{}] and password:[{}]", etxNodeMockProperties.getEtxNodePluginDocumentBundleServiceEndpoint(), etxNodeMockProperties.getEtxNodePluginUser(), etxNodeMockProperties.getEtxNodePluginPassword());

        Map<String, Object> requestContext = ((BindingProvider) documentBundlePortType).getRequestContext();
        requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, etxNodeMockProperties.getEtxNodePluginDocumentBundleServiceEndpoint());
        requestContext.put(BindingProvider.USERNAME_PROPERTY, etxNodeMockProperties.getEtxNodePluginUser());
        requestContext.put(BindingProvider.PASSWORD_PROPERTY, etxNodeMockProperties.getEtxNodePluginPassword());

        documentBundlePortType.submitDocumentBundle(submitDocumentBundleRequest, header);
    }


    class SubmitDocumentBundleResponseBuilder {
        SubmitDocumentBundleResponse submitDocumentBundleResponse;

        public SubmitDocumentBundleResponseBuilder() {
            submitDocumentBundleResponse = new SubmitDocumentBundleResponse();
            submitDocumentBundleResponse.setAck(new AcknowledgmentType());
            submitDocumentBundleResponse.getAck().setAckIndicator(new AckIndicatorType());
            submitDocumentBundleResponse.getAck().setAcknowledgedDocumentReference(new AcknowledgedDocumentReferenceType());
            submitDocumentBundleResponse.getAck().getAcknowledgedDocumentReference().setDocumentReference(new DocumentReferenceType());
            submitDocumentBundleResponse.getAck().getAcknowledgedDocumentReference().getDocumentReference().setID(new IDType());
            submitDocumentBundleResponse.getAck().getAcknowledgedDocumentReference().getDocumentReference().setDocumentTypeCode(new DocumentTypeCodeType());
        }


        public SubmitDocumentBundleResponseBuilder withAckIndicator(boolean ackIndicator) {
            submitDocumentBundleResponse.getAck().getAckIndicator().setValue(ackIndicator);
            return this;
        }

        public SubmitDocumentBundleResponseBuilder withDocumentReferenceId(String documentBundleId) {
            submitDocumentBundleResponse.getAck().getAcknowledgedDocumentReference().getDocumentReference().getID().setValue(documentBundleId);
            return this;
        }

        public SubmitDocumentBundleResponseBuilder withDocumentTypeCode(String documentTypeCode) {
            submitDocumentBundleResponse.getAck().getAcknowledgedDocumentReference().getDocumentReference().getDocumentTypeCode().setValue(documentTypeCode);
            return this;
        }

        public SubmitDocumentBundleResponseBuilder withSenderParty(PartyType senderParty) {
            submitDocumentBundleResponse.getAck().getAcknowledgedDocumentReference().setSenderParty(senderParty);
            return this;
        }

        public SubmitDocumentBundleResponseBuilder withReceiverParties(List<PartyType> receiverParties) {
            submitDocumentBundleResponse.getAck().getAcknowledgedDocumentReference().getReceiverParty().addAll(receiverParties);
            return this;
        }

        public SubmitDocumentBundleResponse getSubmitDocumentBundleResponse() {
            return submitDocumentBundleResponse;
        }
    }
}
package eu.europa.ec.edelivery.etrustexplugin.mock.etrustexnode.applicationresponse;

import ec.schema.xsd.commonaggregatecomponents_2.BusinessHeaderType;
import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.services.wsdl.applicationresponse_2.ApplicationResponsePortType;
import ec.services.wsdl.applicationresponse_2.ApplicationResponseService;
import ec.services.wsdl.applicationresponse_2.SubmitApplicationResponseRequest;
import ec.services.wsdl.documentbundle_2.SubmitDocumentBundleRequest;
import eu.europa.ec.edelivery.etrustexplugin.mock.etrustexnode.application.ETXNodeMockProperties;
import oasis.names.specification.ubl.schema.xsd.applicationresponse_2.ApplicationResponseType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.DocumentReferenceType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.DocumentResponseType;
import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.ResponseType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.unece.cefact.namespaces.standardbusinessdocumentheader.Partner;
import org.unece.cefact.namespaces.standardbusinessdocumentheader.PartnerIdentification;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import java.net.URL;
import java.util.Map;

/**
 * Methods to mock responses from eTrustExNode for <b>LOCAL testing</b>.<br/>
 * For use:<br/>
 * <ol>
 *     <li>In etrustex-node-plugin.properties change:
 *     <ol>
 *         <li>etx.node.services.DocumentBundleService.endpoint to URL of ETX Node mock - http://localhost:8080/etrustexnodemock</li>
 *         <li>etx.node.services.ApplicationResponseService.endpoint to URL of Node Plugin - InboxService ApplicationService</li>
 *         <li>etx.node.services.DocumentWrapperService.endpoint to Wiremock endpoint</li>
 *     </ol>
 *     </li>
 * </ol><br/>
 * <p>
 */
@Service
public class ApplicationResponseServiceMock {

    private static final Logger LOG = LoggerFactory.getLogger(ApplicationResponseServiceMock.class);

    @Autowired
    ETXNodeMockProperties etxNodeMockProperties;

    /**
     * Mocks the _BDL_OK notification from eTrustEx Node.
     *
     * @param outboxSubmitDocumentBundleRequest
     */
    public void triggerApplicationResponseBDLOK(SubmitDocumentBundleRequest outboxSubmitDocumentBundleRequest) {
        try {
            SubmitApplicationResponseRequestBuilder submitApplicationResponseRequestBuilder = new SubmitApplicationResponseRequestBuilder();
            SubmitApplicationResponseRequest bdlOkSubmitApplicationResponseRequest = submitApplicationResponseRequestBuilder
                    .withID(outboxSubmitDocumentBundleRequest.getDocumentBundle().getID())
                    .withIssueDate(outboxSubmitDocumentBundleRequest.getDocumentBundle().getIssueDate())
                    .withResponseCode("BDL:1")
                    .withDescription("Available")
                    .withDocumentReferenceID(outboxSubmitDocumentBundleRequest.getDocumentBundle().getID())
                    .withDocumentReferenceDocumentTypeCode("BDL")
                    .getSubmitApplicationResponseRequest();

            HeaderType bdlOkHeader = submitApplicationResponseRequestBuilder
                    .withSenderHeader(outboxSubmitDocumentBundleRequest.getDocumentBundle().getReceiverParty().get(0).getEndpointID().getValue())
                    .withReceiverHeader(outboxSubmitDocumentBundleRequest.getDocumentBundle().getSenderParty().getEndpointID().getValue())
                    .getHeader();
            Holder<HeaderType> bdlOKHeaderHolder = new Holder<>();
            bdlOKHeaderHolder.value = bdlOkHeader;

            Resource applicationResponseServiceWSDLResource = new ClassPathResource("/node/wsdl/ApplicationResponse-2.0.wsdl", this.getClass());
            URL applicationResponseServiceWSDLLocURL = applicationResponseServiceWSDLResource.getURL();
            LOG.trace("ApplicationResponseServiceWSDLResource getURL:[{}]", applicationResponseServiceWSDLLocURL);

            QName applicationResponseServiceQName = new QName("ec:services:wsdl:ApplicationResponse-2", "ApplicationResponseService");
            ApplicationResponseService applicationResponseService = new ApplicationResponseService(applicationResponseServiceWSDLLocURL, applicationResponseServiceQName);
            javax.xml.ws.Service service = javax.xml.ws.Service.create(applicationResponseServiceWSDLLocURL, applicationResponseService.getServiceName());
            ApplicationResponsePortType applicationResponsePortType = service.getPort(ApplicationResponsePortType.class);

            LOG.info("Invoking NodePlugin ApplicationResponse endpoint:[{}], with user:[{}] and password:[{}]", etxNodeMockProperties.getEtxNodePluginApplicationResponseServiceEndpoint(), etxNodeMockProperties.getEtxNodePluginUser(), etxNodeMockProperties.getEtxNodePluginPassword());

            Map<String, Object> requestContext = ((BindingProvider) applicationResponsePortType).getRequestContext();
            requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, etxNodeMockProperties.getEtxNodePluginApplicationResponseServiceEndpoint());
            requestContext.put(BindingProvider.USERNAME_PROPERTY, etxNodeMockProperties.getEtxNodePluginUser());
            requestContext.put(BindingProvider.PASSWORD_PROPERTY, etxNodeMockProperties.getEtxNodePluginPassword());

            applicationResponsePortType.submitApplicationResponse(bdlOkSubmitApplicationResponseRequest, bdlOKHeaderHolder);
        } catch (Exception e) {
            LOG.error("Faced error while mocking ApplicationResponseRequest BDL OK", e);
        }
    }

    class SubmitApplicationResponseRequestBuilder {
        SubmitApplicationResponseRequest submitApplicationResponseRequest;
        HeaderType headerType;

        public SubmitApplicationResponseRequestBuilder() {
            this.submitApplicationResponseRequest = new SubmitApplicationResponseRequest();
            this.submitApplicationResponseRequest.setApplicationResponse(new ApplicationResponseType());
            this.submitApplicationResponseRequest.getApplicationResponse().setDocumentResponse(new DocumentResponseType());
            this.submitApplicationResponseRequest.getApplicationResponse().getDocumentResponse().setResponse(new ResponseType());
            this.submitApplicationResponseRequest.getApplicationResponse().getDocumentResponse().getResponse().setResponseCode(new ResponseCodeType());
            this.submitApplicationResponseRequest.getApplicationResponse().getDocumentResponse().setDocumentReference(new DocumentReferenceType());

            this.headerType = new HeaderType();
            this.headerType.setBusinessHeader(new BusinessHeaderType());
        }

        public SubmitApplicationResponseRequestBuilder withID(IDType documentBundleID) {
            IDType bdlOKIDType = new IDType();
            bdlOKIDType.setValue(documentBundleID.getValue() + "_BDL_OK");
            submitApplicationResponseRequest.getApplicationResponse().setID(bdlOKIDType);
            return this;
        }

        public SubmitApplicationResponseRequestBuilder withIssueDate(IssueDateType bundleIssueDate) {
            submitApplicationResponseRequest.getApplicationResponse().setIssueDate(bundleIssueDate);
            return this;
        }

        public SubmitApplicationResponseRequestBuilder withResponseCode(String responseCode) {
            submitApplicationResponseRequest.getApplicationResponse().getDocumentResponse().getResponse().getResponseCode().setValue(responseCode);
            return this;
        }

        public SubmitApplicationResponseRequestBuilder withDescription(String description) {
            DescriptionType bdlOKDescription = new DescriptionType();
            bdlOKDescription.setValue(description);
            submitApplicationResponseRequest.getApplicationResponse().getDocumentResponse().getResponse().getDescription().add(bdlOKDescription);
            return this;
        }

        public SubmitApplicationResponseRequestBuilder withDocumentReferenceID(IDType documentBundleID) {
            submitApplicationResponseRequest.getApplicationResponse().getDocumentResponse().getDocumentReference().setID(documentBundleID);
            return this;
        }

        public SubmitApplicationResponseRequestBuilder withDocumentReferenceDocumentTypeCode(String documentTypeCode) {
            DocumentTypeCodeType bdlOKDocumentTypeCodeType = new DocumentTypeCodeType();
            bdlOKDocumentTypeCodeType.setValue(documentTypeCode);
            submitApplicationResponseRequest.getApplicationResponse().getDocumentResponse().getDocumentReference().setDocumentTypeCode(bdlOKDocumentTypeCodeType);
            return this;
        }

        public SubmitApplicationResponseRequest getSubmitApplicationResponseRequest() {
            return submitApplicationResponseRequest;
        }

        public SubmitApplicationResponseRequestBuilder withSenderHeader(String senderParty) {
            Partner senderPartner = new Partner();
            senderPartner.setIdentifier(new PartnerIdentification());
            senderPartner.getIdentifier().setValue(senderParty);
            headerType.getBusinessHeader().getSender().add(senderPartner);
            return this;
        }

        public SubmitApplicationResponseRequestBuilder withReceiverHeader(String receiverParty) {
            Partner receiverPartner = new Partner();
            receiverPartner.setIdentifier(new PartnerIdentification());
            receiverPartner.getIdentifier().setValue(receiverParty);
            headerType.getBusinessHeader().getReceiver().add(receiverPartner);
            return this;
        }

        public HeaderType getHeader() {
            return headerType;
        }

    }
}


package eu.europa.ec.edelivery.etrustexplugin.mock.etrustexnode.application;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.SimpleWsdl11Definition;
import org.springframework.ws.wsdl.wsdl11.Wsdl11Definition;

/**
 * @author Arun Raj
 * @since 1.0
 */
@EnableWs
@Configuration
@PropertySource("classpath:etrustexnode-mock-config.properties")
@ComponentScan(basePackages = "eu.europa.ec.edelivery.etrustexplugin.mock.etrustexnode")
public class WebServiceConfig extends WsConfigurerAdapter {

    @Bean
    public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean<>(servlet, "/etrustexnodemock/*");
    }

    @Bean(name = "DocumentBundleService")
    public Wsdl11Definition defaultWsdl11Definition() throws Exception {
        SimpleWsdl11Definition wsdl11Definition = new SimpleWsdl11Definition();
        wsdl11Definition.setWsdl(new ClassPathResource("/node/wsdl/DocumentBundle-2.0.wsdl")); //your wsdl location
        wsdl11Definition.afterPropertiesSet();
        return wsdl11Definition;
    }
}

package eu.europa.ec.etrustex.common.util;

import ec.schema.xsd.documentbundle_1.DocumentBundleType;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import java.util.Calendar;

public final class CalendarHelper {

    private CalendarHelper() {
    }

    /**
     * It compose a full datetime {@link Calendar} object from a {@link @DateTime} and {@link LocalTime} objects
     *
     * @param jodaDate date info as {@link @DateTime} object
     * @param jodaTime time info as {@link LocalTime} object
     *
     * @return {@link Calendar} object having both date and time info
     */
    public static Calendar getCalendarFromJodaDateTime(DateTime jodaDate, LocalTime jodaTime) {
        if (jodaTime == null) {
            return jodaDate.toGregorianCalendar();
        } else {
            DateTime result = jodaDate
                    .hourOfDay().setCopy(jodaTime.getHourOfDay())
                    .minuteOfHour().setCopy(jodaTime.getMinuteOfHour())
                    .secondOfMinute().setCopy(jodaTime.getSecondOfMinute())
                    .millisOfSecond().setCopy(jodaTime.getMillisOfSecond());
            return result.toGregorianCalendar();
        }
    }

    /**
     *
     * @param documentBundleType {@link DocumentBundleType} cotaining issueDate and issueTime
     * @return {@link Calendar} having both date and time info
     */
    public static Calendar getCalendarIssueDateFromDocumentBundle(DocumentBundleType documentBundleType) {
        if (documentBundleType.getIssueDate() == null) {
            return null;
        }
        return CalendarHelper.getCalendarFromJodaDateTime(documentBundleType.getIssueDate().getValue(),
                documentBundleType.getIssueTime() != null ? documentBundleType.getIssueTime().getValue() : null);
    }

    /**
     * Returns jodatime {@link DateTime} object from {@link Calendar}
     * To keep the compatibility with legacy code we are returning only Date information not time
     *
     * @param date {@link Calendar} object
     * @return {@link DateTime} object having time info striped away
     */
    public static DateTime getJodaDateFromCalendar(Calendar date) {
        return new DateTime(date).withTime(0, 0, 0, 0);
    }

    /**
     * Returns a {@link LocalTime} object from {@link Calendar} having ony time info
     *
     * @param time {@link Calendar} having ony time info
     * @return {@link LocalTime} object
     */
    public static LocalTime getJodaLocalTimeFromCalendar(Calendar time) {
        return LocalDateTime.fromCalendarFields(time).toLocalTime();
    }
}

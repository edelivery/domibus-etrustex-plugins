package eu.europa.ec.etrustex.common.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 * @Author Federico Martini
 * @Since 19/06/2017
 * @Version 1.0
 */
public class SelectableTextMessageCreator extends TextMessageCreator {

    private String selectingProperty;

    private String selectingValue;

    public SelectableTextMessageCreator(String objAsXml, String selectingProp, String selectingVal) {
        super(objAsXml);
        selectingProperty = selectingProp;
        selectingValue = selectingVal;
    }

    @Override
    public Message createMessage(final Session session) throws JMSException {
        TextMessage txtMessage = session.createTextMessage(dto);
        txtMessage.setStringProperty(selectingProperty, selectingValue);
        return txtMessage;
    }

}
package eu.europa.ec.etrustex.common.jaxb;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Custom adapter which extends {@link XmlAdapter} for {@code xsd:time} mapped to {@link LocalTime}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 17/11/2017
 */
public class JodaTimeAdapter extends XmlAdapter<String, LocalTime> {

    //time formatter
    private static final DateTimeFormatter OUT_DTF_TIME = ISODateTimeFormat.hourMinuteSecondMillis();

    @Override
    public LocalTime unmarshal(String s) throws Exception {
        if (s == null) {
            return null;
        }
        return LocalTime.parse(s);
    }

    @Override
    public String marshal(LocalTime lt) throws Exception {
        if (lt == null) {
            return null;
        }
        return OUT_DTF_TIME.print(lt);
    }

}

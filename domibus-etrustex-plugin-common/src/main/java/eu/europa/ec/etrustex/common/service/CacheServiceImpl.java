package eu.europa.ec.etrustex.common.service;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * Implementation for services related to Cache management
 *
 * @author Arun Raj
 * @version 1.0
 * @since 18/07/2017
 */

@Service("EtxPluginCacheService")
public class CacheServiceImpl implements CacheService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(CacheServiceImpl.class);

    protected final CacheManager cacheManager;

    public CacheServiceImpl(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    /**
     * Clears all cache configured in the ehCache
     */
    @Transactional
    public void clearCache() {
        LOG.info("Proceed with clearing cache");
        LOG.debug("Clearing all caches from the cacheManager");
        Collection<String> cacheNames = cacheManager.getCacheNames();
        for (String cacheName : cacheNames) {
            cacheManager.getCache(cacheName).clear();
        }


        LOG.info("cache cleared");
    }

}

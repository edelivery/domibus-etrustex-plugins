package eu.europa.ec.etrustex.common.util.attachment;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * @author micleva
 * @project ETX
 */

public class FileRepositoryUtil {
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(FileRepositoryUtil.class);

    private FileRepositoryUtil() {
        //Do not build
    }

    private static long getAvailableSpace(Path path) {
        long availableSpace = -1;
        try {
            availableSpace = Files.getFileStore(path).getUsableSpace();
        } catch (IOException e) {
            LOG.warn("Failed to retrieve available disk space!", e);
        }
        return availableSpace;
    }

    private static long getTotalSpace(Path path) {
        long totalSpace = -1;
        try {
            totalSpace = Files.getFileStore(path).getTotalSpace();
        } catch (IOException e) {
            LOG.warn("Failed to retrieve total disk space!", e);
        }
        return totalSpace;
    }

    public static String getHumanReadableByteCount(long bytes, boolean useStandardizedUnits) {
        int unit = useStandardizedUnits ? 1000 : 1024;
        String formatted;
        if (bytes < unit) {
            formatted = bytes + " B";
        } else {
            int exp = (int) (Math.log(bytes) / Math.log(unit));
            String pre = (useStandardizedUnits ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (useStandardizedUnits ? "" : "i");
            formatted = String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
        }

        return formatted;
    }

    public static String getHumanReadableStorageInfo(Path path) {
        final String availableSpace = getHumanReadableByteCount(getAvailableSpace(path), true);
        final String totalSpace = getHumanReadableByteCount(getTotalSpace(path), true);

        return "Available disk space: " + availableSpace + " out of total: " + totalSpace;
    }

    public static String getPathContent(Path path) {
        StringBuilder folderStructureBuilder = new StringBuilder();
        try {
            Files.walkFileTree(path, new PrintDirectoryFiles(folderStructureBuilder));
        } catch (IOException e) {
            folderStructureBuilder.append("Unable to read the content of the path: ").append(path).append(e.getMessage());
        }
        return folderStructureBuilder.toString();
    }
}

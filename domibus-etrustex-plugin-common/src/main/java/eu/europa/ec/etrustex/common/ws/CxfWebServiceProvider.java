package eu.europa.ec.etrustex.common.ws;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.Bus;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.configuration.jsse.TLSClientParametersConfig;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.ext.logging.LoggingFeature;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.ConnectionType;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.soap.MTOMFeature;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Service("etxPluginCxfWebServiceProvider")
public class CxfWebServiceProvider {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(CxfWebServiceProvider.class);

    private static final String USER_AGENT = "Domibus ETrustEx Connector";

    private static final int CHUNK_SIZE = 16 * 1024;

    public static final String CLIENT_AUTHENTICATION_XML = "clientauthentication.xml";
    public static final String REGEX_DOMIBUS_CONFIG_LOCATION = "\\Q${domibus.config.location}\\E";
    public static final String DOMIBUS_CONFIG_LOCATION = "domibus.config.location";

    @Autowired
    protected Bus bus;

    @Autowired
    @Qualifier("loggingFeature")
    protected LoggingFeature loggingFeature;

    public void setupConnectionCredentials(BindingProvider bindingProvider, String username, byte[] password) {
        Map<String, Object> contextMap = bindingProvider.getRequestContext();
        contextMap.put(BindingProvider.USERNAME_PROPERTY, username);
        contextMap.put(BindingProvider.PASSWORD_PROPERTY, new String(password));
    }

    public Object buildJaxWsProxy(Class portType, boolean isMtomEnabled, String serviceEndpoint) {
        List<WebServiceFeature> featureList = new ArrayList<>();
        featureList.add(loggingFeature);
        if (isMtomEnabled) {
            featureList.add(new MTOMFeature(CHUNK_SIZE));
        }

        Object proxyObject = ETrustExWsProxyFactoryBean.build(portType, featureList, serviceEndpoint, bus);
        Client wsClient = ClientProxy.getClient(proxyObject);

        if (serviceEndpoint != null && serviceEndpoint.startsWith("https://")) {
            getClientAuthenticationPath()
                    .map(this::getTlsClientParameters)
                    .ifPresent(((HTTPConduit) wsClient.getConduit())::setTlsClientParameters);
        }

        Map<String, List<String>> headers = new HashMap<>();
        headers.put("User-Agent", Collections.singletonList(USER_AGENT));
        wsClient.getRequestContext().put(Message.PROTOCOL_HEADERS, headers);

        disableKeepAlive(wsClient);

        if (isMtomEnabled) {
            enableChunking(wsClient);
        } else {
            disableChunking(wsClient);
        }
        return proxyObject;
    }

    // TODO: 25/10/2019 FGA to be removed after the implementation of EDELIVERY-4842
    protected TLSClientParameters getTlsClientParameters(Path path) {
        try {
            byte[] encoded = Files.readAllBytes(path);
            String config = new String(encoded, StandardCharsets.UTF_8);
            //TODO this replacement should be extracted into a service method
            config = config.replaceAll(REGEX_DOMIBUS_CONFIG_LOCATION, getPropertyDomibusConfigLocation().replace('\\', '/'));

            return (TLSClientParameters) TLSClientParametersConfig.createTLSClientParameters(config);
        } catch (Exception e) {
            LOG.warn("Mutual authentication will not be supported for [{}]", path);
            LOG.trace("", e);
            return null;
        }
    }

    protected String getPropertyDomibusConfigLocation() {
        String property = System.getProperty(DOMIBUS_CONFIG_LOCATION);
        if(StringUtils.isBlank(property)){
            return StringUtils.EMPTY;
        }
        return property;
    }

    protected Optional<Path> getClientAuthenticationPath() {
        String domainSpecificFileName = CLIENT_AUTHENTICATION_XML;
        Path domainSpecificPath = Paths.get(getPropertyDomibusConfigLocation(), domainSpecificFileName);
        boolean domainSpecificPathExists = Files.exists(domainSpecificPath);
        LOG.debug("Client authentication file [{}] exists at the domain specific path [{}]: [{}]", domainSpecificFileName, domainSpecificPath, domainSpecificPathExists);
        if (domainSpecificPathExists) {
            return Optional.of(domainSpecificPath);
        }
        return Optional.empty();
    }

    private void disableKeepAlive(Client wsClient) {
        HTTPClientPolicy httpClientPolicy = getHttpClientPolicy(wsClient);
        httpClientPolicy.setConnection(ConnectionType.CLOSE);
    }

    private void enableChunking(Client endpointClient) {
        HTTPClientPolicy httpClientPolicy = getHttpClientPolicy(endpointClient);
        httpClientPolicy.setAllowChunking(true);
        httpClientPolicy.setChunkingThreshold(CHUNK_SIZE);
        httpClientPolicy.setChunkLength(CHUNK_SIZE);
    }

    private void disableChunking(Client endpointClient) {
        HTTPClientPolicy httpClientPolicy = getHttpClientPolicy(endpointClient);
        httpClientPolicy.setAllowChunking(false);
    }

    protected HTTPClientPolicy getHttpClientPolicy(Client endpointClient) {
        HTTPConduit bundleHttp = (HTTPConduit) endpointClient.getConduit();
        HTTPClientPolicy httpClientPolicy = bundleHttp.getClient();
        if (httpClientPolicy == null) {
            httpClientPolicy = new HTTPClientPolicy();
            bundleHttp.setClient(httpClientPolicy);
        }
        return httpClientPolicy;
    }
}

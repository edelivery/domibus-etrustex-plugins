package eu.europa.ec.etrustex.common.service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

/**
 * It's responsible for writing and deleting attachments from payloads folder of the plugin
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 05/10/2017
 */
public interface WorkspaceService {

    Path writeFile(Long attachmentId, InputStream inputStream) throws IOException;

    Path getFile(Long attachmentId);

    Path getRoot();

    void deleteFile(Long attachmentId);

    void loadConfiguration();
}

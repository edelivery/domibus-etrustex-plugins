package eu.europa.ec.etrustex.common.jms;

import eu.domibus.messaging.MessageConstants;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageConverter;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * @Author François Gautier
 * @Since 13/07/2017
 * @Version 1.0
 */
public class TextMessageConverter implements MessageConverter {

    private String domain;

    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Override
    public Message toMessage(Object object, Session session) throws JMSException, MessageConversionException {
        TextMessage textMessage = session.createTextMessage(TransformerUtils.serializeObjToXML(object));
        textMessage.setStringProperty(MessageConstants.DOMAIN , domain);
        return textMessage;
    }

    @Override
    public Object fromMessage(Message message) throws JMSException, MessageConversionException {
        TextMessage msg = (TextMessage) message;
        if (msg.getText() == null) {
            /*
            When Domibus pushes notification to the eTrustEx plugin notification queues,
            in case of an error with the notification queue consumer then after retries it comes to this segment.
             */
            if (isNotBlank(messageIdIsNotNull(message))) {
                return new ErrorQueueMessage(messageIdIsNotNull(message));
            } else {
                return null;
            }
        }
        return TransformerUtils.deserializeXMLToObj(msg.getText());
    }

    private String messageIdIsNotNull(Message queueMessage) {
        try {
            return queueMessage.getStringProperty(MessageConstants.MESSAGE_ID);
        } catch (JMSException e) {
            return null;
        }
    }
}
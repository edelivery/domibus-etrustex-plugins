package eu.europa.ec.etrustex.common.converters.node;

import ec.schema.xsd.commonaggregatecomponents_2.DocumentReferenceRequestType;
import eu.europa.ec.etrustex.common.model.entity.attachment.AttachmentType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DocumentTypeCodeType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IDType;

/**
 * @author: micleva
 * @date: 6/14/12 4:14 PM
 * @project: ETX
 */
public class DocumentReferenceRequestTypeBuilder {

    private DocumentReferenceRequestTypeBuilder() {
    }

    public static DocumentReferenceRequestType buildDocumentReferenceRequest(String attachmentUUID, AttachmentType attachmentType) {
        DocumentReferenceRequestType sdo = new DocumentReferenceRequestType();

        IDType id = new IDType();
        id.setValue(attachmentUUID);
        sdo.setID(id);

        DocumentTypeCodeType documentTypeCode = new DocumentTypeCodeType();
        documentTypeCode.setValue(attachmentType.name());
        sdo.setDocumentTypeCode(documentTypeCode);

        return sdo;
    }
}

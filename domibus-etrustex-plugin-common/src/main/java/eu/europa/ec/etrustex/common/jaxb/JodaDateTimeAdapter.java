package eu.europa.ec.etrustex.common.jaxb;

import org.joda.time.DateTime;
import org.joda.time.format.*;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Custom adapter which extends {@link XmlAdapter} for {@code xsd:dateTime} mapped to {@link DateTime}
 * <p>
 * <b>Important:</b>Marshal method will strip out (not print) Timezone info - preserving legacy code behavior
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 17/11/2017
 */
public class JodaDateTimeAdapter extends XmlAdapter<String, DateTime> {

    //date time formatter
    private static final DateTimeFormatter OUT_DTF_DATETIME = ISODateTimeFormat.dateHourMinuteSecondMillis();

    //Chooses the date parser with the best match.
    private static DateTimeFormatter formatter = new DateTimeFormatterBuilder()
            .append(null, new DateTimeParser[]{
                    ISODateTimeFormat.dateHourMinuteSecondMillis().getParser(),
                    ISODateTimeFormat.dateParser().getParser(),
                    ISODateTimeFormat.dateTimeParser().getParser()
            }).toFormatter();

    @Override
    public DateTime unmarshal(String s) {
        if (s == null) {
            return null;
        }
        return formatter.parseDateTime(s);
    }

    @Override
    public String marshal(DateTime dt) {
        if (dt == null) {
            return null;
        }
        return OUT_DTF_DATETIME.print(dt);
    }

}

package eu.europa.ec.etrustex.common.util;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.springframework.stereotype.Service;

import static eu.europa.ec.etrustex.common.util.LoggingUtils.*;

/**
 * Service responsible for cleaning the MDC keys after the execution of a method.
 * The MDC keys are cleaned only if before the method execution were not already set.
 *
 * @author Cosmin Baciu
 * @since 3.3
 */
@Service
public class ClearEtxLogKeysService {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ClearEtxLogKeysService.class);

    String[] getDefaultLogKeys() {
        return new String[]{MDC_ETX_MESSAGE_UUID, MDC_ETX_ATT_UUID, MDC_ETX_CONVERSATION_ID};
    }


    public void clearKeys(String ... additionalKeys) {

        for (String key : getDefaultLogKeys()) {
            LOG.removeMDC(key);
        }
        for (String key : additionalKeys){
            LOG.removeMDC(key);
        }
    }

    public void clearSpecificKeys(String ... specificKeys){
        for (String key : specificKeys){
            LOG.removeMDC(key);
        }
    }

}
package eu.europa.ec.etrustex.common.converters.node;

import org.unece.cefact.namespaces.standardbusinessdocumentheader.Partner;
import org.unece.cefact.namespaces.standardbusinessdocumentheader.PartnerIdentification;

public class NodePartnerConverter {

    private NodePartnerConverter() {
    }

    public static Partner buildPartnerType(String partyName) {
        Partner partnerSdo = null;

        if (partyName != null) {
            partnerSdo = new Partner();
            PartnerIdentification partnerIdentification = new PartnerIdentification();
            partnerIdentification.setValue(partyName);
            partnerSdo.setIdentifier(partnerIdentification);

        }

        return partnerSdo;
    }

}

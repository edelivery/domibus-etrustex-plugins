package eu.europa.ec.etrustex.common.util;

import org.apache.commons.collections4.CollectionUtils;
import org.unece.cefact.namespaces.standardbusinessdocumentheader.Partner;
import org.unece.cefact.namespaces.standardbusinessdocumentheader.PartnerIdentification;

import java.util.List;

/**
 * Utility class to extract Value from Objects in the {@link ec.schema.xsd.commonaggregatecomponents_2.HeaderType}
 *
 * @author François Gautier
 * @version 1.0
 * @since 04-Dec-17
 */
public class HeaderUtils {

    private HeaderUtils() {
    }

    /**
     * Get the first value of the {@link Partner} list
     * @param partnerList list of {@link Partner}
     * @return value of the first {@link PartnerIdentification}
     */
    public static String extractValidateParty(List<Partner> partnerList) {
        if (CollectionUtils.isNotEmpty(partnerList)) {
            return partnerList.get(0).getIdentifier().getValue();
        }
        return null;
    }
}

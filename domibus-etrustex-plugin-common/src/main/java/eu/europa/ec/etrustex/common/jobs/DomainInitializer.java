package eu.europa.ec.etrustex.common.jobs;

public interface DomainInitializer {

    void init();
}

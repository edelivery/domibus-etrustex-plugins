package eu.europa.ec.etrustex.common.exceptions;

import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.DescriptionType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.ResponseCodeType;
import oasis.names.specification.ubl.schema.xsd.fault_1.FaultType;

/**
 * Helper class which converts from {@code ETrustExPluginException} to:
 * {@code ErrorType} and {@code FaultType} webservice fault objects
 *
 * @author Catalin Enache
 * @version 2.0
 * @since 02/10/2017
 */
public class ErrorTypeConverter {

    /**
     * private constructor. all methods are static
     */
    private ErrorTypeConverter() {
    }

    /**
     * It builds an {@code FaultType} object for webservices from {@code ETrustExPluginException}
     *
     * @param epEx
     * @return
     */
    public static FaultType buildFaultType(ETrustExPluginException epEx) {
        return buildFaultType(epEx.getError().getCode(), epEx.getError().getDescription());
    }

    /**
     * It builds an {@code FaultType} object for webservices
     *
     * @param errorCode
     * @param description
     * @return
     */
    public static FaultType buildFaultType(final String errorCode, final String description) {
        final FaultType error = new FaultType();

        //error code
        final ResponseCodeType responseCodeType = new ResponseCodeType();
        responseCodeType.setValue(errorCode);
        error.setResponseCode(responseCodeType);

        //description
        final DescriptionType descriptionType = new DescriptionType();
        descriptionType.setValue(description);

        error.getDescription().add(descriptionType);


        return error;
    }

}

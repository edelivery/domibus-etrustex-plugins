package eu.europa.ec.etrustex.common.service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

/**
 * It's responsible for writing and deleting attachments from payloads folder
 *
 * @author François Gautier
 * @version 1.0
 * @since 21/12/2017
 */
public interface WorkspaceFileService {

    void writeLargeFile(Path path, InputStream lis) throws IOException;

    Path getFile(String filePath, String fileName) throws IOException;

    Path getFile(String fileName);

    Path getRoot();

    void deleteFile(Path filePath);
}

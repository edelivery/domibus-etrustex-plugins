package eu.europa.ec.etrustex.common.xstream;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class JodaLocalTimeConverter extends AbstractSingleValueConverter {

    private static final DateTimeFormatter FORMATTER = ISODateTimeFormat.hourMinuteSecondMillis();

    @Override
    public boolean canConvert(Class type) {
        return LocalTime.class.isAssignableFrom(type);
    }

    @Override
    public LocalTime fromString(String s) {
        if (s == null) {
            return null;
        }
        return LocalTime.parse(s);
    }

    @Override
    public String toString(Object dt) {
        if (dt == null) {
            return null;
        }
        return FORMATTER.print((LocalTime) dt);
    }

}

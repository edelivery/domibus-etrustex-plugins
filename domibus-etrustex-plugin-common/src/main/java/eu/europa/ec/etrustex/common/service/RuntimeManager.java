package eu.europa.ec.etrustex.common.service;

/**
 * Façade to combine several runtime admin operations
 *
 * @author Arun Raj
 * @version 1.0
 * @since 18/07/2017
 */
public interface RuntimeManager {
    /**
     * Performs cleanup for all available cache regions.
     */
    void resetCache();
}

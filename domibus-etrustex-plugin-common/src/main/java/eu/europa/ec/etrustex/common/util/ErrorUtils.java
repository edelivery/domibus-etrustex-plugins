package eu.europa.ec.etrustex.common.util;

import eu.domibus.common.ErrorResult;
import eu.domibus.common.MessageReceiveFailureEvent;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Utils class meant for common error handling between Node and backend plugins.
 *
 * @author Arun Raj
 * @version 1.0
 * @since 22/08/2017
 */
public class ErrorUtils {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ErrorUtils.class);

    private ErrorUtils() {
    }

    /**
     * To log the details of a message receive failure event.
     *
     * @param messageReceiveFailureEvent
     * @param connectorClassName
     */
    public static final void logMessageReceiveFailureEventString(MessageReceiveFailureEvent messageReceiveFailureEvent, String connectorClassName) {
        ErrorResult errorResult = messageReceiveFailureEvent.getErrorResult();
        String logMsgRcvFailed = new ToStringBuilder(messageReceiveFailureEvent, ToStringStyle.MULTI_LINE_STYLE)
                .append("messageId", messageReceiveFailureEvent.getMessageId())
                .append("endpoint", messageReceiveFailureEvent.getEndpoint())
                .append(
                        new ToStringBuilder(errorResult, ToStringStyle.MULTI_LINE_STYLE)
                                .append("mshRole", errorResult.getMshRole())
                                .append("messageInErrorId", errorResult.getMessageInErrorId())
                                .append("errorCode", errorResult.getErrorCode())
                                .append("errorDetail", errorResult.getErrorDetail())
                                .append("timestamp", errorResult.getTimestamp())
                                .append("notified", errorResult.getNotified())
                                .toString()
                ).toString();
        LOG.error("Message Receive Failed for [{}]: {}", connectorClassName, logMsgRcvFailed);
    }
}

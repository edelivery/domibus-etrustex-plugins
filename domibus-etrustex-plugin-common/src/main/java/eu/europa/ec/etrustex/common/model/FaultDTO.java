package eu.europa.ec.etrustex.common.model;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.util.AS4MessageConstants;
import eu.europa.ec.etrustex.common.util.ContentId;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.PrintWriter;
import java.io.StringWriter;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

/**
 * DTO to handle critical errors and report it to the backend plugin.
 *
 * @author Federico Martini
 * @version 1.0
 * @since 03 July 2017
 */
public class FaultDTO extends PayloadDTO {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(FaultDTO.class);

    private static final long serialVersionUID = -5491562420920618651L;

    public static final String FAULT_ACTION = "FaultAction";

    ETrustExError error;

    private Throwable throwable;

    public FaultDTO(ETrustExError error, Throwable t) {
        super(ContentId.CONTENT_FAULT.getValue(), getStackTraceError(t).getBytes(), AS4MessageConstants.MIME_TYPE_APP_OCTET, false, FAULT_ACTION);
        this.error = error;
        throwable = t;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }

    public static String getStackTraceError(Throwable t) {
        if(t == null){
            return StringUtils.EMPTY;
        }
        StringWriter sw = new StringWriter();
        PrintWriter ps = new PrintWriter(sw);
        t.printStackTrace(ps);
        String s = sw.toString();
        LOG.debug(s);
        return s;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(error)
                .append(throwable)
                .toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        FaultDTO that = (FaultDTO) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(error, that.error)
                .append(getStackTraceError(throwable), getStackTraceError(that.throwable))
                .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .append("error", error)
                .append("throwable", getStackTraceError(throwable))
                .toString();
    }

}

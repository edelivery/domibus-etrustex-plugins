package eu.europa.ec.etrustex.common.handlers;

import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;

/**
 * Operation handler interface
 *
 * @author Federico Martini
 * @since 24/03/2017
 * @version 1.0
 */
public interface OperationHandler {

    boolean handle(ETrustExAdapterDTO dto) throws ETrustExPluginException;
}

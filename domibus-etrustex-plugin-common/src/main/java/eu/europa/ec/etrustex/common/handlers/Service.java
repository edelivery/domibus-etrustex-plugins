package eu.europa.ec.etrustex.common.handlers;

/**
 * Enum class to easily switch in the Operation handler factory.
 *
 * It maps the corresponding values from PMode file
 *
 * @author Federico Martini, Catalin Enache
 * @version 1.0
 * @since 23/03/2017
 */
public enum Service {

    /* outbox services start */
    APPLICATION_RESPONSE_SERVICE("ApplicationResponseService"),
    DOCUMENT_WRAPPER_SERVICE("DocumentWrapperService"),
    DOCUMENT_BUNDLE_SERVICE("DocumentBundleService"),
    RETRIEVE_ICA_SERVICE("RetrieveICAService"),
    /* outbox services end */

    /* inbox services start */
    BUNDLE_TRANSMISSION_SERVICE("InboxBundleTransmissionService"),
    WRAPPER_TRANSMISSION_SERVICE("WrapperTransmissionService"),
    STATUS_MESSAGE_TRANSMISSION_SERVICE("StatusMessageTransmissionService"),
    /* inbox services end */

    UNKNOWN(" ");

    public final String code;

    public final String getCode() {
        return this.code;
    }

    Service(String code) {
        this.code = code;
    }

    public static Service fromString(String code) {
        if (code != null) {
            for (Service value : Service.values()) {
                if (code.equalsIgnoreCase(value.code)) {
                    return value;
                }
            }
        }
        // Default
        return UNKNOWN;
    }

}

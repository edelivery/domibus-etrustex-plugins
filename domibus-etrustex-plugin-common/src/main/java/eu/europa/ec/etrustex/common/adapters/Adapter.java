package eu.europa.ec.etrustex.common.adapters;

import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;

/**
 * @Author Federico Martini
 * @Since 28/03/2017
 * @Version 1.0
 */
public interface Adapter {

    void performOperation(ETrustExAdapterDTO dto) throws ETrustExPluginException;
}

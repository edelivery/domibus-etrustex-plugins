package eu.europa.ec.etrustex.common.service;

/**
 * Interface to hold the operations related to Cache management
 *
 * @author Arun Raj
 * @version 1.0
 * @since 18/07/2017
 */
public interface CacheService {
    /**
     * Clears all L2 cache objects for all available CacheManagers
     */
    void clearCache();
}

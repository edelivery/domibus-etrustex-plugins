package eu.europa.ec.etrustex.common.exceptions;

/**
 * Class to handle ETrustEx plugin exceptions.
 *
 * @author Federico Martini
 * @version 1.0
 * @see ETrustExError
 * @since 24/03/2017
 */
public class ETrustExPluginException extends RuntimeException {

    private static final long serialVersionUID = -222708353849437412L;

    private ETrustExError error;

    /**
     * Constructs a new ETrustExPluginException with a specific error and message.
     *
     * @param dce     a ETrustExError.
     * @param message the message detail. It is saved for later retrieval by the {@link #getMessage()} method.
     */
    public ETrustExPluginException(ETrustExError dce, String message) {
        super(message);
        error = dce;
    }

    /**
     * Constructs a new ETrustExPluginException with a specific error, message and cause.
     *
     * @param dce     a ETrustExError.
     * @param message the message detail. It is saved for later retrieval by the {@link #getMessage()} method.
     * @param cause   the cause of the exceptions.
     */
    public ETrustExPluginException(ETrustExError dce, String message, Throwable cause) {
        super(message, cause);
        error = dce;
    }

    public ETrustExError getError() {
        return error;
    }

    public void setError(ETrustExError error) {
        this.error = error;
    }

}

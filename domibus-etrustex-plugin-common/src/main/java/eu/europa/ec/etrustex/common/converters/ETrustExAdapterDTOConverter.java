package eu.europa.ec.etrustex.common.converters;

import eu.domibus.ext.domain.PartyIdDTO;
import eu.domibus.ext.domain.PropertyDTO;
import eu.domibus.ext.domain.UserMessageDTO;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import static eu.europa.ec.etrustex.common.util.AS4MessageConstants.*;

/**
 * Convert {@link UserMessageDTO} into {@link ETrustExAdapterDTO}
 *
 * @author François Gautier
 * @version 1.0
 * @since 29-Nov-17
 */
public class ETrustExAdapterDTOConverter {

    private ETrustExAdapterDTOConverter() {
    }

    /**
     * Convert {@link UserMessageDTO} into {@link ETrustExAdapterDTO}
     *
     * The payload and the the property {@link eu.europa.ec.etrustex.common.util.AS4MessageConstants#PROPERTY_FILE_NAME}
     * are NOT populated in the returned instance
     *
     * @param userMessageDTO the meta data of a Domibus message
     * @return instance of {@link ETrustExAdapterDTO}
     */
    public static ETrustExAdapterDTO convertToETrustExAdapterDTO(UserMessageDTO userMessageDTO) {
        ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        if (userMessageDTO.getCollaborationInfo() != null) {
            eTrustExAdapterDTO.setAs4Action(userMessageDTO.getCollaborationInfo().getAction());
            if (userMessageDTO.getCollaborationInfo().getAgreementRef() != null) {
                eTrustExAdapterDTO.setAs4AgreementRef(userMessageDTO.getCollaborationInfo().getAgreementRef().getValue());
            }
            eTrustExAdapterDTO.setAs4ConversationId(userMessageDTO.getCollaborationInfo().getConversationId());
            if (userMessageDTO.getCollaborationInfo().getService() != null) {
                eTrustExAdapterDTO.setAs4Service(userMessageDTO.getCollaborationInfo().getService().getValue());
                eTrustExAdapterDTO.setAs4ServiceType(userMessageDTO.getCollaborationInfo().getService().getType());
            }
        }

        if (userMessageDTO.getMessageInfo() != null) {
            eTrustExAdapterDTO.setAs4MessageId(userMessageDTO.getMessageInfo().getMessageId());
            eTrustExAdapterDTO.setAs4RefToMessageId(userMessageDTO.getMessageInfo().getRefToMessageId());
        }

        if (userMessageDTO.getPartyInfo() != null) {
            if (userMessageDTO.getPartyInfo().getTo() != null) {
                eTrustExAdapterDTO.setAs4ToPartyId(getOnlyElement(userMessageDTO.getPartyInfo().getTo().getPartyId(), PartyIdDTO::getValue));
                eTrustExAdapterDTO.setAs4ToPartyIdType(getOnlyElement(userMessageDTO.getPartyInfo().getTo().getPartyId(), PartyIdDTO::getType));
                eTrustExAdapterDTO.setAs4ToPartyRole(userMessageDTO.getPartyInfo().getTo().getRole());
            }
            if (userMessageDTO.getPartyInfo().getFrom() != null) {
                eTrustExAdapterDTO.setAs4FromPartyId(getOnlyElement(userMessageDTO.getPartyInfo().getFrom().getPartyId(), PartyIdDTO::getValue));
                eTrustExAdapterDTO.setAs4FromPartyIdType(getOnlyElement(userMessageDTO.getPartyInfo().getFrom().getPartyId(), PartyIdDTO::getType));
                eTrustExAdapterDTO.setAs4FromPartyRole(userMessageDTO.getPartyInfo().getFrom().getRole());
            }
        }

        if (userMessageDTO.getMessageProperties() != null) {
            Map<String, String> properties = getMap(userMessageDTO.getMessageProperties().getProperty());
            eTrustExAdapterDTO.setAttachmentId(properties.get(PROPERTY_ETRUSTEX_ATTACHMENT_UUIDS));
            eTrustExAdapterDTO.setAs4FinalRecipient(properties.get(PROPERTY_FINAL_RECIPIENT));
            eTrustExAdapterDTO.setAs4OriginalSender(properties.get(PROPERTY_ORIGINAL_SENDER));
            eTrustExAdapterDTO.setEtxBackendMessageId(properties.get(PROPERTY_ETRUSTEX_BACKEND_MESSAGE_ID));
        }
        return eTrustExAdapterDTO;
    }

    private static <T> String getOnlyElement(Set<T> partyId, Function<T, String> function) {
        return partyId.stream().findFirst().map(function).orElse(StringUtils.EMPTY);
    }

    private static Map<String, String> getMap(Set<PropertyDTO> property) {
        Map<String, String> properties= new HashMap<>();
        for (PropertyDTO propertyDTO : property) {
            properties.put(propertyDTO.getName(), propertyDTO.getValue());
        }
        return properties;
    }
}

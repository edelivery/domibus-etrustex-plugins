package eu.europa.ec.etrustex.common.exceptions;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * @author François Gautier
 * @version 1.0
 * @since 28/03/2018
 */
public interface EtrustExMarker {
    Marker NON_RECOVERABLE = MarkerFactory.getMarker("NON_RECOVERABLE");
}

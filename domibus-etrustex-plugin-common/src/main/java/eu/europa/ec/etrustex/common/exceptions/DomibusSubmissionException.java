package eu.europa.ec.etrustex.common.exceptions;

/**
 * Class to handle exceptions thrown during Domibus Submission.
 *
 * @author Arun Raj
 * @version 1.0
 * @since 06/03/2018
 */
public class DomibusSubmissionException extends RuntimeException {


    private static final long serialVersionUID = -439666915501597030L;

    /**
     * As Domibus exceptions are always technical in nature, the error field will always be {@link ETrustExError#DEFAULT}
     */
    private static final ETrustExError error = ETrustExError.DEFAULT;;

    /**
     * Wraps the exceptions thrown by Domibus to a DomibusSubmissionException.
     *
     * @param message
     * @param cause
     */
    public DomibusSubmissionException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Get method to be in line with ETrustExPluginException handling
     * @return
     */
    public static ETrustExError getError() {
        return error;
    }
}

package eu.europa.ec.etrustex.common.jms;

import org.springframework.jms.core.MessageCreator;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

/**
 * @Author Federico Martini
 * @Since 27/03/2017
 * @Version 1.0
 */
public class TextMessageCreator implements MessageCreator {

    /* XML representation of a DTO object */
    protected String dto;

    public TextMessageCreator(String dtoAsXml) {
        this.dto = dtoAsXml;
    }

    @Override
    public Message createMessage(final Session session) throws JMSException {
        return session.createTextMessage(dto);
    }
}
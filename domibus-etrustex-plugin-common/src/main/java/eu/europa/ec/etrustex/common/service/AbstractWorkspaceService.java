package eu.europa.ec.etrustex.common.service;

import eu.domibus.ext.delegate.services.multitenancy.DomainTaskExecutorExtDelegate;
import eu.domibus.ext.services.DomainContextExtService;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.util.Validate;
import eu.europa.ec.etrustex.common.util.WorkSpaceUtils;
import eu.europa.ec.etrustex.common.util.attachment.FileRepositoryUtil;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * @author François Gautier
 * @version 1.0
 * @since 19-Dec-17
 */
public abstract class AbstractWorkspaceService implements WorkspaceService, WorkspaceFileService {
    protected static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(AbstractWorkspaceService.class);
    /**
     * folder where the payloads are saved - it could be symbolic too
     */
    private Path workspaceRoot = null;

    @Autowired
    protected DomainTaskExecutorExtDelegate executor;
    @Autowired
    protected DomainContextExtService domainContextExtService;

    @PostConstruct
    @Override
    public void loadConfiguration() {
        LOG.info("Loading Workspace configuration");
        workspaceRoot = getRoot();

        LOG.info("Workspace folder fully initialized on path: {}; {}",
                workspaceRoot.toAbsolutePath(),
                FileRepositoryUtil.getHumanReadableStorageInfo(workspaceRoot));
        LOG.info("Workspace folder content is: " + FileRepositoryUtil.getPathContent(workspaceRoot));
    }

    public abstract Path getRoot();

    public abstract int getSleepMillis();

    public abstract int getTimeOut();

    @Override
    public Path getFile(Long attachmentId) {
        return getFile(attachmentId.toString());
    }

    @Override
    public Path getFile(String fileName) {
        return workspaceRoot.resolve(fileName);
    }

    @Override
    public Path writeFile(Long attachmentId, InputStream is) throws IOException {
        Path file = getFile(attachmentId);

        Files.copy(is, file, StandardCopyOption.REPLACE_EXISTING);
        LOG.info("Written {} bytes for attachment [ID: {}]", Files.size(file), attachmentId);

        return file;
    }

    @Override
    public void writeLargeFile(Path path, InputStream lis) throws IOException {
        Validate.notNull(path, "File cannot be null");
        try (OutputStream fileOutputStream = new FileOutputStream(path.toFile())) {
            IOUtils.copyLarge(lis, fileOutputStream);
        }
    }

    @Override
    public void deleteFile(Long attachmentId) {
        deleteFile(getFile(attachmentId));
    }

    /**
     * Delete file specified in the file path. If file is not found in the specified path, then ignore without any exception.
     * IOException maybe encountered due to file lock, in which case request is submitted to DomibusTasExecutor for server aware execution.
     */
    @Override
    public void deleteFile(Path filePath) {
        try {
            del(filePath);
        } catch (IOException e) {
            executor.submitLongRunningTask(() -> {
                int i = 0;
                while (!filePath.toFile().renameTo(filePath.toFile()) && i <= getTimeOut() * 10) {
                    try {
                        Thread.sleep(getSleepMillis());
                        i++;
                    } catch (InterruptedException ex) {
                        LOG.error("Delete file: {}", filePath.getFileName(), ex);
                        Thread.currentThread().interrupt();
                        break;
                    }
                }
                if (i > 1) {
                    LOG.info("Waiting for {} times", i);
                }
                try {
                    del(filePath);
                } catch (IOException ex) {
                    LOG.error("Still couldn't delete the file even after {}s timeout: {}", getTimeOut(), filePath.getFileName(), ex);
                }
            }, domainContextExtService.getCurrentDomain());
        }
    }

    private boolean del(Path filePath) throws IOException {
        boolean isDeleted = Files.deleteIfExists(filePath);
        if (!isDeleted && !filePath.toFile().exists()) {
            LOG.debug("Failed to delete file because no file was present in path: {}", filePath.toAbsolutePath());
        } else if (!isDeleted) {
            LOG.warn("Failed to delete file because no file was present in path: {}", filePath.toAbsolutePath());
        } else {
            LOG.info("Workspace file was deleted: {}", filePath.toAbsolutePath());
        }

        return isDeleted;
    }

    @Override
    public Path getFile(String filePath, String fileName) {
        Path folder;
        if (filePath != null) {
            folder = Paths.get(filePath).normalize().toAbsolutePath();
        } else {
            folder = workspaceRoot;
        }
        Path path = WorkSpaceUtils.getPath(folder);

        LOG.debug("Workspace path is: {}", path);

        path = path.resolve(fileName);
        LOG.debug("File path is: {}", path.toAbsolutePath());
        return path;
    }
}

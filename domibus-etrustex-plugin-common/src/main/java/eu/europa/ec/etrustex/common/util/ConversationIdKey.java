package eu.europa.ec.etrustex.common.util;

/**
 * Keys to populate the AS4 Conversation ID.
 *
 * @author Arun Raj
 * @version 1.0
 * @since 30/08/2017
 */
public enum ConversationIdKey {


    OUTBOX_SEND_MESSAGE_STATUS("OUTBOX_STATUS_MESSAGE_ID_"),
    OUTBOX_SEND_MESSAGE_BUNDLE("OUTBOX_BUNDLE_MESSAGE_ID_"),
    OUTBOX_SEND_ATTACHMENT("OUTBOX_ATTACHMENT_ID_"),
    RETRIEVE_ICA("RICA_"),
    INBOX_MESSAGEBUNDLE("INBOX_BUNDLE_MESSAGE_UUID_"),
    INBOX_ATTACHMENT("INBOX_ATTACHMENT_UUID_"),
    INBOX_MESSAGE_STATUS("INBOX_STATUS_MESSAGE_UUID_");


    private String conversationIdKey;

    ConversationIdKey(String conversationIdKey) {
        this.conversationIdKey = conversationIdKey;
    }

    public String getConversationIdKey() {
        return conversationIdKey;
    }
}

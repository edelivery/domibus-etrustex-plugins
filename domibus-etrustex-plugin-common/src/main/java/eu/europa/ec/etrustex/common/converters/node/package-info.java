/**
 * This package contains classes used by both backend and nod eplugin to build objects used for Node WS calls
 * @author Catalin Enache
 * @verson 1.0
 * @since 04/10/2017
 */
package eu.europa.ec.etrustex.common.converters.node;
package eu.europa.ec.etrustex.common.converters;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.domibus.plugin.Submission;
import eu.domibus.plugin.transformer.MessageRetrievalTransformer;
import eu.domibus.plugin.transformer.MessageSubmissionTransformer;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.LargePayloadDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.util.AS4MessageConstants;
import eu.europa.ec.etrustex.common.util.ContentId;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import static eu.europa.ec.etrustex.common.util.AS4MessageConstants.*;

/**
 * ETrustExAdapterDTO <--> Submission converter class.
 *
 * @author Arun Raj, Federico Martini
 * @version 1.0
 * @since 03-Mar-2017
 */
public class ETrustExAdapterDTOSubmissionConverter implements MessageSubmissionTransformer<ETrustExAdapterDTO>, MessageRetrievalTransformer<ETrustExAdapterDTO> {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ETrustExAdapterDTOSubmissionConverter.class);

    /**
     * Converts the AS4 data from the Domibus internal Submission class to the DTO of the eTrustEx plugin(s).
     *
     * @param submission Domibus class {@link Submission}
     * @param eTrustExAdapterDTO EtrustEx plugin class {@link ETrustExAdapterDTO}
     * @return ETrustExAdapterDTO
     */
    @Override
    public ETrustExAdapterDTO transformFromSubmission(Submission submission, ETrustExAdapterDTO eTrustExAdapterDTO) {

        // MessageInfo
        eTrustExAdapterDTO.setAs4MessageId(submission.getMessageId());
        eTrustExAdapterDTO.setAs4RefToMessageId(submission.getRefToMessageId());
        eTrustExAdapterDTO.setAs4ConversationId(submission.getConversationId());

        // From Party (Only last party considered)
        for (Submission.Party as4FromParty : submission.getFromParties()) {
            eTrustExAdapterDTO.setAs4FromPartyId(as4FromParty.getPartyId());
            eTrustExAdapterDTO.setAs4FromPartyIdType(as4FromParty.getPartyIdType());
        }
        eTrustExAdapterDTO.setAs4FromPartyRole(submission.getFromRole());

        // To Party (Only last party considered)
        for (Submission.Party as4ToParty : submission.getToParties()) {
            eTrustExAdapterDTO.setAs4ToPartyId(as4ToParty.getPartyId());
            eTrustExAdapterDTO.setAs4ToPartyIdType(as4ToParty.getPartyIdType());
        }
        eTrustExAdapterDTO.setAs4ToPartyRole(submission.getToRole());

        // Collaboration Info
        eTrustExAdapterDTO.setAs4AgreementRef(submission.getAgreementRef());
        eTrustExAdapterDTO.setAs4Service(submission.getService());
        eTrustExAdapterDTO.setAs4ServiceType(submission.getServiceType());
        eTrustExAdapterDTO.setAs4Action(submission.getAction());

        for (Submission.Payload submissionPayload : submission.getPayloads()) {
            String description = (null != submissionPayload.getDescription()) ? submissionPayload.getDescription().getValue() : StringUtils.EMPTY;
            Locale locale = (null != submissionPayload.getDescription()) ? submissionPayload.getDescription().getLang() : Locale.getDefault();
            PayloadDTO etxPayloadDTO;
            //extracting the mime type
            String mimeType = extractMimeTypeFromPayload(submission.getMessageId(), submissionPayload);

            // If the payload is "cid:wrapper" we need a large payload DTO!
            if (ContentId.CONTENT_ID_DOCUMENT.getValue().equals(submissionPayload.getContentId())) {
                etxPayloadDTO = new LargePayloadDTO(submissionPayload.getContentId(), mimeType, submissionPayload.getPayloadDatahandler(), description, locale, null);
            } else {
                etxPayloadDTO = new PayloadDTO(submissionPayload.getContentId(), submissionPayload.getPayloadDatahandler(), mimeType, submissionPayload.isInBody(), description, locale);
            }
            eTrustExAdapterDTO.addAs4Payload(etxPayloadDTO);
        }

        // This must be done after the payloads are converted since some properties can regard the payload objects.
        convertMessageProperties(submission.getMessageProperties(), eTrustExAdapterDTO);

        return eTrustExAdapterDTO;
    }

    /**
     * Logic to extract the MimeType from Domibus Payload.<br/>
     * MimeType will be looked up from the {@link Submission.Payload#getPayloadProperties()}.<br/>
     * In case the {@link AS4MessageConstants#MIME_TYPE} was missing from the payload properties, the mime type will be looked up from the data handler as a fallback.<br/>
     *
     * @param domibusMsgId      The AS4 message id of Domibus
     * @param submissionPayload The Payload instance from Domibus
     * @return the mime type extracted
     */
    private String extractMimeTypeFromPayload(String domibusMsgId, Submission.Payload submissionPayload) {
        String mimeType = null;
        if (CollectionUtils.isNotEmpty(submissionPayload.getPayloadProperties())) {
            for (Submission.TypedProperty submissionProperty : submissionPayload.getPayloadProperties()) {
                if (StringUtils.equalsIgnoreCase(AS4MessageConstants.MIME_TYPE, submissionProperty.getKey())) {
                    mimeType = submissionProperty.getValue();
                    break;
                }
            }
        }
        if (mimeType == null) {
            LOG.warn("For payload cid:[{}], MimeType property was not found. Fetching from DataHandler.", submissionPayload.getContentId());
            mimeType = submissionPayload.getPayloadDatahandler().getContentType();
        }
        return mimeType;
    }

    /**
     * Load the AS4 message property set to the DTO of the eTrustEx plugins
     *
     * @param messageProperties {@link Collection} of {@link Submission.TypedProperty}
     * @param dto {@link ETrustExAdapterDTO}
     */
    private void convertMessageProperties(Collection<Submission.TypedProperty> messageProperties, ETrustExAdapterDTO dto) {

        for (Submission.TypedProperty propertyEntry : messageProperties) {
            if (propertyEntry.getKey().equals(PROPERTY_FINAL_RECIPIENT)) {
                dto.setAs4FinalRecipient(propertyEntry.getValue());
            }
            if (propertyEntry.getKey().equals(PROPERTY_ORIGINAL_SENDER)) {
                dto.setAs4OriginalSender(propertyEntry.getValue());
            }
            if (propertyEntry.getKey().equals(PROPERTY_ETRUSTEX_BACKEND_MESSAGE_ID)) {
                dto.setEtxBackendMessageId(propertyEntry.getValue());
            }
            if (propertyEntry.getKey().equals(PROPERTY_FILE_NAME)) {
                LargePayloadDTO largePayloadDto = (LargePayloadDTO) dto.getPayloadFromCID(ContentId.CONTENT_ID_DOCUMENT);
                largePayloadDto.setFileName(propertyEntry.getValue());
            }
            if (propertyEntry.getKey().equals(PROPERTY_ETRUSTEX_ATTACHMENT_UUIDS)) {
                dto.setAttachmentId(propertyEntry.getValue());
            }
        }
    }

    /**
     * Transform the DTO of the eTrustEx Plugin(s) to the Domibus internal Submission object.
     *
     * @param eTrustExAdapterDTO {@link ETrustExAdapterDTO}
     * @return Submission object of Domibus
     */
    @Override
    public Submission transformToSubmission(ETrustExAdapterDTO eTrustExAdapterDTO) {

        Submission submission = new Submission();

        // MessageInfo
        submission.setMessageId(eTrustExAdapterDTO.getAs4MessageId());
        if (StringUtils.isNotBlank(eTrustExAdapterDTO.getAs4RefToMessageId())) {
            submission.setRefToMessageId(eTrustExAdapterDTO.getAs4RefToMessageId());
        }
        if (StringUtils.isNotBlank(eTrustExAdapterDTO.getAs4ConversationId())) {
            submission.setConversationId(eTrustExAdapterDTO.getAs4ConversationId());
        }

        // Adding From Party
        submission.addFromParty(eTrustExAdapterDTO.getAs4FromPartyId(), eTrustExAdapterDTO.getAs4FromPartyIdType());
        submission.setFromRole(eTrustExAdapterDTO.getAs4FromPartyRole());

        // Adding To Party
        submission.addToParty(eTrustExAdapterDTO.getAs4ToPartyId(), eTrustExAdapterDTO.getAs4ToPartyIdType());
        submission.setToRole(eTrustExAdapterDTO.getAs4ToPartyRole());

        // CollaborationInfo
        submission.setAgreementRef(eTrustExAdapterDTO.getAs4AgreementRef());
        submission.setService(eTrustExAdapterDTO.getAs4Service());
        submission.setServiceType(eTrustExAdapterDTO.getAs4ServiceType());
        submission.setAction(eTrustExAdapterDTO.getAs4Action());

        // Message Property
        if (StringUtils.isNotBlank(eTrustExAdapterDTO.getAs4OriginalSender())) {
            submission.addMessageProperty(PROPERTY_ORIGINAL_SENDER, eTrustExAdapterDTO.getAs4OriginalSender());
        }
        if (StringUtils.isNotBlank(eTrustExAdapterDTO.getAs4FinalRecipient())) {
            submission.addMessageProperty(PROPERTY_FINAL_RECIPIENT, eTrustExAdapterDTO.getAs4FinalRecipient());
        }
        LargePayloadDTO largePayloadDto = (LargePayloadDTO) eTrustExAdapterDTO.getPayloadFromCID(ContentId.CONTENT_ID_DOCUMENT);
        if (largePayloadDto != null) {
            submission.addMessageProperty(PROPERTY_FILE_NAME, largePayloadDto.getFileName());
            if (StringUtils.isNotBlank(eTrustExAdapterDTO.getAttachmentId())) {
                submission.addMessageProperty(PROPERTY_ETRUSTEX_ATTACHMENT_UUIDS, eTrustExAdapterDTO.getAttachmentId());
            }
        }
        if (StringUtils.isNotBlank(eTrustExAdapterDTO.getEtxBackendMessageId())) {
            submission.addMessageProperty(PROPERTY_ETRUSTEX_BACKEND_MESSAGE_ID, eTrustExAdapterDTO.getEtxBackendMessageId());
        }


        // Populating the payloads
        for (PayloadDTO etxPayloadDTO : eTrustExAdapterDTO.getAs4Payloads()) {

            Collection<Submission.TypedProperty> etxAS4GenericPayloadProperties = new ArrayList<>();
            Submission.TypedProperty typedProperty = new Submission.TypedProperty(AS4MessageConstants.MIME_TYPE, etxPayloadDTO.getMimeType(), etxPayloadDTO.getContentId());
            etxAS4GenericPayloadProperties.add(typedProperty);

            Submission.Description description = new Submission.Description(etxPayloadDTO.getLocale(), etxPayloadDTO.getDescription());
            submission.addPayload(etxPayloadDTO.getContentId(), etxPayloadDTO.getPayloadDataHandler(), etxAS4GenericPayloadProperties, etxPayloadDTO.isInBody(), description, null);
        }
        return submission;
    }
}

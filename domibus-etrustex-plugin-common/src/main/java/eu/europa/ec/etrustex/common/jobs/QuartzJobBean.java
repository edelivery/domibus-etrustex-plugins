package eu.europa.ec.etrustex.common.jobs;

import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.quartz.DomibusQuartzJobExtBean;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import static eu.europa.ec.etrustex.common.exceptions.EtrustExMarker.NON_RECOVERABLE;

/**
 * We need to manually set the classloader for QuartzJobs because Domibus is not loading the plugin classloader one.
 * Therefore the TypedQuery cannot be used.
 * <p>
 * It should be fixed with Domibus 4.2
 *
 * @see <a href="https://ec.europa.eu/cefdigital/tracker/browse/EDELIVERY-4072">[EDELIVERY-4072] DOMIBUS 4.2</a>
 * @see <a href="https://ec.europa.eu/cefdigital/tracker/browse/EDELIVERY-4192">[EDELIVERY-4192] Rest Resources and Quartzjob are NOT loading PluginClassLoader</a>
 */
public abstract class QuartzJobBean extends DomibusQuartzJobExtBean {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(QuartzJobBean.class);

    @Override
    protected final void executeJob(JobExecutionContext jobExecutionContext, DomainDTO domainDTO) throws JobExecutionException {
        LOG.debug("At Job Name:{}, DomainDTO:{}", getJobName(jobExecutionContext), domainDTO.toString());
        try {
            if (isAllowedToRun(domainDTO.getCode())) {
                getDomainInitializer().init();
                Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());
                process(jobExecutionContext);
            } else {
                LOG.debug("Job {} is not allowed to run on the domain {}", getJobName(jobExecutionContext), domainDTO);
            }
        } catch (Throwable e) {
            LOG.error(NON_RECOVERABLE, "Job could not be ran {} [{}] [{}]", getJobName(jobExecutionContext), domainDTO, this.getClass(), e);
            throw e;
        }
    }

    private String getJobName(JobExecutionContext jobExecutionContext) {
        if (jobExecutionContext == null ||
                jobExecutionContext.getJobDetail() == null ||
                jobExecutionContext.getJobDetail().getKey() == null) {
            return null;
        }
        return jobExecutionContext.getJobDetail().getKey().getName();
    }

    protected abstract DomainInitializer getDomainInitializer();

    protected abstract boolean isAllowedToRun(String domainName);

    public abstract void process(JobExecutionContext context) throws JobExecutionException;
}

package eu.europa.ec.etrustex.common.model;

import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.activation.DataHandler;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Locale;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

/**
 * This class is used to hold the payload data.
 * This is to be used only for small and medium payloads.
 *
 * @author Federico Martini, Arun Raj
 * @version 1.0
 * @since 07-Apr-2017
 */
public class PayloadDTO implements Serializable {

    private static final long serialVersionUID = 3667476271524812949L;

    private String contentId;
    private String description;
    private Locale locale;
    private String mimeType;
    private boolean inBody;
    private byte[] payload;


    public PayloadDTO(String contentId, byte[] payload, String mimeType, boolean inBody, String description) {
        if (payload == null) {
            throw new IllegalArgumentException("payload data must not be null");
        } else {
            this.contentId = contentId;
            this.payload = payload;
            this.description = description;
            this.mimeType = mimeType;
            this.inBody = inBody;
            this.locale = Locale.getDefault();
        }
    }

    /**
     * This should be used only for small and medium payloads.
     *
     */
    public PayloadDTO(String contentId, DataHandler payloadDataHandler, String mimeType, boolean inBody, String description, Locale locale) {
        if (payloadDataHandler == null) {
            throw new IllegalArgumentException("payload Data Handler must not be null");
        } else {

            try (InputStream is = payloadDataHandler.getDataSource().getInputStream()) {
                this.contentId = contentId;
                this.payload = IOUtils.toByteArray(is);
                this.description = description;
                this.mimeType = mimeType;
                this.inBody = inBody;
                this.locale = locale;
            } catch (IOException e) {
                throw new ETrustExPluginException(ETrustExError.DEFAULT, "Failure while adding the payload!", e);
            }
        }
    }

    /**
     * To be used only by subclasses which should hold the reference to the payload.
     *
     */
    protected PayloadDTO(String contentId, String mimeType, boolean inBody, String description, Locale locale) {
        this.contentId = contentId;
        this.description = description;
        this.mimeType = mimeType;
        this.inBody = inBody;
        this.locale = locale;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public boolean isInBody() {
        return inBody;
    }

    public void setInBody(boolean inBody) {
        this.inBody = inBody;
    }

    public byte[] getPayload() {
        return payload;
    }

    public void setPayload(byte[] payload) {
        this.payload = payload;
    }

    public String getPayloadAsString() {
        return new String(getPayload());
    }

    public DataHandler getPayloadDataHandler() {
        return new DataHandler(new ByteArrayDataSource(payload, mimeType));
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PayloadDTO that = (PayloadDTO) o;

        return new EqualsBuilder()
                .append(contentId, that.contentId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(contentId)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .append("contentId", contentId)
                .append("description", description)
                .append("locale", locale)
                .append("mimeType", mimeType)
                .append("inBody", inBody)
                .append("payload", payload)
                .toString();
    }
}

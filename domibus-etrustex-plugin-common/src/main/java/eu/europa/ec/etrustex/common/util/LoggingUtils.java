package eu.europa.ec.etrustex.common.util;

public class LoggingUtils {
    public static final String MDC_ETX_MESSAGE_UUID = "etxMessageUuid";
    public static final String MDC_ETX_ATT_UUID = "etxAttUuid";
    public static final String MDC_ETX_CONVERSATION_ID = "etxConversationId";


    private LoggingUtils() {
        //do not instantiate
    }
}

package eu.europa.ec.etrustex.common.model.entity.attachment;

/**
 * @author Arun Raj
 * @since September 2017
 * @version 1.0
 */
public enum AttachmentType {
    BINARY, METADATA
}

package eu.europa.ec.etrustex.common.jaxb;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Custom adapter which extends {@link XmlAdapter} for {@code xsd:date} mapped to {@link DateTime}
 * <p>
 * <b>Important:</b> Marshal method will strip out (not print) Timezone info - preserving legacy code behavior
 * <p>
 * For adapter compatibility we should accept also full date+time+timezone format as input
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 17/11/2017
 */
public class JodaDateAdapter extends XmlAdapter<String, DateTime> {

    //date input formatters
    private static final DateTimeFormatter[] IN_DTF_DATE_FORMATTERS = {
            DateTimeFormat.forPattern("yyyy-MM-ddZ"),
            DateTimeFormat.forPattern("yyyy-MM-dd")
    };

    //date formatter out
    private static final DateTimeFormatter OUT_DTF_DATE = ISODateTimeFormat.date();

    @Override
    public DateTime unmarshal(String s) throws Exception {
        if (s == null) {
            return null;
        }
        //this is mapped to xsd:date with or without timezone
        for (DateTimeFormatter dateTimeFormatter : IN_DTF_DATE_FORMATTERS) {
            try {
                return dateTimeFormatter.parseDateTime(s);
            } catch (IllegalArgumentException e) {
                // ignore
            }
        }

        //for adapter compatibility we should accept also full date+time+timezone format
        return DateTime.parse(s);
    }

    @Override
    public String marshal(DateTime dt) throws Exception {
        if (dt == null) {
            return null;
        }
        return OUT_DTF_DATE.print(dt);
    }

}

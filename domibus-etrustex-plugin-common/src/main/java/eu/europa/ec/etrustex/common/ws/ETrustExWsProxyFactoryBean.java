package eu.europa.ec.etrustex.common.ws;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.jaxws.support.JaxWsServiceFactoryBean;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceFeature;
import java.util.List;

public class ETrustExWsProxyFactoryBean {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ETrustExWsProxyFactoryBean.class);

    private ETrustExWsProxyFactoryBean() {
        //cannot be build
    }

    public static Object build(Class portType, List<WebServiceFeature> wsFeatures, String serviceEndpoint, Bus bus) {

        LOG.debug("Building the JAX-WS proxy for service [{}]", portType);

        JaxWsProxyFactoryBean proxyFactory = new JaxWsProxyFactoryBean();
        proxyFactory.setServiceClass(portType);
        proxyFactory.setAddress(serviceEndpoint);

        proxyFactory.setBus(bus);

        if (wsFeatures != null) {
            JaxWsServiceFactoryBean serviceFactoryBean = (JaxWsServiceFactoryBean) proxyFactory.getServiceFactory();
            serviceFactoryBean.setWsFeatures(wsFeatures);
        }

        Object proxyObject = proxyFactory.create();

        // for info read: http://cxf.apache.org/faq.html#FAQ-AreJAX-WSclientproxiesthreadsafe?
        // ICA service has different user/pass for parties
        ((BindingProvider) proxyObject).getRequestContext().put("thread.local.request.context", "true");
        ((BindingProvider) proxyObject).getRequestContext().put("schema-validation-enabled", "false");
        ((BindingProvider) proxyObject).getRequestContext().put("set-jaxb-validation-event-handler", "false");

        return proxyObject;
    }

}

package eu.europa.ec.etrustex.common.handlers;

/**
 * Enum class to easily switch in the Operation handler factory.
 *
 * It maps the corresponding values from PMode file
 *
 * @author Federico Martini, Catalin Enache
 * @since 23/03/2017
 * @version 1.0
 */
public enum Action {

    /* outbox services start */
    SUBMIT_APPLICATION_RESPONSE_REQUEST("SubmitApplicationResponseRequest"),
    SUBMIT_APPLICATION_RESPONSE_RESPONSE("SubmitApplicationResponseResponse"),
    STORE_DOCUMENT_WRAPPER_REQUEST("StoreDocumentWrapperRequest"),
    STORE_DOCUMENT_WRAPPER_RESPONSE("StoreDocumentWrapperResponse"),
    SUBMIT_DOCUMENT_BUNDLE_REQUEST("SubmitDocumentBundleRequest"),
    SUBMIT_DOCUMENT_BUNDLE_RESPONSE("SubmitDocumentBundleResponse"),
    RETRIEVE_ICA_REQUEST("RetrieveICARequest"),
    RETRIEVE_ICA_RESPONSE("RetrieveICAResponse"),
    /* outbox services end */

    /* inbox services start */
    TRANSMIT_WRAPPER_TO_BACKEND("TransmitWrapperToBackend"),
    STATUS_MESSAGE_TRANSMISSION("StatusMessageTransmission"),
    NOTIFY_BUNDLE_TO_BACKEND_REQUEST("NotifyInboxBundle"),
    NOTIFY_BUNDLE_TO_BACKEND_RESPONSE("AckInboxBundleNotification"),
    /* inbox services end */

    FAULT_ACTION("FaultAction"),

    UNKNOWN(" ");

    public final String code;

    public final String getCode() {
        return this.code;
    }

    Action(String code) {
        this.code = code;
    }

    public static Action fromString(String code) {
        if (code != null) {
            for (Action value : Action.values()) {
                if (code.equalsIgnoreCase(value.code)) {
                    return value;
                }
            }
        }
        // Default
        return UNKNOWN;
    }
}

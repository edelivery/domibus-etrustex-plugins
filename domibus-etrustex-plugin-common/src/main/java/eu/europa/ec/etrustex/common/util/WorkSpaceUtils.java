package eu.europa.ec.etrustex.common.util;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * @author François Gautier
 * @version 1.0
 * @since 05-Jan-18
 */
public class WorkSpaceUtils {

    protected static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(WorkSpaceUtils.class);

    private WorkSpaceUtils() {
        //do not build
    }

    public static Path getPath(Path workspaceRoot) {
        if (Files.notExists(workspaceRoot)) {
            try {
                workspaceRoot = Files.createDirectories(workspaceRoot);
            } catch (IOException e) {
                LOG.error("Failed to create workspaceRoot folder: " + workspaceRoot.toAbsolutePath().toString(), e);
                throw new IllegalStateException("Failed to create workspaceRoot workspace", e);
            }
            LOG.info("Workspace workspaceRoot folder created after was missing");
        } else {
            LOG.info("Workspace folder initialized on path: {}", workspaceRoot.toAbsolutePath());
            while (Files.isSymbolicLink(workspaceRoot)) {
                try {
                    String rootFolderBuilder = "symbolic link: " + workspaceRoot;
                    workspaceRoot = Files.readSymbolicLink(workspaceRoot);
                    rootFolderBuilder += " -> " + workspaceRoot;
                    LOG.info(rootFolderBuilder);
                } catch (IOException e) {
                    LOG.warn("Unable to read the content of the path: " + workspaceRoot, e);
                }
            }
        }
        return workspaceRoot;
    }
}

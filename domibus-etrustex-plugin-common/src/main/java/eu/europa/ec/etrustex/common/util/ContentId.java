package eu.europa.ec.etrustex.common.util;

public enum ContentId {

    CONTENT_ID_GENERIC("cid:generic"),
    CONTENT_ID_HEADER("cid:header"),
    CONTENT_ID_DOCUMENT("cid:wrapper"),
    CONTENT_ID_FAULT_RESPONSE("cid:faultResponse"),
    CONTENT_FAULT("cid:fault"),
    ;

    private String value;

    ContentId(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}

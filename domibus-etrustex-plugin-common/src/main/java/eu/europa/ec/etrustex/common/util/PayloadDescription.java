package eu.europa.ec.etrustex.common.util;

public enum PayloadDescription {

    ETRUSTEX_GENERIC_PAYLOAD("eTrustExGenericPayload"),
    ETRUSTEX_HEADER_PAYLOAD("eTrustExHeaderPayload"),
    ETRUSTEX_FAULT_PAYLOAD("etxAS4FaultPayload"),
    ETRUSTEX_FAULT_RESPONSE_PAYLOAD("eTrustExFaultResponsePayload");

    private String value;

    PayloadDescription(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

package eu.europa.ec.etrustex.common.util;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Utility class to handle Files functions
 *
 * @author François Gautier
 * @version 1.0
 * @since 18-Jan-18
 */
public class FileUtils {

    private static final DomibusLogger logger = DomibusLoggerFactory.getLogger(FileUtils.class);

    private FileUtils() {
    }

    /**
     * Walk through the root directory and its sub directory for file/directory to delete
     * see {@link FileUtils#deleteIfOld(java.nio.file.Path, long)}
     *
     * @param dir     {@link Path} of a given root directory
     * @param ttlInMs time to live to evaluate
     */
    public static void deleteOldFilesInDirectory(Path dir, final long ttlInMs) {
        logger.debug("working on dir {}", dir);
        try {
            Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                    logger.debug("File found: {}", file);
                    deleteIfOld(file, ttlInMs);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            logger.warn("Failed to delete old files in directory: " + dir.toAbsolutePath().toString(), e);
        }
    }

    /**
     * Delete a file if {@param ttlInMs} > last time the file was modified
     *
     * @param file    {@link Path} of a given file
     * @param ttlInMs time to live to evaluate
     */
    public static void deleteIfOld(Path file, long ttlInMs) {
        try {
            final BasicFileAttributes fileAttributes = Files.getFileAttributeView(file, BasicFileAttributeView.class).readAttributes();
            long lifespan = System.currentTimeMillis() - fileAttributes.lastModifiedTime().toMillis();

            if (lifespan >= ttlInMs) {
                Files.delete(file);
                logger.info("Deleted: {}", file);
            } else {
                logger.debug("Not deleted: {} | lastModified: {} | ttlInMs: {}", file, lifespan, ttlInMs);
            }
        } catch (IOException e) {
            logger.error("Failed to delete: {}", file, e);
        }
    }
}

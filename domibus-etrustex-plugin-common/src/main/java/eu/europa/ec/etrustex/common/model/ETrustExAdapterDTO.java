package eu.europa.ec.etrustex.common.model;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.util.ContentId;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_FAULT;

/**
 * @author Arun Raj, Martini Federico
 * @version 1.0
 * @since March 2017
 */
public class ETrustExAdapterDTO implements Serializable {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(ETrustExAdapterDTO.class);

    private static final long serialVersionUID = 4292035361339077577L;

    /*AS4 Header elements start*/
    private String as4MessageId;
    private String as4RefToMessageId;
    private String as4ConversationId;

    //AS4 From Party Details
    private String as4FromPartyId;
    private String as4FromPartyIdType;
    private String as4FromPartyRole;

    //AS4 To Party Details
    private String as4ToPartyId;
    private String as4ToPartyIdType;
    private String as4ToPartyRole;

    //AS4 Collaboration Info
    private String as4AgreementRef;
    private String as4Service;
    private String as4ServiceType;
    private String as4Action;

    //Message Properties
    private String as4OriginalSender;
    private String as4FinalRecipient;
    private String eTxBackendMessageId;
    private String attachmentId;
    /*AS4 Header elements end*/

    private Collection<PayloadDTO> as4Payloads;

    /**
     * If the message used referenced another domibus, return the reference
     * Otherwise, return the domibus id of the message used.
     *
     * @param eTrustExAdapterResponseDTO of the message used
     * @return domibus id
     */
    public static String getUuid(ETrustExAdapterDTO eTrustExAdapterResponseDTO) {
        if(eTrustExAdapterResponseDTO == null){
            return StringUtils.EMPTY;
        }
        String as4RefToMessageId = eTrustExAdapterResponseDTO.getAs4RefToMessageId();
        if(StringUtils.isBlank(as4RefToMessageId)){
            return eTrustExAdapterResponseDTO.getAs4MessageId();
        }
        return as4RefToMessageId;
    }

    public ETrustExAdapterDTO invert() {

        ETrustExAdapterDTO dto = new ETrustExAdapterDTO();

        dto.setAs4RefToMessageId(getAs4MessageId());
        dto.setAs4ConversationId(getAs4ConversationId());
        dto.setAs4Service(getAs4Service());
        dto.setAs4ServiceType(getAs4ServiceType());
        dto.setAs4AgreementRef(getAs4AgreementRef());

        dto.setAs4OriginalSender(getAs4FinalRecipient());
        dto.setAs4FinalRecipient(getAs4OriginalSender());
        dto.setEtxBackendMessageId(getEtxBackendMessageId());

        dto.setAs4FromPartyId(getAs4ToPartyId());
        dto.setAs4FromPartyIdType(getAs4ToPartyIdType());
        dto.setAs4FromPartyRole(getAs4ToPartyRole());

        dto.setAs4ToPartyId(getAs4FromPartyId());
        dto.setAs4ToPartyIdType(getAs4FromPartyIdType());
        dto.setAs4ToPartyRole(getAs4FromPartyRole());

        return dto;
    }

    public String getAs4MessageId() {
        return as4MessageId;
    }

    public void setAs4MessageId(String as4MessageId) {
        this.as4MessageId = as4MessageId;
    }

    public String getAs4RefToMessageId() {
        return as4RefToMessageId;
    }

    public void setAs4RefToMessageId(String as4RefToMessageId) {
        this.as4RefToMessageId = as4RefToMessageId;
    }

    public String getAs4ConversationId() {
        return as4ConversationId;
    }

    public void setAs4ConversationId(String as4ConversationId) {
        this.as4ConversationId = as4ConversationId;
    }

    public String getAs4FromPartyId() {
        return as4FromPartyId;
    }

    public void setAs4FromPartyId(String as4FromPartyId) {
        this.as4FromPartyId = as4FromPartyId;
    }

    public String getAs4FromPartyIdType() {
        return as4FromPartyIdType;
    }

    public void setAs4FromPartyIdType(String as4FromPartyIdType) {
        this.as4FromPartyIdType = as4FromPartyIdType;
    }

    public String getAs4FromPartyRole() {
        return as4FromPartyRole;
    }

    public void setAs4FromPartyRole(String as4FromPartyRole) {
        this.as4FromPartyRole = as4FromPartyRole;
    }

    public String getAs4ToPartyId() {
        return as4ToPartyId;
    }

    public void setAs4ToPartyId(String as4ToPartyId) {
        this.as4ToPartyId = as4ToPartyId;
    }

    public String getAs4ToPartyIdType() {
        return as4ToPartyIdType;
    }

    public void setAs4ToPartyIdType(String as4ToPartyIdType) {
        this.as4ToPartyIdType = as4ToPartyIdType;
    }

    public String getAs4ToPartyRole() {
        return as4ToPartyRole;
    }

    public void setAs4ToPartyRole(String as4ToPartyRole) {
        this.as4ToPartyRole = as4ToPartyRole;
    }

    public String getAs4AgreementRef() {
        return as4AgreementRef;
    }

    public void setAs4AgreementRef(String as4AgreementRef) {
        this.as4AgreementRef = as4AgreementRef;
    }

    public String getAs4Service() {
        return as4Service;
    }

    public void setAs4Service(String as4Service) {
        this.as4Service = as4Service;
    }

    public String getAs4ServiceType() {
        return as4ServiceType;
    }

    public void setAs4ServiceType(String as4ServiceType) {
        this.as4ServiceType = as4ServiceType;
    }

    public String getAs4Action() {
        return as4Action;
    }

    public void setAs4Action(String as4Action) {
        this.as4Action = as4Action;
    }

    public String getAs4OriginalSender() {
        return as4OriginalSender;
    }

    public void setAs4OriginalSender(String as4OriginalSender) {
        this.as4OriginalSender = as4OriginalSender;
    }

    public String getAs4FinalRecipient() {
        return as4FinalRecipient;
    }

    public void setAs4FinalRecipient(String as4FinalRecipient) {
        this.as4FinalRecipient = as4FinalRecipient;
    }

    public String getEtxBackendMessageId() {
        return eTxBackendMessageId;
    }

    public void setEtxBackendMessageId(String eTxBackendMessageId) {
        this.eTxBackendMessageId = eTxBackendMessageId;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }

    public Collection<PayloadDTO> getAs4Payloads() {
        if (as4Payloads == null) {
            as4Payloads = new HashSet<>();
        }
        return as4Payloads;
    }

    public void addAs4Payload(PayloadDTO payload) {
        if (null == as4Payloads) {
            as4Payloads = new HashSet<>();
        }
        as4Payloads.add(payload);
    }

    public PayloadDTO getPayloadFromCID(ContentId contentId) {
        if (as4Payloads != null) {
            for (PayloadDTO as4Payload : as4Payloads) {
                if (contentId.getValue().equals(as4Payload.getContentId())) {
                    return as4Payload;
                }
            }
        }
        return null;
    }

    public boolean hasFaultPayload() {
        return (null != getPayloadFromCID(CONTENT_FAULT));
    }

    public void clearFaultPayloads() {
        Iterator<PayloadDTO> i = as4Payloads.iterator();
        while (i.hasNext()) {
            PayloadDTO as4Payload = i.next();
            if (CONTENT_FAULT.getValue().equals(as4Payload.getContentId())) {
                i.remove();
            }
        }
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("as4MessageId", as4MessageId)
                .append("as4RefToMessageId", as4RefToMessageId)
                .append("as4ConversationId", as4ConversationId)
                .append("as4FromPartyId", as4FromPartyId)
                .append("as4FromPartyIdType", as4FromPartyIdType)
                .append("as4FromPartyRole", as4FromPartyRole)
                .append("as4ToPartyId", as4ToPartyId)
                .append("as4ToPartyIdType", as4ToPartyIdType)
                .append("as4ToPartyRole", as4ToPartyRole)
                .append("as4AgreementRef", as4AgreementRef)
                .append("as4Service", as4Service)
                .append("as4ServiceType", as4ServiceType)
                .append("as4Action", as4Action)
                .append("as4OriginalSender", as4OriginalSender)
                .append("as4FinalRecipient", as4FinalRecipient)
                .append("eTxBackendMessageId", eTxBackendMessageId)
                .append("as4Payloads", as4Payloads)
                .toString();
    }
}

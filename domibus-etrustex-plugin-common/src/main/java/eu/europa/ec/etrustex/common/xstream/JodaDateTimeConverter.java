package eu.europa.ec.etrustex.common.xstream;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class JodaDateTimeConverter extends AbstractSingleValueConverter {

    private static final DateTimeFormatter FORMATTER = ISODateTimeFormat.dateTime();

    @Override
    public boolean canConvert(Class type) {
        return DateTime.class.isAssignableFrom(type);
    }

    @Override
    public DateTime fromString(String s) {
        if (s == null) {
            return null;
        }
        return FORMATTER.parseDateTime(s);
    }

    @Override
    public String toString(Object dt) {
        if (dt == null) {
            return null;
        }
        return FORMATTER.print((DateTime) dt);
    }

}

package eu.europa.ec.etrustex.common.model;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import java.io.File;
import java.util.Locale;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

/**
 * This DTO class is used to hold the payload data of the large attachments.
 *
 * @author Federico Martini
 * @version 2.0
 * @since Dec-2017
 */
public class LargePayloadDTO extends PayloadDTO {

    public static final String PAYLOAD_SUFFIX = ".payload";
    public static final String UNKNOWN_PAYLOAD = "unknown.payload";
    private static final long serialVersionUID = -5544770213076879737L;
    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(LargePayloadDTO.class);
    private String filePath;
    private String fileName;
    private String fileUID;

    private transient DataHandler largePayload;

    /**
     * Constructor for <code>LargePayloadDTO</code> object
     */
    public LargePayloadDTO(String contentId, String mimeType, DataHandler dataHandler, String description, Locale locale, String fileName) {
        this(contentId, mimeType, dataHandler, description, locale, fileName, null);
    }

    /**
     * Constructor for <code>LargePayloadDTO</code> object
     */
    public LargePayloadDTO(String contentId, String mimeType, DataHandler dataHandler, String description, Locale locale, String fileName, String filePath) {
        super(contentId, mimeType, false, description, locale);
        if (StringUtils.isNotEmpty(fileName)) {
            this.filePath = filePath;
            this.fileName = fileName;
        } else {
            LOG.debug("File was not set");
        }
        this.largePayload = dataHandler;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileUID() {
        return fileUID;
    }

    public void setFileUID(String fileUID) {
        this.fileUID = fileUID;
    }

    @Override
    public DataHandler getPayloadDataHandler() {
        return largePayload;
    }

    /**
     * To be used in order to initialize the data handler.
     *
     * @param file the file containing the payload to be loaded
     */
    public void loadPayloadFromFile(File file) {
        if (file != null) {
            try {
                largePayload = new DataHandler(new FileDataSource(file));
            } catch (Exception ex) {
                String message = "Could not load payload from file [" + file.getAbsolutePath() + "]";
                LOG.error(message);
                throw new ETrustExPluginException(ETrustExError.DEFAULT, message, ex);
            }
        }
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
                .append("filePath", filePath)
                .append("fileName", fileName)
                .append("fileUID", fileUID)
                .toString();
    }

}

package eu.europa.ec.etrustex.common.util;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.LargePayloadDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.xstream.JodaDateTimeConverter;
import eu.europa.ec.etrustex.common.xstream.JodaLocalTimeConverter;
import org.apache.commons.lang3.StringUtils;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.util.Locale;

import static eu.europa.ec.etrustex.common.util.AS4MessageConstants.MIME_TYPE_TEXT_XML;
import static eu.europa.ec.etrustex.common.util.ContentId.*;
import static eu.europa.ec.etrustex.common.util.PayloadDescription.*;
import static org.apache.commons.codec.binary.StringUtils.getBytesUtf8;
import static org.apache.commons.codec.binary.StringUtils.newStringUtf8;

/**
 * Utility class for transformation from Xml to Objects and viceversa.
 *
 * @author Arun Raj, Federico Martini
 * @version 1.0
 */
public final class TransformerUtils {

    private static final DomibusLogger LOG = DomibusLoggerFactory.getLogger(TransformerUtils.class);
    /**
     * {@link XStream} object used to convert and retrieve objects from XML.<br/>
     * XStream documentation records it as thread-safe. It is also known to cause memory issue when instantiated for transactions.<br/>
     * <b>IMPORTANT NOTE: With this object never use XStream aliases</b><br/>
     *
     * @see XStream
     */
    private static final XStream xstream = new XStream(new StaxDriver());

    private TransformerUtils() {
        // private constructor
    }

    static {
        XStream.setupDefaultSecurity(xstream);
        xstream.registerConverter(new JodaDateTimeConverter());
        xstream.registerConverter(new JodaLocalTimeConverter());
        xstream.allowTypesByWildcard(new String[]{
                "oasis.names.specification.**",
                "com.sun.org.apache.xerces.**",
                "eu.europa.ec.etrustex.**",
                "org.etsi.uri.**",
                "org.unece.cefact.namespaces.**",
                "org.w3.**",
                "un.unece.uncefact.**",
                "com.sun.org.apache.**",
                "javax.xml.ws.soap.**",
                "org.apache.cxf.common.**",
                "oracle.j2ee.ws.**",
                "ec.services.wsdl.**",
                "ec.schema.xsd.**",
                "com.sun.xml.internal.**",
                "javax.xml.soap.**"
        });
    }

    /**
     * Parses an Xml and transforms it in an object.
     * Pull parser is used.
     *
     * @param xml payload in XML
     * @param <F> the object type
     * @return an object
     */
    public static <F> F getObjectFromXml(String xml) {
        try {
            if (StringUtils.isBlank(xml)) {
                throw new IllegalArgumentException("Empty xml string");
            }
            return (F) deserializeXMLToObj(xml);
        } catch (Exception ex) {
            throw new ETrustExPluginException(ETrustExError.DEFAULT, "Xml could not be parsed", ex);
        }
    }

    public static PayloadDTO getGenericPayloadDTO(String genericPayloadXML) {
        PayloadDTO payloadDTO = null;
        if (StringUtils.isNotBlank(genericPayloadXML)) {
            payloadDTO = new PayloadDTO(CONTENT_ID_GENERIC.getValue(), getBytesUtf8(genericPayloadXML), MIME_TYPE_TEXT_XML, false, ETRUSTEX_GENERIC_PAYLOAD.getValue());
        }
        return payloadDTO;
    }

    public static PayloadDTO getHeaderPayloadDTO(String headerPayloadXML) {
        PayloadDTO payloadDTO = null;
        if (StringUtils.isNotBlank(headerPayloadXML)) {
            payloadDTO = new PayloadDTO(CONTENT_ID_HEADER.getValue(), getBytesUtf8(headerPayloadXML), MIME_TYPE_TEXT_XML, false, ETRUSTEX_HEADER_PAYLOAD.getValue());
        }
        return payloadDTO;
    }

    public static PayloadDTO getFaultResponsePayloadDTO(Object faultResponse) {
        PayloadDTO payloadDTO = null;
        String faultResponsePayloadXML = serializeObjToXML(faultResponse);
        if (StringUtils.isNotBlank(faultResponsePayloadXML)) {
            payloadDTO = new PayloadDTO(
                    CONTENT_ID_FAULT_RESPONSE.getValue(),
                    getBytesUtf8(faultResponsePayloadXML),
                    MIME_TYPE_TEXT_XML,
                    false,
                    ETRUSTEX_FAULT_RESPONSE_PAYLOAD.getValue()
            );
        }
        return payloadDTO;
    }

    public static PayloadDTO getFaultPayloadDTO(String error) {
        return new PayloadDTO(
                CONTENT_FAULT.getValue(),
                error.getBytes(),
                AS4MessageConstants.MIME_TYPE_APP_OCTET,
                false,
                ETRUSTEX_FAULT_PAYLOAD.getValue()
        );
    }

    public static LargePayloadDTO getLargePayloadDTO(File attachmentFile, String filename, String mimeType) {
        LargePayloadDTO largePayloadDTO = null;
        if (attachmentFile != null) {
            DataHandler wrapperDataHandler = new DataHandler(new FileDataSource(attachmentFile));
            largePayloadDTO = new LargePayloadDTO(CONTENT_ID_DOCUMENT.getValue(), mimeType, wrapperDataHandler, filename, Locale.getDefault(), filename);
        }
        return largePayloadDTO;
    }

    public static String getPayloadAsXML(@NotNull PayloadDTO as4Payload) {
        if (as4Payload.getPayload() == null || as4Payload.getPayload().length == 0) {
            throw new IllegalArgumentException("AS4 Payload [" + as4Payload.getContentId() + "] is empty");
        }
        return newStringUtf8(as4Payload.getPayload());
    }

    public static <T> T extractETrustExXMLPayload(ContentId requestContentId, ETrustExAdapterDTO eTrustExAdapterDTO) {
        if (eTrustExAdapterDTO != null) {
            PayloadDTO payloadDTO = eTrustExAdapterDTO.getPayloadFromCID(requestContentId);
            if (payloadDTO != null) {
                String payloadXML = TransformerUtils.getPayloadAsXML(payloadDTO);
                LOG.debug("On searching with content ID: [{}] ETrustEx Payload XML extracted:[{}]", requestContentId, payloadXML);
                return TransformerUtils.getObjectFromXml(payloadXML);
            }
        }
        return null;
    }

    public static <T> T extractETrustExXMLPayloadFaultResponse(ETrustExAdapterDTO eTrustExAdapterDTO, T defaultFaultResponse) {
        if (eTrustExAdapterDTO != null) {
            PayloadDTO payloadDTO = eTrustExAdapterDTO.getPayloadFromCID(CONTENT_ID_FAULT_RESPONSE);
            if (payloadDTO != null) {
                try {
                    String payloadXML = TransformerUtils.getPayloadAsXML(payloadDTO);
                    LOG.debug("On searching with content id:[cid:faultResponse] ETrustEx Payload XML extracted: {}", payloadXML);

                    Object defaultResponse = deserializeXMLToObj(payloadXML);
                    if (defaultResponse.getClass().equals(defaultFaultResponse.getClass())) {
                        return (T) defaultResponse;
                    }
                } catch (Exception e) {
                    LOG.error("Could not deserialize the object", e);
                }
                return defaultFaultResponse;
            }
        }
        return null;
    }

    public static String serializeObjToXML(Object obj) {
        return xstream.toXML(obj);
    }

    public static Object deserializeXMLToObj(String xml) {
        try {
            return xstream.fromXML(xml);
        } catch (Exception e) {
            LOG.error(xml.replace("><", ">\n<"));
            throw e;
        }
    }
}
package eu.europa.ec.etrustex.common.converters.node;

import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PartyType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.EndpointIDType;

/**
 * Builds a {@code PartyType} object used in Node requests
 */
public class NodePartyConverter {

    private NodePartyConverter() {
    }

    public static PartyType buildPartyType(String partyUuid) {
        PartyType partySdo = null;

        if (partyUuid != null) {
            partySdo = new PartyType();

            EndpointIDType endpoint = new EndpointIDType();
            endpoint.setValue(partyUuid);
            partySdo.setEndpointID(endpoint);
        }

        return partySdo;
    }
}

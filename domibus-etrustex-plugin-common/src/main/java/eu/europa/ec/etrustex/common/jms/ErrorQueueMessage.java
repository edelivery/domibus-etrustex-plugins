package eu.europa.ec.etrustex.common.jms;

public class ErrorQueueMessage extends QueueMessage {

    String id;

    public ErrorQueueMessage(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

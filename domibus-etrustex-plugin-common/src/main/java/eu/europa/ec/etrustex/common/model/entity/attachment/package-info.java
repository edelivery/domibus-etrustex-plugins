/**
 * This package will store common classes used for EtxAttachment entity
 * @author Catalin Enache
 * @version 2.0
 * @since  04/10/2017
 */
package eu.europa.ec.etrustex.common.model.entity.attachment;
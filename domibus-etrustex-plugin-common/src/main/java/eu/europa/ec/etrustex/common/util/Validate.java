package eu.europa.ec.etrustex.common.util;

import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Utility class to validate objects
 *
 * @author François Gautier
 * @version 1.0
 * @since 05-Dec-17
 */
public class Validate {

    private Validate() {
    }

    /**
     * Validate if a given object is null
     *
     * @param o object to validate
     * @param errorMessage message to use in the {@link ETrustExPluginException} constructor
     * @throws ETrustExPluginException if object is null with {@link ETrustExError#DEFAULT}
     */
    public static void notNull(Object o, String errorMessage) throws ETrustExPluginException{
        notNull(o, ETrustExError.DEFAULT, errorMessage);
    }

    /**
     * Validate if a given object is null
     *
     * @param o object to validate
     * @param eTrustExError {@link ETrustExError} to use in the {@link ETrustExPluginException} constructor
     * @param errorMessage message to use in the {@link ETrustExPluginException} constructor
     * @throws ETrustExPluginException if object is null
     */
    public static void notNull(Object o, ETrustExError eTrustExError, String errorMessage) throws ETrustExPluginException{
        if (o == null) {
            throw new ETrustExPluginException(eTrustExError, errorMessage);
        }
    }

    /**
     * Validate if a given String is blank
     *
     * @param o object to validate
     * @param errorMessage message to use in the {@link ETrustExPluginException} constructor
     * @throws ETrustExPluginException if string is blank (null or empty) with {@link ETrustExError#DEFAULT}
     */
    public static void notBlank(String o, String errorMessage) {
        notBlank(o, ETrustExError.DEFAULT, errorMessage);
    }

    /**
     * Validate if a given String is blank
     *
     * @param o object to validate
     * @param eTrustExError {@link ETrustExError} to use in the {@link ETrustExPluginException} constructor
     * @param errorMessage message to use in the {@link ETrustExPluginException} constructor
     * @throws ETrustExPluginException if object is null
     */
    public static void notBlank(String o, ETrustExError eTrustExError, String errorMessage) {
        if (isBlank(o)) {
            throw new ETrustExPluginException(eTrustExError, errorMessage);
        }
    }
}

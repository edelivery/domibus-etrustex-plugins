package eu.europa.ec.etrustex.common.util.attachment;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * @author micleva
 * @project ETX
 */

public class PrintDirectoryFiles extends SimpleFileVisitor<Path> {

    StringBuilder folderStructureBuilder;

    public PrintDirectoryFiles(StringBuilder folderStructureBuilder) {
        this.folderStructureBuilder = folderStructureBuilder;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
        folderStructureBuilder.append('\n').append(dir);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
        folderStructureBuilder.append('\n').append(file);
        try {
            folderStructureBuilder.append(" size: ").append(Files.size(file));
        } catch (IOException e) {
            //ignore
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file,
                                           IOException exc) {
        folderStructureBuilder.append('\n').append("Failed to visit folder ").append(file).append("; exc: ").append(exc);
        return FileVisitResult.CONTINUE;
    }
}

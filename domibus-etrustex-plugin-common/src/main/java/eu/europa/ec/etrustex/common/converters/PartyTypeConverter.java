package eu.europa.ec.etrustex.common.converters;

import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PartyType;

public class PartyTypeConverter {

    static public String getUuid(PartyType partyType) {
        if (partyType != null && partyType.getPartyIdentification() != null && partyType.getPartyIdentification().get(0) != null && partyType.getPartyIdentification().get(0).getID() != null) {
            return partyType.getPartyIdentification().get(0).getID().getValue();
        }
        return null;
    }
}

package eu.europa.ec.etrustex.common.util;

import eu.domibus.ext.domain.DomibusPropertyMetadataDTO;
import eu.domibus.ext.services.DomibusPropertyExtServiceDelegateAbstract;
import eu.domibus.ext.services.DomibusPropertyManagerExt;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static eu.domibus.ext.domain.DomibusPropertyMetadataDTO.Type.STRING;
import static eu.domibus.ext.domain.DomibusPropertyMetadataDTO.Usage.GLOBAL;

/**
 * This is a common class to load the common AS4 properties shared between ETrustEx Node and ETrustEx Backend plugin.<br/>
 * Mainly the common properties relate to the PartyIdType, PartyRole, Services and Actions<br/>
 * Domibus Plugins using this property class should mandatorily have the following:<br/>
 * 1. Have properties bean definition as:<br/>
 * &lt;context:property-placeholder properties-ref="eTrustExPluginCommonAS4Properties" ignore-resource-not-found="false" ignore-unresolvable="true"/&gt;
 * &lt;util:properties id="eTrustExPluginCommonAS4Properties" location="file:///${domibus.config.location}/plugins/config/domibus-etrustex-plugin-commonAS4-plugin-default.properties" scope="singleton"/&gt;
 *
 * @author Arun Raj
 */
@Component("etxPluginAS4Properties")
public class ETrustExPluginAS4Properties extends DomibusPropertyExtServiceDelegateAbstract implements DomibusPropertyManagerExt {

    public static final String ETX_PLUGINS_COMMON_MODULE = "ETX_PLUGINS_COMMON_ETX_PLUGINS_COMMON_MODULE";
    public static final String AS4_FROM_PARTYID_TYPE = "as4.FromPartyIdType";
    public static final String AS4_FROM_PARTY_ROLE = "as4.FromPartyRole";
    public static final String AS4_ETRUSTEX_NODE_PARTYID = "as4.ETrustExNodePartyId";
    public static final String AS4_TO_PARTYID_TYPE = "as4.ToPartyIdType";
    public static final String AS4_TO_PARTY_ROLE = "as4.ToPartyRole";
    public static final String AS4_SERVICE_APPLICATION_RESPONSE = "as4.Service.ApplicationResponse";
    public static final String AS4_SERVICE_APPLICATION_RESPONSE_SERVICE_TYPE = "as4.Service.ApplicationResponse.ServiceType";
    public static final String AS4_ACTION_SUBMIT_APPLICATION_RESPONSE_REQUEST = "as4.Action.SubmitApplicationResponse.Request";
    public static final String AS4_ACTION_SUBMIT_APPLICATION_RESPONSE_RESPONSE = "as4.Action.SubmitApplicationResponse.Response";
    public static final String AS4_SERVICE_DOCUMENT_WRAPPER = "as4.Service.DocumentWrapper";
    public static final String AS4_SERVICE_DOCUMENT_WRAPPER_SERVICE_TYPE = "as4.Service.DocumentWrapper.ServiceType";
    public static final String AS4_ACTION_STORE_DOCUMENT_WRAPPER_REQUEST = "as4.Action.StoreDocumentWrapper.Request";
    public static final String AS4_ACTION_STORE_DOCUMENT_WRAPPER_RESPONSE = "as4.Action.StoreDocumentWrapper.Response";
    public static final String AS4_SERVICE_DOCUMENT_BUNDLE = "as4.Service.DocumentBundle";
    public static final String AS4_SERVICE_DOCUMENT_BUNDLE_SERVICE_TYPE = "as4.Service.DocumentBundle.ServiceType";
    public static final String AS4_ACTION_SUBMIT_DOCUMENT_BUNDLE_REQUEST = "as4.Action.SubmitDocumentBundle.Request";
    public static final String AS4_ACTION_SUBMIT_DOCUMENT_BUNDLE_RESPONSE = "as4.Action.SubmitDocumentBundle.Response";
    public static final String AS4_SERVICE_RETRIEVE_ICA = "as4.Service.RetrieveICA";
    public static final String AS4_SERVICE_RETRIEVE_ICA_SERVICE_TYPE = "as4.Service.RetrieveICA.ServiceType";
    public static final String AS4_ACTION_RETRIEVE_ICA_REQUEST = "as4.Action.RetrieveICA.Request";
    public static final String AS4_ACTION_RETRIEVE_ICA_RESPONSE = "as4.Action.RetrieveICA.Response";
    public static final String AS4_SERVICE_INBOX_WRAPPER_TRANSMISSION = "as4.Service.InboxWrapperTransmission";
    public static final String AS4_SERVICE_INBOX_WRAPPER_TRANSMISSION_SERVICE_TYPE = "as4.Service.InboxWrapperTransmission.ServiceType";
    public static final String AS4_ACTION_INBOX_WRAPPER_TRANSMISSION_REQUEST = "as4.Action.InboxWrapperTransmission.Request";
    public static final String AS4_SERVICE_STATUS_MESSAGE_TRANSMISSION = "as4.Service.StatusMessageTransmission";
    public static final String AS4_SERVICE_STATUS_MESSAGE_TRANSMISSION_SERVICE_TYPE = "as4.Service.StatusMessageTransmission.ServiceType";
    public static final String AS4_ACTION_STATUS_MESSAGE_TRANSMISSION_REQUEST = "as4.Action.StatusMessageTransmission.Request";
    public static final String AS4_SERVICE_INBOX_BUNDLE_TRANSMISSION = "as4.Service.InboxBundleTransmission";
    public static final String AS4_SERVICE_INBOX_BUNDLE_TRANSMISSION_SERVICE_TYPE = "as4.Service.InboxBundleTransmission.ServiceType";
    public static final String AS4_ACTION_INBOX_BUNDLE_TRANSMISSION_REQUEST = "as4.Action.InboxBundleTransmission.Request";
    public static final String AS4_ACTION_INBOX_BUNDLE_TRANSMISSION_ACK = "as4.Action.InboxBundleTransmission.Ack";
//    @Autowired
//    protected DomibusPropertyExtService domibusPropertyExtService;

    private Map<String, DomibusPropertyMetadataDTO> knownProperties;

    @Override
    public Map<String, DomibusPropertyMetadataDTO> getKnownProperties() {
        return knownProperties;
    }

    public ETrustExPluginAS4Properties() {
        List<DomibusPropertyMetadataDTO> allProperties = Arrays.asList(
                new DomibusPropertyMetadataDTO(AS4_FROM_PARTYID_TYPE, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_FROM_PARTY_ROLE, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_ETRUSTEX_NODE_PARTYID, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_TO_PARTYID_TYPE, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_TO_PARTY_ROLE, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_SERVICE_APPLICATION_RESPONSE, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_SERVICE_APPLICATION_RESPONSE_SERVICE_TYPE, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_ACTION_SUBMIT_APPLICATION_RESPONSE_REQUEST, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_ACTION_SUBMIT_APPLICATION_RESPONSE_RESPONSE, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_SERVICE_DOCUMENT_WRAPPER, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_SERVICE_DOCUMENT_WRAPPER_SERVICE_TYPE, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_ACTION_STORE_DOCUMENT_WRAPPER_REQUEST, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_ACTION_STORE_DOCUMENT_WRAPPER_RESPONSE, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_SERVICE_DOCUMENT_BUNDLE, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_SERVICE_DOCUMENT_BUNDLE_SERVICE_TYPE, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_ACTION_SUBMIT_DOCUMENT_BUNDLE_REQUEST, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_ACTION_SUBMIT_DOCUMENT_BUNDLE_RESPONSE, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_SERVICE_RETRIEVE_ICA, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_SERVICE_RETRIEVE_ICA_SERVICE_TYPE, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_ACTION_RETRIEVE_ICA_REQUEST, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_ACTION_RETRIEVE_ICA_RESPONSE, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_SERVICE_INBOX_WRAPPER_TRANSMISSION, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_SERVICE_INBOX_WRAPPER_TRANSMISSION_SERVICE_TYPE, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_ACTION_INBOX_WRAPPER_TRANSMISSION_REQUEST, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_SERVICE_STATUS_MESSAGE_TRANSMISSION, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_SERVICE_STATUS_MESSAGE_TRANSMISSION_SERVICE_TYPE, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_ACTION_STATUS_MESSAGE_TRANSMISSION_REQUEST, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_SERVICE_INBOX_BUNDLE_TRANSMISSION, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true),
                new DomibusPropertyMetadataDTO(AS4_SERVICE_INBOX_BUNDLE_TRANSMISSION_SERVICE_TYPE, STRING, ETX_PLUGINS_COMMON_MODULE, GLOBAL, true)
        );
        knownProperties = allProperties.stream().collect(Collectors.toMap(x -> x.getName(), x -> x));
    }


    public String getAs4FromPartyIdType() {
        return domibusPropertyExtService.getProperty(AS4_FROM_PARTYID_TYPE);
    }

    public String getAs4FromPartyRole() {
        return domibusPropertyExtService.getProperty(AS4_FROM_PARTY_ROLE);
    }

    public String getAs4ETrustExNodePartyId() {
        return domibusPropertyExtService.getProperty(AS4_ETRUSTEX_NODE_PARTYID);
    }

    public String getAs4ToPartyIdType() {
        return domibusPropertyExtService.getProperty(AS4_TO_PARTYID_TYPE);
    }

    public String getAs4ToPartyRole() {
        return domibusPropertyExtService.getProperty(AS4_TO_PARTY_ROLE);
    }

    public String getAs4_Service_ApplicationResponse() {
        return domibusPropertyExtService.getProperty(AS4_SERVICE_APPLICATION_RESPONSE);
    }

    public String getAs4_Service_ApplicationResponse_ServiceType() {
        return domibusPropertyExtService.getProperty(AS4_SERVICE_APPLICATION_RESPONSE_SERVICE_TYPE);
    }

    public String getAs4_Action_SubmitApplicationResponse_Request() {
        return domibusPropertyExtService.getProperty(AS4_ACTION_SUBMIT_APPLICATION_RESPONSE_REQUEST);
    }

    public String getAs4_Action_SubmitApplicationResponse_Response() {
        return domibusPropertyExtService.getProperty(AS4_ACTION_SUBMIT_APPLICATION_RESPONSE_RESPONSE);
    }

    public String getAs4_Service_DocumentWrapper() {
        return domibusPropertyExtService.getProperty(AS4_SERVICE_DOCUMENT_WRAPPER);
    }

    public String getAs4_Service_DocumentWrapper_ServiceType() {
        return domibusPropertyExtService.getProperty(AS4_SERVICE_DOCUMENT_WRAPPER_SERVICE_TYPE);
    }

    public String getAs4_Action_StoreDocumentWrapper_Request() {
        return domibusPropertyExtService.getProperty(AS4_ACTION_STORE_DOCUMENT_WRAPPER_REQUEST);
    }

    public String getAs4_Action_StoreDocumentWrapper_Response() {
        return domibusPropertyExtService.getProperty(AS4_ACTION_STORE_DOCUMENT_WRAPPER_RESPONSE);
    }

    public String getAs4_Service_DocumentBundle() {
        return domibusPropertyExtService.getProperty(AS4_SERVICE_DOCUMENT_BUNDLE);
    }

    public String getAs4_Service_DocumentBundle_ServiceType() {
        return domibusPropertyExtService.getProperty(AS4_SERVICE_DOCUMENT_BUNDLE_SERVICE_TYPE);
    }

    public String getAs4_Action_SubmitDocumentBundle_Request() {
        return domibusPropertyExtService.getProperty(AS4_ACTION_SUBMIT_DOCUMENT_BUNDLE_REQUEST);
    }

    public String getAs4_Action_SubmitDocumentBundle_Response() {
        return domibusPropertyExtService.getProperty(AS4_ACTION_SUBMIT_DOCUMENT_BUNDLE_RESPONSE);
    }

    public String getAs4_Service_RetrieveICA() {
        return domibusPropertyExtService.getProperty(AS4_SERVICE_RETRIEVE_ICA);
    }

    public String getAs4_Service_RetrieveICA_ServiceType() {
        return domibusPropertyExtService.getProperty(AS4_SERVICE_RETRIEVE_ICA_SERVICE_TYPE);
    }

    public String getAs4_Action_RetrieveICA_Request() {
        return domibusPropertyExtService.getProperty(AS4_ACTION_RETRIEVE_ICA_REQUEST);
    }

    public String getAs4_Action_RetrieveICA_Response() {
        return domibusPropertyExtService.getProperty(AS4_ACTION_RETRIEVE_ICA_RESPONSE);
    }

    public String getAs4ServiceInboxWrapperTransmission() {
        return domibusPropertyExtService.getProperty(AS4_SERVICE_INBOX_WRAPPER_TRANSMISSION);
    }

    public String getAs4ServiceInboxWrapperTransmissionServiceType() {
        return domibusPropertyExtService.getProperty(AS4_SERVICE_INBOX_WRAPPER_TRANSMISSION_SERVICE_TYPE);
    }

    public String getAs4ActionInboxWrapperTransmissionRequest() {
        return domibusPropertyExtService.getProperty(AS4_ACTION_INBOX_WRAPPER_TRANSMISSION_REQUEST);
    }

    public String getAs4ServiceStatusMessageTransmission() {
        return domibusPropertyExtService.getProperty(AS4_SERVICE_STATUS_MESSAGE_TRANSMISSION);
    }

    public String getAs4ServiceStatusMessageTransmissionServiceType() {
        return domibusPropertyExtService.getProperty(AS4_SERVICE_STATUS_MESSAGE_TRANSMISSION_SERVICE_TYPE);
    }

    public String getAs4ActionStatusMessageTransmissionRequest() {
        return domibusPropertyExtService.getProperty(AS4_ACTION_STATUS_MESSAGE_TRANSMISSION_REQUEST);
    }

    public String getAs4ServiceInboxBundleTransmission() {
        return domibusPropertyExtService.getProperty(AS4_SERVICE_INBOX_BUNDLE_TRANSMISSION);
    }

    public String getAs4ServiceInboxBundleTransmissionServiceType() {
        return domibusPropertyExtService.getProperty(AS4_SERVICE_INBOX_BUNDLE_TRANSMISSION_SERVICE_TYPE);
    }

    public String getAs4ActionInboxBundleTransmissionRequest() {
        return domibusPropertyExtService.getProperty(AS4_ACTION_INBOX_BUNDLE_TRANSMISSION_REQUEST);
    }

    public String getAs4ActionInboxBundleTransmissionAck() {
        return domibusPropertyExtService.getProperty(AS4_ACTION_INBOX_BUNDLE_TRANSMISSION_ACK);
    }
}

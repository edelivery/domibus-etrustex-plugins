package eu.europa.ec.etrustex.common.exceptions;

/**
 * Enum for ETrustEx error codes.
 *
 * @author Federico Martini
 * @version 1.0
 * @since 24/03/2017
 */
public enum ETrustExError {

    /**
     * Unauthorized access
     */
    ETX_001("FLT:1", "Unauthorized access"),

    /**
     * Invalid SOAP request
     */
    ETX_002("FLT:2", "Invalid SOAP request"),

    /**
     * Message already exist
     */
    ETX_003("MSG:1", "Message already exist"),

    /**
     * Invalid party
     */
    ETX_004("MSG:2", "Invalid party"),

    /**
     * Message not found
     */
    ETX_005("MSG:3", "Message not found"),

    /**
     * File already exist
     */
    ETX_006("FMD:1", "File already exist"),

    /**
     * Missing file
     */
    ETX_007("FMD:2", "Missing file"),

    /**
     * Hard business rule violated
     */
    ETX_009("BDL:4", "Hard business rule violated"),

    /**
     * Attachment expired
     */
    ETX_010("ATT:1", "Attachment expired"),

    /**
     * Technical failure
     */
    DEFAULT("TECHNICAL", "Technical failure");

    private final String code;
    private final String description;

    ETrustExError(String code, String desc) {
        this.code = code;
        this.description = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}

/**
 * This package contains helper classes used by both backend and node plugin for Workspace or PayloadService attachment
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 05/10/2017
 */
package eu.europa.ec.etrustex.common.util.attachment;
package eu.europa.ec.etrustex.common.util;

public interface AS4MessageConstants {

    String MESSAGE_ID = "messageId";
    String JMS_BACKEND_MESSAGE_TYPE_PROPERTY_KEY = "messageType";
    String MIME_TYPE = "MimeType";
    String MIME_TYPE_TEXT_XML = "text/xml";
    String MIME_TYPE_APP_OCTET = "application/octet-stream";
    String ACTION = "action";
    String SERVICE = "service";
    String SERVICE_TYPE = "serviceType";
    String CONVERSATION_ID = "conversationId";
    String AGREEMENT_REF = "agreementRef";
    String REF_TO_MESSAGE_ID = "refToMessageId";
    String FROM_PARTY_ID = "fromPartyId";
    String FROM_PARTY_TYPE = "fromPartyType";
    String FROM_ROLE = "fromRole";
    String TO_PARTY_ID = "toPartyId";
    String TO_PARTY_TYPE = "toPartyType";
    String TO_ROLE = "toRole";
    String PROPERTY_ORIGINAL_SENDER = "originalSender";
    String PROPERTY_FINAL_RECIPIENT = "finalRecipient";
    String PROPERTY_ENDPOINT = "endPointAddress";
    String PROTOCOL = "protocol";
    String TOTAL_NUMBER_OF_PAYLOADS = "totalNumberOfPayloads";
    String PAYLOAD_FILE_NAME_FORMAT = "payload_{0}.bin";
    String BODYLOAD_FILE_NAME_FORMAT = "bodyload.bin";
    String MESSAGE_TYPE_SUBMIT = "submitMessage";
    String MESSAGE_TYPE_SUBMIT_RESPONSE = "submitResponse";
    String MESSAGE_TYPE_INCOMING = "incomingMessage";
    String MESSAGE_TYPE_SEND_SUCCESS = "messageSent";
    String MESSAGE_TYPE_SEND_FAILURE = "messageSendFailure";
    String MESSAGE_TYPE_RECEIVE_FAILURE = "messageReceptionFailure";
    String ERROR_CODE = "errorCode";
    String ERROR_DETAIL = "errorDetail";
    String PAYLOAD_NAME_PREFIX = "payload-";
    String PAYLOAD_NAME_FORMAT = PAYLOAD_NAME_PREFIX + "{0}";
    String PAYLOAD_DESCRIPTION_SUFFIX = "-description";
    String PAYLOAD_DESCRIPTION_FORMAT = PAYLOAD_NAME_FORMAT + PAYLOAD_DESCRIPTION_SUFFIX;
    String PAYLOAD_MIME_TYPE_SUFFIX = "-MimeType";
    String PAYLOAD_MIME_TYPE_FORMAT = PAYLOAD_NAME_FORMAT + PAYLOAD_MIME_TYPE_SUFFIX;
    String PAYLOAD_MIME_CONTENT_ID_SUFFIX = "-MimeContentId";
    String PAYLOAD_MIME_CONTENT_ID_FORMAT = PAYLOAD_NAME_FORMAT + PAYLOAD_MIME_CONTENT_ID_SUFFIX;
    String PROPERTY_ETRUSTEX_BACKEND_MESSAGE_ID = "eTxBackendMsgId";
    String PROPERTY_FILE_NAME = "fileName";
    String PROPERTY_ETRUSTEX_ATTACHMENT_UUIDS = "attachmentUUIDs";

}

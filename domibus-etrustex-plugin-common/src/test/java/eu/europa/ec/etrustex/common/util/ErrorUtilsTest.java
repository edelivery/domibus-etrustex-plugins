package eu.europa.ec.etrustex.common.util;

import eu.domibus.common.ErrorResult;
import eu.domibus.common.MessageReceiveFailureEvent;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author Arun Raj
 * @version 1.0
 * @since 22/08/2017
 */
@RunWith(JMockit.class)
public class ErrorUtilsTest {

    @Test
    public void testLogMessageReceiveFailureEventString(@Mocked final MessageReceiveFailureEvent messageReceiveFailureEvent, @Mocked final ErrorResult errorResult) {

        new Expectations() {{
            messageReceiveFailureEvent.getMessageId();
            result = "TestMessageId";

            messageReceiveFailureEvent.getEndpoint();
            result = "TestEndPoint";

            messageReceiveFailureEvent.getErrorResult();
            result = errorResult;

        }};

        ErrorUtils.logMessageReceiveFailureEventString(messageReceiveFailureEvent, "ErrorUtilsTest");

        new Verifications() {{
            messageReceiveFailureEvent.getMessageId();
            times = 1;
            messageReceiveFailureEvent.getEndpoint();
            times = 1;
            errorResult.getMshRole();
            times = 1;
            errorResult.getMessageInErrorId();
            times = 1;
            errorResult.getErrorCode();
            times = 1;
            errorResult.getErrorDetail();
            times = 1;
            errorResult.getTimestamp();
            times = 1;
            errorResult.getNotified();
            times = 1;

        }};


    }
}

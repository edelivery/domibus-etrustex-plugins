package eu.europa.ec.etrustex.common.util;

import ec.schema.xsd.documentbundle_1.DocumentBundleType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IssueDateType;
import oasis.names.specification.ubl.schema.xsd.commonbasiccomponents_2.IssueTimeType;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.junit.Assert;
import org.junit.Test;

import java.util.Calendar;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;

/**
 * @author François Gautier
 * @version 1.0
 * @since 16/02/2018
 */
public class CalendarHelperTest {

    private static final String DATE = "2018-01-01";
    private static final String TIME = "08:19.42";
    private static final DateTime JODA_DATE_TIME = DateTime.parse(DATE + "T" + TIME);
    private static final Calendar GREGORIAN_CALENDAR = JODA_DATE_TIME.toGregorianCalendar();
    private static final DateTime JODA_DATE = DateTime.parse(DATE);
    private static final LocalTime JODA_TIME = LocalTime.parse(TIME);

    @Test
    public void getCalendarFromJodaDateTime_full() {
        Calendar result = CalendarHelper.getCalendarFromJodaDateTime(JODA_DATE, JODA_TIME);
        Assert.assertThat(result, is(GREGORIAN_CALENDAR));
    }

    @Test
    public void getCalendarFromJodaDateTime_noTime() {
        Calendar result = CalendarHelper.getCalendarFromJodaDateTime(JODA_DATE, null);
        Assert.assertThat(result, is(((Calendar) JODA_DATE.toGregorianCalendar())));
    }

    @Test
    public void getCalendarIssueDateFromDocumentBundle_null() {
        Calendar result = CalendarHelper.getCalendarIssueDateFromDocumentBundle(new DocumentBundleType());
        Assert.assertThat(result, is(nullValue()));
    }

    @Test
    public void getCalendarIssueDateFromDocumentBundle_happy() {
        DocumentBundleType documentBundleType = getDocumentBundleType(JODA_DATE, JODA_TIME);
        Calendar result = CalendarHelper.getCalendarIssueDateFromDocumentBundle(documentBundleType);
        Assert.assertThat(result, is(GREGORIAN_CALENDAR));
    }

    @Test
    public void getCalendarIssueDateFromDocumentBundle_dateOnly() {
        DocumentBundleType documentBundleType = getDocumentBundleType(JODA_DATE, null);
        Calendar result = CalendarHelper.getCalendarIssueDateFromDocumentBundle(documentBundleType);
        Assert.assertThat(result, is((Calendar) JODA_DATE.toGregorianCalendar()));
    }

    @Test
    public void getJodaDateFromCalendar() {
        DateTime result = CalendarHelper.getJodaDateFromCalendar(CalendarHelper.getCalendarFromJodaDateTime(JODA_DATE, JODA_TIME));
        Assert.assertThat(result.toString(), is(JODA_DATE.toString()));
    }

    @Test
    public void getJodaLocalTimeFromCalendar() {
        LocalTime result = CalendarHelper.getJodaLocalTimeFromCalendar(GREGORIAN_CALENDAR);
        Assert.assertThat(result, is(JODA_TIME));
    }

    private DocumentBundleType getDocumentBundleType(DateTime jodaDate, LocalTime jodaTime) {
        DocumentBundleType documentBundleType = new DocumentBundleType();
        documentBundleType.setIssueDate(getIssueDateType(jodaDate));
        documentBundleType.setIssueTime(getIssueTimeType(jodaTime));
        return documentBundleType;
    }

    private IssueTimeType getIssueTimeType(LocalTime jodaTime) {
        IssueTimeType value = new IssueTimeType();
        value.setValue(jodaTime);
        return value;
    }

    private IssueDateType getIssueDateType(DateTime jodaDate) {
        IssueDateType issueDate = new IssueDateType();
        issueDate.setValue(jodaDate);
        return issueDate;
    }
}
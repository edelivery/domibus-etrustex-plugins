package eu.europa.ec.etrustex.common.util;

import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.core.Is.is;

/**
 * @author François Gautier
 * @version 1.0
 * @since 06-Dec-17
 */
public class ValidateTest {

    public static final String ERROR = "error";

    @Test
    public void notBlank_exception_emptyString() {
        try {
            Validate.notBlank(StringUtils.EMPTY, ETrustExError.ETX_001, "error");
            Assert.fail();
        } catch (ETrustExPluginException e) {
            Assert.assertThat(e.getError(), is(ETrustExError.ETX_001));
            Assert.assertThat(e.getMessage(), CoreMatchers.containsString(ERROR));
        }
    }
    @Test
    public void notBlank_exception_null() {
        try {
            Validate.notBlank(null, ETrustExError.ETX_001, "error");
            Assert.fail();
        } catch (ETrustExPluginException e) {
            Assert.assertThat(e.getError(), is(ETrustExError.ETX_001));
            Assert.assertThat(e.getMessage(), CoreMatchers.containsString(ERROR));
        }
    }

    @Test
    public void notBlank_ok() {
        Validate.notBlank("OK", ETrustExError.ETX_001, "error");
    }

    @Test
    public void notBlank_okDefault() {
        Validate.notBlank("OK", "error");
    }

    @Test
    public void notNull_exception() {
        try {
            Validate.notNull(null, ETrustExError.ETX_001, "error");
            Assert.fail();
        } catch (ETrustExPluginException e) {
            Assert.assertThat(e.getError(), is(ETrustExError.ETX_001));
            Assert.assertThat(e.getMessage(), CoreMatchers.containsString(ERROR));
        }
    }

    @Test
    public void notNull_ok() {
        Validate.notNull(new Object(), ETrustExError.ETX_001, "error");
    }
}
package eu.europa.ec.etrustex.common.jms;

import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 * @author François Gautier
 * @version 1.0
 * @since 04/05/2018
 */
@RunWith(JMockit.class)
public class TextMessageCreatorTest {

    private static final String XML = "xml";

    @Mocked
    private Session session;
    @Mocked
    private TextMessage message;

    @Test
    public void createMessage() throws JMSException {
        new Expectations() {{
            session.createTextMessage(XML);
            times = 1;
            result = message;
        }};

        new TextMessageCreator(XML).createMessage(session);

        new FullVerifications() {{
        }};
    }
}
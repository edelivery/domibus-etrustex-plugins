package eu.europa.ec.etrustex.common.service;

import eu.domibus.ext.delegate.services.multitenancy.DomainTaskExecutorExtDelegate;
import eu.domibus.ext.domain.DomainDTO;
import eu.domibus.ext.services.DomainContextExtService;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.util.ETrustExPluginAS4Properties;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Injectable;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * @author François Gautier
 * @version 1.0
 * @since 19-Dec-17
 */
@RunWith(JMockit.class)
public class AbstractWorkspaceServiceTest {

    private static final String TEST = "TEST";
    private static final String FILE_PATH = "FILE_PATH";
    private static final String FILE_NAME = "FILE_NAME";

    @Injectable
    private ETrustExPluginAS4Properties etxPluginAS4Properties;

    @Injectable
    private DomainContextExtService domainContextExtService;

    protected DomainTaskExecutorExtDelegate executor;

    private WorkSpaceServiceTest workSpaceServiceTest;

    @Mocked
    protected Path path;

    protected Long attachmentId;
    protected File file;
    private ByteArrayInputStream inputStream;


    @Before
    public void setUp() throws Exception {
        executor = new DomainTaskExecutorExtDelegate(){
            public void submitLongRunningTask(Runnable task, DomainDTO domainDTO) {
                task.run();
            }
        };
        attachmentId = RandomUtils.nextLong(0, 99999);
        workSpaceServiceTest = new WorkSpaceServiceTest(domainContextExtService, executor);
        workSpaceServiceTest.loadConfiguration();

        file = File.createTempFile("WorkSpaceServiceTest", "");
        inputStream = new ByteArrayInputStream(StringUtils.getBytesUtf8("test"));
    }

    class WorkSpaceServiceTest extends AbstractWorkspaceService {

        WorkSpaceServiceTest(DomainContextExtService domainContextExtService, DomainTaskExecutorExtDelegate executor) {
            this.domainContextExtService = domainContextExtService;
            this.executor = executor;
        }

        @Override
        public Path getRoot() {
            return path;
        }

        @Override
        public int getSleepMillis() {
            return 0;
        }

        @Override
        public int getTimeOut() {
            return 0;
        }

    }

    @After
    public void tearDown() throws IOException {
        Files.deleteIfExists(file.toPath());
    }

    @Test(expected = ETrustExPluginException.class)
    public void writeLargeFile_nullPath() throws IOException {
        workSpaceServiceTest.writeLargeFile(null, new ByteArrayInputStream(StringUtils.getBytesUtf8(TEST)));
    }

    @Test
    public void writeLargeFile() throws IOException {
        new Expectations() {{
            path.toFile();
            times = 1;
            result = file;
        }};
        workSpaceServiceTest.writeLargeFile(path, new ByteArrayInputStream(StringUtils.getBytesUtf8(TEST)));

        assertThat(Files.readAllBytes(file.toPath()), is(StringUtils.getBytesUtf8(TEST)));

        new FullVerifications() {{
        }};
    }

    @Test
    public void writeFile_ok() throws IOException {
        final String pathname = "New.txt";

        assertFalse(new File(pathname).exists());
        new Expectations() {{
            path.resolve(attachmentId.toString());
            times = 1;
            result = new File(pathname).toPath();
        }};
        workSpaceServiceTest.writeFile(attachmentId, inputStream);

        File file = new File(pathname);
        assertTrue(file.exists());
        Files.deleteIfExists(file.toPath());
    }

    @Test(expected = IOException.class)
    public void writeFile_exception() throws IOException {
        new Expectations() {{
            path.resolve(attachmentId.toString());
            times = 1;
            result = new File("").toPath();
        }};

        workSpaceServiceTest.writeFile(attachmentId, inputStream);

        new FullVerifications() {{
        }};
    }

    @Test
    public void getFile() {
        new Expectations() {{
            path.resolve(attachmentId.toString());
            times = 1;
            result = path;
        }};

        assertThat(workSpaceServiceTest.getFile(attachmentId), is(path));
    }

    @Test
    public void deleteFile_doesNotExists_exception() {
        new Expectations() {{
            path.resolve(attachmentId.toString());
            times = 1;
            result = new File("").toPath();

            domainContextExtService.getCurrentDomain();
            times = 1;
            result = null;
        }};
        workSpaceServiceTest.deleteFile(attachmentId);
        new FullVerifications() {{
        }};
    }

    @Test
    public void deleteFile_false() {
        new Expectations() {{
            path.resolve(attachmentId.toString());
            times = 1;
            result = new File("Nope").toPath();
        }};

        workSpaceServiceTest.deleteFile(attachmentId);

        new FullVerifications() {{
        }};
    }

    @Test(expected = ETrustExPluginException.class)
    @Ignore // 12/10/2018 FGA the file locks correctly locally but not in cloud bamboo
    public void deleteFile_lock() throws Exception {
        new Expectations() {{
            path.resolve(attachmentId.toString());
            times = 1;
            result = file.toPath();
        }};

        try (FileInputStream in = new FileInputStream(file)) {
            java.nio.channels.FileLock lock = in.getChannel().tryLock(0L, Long.MAX_VALUE, true);
            try {
                workSpaceServiceTest.deleteFile(attachmentId);
                fail();
            } finally {
                lock.release();
            }
        }

        new FullVerifications() {{
        }};
    }

    @Test
    public void deleteFile_true() {
        new Expectations() {{
            path.resolve(attachmentId.toString());
            times = 1;
            result = file.toPath();
        }};
        workSpaceServiceTest.deleteFile(attachmentId);
        new FullVerifications() {{
        }};
    }

    private Path getPath() {
        return new File("").toPath();
    }

    @Test
    @SuppressWarnings("unused")
    public void getFile(@Mocked final Paths paths) {

        final Path path = getPath();

        new Expectations() {{
            Paths.get(FILE_PATH);
            times = 1;
            result = path;
        }};

        Path file = workSpaceServiceTest.getFile(FILE_PATH, FILE_NAME);

        new FullVerifications() {{

        }};

        assertThat(file.toString(), containsString(path.normalize().toAbsolutePath().toString()));
    }
}
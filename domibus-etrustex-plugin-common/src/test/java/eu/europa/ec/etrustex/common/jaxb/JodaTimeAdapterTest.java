package eu.europa.ec.etrustex.common.jaxb;

import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.joda.time.LocalTime;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.fail;

/**
 * JUnit for {@link JodaTimeAdapter}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 30/11/2017
 */
@RunWith(JMockit.class)
public class JodaTimeAdapterTest {
    private final int year = 2017;
    private final int month = 6;
    private final int day = 27;
    private final int hour = 12;
    private final int minute = 13;
    private final int second = 14;
    private final int millis = 359;
    private final String timezone = "+02:00";

    @Tested
    private JodaTimeAdapter jodaTimeAdapter;

    @Test
    public void testUnmarshal_HappyFlow() throws Exception {

        //input string is '12:13:14.359'
        final String inputDateStr = String.format("%d:%d:%d.%d", hour, minute, second, millis);

        //tested method
        LocalTime outputLocalTime = jodaTimeAdapter.unmarshal(inputDateStr);

        Assert.assertNotNull("resulting dateTime object shouldn't be null", outputLocalTime);

        Assert.assertEquals("hour must match", hour, outputLocalTime.getHourOfDay());
        Assert.assertEquals("minute must match", minute, outputLocalTime.getMinuteOfHour());
        Assert.assertEquals("second must match", second, outputLocalTime.getSecondOfMinute());
        Assert.assertEquals("millis must match", millis, outputLocalTime.getMillisOfSecond());
    }

    @Test
    public void testUnmarshal_InvalidInput() throws Exception {

        //input string is '2017-06-27T12:13:14.359+02:00'
        final String inputDateStr = String.format("%d-%02d-%dT%d:%d:%d.%d%s", year, month, day, hour, minute, second, millis, timezone);

        try {
            //tested method
            jodaTimeAdapter.unmarshal(inputDateStr);
            fail("expected exception");
        } catch (Exception ex) {
            Assert.assertEquals(ex.getClass(), IllegalArgumentException.class);
        }
    }

    @Test
    public void testUnmarshal_NullInput() throws Exception {
        final String inputDateStr = null;

        //tested method
        LocalTime outputLocalTime = jodaTimeAdapter.unmarshal(inputDateStr);

        Assert.assertNull("resulting dateTime object should be null", outputLocalTime);
    }

    @Test
    public void testMarshal_HappyFlow() throws Exception {

        final LocalTime inputLocalTime = new LocalTime(hour, minute, second, millis);

        //"12:13:14.359"
        final String printedLocalTime = String.format("%d:%d:%d.%d", hour, minute, second, millis);

        //tested method
        final String outputLocalTimeStr = jodaTimeAdapter.marshal(inputLocalTime);

        Assert.assertNotNull("printed time shouldn't be null", outputLocalTimeStr);
        Assert.assertEquals("printed time should match", printedLocalTime, outputLocalTimeStr);
    }

    @Test
    public void testMarshal_NullInput() throws Exception {
        final LocalTime inputLocalTime = null;

        //tested method
        final String dateTime = jodaTimeAdapter.marshal(inputLocalTime);

        Assert.assertNull("printed time should be null", dateTime);
    }

}
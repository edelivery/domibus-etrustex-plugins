package eu.europa.ec.etrustex.common.handlers;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static eu.europa.ec.etrustex.common.handlers.Service.*;
import static org.hamcrest.core.Is.is;

/**
 * @author François Gautier
 * @version 1.0
 * @since 07/02/2018
 */

@RunWith(Parameterized.class)
public class ServiceTest {

    @Parameter
    public Service service;
    @Parameter(1)
    public String code;

    @Parameters(name = "{index}: Action({0})")
    public static Collection combinations() {
        return Arrays.asList(new Object[][]{
                // Happy flow scenario
                {APPLICATION_RESPONSE_SERVICE, "ApplicationResponseService"},
                {DOCUMENT_WRAPPER_SERVICE, "DocumentWrapperService"},
                {DOCUMENT_BUNDLE_SERVICE, "DocumentBundleService"},
                {RETRIEVE_ICA_SERVICE, "RetrieveICAService"},
                {BUNDLE_TRANSMISSION_SERVICE, "InboxBundleTransmissionService"},
                {WRAPPER_TRANSMISSION_SERVICE, "WrapperTransmissionService"},
                {STATUS_MESSAGE_TRANSMISSION_SERVICE, "StatusMessageTransmissionService"},
                // returns null
                {UNKNOWN, ""},
                {UNKNOWN, null},
        });
    }

    @Test
    public void test() {
        Service result = Service.fromString(code);
        Assert.assertThat(result, is(service));
    }
}
package eu.europa.ec.etrustex.common.util;

import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 *
 * Contains some tests that are really writing and deleting files
 *
 * @author François Gautier
 * @version 1.0
 * @since 19-Dec-17
 */
@SuppressWarnings("unused")
@RunWith(JMockit.class)
public class WorkSpaceUtilsTest {

    private static final String FILE_PATH = "FILE_PATH";
    private static final String FILE_NAME = "FILE_NAME";
    protected File file;

    @Before
    public void setUp() throws Exception {
        file = File.createTempFile("WorkSpaceServiceTest", "");
    }

    @After
    public void tearDown() throws IOException {
        Files.deleteIfExists(file.toPath());
    }

    private Path getPath() {
        return new File("").toPath();
    }

    @Test(expected = IllegalStateException.class)
    public void getPath_NotExistent_IOExceptionCreateDir(@Mocked final Files files) throws IOException {
        new Expectations() {{
            Files.notExists(file.toPath());
            result = true;
            times = 1;

            Files.createDirectories(file.toPath());
            result = new IOException();
            times = 1;
        }};

        WorkSpaceUtils.getPath(file.toPath());

        new FullVerifications() {{
        }};
    }

    @Test
    public void getPath_Existent_SymbolicLink_2layers(@Mocked Files files) throws IOException {
        final File target = new File("");

        new Expectations() {{
            Files.notExists(file.toPath());
            result = false;
            times = 1;

            Files.isSymbolicLink(file.toPath());
            result = true;
            times = 1;

            Files.readSymbolicLink(file.toPath());
            result = target.toPath();
            times = 1;

            Files.isSymbolicLink(target.toPath());
            result = false;
            times = 1;
        }};

        Path pathToWorkSpace = WorkSpaceUtils.getPath(file.toPath());
        assertThat(pathToWorkSpace, is(target.toPath()));

        new FullVerifications() {{
        }};
    }

    @Test
    public void getPath_Existent_SymbolicLink_IOException(@Mocked Files files) throws IOException {
        new Expectations() {{
            Files.notExists(file.toPath());
            result = false;
            times = 1;

            Files.isSymbolicLink(file.toPath());
            result = true;
            result = false;
            times = 2;

            Files.readSymbolicLink(file.toPath());
            result = new IOException("This exception is not propagated, only logged");
            times = 1;
        }};

        Path pathToWorkSpace = WorkSpaceUtils.getPath(file.toPath());
        assertThat(pathToWorkSpace, is(file.toPath()));

        new FullVerifications() {{
        }};
    }

    @Test
    public void getPath_Existent() {
        Path pathToWorkSpace = WorkSpaceUtils.getPath(file.toPath());
        assertThat(pathToWorkSpace, is(file.toPath()));
    }

    @Test
    public void getPath_nonExistent() throws IOException {

        String folderName = "newWorkSpaceFolder";
        File file = getNonExistentFile(folderName);

        assertFalse(file.exists());

        Path pathToWorkSpace = WorkSpaceUtils.getPath(file.toPath());

        File workspace = new File(pathToWorkSpace.toUri());
        assertTrue(workspace.exists());
        assertTrue(workspace.isDirectory());
        assertThat(workspace.getName(), is(folderName));

        Files.deleteIfExists(workspace.toPath());
    }

    private File getNonExistentFile(String pathname) throws IOException {
        File file = new File(pathname);
        Path path = file.toPath();
        Files.deleteIfExists(path);
        return file;
    }
}
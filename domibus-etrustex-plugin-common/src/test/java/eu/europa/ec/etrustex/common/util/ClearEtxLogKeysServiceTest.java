package eu.europa.ec.etrustex.common.util;

import eu.domibus.logging.DomibusLogger;
import eu.domibus.logging.DomibusLoggerFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.reflect.MethodSignature;
import org.aspectj.lang.reflect.SourceLocation;
import org.hamcrest.core.Is;
import org.hamcrest.core.IsNull;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.Arrays;

import static eu.europa.ec.etrustex.common.util.LoggingUtils.*;


public class ClearEtxLogKeysServiceTest {

    @ClearEtxLogKeys
    public void clear() {

    }

    @Test
    public void process() {
        DomibusLogger logger = DomibusLoggerFactory.getLogger(ClearEtxLogKeysService.class);
        for (String key : Arrays.asList(MDC_ETX_MESSAGE_UUID, MDC_ETX_ATT_UUID, MDC_ETX_CONVERSATION_ID)) {
            logger.putMDC(key, "TEST");
        }

        Assert.assertThat(logger.getMDC(MDC_ETX_MESSAGE_UUID), Is.is("TEST"));
        Assert.assertThat(logger.getMDC(MDC_ETX_ATT_UUID), Is.is("TEST"));
        Assert.assertThat(logger.getMDC(MDC_ETX_CONVERSATION_ID), Is.is("TEST"));

        new ClearEtxLogKeysService().clearKeys();

        Assert.assertThat(logger.getMDC(MDC_ETX_MESSAGE_UUID), IsNull.nullValue());
        Assert.assertThat(logger.getMDC(MDC_ETX_ATT_UUID), IsNull.nullValue());
        Assert.assertThat(logger.getMDC(MDC_ETX_CONVERSATION_ID), IsNull.nullValue());
    }

    private JoinPoint getJoinPoint() {
        return new JoinPoint() {
            @Override
            public String toShortString() {
                return null;
            }

            @Override
            public String toLongString() {
                return null;
            }

            @Override
            public Object getThis() {
                return null;
            }

            @Override
            public Object getTarget() {
                return null;
            }

            @Override
            public Object[] getArgs() {
                return new Object[0];
            }

            @Override
            public Signature getSignature() {
                return getMethodSignature();
            }

            @Override
            public SourceLocation getSourceLocation() {
                return null;
            }

            @Override
            public String getKind() {
                return null;
            }

            @Override
            public StaticPart getStaticPart() {
                return null;
            }
        };
    }

    private MethodSignature getMethodSignature() {
        return new MethodSignature() {
            @Override
            public String toString() {
                return null;
            }

            @Override
            public String toShortString() {
                return null;
            }

            @Override
            public String toLongString() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public int getModifiers() {
                return 0;
            }

            @Override
            public Class getDeclaringType() {
                return null;
            }

            @Override
            public String getDeclaringTypeName() {
                return null;
            }

            @Override
            public Class[] getParameterTypes() {
                return new Class[0];
            }

            @Override
            public String[] getParameterNames() {
                return new String[0];
            }

            @Override
            public Class[] getExceptionTypes() {
                return new Class[0];
            }

            @Override
            public Class getReturnType() {
                return null;
            }

            @Override
            public Method getMethod() {
                try {
                    return ClearEtxLogKeysServiceTest.class.getMethod("clear");
                } catch (NoSuchMethodException e) {
                    return null;
                }
            }
        };
    }
}
package eu.europa.ec.etrustex.common.converters;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.domibus.ext.domain.UserMessageDTO;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import org.junit.Test;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static eu.europa.ec.etrustex.common.converters.ETrustExAdapterDTOConverter.convertToETrustExAdapterDTO;
import static eu.europa.ec.etrustex.common.testutils.ResourcesUtils.readResource;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 30-Nov-17
 */
public class ETrustExAdapterDTOConverterTest {
    @Test
    public void name() throws Exception {
        UserMessageDTO userMessageDTO = new ObjectMapper().readValue(readResource("data/messagesendfailed/UserMessageDTO.json"), UserMessageDTO.class);
        ETrustExAdapterDTO eTrustExAdapterDTO = convertToETrustExAdapterDTO(userMessageDTO);
        assertThat(
                eTrustExAdapterDTO,
                sameBeanAs(new ObjectMapper().readValue(readResource("data/messagesendfailed/ETrustExAdapterDto.json"), ETrustExAdapterDTO.class))
        );
    }
}
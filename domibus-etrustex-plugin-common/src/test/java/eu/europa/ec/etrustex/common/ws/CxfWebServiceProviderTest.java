package eu.europa.ec.etrustex.common.ws;

import ec.services.wsdl.retrieveinterchangeagreementsrequest_2.RetrieveInterchangeAgreementsRequestPortType;
import mockit.*;
import mockit.integration.junit4.JMockit;
import org.apache.cxf.Bus;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.ext.logging.LoggingFeature;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceFeature;
import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static eu.europa.ec.etrustex.common.ws.CxfWebServiceProvider.CLIENT_AUTHENTICATION_XML;
import static eu.europa.ec.etrustex.common.ws.CxfWebServiceProvider.DOMIBUS_CONFIG_LOCATION;
import static org.apache.commons.codec.binary.StringUtils.getBytesUtf8;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 20/04/2018
 */
@RunWith(JMockit.class)
public class CxfWebServiceProviderTest {

    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    @Tested
    private CxfWebServiceProvider cxfWebServiceProvider;

    @Injectable
    protected Bus bus;

    @Injectable
    protected LoggingFeature loggingFeature;
    private Path pathClientAuth;

    @Before
    public void setUp() {
        pathClientAuth = new File(CxfWebServiceProviderTest.class.getResource("/" + CLIENT_AUTHENTICATION_XML).getFile()).toPath();
        String replace = pathClientAuth.toString()
                .replace("\\" + CLIENT_AUTHENTICATION_XML, "")
                .replace("/" + CLIENT_AUTHENTICATION_XML, "");
        System.setProperty(DOMIBUS_CONFIG_LOCATION, replace);
    }

    @Test
    public void getClientAuthenticationPath() {
        Optional<Path> clientAuthenticationPath = cxfWebServiceProvider.getClientAuthenticationPath();
        assertThat(clientAuthenticationPath, not(Optional.empty()));
    }

    @Test
    public void getClientAuthenticationPath_empty() {
        System.setProperty(DOMIBUS_CONFIG_LOCATION, "");

        Optional<Path> clientAuthenticationPath = cxfWebServiceProvider.getClientAuthenticationPath();
        assertThat(clientAuthenticationPath, is(Optional.empty()));
    }

    @Test
    public void getPropertyDomibusConfigLocation() {
        String propertyDomibusConfigLocation = cxfWebServiceProvider.getPropertyDomibusConfigLocation();
        assertThat(propertyDomibusConfigLocation, not(""));
    }

    @Test
    public void getPropertyDomibusConfigLocation_null() {
        System.setProperty(DOMIBUS_CONFIG_LOCATION, "");
        String propertyDomibusConfigLocation = cxfWebServiceProvider.getPropertyDomibusConfigLocation();
        assertThat(propertyDomibusConfigLocation, is(""));
    }

    @Test
    public void getTlsClientParameters() {
        TLSClientParameters tlsClientParameters = cxfWebServiceProvider.getTlsClientParameters(pathClientAuth);
        assertThat(tlsClientParameters, notNullValue());
    }

    @Test
    public void getTlsClientParameters_null() {
        TLSClientParameters tlsClientParameters = cxfWebServiceProvider.getTlsClientParameters(null);
        assertThat(tlsClientParameters, nullValue());
    }

    @Test
    public void buildJaxWsProxyP2PAwarehttp_https_MtomDisable(final @Mocked ETrustExWsProxyFactoryBean eTrustExWsProxyFactoryBean,
                                                            final @Mocked ClientProxy clientProxy,
                                                            final @Mocked ClientHttpConduitInTest client,
                                                            final @Mocked HTTPConduit httpConduit) {
        final HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
        final boolean isMtomEnabled = false;
        final String serviceEndpoint = "https://serviceEndpoint";
        final Object proxyObject = new Object();
        final Map<String, Object> map = new HashMap<>();

        new Expectations() {{
            ETrustExWsProxyFactoryBean.build(RetrieveInterchangeAgreementsRequestPortType.class,
                    (List<WebServiceFeature>) any, serviceEndpoint, (Bus) any);
            times = 1;
            result = proxyObject;

            ClientProxy.getClient(proxyObject);
            times = 1;
            result = client;

            client.getRequestContext();
            times = 1;
            result = map;

            client.getConduit();
            result = httpConduit;
            times = 3;

            httpConduit.getClient();
            result = httpClientPolicy;
            times = 2;
        }};

        cxfWebServiceProvider.buildJaxWsProxy(
                RetrieveInterchangeAgreementsRequestPortType.class,
                isMtomEnabled,
                serviceEndpoint);

        new FullVerifications() {{
            httpConduit.setTlsClientParameters((TLSClientParameters) any);
            times = 1;
        }};

        assertThat(
                ((Map<String, List<String>>) map.get(Message.PROTOCOL_HEADERS)).get("User-Agent").get(0),
                is("Domibus ETrustEx Connector")
        );
        assertThat(httpClientPolicy.isAllowChunking(), is(false));
    }

    @Test
    public void buildJaxWsProxyP2PAware_p2pFalse_MtomEnable(final @Mocked ETrustExWsProxyFactoryBean eTrustExWsProxyFactoryBean,
                                                            final @Mocked ClientProxy clientProxy,
                                                            final @Mocked ClientHttpConduitInTest client,
                                                            final @Mocked HTTPConduit httpConduit) {
        final HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
        final boolean isMtomEnabled = true;
        final String serviceEndpoint = "serviceEndpoint";
        final Object proxyObject = new Object();
        final Map<String, Object> map = new HashMap<>();

        new Expectations() {{
            ETrustExWsProxyFactoryBean.build(RetrieveInterchangeAgreementsRequestPortType.class,
                    (List) any, anyString, (Bus) any);
            times = 1;
            result = proxyObject;

            ClientProxy.getClient(proxyObject);
            times = 1;
            result = client;

            client.getRequestContext();
            times = 1;
            result = map;

            client.getConduit();
            result = httpConduit;
            times = 2;

            httpConduit.getClient();
            result = httpClientPolicy;
            times = 2;
        }};

        cxfWebServiceProvider.buildJaxWsProxy(
                RetrieveInterchangeAgreementsRequestPortType.class,
                isMtomEnabled,
                serviceEndpoint);

        new FullVerifications() {{
        }};

        assertThat(
                ((Map<String, List<String>>) map.get(Message.PROTOCOL_HEADERS)).get("User-Agent").get(0),
                is("Domibus ETrustEx Connector")
        );
        assertThat(httpClientPolicy.isAllowChunking(), is(true));
    }

    @Test
    public void setupConnectionCredentials(final @Mocked BindingProvider bindingProvider) {
        final Map<String, Object> map = new HashMap<>();
        new Expectations() {{
            bindingProvider.getRequestContext();
            times = 1;
            result = map;
        }};
        cxfWebServiceProvider.setupConnectionCredentials(bindingProvider, USERNAME, getBytesUtf8(PASSWORD));
        new FullVerifications() {{
        }};
        assertThat(map.get(BindingProvider.USERNAME_PROPERTY).toString(), is(USERNAME));
        assertThat(map.get(BindingProvider.PASSWORD_PROPERTY).toString(), is(PASSWORD));
    }

    @Test
    public void httpClientPolicyFound(final @Mocked ClientHttpConduitInTest client, final @Mocked HTTPConduit httpConduit) {
        final HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();

        new Expectations() {{
            client.getConduit();
            result = httpConduit;
            times = 1;

            httpConduit.getClient();
            result = httpClientPolicy;
            times = 1;
        }};

        HTTPClientPolicy httpClient = cxfWebServiceProvider.getHttpClientPolicy(client);

        assertThat(httpClient, is(httpClientPolicy));
        new FullVerifications() {{

        }};
    }

    @Test
    public void httpClientPolicyNotFound(final @Mocked ClientHttpConduitInTest client, final @Mocked HTTPConduit httpConduit) {

        new Expectations() {{
            client.getConduit();
            result = httpConduit;
            times = 1;

            httpConduit.getClient();
            result = null;
            times = 1;

        }};

        HTTPClientPolicy httpClient = cxfWebServiceProvider.getHttpClientPolicy(client);

        assertThat(httpClient, is(notNullValue()));

        new FullVerifications() {{
            httpConduit.setClient((HTTPClientPolicy) any);
            times = 1;
        }};
    }
}
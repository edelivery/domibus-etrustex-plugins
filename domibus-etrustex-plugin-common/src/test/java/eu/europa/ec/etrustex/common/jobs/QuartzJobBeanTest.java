package eu.europa.ec.etrustex.common.jobs;

import eu.domibus.ext.domain.DomainDTO;
import org.junit.Test;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.JobExecutionContextImpl;
import org.quartz.impl.RemoteScheduler;
import org.quartz.impl.triggers.CalendarIntervalTriggerImpl;
import org.quartz.spi.TriggerFiredBundle;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class QuartzJobBeanTest {

    private QuartzJobBeanImplTest quartzJobBeanImplTest = new QuartzJobBeanImplTest();

    @Test
    public void checked_allowed() throws JobExecutionException {
        quartzJobBeanImplTest.setAllowed(true);
        assertFalse(quartzJobBeanImplTest.processed);

        quartzJobBeanImplTest.executeJob(getJobExecutionContext(), new DomainDTO());

        assertTrue(quartzJobBeanImplTest.processed);
    }

    @Test
    public void checked_notAllowed() throws JobExecutionException {
        quartzJobBeanImplTest.setAllowed(false);
        assertFalse(quartzJobBeanImplTest.processed);

        quartzJobBeanImplTest.executeJob(getJobExecutionContext(), new DomainDTO());

        assertFalse(quartzJobBeanImplTest.processed);
    }

    private JobExecutionContextImpl getJobExecutionContext() {
        return new JobExecutionContextImpl(
                new RemoteScheduler("", "", 99),
                new TriggerFiredBundle(
                        new JobDetailImpl(),
                        new CalendarIntervalTriggerImpl(),
                        null,
                        false,
                        null,
                        null,
                        null,
                        null),
                null);
    }

    class QuartzJobBeanImplTest extends QuartzJobBean {

        private boolean allowed;
        private boolean processed = false;

        public void setAllowed(boolean allowed) {
            this.allowed = allowed;
        }

        @Override
        protected DomainInitializer getDomainInitializer() {
            return new DomainInitializer() {
                @Override
                public void init() {

                }
            };
        }

        @Override
        protected boolean isAllowedToRun(String domainName) {
            return allowed;
        }

        @Override
        public void process(JobExecutionContext context) {
            processed = true;
        }
    }

}
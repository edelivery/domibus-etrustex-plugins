package eu.europa.ec.etrustex.common.exceptions;

import oasis.names.specification.ubl.schema.xsd.fault_1.FaultType;
import org.junit.Assert;
import org.junit.Test;

import static eu.europa.ec.etrustex.common.exceptions.ETrustExError.ETX_001;
import static org.hamcrest.CoreMatchers.is;

/**
 * @author François Gautier
 * @version 1.0
 * @since 07/02/2018
 */
public class ErrorTypeConverterTest {
    @Test
    public void name() {
        FaultType result = ErrorTypeConverter.buildFaultType(new ETrustExPluginException(ETX_001, "Description"));
        Assert.assertThat(result.getResponseCode().getValue(), is(ETX_001.getCode()));
        Assert.assertThat(result.getDescription().get(0).getValue(), is(ETX_001.getDescription()));
    }
}
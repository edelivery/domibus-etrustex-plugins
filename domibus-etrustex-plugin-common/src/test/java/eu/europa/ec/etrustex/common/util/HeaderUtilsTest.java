package eu.europa.ec.etrustex.common.util;

import org.junit.Assert;
import org.junit.Test;
import org.unece.cefact.namespaces.standardbusinessdocumentheader.Partner;
import org.unece.cefact.namespaces.standardbusinessdocumentheader.PartnerIdentification;

import java.util.ArrayList;

import static eu.europa.ec.etrustex.common.util.HeaderUtils.extractValidateParty;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.nullValue;

/**
 * @author François Gautier
 * @version 1.0
 * @since 07-Dec-17
 */
public class HeaderUtilsTest {

    public static final String TEST = "TEST";

    @Test
    public void extractValidateParty_null() {
        Assert.assertThat(extractValidateParty(null), nullValue());
    }

    @Test
    public void extractValidateParty_empty() {
        Assert.assertThat(extractValidateParty(new ArrayList<Partner>()), nullValue());
    }

    @Test
    public void extractValidateParty_ok() {
        Assert.assertThat(extractValidateParty(asList(getPartner(TEST), getPartner("Not found"))), containsString(TEST));
    }

    private Partner getPartner(String test) {
        Partner partner = new Partner();
        PartnerIdentification value = new PartnerIdentification();
        value.setValue(test);
        partner.setIdentifier(value);
        return partner;
    }
}
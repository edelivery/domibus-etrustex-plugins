package eu.europa.ec.etrustex.common.util;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 04/05/2018
 */
public class ConversationIdKeyTest {

    @Test
    public void testEnum() {
        ConversationIdKey[] values = ConversationIdKey.values();
        for (ConversationIdKey conversationIdKey : values) {
            conversationIdKey.getConversationIdKey();
        }

        assertThat(values.length, is(7));
    }
}
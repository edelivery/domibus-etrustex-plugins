package eu.europa.ec.etrustex.common.handlers;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static eu.europa.ec.etrustex.common.handlers.Action.*;
import static org.hamcrest.core.Is.is;

/**
 * @author François Gautier
 * @version 1.0
 * @since 07/02/2018
 */

@RunWith(Parameterized.class)
public class ActionTest {

    @Parameter
    public Action action;
    @Parameter(1)
    public String code;

    @Parameters(name = "{index}: Action({0})")
    public static Collection combinations() {
        return Arrays.asList(new Object[][]{
                // Happy flow scenario
                {SUBMIT_APPLICATION_RESPONSE_REQUEST, "SubmitApplicationResponseRequest"},
                {SUBMIT_APPLICATION_RESPONSE_RESPONSE, "SubmitApplicationResponseResponse"},
                {STORE_DOCUMENT_WRAPPER_REQUEST, "StoreDocumentWrapperRequest"},
                {STORE_DOCUMENT_WRAPPER_RESPONSE, "StoreDocumentWrapperResponse"},
                {SUBMIT_DOCUMENT_BUNDLE_REQUEST, "SubmitDocumentBundleRequest"},
                {SUBMIT_DOCUMENT_BUNDLE_RESPONSE, "SubmitDocumentBundleResponse"},
                {RETRIEVE_ICA_REQUEST, "RetrieveICARequest"},
                {RETRIEVE_ICA_RESPONSE, "RetrieveICAResponse"},
                {TRANSMIT_WRAPPER_TO_BACKEND, "TransmitWrapperToBackend"},
                {STATUS_MESSAGE_TRANSMISSION, "StatusMessageTransmission"},
                {NOTIFY_BUNDLE_TO_BACKEND_REQUEST, "NotifyInboxBundle"},
                {NOTIFY_BUNDLE_TO_BACKEND_RESPONSE, "AckInboxBundleNotification"},
                {FAULT_ACTION, "FaultAction"},
                // returns null
                {UNKNOWN, ""},
                {UNKNOWN, null},
        });
    }

    @Test
    public void test() {
        Action result = Action.fromString(code);
        Assert.assertThat(result, is(action));
    }
}
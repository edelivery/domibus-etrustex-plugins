package eu.europa.ec.etrustex.common.ws;

import ec.services.wsdl.retrieveinterchangeagreementsrequest_2.RetrieveInterchangeAgreementsRequestPortType;
import org.junit.Test;

import javax.xml.ws.BindingProvider;
import java.util.ArrayList;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 20/04/2018
 */
public class ETrustExWsProxyFactoryBeanTest {

    @Test
    public void build() {
        Object serviceEndPoints = ETrustExWsProxyFactoryBean.build(
                RetrieveInterchangeAgreementsRequestPortType.class,
                new ArrayList<>(),
                "serviceEndPoints", null);

        Map<String, Object> requestContext = ((BindingProvider) serviceEndPoints).getRequestContext();
        assertThat(requestContext.get("thread.local.request.context").toString(), is("true"));
        assertThat(requestContext.get("schema-validation-enabled").toString(), is("false"));
        assertThat(requestContext.get("set-jaxb-validation-event-handler").toString(), is("false"));

    }
}
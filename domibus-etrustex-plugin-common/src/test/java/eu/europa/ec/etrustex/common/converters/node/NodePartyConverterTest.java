package eu.europa.ec.etrustex.common.converters.node;

import oasis.names.specification.ubl.schema.xsd.commonaggregatecomponents_2.PartyType;
import org.hamcrest.core.Is;
import org.hamcrest.core.IsNull;
import org.junit.Test;

import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 04/05/2018
 */
public class NodePartyConverterTest {

    @Test
    public void happyFlow() {
        PartyType uuid = NodePartyConverter.buildPartyType("uuid");
        assertThat(uuid, IsNull.notNullValue());
        assertThat(uuid.getEndpointID().getValue(), Is.is("uuid"));
    }

    @Test
    public void isNull() {
        PartyType uuid = NodePartyConverter.buildPartyType(null);
        assertThat(uuid, IsNull.nullValue());
    }
}
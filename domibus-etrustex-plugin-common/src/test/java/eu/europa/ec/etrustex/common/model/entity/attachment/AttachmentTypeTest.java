package eu.europa.ec.etrustex.common.model.entity.attachment;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * JUnit class for <code>AttachmentType</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
public class AttachmentTypeTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testValueOf_HappyFlow() {
        //test all the values
        Assert.assertEquals(AttachmentType.valueOf("BINARY"),
                AttachmentType.BINARY);

        Assert.assertEquals(AttachmentType.valueOf("METADATA"),
                AttachmentType.METADATA);
    }

    @Test
    public void testValueOf_UnhappyFlow() {

        //test with a wrong value
        final String wrongValue = "metadata";

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("No enum constant " + AttachmentType.class.getName() + "." + wrongValue);

        Assert.assertNull(AttachmentType.valueOf(wrongValue));

    }

}
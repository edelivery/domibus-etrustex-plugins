package eu.europa.ec.etrustex.common.model;

/**
 * @author François Gautier
 * @version 1.0
 * @since 04/06/2018
 */
public class LargePayloadDTOTest extends AbstractDtoTest<LargePayloadDTO> {

    @Override
    protected LargePayloadDTO getInstance() {
        return new LargePayloadDTO(null, null, null, null, null, null, null);
    }
}
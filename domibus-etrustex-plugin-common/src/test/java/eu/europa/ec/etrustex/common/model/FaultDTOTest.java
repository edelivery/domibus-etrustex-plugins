package eu.europa.ec.etrustex.common.model;

import eu.europa.ec.etrustex.common.exceptions.ETrustExError;

/**
 * @author François Gautier
 * @version 1.0
 * @since 04/06/2018
 */
public class FaultDTOTest extends AbstractDtoTest<FaultDTO> {

    @Override
    protected FaultDTO getInstance() {
        return new FaultDTO(ETrustExError.DEFAULT, null);
    }
}
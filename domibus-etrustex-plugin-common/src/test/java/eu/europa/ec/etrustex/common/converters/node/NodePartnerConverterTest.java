package eu.europa.ec.etrustex.common.converters.node;

import org.hamcrest.core.Is;
import org.hamcrest.core.IsNull;
import org.junit.Test;
import org.unece.cefact.namespaces.standardbusinessdocumentheader.Partner;

import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 04/05/2018
 */
public class NodePartnerConverterTest {

    @Test
    public void happyFlow() {
        Partner uuid = NodePartnerConverter.buildPartnerType("uuid");
        assertThat(uuid, IsNull.notNullValue());
        assertThat(uuid.getIdentifier().getValue(), Is.is("uuid"));
    }

    @Test
    public void isNull() {
        Partner uuid = NodePartnerConverter.buildPartnerType(null);
        assertThat(uuid, IsNull.nullValue());
    }
}
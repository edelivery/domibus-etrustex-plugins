package eu.europa.ec.etrustex.common.jaxb;

import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * JUnit for {@link JodaDateAdapter}
 *
 * @author Catalin Enache
 * @version 1.0
 * @since 29/11/2017
 */
@RunWith(JMockit.class)
public class JodaDateAdapterTest {
    private final int year = 2017;
    private final int month = 6;
    private final int day = 27;
    private final String timezone = "+02:00";

    @Tested
    private JodaDateAdapter jodaDateAdapter;

    @Test
    public void testUnmarshal_HappyFlow() throws Exception {

        //input string is '2017-06-27+02:00'
        final String inputDateStr = String.format("%d-%02d-%d%s", year, month, day, timezone);

        //tested method
        DateTime outputDateTime = jodaDateAdapter.unmarshal(inputDateStr);

        Assert.assertNotNull("resulting dateTime object shouldn't be null", outputDateTime);
        Assert.assertEquals("year must match", year, outputDateTime.year().get());
        Assert.assertEquals("month must match", month, outputDateTime.monthOfYear().get());
        Assert.assertEquals("day must match", day, outputDateTime.dayOfMonth().get());
        Assert.assertEquals("hour must match", 0, outputDateTime.getHourOfDay());
        Assert.assertEquals("minute must match", 0, outputDateTime.getMinuteOfHour());
        Assert.assertEquals("second must match", 0, outputDateTime.getSecondOfMinute());
        Assert.assertEquals("millis must match", 0, outputDateTime.getMillisOfSecond());
        //timezone assigned to local/default
        Assert.assertEquals("timezone must match", DateTimeZone.getDefault().getID(), outputDateTime.getZone().getID());
    }

    @Test
    public void testUnmarshal_NoTimezone() throws Exception {

        //input string is '2017-06-27'
        final String inputDateStr = String.format("%d-%02d-%d", year, month, day);

        //tested method
        DateTime outputDateTime = jodaDateAdapter.unmarshal(inputDateStr);

        Assert.assertNotNull("resulting dateTime object shouldn't be null", outputDateTime);
        Assert.assertEquals("year must match", year, outputDateTime.year().get());
        Assert.assertEquals("month must match", month, outputDateTime.monthOfYear().get());
        Assert.assertEquals("day must match", day, outputDateTime.dayOfMonth().get());
        Assert.assertEquals("hour must match", 0, outputDateTime.getHourOfDay());
        Assert.assertEquals("minute must match", 0, outputDateTime.getMinuteOfHour());
        Assert.assertEquals("second must match", 0, outputDateTime.getSecondOfMinute());
        Assert.assertEquals("millis must match", 0, outputDateTime.getMillisOfSecond());
        //timezone assigned to local/default
        Assert.assertEquals("timezone must match", DateTimeZone.getDefault().getID(), outputDateTime.getZone().getID());
    }

    /**
     * This is mapped to xsd:date format but for adapter compatibility we should accept also full date+time+timezone format
     *
     * @throws Exception
     */
    @Test
    public void testUnmarshal_FullDateTime() throws Exception {
        final int hour = 12;
        final int minute = 13;
        final int second = 14;
        final int millis = 359;

        //input string is '2017-06-27T12:13:14.359+02:00'
        final String inputDateStr = String.format("%d-%02d-%dT%d:%d:%d.%d%s", year, month, day, hour, minute, second, millis, timezone);

        //tested method
        DateTime outputDateTime = jodaDateAdapter.unmarshal(inputDateStr);

        Assert.assertNotNull("resulting dateTime object shouldn't be null", outputDateTime);
        Assert.assertEquals("year must match", year, outputDateTime.year().get());
        Assert.assertEquals("month must match", month, outputDateTime.monthOfYear().get());
        Assert.assertEquals("day must match", day, outputDateTime.dayOfMonth().get());
        Assert.assertEquals("hour must match", hour, outputDateTime.getHourOfDay());
        Assert.assertEquals("minute must match", minute, outputDateTime.getMinuteOfHour());
        Assert.assertEquals("second must match", second, outputDateTime.getSecondOfMinute());
        Assert.assertEquals("millis must match", millis, outputDateTime.getMillisOfSecond());
        //timezone assigned to local/default
        Assert.assertEquals("timezone must match", timezone, outputDateTime.getZone().getID());
    }

    @Test
    public void testUnmarshal_NullInput() throws Exception {
        final String inputDateStr = null;

        //tested method
        DateTime dateTime = jodaDateAdapter.unmarshal(inputDateStr);

        Assert.assertNull("resulting dateTime object should be null", dateTime);
    }

    @Test
    public void testMarshal_HappyFlow() throws Exception {

        final DateTime inputDateTime = new DateTime().
                withDate(year, month, day).
                withZone(DateTimeZone.forID(timezone));

        //"2017-10-27T12:13:14.359"
        final String printedDateTime = String.format("%d-%02d-%d", year, month, day);

        //tested method
        final String outputDateTimeStr = jodaDateAdapter.marshal(inputDateTime);

        Assert.assertNotNull("printed date shouldn't be null", outputDateTimeStr);
        Assert.assertEquals("printed date should match", printedDateTime, outputDateTimeStr);
    }

    @Test
    public void testMarshal_NullInput() throws Exception {
        final DateTime inputDateTime = null;

        //tested method
        final String dateTime = jodaDateAdapter.marshal(inputDateTime);

        Assert.assertNull("printed date shoul be null", dateTime);
    }

}
package eu.europa.ec.etrustex.common.jms;

import eu.domibus.messaging.MessageConstants;
import eu.europa.ec.etrustex.common.exceptions.ETrustExError;
import eu.europa.ec.etrustex.common.util.TransformerUtils;
import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.hamcrest.core.Is;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.TextMessage;

import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * @Author François Gautier
 * @Since 13/07/2017
 * @Version 1.0
 */
@RunWith(JMockit.class)
public class TextMessageConverterTest {

    public static final ETrustExError DUMMY_OBJECT = ETrustExError.DEFAULT;

    @Test
    public void toMessage(final @Mocked Session session, @Mocked TextMessage textMessage) throws JMSException {

        TextMessageConverter textMessageConverter = new TextMessageConverter();
        textMessageConverter.setDomain("TEST");
        new Expectations(){{
            session.createTextMessage(TransformerUtils.serializeObjToXML(DUMMY_OBJECT));
            result = textMessage;
            times = 1;
        }};
        textMessageConverter.toMessage(DUMMY_OBJECT, session);

        new FullVerifications() {{
            textMessage.setStringProperty(MessageConstants.DOMAIN , "TEST");
            times = 1;
        }};
    }

    @Test
    public void fromMessage(final @Mocked TextMessage message) throws JMSException {
        new Expectations() {{
            message.getText();
            result = TransformerUtils.serializeObjToXML(DUMMY_OBJECT);
            times = 2;
        }};

        assertThat(new TextMessageConverter().fromMessage(message), Is.is(DUMMY_OBJECT));

        new FullVerifications(){{}};
    }

    @Test
    public void fromMessage_noText(final @Mocked TextMessage message) throws JMSException {
        new Expectations() {{
            message.getText();
            result = null;
            times = 1;

            message.getStringProperty(MessageConstants.MESSAGE_ID);
            result = null;
            times = 1;
        }};

        assertThat(new TextMessageConverter().fromMessage(message), nullValue());

        new FullVerifications(){{}};
    }

    @Test
    public void fromMessage_noTextJmsException(final @Mocked TextMessage message) throws JMSException {
        new Expectations() {{
            message.getText();
            result = null;
            times = 1;

            message.getStringProperty(MessageConstants.MESSAGE_ID);
            result = new JMSException("");
            times = 1;
        }};

        assertThat(new TextMessageConverter().fromMessage(message), nullValue());

        new FullVerifications(){{}};
    }

    @Test
    public void fromMessage_noTextWithMessageId(final @Mocked TextMessage message) throws JMSException {
        new Expectations() {{
            message.getText();
            result = null;
            times = 1;

            message.getStringProperty(MessageConstants.MESSAGE_ID);
            result = "ID";
            times = 2;
        }};

        assertThat((ErrorQueueMessage) new TextMessageConverter().fromMessage(message), IsInstanceOf.any(ErrorQueueMessage.class));

        new FullVerifications(){{}};
    }
}

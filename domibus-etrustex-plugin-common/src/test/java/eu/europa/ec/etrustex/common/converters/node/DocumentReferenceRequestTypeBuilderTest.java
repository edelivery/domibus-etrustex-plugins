package eu.europa.ec.etrustex.common.converters.node;

import ec.schema.xsd.commonaggregatecomponents_2.DocumentReferenceRequestType;
import eu.europa.ec.etrustex.common.model.entity.attachment.AttachmentType;
import org.hamcrest.core.Is;
import org.hamcrest.core.IsNull;
import org.junit.Test;

import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 04/05/2018
 */
public class DocumentReferenceRequestTypeBuilderTest {

    @Test
    public void happyFlow() {
        DocumentReferenceRequestType uuid = DocumentReferenceRequestTypeBuilder.buildDocumentReferenceRequest("uuid", AttachmentType.BINARY);
        assertThat(uuid, IsNull.notNullValue());
        assertThat(uuid.getID().getValue(), Is.is("uuid"));
        assertThat(uuid.getDocumentTypeCode().getValue(), Is.is(AttachmentType.BINARY.name()));
    }

    @Test(expected = NullPointerException.class)
    public void isNull() {
        DocumentReferenceRequestTypeBuilder.buildDocumentReferenceRequest(null, null);
    }
}
package eu.europa.ec.etrustex.common.model;

/**
 * @author François Gautier
 * @version 1.0
 * @since 04/06/2018
 */
public class PayloadDTOTest extends AbstractDtoTest<PayloadDTO> {

    @Override
    protected PayloadDTO getInstance() {
        return new PayloadDTO(null, null, true, null, null);
    }
}
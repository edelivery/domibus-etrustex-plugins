package eu.europa.ec.etrustex.common.ws;

import org.apache.cxf.Bus;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.service.model.EndpointInfo;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.ws.addressing.EndpointReferenceType;

import java.io.IOException;

/**
 * @author François Gautier
 * @version 1.0
 * @since 20/04/2018
 */
public abstract class ClientHttpConduitInTest extends HTTPConduit implements Client {
    public ClientHttpConduitInTest(Bus b, EndpointInfo ei) throws IOException {
        super(b, ei);
    }

    public ClientHttpConduitInTest(Bus b, EndpointInfo ei, EndpointReferenceType t) throws IOException {
        super(b, ei, t);
    }
}

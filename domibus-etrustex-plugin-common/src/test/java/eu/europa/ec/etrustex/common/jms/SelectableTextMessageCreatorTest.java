package eu.europa.ec.etrustex.common.jms;

import mockit.Expectations;
import mockit.FullVerifications;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 * @author François Gautier
 * @version 1.0
 * @since 04/05/2018
 */
@RunWith(JMockit.class)
public class SelectableTextMessageCreatorTest {
    private static final String XML = "xml";
    private static final String SELECTING_PROP = "selectingProp";
    private static final String SELECTED_VALUE = "selectedValue";

    @Mocked
    private Session session;

    @Mocked
    private TextMessage textMessage;

    @Test
    public void createMessage() throws JMSException {
        new Expectations() {{
            session.createTextMessage(XML);
            times = 1;
            result = textMessage;
        }};

        new SelectableTextMessageCreator(XML, SELECTING_PROP, SELECTED_VALUE).createMessage(session);

        new FullVerifications() {{
            textMessage.setStringProperty(SELECTING_PROP, SELECTED_VALUE);
            times = 1;
        }};
    }

}
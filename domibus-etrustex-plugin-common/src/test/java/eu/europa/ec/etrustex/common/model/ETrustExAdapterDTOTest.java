package eu.europa.ec.etrustex.common.model;

import eu.europa.ec.etrustex.common.testutils.PayloadDTOBuilder;
import eu.europa.ec.etrustex.common.util.ContentId;
import org.hamcrest.core.Is;
import org.hamcrest.core.IsNull;
import org.junit.Assert;
import org.junit.Test;

import java.nio.charset.StandardCharsets;

/**
 * @author François Gautier
 * @version 1.0
 * @since 04/06/2018
 */
public class ETrustExAdapterDTOTest extends AbstractDtoTest<ETrustExAdapterDTO> {

    @Override
    protected ETrustExAdapterDTO getInstance() {
        return new ETrustExAdapterDTO();
    }

    @Test
    public void getUuid_empty() {
        String uuid = ETrustExAdapterDTO.getUuid(new ETrustExAdapterDTO());
        Assert.assertThat(uuid, IsNull.nullValue());
    }

    @Test
    public void getUuid_null() {
        String uuid = ETrustExAdapterDTO.getUuid(null);
        Assert.assertThat(uuid, Is.is(""));
    }

    @Test
    public void getUuid_refUuid() {
        ETrustExAdapterDTO eTrustExAdapterResponseDTO = geteTrustExAdapterDTO("As4RefToMessageId", "As4MessageId");

        String uuid = ETrustExAdapterDTO.getUuid(eTrustExAdapterResponseDTO);
        Assert.assertThat(uuid, Is.is("As4RefToMessageId"));
    }

    private ETrustExAdapterDTO geteTrustExAdapterDTO(String as4RefToMessageId, String as4MessageId) {
        ETrustExAdapterDTO eTrustExAdapterResponseDTO = new ETrustExAdapterDTO();
        eTrustExAdapterResponseDTO.setAs4RefToMessageId(as4RefToMessageId);
        eTrustExAdapterResponseDTO.setAs4MessageId(as4MessageId);
        return eTrustExAdapterResponseDTO;
    }

    @Test
    public void getUuid_null_As4MessageId() {
        ETrustExAdapterDTO eTrustExAdapterResponseDTO = geteTrustExAdapterDTO(null, "As4MessageId");

        String uuid = ETrustExAdapterDTO.getUuid(eTrustExAdapterResponseDTO);
        Assert.assertThat(uuid, Is.is("As4MessageId"));
    }

    @Test
    public void invert() {
        ETrustExAdapterDTO dto = new ETrustExAdapterDTO();
        dto.setAs4FromPartyId("getAs4FromPartyId");
        dto.setAs4FromPartyIdType("getAs4FromPartyIdType");
        dto.setAs4FromPartyRole("getAs4FromPartyRole");
        dto.setAs4ToPartyId("getAs4ToPartyId");
        dto.setAs4ToPartyIdType("getAs4ToPartyIdType");
        dto.setAs4ToPartyRole("getAs4ToPartyRole");

        ETrustExAdapterDTO invert = dto.invert();

        Assert.assertThat(invert.getAs4FromPartyId(), Is.is(dto.getAs4ToPartyId()));
        Assert.assertThat(invert.getAs4FromPartyIdType(), Is.is(dto.getAs4ToPartyIdType()));
        Assert.assertThat(invert.getAs4FromPartyRole(), Is.is(dto.getAs4ToPartyRole()));

        Assert.assertThat(invert.getAs4ToPartyId(), Is.is(dto.getAs4FromPartyId()));
        Assert.assertThat(invert.getAs4ToPartyIdType(), Is.is(dto.getAs4FromPartyIdType()));
        Assert.assertThat(invert.getAs4ToPartyRole(), Is.is(dto.getAs4FromPartyRole()));
    }

    @Test
    public void clearFaultPayloads() {
        ETrustExAdapterDTO dto = new ETrustExAdapterDTO();

        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_FAULT)
                .withPayload("test".getBytes((StandardCharsets.UTF_8)))
                .build());

        dto.clearFaultPayloads();

        Assert.assertFalse(dto.hasFaultPayload());
    }

    @Test
    public void hasFaultPayload_true() {
        ETrustExAdapterDTO dto = new ETrustExAdapterDTO();

        dto.addAs4Payload(PayloadDTOBuilder
                .getInstance()
                .withContentId(ContentId.CONTENT_FAULT)
                .withPayload("test".getBytes((StandardCharsets.UTF_8)))
                .build());

        Assert.assertTrue(dto.hasFaultPayload());
    }

    @Test
    public void hasFaultPayload_false() {
        ETrustExAdapterDTO dto = new ETrustExAdapterDTO();

        Assert.assertFalse(dto.hasFaultPayload());
    }
}
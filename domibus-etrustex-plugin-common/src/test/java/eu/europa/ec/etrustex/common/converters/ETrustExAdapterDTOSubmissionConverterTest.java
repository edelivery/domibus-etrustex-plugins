package eu.europa.ec.etrustex.common.converters;

import eu.domibus.plugin.Submission;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.LargePayloadDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.testutils.ResourcesUtils;
import eu.europa.ec.etrustex.common.util.AS4MessageConstants;
import mockit.FullVerifications;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.activation.DataHandler;
import javax.validation.constraints.Null;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import static eu.europa.ec.etrustex.common.util.AS4MessageConstants.*;
import static eu.europa.ec.etrustex.common.util.ContentId.CONTENT_ID_DOCUMENT;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * JUnit class for <code>ETrustExAdapterDTOSubmissionConverter</code>
 *
 * @author Catalin Enache
 * @version 1.0
 */
@RunWith(JMockit.class)
public class ETrustExAdapterDTOSubmissionConverterTest {

    private static final String PROPERTY_FINAL_RECIPIENT_VALUE = "PROPERTY_FINAL_RECIPIENT";
    private static final String PROPERTY_ORIGINAL_SENDER_VALUE = "PROPERTY_ORIGINAL_SENDER";
    private static final String PROPERTY_ETRUSTEX_BACKEND_MESSAGE_ID_VALUE = "PROPERTY_ETRUSTEX_BACKEND_MESSAGE_ID";
    private static final String PROPERTY_FILE_NAME_VALUE = "PROPERTY_FILE_NAME";
    private static final String PROPERTY_ETRUSTEX_ATTACHMENT_UUIDS_VALUE = "PROPERTY_ETRUSTEX_ATTACHMENT_UUIDS";
    private static final String messageId = "message Id 123";
    private static final String refToMessageId = "ref to messageId123";
    private static final String conversationId = "conversation Id 123";
    private static final String fromRole = "from role test";
    private static final String toRole = "to role test";
    private static final String partyId1 = "partyId 1";
    private static final String partyId2 = "partyId 2";
    private static final String partyId3 = "partyId 3";
    private static final String partyId4 = "partyId 4";
    private static final String partyIdType1 = "partyIdType 1";
    private static final String partyIdType2 = "partyIdType 2";
    private static final String partyIdType3 = "partyIdType 3";
    private static final String partyIdType4 = "partyIdType 4";
    private static final String agreementRef = "agreementRef test";
    private static final String service = "service test";
    private static final String serviceType = "serviceType test";
    private static final String action = "action test";
    private static final String fileName = "data/payload.xml";
    private static final String fileType = "application/xml";
    private static final String dtoDescription = "dto description";
    private static final Locale dtoLocale = Locale.getDefault();
    private static final Submission submission = new Submission();

    @Tested
    private ETrustExAdapterDTOSubmissionConverter converter;


    @Before
    public void setUp() {
        submission.setMessageId(messageId);
        submission.setRefToMessageId(refToMessageId);
        submission.setConversationId(conversationId);
        submission.setFromRole(fromRole);
        submission.addFromParty(partyId1, partyIdType1);
        submission.addFromParty(partyId2, partyIdType2);
        submission.addToParty(partyId3, partyIdType3);
        submission.addToParty(partyId4, partyIdType4);
        submission.setToRole(toRole);
        submission.setAgreementRef(agreementRef);
        submission.setService(service);
        submission.setServiceType(serviceType);
        submission.setAction(action);

        submission.addMessageProperty(PROPERTY_FINAL_RECIPIENT, PROPERTY_FINAL_RECIPIENT_VALUE);
        submission.addMessageProperty(PROPERTY_ORIGINAL_SENDER, PROPERTY_ORIGINAL_SENDER_VALUE);
        submission.addMessageProperty(PROPERTY_ETRUSTEX_BACKEND_MESSAGE_ID, PROPERTY_ETRUSTEX_BACKEND_MESSAGE_ID_VALUE);
        submission.addMessageProperty(PROPERTY_FILE_NAME, PROPERTY_FILE_NAME_VALUE);
        submission.addMessageProperty(PROPERTY_ETRUSTEX_ATTACHMENT_UUIDS, PROPERTY_ETRUSTEX_ATTACHMENT_UUIDS_VALUE);
    }

    @Test(expected = NullPointerException.class)
    public void transformToSubmission_partyIdEmpty() {
        converter.transformToSubmission(new ETrustExAdapterDTO());
    }

    @Test(expected = IllegalArgumentException.class)
    public void transformToSubmission_roleEmpty() {
        ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        eTrustExAdapterDTO.setAs4FromPartyId("ID");
        eTrustExAdapterDTO.setAs4ToPartyId("ID");
        converter.transformToSubmission(eTrustExAdapterDTO);
    }

    @Test(expected = IllegalArgumentException.class)
    public void transformToSubmission_serviceEmpty() {
        ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        eTrustExAdapterDTO.setAs4FromPartyId("ID");
        eTrustExAdapterDTO.setAs4FromPartyRole("ROLE");
        eTrustExAdapterDTO.setAs4ToPartyId("ID");
        eTrustExAdapterDTO.setAs4ToPartyRole("ROLE");
        converter.transformToSubmission(eTrustExAdapterDTO);
    }

    @Test(expected = IllegalArgumentException.class)
    public void transformToSubmission_actionEmpty() {
        ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        eTrustExAdapterDTO.setAs4FromPartyId("ID");
        eTrustExAdapterDTO.setAs4FromPartyRole("ROLE");
        eTrustExAdapterDTO.setAs4ToPartyId("ID");
        eTrustExAdapterDTO.setAs4ToPartyRole("ROLE");

        eTrustExAdapterDTO.setAs4Service("Service");

        converter.transformToSubmission(eTrustExAdapterDTO);
    }

    @Test
    public void transformToSubmission_happyFlow() throws IOException {
        final String testMimeType = "application/pdf";

        final DataHandler payloadDataHandler = new DataHandler(new javax.mail.util.ByteArrayDataSource(ResourcesUtils.readResource(fileName), fileType));
        Submission.TypedProperty mimeTypeProperty = new Submission.TypedProperty(AS4MessageConstants.MIME_TYPE, testMimeType, CONTENT_ID_DOCUMENT.getValue());
        Collection<Submission.TypedProperty> etxAS4GenericPayloadProperties = new ArrayList<>();
        etxAS4GenericPayloadProperties.add(mimeTypeProperty);
        submission.addPayload(CONTENT_ID_DOCUMENT.getValue(), payloadDataHandler, etxAS4GenericPayloadProperties, false,
                new Submission.Description(dtoLocale, dtoDescription), StringUtils.EMPTY);

        Submission submission = converter.transformToSubmission(converter.transformFromSubmission(ETrustExAdapterDTOSubmissionConverterTest.submission, new ETrustExAdapterDTO()));

        assertEquals(messageId, submission.getMessageId());
        assertEquals(refToMessageId, submission.getRefToMessageId());
        assertEquals(conversationId, submission.getConversationId());
        Submission.Party from = submission.getFromParties().iterator().next();
        assertEquals(partyId2, from.getPartyId());
        assertEquals(partyIdType2, from.getPartyIdType());
        assertEquals(fromRole, submission.getFromRole());

        Submission.Party to = submission.getToParties().iterator().next();
        assertEquals(partyId4, to.getPartyId());
        assertEquals(partyIdType4, to.getPartyIdType());
        assertEquals(toRole, submission.getToRole());
        assertEquals(agreementRef, submission.getAgreementRef());
        assertEquals(service, submission.getService());
        assertEquals(serviceType, submission.getServiceType());
        assertEquals(action, submission.getAction());

        Submission.Payload next = submission.getPayloads().iterator().next();
        assertEquals(CONTENT_ID_DOCUMENT.getValue(), next.getContentId());
        assertEquals(dtoDescription, next.getDescription().getValue());

        for (Submission.TypedProperty typedProperty : submission.getMessageProperties()) {
            if(typedProperty.getKey().equals(PROPERTY_FINAL_RECIPIENT)){
                assertThat(typedProperty.getValue(), is(PROPERTY_FINAL_RECIPIENT_VALUE));
            }
            if(typedProperty.getKey().equals(PROPERTY_ORIGINAL_SENDER)){
                assertThat(typedProperty.getValue(), is(PROPERTY_ORIGINAL_SENDER_VALUE));
            }
            if(typedProperty.getKey().equals(PROPERTY_ETRUSTEX_BACKEND_MESSAGE_ID)){
                assertThat(typedProperty.getValue(), is(PROPERTY_ETRUSTEX_BACKEND_MESSAGE_ID_VALUE));
            }
            if(typedProperty.getKey().equals(PROPERTY_FILE_NAME)){

                assertThat(typedProperty.getValue(), is(PROPERTY_FILE_NAME_VALUE));
            }
            if(typedProperty.getKey().equals(PROPERTY_ETRUSTEX_ATTACHMENT_UUIDS)){
                assertThat(typedProperty.getValue(), is(PROPERTY_ETRUSTEX_ATTACHMENT_UUIDS_VALUE));
            }
        }
    }

    /**
     * Scenario for successfully testing of the transformation from <code>Submission</code> to <code>ETrustExAdapterDTO</code>
     */
    @Test
    public void testTransformFromSubmission_HappyFlow() throws Exception {

        final String testMimeType = "application/pdf";
        final DataHandler payloadDataHandler = new DataHandler(new javax.mail.util.ByteArrayDataSource(ResourcesUtils.readResource(fileName), fileType));
        Submission.TypedProperty mimeTypeProperty = new Submission.TypedProperty(AS4MessageConstants.MIME_TYPE, testMimeType, CONTENT_ID_DOCUMENT.getValue());
        Collection<Submission.TypedProperty> etxAS4GenericPayloadProperties = new ArrayList<>();
        etxAS4GenericPayloadProperties.add(mimeTypeProperty);
        submission.addPayload(CONTENT_ID_DOCUMENT.getValue(), payloadDataHandler, etxAS4GenericPayloadProperties, false,
                new Submission.Description(dtoLocale, dtoDescription), StringUtils.EMPTY);
        ETrustExAdapterDTO dto = new ETrustExAdapterDTO();

        //method to be tested
        ETrustExAdapterDTO eTrustExAdapterDTOActual = converter.transformFromSubmission(submission, dto);
        assertNotNull(eTrustExAdapterDTOActual);

        new FullVerifications() {{

        }};
        assertEquals(messageId, dto.getAs4MessageId());
        assertEquals(refToMessageId, dto.getAs4RefToMessageId());
        assertEquals(conversationId, dto.getAs4ConversationId());
        assertEquals(partyId2, dto.getAs4FromPartyId());
        assertEquals(partyIdType2, dto.getAs4FromPartyIdType());
        assertEquals(fromRole, dto.getAs4FromPartyRole());
        assertEquals(partyId4, dto.getAs4ToPartyId());
        assertEquals(partyIdType4, dto.getAs4ToPartyIdType());
        assertEquals(toRole, dto.getAs4ToPartyRole());
        assertEquals(agreementRef, dto.getAs4AgreementRef());
        assertEquals(service, dto.getAs4Service());
        assertEquals(serviceType, dto.getAs4ServiceType());
        assertEquals(action, dto.getAs4Action());

        PayloadDTO payloadDTO = dto.getPayloadFromCID(CONTENT_ID_DOCUMENT);
        assertEquals("LargePayloadDTO should be added", LargePayloadDTO.class, payloadDTO.getClass());
        assertEquals(CONTENT_ID_DOCUMENT.getValue(), payloadDTO.getContentId());
        assertEquals(dtoDescription, payloadDTO.getDescription());
        assertEquals(dtoLocale, payloadDTO.getLocale());
        assertEquals(payloadDataHandler, payloadDTO.getPayloadDataHandler());
        assertEquals(fileType, payloadDTO.getPayloadDataHandler().getContentType());
        assertEquals(testMimeType, payloadDTO.getMimeType());
        assertThat(((LargePayloadDTO) payloadDTO).getFileName(), is(PROPERTY_FILE_NAME_VALUE));

        assertThat(dto.getAs4FinalRecipient(), is(PROPERTY_FINAL_RECIPIENT_VALUE));
        assertThat(dto.getAs4OriginalSender(), is(PROPERTY_ORIGINAL_SENDER_VALUE));
        assertThat(dto.getEtxBackendMessageId(), is(PROPERTY_ETRUSTEX_BACKEND_MESSAGE_ID_VALUE));
        assertThat(dto.getAttachmentId(), is(PROPERTY_ETRUSTEX_ATTACHMENT_UUIDS_VALUE));
    }

}
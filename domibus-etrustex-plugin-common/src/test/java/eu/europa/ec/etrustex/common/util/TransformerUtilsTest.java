package eu.europa.ec.etrustex.common.util;

import com.thoughtworks.xstream.converters.ConversionException;
import ec.schema.xsd.commonaggregatecomponents_2.HeaderType;
import ec.services.wsdl.documentbundle_2.FaultResponse;
import eu.europa.ec.etrustex.common.exceptions.ETrustExPluginException;
import eu.europa.ec.etrustex.common.model.ETrustExAdapterDTO;
import eu.europa.ec.etrustex.common.model.PayloadDTO;
import eu.europa.ec.etrustex.common.testutils.ResourcesUtils;
import oasis.names.specification.ubl.schema.xsd.fault_1.FaultType;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static eu.europa.ec.etrustex.common.testutils.ResourcesUtils.readResource;
import static eu.europa.ec.etrustex.common.util.AS4MessageConstants.MIME_TYPE_APP_OCTET;
import static eu.europa.ec.etrustex.common.util.AS4MessageConstants.MIME_TYPE_TEXT_XML;
import static eu.europa.ec.etrustex.common.util.ContentId.*;
import static eu.europa.ec.etrustex.common.util.PayloadDescription.*;
import static eu.europa.ec.etrustex.common.util.TransformerUtils.*;
import static org.apache.commons.codec.binary.StringUtils.getBytesUtf8;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.*;

/**
 * @Author François Gautier
 * @Since 25/08/2017
 * @Version 1.0
 */
public class TransformerUtilsTest {
    @Test(expected = ConversionException.class)
    public void externalizableConverter() {
        deserializeXMLToObj(readResource("data/fault/EtrustExDTO_XStreamInError.xml"));
    }

    @Test(expected = ConversionException.class)
    public void externalizableConverter2() {
        deserializeXMLToObj(readResource("data/fault/SoapFaultExtNode_XStreamInError.xml"));
    }

    @Test
    public void cdata() {
        Object faultResponse = deserializeXMLToObj(
                readResource("data/sendMessageBundleRequestWithSignature.xml"));
        PayloadDTO faultResponsePayloadDTO = getFaultResponsePayloadDTO(faultResponse);
        assertThat(
                deserializeXMLToObj(faultResponsePayloadDTO.getPayloadAsString()),
                sameBeanAs(faultResponse));
    }

    @Test
    public void extractETrustExXMLPayloadXStream_happyFlow() {
        ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        eTrustExAdapterDTO.addAs4Payload(getPayloadDTO(CONTENT_ID_GENERIC, serializeObjToXML(new HeaderType())));
        HeaderType result = extractETrustExXMLPayload(CONTENT_ID_GENERIC, eTrustExAdapterDTO);

        assertThat(result, is(notNullValue()));
    }

    @Test
    public void extractETrustExXMLPayloadXStream_noPayloadDTO() {
        Object result = extractETrustExXMLPayload(CONTENT_ID_GENERIC, new ETrustExAdapterDTO());
        assertThat(result, is(nullValue()));
    }

    @Test
    public void extractETrustExXMLPayloadXStream_null() {
        Object result = extractETrustExXMLPayload(CONTENT_ID_GENERIC, null);
        //noinspection ConstantConditions
        assertThat(result, is(nullValue()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getPayloadAsXML_empty() {
        String result = getPayloadAsXML(new PayloadDTO(CONTENT_ID_GENERIC.getValue(), getBytesUtf8(""), "", true, ""));
        assertThat(result, is(nullValue()));
    }

    @Test
    public void getLargePayloadDTO_happyFlow() {
        PayloadDTO test = getLargePayloadDTO(new File(""), "Test", MIME_TYPE_TEXT_XML);
        assertThat(test.getContentId(), is(CONTENT_ID_DOCUMENT.getValue()));
        assertThat(test.getMimeType(), is(MIME_TYPE_TEXT_XML));
        assertThat(test.isInBody(), is(false));
        assertThat(test.getDescription(), is("Test"));
        assertThat(test.getLocale(), is(Locale.getDefault()));
    }

    @Test
    public void getLargePayloadDTO_null() {
        PayloadDTO test = getLargePayloadDTO(null, null, null);
        assertThat(test, is(nullValue()));
    }

    @Test
    public void getGenericPayloadDTO_happyFlow() {
        PayloadDTO test = getGenericPayloadDTO("TEST");
        assertThat(test.getContentId(), is(CONTENT_ID_GENERIC.getValue()));
        assertThat(test.getMimeType(), is(MIME_TYPE_TEXT_XML));
        assertThat(test.isInBody(), is(false));
        assertThat(test.getDescription(), is(PayloadDescription.ETRUSTEX_GENERIC_PAYLOAD.getValue()));
        assertThat(test.getLocale(), is(Locale.getDefault()));
        assertThat(test.getPayload(), is(getBytesUtf8("TEST")));
    }

    @Test
    public void getGenericPayloadDTO_null() {
        PayloadDTO test = getGenericPayloadDTO(null);
        assertThat(test, is(nullValue()));
    }

    @Test
    public void getHeaderPayloadDTO_happyFlow() {
        PayloadDTO test = getHeaderPayloadDTO("TEST");
        assertThat(test.getContentId(), is(CONTENT_ID_HEADER.getValue()));
        assertThat(test.getMimeType(), is(MIME_TYPE_TEXT_XML));
        assertThat(test.isInBody(), is(false));
        assertThat(test.getDescription(), is(ETRUSTEX_HEADER_PAYLOAD.getValue()));
        assertThat(test.getLocale(), is(Locale.getDefault()));
        assertThat(test.getPayload(), is(getBytesUtf8("TEST")));
    }

    @Test
    public void getHeaderPayloadDTO_null() {
        PayloadDTO test = getHeaderPayloadDTO(null);
        assertThat(test, is(nullValue()));
    }

    @Test
    public void getFaultResponsePayloadDTO_happyFlow() {
        Object faultResponse = deserializeXMLToObj(readResource("data/fault/soapFault.xml"));
        PayloadDTO faultResponsePayloadDTO = getFaultResponsePayloadDTO(faultResponse);
        assertThat(
                deserializeXMLToObj(faultResponsePayloadDTO.getPayloadAsString()),
                sameBeanAs(faultResponse));
    }

    @Test
    public void extractETrustExXMLPayloadFaultResponse_happyFlow() {
        ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        eTrustExAdapterDTO.addAs4Payload(getPayloadDTO(CONTENT_ID_FAULT_RESPONSE, readResource("data/fault/soapFault.xml")));
        FaultResponse actual = extractETrustExXMLPayloadFaultResponse(eTrustExAdapterDTO, new FaultResponse("Default FaultResponse", new FaultType()));
        assertThat(actual, is(notNullValue()));
    }

    @Test
    public void extractETrustExXMLPayloadFaultResponse_wrongClass() {
        ETrustExAdapterDTO eTrustExAdapterDTO = (ETrustExAdapterDTO) deserializeXMLToObj(readResource("data/fault/ETrustExAdapterDTOFaultResponse.xml"));
        assertThat(extractETrustExXMLPayloadFaultResponse(eTrustExAdapterDTO, new Object()), is(notNullValue()));
    }

    @Test
    public void extractETrustExXMLPayloadFaultResponse_notXml() {
        ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        eTrustExAdapterDTO.addAs4Payload(getPayloadDTO(CONTENT_ID_FAULT_RESPONSE, "TEST"));
        assertThat(extractETrustExXMLPayloadFaultResponse(eTrustExAdapterDTO, new Object()), is(notNullValue()));
    }

    @Test
    public void extractETrustExXMLPayloadFaultResponse_WrongContentId() {
        ETrustExAdapterDTO eTrustExAdapterDTO = new ETrustExAdapterDTO();
        eTrustExAdapterDTO.addAs4Payload(getPayloadDTO(CONTENT_ID_GENERIC, "TEST"));
        assertThat(extractETrustExXMLPayloadFaultResponse(eTrustExAdapterDTO, new Object()), is(nullValue()));
    }

    private PayloadDTO getPayloadDTO(ContentId contentIdGeneric, String test) {
        return new PayloadDTO(
                contentIdGeneric.getValue(),
                getBytesUtf8(test),
                MIME_TYPE_TEXT_XML,
                false,
                ETRUSTEX_FAULT_RESPONSE_PAYLOAD.getValue()
        );
    }

    @Test
    public void extractETrustExXMLPayloadFaultResponse_emptyDto() {
        assertThat(extractETrustExXMLPayloadFaultResponse(new ETrustExAdapterDTO(), new Object()), is(nullValue()));
    }

    @Test
    public void extractETrustExXMLPayloadFaultResponse_nullDto() {
        assertThat(extractETrustExXMLPayloadFaultResponse(null, new Object()), is(nullValue()));
    }

    @Test
    public void getObjectFromXml_Empty() {
        try {
            TransformerUtils.getObjectFromXml("");
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getCause().getMessage(), is("Empty xml string"));
        }
    }

    @Test
    public void getObjectFromXml_NoTag() {
        try {
            TransformerUtils.getObjectFromXml("non parseable text");
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getMessage(), is("Xml could not be parsed"));
        }
    }

    @Test
    public void getObjectFromXml_withString() {
        try {
            TransformerUtils.getObjectFromXml("<tag>Parseable content</tag>");
            fail();
        } catch (ETrustExPluginException e) {
            assertThat(e.getMessage(), is("Xml could not be parsed"));
        }
    }

    @Test
    public void getFaultPayloadDTO() {
        String test = "test";
        PayloadDTO payload = TransformerUtils.getFaultPayloadDTO(test);

        assertThat(payload.getDescription(), is(ETRUSTEX_FAULT_PAYLOAD.getValue()));
        assertThat(payload.getContentId(), is(CONTENT_FAULT.getValue()));
        assertThat(payload.getLocale(), is(Locale.getDefault()));
        assertThat(payload.getMimeType(), is(MIME_TYPE_APP_OCTET));
        assertThat(payload.getPayloadAsString(), is(test));
        assertThat(payload.getPayload(), is(test.getBytes()));
        assertThat(payload.getPayloadDataHandler(), CoreMatchers.notNullValue());

    }

    @Test
    public void deserializeXMLToObj_JMSErrorString() throws IOException {
        final String faultErrorJMSTextMsg = ResourcesUtils.readResource("data/fault/ETrustExAdapterDTO_InternalServerErrorAtEtxNode.xml");
        ETrustExAdapterDTO o = (ETrustExAdapterDTO) deserializeXMLToObj(faultErrorJMSTextMsg);
        assertNotNull(o);
    }
}
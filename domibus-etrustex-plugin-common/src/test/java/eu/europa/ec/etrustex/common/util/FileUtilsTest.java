package eu.europa.ec.etrustex.common.util;

import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;

import static eu.europa.ec.etrustex.common.util.FileUtils.deleteIfOld;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author François Gautier
 * @version 1.0
 * @since 18-Jan-18
 */
@RunWith(JMockit.class)
public class FileUtilsTest {
    private static final Long DAY_IN_MS = 24L * 60L * 60L * 1000L;

    private Path tempDir;

    @Before
    public void setUp() throws Exception {
        tempDir = Files.createTempDirectory(this.getClass().getSimpleName());
    }

    @After
    public void onTearDown() throws Exception {
        Files.walkFileTree(tempDir, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    private void updatePathAccessTimes(Path path, long timeInMillis) throws IOException {
        final BasicFileAttributeView fileAttributeView = Files.getFileAttributeView(path, BasicFileAttributeView.class);
        final BasicFileAttributes fileAttributes = fileAttributeView.readAttributes();
        FileTime newTime = FileTime.fromMillis(fileAttributes.lastModifiedTime().toMillis() + timeInMillis);
        fileAttributeView.setTimes(newTime, newTime, newTime);
    }

    @Test
    public void deleteOldFilesInDirectory_IoException(@Mocked final Files files) throws IOException {
        final Path base = Paths.get(tempDir.toString(), "base");
        new Expectations(){{
            Files.walkFileTree(base, (FileVisitor<? super Path>) any);
            result = new IOException("IO Error");
            times = 1;
        }};

        // when
        FileUtils.deleteOldFilesInDirectory(base, DAY_IN_MS);

    }

    @Test
    public void deleteIfOld_IoException(@Mocked final Files files) {
        final Path base = Paths.get(tempDir.toString(), "base");
        new Expectations(){{
            Files.getFileAttributeView((Path) any, BasicFileAttributeView.class);
            result = new IOException("IO Error");
            times = 1;
        }};

        // when
        FileUtils.deleteIfOld(base, DAY_IN_MS);

    }

    @Test
    public void test_deleteOldFilesInDirectory_should_tryToDeleteOld_when_notDirectory() throws IOException {
        // given
        Path baseDir = Paths.get(tempDir.toString(), "base");
        Files.createDirectories(baseDir);

        Path fileToDelete = Paths.get(baseDir.toString(), "someOldFile.txt");
        Files.createFile(fileToDelete);
        updatePathAccessTimes(fileToDelete, -DAY_IN_MS);

        Path newFilePath = Files.createFile(Paths.get(baseDir.toString(), "someNewFile.txt"));

        Path oldDirPath = Paths.get(baseDir.toString(), "oldDir");
        Files.createDirectories(oldDirPath);
        updatePathAccessTimes(oldDirPath, -2 * DAY_IN_MS);

        Path newDirPath = Files.createDirectories(Paths.get(baseDir.toString(), "newDir"));

        long ttlInMs = DAY_IN_MS;

        // when
        FileUtils.deleteOldFilesInDirectory(baseDir, ttlInMs);

        // then
        assertThat(Files.notExists(fileToDelete), is(equalTo(true)));
        assertThat(Files.exists(newFilePath), is(equalTo(true)));
        assertThat(Files.exists(oldDirPath), is(equalTo(true)));
        assertThat(Files.exists(newDirPath), is(equalTo(true)));
    }


    @Test
    public void test_deleteIfOld_should_tryToDeleteFile_when_lastModifiedEarlierThanTTL() throws IOException {
        // given
        Path oldFile = Paths.get(tempDir.toString(), "oldFile.txt");
        Files.createFile(oldFile);
        long ttlInMs = 24 * 60 * 60 * 1000;//1 day
        updatePathAccessTimes(oldFile, -ttlInMs);

        // when
        deleteIfOld(oldFile, ttlInMs);

        // then
        assertThat(Files.notExists(oldFile), is(equalTo(true)));
    }

    @Test
    public void test_deleteIfOld_should_notDeleteFile_when_lastModifiedLaterThanTTL() throws IOException {
        // given
        Path oldFile = Paths.get(tempDir.toString(), "oldFile.txt");
        Files.createFile(oldFile);

        long ttlInMs = 24 * 60 * 60 * 1000;//1 day
        updatePathAccessTimes(oldFile, -(ttlInMs / 2));

        // when
        deleteIfOld(oldFile, ttlInMs);

        // then
        assertThat(Files.notExists(oldFile), is(equalTo(false)));
    }


}